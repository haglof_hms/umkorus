
#include "stdafx.h"
#include <DBBaseClass_SQLApi.h>
#include <shlobj.h>
#include <SQLAPI.h>
#include "DBFunc.h"

const TCHAR SUBDIR_HMS[]		= _T("HMS");
const TCHAR SUBDIR_DATA[]		= _T("Data");
const TCHAR SUBDIR_KORUS[]	= _T("KORUS");

const TCHAR SQLCREATE_KORUS_DATA[] =		_T("CREATE TABLE [dbo].[korus_data]")
																				_T("(")																	
																				_T("[invtyp] int NOT NULL,")
																				_T("[regionid] int NOT NULL,")
																				_T("[distriktid] int NOT NULL,")
																				_T("[traktnr] int NOT NULL,")
																				_T("[standort] int NOT NULL,")
																				_T("[entreprenor] int NOT NULL,")
																				_T("[rapport_typ] int NOT NULL,")
																				_T("[artal] int NOT NULL,")
																				_T("[traktnamn] varchar(32) NULL,")
																				_T("[regionnamn] varchar(32) NULL,")
																				_T("[distriktnamn] varchar(32) NULL,")
																				_T("[entreprenornamn] varchar(64) NULL,")
																				_T("[ursprung] varchar(32) NULL,")
																				_T("[datum] varchar(32) NULL,")
																				_T("[markberedningsmetod] varchar(32) NOT NULL,")
																				_T("[datainsamlingsmetod] varchar(32) NOT NULL,")
																				_T("[areal] float NULL,")
																				_T("[malgrundyta] int NULL,")
																				_T("[stickvagssystem] varchar(32) NOT NULL,")
																				_T("[stickvagsavstand] int NULL,")
																				_T("[gisstracka] int NULL,")
																				_T("[gisareal] float NULL,")
																				_T("[provytestorlek] int NULL,")
																				_T("[kommentar] varchar(4096) NULL,")
																				_T("[antalvaltor] int NULL,")
																				_T("[avstand] int NULL,")
																				_T("[bedomdvolym] int NULL,")
																				_T("[hygge] int NULL,")
																				_T("[vagkant] int NULL,")
																				_T("[aker] int NULL,")
																				_T("[latbilshugg] bit NOT NULL,")
																				_T("[grotbil] bit NOT NULL,")
																				_T("[traktordragenhugg] bit NOT NULL,")
																				_T("[skotarburenhugg] bit NOT NULL,")
																				_T("[avlagg1norr] int NULL,")
																				_T("[avlagg1ost] int NULL,")
																				_T("[avlagg2norr] int NULL,")
																				_T("[avlagg2ost] int NULL,")
																				_T("[avlagg3norr] int NULL,")
																				_T("[avlagg3ost] int NULL,")
																				_T("[avlagg4norr] int NULL,")
																				_T("[avlagg4ost] int NULL,")
																				_T("[avlagg5norr] int NULL,")
																				_T("[avlagg5ost] int NULL,")
																				_T("[avlagg6norr] int NULL,")
																				_T("[avlagg6ost] int NULL,")
																				_T("[atgareal] float NULL,")																				
																				_T("CONSTRAINT [PK_data] PRIMARY KEY ")
																				_T("(")																				
																				_T("[invtyp],")
																				_T("[regionid],")
																				_T("[distriktid],")
																				_T("[traktnr],")
																				_T("[standort],")
																				_T("[entreprenor],")
																				_T("[rapport_typ],")
																				_T("[artal]")
																				_T(")")
																				_T(")");

const TCHAR SQLCREATE_KORUS_YTOR[] =		_T("CREATE TABLE [dbo].[korus_ytor]")
																				_T("(")
																				_T("[ytorid] int IDENTITY NOT NULL,")
																				_T("[yta] int NOT NULL,")																				
																				_T("[invtyp] int NOT NULL,")
																				_T("[regionid] int NOT NULL,")
																				_T("[distriktid] int NOT NULL,")
																				_T("[traktnr] int NOT NULL,")
																				_T("[standort] int NOT NULL,")
																				_T("[entreprenor] int NOT NULL,")
																				_T("[rapport_typ] int NOT NULL,")
																				_T("[artal] int NOT NULL,")
																				_T("[optimalt] int NULL,")															
																				_T("[bra] int NULL,")
																				_T("[varavbattre] int NULL,")
																				_T("[blekjordsflack] int NULL,")
																				_T("[optimaltbra] int NULL,")
																				_T("[ovrigt] int NULL,")
																				_T("[planterade] int NULL,")
																				_T("[planteringsdjupfel] int NULL,")
																				_T("[tilltryckning] int NULL,")
																				_T("[stickvbredd] float NULL,")
																				_T("[tall] int NULL,")
																				_T("[tallmedelhojd] float NULL,")
																				_T("[gran] int NULL,")
																				_T("[granmedelhojd] float NULL,")
																				_T("[lov] int NULL,")
																				_T("[lovmedelhojd] float NULL,")
																				_T("[contorta] int NULL,")
																				_T("[contortamedelhojd] float NULL,")
																				_T("[hst] int NULL,")
																				_T("[mhojd] float NULL,")
																				_T("[summa] int NULL,")
																				_T("[skador] int NULL,")
																				_T("[rojstam] int NULL,")																				
																				_T("CONSTRAINT [PK_ytor] PRIMARY KEY ")
																				_T("(")
																				_T("[ytorid]")
																				_T(")")
																				_T(")");


const TCHAR SQLCREATE_KORUS_FRAGOR[] =	_T("CREATE TABLE [dbo].[korus_fragor]")
																				_T("(")
																				_T("[fragorid] int IDENTITY NOT NULL,")																				
																				_T("[invtyp] int NOT NULL,")
																				_T("[regionid] int NOT NULL,")
																				_T("[distriktid] int NOT NULL,")
																				_T("[traktnr] int NOT NULL,")
																				_T("[standort] int NOT NULL,")
																				_T("[entreprenor] int NOT NULL,")
																				_T("[rapport_typ] int NOT NULL,")
																				_T("[artal] int NOT NULL,")
																				_T("[fraga1] varchar(64) NULL,")
																				_T("[fraga2] varchar(64) NULL,")
																				_T("[fraga3] varchar(64) NULL,")
																				_T("[fraga4] varchar(64) NULL,")
																				_T("[fraga5] varchar(64) NULL,")
																				_T("[fraga6] varchar(64) NULL,")
																				_T("[fraga7] varchar(64) NULL,")
																				_T("[fraga8] varchar(64) NULL,")
																				_T("[fraga9] varchar(64) NULL,")
																				_T("[fraga10] varchar(64) NULL,")
																				_T("[fraga11] varchar(64) NULL,")
																				_T("[fraga12] varchar(64) NULL,")
																				_T("[fraga13] varchar(64) NULL,")
																				_T("[fraga14] varchar(64) NULL,")
																				_T("[fraga15] varchar(64) NULL,")
																				_T("[fraga16] varchar(64) NULL,")
																				_T("[fraga17] varchar(64) NULL,")
																				_T("[fraga18] varchar(64) NULL,")
																				_T("[fraga19] varchar(64) NULL,")
																				_T("[fraga20] varchar(64) NULL,")
																				_T("[fraga21] varchar(64) NULL,")
																				_T("[ovrigt] varchar(4096) NULL,")
																				_T("CONSTRAINT [PK_fragor] PRIMARY KEY ")
																				_T("(")
																				_T("[fragorid]")
																				_T(")")
																				_T(")");

const TCHAR SQLCREATE_KORUS_ENTREPRENORER[] =	_T("CREATE TABLE [dbo].[korus_entreprenorer]")
																				_T("(")
																				_T("[entreprenorid] int IDENTITY NOT NULL,")
																				_T("[regionid] int NOT NULL,")
																				_T("[entreprenornamn] varchar(64) NULL,")
																				_T("CONSTRAINT [PK_entreprenorer] PRIMARY KEY ")
																				_T("(")
																				_T("[entreprenorid],")
																				_T("[regionid]")
																				_T(")")
																				_T(")");


const TCHAR SQLCONSTRAINT_KORUS_DATA_YTOR[]		= _T("ALTER TABLE [korus_ytor] ADD CONSTRAINT [FK_korus_ytor_korus_data] FOREIGN KEY([invtyp], [regionid], [distriktid], [traktnr], [standort], [entreprenor], [rapport_typ], [artal])")
																								_T("REFERENCES [korus_data] ([invtyp], [regionid], [distriktid], [traktnr], [standort], [entreprenor], [rapport_typ], [artal])")
																								_T("ON UPDATE CASCADE ")
																								_T("ON DELETE CASCADE");

const TCHAR SQLCONSTRAINT_KORUS_DATA_FRAGOR[]	= _T("ALTER TABLE [korus_fragor] ADD CONSTRAINT [FK_korus_fragor_korus_data] FOREIGN KEY([invtyp], [regionid], [distriktid], [traktnr], [standort], [entreprenor], [rapport_typ], [artal])")
																								_T("REFERENCES [korus_data] ([invtyp], [regionid], [distriktid], [traktnr], [standort], [entreprenor], [rapport_typ], [artal])")
																								_T("ON UPDATE CASCADE ")
																								_T("ON DELETE CASCADE");



CDBFunc::CDBFunc( DB_CONNECTION_DATA &condata )
	: CDBBaseClass_SQLApi(condata, 1)
{
}

CDBFunc::~CDBFunc()
{}

void CDBFunc::setupForDBConnection( HWND hWndReciv, HWND hWndSend )
{
	// Request connection info from shell
	if (hWndReciv != NULL)
	{
		DB_CONNECTION_DATA data;
		COPYDATASTRUCT HSData;

		memset(&HSData, 0, sizeof(COPYDATASTRUCT));
		HSData.dwData = 1;
		HSData.cbData = sizeof(DB_CONNECTION_DATA);
		HSData.lpData = &data;

		data.conn = NULL;

		::SendMessage(hWndReciv, WM_COPYDATA, (WPARAM)hWndSend, (LPARAM)&HSData);
	}
}

bool CDBFunc::DeleteKORUSYta(Egenuppfoljning::KorusDataEventArgs^ &kh)
{
	try
	{
		CString str;
		str.Format( _T("Vill du ta bort KORUS Yta nr %d för id: \nInvTyp/Region/distrikt/traktnr/traktdel/entreprnör: %d/%d/%d/%d/%d/%d ?"),
			kh->yta.yta, kh->data.invtyp, kh->data.regionid, kh->data.distriktid, kh->data.traktnr, kh->data.standort, kh->data.entreprenor, kh->data.rapport_typ, kh->data.årtal );

		if(AfxMessageBox(str, MB_YESNO | MB_ICONQUESTION)==IDYES)
		{
			m_saCommand.setCommandText(_T("DELETE FROM korus_ytor WHERE regionid = :1 AND distriktid = :2 AND traktnr = :3 AND standort = :4 AND entreprenor = :5 AND rapport_typ = :6 AND artal = :7 AND yta = :8 AND invtyp = :9" ));
			m_saCommand.Param(1).setAsLong()		= kh->data.regionid;
			m_saCommand.Param(2).setAsLong()		= kh->data.distriktid;
			m_saCommand.Param(3).setAsLong()		= kh->data.traktnr;
			m_saCommand.Param(4).setAsLong()		= kh->data.standort;
			m_saCommand.Param(5).setAsLong()		= kh->data.entreprenor;
			m_saCommand.Param(6).setAsLong()		= kh->data.rapport_typ;
			m_saCommand.Param(7).setAsLong()		= kh->data.årtal;
			m_saCommand.Param(8).setAsLong()		= kh->yta.yta;
			m_saCommand.Param(8).setAsLong()		= kh->data.invtyp;
			m_saCommand.Execute();
		}
		else
		{
			return false;
		}
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CDBFunc::DeleteKORUSEntreprenör(Egenuppfoljning::KorusDataEventArgs^ &kh)
{
	try
	{
		CString str;
		str.Format( _T("Vill du ta bort entreprenör för id: %d ?"),kh->data.entreprenor);

		if(AfxMessageBox(str, MB_YESNO | MB_ICONQUESTION)==IDYES)
		{
			m_saCommand.setCommandText(_T("DELETE FROM korus_entreprenorer WHERE entreprenorid = :1 AND regionid = :2"));
			m_saCommand.Param(1).setAsLong()		= kh->data.entreprenor;
			m_saCommand.Param(2).setAsLong()		= kh->data.regionid;
			m_saCommand.Execute();
		}
		else
		{
			return false;
		}
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CDBFunc::DeleteKORUSFrågeformulär(Egenuppfoljning::KorusDataEventArgs^ &kh)
{
	try
	{
		CString str;
		str.Format( _T("Vill du ta bort KORUS Frågeformulär för id: \nInvTyp/Region/distrikt/traktnr/traktdel/entreprnör: %d/%d/%d/%d/%d/%d ?"),
			kh->data.invtyp,kh->data.regionid, kh->data.distriktid, kh->data.traktnr, kh->data.standort, kh->data.entreprenor, kh->data.rapport_typ, kh->data.årtal );

		if(AfxMessageBox(str, MB_YESNO | MB_ICONQUESTION)==IDYES)
		{
			m_saCommand.setCommandText(_T("DELETE FROM korus_fragor WHERE regionid = :1 AND distriktid = :2 AND traktnr = :3 AND standort = :4 AND entreprenor = :5 AND rapport_typ = :6 AND artal = :7 AND invtyp = :8"));
			m_saCommand.Param(1).setAsLong()		= kh->data.regionid;
			m_saCommand.Param(2).setAsLong()		= kh->data.distriktid;
			m_saCommand.Param(3).setAsLong()		= kh->data.traktnr;
			m_saCommand.Param(4).setAsLong()		= kh->data.standort;
			m_saCommand.Param(5).setAsLong()		= kh->data.entreprenor;
			m_saCommand.Param(6).setAsLong()		= kh->data.rapport_typ;
			m_saCommand.Param(7).setAsLong()		= kh->data.årtal;
			m_saCommand.Param(8).setAsLong()		= kh->data.invtyp;
			m_saCommand.Execute();
		}
		else
		{
			return false;
		}
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

bool CDBFunc::DeleteKORUSData(Egenuppfoljning::KorusDataEventArgs^ &kh)
{
	try
	{
		CString str;
		str.Format( _T("Vill du ta bort KORUS Data för id \nInvTyp/Region/distrikt/traktnr/traktdel/entreprnör: %d/%d/%d/%d/%d/%d ?"),
			kh->data.invtyp,kh->data.regionid, kh->data.distriktid, kh->data.traktnr, kh->data.standort, kh->data.entreprenor, kh->data.rapport_typ, kh->data.årtal );

		if(AfxMessageBox(str, MB_YESNO | MB_ICONQUESTION)==IDYES)
		{
			m_saCommand.setCommandText(_T("DELETE FROM korus_data WHERE regionid = :1 AND distriktid = :2 AND traktnr = :3 AND standort = :4 AND entreprenor = :5 AND rapport_typ = :6 AND artal = :7 AND invtyp = :8"));
			m_saCommand.Param(1).setAsLong()		= kh->data.regionid;
			m_saCommand.Param(2).setAsLong()		= kh->data.distriktid;
			m_saCommand.Param(3).setAsLong()		= kh->data.traktnr;
			m_saCommand.Param(4).setAsLong()		= kh->data.standort;
			m_saCommand.Param(5).setAsLong()		= kh->data.entreprenor;
			m_saCommand.Param(6).setAsLong()		= kh->data.rapport_typ;
			m_saCommand.Param(7).setAsLong()		= kh->data.årtal;
			m_saCommand.Param(8).setAsLong()		= kh->data.invtyp;
			m_saCommand.Execute();
		}
		else
		{
			return false;
		}
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

List<Egenuppfoljning::KorusDataEventArgs::Entreprenör>^ CDBFunc::GetEntreprenörerLista()
{
	List<Egenuppfoljning::KorusDataEventArgs::Entreprenör>^ entreprenörer= gcnew List<Egenuppfoljning::KorusDataEventArgs::Entreprenör>();
	try
	{
		m_saCommand.setCommandText(_T("SELECT * FROM korus_entreprenorer ORDER BY entreprenorid, regionid"));
		m_saCommand.Execute();
		while( m_saCommand.FetchNext() )
		{
			Egenuppfoljning::KorusDataEventArgs::Entreprenör entreprenör;
			entreprenör.id = m_saCommand.Field("entreprenorid").asLong();
			entreprenör.regionid = m_saCommand.Field("regionid").asLong(); 
			entreprenör.namn = gcnew String(m_saCommand.Field("entreprenornamn").asString());
			entreprenörer->Add(entreprenör);
		}
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
	}

	return entreprenörer;
}
List<Egenuppfoljning::KorusDataEventArgs::Frågor>^ CDBFunc::GetFragorLista()
{
  Egenuppfoljning::KorusDataEventArgs^ tomData = gcnew Egenuppfoljning::KorusDataEventArgs();
  return GetFragorLista(tomData);
}

void CDBFunc::PrepareSelectCommand(Egenuppfoljning::KorusDataEventArgs^ &kh, CString aTable)
{
	CString strCommand;
	if(kh->data.invtyp <0 && kh->data.regionid <=0 && kh->data.distriktid <=0 && kh->data.traktnr <=0 && kh->data.standort <=0 && kh->data.entreprenor <=0 && kh->data.årtal <=0)
	{	
		strCommand.Format(_T("SELECT * FROM %s ORDER BY traktnr, invtyp, regionid, distriktid, standort, entreprenor, artal"),aTable);
		SAString sastring = SAString(strCommand);
		m_saCommand.setCommandText(sastring);
	}
	else
	{
		//ORDER BY traktnr, regionid, distriktid, standort, entreprenor, artal
		strCommand.Format(_T("SELECT * FROM %s WHERE "),aTable);

		if(kh->data.invtyp>=0)
		{
			strCommand.Append(_T("invtyp = :IT AND "));
		}
		if(kh->data.regionid>0)
		{
			strCommand.Append(_T("regionid = :RI AND "));
		}
		if(kh->data.distriktid>0)
		{
			strCommand.Append(_T("distriktid = :DI AND "));
		}
		if(kh->data.traktnr>0)
		{
			strCommand.Append(_T("traktnr = :TN AND "));
		}
		if(kh->data.standort>0)
		{
			strCommand.Append(_T("standort = :SO AND "));
		}
		if(kh->data.entreprenor>0)
		{
			strCommand.Append(_T("entreprenor = :ENT AND "));
		}
		if(kh->data.rapport_typ>0)
		{
			strCommand.Append(_T("rapport_typ = :RTYP AND "));
		}
		if(kh->data.årtal>0)
		{
			strCommand.Append(_T("artal = :AR AND "));
		}

		strCommand.Delete(strCommand.GetLength()-5,5); // Ta bort " AND "
		strCommand.Append(_T(" ORDER BY traktnr, invtyp, regionid, distriktid, standort, entreprenor, artal"));

		SAString sastring = SAString(strCommand);
		m_saCommand.setCommandText(sastring);

		if(kh->data.invtyp>=0)
		{
			m_saCommand.Param("IT").setAsLong() = kh->data.invtyp;
		}
		if(kh->data.regionid>0)
		{
			m_saCommand.Param("RI").setAsLong() = kh->data.regionid;
		}
		if(kh->data.distriktid>0)
		{
			m_saCommand.Param("DI").setAsLong() = kh->data.distriktid;
		}
		if(kh->data.traktnr>0)
		{
			m_saCommand.Param("TN").setAsLong() = kh->data.traktnr;
		}
		if(kh->data.standort>0)
		{
			m_saCommand.Param("SO").setAsLong() = kh->data.standort;
		}
		if(kh->data.entreprenor>0)
		{
			m_saCommand.Param("ENT").setAsLong() = kh->data.entreprenor;
		}
		if(kh->data.rapport_typ>0)
		{
			m_saCommand.Param("RTYP").setAsLong() = kh->data.rapport_typ;
		}
		if(kh->data.årtal>0)
		{
			m_saCommand.Param("AR").setAsLong() = kh->data.årtal;
		}
	}
 
}

List<Egenuppfoljning::KorusDataEventArgs::Frågor>^ CDBFunc::GetFragorLista(Egenuppfoljning::KorusDataEventArgs^ &kh)
{
	List<Egenuppfoljning::KorusDataEventArgs::Frågor>^ frågor= gcnew List<Egenuppfoljning::KorusDataEventArgs::Frågor>();
	frågor->Clear();
	try
	{
		PrepareSelectCommand(kh,_T("korus_fragor"));
		m_saCommand.Execute();
		while( m_saCommand.FetchNext() )
		{
			Egenuppfoljning::KorusDataEventArgs::Frågor frågeformulär;

			frågeformulär.fragorid = m_saCommand.Field("fragorid").asLong();

			//Nyckel variabler
			frågeformulär.invtyp = m_saCommand.Field("invtyp").asLong(); //int
			frågeformulär.regionid = m_saCommand.Field("regionid").asLong(); //int
			frågeformulär.distriktid = m_saCommand.Field("distriktid").asLong();
			frågeformulär.traktnr = m_saCommand.Field("traktnr").asLong();
			frågeformulär.standort = m_saCommand.Field("standort").asLong();
			frågeformulär.entreprenor = m_saCommand.Field("entreprenor").asLong();
			frågeformulär.rapport_typ = m_saCommand.Field("rapport_typ").asLong();
			frågeformulär.årtal = m_saCommand.Field("artal").asLong();

			frågeformulär.fraga1 = gcnew String(m_saCommand.Field("fraga1").asString());
			frågeformulär.fraga2 = gcnew String(m_saCommand.Field("fraga2").asString());
			frågeformulär.fraga3 = gcnew String(m_saCommand.Field("fraga3").asString());
			frågeformulär.fraga4 = gcnew String(m_saCommand.Field("fraga4").asString());
			frågeformulär.fraga5 = gcnew String(m_saCommand.Field("fraga5").asString());
			frågeformulär.fraga6 = gcnew String(m_saCommand.Field("fraga6").asString());
			frågeformulär.fraga7 = gcnew String(m_saCommand.Field("fraga7").asString());
			frågeformulär.fraga8 = gcnew String(m_saCommand.Field("fraga8").asString());
			frågeformulär.fraga9 = gcnew String(m_saCommand.Field("fraga9").asString());
			frågeformulär.fraga10 = gcnew String(m_saCommand.Field("fraga10").asString());
			frågeformulär.fraga11 = gcnew String(m_saCommand.Field("fraga11").asString());
			frågeformulär.fraga12 = gcnew String(m_saCommand.Field("fraga12").asString());
			frågeformulär.fraga13 = gcnew String(m_saCommand.Field("fraga13").asString());
			frågeformulär.fraga14 = gcnew String(m_saCommand.Field("fraga14").asString());
			frågeformulär.fraga15 = gcnew String(m_saCommand.Field("fraga15").asString());
			frågeformulär.fraga16 = gcnew String(m_saCommand.Field("fraga16").asString());
			frågeformulär.fraga17 = gcnew String(m_saCommand.Field("fraga17").asString());
			frågeformulär.fraga18 = gcnew String(m_saCommand.Field("fraga18").asString());
			frågeformulär.fraga19 = gcnew String(m_saCommand.Field("fraga19").asString());
			frågeformulär.fraga20 = gcnew String(m_saCommand.Field("fraga20").asString());
			frågeformulär.fraga21 = gcnew String(m_saCommand.Field("fraga21").asString());
			frågeformulär.ovrigt = gcnew String(m_saCommand.Field("ovrigt").asString());

			frågor->Add(frågeformulär);
		}
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
	}

	return frågor;
}

List<Egenuppfoljning::KorusDataEventArgs::Yta>^ CDBFunc::GetYtorLista()
{
	Egenuppfoljning::KorusDataEventArgs^ tomData = gcnew Egenuppfoljning::KorusDataEventArgs();
	return GetYtorLista(tomData);
}

List<Egenuppfoljning::KorusDataEventArgs::Yta>^ CDBFunc::GetYtorLista(Egenuppfoljning::KorusDataEventArgs^ &kh)
{
	Egenuppfoljning::KorusDataEventArgs::Yta yta;
	List<Egenuppfoljning::KorusDataEventArgs::Yta>^ ytor= gcnew List<Egenuppfoljning::KorusDataEventArgs::Yta>();
	ytor->Clear();

	try
	{
		PrepareSelectCommand(kh,_T("korus_ytor"));
		m_saCommand.Execute();
		while( m_saCommand.FetchNext() )
		{
			Egenuppfoljning::KorusDataEventArgs::Yta yta;

			//Nyckel variabler
			yta.invtyp = m_saCommand.Field("invtyp").asLong(); //int
			yta.regionid = m_saCommand.Field("regionid").asLong(); //int
			yta.distriktid = m_saCommand.Field("distriktid").asLong();
			yta.traktnr = m_saCommand.Field("traktnr").asLong();
			yta.standort = m_saCommand.Field("standort").asLong();
			yta.entreprenor = m_saCommand.Field("entreprenor").asLong();
			yta.rapport_typ = m_saCommand.Field("rapport_typ").asLong();
			yta.årtal = m_saCommand.Field("artal").asLong();

			yta.yta = m_saCommand.Field("yta").asLong();
			yta.optimalt = m_saCommand.Field("optimalt").asLong();
			yta.bra = m_saCommand.Field("bra").asLong();
			yta.varavbattre = m_saCommand.Field("varavbattre").asLong();
			yta.blekjordsflack = m_saCommand.Field("blekjordsflack").asLong();
			yta.optimaltbra = m_saCommand.Field("optimaltbra").asLong();
			yta.ovrigt = m_saCommand.Field("ovrigt").asLong();
			yta.planterade = m_saCommand.Field("planterade").asLong();
			yta.planteringsdjupfel = m_saCommand.Field("planteringsdjupfel").asLong();
			yta.tilltryckning = m_saCommand.Field("tilltryckning").asLong();
			yta.stickvbredd = (float)m_saCommand.Field("stickvbredd").asDouble();
			yta.tall = m_saCommand.Field("tall").asLong();
			yta.tallmedelhojd = (float)m_saCommand.Field("tallmedelhojd").asDouble();
			yta.gran = m_saCommand.Field("gran").asLong();
			yta.granmedelhojd = (float)m_saCommand.Field("granmedelhojd").asDouble();
			yta.lov = m_saCommand.Field("lov").asLong();
			yta.lovmedelhojd = (float)m_saCommand.Field("lovmedelhojd").asDouble();
			yta.contorta = m_saCommand.Field("contorta").asLong();
			yta.contortamedelhojd = (float)m_saCommand.Field("contortamedelhojd").asDouble();
			yta.hst = m_saCommand.Field("hst").asLong();
			yta.mhojd = (float)m_saCommand.Field("mhojd").asDouble();
			yta.summa = m_saCommand.Field("summa").asLong();
			yta.skador = m_saCommand.Field("skador").asLong();
			yta.rojstam = m_saCommand.Field("rojstam").asLong();
			ytor->Add(yta);
		}
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
	}

	return ytor;
}


bool CDBFunc::UpdateKORUSRapporter(List<Egenuppfoljning::KorusDataEventArgs::Entreprenör>^ entreprenörer)
{
	try
	{
		for(int i=0;i<entreprenörer->Count;i++)
		{
			m_saCommand.setCommandText( _T("UPDATE korus_data SET entreprenornamn = :1 ")
				_T("WHERE entreprenor = :2") );

			m_saCommand.Param(1).setAsString()	= CString(entreprenörer[i].namn);
			m_saCommand.Param(2).setAsLong()		= entreprenörer[i].id;
			m_saCommand.Execute();
		}
	}
	catch( SAException &e )
	{
		// Undo any changes to db
		try
		{
			getDBConnection().conn->Rollback();
		}
		catch(...)
		{
		}

		AfxMessageBox( e.ErrText() );
		return false;
	}
	return true;
}

List<Egenuppfoljning::KorusDataEventArgs::Data>^ CDBFunc::GetRapporterLista()
{
	List<Egenuppfoljning::KorusDataEventArgs::Data>^ rapporter= gcnew List<Egenuppfoljning::KorusDataEventArgs::Data>();
	rapporter->Clear();

	try
	{
		m_saCommand.setCommandText(_T("SELECT * FROM korus_data ORDER BY traktnr, invtyp, regionid, distriktid, standort, entreprenor, artal"));
		m_saCommand.Execute();
		while( m_saCommand.FetchNext() )
		{
			//data=gcnew Egenuppfoljning::KorusDataEventArgs::Data();
			Egenuppfoljning::KorusDataEventArgs::Data data;

			//Nyckel variabler
			data.invtyp = m_saCommand.Field("invtyp").asLong(); //int
			data.regionid = m_saCommand.Field("regionid").asLong(); //int
			data.distriktid = m_saCommand.Field("distriktid").asLong();
			data.traktnr = m_saCommand.Field("traktnr").asLong();
			data.standort = m_saCommand.Field("standort").asLong();
			data.entreprenor = m_saCommand.Field("entreprenor").asLong();
			data.rapport_typ = m_saCommand.Field("rapport_typ").asLong();
			data.årtal = m_saCommand.Field("artal").asLong();

			data.traktnamn = gcnew String(m_saCommand.Field("traktnamn").asString()); //string
			data.regionnamn = gcnew String(m_saCommand.Field("regionnamn").asString());
			data.distriktnamn = gcnew String(m_saCommand.Field("distriktnamn").asString());
			data.entreprenornamn = gcnew String(m_saCommand.Field("entreprenornamn").asString());
			data.ursprung = gcnew String(m_saCommand.Field("ursprung").asString());
			data.datum = gcnew String(m_saCommand.Field("datum").asString());
			data.markberedningsmetod = gcnew String(m_saCommand.Field("markberedningsmetod").asString());
			data.datainsamlingsmetod = gcnew String(m_saCommand.Field("datainsamlingsmetod").asString());

			data.areal = (float)m_saCommand.Field("areal").asDouble(); //float
			data.malgrundyta = m_saCommand.Field("malgrundyta").asLong(); //int
			data.stickvagssystem = gcnew String(m_saCommand.Field("stickvagssystem").asString());	
			data.stickvagsavstand = m_saCommand.Field("stickvagsavstand").asLong();
			data.gisstracka = m_saCommand.Field("gisstracka").asLong();
			data.gisareal = (float)m_saCommand.Field("gisareal").asDouble(); //float
			data.provytestorlek = m_saCommand.Field("provytestorlek").asLong(); //int
			data.kommentar = gcnew String(m_saCommand.Field("kommentar").asString()); //string

			data.antalvaltor = m_saCommand.Field("antalvaltor").asLong(); //int
			data.avstand = m_saCommand.Field("avstand").asLong(); //int
			data.bedomdvolym = m_saCommand.Field("bedomdvolym").asLong(); //int
			data.hygge = m_saCommand.Field("hygge").asLong(); //int
			data.vagkant = m_saCommand.Field("vagkant").asLong(); //int
			data.aker = m_saCommand.Field("aker").asLong(); //int

			data.latbilshugg = m_saCommand.Field("latbilshugg").asBool(); //bool
			data.grotbil = m_saCommand.Field("grotbil").asBool(); //bool
			data.traktordragenhugg = m_saCommand.Field("traktordragenhugg").asBool(); //bool
			data.skotarburenhugg = m_saCommand.Field("skotarburenhugg").asBool(); //bool

			data.avlagg1norr = m_saCommand.Field("avlagg1norr").asLong(); //int
			data.avlagg1ost = m_saCommand.Field("avlagg1ost").asLong(); //int
			data.avlagg2norr = m_saCommand.Field("avlagg2norr").asLong(); //int
			data.avlagg2ost = m_saCommand.Field("avlagg2ost").asLong(); //int
			data.avlagg3norr = m_saCommand.Field("avlagg3norr").asLong(); //int
			data.avlagg3ost = m_saCommand.Field("avlagg3ost").asLong(); //int
			data.avlagg4norr = m_saCommand.Field("avlagg4norr").asLong(); //int
			data.avlagg4ost = m_saCommand.Field("avlagg4ost").asLong(); //int
			data.avlagg5norr = m_saCommand.Field("avlagg5norr").asLong(); //int
			data.avlagg5ost = m_saCommand.Field("avlagg5ost").asLong(); //int
			data.avlagg6norr = m_saCommand.Field("avlagg6norr").asLong(); //int
			data.avlagg6ost = m_saCommand.Field("avlagg6ost").asLong(); //int
			data.atgareal = (float)m_saCommand.Field("atgareal").asDouble(); //float
			rapporter->Add(data);
		}
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
	}

	return rapporter;
}

List<Egenuppfoljning::KorusDataEventArgs^>^ CDBFunc::GetKORUSData()
{
	Egenuppfoljning::KorusDataEventArgs^ kh;
	List<Egenuppfoljning::KorusDataEventArgs^>^ reports= gcnew List<Egenuppfoljning::KorusDataEventArgs^>();
	reports->Clear();

	try
	{
		m_saCommand.setCommandText(_T("SELECT * FROM korus_data ORDER BY invtyp, regionid, distriktid, traktnr, standort, entreprenor, rapport_typ, artal"));
		m_saCommand.Execute();
		while( m_saCommand.FetchNext() )
		{
			kh=gcnew Egenuppfoljning::KorusDataEventArgs();

			//Nyckel variabler
			kh->data.invtyp = m_saCommand.Field("invtyp").asLong(); //int
			kh->data.regionid = m_saCommand.Field("regionid").asLong(); //int
			kh->data.distriktid = m_saCommand.Field("distriktid").asLong();
			kh->data.traktnr = m_saCommand.Field("traktnr").asLong();
			kh->data.standort = m_saCommand.Field("standort").asLong();
			kh->data.entreprenor = m_saCommand.Field("entreprenor").asLong();
			kh->data.rapport_typ = m_saCommand.Field("rapport_typ").asLong();
			kh->data.årtal = m_saCommand.Field("artal").asLong();

			kh->data.traktnamn = gcnew String(m_saCommand.Field("traktnamn").asString()); //string
			kh->data.regionnamn = gcnew String(m_saCommand.Field("regionnamn").asString());
			kh->data.distriktnamn = gcnew String(m_saCommand.Field("distriktnamn").asString());
			kh->data.entreprenornamn = gcnew String(m_saCommand.Field("entreprenornamn").asString());
			kh->data.ursprung = gcnew String(m_saCommand.Field("ursprung").asString());
			kh->data.datum = gcnew String(m_saCommand.Field("datum").asString());
			kh->data.markberedningsmetod = gcnew String(m_saCommand.Field("markberedningsmetod").asString());
			kh->data.datainsamlingsmetod = gcnew String(m_saCommand.Field("datainsamlingsmetod").asString());

			kh->data.areal = (float)m_saCommand.Field("areal").asDouble(); //float
			kh->data.malgrundyta = m_saCommand.Field("malgrundyta").asLong(); //int
			kh->data.atgareal = (float)m_saCommand.Field("atgareal").asDouble();

			kh->data.stickvagssystem = gcnew String(m_saCommand.Field("stickvagssystem").asString());	
			kh->data.stickvagsavstand = m_saCommand.Field("stickvagsavstand").asLong();
			kh->data.gisstracka = m_saCommand.Field("gisstracka").asLong();
			kh->data.gisareal = (float)m_saCommand.Field("gisareal").asDouble(); //float
			kh->data.provytestorlek = m_saCommand.Field("provytestorlek").asLong(); //int
			kh->data.kommentar = gcnew String(m_saCommand.Field("kommentar").asString()); //string

			kh->data.antalvaltor = m_saCommand.Field("antalvaltor").asLong(); //int
			kh->data.avstand = m_saCommand.Field("avstand").asLong(); //int
			kh->data.bedomdvolym = m_saCommand.Field("bedomdvolym").asLong(); //int
			kh->data.hygge = m_saCommand.Field("hygge").asLong(); //int
			kh->data.vagkant = m_saCommand.Field("vagkant").asLong(); //int
			kh->data.aker = m_saCommand.Field("aker").asLong(); //int

			kh->data.latbilshugg = m_saCommand.Field("latbilshugg").asBool(); //bool
			kh->data.grotbil = m_saCommand.Field("grotbil").asBool(); //bool
			kh->data.traktordragenhugg = m_saCommand.Field("traktordragenhugg").asBool(); //bool
			kh->data.skotarburenhugg = m_saCommand.Field("skotarburenhugg").asBool(); //bool

			kh->data.avlagg1norr = m_saCommand.Field("avlagg1norr").asLong(); //int
			kh->data.avlagg1ost = m_saCommand.Field("avlagg1ost").asLong(); //int
			kh->data.avlagg2norr = m_saCommand.Field("avlagg2norr").asLong(); //int
			kh->data.avlagg2ost = m_saCommand.Field("avlagg2ost").asLong(); //int
			kh->data.avlagg3norr = m_saCommand.Field("avlagg3norr").asLong(); //int
			kh->data.avlagg3ost = m_saCommand.Field("avlagg3ost").asLong(); //int
			kh->data.avlagg4norr = m_saCommand.Field("avlagg4norr").asLong(); //int
			kh->data.avlagg4ost = m_saCommand.Field("avlagg4ost").asLong(); //int
			kh->data.avlagg5norr = m_saCommand.Field("avlagg5norr").asLong(); //int
			kh->data.avlagg5ost = m_saCommand.Field("avlagg5ost").asLong(); //int
			kh->data.avlagg6norr = m_saCommand.Field("avlagg6norr").asLong(); //int
			kh->data.avlagg6ost = m_saCommand.Field("avlagg6ost").asLong(); //int

			reports->Add(kh);
		}

		for(int i=0;i<reports->Count;i++)
		{
			m_saCommand.setCommandText( _T("SELECT * FROM korus_fragor ")
				_T("WHERE regionid = :1 AND distriktid = :2 AND traktnr = :3 AND standort = :4 AND entreprenor = :5 AND rapport_typ = :6 AND artal = :7 AND invtyp = :8") );
			m_saCommand.Param(1).setAsLong()	= reports[i]->data.regionid;
			m_saCommand.Param(2).setAsLong()	= reports[i]->data.distriktid;
			m_saCommand.Param(3).setAsLong()	= reports[i]->data.traktnr;
			m_saCommand.Param(4).setAsLong()	= reports[i]->data.standort;
			m_saCommand.Param(5).setAsLong()	= reports[i]->data.entreprenor;
			m_saCommand.Param(6).setAsLong()	= reports[i]->data.rapport_typ;
			m_saCommand.Param(7).setAsLong()	= reports[i]->data.årtal;
			m_saCommand.Param(8).setAsLong()	= reports[i]->data.invtyp;
			m_saCommand.Execute();
			m_saCommand.FetchNext();

			reports[i]->frågeformulär.fraga1 = gcnew String(m_saCommand.Field("fraga1").asString());
			reports[i]->frågeformulär.fraga2 = gcnew String(m_saCommand.Field("fraga2").asString());
			reports[i]->frågeformulär.fraga3 = gcnew String(m_saCommand.Field("fraga3").asString());
			reports[i]->frågeformulär.fraga4 = gcnew String(m_saCommand.Field("fraga4").asString());
			reports[i]->frågeformulär.fraga5 = gcnew String(m_saCommand.Field("fraga5").asString());
			reports[i]->frågeformulär.fraga6 = gcnew String(m_saCommand.Field("fraga6").asString());
			reports[i]->frågeformulär.fraga7 = gcnew String(m_saCommand.Field("fraga7").asString());
			reports[i]->frågeformulär.fraga8 = gcnew String(m_saCommand.Field("fraga8").asString());
			reports[i]->frågeformulär.fraga9 = gcnew String(m_saCommand.Field("fraga9").asString());
			reports[i]->frågeformulär.fraga10 = gcnew String(m_saCommand.Field("fraga10").asString());
			reports[i]->frågeformulär.fraga11 = gcnew String(m_saCommand.Field("fraga11").asString());
			reports[i]->frågeformulär.fraga12 = gcnew String(m_saCommand.Field("fraga12").asString());
			reports[i]->frågeformulär.fraga13 = gcnew String(m_saCommand.Field("fraga13").asString());
			reports[i]->frågeformulär.fraga14 = gcnew String(m_saCommand.Field("fraga14").asString());
			reports[i]->frågeformulär.fraga15 = gcnew String(m_saCommand.Field("fraga15").asString());
			reports[i]->frågeformulär.fraga16 = gcnew String(m_saCommand.Field("fraga16").asString());
			reports[i]->frågeformulär.fraga17 = gcnew String(m_saCommand.Field("fraga17").asString());
			reports[i]->frågeformulär.fraga18 = gcnew String(m_saCommand.Field("fraga18").asString());
			reports[i]->frågeformulär.fraga19 = gcnew String(m_saCommand.Field("fraga19").asString());
			reports[i]->frågeformulär.fraga20 = gcnew String(m_saCommand.Field("fraga20").asString());
			reports[i]->frågeformulär.fraga21 = gcnew String(m_saCommand.Field("fraga21").asString());
			reports[i]->frågeformulär.ovrigt = gcnew String(m_saCommand.Field("ovrigt").asString());

			m_saCommand.setCommandText( _T("SELECT * FROM korus_ytor ")
				_T("WHERE regionid = :1 AND distriktid = :2 AND traktnr = :3 AND standort = :4 AND entreprenor = :5 AND rapport_typ = :6 AND artal = :7 AND invtyp = :8 ORDER BY yta") );
			m_saCommand.Param(1).setAsLong()	= reports[i]->data.regionid;
			m_saCommand.Param(2).setAsLong()	= reports[i]->data.distriktid;
			m_saCommand.Param(3).setAsLong()	= reports[i]->data.traktnr;
			m_saCommand.Param(4).setAsLong()	= reports[i]->data.standort;
			m_saCommand.Param(5).setAsLong()	= reports[i]->data.entreprenor;
			m_saCommand.Param(6).setAsLong()	= reports[i]->data.rapport_typ;
			m_saCommand.Param(7).setAsLong()	= reports[i]->data.årtal;
			m_saCommand.Param(8).setAsLong()	= reports[i]->data.invtyp;
			m_saCommand.Execute();

			while( m_saCommand.FetchNext() )
			{
				Egenuppfoljning::KorusDataEventArgs::Yta yta;
				yta.yta=m_saCommand.Field("yta").asLong(); //löpnummer
				yta.optimalt = m_saCommand.Field("optimalt").asLong();
				yta.bra = m_saCommand.Field("bra").asLong();
				yta.varavbattre = m_saCommand.Field("varavbattre").asLong();
				yta.blekjordsflack = m_saCommand.Field("blekjordsflack").asLong();
				yta.optimaltbra = m_saCommand.Field("optimaltbra").asLong();
				yta.ovrigt = m_saCommand.Field("ovrigt").asLong();
				yta.planterade = m_saCommand.Field("planterade").asLong();
				yta.planteringsdjupfel = m_saCommand.Field("planteringsdjupfel").asLong();
				yta.tilltryckning = m_saCommand.Field("tilltryckning").asLong();
				yta.stickvbredd = (float)m_saCommand.Field("stickvbredd").asDouble();				
				yta.tall = m_saCommand.Field("tall").asLong();
				yta.tallmedelhojd = (float)m_saCommand.Field("tallmedelhojd").asDouble();
				yta.gran = m_saCommand.Field("gran").asLong();
				yta.granmedelhojd = (float)m_saCommand.Field("granmedelhojd").asDouble();
				yta.lov = m_saCommand.Field("lov").asLong();
				yta.lovmedelhojd = (float)m_saCommand.Field("lovmedelhojd").asDouble();
				yta.contorta = m_saCommand.Field("contorta").asLong();
				yta.contortamedelhojd = (float)m_saCommand.Field("contortamedelhojd").asDouble();
				yta.hst = m_saCommand.Field("hst").asLong();
				yta.mhojd = (float)m_saCommand.Field("mhojd").asDouble();
				yta.summa = m_saCommand.Field("summa").asLong();
				yta.skador = m_saCommand.Field("skador").asLong();
				yta.rojstam = m_saCommand.Field("rojstam").asLong();
				reports[i]->ytor->Add(yta);
			}
		}
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
	}

	return reports;
}

bool CDBFunc::UpdateKORUSEntreprenörer(List<Egenuppfoljning::KorusDataEventArgs::Entreprenör>^ aEntreprenörer)
{
	try
	{
		for(int i=0;i<aEntreprenörer->Count;i++)
		{
			//Update KORUS entreprenörer
			m_saCommand.setCommandText( _T("UPDATE korus_entreprenorer SET regionid = :2, entreprenornamn = :3 ")
				_T("WHERE entreprenorid = :1 AND regionid = :2 ") 
				_T("IF @@ROWCOUNT=0 ")
				_T("INSERT INTO korus_entreprenorer (entreprenorid, regionid, entreprenornamn) ")
				_T("VALUES(:1, :2, :3) ") );

			m_saCommand.Param(1).setAsLong()		= aEntreprenörer[i].id;
			m_saCommand.Param(2).setAsLong()		= aEntreprenörer[i].regionid;
			m_saCommand.Param(3).setAsString()	= CString(aEntreprenörer[i].namn);

			m_saCommand.Execute();
		}
	}
	catch( SAException &e )
	{
		// Undo any changes to db
		try
		{
			getDBConnection().conn->Rollback();
		}
		catch(...)
		{
		}

		AfxMessageBox( e.ErrText() );
		return false;
	}
	return true;
}

bool CDBFunc::UpdateKORUSFrågor(List<Egenuppfoljning::KorusDataEventArgs::Frågor>^ frågor)
{
	try
	{
		for(int i=0;i<frågor->Count;i++)
		{
			//Update KORUS Frågor
			m_saCommand.setCommandText( _T("UPDATE korus_fragor SET regionid = :1, distriktid = :2, traktnr = :3, standort = :4, entreprenor = :5, rapport_typ = :6, artal = :7, ")
				_T("fraga1 = :8, fraga2 = :9, fraga3 = :10, fraga4 = :11, fraga5 = :12, fraga6 = :13, fraga7 = :14, fraga8 = :15, ")
				_T("fraga9 = :16, fraga10 = :17, fraga11 = :18, fraga12 = :19, fraga13 = :20, fraga14 = :21, fraga15 = :22, fraga16 = :23, fraga17 = :24,  fraga18 = :25,  fraga19 = :26, fraga20 = :27, fraga21 = :28, ovrigt = :29, invtyp = :30 ")
				_T("WHERE regionid = :31 AND distriktid = :32 AND traktnr = :33 AND standort = :34 AND entreprenor = :35 AND rapport_typ = :36 AND artal = :37 AND invtyp = :38 ") 
				_T("IF @@ROWCOUNT=0 ")
				_T("INSERT INTO korus_fragor (regionid, distriktid, traktnr, standort, entreprenor, rapport_typ, artal, fraga1, fraga2, fraga3, fraga4, fraga5, fraga6, fraga7, fraga8, fraga9, fraga10, fraga11, fraga12, fraga13, fraga14, fraga15, fraga16, fraga17, fraga18, fraga19, fraga20, fraga21, ovrigt, invtyp) ")
				_T("VALUES(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15, :16, :17, :18, :19, :20, :21, :22, :23, :24, :25, :26, :27, :28, :29, :30)") );

			m_saCommand.Param(1).setAsLong()		= frågor[i].regionid;
			m_saCommand.Param(2).setAsLong()		= frågor[i].distriktid;
			m_saCommand.Param(3).setAsLong()		= frågor[i].traktnr;
			m_saCommand.Param(4).setAsLong()		= frågor[i].standort;
			m_saCommand.Param(5).setAsLong()		= frågor[i].entreprenor;
			m_saCommand.Param(6).setAsLong()		= frågor[i].rapport_typ;
			m_saCommand.Param(7).setAsLong()		= frågor[i].årtal;
			m_saCommand.Param(8).setAsString()	= CString(frågor[i].fraga1);
			m_saCommand.Param(9).setAsString()	= CString(frågor[i].fraga2);
			m_saCommand.Param(10).setAsString()	= CString(frågor[i].fraga3);
			m_saCommand.Param(11).setAsString()	= CString(frågor[i].fraga4);
			m_saCommand.Param(12).setAsString()	= CString(frågor[i].fraga5);
			m_saCommand.Param(13).setAsString()	= CString(frågor[i].fraga6);
			m_saCommand.Param(14).setAsString()	= CString(frågor[i].fraga7);
			m_saCommand.Param(15).setAsString()	= CString(frågor[i].fraga8);
			m_saCommand.Param(16).setAsString()	= CString(frågor[i].fraga9);
			m_saCommand.Param(17).setAsString()	= CString(frågor[i].fraga10);
			m_saCommand.Param(18).setAsString()	= CString(frågor[i].fraga11);
			m_saCommand.Param(19).setAsString()	= CString(frågor[i].fraga12);
			m_saCommand.Param(20).setAsString()	= CString(frågor[i].fraga13);
			m_saCommand.Param(21).setAsString()	= CString(frågor[i].fraga14);
			m_saCommand.Param(22).setAsString()	= CString(frågor[i].fraga15);
			m_saCommand.Param(23).setAsString()	= CString(frågor[i].fraga16);
			m_saCommand.Param(24).setAsString()	= CString(frågor[i].fraga17);
			m_saCommand.Param(25).setAsString()	= CString(frågor[i].fraga18);
			m_saCommand.Param(26).setAsString()	= CString(frågor[i].fraga19);
			m_saCommand.Param(27).setAsString()	= CString(frågor[i].fraga20);
			m_saCommand.Param(28).setAsString()	= CString(frågor[i].fraga21);
			m_saCommand.Param(29).setAsString()	= CString(frågor[i].ovrigt);
			m_saCommand.Param(30).setAsLong()	= frågor[i].invtyp;


			m_saCommand.Param(31).setAsLong()	= frågor[i].regionid;
			m_saCommand.Param(32).setAsLong()	= frågor[i].distriktid;
			m_saCommand.Param(33).setAsLong()	= frågor[i].traktnr;
			m_saCommand.Param(34).setAsLong()	= frågor[i].standort;
			m_saCommand.Param(35).setAsLong()	= frågor[i].entreprenor;
			m_saCommand.Param(36).setAsLong()	= frågor[i].rapport_typ;
			m_saCommand.Param(37).setAsLong()	= frågor[i].årtal;
			m_saCommand.Param(38).setAsLong()	= frågor[i].invtyp;
			m_saCommand.Execute();
		}
	}
	catch( SAException &e )
	{
		// Undo any changes to db
		try
		{
			getDBConnection().conn->Rollback();
		}
		catch(...)
		{
		}

		AfxMessageBox( e.ErrText() );
		return false;
	}
	return true;
}

bool CDBFunc::UpdateKORUSYtor(List<Egenuppfoljning::KorusDataEventArgs::Yta>^ ytor)
{
	try
	{
		for(int i=0;i<ytor->Count;i++)
		{
			//Update KORUS Frågor
			m_saCommand.setCommandText( _T("UPDATE korus_ytor SET yta = :1, regionid = :2, distriktid = :3, traktnr = :4, standort = :5, entreprenor = :6, rapport_typ = :7, artal = :8, ")
				_T("optimalt = :9, bra = :10, varavbattre = :11, blekjordsflack = :12, optimaltbra = :13, ovrigt = :14, planterade = :15, planteringsdjupfel = :16, ")
				_T("tilltryckning = :17, stickvbredd = :18, tall = :19, tallmedelhojd = :20, gran = :21, granmedelhojd = :22, lov = :23, lovmedelhojd = :24, contorta = :25, contortamedelhojd = :26, ")
				_T("hst = :27, mhojd = :28, summa = :29, skador = :30, invtyp = :31 , rojstam = :32  ")
				_T("WHERE yta = :33 AND regionid = :34 AND distriktid = :35 AND traktnr = :36 AND standort = :37 AND entreprenor = :38 AND rapport_typ = :39 AND artal = :40 AND invtyp = :41")
				_T("IF @@ROWCOUNT=0 ")
				_T("INSERT INTO korus_ytor (yta, regionid, distriktid, traktnr, standort, entreprenor, rapport_typ, artal, optimalt, bra, varavbattre, blekjordsflack, optimaltbra, ovrigt, planterade, planteringsdjupfel, tilltryckning, stickvbredd, tall, tallmedelhojd, gran, granmedelhojd, lov, lovmedelhojd, contorta, contortamedelhojd, hst, mhojd, summa, skador, invtyp, rojstam) ")
				_T("VALUES(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15, :16, :17, :18, :19, :20, :21, :22, :23, :24, :25, :26, :27, :28, :29, :30, :31, :32)") );

			m_saCommand.Param(1).setAsLong()		= ytor[i].yta;	
			m_saCommand.Param(2).setAsLong() = ytor[i].regionid;
			m_saCommand.Param(3).setAsLong() = ytor[i].distriktid;
			m_saCommand.Param(4).setAsLong() = ytor[i].traktnr;
			m_saCommand.Param(5).setAsLong() = ytor[i].standort;
			m_saCommand.Param(6).setAsLong() = ytor[i].entreprenor;
			m_saCommand.Param(7).setAsLong() = ytor[i].rapport_typ;
			m_saCommand.Param(8).setAsLong() = ytor[i].årtal;
			m_saCommand.Param(9).setAsLong()		= ytor[i].optimalt;
			m_saCommand.Param(10).setAsLong()		= ytor[i].bra;
			m_saCommand.Param(11).setAsLong()		= ytor[i].varavbattre;
			m_saCommand.Param(12).setAsLong()		= ytor[i].blekjordsflack;
			m_saCommand.Param(13).setAsLong()		= ytor[i].optimaltbra;
			m_saCommand.Param(14).setAsLong()		= ytor[i].ovrigt;
			m_saCommand.Param(15).setAsLong()		= ytor[i].planterade;
			m_saCommand.Param(16).setAsLong()		= ytor[i].planteringsdjupfel;
			m_saCommand.Param(17).setAsLong()		= ytor[i].tilltryckning;
			m_saCommand.Param(18).setAsDouble()	= ytor[i].stickvbredd;
			m_saCommand.Param(19).setAsLong()		= ytor[i].tall;
			m_saCommand.Param(20).setAsDouble()	= ytor[i].tallmedelhojd;
			m_saCommand.Param(21).setAsLong()		= ytor[i].gran;
			m_saCommand.Param(22).setAsDouble()	= ytor[i].granmedelhojd;
			m_saCommand.Param(23).setAsLong()		= ytor[i].lov;
			m_saCommand.Param(24).setAsDouble()	= ytor[i].lovmedelhojd;
			m_saCommand.Param(25).setAsLong()		= ytor[i].contorta;
			m_saCommand.Param(26).setAsDouble()	= ytor[i].contortamedelhojd;
			m_saCommand.Param(27).setAsLong()		= ytor[i].hst;
			m_saCommand.Param(28).setAsDouble()	= ytor[i].mhojd;
			m_saCommand.Param(29).setAsLong()		= ytor[i].summa;
			m_saCommand.Param(30).setAsLong()		= ytor[i].skador;
			m_saCommand.Param(31).setAsLong()		= ytor[i].invtyp;
			m_saCommand.Param(32).setAsLong()		= ytor[i].rojstam;

			m_saCommand.Param(33).setAsLong()	= ytor[i].yta;
			m_saCommand.Param(34).setAsLong()	= ytor[i].regionid;
			m_saCommand.Param(35).setAsLong()	= ytor[i].distriktid;
			m_saCommand.Param(36).setAsLong()	= ytor[i].traktnr;
			m_saCommand.Param(37).setAsLong()	= ytor[i].standort;
			m_saCommand.Param(38).setAsLong()	= ytor[i].entreprenor;
			m_saCommand.Param(39).setAsLong()	= ytor[i].rapport_typ;
			m_saCommand.Param(40).setAsLong()	= ytor[i].årtal;
			m_saCommand.Param(41).setAsLong()	= ytor[i].invtyp;
			m_saCommand.Execute();
		}
	}
	catch( SAException &e )
	{
		// Undo any changes to db
		try
		{
			getDBConnection().conn->Rollback();
		}
		catch(...)
		{
		}

		AfxMessageBox( e.ErrText() );
		return false;
	}
	return true;
}


bool CDBFunc::UpdateKORUSRapporter(List<Egenuppfoljning::KorusDataEventArgs::Data>^ rapporter)
{
	try
	{
		for(int i=0;i<rapporter->Count;i++)
		{

			//Update KORUS Data
			m_saCommand.setCommandText( _T("UPDATE korus_data SET regionid = :1, distriktid = :2, traktnr = :3, standort = :4, entreprenor = :5, rapport_typ = :6, artal = :7, ")
				_T("traktnamn = :8, regionnamn = :9, distriktnamn = :10, entreprenornamn = :11, ursprung = :12, datum = :13, markberedningsmetod = :14, datainsamlingsmetod = :15, areal = :16, malgrundyta = :17, stickvagssystem = :18, stickvagsavstand = :19, ")
				_T("gisstracka = :20, gisareal = :21, provytestorlek = :22, kommentar = :23, antalvaltor = :24, avstand = :25, bedomdvolym = :26, hygge = :27, vagkant = :28, aker = :29, latbilshugg = :30, grotbil = :31, traktordragenhugg = :32, skotarburenhugg = :33, avlagg1norr = :34, avlagg1ost = :35, avlagg2norr = :36, avlagg2ost = :37, avlagg3norr = :38, avlagg3ost = :39,	avlagg4norr = :40, avlagg4ost = :41, avlagg5norr = :42, avlagg5ost = :43, avlagg6norr = :44, avlagg6ost = :45, invtyp = :46 , atgareal = :47  ")
				_T("WHERE regionid = :48 AND distriktid = :49 AND traktnr = :50 AND standort = :51 AND entreprenor = :52 AND rapport_typ = :53 AND artal = :54 AND invtyp = :55")
				_T("IF @@ROWCOUNT=0 ")
				_T("INSERT INTO korus_data (regionid, distriktid, traktnr, standort, entreprenor, rapport_typ, artal, traktnamn, regionnamn, distriktnamn, entreprenornamn, ursprung, datum, markberedningsmetod, datainsamlingsmetod, areal, malgrundyta, stickvagssystem, stickvagsavstand, gisstracka, gisareal, provytestorlek, kommentar, antalvaltor, avstand, bedomdvolym, hygge, vagkant, aker, latbilshugg, grotbil, traktordragenhugg, skotarburenhugg, avlagg1norr, avlagg1ost, avlagg2norr, avlagg2ost, avlagg3norr, avlagg3ost,	avlagg4norr, avlagg4ost, avlagg5norr, avlagg5ost, avlagg6norr, avlagg6ost, invtyp, atgareal) ")
				_T("VALUES(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15, :16, :17, :18, :19, :20, :21, :22, :23, :24, :25, :26, :27, :28, :29, :30, :31, :32, :33, :34, :35, :36, :37, :38, :39, :40, :41, :42, :43, :44, :45, :46, :47)"));

			m_saCommand.Param(1).setAsLong()		= rapporter[i].regionid;
			m_saCommand.Param(2).setAsLong()		= rapporter[i].distriktid;
			m_saCommand.Param(3).setAsLong()		= rapporter[i].traktnr;
			m_saCommand.Param(4).setAsLong()		= rapporter[i].standort;
			m_saCommand.Param(5).setAsLong()		= rapporter[i].entreprenor;
			m_saCommand.Param(6).setAsLong()		= rapporter[i].rapport_typ;
			m_saCommand.Param(7).setAsLong()		= rapporter[i].årtal;

			m_saCommand.Param(8).setAsString()	= CString(rapporter[i].traktnamn);
			m_saCommand.Param(9).setAsString()	= CString(rapporter[i].regionnamn);
			m_saCommand.Param(10).setAsString()	= CString(rapporter[i].distriktnamn);
			m_saCommand.Param(11).setAsString()	= CString(rapporter[i].entreprenornamn);
			m_saCommand.Param(12).setAsString()	= CString(rapporter[i].ursprung);
			m_saCommand.Param(13).setAsString()	= CString(rapporter[i].datum);
			m_saCommand.Param(14).setAsString()	= CString(rapporter[i].markberedningsmetod);
			m_saCommand.Param(15).setAsString()	= CString(rapporter[i].datainsamlingsmetod);
			m_saCommand.Param(16).setAsDouble()		= rapporter[i].areal;
			m_saCommand.Param(17).setAsLong()		= rapporter[i].malgrundyta;
			m_saCommand.Param(18).setAsString()	= CString(rapporter[i].stickvagssystem);
			m_saCommand.Param(19).setAsLong()		= rapporter[i].stickvagsavstand;
			m_saCommand.Param(20).setAsLong()		= rapporter[i].gisstracka;
			m_saCommand.Param(21).setAsDouble()	= rapporter[i].gisareal;
			m_saCommand.Param(22).setAsLong()		= rapporter[i].provytestorlek;
			m_saCommand.Param(23).setAsString()	= CString(rapporter[i].kommentar);

			//Biobränsle
			m_saCommand.Param(24).setAsLong()		= rapporter[i].antalvaltor;
			m_saCommand.Param(25).setAsLong()		= rapporter[i].avstand;
			m_saCommand.Param(26).setAsLong()		= rapporter[i].bedomdvolym;
			m_saCommand.Param(27).setAsLong()		= rapporter[i].hygge;
			m_saCommand.Param(28).setAsLong()		= rapporter[i].vagkant;
			m_saCommand.Param(29).setAsLong()		= rapporter[i].aker;
			m_saCommand.Param(30).setAsBool()		= rapporter[i].latbilshugg;
			m_saCommand.Param(31).setAsBool()		= rapporter[i].grotbil;
			m_saCommand.Param(32).setAsBool()		= rapporter[i].traktordragenhugg;
			m_saCommand.Param(33).setAsBool()		= rapporter[i].skotarburenhugg;
			m_saCommand.Param(34).setAsLong()		= rapporter[i].avlagg1norr;
			m_saCommand.Param(35).setAsLong()		= rapporter[i].avlagg1ost;
			m_saCommand.Param(36).setAsLong()		= rapporter[i].avlagg2norr;
			m_saCommand.Param(37).setAsLong()		= rapporter[i].avlagg2ost;
			m_saCommand.Param(38).setAsLong()		= rapporter[i].avlagg3norr;
			m_saCommand.Param(39).setAsLong()		= rapporter[i].avlagg3ost;
			m_saCommand.Param(40).setAsLong()		= rapporter[i].avlagg4norr;
			m_saCommand.Param(41).setAsLong()		= rapporter[i].avlagg4ost;
			m_saCommand.Param(42).setAsLong()		= rapporter[i].avlagg5norr;
			m_saCommand.Param(43).setAsLong()		= rapporter[i].avlagg5ost;
			m_saCommand.Param(44).setAsLong()		= rapporter[i].avlagg6norr;
			m_saCommand.Param(45).setAsLong()		= rapporter[i].avlagg6ost;
			m_saCommand.Param(46).setAsLong()		= rapporter[i].invtyp;
			m_saCommand.Param(47).setAsDouble()		= rapporter[i].atgareal;
      
			//Id
			m_saCommand.Param(48).setAsLong()	= rapporter[i].regionid;
			m_saCommand.Param(49).setAsLong()	= rapporter[i].distriktid;
			m_saCommand.Param(50).setAsLong()	= rapporter[i].traktnr;
			m_saCommand.Param(51).setAsLong()	= rapporter[i].standort;
			m_saCommand.Param(52).setAsLong()	= rapporter[i].entreprenor;
			m_saCommand.Param(53).setAsLong()	= rapporter[i].rapport_typ;
			m_saCommand.Param(54).setAsLong()	= rapporter[i].årtal;
			m_saCommand.Param(55).setAsLong()	= rapporter[i].invtyp;
			m_saCommand.Execute();
		}
	}
	catch( SAException &e )
	{
		// Undo any changes to db
		try
		{
			getDBConnection().conn->Rollback();
		}
		catch(...)
		{
		}

		AfxMessageBox( e.ErrText() );
		return false;
	}
	return true;
}

bool CDBFunc::UpdateKORUSData(Egenuppfoljning::KorusDataEventArgs^ &kh)
{
	try
	{
		//Update KORUS Data
		m_saCommand.setCommandText( _T("UPDATE korus_data SET regionid = :1, distriktid = :2, traktnr = :3, standort = :4, entreprenor = :5, rapport_typ = :6, artal = :7, ")
			_T("traktnamn = :8, regionnamn = :9, distriktnamn = :10, entreprenornamn = :11, ursprung = :12, datum = :13, markberedningsmetod = :14, datainsamlingsmetod = :15, areal = :16, malgrundyta = :17, stickvagssystem = :18, stickvagsavstand = :19, ")
			_T("gisstracka = :20, gisareal = :21, provytestorlek = :22, kommentar = :23, antalvaltor = :24, avstand = :25, bedomdvolym = :26, hygge = :27, vagkant = :28, aker = :29, latbilshugg = :30, grotbil = :31, traktordragenhugg = :32, skotarburenhugg = :33, avlagg1norr = :34, avlagg1ost = :35, avlagg2norr = :36, avlagg2ost = :37, avlagg3norr = :38, avlagg3ost = :39,	avlagg4norr = :40, avlagg4ost = :41, avlagg5norr = :42, avlagg5ost = :43, avlagg6norr = :44, avlagg6ost = :45, invtyp = :46, atgareal = :47  ")
			_T("WHERE regionid = :48 AND distriktid = :49 AND traktnr = :50 AND standort = :51 AND entreprenor = :52 AND rapport_typ = :53 AND artal = :54 AND invtyp = :55 ")
			_T("IF @@ROWCOUNT=0 ")
			_T("INSERT INTO korus_data (regionid, distriktid, traktnr, standort, entreprenor, rapport_typ, artal, traktnamn, regionnamn, distriktnamn, entreprenornamn, ursprung, datum, markberedningsmetod, datainsamlingsmetod, areal, malgrundyta, stickvagssystem, stickvagsavstand, gisstracka, gisareal, provytestorlek, kommentar, antalvaltor, avstand, bedomdvolym, hygge, vagkant, aker, latbilshugg, grotbil, traktordragenhugg, skotarburenhugg, avlagg1norr, avlagg1ost, avlagg2norr, avlagg2ost, avlagg3norr, avlagg3ost,	avlagg4norr, avlagg4ost, avlagg5norr, avlagg5ost, avlagg6norr, avlagg6ost, invtyp, atgareal) ")
			_T("VALUES(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15, :16, :17, :18, :19, :20, :21, :22, :23, :24, :25, :26, :27, :28, :29, :30, :31, :32, :33, :34, :35, :36, :37, :38, :39, :40, :41, :42, :43, :44, :45, :46, :47)"));

		m_saCommand.Param(1).setAsLong()		= kh->data.regionid;
		m_saCommand.Param(2).setAsLong()		= kh->data.distriktid;
		m_saCommand.Param(3).setAsLong()		= kh->data.traktnr;
		m_saCommand.Param(4).setAsLong()		= kh->data.standort;
		m_saCommand.Param(5).setAsLong()		= kh->data.entreprenor;
		m_saCommand.Param(6).setAsLong()		= kh->data.rapport_typ;
		m_saCommand.Param(7).setAsLong()		= kh->data.årtal;
		m_saCommand.Param(8).setAsString()	= CString(kh->data.traktnamn);
		m_saCommand.Param(9).setAsString()	= CString(kh->data.regionnamn);
		m_saCommand.Param(10).setAsString()	= CString(kh->data.distriktnamn);
		m_saCommand.Param(11).setAsString()	= CString(kh->data.entreprenornamn);
		m_saCommand.Param(12).setAsString()	= CString(kh->data.ursprung);
		m_saCommand.Param(13).setAsString()	= CString(kh->data.datum);
		m_saCommand.Param(14).setAsString()	= CString(kh->data.markberedningsmetod);
		m_saCommand.Param(15).setAsString()	= CString(kh->data.datainsamlingsmetod);
		m_saCommand.Param(16).setAsDouble()		= kh->data.areal;
		m_saCommand.Param(17).setAsLong()		= kh->data.malgrundyta;
		m_saCommand.Param(18).setAsString()	= CString(kh->data.stickvagssystem);
		m_saCommand.Param(19).setAsLong()		= kh->data.stickvagsavstand;
		m_saCommand.Param(20).setAsLong()		= kh->data.gisstracka;
		m_saCommand.Param(21).setAsDouble()	= kh->data.gisareal;
		m_saCommand.Param(22).setAsLong()		= kh->data.provytestorlek;
		m_saCommand.Param(23).setAsString()	= CString(kh->data.kommentar);

		m_saCommand.Param(24).setAsLong()		= kh->data.antalvaltor;
		m_saCommand.Param(25).setAsLong()		= kh->data.avstand;
		m_saCommand.Param(26).setAsLong()		= kh->data.bedomdvolym;
		m_saCommand.Param(27).setAsLong()		= kh->data.hygge;
		m_saCommand.Param(28).setAsLong()		= kh->data.vagkant;
		m_saCommand.Param(29).setAsLong()		= kh->data.aker;
		m_saCommand.Param(30).setAsBool()		= kh->data.latbilshugg;
		m_saCommand.Param(31).setAsBool()		= kh->data.grotbil;
		m_saCommand.Param(32).setAsBool()		= kh->data.traktordragenhugg;
		m_saCommand.Param(33).setAsBool()		= kh->data.skotarburenhugg;
		m_saCommand.Param(34).setAsLong()		= kh->data.avlagg1norr;
		m_saCommand.Param(35).setAsLong()		= kh->data.avlagg1ost;
		m_saCommand.Param(36).setAsLong()		= kh->data.avlagg2norr;
		m_saCommand.Param(37).setAsLong()		= kh->data.avlagg2ost;
		m_saCommand.Param(38).setAsLong()		= kh->data.avlagg3norr;
		m_saCommand.Param(39).setAsLong()		= kh->data.avlagg3ost;
		m_saCommand.Param(40).setAsLong()		= kh->data.avlagg4norr;
		m_saCommand.Param(41).setAsLong()		= kh->data.avlagg4ost;
		m_saCommand.Param(42).setAsLong()		= kh->data.avlagg5norr;
		m_saCommand.Param(43).setAsLong()		= kh->data.avlagg5ost;
		m_saCommand.Param(44).setAsLong()		= kh->data.avlagg6norr;
		m_saCommand.Param(45).setAsLong()		= kh->data.avlagg6ost;
		m_saCommand.Param(46).setAsLong()		= kh->data.invtyp;
		m_saCommand.Param(47).setAsDouble()		= kh->data.atgareal;
		//id
		m_saCommand.Param(48).setAsLong()	= kh->data.regionid;
		m_saCommand.Param(49).setAsLong()	= kh->data.distriktid;
		m_saCommand.Param(50).setAsLong()	= kh->data.traktnr;
		m_saCommand.Param(51).setAsLong()	= kh->data.standort;
		m_saCommand.Param(52).setAsLong()	= kh->data.entreprenor;
		m_saCommand.Param(53).setAsLong()	= kh->data.rapport_typ;
		m_saCommand.Param(54).setAsLong()	= kh->data.årtal;
		m_saCommand.Param(55).setAsLong()	= kh->data.invtyp;
		m_saCommand.Execute();

		
		//Update KORUS Frågor
		m_saCommand.setCommandText( _T("UPDATE korus_fragor SET regionid = :1, distriktid = :2, traktnr = :3, standort = :4, entreprenor = :5, rapport_typ = :6, artal = :7, ")
			_T("fraga1 = :8, fraga2 = :9, fraga3 = :10, fraga4 = :11, fraga5 = :12, fraga6 = :13, fraga7 = :14, fraga8 = :15, ")
			_T("fraga9 = :16, fraga10 = :17, fraga11 = :18, fraga12 = :19, fraga13 = :20, fraga14 = :21, fraga15 = :22, fraga16 = :23, fraga17 = :24, fraga18 = :25, fraga19 = :26, fraga20 = :27, fraga21 = :28, ovrigt = :29 , invtyp = :30 ")
			_T("WHERE regionid = :31 AND distriktid = :32 AND traktnr = :33 AND standort = :34 AND entreprenor = :35 AND rapport_typ = :36 AND artal = :37 AND invtyp = :38 ") 
			_T("IF @@ROWCOUNT=0 ")
			_T("INSERT INTO korus_fragor (regionid, distriktid, traktnr, standort, entreprenor, rapport_typ, artal, fraga1, fraga2, fraga3, fraga4, fraga5, fraga6, fraga7, fraga8, fraga9, fraga10, fraga11, fraga12, fraga13, fraga14, fraga15, fraga16, fraga17, fraga18, fraga19, fraga20, fraga21, ovrigt, invtyp) ")
			_T("VALUES(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15, :16, :17, :18, :19, :20, :21, :22, :23, :24, :25, :26, :27, :28, :29, :30)") );

		m_saCommand.Param(1).setAsLong()		= kh->data.regionid;
		m_saCommand.Param(2).setAsLong()		= kh->data.distriktid;
		m_saCommand.Param(3).setAsLong()		= kh->data.traktnr;
		m_saCommand.Param(4).setAsLong()		= kh->data.standort;
		m_saCommand.Param(5).setAsLong()		= kh->data.entreprenor;
		m_saCommand.Param(6).setAsLong()		= kh->data.rapport_typ;
		m_saCommand.Param(7).setAsLong()		= kh->data.årtal;
		m_saCommand.Param(8).setAsString()	= CString(kh->frågeformulär.fraga1);
		m_saCommand.Param(9).setAsString()	= CString(kh->frågeformulär.fraga2);
		m_saCommand.Param(10).setAsString()	= CString(kh->frågeformulär.fraga3);
		m_saCommand.Param(11).setAsString()	= CString(kh->frågeformulär.fraga4);
		m_saCommand.Param(12).setAsString()	= CString(kh->frågeformulär.fraga5);
		m_saCommand.Param(13).setAsString()	= CString(kh->frågeformulär.fraga6);
		m_saCommand.Param(14).setAsString()	= CString(kh->frågeformulär.fraga7);
		m_saCommand.Param(15).setAsString()	= CString(kh->frågeformulär.fraga8);
		m_saCommand.Param(16).setAsString()	= CString(kh->frågeformulär.fraga9);
		m_saCommand.Param(17).setAsString()	= CString(kh->frågeformulär.fraga10);
		m_saCommand.Param(18).setAsString()	= CString(kh->frågeformulär.fraga11);
		m_saCommand.Param(19).setAsString()	= CString(kh->frågeformulär.fraga12);
		m_saCommand.Param(20).setAsString()	= CString(kh->frågeformulär.fraga13);
		m_saCommand.Param(21).setAsString()	= CString(kh->frågeformulär.fraga14);
		m_saCommand.Param(22).setAsString()	= CString(kh->frågeformulär.fraga15);
		m_saCommand.Param(23).setAsString()	= CString(kh->frågeformulär.fraga16);
		m_saCommand.Param(24).setAsString()	= CString(kh->frågeformulär.fraga17);
		m_saCommand.Param(25).setAsString()	= CString(kh->frågeformulär.fraga18);
		m_saCommand.Param(26).setAsString()	= CString(kh->frågeformulär.fraga19);
		m_saCommand.Param(27).setAsString()	= CString(kh->frågeformulär.fraga20);
		m_saCommand.Param(28).setAsString()	= CString(kh->frågeformulär.fraga21);
		m_saCommand.Param(29).setAsString()	= CString(kh->frågeformulär.ovrigt);
		m_saCommand.Param(30).setAsLong()		= kh->data.invtyp;

		m_saCommand.Param(31).setAsLong()	= kh->data.regionid;
		m_saCommand.Param(32).setAsLong()	= kh->data.distriktid;
		m_saCommand.Param(33).setAsLong()	= kh->data.traktnr;
		m_saCommand.Param(34).setAsLong()	= kh->data.standort;
		m_saCommand.Param(35).setAsLong()	= kh->data.entreprenor;
		m_saCommand.Param(36).setAsLong()	= kh->data.rapport_typ;
		m_saCommand.Param(37).setAsLong()	= kh->data.årtal;
		m_saCommand.Param(38).setAsLong()	= kh->data.invtyp;
		m_saCommand.Execute();

		//Update KORUS Ytor
		for(int i=0;i<kh->ytor->Count;i++)
		{
			Egenuppfoljning::KorusDataEventArgs::Yta^ yta=safe_cast<Egenuppfoljning::KorusDataEventArgs::Yta^>(kh->ytor[i]);

			m_saCommand.setCommandText( _T("UPDATE korus_ytor SET yta = :1, regionid = :2, distriktid = :3, traktnr = :4, standort = :5, entreprenor = :6, rapport_typ = :7, artal = :8, ")
				_T("optimalt = :9, bra = :10, varavbattre = :11, blekjordsflack = :12, optimaltbra = :13, ovrigt = :14, planterade = :15, planteringsdjupfel = :16, ")
				_T("tilltryckning = :17, stickvbredd = :18, tall = :19, tallmedelhojd = :20, gran = :21, granmedelhojd = :22, lov = :23, lovmedelhojd = :24, contorta = :25, contortamedelhojd = :26, ")
				_T("hst = :27, mhojd = :28, summa = :29, skador = :30, invtyp = :31, rojstam = :32  ")
				_T("WHERE yta = :33 AND regionid = :34 AND distriktid = :35 AND traktnr = :36 AND standort = :37 AND entreprenor = :38 AND rapport_typ = :39 AND artal = :40 AND invtyp = :41 ")
				_T("IF @@ROWCOUNT=0 ")
				_T("INSERT INTO korus_ytor (yta, regionid, distriktid, traktnr, standort, entreprenor, rapport_typ, artal, optimalt, bra, varavbattre, blekjordsflack, optimaltbra, ovrigt, planterade, planteringsdjupfel, tilltryckning, stickvbredd, tall, tallmedelhojd, gran, granmedelhojd, lov, lovmedelhojd, contorta, contortamedelhojd, hst, mhojd, summa, skador, invtyp, rojstam) ")
				_T("VALUES(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15, :16, :17, :18, :19, :20, :21, :22, :23, :24, :25, :26, :27, :28, :29, :30, :31, :32)") );

			m_saCommand.Param(1).setAsLong()		= yta->yta;			
			m_saCommand.Param(2).setAsLong()		= kh->data.regionid;
			m_saCommand.Param(3).setAsLong()		= kh->data.distriktid;
			m_saCommand.Param(4).setAsLong()		= kh->data.traktnr;
			m_saCommand.Param(5).setAsLong()		= kh->data.standort;
			m_saCommand.Param(6).setAsLong()		= kh->data.entreprenor;
			m_saCommand.Param(7).setAsLong()		= kh->data.rapport_typ;
			m_saCommand.Param(8).setAsLong()		= kh->data.årtal;
			m_saCommand.Param(9).setAsLong()		= yta->optimalt;
			m_saCommand.Param(10).setAsLong()		= yta->bra;
			m_saCommand.Param(11).setAsLong()		= yta->varavbattre;
			m_saCommand.Param(12).setAsLong()		= yta->blekjordsflack;
			m_saCommand.Param(13).setAsLong()		= yta->optimaltbra;
			m_saCommand.Param(14).setAsLong()		= yta->ovrigt;
			m_saCommand.Param(15).setAsLong()		= yta->planterade;
			m_saCommand.Param(16).setAsLong()		= yta->planteringsdjupfel;
			m_saCommand.Param(17).setAsLong()		= yta->tilltryckning;
			m_saCommand.Param(18).setAsDouble()	= yta->stickvbredd;
			m_saCommand.Param(19).setAsLong()		= yta->tall;
			m_saCommand.Param(20).setAsDouble()	= yta->tallmedelhojd;
			m_saCommand.Param(21).setAsLong()		= yta->gran;
			m_saCommand.Param(22).setAsDouble()	= yta->granmedelhojd;
			m_saCommand.Param(23).setAsLong()		= yta->lov;
			m_saCommand.Param(24).setAsDouble()	= yta->lovmedelhojd;
			m_saCommand.Param(25).setAsLong()		= yta->contorta;
			m_saCommand.Param(26).setAsDouble()	= yta->contortamedelhojd;
			m_saCommand.Param(27).setAsLong()		= yta->hst;
			m_saCommand.Param(28).setAsDouble()	= yta->mhojd;
			m_saCommand.Param(29).setAsLong()		= yta->summa;
			m_saCommand.Param(30).setAsLong()		= yta->skador;
			m_saCommand.Param(31).setAsLong()		= kh->data.invtyp;
			m_saCommand.Param(32).setAsLong()		= yta->rojstam;

			m_saCommand.Param(33).setAsLong()	= yta->yta;
			m_saCommand.Param(34).setAsLong()	= kh->data.regionid;
			m_saCommand.Param(35).setAsLong()	= kh->data.distriktid;
			m_saCommand.Param(36).setAsLong()	= kh->data.traktnr;
			m_saCommand.Param(37).setAsLong()	= kh->data.standort;
			m_saCommand.Param(38).setAsLong()	= kh->data.entreprenor;
			m_saCommand.Param(39).setAsLong()	= kh->data.rapport_typ;
			m_saCommand.Param(40).setAsLong()	= kh->data.årtal;
			m_saCommand.Param(41).setAsLong()	= kh->data.invtyp;
			m_saCommand.Execute();
		}

		getDBConnection().conn->Commit();
	}
	catch( SAException &e )
	{
		// Undo any changes to db
		try
		{
			getDBConnection().conn->Rollback();
		}
		catch(...)
		{
		}

		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}

System::String^ CDBFunc::GetKORUSEntreprenör(Egenuppfoljning::KorusDataEventArgs^ &kh)
{
	System::String^ entreprenörNamn="";
	try
	{
		m_saCommand.setCommandText( _T("SELECT * FROM korus_entreprenorer WHERE entreprenorid = :1 AND regionid = :2") );
		m_saCommand.Param(1).setAsLong()	= kh->data.entreprenor;
		m_saCommand.Param(2).setAsLong()	= kh->data.regionid;
		m_saCommand.Execute();
		while( m_saCommand.FetchNext() )
		{
			entreprenörNamn=gcnew String(m_saCommand.Field("entreprenornamn").asString());
		}
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
	}

	return entreprenörNamn;
}

bool CDBFunc::AddKORUSEntreprenör(Egenuppfoljning::KorusDataEventArgs^ &kh)
{
	// Turn off autocommit in case an error occur
	getDBConnection().conn->setAutoCommit(SA_AutoCommitOff);

	try
	{
		// Make sure this file hasn't been imported already
		m_saCommand.setCommandText( _T("SELECT 1 FROM korus_entreprenorer WHERE entreprenorid = :1 AND regionid = :2") );
		m_saCommand.Param(1).setAsLong()	= kh->data.entreprenor;
		m_saCommand.Param(2).setAsLong()	= kh->data.regionid;
		m_saCommand.Execute();

		if( m_saCommand.FetchNext() )
		{
			// Make sure we reach end-of-record to avoid trigging SQLAPI bug: see Redmine Wiki
			while( m_saCommand.FetchNext() ) {}

			// Record found, then just return
			return false;
		}

		m_saCommand.setCommandText( _T("SET IDENTITY_INSERT korus_entreprenorer ON INSERT INTO korus_entreprenorer (entreprenorid, regionid, entreprenornamn) ")
			_T("VALUES(:1, :2, :3)") );
		m_saCommand.Param(1).setAsLong()		= kh->data.entreprenor;
		m_saCommand.Param(2).setAsLong()		= kh->data.regionid;
		m_saCommand.Param(3).setAsString()	= CString(kh->data.entreprenornamn);
		m_saCommand.Execute();


		// Everything succeeded - commit changes to db
		getDBConnection().conn->Commit();
	}
	catch( SAException &e )
	{
		// Undo any changes to db
		try
		{
			getDBConnection().conn->Rollback();
		}
		catch(...)
		{
		}

		AfxMessageBox( e.ErrText() );
		return false;
	}

	// Turn autocommit back on
	getDBConnection().conn->setAutoCommit(SA_AutoCommitOn);

	return true;	

}

bool CDBFunc::AddKORUSDataRecord(Egenuppfoljning::KorusDataEventArgs^ &kh)
{
	CString date;
	int fragorid = 0;
	int ytorid = 0;

	//FormatDate(th.datum, date);

	// Turn off autocommit in case an error occur
	getDBConnection().conn->setAutoCommit(SA_AutoCommitOff);

	try
	{
		// Make sure this file hasn't been imported already
		m_saCommand.setCommandText( _T("SELECT 1 FROM korus_data WHERE regionid = :1 AND distriktid = :2 AND traktnr = :3 AND standort = :4 AND entreprenor = :5 AND rapport_typ = :6 AND artal = :7 AND invtyp = :8") );
		m_saCommand.Param(1).setAsLong()	= kh->data.regionid;
		m_saCommand.Param(2).setAsLong()	= kh->data.distriktid;
		m_saCommand.Param(3).setAsLong()	= kh->data.traktnr;
		m_saCommand.Param(4).setAsLong()	= kh->data.standort;
		m_saCommand.Param(5).setAsLong()	= kh->data.entreprenor;
		m_saCommand.Param(6).setAsLong()	= kh->data.rapport_typ;
		m_saCommand.Param(7).setAsLong()	= kh->data.årtal;
		m_saCommand.Param(8).setAsLong()	= kh->data.invtyp;
		m_saCommand.Execute();

		if( m_saCommand.FetchNext() )
		{
			// Make sure we reach end-of-record to avoid trigging SQLAPI bug: see Redmine Wiki
			while( m_saCommand.FetchNext() ) {}

			// Record found
			CString str;
			str.Format( _T("KORUS rapporten med nyckel ids: \nInvtyp/Region/distrikt/traktnr/traktdel/entreprenör/årtal: %d/%d/%d/%d/%d/%d/%d finns redan lagrad. Vill du skriva över det befintliga datat för denna rapport?"),
				kh->data.invtyp,kh->data.regionid, kh->data.distriktid, kh->data.traktnr, kh->data.standort, kh->data.entreprenor, kh->data.årtal );

			if(AfxMessageBox(str, MB_YESNO | MB_ICONQUESTION)==IDYES)
			{
				return UpdateKORUSData(kh);
			}
			return false;
		}

		// Add KORUS Data record
		m_saCommand.setCommandText( _T("INSERT INTO korus_data (regionid, distriktid, traktnr, standort, entreprenor, rapport_typ, artal, traktnamn, regionnamn, distriktnamn, entreprenornamn, ursprung, datum, markberedningsmetod, datainsamlingsmetod, areal, malgrundyta, stickvagssystem, stickvagsavstand, gisstracka, gisareal, provytestorlek, kommentar, antalvaltor, avstand, bedomdvolym, hygge, vagkant, aker, latbilshugg, grotbil, traktordragenhugg, skotarburenhugg, avlagg1norr, avlagg1ost, avlagg2norr, avlagg2ost, avlagg3norr, avlagg3ost,	avlagg4norr, avlagg4ost, avlagg5norr, avlagg5ost, avlagg6norr, avlagg6ost, invtyp, atgareal) ")
			_T("VALUES(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15, :16, :17, :18, :19, :20, :21, :22, :23, :24, :25, :26, :27, :28, :29, :30, :31, :32, :33, :34, :35, :36, :37, :38, :39, :40, :41, :42, :43, :44, :45, :46, :47)") );
		m_saCommand.Param(1).setAsLong()		= kh->data.regionid;
		m_saCommand.Param(2).setAsLong()		= kh->data.distriktid;
		m_saCommand.Param(3).setAsLong()		= kh->data.traktnr;
		m_saCommand.Param(4).setAsLong()		= kh->data.standort;
		m_saCommand.Param(5).setAsLong()		= kh->data.entreprenor;
		m_saCommand.Param(6).setAsLong()		= kh->data.rapport_typ;
		m_saCommand.Param(7).setAsLong()		= kh->data.årtal;
		m_saCommand.Param(8).setAsString()	= CString(kh->data.traktnamn);
		m_saCommand.Param(9).setAsString()	= CString(kh->data.regionnamn);
		m_saCommand.Param(10).setAsString()	= CString(kh->data.distriktnamn);
		m_saCommand.Param(11).setAsString()	= CString(kh->data.entreprenornamn);
		m_saCommand.Param(12).setAsString()	= CString(kh->data.ursprung);
		m_saCommand.Param(13).setAsString()	= CString(kh->data.datum);
		m_saCommand.Param(14).setAsString()	= CString(kh->data.markberedningsmetod);
		m_saCommand.Param(15).setAsString()	= CString(kh->data.datainsamlingsmetod);
		m_saCommand.Param(16).setAsDouble()		= kh->data.areal;
		m_saCommand.Param(17).setAsLong()		= kh->data.malgrundyta;
		m_saCommand.Param(18).setAsString()	= CString(kh->data.stickvagssystem);
		m_saCommand.Param(19).setAsLong()		= kh->data.stickvagsavstand;
		m_saCommand.Param(20).setAsLong()		= kh->data.gisstracka;
		m_saCommand.Param(21).setAsDouble()	= kh->data.gisareal;
		m_saCommand.Param(22).setAsLong()		= kh->data.provytestorlek;
		m_saCommand.Param(23).setAsString()	= CString(kh->data.kommentar);
		m_saCommand.Param(24).setAsLong()		= kh->data.antalvaltor;
		m_saCommand.Param(25).setAsLong()		= kh->data.avstand;
		m_saCommand.Param(26).setAsLong()		= kh->data.bedomdvolym;
		m_saCommand.Param(27).setAsLong()		= kh->data.hygge;
		m_saCommand.Param(28).setAsLong()		= kh->data.vagkant;
		m_saCommand.Param(29).setAsLong()		= kh->data.aker;
		m_saCommand.Param(30).setAsBool()		= kh->data.latbilshugg;
		m_saCommand.Param(31).setAsBool()		= kh->data.grotbil;
		m_saCommand.Param(32).setAsBool()		= kh->data.traktordragenhugg;
		m_saCommand.Param(33).setAsBool()		= kh->data.skotarburenhugg;
		m_saCommand.Param(34).setAsLong()		= kh->data.avlagg1norr;
		m_saCommand.Param(35).setAsLong()		= kh->data.avlagg1ost;
		m_saCommand.Param(36).setAsLong()		= kh->data.avlagg2norr;
		m_saCommand.Param(37).setAsLong()		= kh->data.avlagg2ost;
		m_saCommand.Param(38).setAsLong()		= kh->data.avlagg3norr;
		m_saCommand.Param(39).setAsLong()		= kh->data.avlagg3ost;
		m_saCommand.Param(40).setAsLong()		= kh->data.avlagg4norr;
		m_saCommand.Param(41).setAsLong()		= kh->data.avlagg4ost;
		m_saCommand.Param(42).setAsLong()		= kh->data.avlagg5norr;
		m_saCommand.Param(43).setAsLong()		= kh->data.avlagg5ost;
		m_saCommand.Param(44).setAsLong()		= kh->data.avlagg6norr;
		m_saCommand.Param(45).setAsLong()		= kh->data.avlagg6ost;
		m_saCommand.Param(46).setAsLong()		= kh->data.invtyp;
		m_saCommand.Param(47).setAsDouble()		= kh->data.atgareal;
		m_saCommand.Execute();

		// Add KORUS Frågeformulär data
		m_saCommand.setCommandText( _T("INSERT INTO korus_fragor (regionid, distriktid, traktnr, standort, entreprenor, rapport_typ, artal, fraga1, fraga2, fraga3, fraga4, fraga5, fraga6, fraga7, fraga8, fraga9, fraga10, fraga11, fraga12, fraga13, fraga14, fraga15, fraga16, fraga17, fraga18, fraga19, fraga20, fraga21, ovrigt, invtyp) ")
			_T("VALUES(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15, :16, :17, :18, :19, :20, :21, :22, :23, :24, :25, :26, :27, :28, :29, :30)") );
		m_saCommand.Param(1).setAsLong()		= kh->data.regionid;
		m_saCommand.Param(2).setAsLong()		= kh->data.distriktid;
		m_saCommand.Param(3).setAsLong()		= kh->data.traktnr;
		m_saCommand.Param(4).setAsLong()		= kh->data.standort;
		m_saCommand.Param(5).setAsLong()		= kh->data.entreprenor;
		m_saCommand.Param(6).setAsLong()		= kh->data.rapport_typ;
		m_saCommand.Param(7).setAsLong()		= kh->data.årtal;
		m_saCommand.Param(8).setAsString()	= CString(kh->frågeformulär.fraga1);
		m_saCommand.Param(9).setAsString()	= CString(kh->frågeformulär.fraga2);
		m_saCommand.Param(10).setAsString()	= CString(kh->frågeformulär.fraga3);
		m_saCommand.Param(11).setAsString()	= CString(kh->frågeformulär.fraga4);
		m_saCommand.Param(12).setAsString()	= CString(kh->frågeformulär.fraga5);
		m_saCommand.Param(13).setAsString()	= CString(kh->frågeformulär.fraga6);
		m_saCommand.Param(14).setAsString()	= CString(kh->frågeformulär.fraga7);
		m_saCommand.Param(15).setAsString()	= CString(kh->frågeformulär.fraga8);
		m_saCommand.Param(16).setAsString()	= CString(kh->frågeformulär.fraga9);
		m_saCommand.Param(17).setAsString()	= CString(kh->frågeformulär.fraga10);
		m_saCommand.Param(18).setAsString()	= CString(kh->frågeformulär.fraga11);
		m_saCommand.Param(19).setAsString()	= CString(kh->frågeformulär.fraga12);
		m_saCommand.Param(20).setAsString()	= CString(kh->frågeformulär.fraga13);
		m_saCommand.Param(21).setAsString()	= CString(kh->frågeformulär.fraga14);
		m_saCommand.Param(22).setAsString()	= CString(kh->frågeformulär.fraga15);
		m_saCommand.Param(23).setAsString()	= CString(kh->frågeformulär.fraga16);
		m_saCommand.Param(24).setAsString()	= CString(kh->frågeformulär.fraga17);
		m_saCommand.Param(25).setAsString()	= CString(kh->frågeformulär.fraga18);
		m_saCommand.Param(26).setAsString()	= CString(kh->frågeformulär.fraga19);
		m_saCommand.Param(27).setAsString()	= CString(kh->frågeformulär.fraga20);
		m_saCommand.Param(28).setAsString()	= CString(kh->frågeformulär.fraga21);
		m_saCommand.Param(29).setAsString()	= CString(kh->frågeformulär.ovrigt);
		m_saCommand.Param(30).setAsLong()		= kh->data.invtyp;
		m_saCommand.Execute();

		for(int i=0;i<kh->ytor->Count;i++)
		{
			Egenuppfoljning::KorusDataEventArgs::Yta^ yta=safe_cast<Egenuppfoljning::KorusDataEventArgs::Yta^>(kh->ytor[i]);
	
			m_saCommand.setCommandText( _T("INSERT INTO korus_ytor (yta, regionid, distriktid, traktnr, standort, entreprenor, rapport_typ, artal, optimalt, bra, varavbattre, blekjordsflack, optimaltbra, ovrigt, planterade, planteringsdjupfel, tilltryckning, stickvbredd, tall, tallmedelhojd, gran, granmedelhojd, lov, lovmedelhojd, contorta, contortamedelhojd, hst, mhojd, summa, skador, invtyp, rojstam) ")
				_T("VALUES(:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12, :13, :14, :15, :16, :17, :18, :19, :20, :21, :22, :23, :24, :25, :26, :27, :28, :29, :30, :31, :32)") );
			m_saCommand.Param(1).setAsLong()		= yta->yta;			
			m_saCommand.Param(2).setAsLong()		= kh->data.regionid;
			m_saCommand.Param(3).setAsLong()		= kh->data.distriktid;
			m_saCommand.Param(4).setAsLong()		= kh->data.traktnr;
			m_saCommand.Param(5).setAsLong()		= kh->data.standort;
			m_saCommand.Param(6).setAsLong()		= kh->data.entreprenor;
			m_saCommand.Param(7).setAsLong()		= kh->data.rapport_typ;
			m_saCommand.Param(8).setAsLong()		= kh->data.årtal;
			m_saCommand.Param(9).setAsLong()		= yta->optimalt;
			m_saCommand.Param(10).setAsLong()		= yta->bra;
			m_saCommand.Param(11).setAsLong()		= yta->varavbattre;
			m_saCommand.Param(12).setAsLong()		= yta->blekjordsflack;
			m_saCommand.Param(13).setAsLong()		= yta->optimaltbra;
			m_saCommand.Param(14).setAsLong()		= yta->ovrigt;
			m_saCommand.Param(15).setAsLong()		= yta->planterade;
			m_saCommand.Param(16).setAsLong()		= yta->planteringsdjupfel;
			m_saCommand.Param(17).setAsLong()		= yta->tilltryckning;
			m_saCommand.Param(18).setAsDouble()	= yta->stickvbredd;
			m_saCommand.Param(19).setAsLong()		= yta->tall;
			m_saCommand.Param(20).setAsDouble()	= yta->tallmedelhojd;
			m_saCommand.Param(21).setAsLong()		= yta->gran;
			m_saCommand.Param(22).setAsDouble()	= yta->granmedelhojd;
			m_saCommand.Param(23).setAsLong()		= yta->lov;
			m_saCommand.Param(24).setAsDouble()	= yta->lovmedelhojd;
			m_saCommand.Param(25).setAsLong()		= yta->contorta;
			m_saCommand.Param(26).setAsDouble()	= yta->contortamedelhojd;
			m_saCommand.Param(27).setAsLong()		= yta->hst;
			m_saCommand.Param(28).setAsDouble()	= yta->mhojd;
			m_saCommand.Param(29).setAsLong()		= yta->summa;
			m_saCommand.Param(30).setAsLong()		= yta->skador;
			m_saCommand.Param(31).setAsLong()		= kh->data.invtyp;
			m_saCommand.Param(32).setAsLong()		= yta->rojstam;
			m_saCommand.Execute();
		}

		// Everything succeeded - commit changes to db
		getDBConnection().conn->Commit();
	}
	catch( SAException &e )
	{
		// Undo any changes to db
		try
		{
			getDBConnection().conn->Rollback();
		}
		catch(...)
		{
		}

		AfxMessageBox( e.ErrText() );
		return false;
	}

	// Turn autocommit back on
	getDBConnection().conn->setAutoCommit(SA_AutoCommitOn);

	return true;
}

bool CDBFunc::ExportRecords(CString dbFile)
{
	return true;
}


bool CDBFunc::DropAllTables()
{
	SAString str;

	try
	{
		// Entreprenörer
		str.Format(_T("IF EXISTS(SELECT 1 FROM sysobjects WHERE name LIKE N'korus_entreprenorer' AND xtype='U') %s"), _T("DROP TABLE [korus_entreprenorer]"));
		m_saCommand.setCommandText(str);
		m_saCommand.Execute();

		// Yta
		str.Format(_T("IF EXISTS(SELECT 1 FROM sysobjects WHERE name LIKE N'korus_ytor' AND xtype='U') %s"), _T("DROP TABLE [korus_ytor]"));
		m_saCommand.setCommandText(str);
		m_saCommand.Execute();

		// Frågor
		str.Format(_T("IF EXISTS(SELECT 1 FROM sysobjects WHERE name LIKE N'korus_fragor' AND xtype='U') %s"), _T("DROP TABLE [korus_fragor]"));
		m_saCommand.setCommandText(str);
		m_saCommand.Execute();

		// KORUS
		str.Format(_T("IF EXISTS(SELECT 1 FROM sysobjects WHERE name LIKE N'korus_data' AND xtype='U') %s"), _T("DROP TABLE [korus_data]"));
		m_saCommand.setCommandText(str);
		m_saCommand.Execute();
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}


bool CDBFunc::MakeSureTablesExist()
{
	SAString str;

	try
	{
		// KORUS
		str.Format(_T("IF NOT EXISTS(SELECT 1 FROM sysobjects WHERE name LIKE N'korus_data' AND xtype='U') %s"), SQLCREATE_KORUS_DATA);
		m_saCommand.setCommandText(str);
		m_saCommand.Execute();

		// Yta
		str.Format(_T("IF NOT EXISTS(SELECT 1 FROM sysobjects WHERE name LIKE N'korus_ytor' AND xtype='U') %s"), SQLCREATE_KORUS_YTOR);
		m_saCommand.setCommandText(str);
		m_saCommand.Execute();

		// Frågor
		str.Format(_T("IF NOT EXISTS(SELECT 1 FROM sysobjects WHERE name LIKE N'korus_fragor' AND xtype='U') %s"), SQLCREATE_KORUS_FRAGOR);
		m_saCommand.setCommandText(str);
		m_saCommand.Execute();

		// Entreprenörer
		str.Format(_T("IF NOT EXISTS(SELECT 1 FROM sysobjects WHERE name LIKE N'korus_entreprenorer' AND xtype='U') %s"), SQLCREATE_KORUS_ENTREPRENORER);
		m_saCommand.setCommandText(str);
		m_saCommand.Execute();

		// Foreign key constraints
		// Yta - KORUS
		str.Format(_T("IF NOT EXISTS(SELECT 1 FROM Information_Schema.Table_Constraints WHERE CONSTRAINT_NAME = 'FK_korus_ytor_korus_data') %s"), SQLCONSTRAINT_KORUS_DATA_YTOR);
		m_saCommand.setCommandText(str);
		m_saCommand.Execute();

		// Frågor - KORUS
		str.Format(_T("IF NOT EXISTS(SELECT 1 FROM Information_Schema.Table_Constraints WHERE CONSTRAINT_NAME = 'FK_korus_fragor_korus_data') %s"), SQLCONSTRAINT_KORUS_DATA_FRAGOR);
		m_saCommand.setCommandText(str);
		m_saCommand.Execute();
	}
	catch( SAException &e )
	{
		AfxMessageBox( e.ErrText() );
		return false;
	}

	return true;
}
