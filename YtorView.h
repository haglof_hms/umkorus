#pragma once

#pragma managed

#include <afxwin.h>
#include <afxwinforms.h>
#include <map>
#include "DBFunc.h"

#using <Egenuppfoljning.dll>

using namespace Egenuppfoljning;
using namespace std; 

class CYtorView : public CWinFormsView
{
public:
	void OnKORUSStatusRefresh(System::Object^ sender, Egenuppfoljning::KorusStatusEventArgs^ dataobjekt);
	void OnKORUSDataGåTillRapport(System::Object^ sender, Egenuppfoljning::KorusDataEventArgs^ dataobjekt);
	void OnKORUSDataTaBortYta( System::Object^ sender, Egenuppfoljning::KorusDataEventArgs^ kd );
	void OnKORUSLagraÄndradeYtor( System::Object^ sender, List<Egenuppfoljning::KorusDataEventArgs::Yta>^ ytor );

	BEGIN_DELEGATE_MAP( CYtorView )
		EVENT_DELEGATE_ENTRY( OnKORUSStatusRefresh, System::Object^, Egenuppfoljning::KorusStatusEventArgs^ )
		EVENT_DELEGATE_ENTRY( OnKORUSDataGåTillRapport, System::Object^, Egenuppfoljning::KorusDataEventArgs^ )
		EVENT_DELEGATE_ENTRY( OnKORUSDataTaBortYta, System::Object^, Egenuppfoljning::KorusDataEventArgs^ )
		EVENT_DELEGATE_ENTRY( OnKORUSLagraÄndradeYtor, System::Object^, List<Egenuppfoljning::KorusDataEventArgs::Yta>^ )
	END_DELEGATE_MAP()
	DECLARE_DYNCREATE(CYtorView)

protected:
	CYtorView();
	virtual ~CYtorView();
	CDBFunc *m_pDB;
	LRESULT OnSuiteMessage(WPARAM wParam, LPARAM lParam);
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	Egenuppfoljning::Controls::FormListaYtor^ GetControl();

	enum { IDD = IDD_LISTA_YTOR };

protected:
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg LRESULT OnMessageFromSuite(WPARAM wParam, LPARAM lParam);
	afx_msg int  OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	CString m_sLangFN;
public:
	DECLARE_MESSAGE_MAP()

};
