#include "stdafx.h"
#include "MessageDlg.h"
#include "UMKORUSStrings.h"
#include "UMKORUSGenerics.h"
#include "RapporterView.h"
#include "RapporterFrame.h"

using namespace std;

// CRapporterView

IMPLEMENT_DYNCREATE(CRapporterView, CWinFormsView)
CRapporterView::CRapporterView():
CWinFormsView(Egenuppfoljning::Controls::FormListaRapporter::typeid),
m_pDB(NULL)
{
}

CRapporterView::~CRapporterView()
{
	if( m_pDB )
	{
		delete m_pDB;
		m_pDB = NULL;
	}
}


BEGIN_MESSAGE_MAP(CRapporterView, CWinFormsView)
	ON_WM_CREATE()
	ON_WM_COPYDATA()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromSuite)
	ON_WM_SETFOCUS()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_WM_DESTROY()
END_MESSAGE_MAP()



Egenuppfoljning::Controls::FormListaRapporter^ CRapporterView::GetControl()
{
	System::Windows::Forms::Control^ control = CWinFormsView::GetControl();
	return safe_cast<Egenuppfoljning::Controls::FormListaRapporter^>(control);
}

int CRapporterView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if( CWinFormsView::OnCreate(lpCreateStruct) == -1 )
		return -1;

	// Request db connection
	CDBFunc::setupForDBConnection( AfxGetMainWnd()->GetSafeHwnd(), this->GetSafeHwnd() );

	// Set up language filename
	m_sLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, getLangSet(), LANGUAGE_FN_EXT);
	return 0;
}

BOOL CRapporterView::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CRapporterView::OnSize(UINT nType, int cx, int cy)
{
	CWinFormsView::OnSize(nType, cx, cy);
}

void CRapporterView::OnDestroy()
{
	GetControl()->Close();
}

void CRapporterView::OnInitialUpdate()
{
	CWinFormsView::OnInitialUpdate();

	// First of all, make sure required tables exist
	m_pDB->MakeSureTablesExist();
	GetControl()->HämtaFrågorFörValtFilter += MAKE_DELEGATE( Egenuppfoljning::DataEventHandler, OnKORUSDataHämtaFrågorFörValtFilter );
	GetControl()->HämtaYtorFörValtFilter += MAKE_DELEGATE( Egenuppfoljning::DataEventHandler, OnKORUSDataHämtaYtorFörValtFilter );

	GetControl()->StatusEventRefresh += MAKE_DELEGATE( Egenuppfoljning::StatusEventHandler, OnKORUSStatusRefresh );
	GetControl()->DataEventGåTillRapport += MAKE_DELEGATE( Egenuppfoljning::DataEventHandler, OnKORUSDataGåTillRapport );
	GetControl()->DataEventTaBortRapport += MAKE_DELEGATE( Egenuppfoljning::DataEventHandler, OnKORUSDataTaBortRapport );
	GetControl()->DataEventLagraÄndradeRapporter += MAKE_DELEGATE( Egenuppfoljning::RapportEventHandler, OnKORUSDataLagraÄndradeRapporter );

	//Maximera fönstret
	GetParentFrame()->ShowWindow( SW_SHOWMAXIMIZED );
	GetControl()->UppdateraRapportLista(m_pDB->GetRapporterLista());
}

void CRapporterView::OnSetFocus(CWnd* pOldWnd)
{
	CWinFormsView::OnSetFocus(pOldWnd);

	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, TRUE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, TRUE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, TRUE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_PREVIEW_ITEM, FALSE);

	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_LIST, FALSE);

	//Read the data from the database, into to the rapport vector, and show it in the GUI
	//GetControl()->UppdateraRapportLista(m_pDB->GetRapporterLista());
}

void CRapporterView::OnKORUSStatusRefresh( System::Object^ /*sender*/, Egenuppfoljning::KorusStatusEventArgs^ /*kd*/ )
{
	//Read the data from the database, into to the rapport vector, and show it in the GUI
	GetControl()->UppdateraRapportLista(m_pDB->GetRapporterLista());
	AfxMessageBox(_T("Datat för rapporter har nu blivit uppdaterat från databasen."), MB_OK | MB_ICONINFORMATION);
}

void CRapporterView::OnKORUSDataTaBortRapport( System::Object^ /*sender*/, Egenuppfoljning::KorusDataEventArgs^ kd )
{
	if(m_pDB->DeleteKORUSData(kd))
	{ //om ta bort rapport lyckas uppdateras i så fall även frågor och ytor
	  GetControl()->TaBortKORUSDataLyckades();
	}
}

void CRapporterView::OnKORUSDataLagraÄndradeRapporter( System::Object^ /*sender*/, List<Egenuppfoljning::KorusDataEventArgs::Data>^ rapporter )
{
	if(m_pDB->UpdateKORUSRapporter(rapporter))
	{
		AfxMessageBox(_T("De lagrade KORUS rapporterna har nu blivit uppdaterade."), MB_OK | MB_ICONINFORMATION);
	}
}

void CRapporterView::OnKORUSDataGåTillRapport( System::Object^ /*sender*/, Egenuppfoljning::KorusDataEventArgs^ kd )
{
	showFormView( IDD_HANTERA_RAPPORT, m_sLangFN, ID_UPDATE_ITEM, (LPARAM)&kd );
}

void CRapporterView::OnKORUSDataHämtaFrågorFörValtFilter( System::Object^ /*sender*/, Egenuppfoljning::KorusDataEventArgs^ kd )
{
  GetControl()->UppdateraFrågorLista(m_pDB->GetFragorLista(kd));
}

void CRapporterView::OnKORUSDataHämtaYtorFörValtFilter( System::Object^ /*sender*/, Egenuppfoljning::KorusDataEventArgs^ kd )
{
	GetControl()->UppdateraYtorLista(m_pDB->GetYtorLista(kd));
}

LRESULT CRapporterView::OnMessageFromSuite(WPARAM wParam, LPARAM /*lParam*/)
{
	switch(wParam)
	{
	case ID_OPEN_ITEM:
		GetControl()->GåTillRapportData();
		break;
	case ID_SAVE_ITEM:
		GetControl()->LagraÄndradeKORUSRapporter();
		break;
	case ID_DELETE_ITEM:
		GetControl()->TaBortKORUSRapport();
		break;
	default:
		break;
	}
	return 0;
}
LRESULT CRapporterView::OnSuiteMessage(WPARAM /*wParam*/, LPARAM /*lParam*/)
{
	return 0;
}


BOOL CRapporterView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if( pData->cbData == sizeof(DB_CONNECTION_DATA) )
	{
		DB_CONNECTION_DATA connData;

		// Check if we have already set up a connection
		if( !m_pDB )
		{
			memcpy( &connData, pData->lpData, sizeof(DB_CONNECTION_DATA) );
			m_pDB = new CDBFunc(connData);
		}
	}

	return CWinFormsView::OnCopyData(pWnd, pData);
}

