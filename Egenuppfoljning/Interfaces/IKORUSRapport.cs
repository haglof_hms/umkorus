﻿namespace Egenuppfoljning.Interfaces
{
  public enum Rapport
  {
    Obehandlad,
    Nekad,
    Godkänd
  };

  public enum RapportTyp
  {
    Markberedning,
    Plantering,
    Röjning,
    Gallring,
    Biobränsle,
    Slutavverkning
  }

  public enum RapportFormat
  {
    Excel2007Export,
    AdobePdf
  }

  internal interface IKorusRapport
  {
    bool RedigeraLagratData { get; }
    void ShowReport();
    void ExporteraExcel2007();
    void ExporteraAdobePDF();
    void SkrivUt();
    string GetDsStr(string aTable, string aColumn);
    string GetEgenuppföljningsTyp();
    int GetTotalaAntaletYtor();
    int GetProcentStatus();
    void LaddaForm(string aRapportSökväg);
    void Close();
    void ShowEditDialog(bool aRedigeraLagratData);

    bool WriteDataToReportXmlFile(string aFilePath, bool aShowDialog, bool aWriteSchema, string aReportStatus,
                                  string aReportStatusDatum);

    void SändEpost(string aEpostAdress, string aTitel, string aMeddelande, bool aAnvändInternEpost, bool aBifogaRapport);
    KorusDataEventArgs GetKORUSData();
    void UppdateraData(KorusDataEventArgs aData);
    string GetReportFileName();
  }
}