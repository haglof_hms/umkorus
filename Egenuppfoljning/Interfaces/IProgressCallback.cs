﻿namespace Egenuppfoljning.Interfaces
{
  internal interface IProgressCallback
  {
    void StängProgressDialog();
  }
}