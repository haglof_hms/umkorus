﻿namespace Egenuppfoljning.Dialogs
{
  partial class FormSendReport
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSendReport));
      this.buttonAvbryt = new System.Windows.Forms.Button();
      this.imageListSkickaKvittoRespons = new System.Windows.Forms.ImageList(this.components);
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.richTextBoxMeddelande = new System.Windows.Forms.RichTextBox();
      this.textBoxTitel = new System.Windows.Forms.TextBox();
      this.label3 = new System.Windows.Forms.Label();
      this.buttonSkicka = new System.Windows.Forms.Button();
      this.label4 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.labelBifogadRapport = new System.Windows.Forms.Label();
      this.labelMottagare = new System.Windows.Forms.Label();
      this.labelEpostSändsVia = new System.Windows.Forms.Label();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.groupBox1.SuspendLayout();
      this.groupBox2.SuspendLayout();
      this.SuspendLayout();
      // 
      // buttonAvbryt
      // 
      this.buttonAvbryt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.buttonAvbryt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.buttonAvbryt.ImageIndex = 1;
      this.buttonAvbryt.ImageList = this.imageListSkickaKvittoRespons;
      this.buttonAvbryt.Location = new System.Drawing.Point(275, 14);
      this.buttonAvbryt.Name = "buttonAvbryt";
      this.buttonAvbryt.Size = new System.Drawing.Size(162, 30);
      this.buttonAvbryt.TabIndex = 1;
      this.buttonAvbryt.Text = "Hoppa över";
      this.buttonAvbryt.UseVisualStyleBackColor = true;
      this.buttonAvbryt.Click += new System.EventHandler(this.buttonAvbryt_Click);
      // 
      // imageListSkickaKvittoRespons
      // 
      this.imageListSkickaKvittoRespons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListSkickaKvittoRespons.ImageStream")));
      this.imageListSkickaKvittoRespons.TransparentColor = System.Drawing.Color.Transparent;
      this.imageListSkickaKvittoRespons.Images.SetKeyName(0, "SkickaKvittoRespons.ico");
      this.imageListSkickaKvittoRespons.Images.SetKeyName(1, "Neka.ico");
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.Location = new System.Drawing.Point(15, 63);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(35, 13);
      this.label1.TabIndex = 4;
      this.label1.Text = "Titel:";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label2.Location = new System.Drawing.Point(15, 120);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(79, 13);
      this.label2.TabIndex = 6;
      this.label2.Text = "Meddelande:";
      // 
      // richTextBoxMeddelande
      // 
      this.richTextBoxMeddelande.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.richTextBoxMeddelande.Location = new System.Drawing.Point(19, 136);
      this.richTextBoxMeddelande.MaxLength = 256;
      this.richTextBoxMeddelande.Name = "richTextBoxMeddelande";
      this.richTextBoxMeddelande.Size = new System.Drawing.Size(418, 96);
      this.richTextBoxMeddelande.TabIndex = 7;
      this.richTextBoxMeddelande.Text = "";
      // 
      // textBoxTitel
      // 
      this.textBoxTitel.Location = new System.Drawing.Point(19, 81);
      this.textBoxTitel.MaxLength = 64;
      this.textBoxTitel.Name = "textBoxTitel";
      this.textBoxTitel.Size = new System.Drawing.Size(418, 21);
      this.textBoxTitel.TabIndex = 5;
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label3.Location = new System.Drawing.Point(15, 21);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(116, 13);
      this.label3.TabIndex = 0;
      this.label3.Text = "E-posten sänds via:";
      // 
      // buttonSkicka
      // 
      this.buttonSkicka.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.buttonSkicka.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.buttonSkicka.ImageIndex = 0;
      this.buttonSkicka.ImageList = this.imageListSkickaKvittoRespons;
      this.buttonSkicka.Location = new System.Drawing.Point(13, 14);
      this.buttonSkicka.Name = "buttonSkicka";
      this.buttonSkicka.Size = new System.Drawing.Size(162, 30);
      this.buttonSkicka.TabIndex = 0;
      this.buttonSkicka.Text = "Skicka";
      this.buttonSkicka.UseVisualStyleBackColor = true;
      this.buttonSkicka.Click += new System.EventHandler(this.buttonSkicka_Click);
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label4.Location = new System.Drawing.Point(15, 42);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(70, 13);
      this.label4.TabIndex = 2;
      this.label4.Text = "Mottagare:";
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label5.Location = new System.Drawing.Point(15, 241);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(98, 13);
      this.label5.TabIndex = 8;
      this.label5.Text = "Bifogad rapport:";
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.labelBifogadRapport);
      this.groupBox1.Controls.Add(this.labelMottagare);
      this.groupBox1.Controls.Add(this.labelEpostSändsVia);
      this.groupBox1.Controls.Add(this.richTextBoxMeddelande);
      this.groupBox1.Controls.Add(this.label5);
      this.groupBox1.Controls.Add(this.label1);
      this.groupBox1.Controls.Add(this.label4);
      this.groupBox1.Controls.Add(this.label2);
      this.groupBox1.Controls.Add(this.textBoxTitel);
      this.groupBox1.Controls.Add(this.label3);
      this.groupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.groupBox1.Location = new System.Drawing.Point(7, 2);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(450, 278);
      this.groupBox1.TabIndex = 0;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "SÄNDA E-POST";
      // 
      // labelBifogadRapport
      // 
      this.labelBifogadRapport.AutoSize = true;
      this.labelBifogadRapport.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelBifogadRapport.Location = new System.Drawing.Point(16, 257);
      this.labelBifogadRapport.Name = "labelBifogadRapport";
      this.labelBifogadRapport.Size = new System.Drawing.Size(14, 13);
      this.labelBifogadRapport.TabIndex = 9;
      this.labelBifogadRapport.Text = "_";
      // 
      // labelMottagare
      // 
      this.labelMottagare.AutoSize = true;
      this.labelMottagare.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelMottagare.Location = new System.Drawing.Point(86, 42);
      this.labelMottagare.Name = "labelMottagare";
      this.labelMottagare.Size = new System.Drawing.Size(14, 13);
      this.labelMottagare.TabIndex = 3;
      this.labelMottagare.Text = "_";
      // 
      // labelEpostSändsVia
      // 
      this.labelEpostSändsVia.AutoSize = true;
      this.labelEpostSändsVia.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelEpostSändsVia.Location = new System.Drawing.Point(132, 21);
      this.labelEpostSändsVia.Name = "labelEpostSändsVia";
      this.labelEpostSändsVia.Size = new System.Drawing.Size(14, 13);
      this.labelEpostSändsVia.TabIndex = 1;
      this.labelEpostSändsVia.Text = "_";
      // 
      // groupBox2
      // 
      this.groupBox2.Controls.Add(this.buttonSkicka);
      this.groupBox2.Controls.Add(this.buttonAvbryt);
      this.groupBox2.Location = new System.Drawing.Point(7, 277);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(450, 53);
      this.groupBox2.TabIndex = 1;
      this.groupBox2.TabStop = false;
      // 
      // FormSendReport
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(464, 336);
      this.Controls.Add(this.groupBox2);
      this.Controls.Add(this.groupBox1);
      this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.MinimumSize = new System.Drawing.Size(8, 255);
      this.Name = "FormSendReport";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Skicka Kvitto/Respons";
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.groupBox2.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Button buttonAvbryt;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.RichTextBox richTextBoxMeddelande;
    private System.Windows.Forms.TextBox textBoxTitel;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Button buttonSkicka;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.Label labelBifogadRapport;
    private System.Windows.Forms.Label labelMottagare;
    private System.Windows.Forms.Label labelEpostSändsVia;
    private System.Windows.Forms.ImageList imageListSkickaKvittoRespons;
    private System.Windows.Forms.GroupBox groupBox2;
  }
}