﻿#region

using System;
using System.Globalization;
using System.Windows.Forms;
using Egenuppfoljning.Properties;

#endregion

namespace Egenuppfoljning.Dialogs
{
  public partial class FormSparaEntreprenör : Form
  {
    public FormSparaEntreprenör()
    {
      InitializeComponent();
      Settings.Default.EntreprenorNamn = String.Empty;
    }

    private void buttonSpara_Click(object sender, EventArgs e)
    {
      if (textBoxEntpreprenörNamn.Text.Trim().Equals(string.Empty))
      {
        MessageBox.Show(Resources.Ett_namn_maste_forst, Resources.Info,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
      }
      else
      {
        Settings.Default.EntreprenorNamn = textBoxEntpreprenörNamn.Text;
        DialogResult = DialogResult.OK;
        Close();
      }
    }

    private void buttonAvbryt_Click(object sender, EventArgs e)
    {
      Settings.Default.EntreprenorNamn = String.Empty;
      DialogResult = DialogResult.Cancel;
      Close();
    }

    public void InitData(int aEntreprenörNr, int aRegionId)
    {
      labelEntreprenörNr.Text = string.Format("{0:0000}", aEntreprenörNr);
      labelRegion.Text = aRegionId.ToString(CultureInfo.InvariantCulture);
    }
  }
}