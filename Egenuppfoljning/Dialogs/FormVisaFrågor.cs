﻿#region

using System;
using System.Text;
using System.Windows.Forms;
using Egenuppfoljning.Interfaces;
using Egenuppfoljning.Properties;

#endregion

namespace Egenuppfoljning.Dialogs
{
  public partial class FormVisaFrågor : Form
  {
    public FormVisaFrågor()
    {
      InitializeComponent();
    }

    private string HämtaFrågorBeroendePåRapportTyp()
    {
      var sb = new StringBuilder();

      switch (Settings.Default.ValdTypFragor)
      {
        case (int) RapportTyp.Gallring:
          groupBoxFrågor.Text = Resources.Fragor_for + RapportTyp.Gallring;
          sb.AppendLine("Fråga1: " + Resources.GallringFraga1);
          sb.AppendLine();
          sb.AppendLine("Fråga2: " + Resources.GallringFraga2);
          sb.AppendLine();
          sb.AppendLine("Fråga3: " + Resources.GallringFraga3);
          sb.AppendLine();
          sb.AppendLine("Fråga4: " + Resources.GallringFraga4);
          sb.AppendLine();
          sb.AppendLine("Fråga5: " + Resources.GallringFraga5);
          sb.AppendLine();
          sb.AppendLine("Fråga6: " + Resources.GallringFraga6);
          sb.AppendLine();
          sb.AppendLine("Fråga7: " + Resources.GallringFraga7);
          sb.AppendLine();
          sb.AppendLine("Fråga8: " + Resources.GallringFraga8);
          sb.AppendLine();
          sb.AppendLine("Fråga9: " + Resources.GallringFraga9);
          break;
        case (int) RapportTyp.Markberedning:
          groupBoxFrågor.Text = Resources.Fragor_for + RapportTyp.Markberedning;
          sb.AppendLine("Fråga1: " + Resources.MarkberedningFraga1);
          sb.AppendLine();
          sb.AppendLine("Fråga2: " + Resources.MarkberedningFraga2);
          sb.AppendLine();
          sb.AppendLine("Fråga3: " + Resources.MarkberedningFraga3);
          sb.AppendLine();
          sb.AppendLine("Fråga4: " + Resources.MarkberedningFraga4);
          sb.AppendLine();
          sb.AppendLine("Fråga5: " + Resources.MarkberedningFraga5);
          sb.AppendLine();
          sb.AppendLine("Fråga6: " + Resources.MarkberedningFraga6);
          sb.AppendLine();
          sb.AppendLine("Fråga7: " + Resources.MarkberedningFraga7);
          break;
        case (int) RapportTyp.Plantering:
          groupBoxFrågor.Text = Resources.Fragor_for + RapportTyp.Plantering;
          sb.AppendLine("Fråga1: " + Resources.PlanteringFraga1);
          sb.AppendLine();
          sb.AppendLine("Fråga2: " + Resources.PlanteringFraga2);
          break;
        case (int) RapportTyp.Röjning:
          groupBoxFrågor.Text = Resources.Fragor_for + RapportTyp.Röjning;
          sb.AppendLine("Fråga1: " + Resources.RojningFraga1);
          sb.AppendLine();
          sb.AppendLine("Fråga2: " + Resources.RojningFraga2);
          sb.AppendLine();
          sb.AppendLine("Fråga3: " + Resources.RojningFraga3);
          sb.AppendLine();
          sb.AppendLine("Fråga4: " + Resources.RojningFraga4);
          sb.AppendLine();
          sb.AppendLine("Fråga5: " + Resources.RojningFraga5);
          sb.AppendLine();
          sb.AppendLine("Fråga6: " + Resources.RojningFraga6);
          break;
        case (int) RapportTyp.Slutavverkning:
          groupBoxFrågor.Text = Resources.Fragor_for + RapportTyp.Slutavverkning;
          sb.AppendLine("Fråga1: " + Resources.SlutavverkningFraga1a);
          sb.AppendLine();
          sb.AppendLine("Fråga2: " + Resources.SlutavverkningFraga1b);
          sb.AppendLine();
          sb.AppendLine("Fråga3: " + Resources.SlutavverkningFraga2a);
          sb.AppendLine();
          sb.AppendLine("Fråga4: " + Resources.SlutavverkningFraga2b);
          sb.AppendLine();
          sb.AppendLine("Fråga5: " + Resources.SlutavverkningFraga2c);
          sb.AppendLine();
          sb.AppendLine("Fråga6: " + Resources.SlutavverkningFraga2d);
          sb.AppendLine();
          sb.AppendLine("Fråga7: " + Resources.SlutavverkningFraga2e);
          sb.AppendLine();
          sb.AppendLine("Fråga8: " + Resources.SlutavverkningFraga2f);
          sb.AppendLine();
          sb.AppendLine("Fråga9: " + Resources.SlutavverkningFraga2g);
          sb.AppendLine();
          sb.AppendLine("Fråga10: " + Resources.SlutavverkningFraga2h);
          sb.AppendLine();
          sb.AppendLine("Fråga11: " + Resources.SlutavverkningFraga3);
          sb.AppendLine();
          sb.AppendLine("Fråga12: " + Resources.SlutavverkningFraga4a);
          sb.AppendLine();
          sb.AppendLine("Fråga13: " + Resources.SlutavverkningFraga4b);
          sb.AppendLine();
          sb.AppendLine("Fråga14: " + Resources.SlutavverkningFraga5);
          sb.AppendLine();
          sb.AppendLine("Fråga15: " + Resources.SlutavverkningFraga6);
          sb.AppendLine();
          sb.AppendLine("Fråga16: " + Resources.SlutavverkningFraga7);
          sb.AppendLine();
          sb.AppendLine("Fråga17: " + Resources.SlutavverkningFraga8);
          sb.AppendLine();
          sb.AppendLine("Fråga18: " + Resources.SlutavverkningFraga9);
          sb.AppendLine();
          sb.AppendLine("Fråga19: " + Resources.SlutavverkningFraga10);
          sb.AppendLine();
          sb.AppendLine("Fråga20: " + Resources.SlutavverkningFraga11);
          sb.AppendLine();
          sb.AppendLine("Fråga21: " + Resources.SlutavverkningFraga12);
          break;
        case (int) RapportTyp.Biobränsle:
          groupBoxFrågor.Text = Resources.Fragor_for + RapportTyp.Biobränsle;
          sb.AppendLine("Fråga1: " + Resources.BiobransleFraga1);
          sb.AppendLine();
          sb.AppendLine("Fråga2: " + Resources.BiobransleFraga2);
          sb.AppendLine();
          sb.AppendLine("Fråga3: " + Resources.BiobransleFraga3);
          sb.AppendLine();
          sb.AppendLine("Fråga4: " + Resources.BiobransleFraga4);
          sb.AppendLine();
          sb.AppendLine("Fråga5: " + Resources.BiobransleFraga5);
          sb.AppendLine();
          sb.AppendLine("Fråga6: " + Resources.BiobransleFraga6);
          sb.AppendLine();
          sb.AppendLine("Fråga7: " + Resources.BiobransleFraga7);
          sb.AppendLine();
          sb.AppendLine("Fråga8: " + Resources.BiobransleFraga8);
          sb.AppendLine();
          sb.AppendLine("Fråga9: " + Resources.BiobransleFraga9);
          sb.AppendLine();
          sb.AppendLine("Fråga10: " + Resources.BiobransleFraga10);
          sb.AppendLine();
          sb.AppendLine("Fråga11: " + Resources.BiobransleFraga11);
          sb.AppendLine();
          sb.AppendLine("Fråga12: " + Resources.BiobransleFraga12);
          sb.AppendLine();
          sb.AppendLine("Fråga13: " + Resources.BiobransleFraga13);
          sb.AppendLine();
          sb.AppendLine("Fråga14: " + Resources.BiobransleFraga14);
          sb.AppendLine();
          sb.AppendLine("Fråga15: " + Resources.BiobransleFraga15);
          sb.AppendLine();
          sb.AppendLine("Fråga16: " + Resources.BiobransleFraga16);
          sb.AppendLine();
          sb.AppendLine("Fråga17: " + Resources.BiobransleFraga17);
          sb.AppendLine();
          sb.AppendLine("Fråga18: " + Resources.BiobransleFraga18);
          sb.AppendLine();
          sb.AppendLine("Fråga19: " + Resources.BiobransleFraga19);
          break;
      }

      return sb.ToString();
    }

    private void FormVisaFrågor_Load(object sender, EventArgs e)
    {
      richTextBoxFrågor.Text = HämtaFrågorBeroendePåRapportTyp();
    }

    private void buttonOk_Click(object sender, EventArgs e)
    {
      DialogResult = DialogResult.OK;
      Close();
    }
  }
}