﻿namespace Egenuppfoljning.Dialogs
{
  partial class FormFiltreraYtor
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.components = new System.ComponentModel.Container();
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormFiltreraYtor));
        this.groupBoxTabellData = new System.Windows.Forms.GroupBox();
        this.label1 = new System.Windows.Forms.Label();
        this.textBoxStickvbredd = new System.Windows.Forms.TextBox();
        this.label25 = new System.Windows.Forms.Label();
        this.textBoxSkador = new System.Windows.Forms.TextBox();
        this.label26 = new System.Windows.Forms.Label();
        this.textBoxSumma = new System.Windows.Forms.TextBox();
        this.textBoxContorta = new System.Windows.Forms.TextBox();
        this.label19 = new System.Windows.Forms.Label();
        this.label20 = new System.Windows.Forms.Label();
        this.textBoxMHöjd = new System.Windows.Forms.TextBox();
        this.label21 = new System.Windows.Forms.Label();
        this.textBoxHst = new System.Windows.Forms.TextBox();
        this.label22 = new System.Windows.Forms.Label();
        this.textBoxLövMedelhöjd = new System.Windows.Forms.TextBox();
        this.label23 = new System.Windows.Forms.Label();
        this.textBoxContortaMedelhöjd = new System.Windows.Forms.TextBox();
        this.label24 = new System.Windows.Forms.Label();
        this.textBoxLöv = new System.Windows.Forms.TextBox();
        this.textBoxGran = new System.Windows.Forms.TextBox();
        this.label13 = new System.Windows.Forms.Label();
        this.label14 = new System.Windows.Forms.Label();
        this.textBoxTallMedelhöjd = new System.Windows.Forms.TextBox();
        this.label15 = new System.Windows.Forms.Label();
        this.textBoxTall = new System.Windows.Forms.TextBox();
        this.label16 = new System.Windows.Forms.Label();
        this.textBoxTilltryckning = new System.Windows.Forms.TextBox();
        this.label17 = new System.Windows.Forms.Label();
        this.textBoxGranMedelhöjd = new System.Windows.Forms.TextBox();
        this.label18 = new System.Windows.Forms.Label();
        this.textBoxÖvrigt = new System.Windows.Forms.TextBox();
        this.label7 = new System.Windows.Forms.Label();
        this.textBoxOptimaltBra = new System.Windows.Forms.TextBox();
        this.label8 = new System.Windows.Forms.Label();
        this.textBoxBlekjordsfläck = new System.Windows.Forms.TextBox();
        this.label9 = new System.Windows.Forms.Label();
        this.textBoxVaravBättre = new System.Windows.Forms.TextBox();
        this.label10 = new System.Windows.Forms.Label();
        this.textBoxBra = new System.Windows.Forms.TextBox();
        this.label11 = new System.Windows.Forms.Label();
        this.textBoxOptimalt = new System.Windows.Forms.TextBox();
        this.label12 = new System.Windows.Forms.Label();
        this.textBoxYta = new System.Windows.Forms.TextBox();
        this.groupBox1 = new System.Windows.Forms.GroupBox();
        this.comboBoxInvTypId = new System.Windows.Forms.ComboBox();
        this.dataSetFilter = new System.Data.DataSet();
        this.dataTableRegion = new System.Data.DataTable();
        this.dataColumnFilterRegionId = new System.Data.DataColumn();
        this.dataColumnFilterRegionNamn = new System.Data.DataColumn();
        this.dataTableDistrikt = new System.Data.DataTable();
        this.dataColumnFilterDistriktRegionId = new System.Data.DataColumn();
        this.dataColumnFilterDistriktId = new System.Data.DataColumn();
        this.dataColumnFilterDistriktNamn = new System.Data.DataColumn();
        this.dataTableTaktDel = new System.Data.DataTable();
        this.dataColumnFilterTraktDel = new System.Data.DataColumn();
        this.dataTableTrakt = new System.Data.DataTable();
        this.dataColumnFilterTraktId = new System.Data.DataColumn();
        this.dataColumnFilterTraktNamn = new System.Data.DataColumn();
        this.dataTableEntreprenör = new System.Data.DataTable();
        this.dataColumnFilterEntreprenörId = new System.Data.DataColumn();
        this.dataColumnFilterEntreprenörNamn = new System.Data.DataColumn();
        this.dataTableRapportTyp = new System.Data.DataTable();
        this.dataColumnFilterRapportTypId = new System.Data.DataColumn();
        this.dataColumnFilterRapportTypNamn = new System.Data.DataColumn();
        this.dataTableArtal = new System.Data.DataTable();
        this.dataColumnFilterÅrtalId = new System.Data.DataColumn();
        this.dataTableInvTyp = new System.Data.DataTable();
        this.dataColumnFilterInvTypId = new System.Data.DataColumn();
        this.dataColumnFilterInvTypNamn = new System.Data.DataColumn();
        this.label2 = new System.Windows.Forms.Label();
        this.buttonTömAllaFilter = new System.Windows.Forms.Button();
        this.imageListFilter = new System.Windows.Forms.ImageList(this.components);
        this.comboBoxÅrtal = new System.Windows.Forms.ComboBox();
        this.buttonAvbryt = new System.Windows.Forms.Button();
        this.buttonUppdateraTabell = new System.Windows.Forms.Button();
        this.comboBoxTraktDelId = new System.Windows.Forms.ComboBox();
        this.comboBoxEntreprenörId = new System.Windows.Forms.ComboBox();
        this.comboBoxTraktNr = new System.Windows.Forms.ComboBox();
        this.comboBoxDistriktId = new System.Windows.Forms.ComboBox();
        this.comboBoxRegionId = new System.Windows.Forms.ComboBox();
        this.label37 = new System.Windows.Forms.Label();
        this.label38 = new System.Windows.Forms.Label();
        this.label39 = new System.Windows.Forms.Label();
        this.label40 = new System.Windows.Forms.Label();
        this.label41 = new System.Windows.Forms.Label();
        this.label42 = new System.Windows.Forms.Label();
        this.groupBoxFiltreringsalternativ = new System.Windows.Forms.GroupBox();
        this.radioButtonAllaRapportTyper = new System.Windows.Forms.RadioButton();
        this.radioButtonValdRapportTyp = new System.Windows.Forms.RadioButton();
        this.groupBoxTabellData.SuspendLayout();
        this.groupBox1.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetFilter)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableRegion)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableDistrikt)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableTaktDel)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableTrakt)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableEntreprenör)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableRapportTyp)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableArtal)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableInvTyp)).BeginInit();
        this.groupBoxFiltreringsalternativ.SuspendLayout();
        this.SuspendLayout();
        // 
        // groupBoxTabellData
        // 
        this.groupBoxTabellData.Controls.Add(this.label1);
        this.groupBoxTabellData.Controls.Add(this.textBoxStickvbredd);
        this.groupBoxTabellData.Controls.Add(this.label25);
        this.groupBoxTabellData.Controls.Add(this.textBoxSkador);
        this.groupBoxTabellData.Controls.Add(this.label26);
        this.groupBoxTabellData.Controls.Add(this.textBoxSumma);
        this.groupBoxTabellData.Controls.Add(this.textBoxContorta);
        this.groupBoxTabellData.Controls.Add(this.label19);
        this.groupBoxTabellData.Controls.Add(this.label20);
        this.groupBoxTabellData.Controls.Add(this.textBoxMHöjd);
        this.groupBoxTabellData.Controls.Add(this.label21);
        this.groupBoxTabellData.Controls.Add(this.textBoxHst);
        this.groupBoxTabellData.Controls.Add(this.label22);
        this.groupBoxTabellData.Controls.Add(this.textBoxLövMedelhöjd);
        this.groupBoxTabellData.Controls.Add(this.label23);
        this.groupBoxTabellData.Controls.Add(this.textBoxContortaMedelhöjd);
        this.groupBoxTabellData.Controls.Add(this.label24);
        this.groupBoxTabellData.Controls.Add(this.textBoxLöv);
        this.groupBoxTabellData.Controls.Add(this.textBoxGran);
        this.groupBoxTabellData.Controls.Add(this.label13);
        this.groupBoxTabellData.Controls.Add(this.label14);
        this.groupBoxTabellData.Controls.Add(this.textBoxTallMedelhöjd);
        this.groupBoxTabellData.Controls.Add(this.label15);
        this.groupBoxTabellData.Controls.Add(this.textBoxTall);
        this.groupBoxTabellData.Controls.Add(this.label16);
        this.groupBoxTabellData.Controls.Add(this.textBoxTilltryckning);
        this.groupBoxTabellData.Controls.Add(this.label17);
        this.groupBoxTabellData.Controls.Add(this.textBoxGranMedelhöjd);
        this.groupBoxTabellData.Controls.Add(this.label18);
        this.groupBoxTabellData.Controls.Add(this.textBoxÖvrigt);
        this.groupBoxTabellData.Controls.Add(this.label7);
        this.groupBoxTabellData.Controls.Add(this.textBoxOptimaltBra);
        this.groupBoxTabellData.Controls.Add(this.label8);
        this.groupBoxTabellData.Controls.Add(this.textBoxBlekjordsfläck);
        this.groupBoxTabellData.Controls.Add(this.label9);
        this.groupBoxTabellData.Controls.Add(this.textBoxVaravBättre);
        this.groupBoxTabellData.Controls.Add(this.label10);
        this.groupBoxTabellData.Controls.Add(this.textBoxBra);
        this.groupBoxTabellData.Controls.Add(this.label11);
        this.groupBoxTabellData.Controls.Add(this.textBoxOptimalt);
        this.groupBoxTabellData.Controls.Add(this.label12);
        this.groupBoxTabellData.Controls.Add(this.textBoxYta);
        this.groupBoxTabellData.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.groupBoxTabellData.Location = new System.Drawing.Point(0, 117);
        this.groupBoxTabellData.Name = "groupBoxTabellData";
        this.groupBoxTabellData.Size = new System.Drawing.Size(699, 241);
        this.groupBoxTabellData.TabIndex = 12;
        this.groupBoxTabellData.TabStop = false;
        // 
        // label1
        // 
        this.label1.AutoSize = true;
        this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label1.Location = new System.Drawing.Point(233, 75);
        this.label1.Name = "label1";
        this.label1.Size = new System.Drawing.Size(78, 13);
        this.label1.TabIndex = 40;
        this.label1.Text = "Stickvbredd:";
        // 
        // textBoxStickvbredd
        // 
        this.textBoxStickvbredd.Location = new System.Drawing.Point(235, 93);
        this.textBoxStickvbredd.MaxLength = 16;
        this.textBoxStickvbredd.Name = "textBoxStickvbredd";
        this.textBoxStickvbredd.Size = new System.Drawing.Size(90, 21);
        this.textBoxStickvbredd.TabIndex = 41;
        // 
        // label25
        // 
        this.label25.AutoSize = true;
        this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label25.Location = new System.Drawing.Point(343, 186);
        this.label25.Name = "label25";
        this.label25.Size = new System.Drawing.Size(50, 13);
        this.label25.TabIndex = 38;
        this.label25.Text = "Skador:";
        // 
        // textBoxSkador
        // 
        this.textBoxSkador.Location = new System.Drawing.Point(346, 204);
        this.textBoxSkador.MaxLength = 16;
        this.textBoxSkador.Name = "textBoxSkador";
        this.textBoxSkador.Size = new System.Drawing.Size(90, 21);
        this.textBoxSkador.TabIndex = 39;
        // 
        // label26
        // 
        this.label26.AutoSize = true;
        this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label26.Location = new System.Drawing.Point(234, 186);
        this.label26.Name = "label26";
        this.label26.Size = new System.Drawing.Size(53, 13);
        this.label26.TabIndex = 36;
        this.label26.Text = "Summa:";
        // 
        // textBoxSumma
        // 
        this.textBoxSumma.Location = new System.Drawing.Point(236, 204);
        this.textBoxSumma.MaxLength = 16;
        this.textBoxSumma.Name = "textBoxSumma";
        this.textBoxSumma.Size = new System.Drawing.Size(90, 21);
        this.textBoxSumma.TabIndex = 37;
        // 
        // textBoxContorta
        // 
        this.textBoxContorta.Location = new System.Drawing.Point(22, 204);
        this.textBoxContorta.MaxLength = 16;
        this.textBoxContorta.Name = "textBoxContorta";
        this.textBoxContorta.Size = new System.Drawing.Size(90, 21);
        this.textBoxContorta.TabIndex = 33;
        // 
        // label19
        // 
        this.label19.AutoSize = true;
        this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label19.Location = new System.Drawing.Point(564, 134);
        this.label19.Name = "label19";
        this.label19.Size = new System.Drawing.Size(93, 13);
        this.label19.TabIndex = 26;
        this.label19.Text = "Löv medelhöjd:";
        // 
        // label20
        // 
        this.label20.AutoSize = true;
        this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label20.Location = new System.Drawing.Point(565, 186);
        this.label20.Name = "label20";
        this.label20.Size = new System.Drawing.Size(50, 13);
        this.label20.TabIndex = 30;
        this.label20.Text = "M-höjd:";
        // 
        // textBoxMHöjd
        // 
        this.textBoxMHöjd.Location = new System.Drawing.Point(567, 204);
        this.textBoxMHöjd.MaxLength = 16;
        this.textBoxMHöjd.Name = "textBoxMHöjd";
        this.textBoxMHöjd.Size = new System.Drawing.Size(90, 21);
        this.textBoxMHöjd.TabIndex = 31;
        // 
        // label21
        // 
        this.label21.AutoSize = true;
        this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label21.Location = new System.Drawing.Point(128, 186);
        this.label21.Name = "label21";
        this.label21.Size = new System.Drawing.Size(123, 13);
        this.label21.TabIndex = 34;
        this.label21.Text = "Contorta medelhöjd:";
        // 
        // textBoxHst
        // 
        this.textBoxHst.Location = new System.Drawing.Point(457, 204);
        this.textBoxHst.MaxLength = 16;
        this.textBoxHst.Name = "textBoxHst";
        this.textBoxHst.Size = new System.Drawing.Size(90, 21);
        this.textBoxHst.TabIndex = 29;
        // 
        // label22
        // 
        this.label22.AutoSize = true;
        this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label22.Location = new System.Drawing.Point(453, 186);
        this.label22.Name = "label22";
        this.label22.Size = new System.Drawing.Size(29, 13);
        this.label22.TabIndex = 28;
        this.label22.Text = "Hst:";
        // 
        // textBoxLövMedelhöjd
        // 
        this.textBoxLövMedelhöjd.Location = new System.Drawing.Point(566, 152);
        this.textBoxLövMedelhöjd.MaxLength = 16;
        this.textBoxLövMedelhöjd.Name = "textBoxLövMedelhöjd";
        this.textBoxLövMedelhöjd.Size = new System.Drawing.Size(90, 21);
        this.textBoxLövMedelhöjd.TabIndex = 27;
        // 
        // label23
        // 
        this.label23.AutoSize = true;
        this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label23.Location = new System.Drawing.Point(19, 186);
        this.label23.Name = "label23";
        this.label23.Size = new System.Drawing.Size(60, 13);
        this.label23.TabIndex = 32;
        this.label23.Text = "Contorta:";
        // 
        // textBoxContortaMedelhöjd
        // 
        this.textBoxContortaMedelhöjd.Location = new System.Drawing.Point(131, 204);
        this.textBoxContortaMedelhöjd.MaxLength = 16;
        this.textBoxContortaMedelhöjd.Name = "textBoxContortaMedelhöjd";
        this.textBoxContortaMedelhöjd.Size = new System.Drawing.Size(90, 21);
        this.textBoxContortaMedelhöjd.TabIndex = 35;
        // 
        // label24
        // 
        this.label24.AutoSize = true;
        this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label24.Location = new System.Drawing.Point(454, 134);
        this.label24.Name = "label24";
        this.label24.Size = new System.Drawing.Size(30, 13);
        this.label24.TabIndex = 24;
        this.label24.Text = "Löv:";
        // 
        // textBoxLöv
        // 
        this.textBoxLöv.Location = new System.Drawing.Point(457, 152);
        this.textBoxLöv.MaxLength = 16;
        this.textBoxLöv.Name = "textBoxLöv";
        this.textBoxLöv.Size = new System.Drawing.Size(90, 21);
        this.textBoxLöv.TabIndex = 25;
        // 
        // textBoxGran
        // 
        this.textBoxGran.AcceptsTab = true;
        this.textBoxGran.Location = new System.Drawing.Point(235, 152);
        this.textBoxGran.MaxLength = 16;
        this.textBoxGran.Name = "textBoxGran";
        this.textBoxGran.Size = new System.Drawing.Size(90, 21);
        this.textBoxGran.TabIndex = 21;
        // 
        // label13
        // 
        this.label13.AutoSize = true;
        this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label13.Location = new System.Drawing.Point(126, 75);
        this.label13.Name = "label13";
        this.label13.Size = new System.Drawing.Size(80, 13);
        this.label13.TabIndex = 14;
        this.label13.Text = "Tilltryckning:";
        // 
        // label14
        // 
        this.label14.AutoSize = true;
        this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label14.Location = new System.Drawing.Point(125, 134);
        this.label14.Name = "label14";
        this.label14.Size = new System.Drawing.Size(93, 13);
        this.label14.TabIndex = 18;
        this.label14.Text = "Tall medelhöjd:";
        // 
        // textBoxTallMedelhöjd
        // 
        this.textBoxTallMedelhöjd.Location = new System.Drawing.Point(127, 152);
        this.textBoxTallMedelhöjd.MaxLength = 16;
        this.textBoxTallMedelhöjd.Name = "textBoxTallMedelhöjd";
        this.textBoxTallMedelhöjd.Size = new System.Drawing.Size(90, 21);
        this.textBoxTallMedelhöjd.TabIndex = 19;
        // 
        // label15
        // 
        this.label15.AutoSize = true;
        this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label15.Location = new System.Drawing.Point(343, 134);
        this.label15.Name = "label15";
        this.label15.Size = new System.Drawing.Size(100, 13);
        this.label15.TabIndex = 22;
        this.label15.Text = "Gran medelhöjd:";
        // 
        // textBoxTall
        // 
        this.textBoxTall.Location = new System.Drawing.Point(22, 152);
        this.textBoxTall.MaxLength = 16;
        this.textBoxTall.Name = "textBoxTall";
        this.textBoxTall.Size = new System.Drawing.Size(90, 21);
        this.textBoxTall.TabIndex = 17;
        // 
        // label16
        // 
        this.label16.AutoSize = true;
        this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label16.Location = new System.Drawing.Point(233, 134);
        this.label16.Name = "label16";
        this.label16.Size = new System.Drawing.Size(37, 13);
        this.label16.TabIndex = 20;
        this.label16.Text = "Gran:";
        // 
        // textBoxTilltryckning
        // 
        this.textBoxTilltryckning.Location = new System.Drawing.Point(128, 93);
        this.textBoxTilltryckning.MaxLength = 16;
        this.textBoxTilltryckning.Name = "textBoxTilltryckning";
        this.textBoxTilltryckning.Size = new System.Drawing.Size(90, 21);
        this.textBoxTilltryckning.TabIndex = 15;
        // 
        // label17
        // 
        this.label17.AutoSize = true;
        this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label17.Location = new System.Drawing.Point(20, 134);
        this.label17.Name = "label17";
        this.label17.Size = new System.Drawing.Size(30, 13);
        this.label17.TabIndex = 16;
        this.label17.Text = "Tall:";
        // 
        // textBoxGranMedelhöjd
        // 
        this.textBoxGranMedelhöjd.Location = new System.Drawing.Point(347, 152);
        this.textBoxGranMedelhöjd.MaxLength = 16;
        this.textBoxGranMedelhöjd.Name = "textBoxGranMedelhöjd";
        this.textBoxGranMedelhöjd.Size = new System.Drawing.Size(90, 21);
        this.textBoxGranMedelhöjd.TabIndex = 23;
        // 
        // label18
        // 
        this.label18.AutoSize = true;
        this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label18.Location = new System.Drawing.Point(16, 75);
        this.label18.Name = "label18";
        this.label18.Size = new System.Drawing.Size(45, 13);
        this.label18.TabIndex = 12;
        this.label18.Text = "Övrigt:";
        // 
        // textBoxÖvrigt
        // 
        this.textBoxÖvrigt.Location = new System.Drawing.Point(19, 93);
        this.textBoxÖvrigt.MaxLength = 16;
        this.textBoxÖvrigt.Name = "textBoxÖvrigt";
        this.textBoxÖvrigt.Size = new System.Drawing.Size(90, 21);
        this.textBoxÖvrigt.TabIndex = 13;
        // 
        // label7
        // 
        this.label7.AutoSize = true;
        this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label7.Location = new System.Drawing.Point(562, 17);
        this.label7.Name = "label7";
        this.label7.Size = new System.Drawing.Size(81, 13);
        this.label7.TabIndex = 10;
        this.label7.Text = "Optimalt bra:";
        // 
        // textBoxOptimaltBra
        // 
        this.textBoxOptimaltBra.Location = new System.Drawing.Point(567, 35);
        this.textBoxOptimaltBra.MaxLength = 16;
        this.textBoxOptimaltBra.Name = "textBoxOptimaltBra";
        this.textBoxOptimaltBra.Size = new System.Drawing.Size(90, 21);
        this.textBoxOptimaltBra.TabIndex = 11;
        // 
        // label8
        // 
        this.label8.AutoSize = true;
        this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label8.Location = new System.Drawing.Point(451, 17);
        this.label8.Name = "label8";
        this.label8.Size = new System.Drawing.Size(90, 13);
        this.label8.TabIndex = 8;
        this.label8.Text = "Blekjordsfläck:";
        // 
        // textBoxBlekjordsfläck
        // 
        this.textBoxBlekjordsfläck.Location = new System.Drawing.Point(456, 35);
        this.textBoxBlekjordsfläck.MaxLength = 16;
        this.textBoxBlekjordsfläck.Name = "textBoxBlekjordsfläck";
        this.textBoxBlekjordsfläck.Size = new System.Drawing.Size(90, 21);
        this.textBoxBlekjordsfläck.TabIndex = 9;
        // 
        // label9
        // 
        this.label9.AutoSize = true;
        this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label9.Location = new System.Drawing.Point(343, 17);
        this.label9.Name = "label9";
        this.label9.Size = new System.Drawing.Size(82, 13);
        this.label9.TabIndex = 6;
        this.label9.Text = "Varav bättre:";
        // 
        // textBoxVaravBättre
        // 
        this.textBoxVaravBättre.Location = new System.Drawing.Point(346, 35);
        this.textBoxVaravBättre.MaxLength = 16;
        this.textBoxVaravBättre.Name = "textBoxVaravBättre";
        this.textBoxVaravBättre.Size = new System.Drawing.Size(90, 21);
        this.textBoxVaravBättre.TabIndex = 7;
        // 
        // label10
        // 
        this.label10.AutoSize = true;
        this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label10.Location = new System.Drawing.Point(232, 17);
        this.label10.Name = "label10";
        this.label10.Size = new System.Drawing.Size(29, 13);
        this.label10.TabIndex = 4;
        this.label10.Text = "Bra:";
        // 
        // textBoxBra
        // 
        this.textBoxBra.Location = new System.Drawing.Point(236, 35);
        this.textBoxBra.MaxLength = 16;
        this.textBoxBra.Name = "textBoxBra";
        this.textBoxBra.Size = new System.Drawing.Size(90, 21);
        this.textBoxBra.TabIndex = 5;
        // 
        // label11
        // 
        this.label11.AutoSize = true;
        this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label11.Location = new System.Drawing.Point(127, 17);
        this.label11.Name = "label11";
        this.label11.Size = new System.Drawing.Size(59, 13);
        this.label11.TabIndex = 2;
        this.label11.Text = "Optimalt:";
        // 
        // textBoxOptimalt
        // 
        this.textBoxOptimalt.Location = new System.Drawing.Point(129, 35);
        this.textBoxOptimalt.MaxLength = 16;
        this.textBoxOptimalt.Name = "textBoxOptimalt";
        this.textBoxOptimalt.Size = new System.Drawing.Size(90, 21);
        this.textBoxOptimalt.TabIndex = 3;
        // 
        // label12
        // 
        this.label12.AutoSize = true;
        this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label12.Location = new System.Drawing.Point(16, 17);
        this.label12.Name = "label12";
        this.label12.Size = new System.Drawing.Size(44, 13);
        this.label12.TabIndex = 0;
        this.label12.Text = "Yta nr:";
        // 
        // textBoxYta
        // 
        this.textBoxYta.Location = new System.Drawing.Point(20, 35);
        this.textBoxYta.MaxLength = 16;
        this.textBoxYta.Name = "textBoxYta";
        this.textBoxYta.Size = new System.Drawing.Size(90, 21);
        this.textBoxYta.TabIndex = 1;
        // 
        // groupBox1
        // 
        this.groupBox1.Controls.Add(this.comboBoxInvTypId);
        this.groupBox1.Controls.Add(this.label2);
        this.groupBox1.Controls.Add(this.buttonTömAllaFilter);
        this.groupBox1.Controls.Add(this.groupBoxTabellData);
        this.groupBox1.Controls.Add(this.comboBoxÅrtal);
        this.groupBox1.Controls.Add(this.buttonAvbryt);
        this.groupBox1.Controls.Add(this.buttonUppdateraTabell);
        this.groupBox1.Controls.Add(this.comboBoxTraktDelId);
        this.groupBox1.Controls.Add(this.comboBoxEntreprenörId);
        this.groupBox1.Controls.Add(this.comboBoxTraktNr);
        this.groupBox1.Controls.Add(this.comboBoxDistriktId);
        this.groupBox1.Controls.Add(this.comboBoxRegionId);
        this.groupBox1.Controls.Add(this.label37);
        this.groupBox1.Controls.Add(this.label38);
        this.groupBox1.Controls.Add(this.label39);
        this.groupBox1.Controls.Add(this.label40);
        this.groupBox1.Controls.Add(this.label41);
        this.groupBox1.Controls.Add(this.label42);
        this.groupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.groupBox1.Location = new System.Drawing.Point(7, 48);
        this.groupBox1.Name = "groupBox1";
        this.groupBox1.Size = new System.Drawing.Size(699, 405);
        this.groupBox1.TabIndex = 0;
        this.groupBox1.TabStop = false;
        // 
        // comboBoxInvTypId
        // 
        this.comboBoxInvTypId.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.dataSetFilter, "InvTyp.Id", true));
        this.comboBoxInvTypId.DataSource = this.dataSetFilter;
        this.comboBoxInvTypId.DisplayMember = "InvTyp.Namn";
        this.comboBoxInvTypId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxInvTypId.FormattingEnabled = true;
        this.comboBoxInvTypId.Location = new System.Drawing.Point(152, 85);
        this.comboBoxInvTypId.Name = "comboBoxInvTypId";
        this.comboBoxInvTypId.Size = new System.Drawing.Size(116, 21);
        this.comboBoxInvTypId.TabIndex = 17;
        this.comboBoxInvTypId.ValueMember = "InvTyp.Id";
        // 
        // dataSetFilter
        // 
        this.dataSetFilter.DataSetName = "DataSetFilter";
        this.dataSetFilter.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableRegion,
            this.dataTableDistrikt,
            this.dataTableTaktDel,
            this.dataTableTrakt,
            this.dataTableEntreprenör,
            this.dataTableRapportTyp,
            this.dataTableArtal,
            this.dataTableInvTyp});
        // 
        // dataTableRegion
        // 
        this.dataTableRegion.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFilterRegionId,
            this.dataColumnFilterRegionNamn});
        this.dataTableRegion.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, true)});
        this.dataTableRegion.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnFilterRegionId};
        this.dataTableRegion.TableName = "Region";
        // 
        // dataColumnFilterRegionId
        // 
        this.dataColumnFilterRegionId.AllowDBNull = false;
        this.dataColumnFilterRegionId.ColumnName = "Id";
        this.dataColumnFilterRegionId.DataType = typeof(int);
        this.dataColumnFilterRegionId.ReadOnly = true;
        // 
        // dataColumnFilterRegionNamn
        // 
        this.dataColumnFilterRegionNamn.ColumnName = "Namn";
        // 
        // dataTableDistrikt
        // 
        this.dataTableDistrikt.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFilterDistriktRegionId,
            this.dataColumnFilterDistriktId,
            this.dataColumnFilterDistriktNamn});
        this.dataTableDistrikt.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, true)});
        this.dataTableDistrikt.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnFilterDistriktId};
        this.dataTableDistrikt.TableName = "Distrikt";
        // 
        // dataColumnFilterDistriktRegionId
        // 
        this.dataColumnFilterDistriktRegionId.ColumnName = "RegionId";
        this.dataColumnFilterDistriktRegionId.DataType = typeof(int);
        // 
        // dataColumnFilterDistriktId
        // 
        this.dataColumnFilterDistriktId.AllowDBNull = false;
        this.dataColumnFilterDistriktId.ColumnName = "Id";
        this.dataColumnFilterDistriktId.DataType = typeof(int);
        // 
        // dataColumnFilterDistriktNamn
        // 
        this.dataColumnFilterDistriktNamn.ColumnName = "Namn";
        // 
        // dataTableTaktDel
        // 
        this.dataTableTaktDel.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFilterTraktDel});
        this.dataTableTaktDel.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, true)});
        this.dataTableTaktDel.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnFilterTraktDel};
        this.dataTableTaktDel.TableName = "TraktDel";
        // 
        // dataColumnFilterTraktDel
        // 
        this.dataColumnFilterTraktDel.AllowDBNull = false;
        this.dataColumnFilterTraktDel.ColumnName = "Id";
        this.dataColumnFilterTraktDel.DataType = typeof(int);
        // 
        // dataTableTrakt
        // 
        this.dataTableTrakt.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFilterTraktId,
            this.dataColumnFilterTraktNamn});
        this.dataTableTrakt.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, true)});
        this.dataTableTrakt.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnFilterTraktId};
        this.dataTableTrakt.TableName = "Trakt";
        // 
        // dataColumnFilterTraktId
        // 
        this.dataColumnFilterTraktId.AllowDBNull = false;
        this.dataColumnFilterTraktId.ColumnName = "Id";
        this.dataColumnFilterTraktId.DataType = typeof(int);
        // 
        // dataColumnFilterTraktNamn
        // 
        this.dataColumnFilterTraktNamn.ColumnName = "Namn";
        // 
        // dataTableEntreprenör
        // 
        this.dataTableEntreprenör.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFilterEntreprenörId,
            this.dataColumnFilterEntreprenörNamn});
        this.dataTableEntreprenör.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, true)});
        this.dataTableEntreprenör.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnFilterEntreprenörId};
        this.dataTableEntreprenör.TableName = "Entreprenör";
        // 
        // dataColumnFilterEntreprenörId
        // 
        this.dataColumnFilterEntreprenörId.AllowDBNull = false;
        this.dataColumnFilterEntreprenörId.ColumnName = "Id";
        // 
        // dataColumnFilterEntreprenörNamn
        // 
        this.dataColumnFilterEntreprenörNamn.ColumnName = "Namn";
        // 
        // dataTableRapportTyp
        // 
        this.dataTableRapportTyp.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFilterRapportTypId,
            this.dataColumnFilterRapportTypNamn});
        this.dataTableRapportTyp.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, true)});
        this.dataTableRapportTyp.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnFilterRapportTypId};
        this.dataTableRapportTyp.TableName = "RapportTyp";
        // 
        // dataColumnFilterRapportTypId
        // 
        this.dataColumnFilterRapportTypId.AllowDBNull = false;
        this.dataColumnFilterRapportTypId.ColumnName = "Id";
        this.dataColumnFilterRapportTypId.DataType = typeof(int);
        // 
        // dataColumnFilterRapportTypNamn
        // 
        this.dataColumnFilterRapportTypNamn.ColumnName = "Namn";
        // 
        // dataTableArtal
        // 
        this.dataTableArtal.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFilterÅrtalId});
        this.dataTableArtal.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, true)});
        this.dataTableArtal.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnFilterÅrtalId};
        this.dataTableArtal.TableName = "Artal";
        // 
        // dataColumnFilterÅrtalId
        // 
        this.dataColumnFilterÅrtalId.AllowDBNull = false;
        this.dataColumnFilterÅrtalId.ColumnName = "Id";
        this.dataColumnFilterÅrtalId.DataType = typeof(int);
        // 
        // dataTableInvTyp
        // 
        this.dataTableInvTyp.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFilterInvTypId,
            this.dataColumnFilterInvTypNamn});
        this.dataTableInvTyp.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, true)});
        this.dataTableInvTyp.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnFilterInvTypId};
        this.dataTableInvTyp.TableName = "InvTyp";
        // 
        // dataColumnFilterInvTypId
        // 
        this.dataColumnFilterInvTypId.AllowDBNull = false;
        this.dataColumnFilterInvTypId.ColumnName = "Id";
        this.dataColumnFilterInvTypId.DataType = typeof(int);
        this.dataColumnFilterInvTypId.ReadOnly = true;
        // 
        // dataColumnFilterInvTypNamn
        // 
        this.dataColumnFilterInvTypNamn.ColumnName = "Namn";
        // 
        // label2
        // 
        this.label2.AutoSize = true;
        this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label2.Location = new System.Drawing.Point(152, 67);
        this.label2.Name = "label2";
        this.label2.Size = new System.Drawing.Size(63, 13);
        this.label2.TabIndex = 16;
        this.label2.Text = "Invtyp Id:";
        // 
        // buttonTömAllaFilter
        // 
        this.buttonTömAllaFilter.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.buttonTömAllaFilter.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonTömAllaFilter.ImageIndex = 1;
        this.buttonTömAllaFilter.ImageList = this.imageListFilter;
        this.buttonTömAllaFilter.Location = new System.Drawing.Point(288, 366);
        this.buttonTömAllaFilter.Name = "buttonTömAllaFilter";
        this.buttonTömAllaFilter.Size = new System.Drawing.Size(144, 30);
        this.buttonTömAllaFilter.TabIndex = 14;
        this.buttonTömAllaFilter.Text = "Töm alla filter";
        this.buttonTömAllaFilter.UseVisualStyleBackColor = true;
        this.buttonTömAllaFilter.Click += new System.EventHandler(this.buttonTömAllaFilter_Click);
        // 
        // imageListFilter
        // 
        this.imageListFilter.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListFilter.ImageStream")));
        this.imageListFilter.TransparentColor = System.Drawing.Color.Transparent;
        this.imageListFilter.Images.SetKeyName(0, "UppdateraFilter.ico");
        this.imageListFilter.Images.SetKeyName(1, "TömFilter.ico");
        this.imageListFilter.Images.SetKeyName(2, "Neka.ico");
        // 
        // comboBoxÅrtal
        // 
        this.comboBoxÅrtal.DataSource = this.dataSetFilter;
        this.comboBoxÅrtal.DisplayMember = "Artal.Id";
        this.comboBoxÅrtal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxÅrtal.FormattingEnabled = true;
        this.comboBoxÅrtal.Location = new System.Drawing.Point(21, 85);
        this.comboBoxÅrtal.Name = "comboBoxÅrtal";
        this.comboBoxÅrtal.Size = new System.Drawing.Size(116, 21);
        this.comboBoxÅrtal.TabIndex = 11;
        // 
        // buttonAvbryt
        // 
        this.buttonAvbryt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.buttonAvbryt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonAvbryt.ImageIndex = 2;
        this.buttonAvbryt.ImageList = this.imageListFilter;
        this.buttonAvbryt.Location = new System.Drawing.Point(544, 366);
        this.buttonAvbryt.Name = "buttonAvbryt";
        this.buttonAvbryt.Size = new System.Drawing.Size(144, 30);
        this.buttonAvbryt.TabIndex = 15;
        this.buttonAvbryt.Text = "Avbryt";
        this.buttonAvbryt.UseVisualStyleBackColor = true;
        this.buttonAvbryt.Click += new System.EventHandler(this.buttonAvbryt_Click);
        // 
        // buttonUppdateraTabell
        // 
        this.buttonUppdateraTabell.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.buttonUppdateraTabell.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonUppdateraTabell.ImageIndex = 0;
        this.buttonUppdateraTabell.ImageList = this.imageListFilter;
        this.buttonUppdateraTabell.Location = new System.Drawing.Point(14, 366);
        this.buttonUppdateraTabell.Name = "buttonUppdateraTabell";
        this.buttonUppdateraTabell.Size = new System.Drawing.Size(144, 30);
        this.buttonUppdateraTabell.TabIndex = 13;
        this.buttonUppdateraTabell.Text = "Uppdatera tabell";
        this.buttonUppdateraTabell.UseVisualStyleBackColor = true;
        this.buttonUppdateraTabell.Click += new System.EventHandler(this.buttonUppdateraTabell_Click);
        // 
        // comboBoxTraktDelId
        // 
        this.comboBoxTraktDelId.DataSource = this.dataSetFilter;
        this.comboBoxTraktDelId.DisplayMember = "TraktDel.Id";
        this.comboBoxTraktDelId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxTraktDelId.FormattingEnabled = true;
        this.comboBoxTraktDelId.Location = new System.Drawing.Point(561, 35);
        this.comboBoxTraktDelId.Name = "comboBoxTraktDelId";
        this.comboBoxTraktDelId.Size = new System.Drawing.Size(116, 21);
        this.comboBoxTraktDelId.TabIndex = 9;
        this.comboBoxTraktDelId.ValueMember = "TraktDel.Id";
        // 
        // comboBoxEntreprenörId
        // 
        this.comboBoxEntreprenörId.DataSource = this.dataSetFilter;
        this.comboBoxEntreprenörId.DisplayMember = "Entreprenör.Id";
        this.comboBoxEntreprenörId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxEntreprenörId.FormattingEnabled = true;
        this.comboBoxEntreprenörId.Location = new System.Drawing.Point(425, 35);
        this.comboBoxEntreprenörId.Name = "comboBoxEntreprenörId";
        this.comboBoxEntreprenörId.Size = new System.Drawing.Size(116, 21);
        this.comboBoxEntreprenörId.TabIndex = 7;
        this.comboBoxEntreprenörId.ValueMember = "Region.Id";
        // 
        // comboBoxTraktNr
        // 
        this.comboBoxTraktNr.DataSource = this.dataSetFilter;
        this.comboBoxTraktNr.DisplayMember = "Trakt.Id";
        this.comboBoxTraktNr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxTraktNr.FormatString = "000000";
        this.comboBoxTraktNr.FormattingEnabled = true;
        this.comboBoxTraktNr.Location = new System.Drawing.Point(288, 35);
        this.comboBoxTraktNr.Name = "comboBoxTraktNr";
        this.comboBoxTraktNr.Size = new System.Drawing.Size(116, 21);
        this.comboBoxTraktNr.TabIndex = 5;
        this.comboBoxTraktNr.ValueMember = "Region.Id";
        // 
        // comboBoxDistriktId
        // 
        this.comboBoxDistriktId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxDistriktId.FormattingEnabled = true;
        this.comboBoxDistriktId.Location = new System.Drawing.Point(155, 35);
        this.comboBoxDistriktId.Name = "comboBoxDistriktId";
        this.comboBoxDistriktId.Size = new System.Drawing.Size(116, 21);
        this.comboBoxDistriktId.TabIndex = 3;
        // 
        // comboBoxRegionId
        // 
        this.comboBoxRegionId.DataSource = this.dataSetFilter;
        this.comboBoxRegionId.DisplayMember = "Region.Id";
        this.comboBoxRegionId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxRegionId.FormattingEnabled = true;
        this.comboBoxRegionId.Location = new System.Drawing.Point(21, 35);
        this.comboBoxRegionId.Name = "comboBoxRegionId";
        this.comboBoxRegionId.Size = new System.Drawing.Size(116, 21);
        this.comboBoxRegionId.TabIndex = 1;
        this.comboBoxRegionId.ValueMember = "Region.Id";
        this.comboBoxRegionId.SelectedIndexChanged += new System.EventHandler(this.comboBoxRegionId_SelectedIndexChanged);
        // 
        // label37
        // 
        this.label37.AutoSize = true;
        this.label37.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label37.Location = new System.Drawing.Point(421, 17);
        this.label37.Name = "label37";
        this.label37.Size = new System.Drawing.Size(90, 13);
        this.label37.TabIndex = 6;
        this.label37.Text = "Entreprenörnr:";
        // 
        // label38
        // 
        this.label38.AutoSize = true;
        this.label38.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label38.Location = new System.Drawing.Point(558, 17);
        this.label38.Name = "label38";
        this.label38.Size = new System.Drawing.Size(58, 13);
        this.label38.TabIndex = 8;
        this.label38.Text = "Traktdel:";
        // 
        // label39
        // 
        this.label39.AutoSize = true;
        this.label39.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label39.Location = new System.Drawing.Point(285, 17);
        this.label39.Name = "label39";
        this.label39.Size = new System.Drawing.Size(56, 13);
        this.label39.TabIndex = 4;
        this.label39.Text = "Trakt Nr:";
        // 
        // label40
        // 
        this.label40.AutoSize = true;
        this.label40.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label40.Location = new System.Drawing.Point(152, 17);
        this.label40.Name = "label40";
        this.label40.Size = new System.Drawing.Size(67, 13);
        this.label40.TabIndex = 2;
        this.label40.Text = "Distrikt Id:";
        // 
        // label41
        // 
        this.label41.AutoSize = true;
        this.label41.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label41.Location = new System.Drawing.Point(17, 17);
        this.label41.Name = "label41";
        this.label41.Size = new System.Drawing.Size(64, 13);
        this.label41.TabIndex = 0;
        this.label41.Text = "Region Id:";
        // 
        // label42
        // 
        this.label42.AutoSize = true;
        this.label42.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label42.Location = new System.Drawing.Point(17, 69);
        this.label42.Name = "label42";
        this.label42.Size = new System.Drawing.Size(38, 13);
        this.label42.TabIndex = 10;
        this.label42.Text = "Årtal:";
        // 
        // groupBoxFiltreringsalternativ
        // 
        this.groupBoxFiltreringsalternativ.Controls.Add(this.radioButtonAllaRapportTyper);
        this.groupBoxFiltreringsalternativ.Controls.Add(this.radioButtonValdRapportTyp);
        this.groupBoxFiltreringsalternativ.Location = new System.Drawing.Point(7, 6);
        this.groupBoxFiltreringsalternativ.Name = "groupBoxFiltreringsalternativ";
        this.groupBoxFiltreringsalternativ.Size = new System.Drawing.Size(699, 50);
        this.groupBoxFiltreringsalternativ.TabIndex = 2;
        this.groupBoxFiltreringsalternativ.TabStop = false;
        this.groupBoxFiltreringsalternativ.Text = "Visa filtreringsalternativ";
        // 
        // radioButtonAllaRapportTyper
        // 
        this.radioButtonAllaRapportTyper.AutoSize = true;
        this.radioButtonAllaRapportTyper.Location = new System.Drawing.Point(188, 21);
        this.radioButtonAllaRapportTyper.Name = "radioButtonAllaRapportTyper";
        this.radioButtonAllaRapportTyper.Size = new System.Drawing.Size(118, 17);
        this.radioButtonAllaRapportTyper.TabIndex = 1;
        this.radioButtonAllaRapportTyper.Text = "Alla rapporttyper";
        this.radioButtonAllaRapportTyper.UseVisualStyleBackColor = true;
        this.radioButtonAllaRapportTyper.CheckedChanged += new System.EventHandler(this.radioButtonAllaRapportTyper_CheckedChanged);
        // 
        // radioButtonValdRapportTyp
        // 
        this.radioButtonValdRapportTyp.AutoSize = true;
        this.radioButtonValdRapportTyp.Checked = true;
        this.radioButtonValdRapportTyp.Location = new System.Drawing.Point(19, 21);
        this.radioButtonValdRapportTyp.Name = "radioButtonValdRapportTyp";
        this.radioButtonValdRapportTyp.Size = new System.Drawing.Size(111, 17);
        this.radioButtonValdRapportTyp.TabIndex = 0;
        this.radioButtonValdRapportTyp.TabStop = true;
        this.radioButtonValdRapportTyp.Text = "Vald rapporttyp";
        this.radioButtonValdRapportTyp.UseVisualStyleBackColor = true;
        this.radioButtonValdRapportTyp.CheckedChanged += new System.EventHandler(this.radioButtonValdRapportTyp_CheckedChanged);
        // 
        // FormFiltreraYtor
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.ClientSize = new System.Drawing.Size(712, 462);
        this.Controls.Add(this.groupBoxFiltreringsalternativ);
        this.Controls.Add(this.groupBox1);
        this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
        this.Name = "FormFiltreraYtor";
        this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
        this.Text = "Filtrera ytor för ";
        this.Load += new System.EventHandler(this.FormFiltreraYtor_Load);
        this.groupBoxTabellData.ResumeLayout(false);
        this.groupBoxTabellData.PerformLayout();
        this.groupBox1.ResumeLayout(false);
        this.groupBox1.PerformLayout();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetFilter)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableRegion)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableDistrikt)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableTaktDel)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableTrakt)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableEntreprenör)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableRapportTyp)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableArtal)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableInvTyp)).EndInit();
        this.groupBoxFiltreringsalternativ.ResumeLayout(false);
        this.groupBoxFiltreringsalternativ.PerformLayout();
        this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox groupBoxTabellData;
    private System.Windows.Forms.TextBox textBoxGran;
    private System.Windows.Forms.Label label13;
    private System.Windows.Forms.Label label14;
    private System.Windows.Forms.TextBox textBoxTallMedelhöjd;
    private System.Windows.Forms.Label label15;
    private System.Windows.Forms.TextBox textBoxTall;
    private System.Windows.Forms.Label label16;
    private System.Windows.Forms.TextBox textBoxTilltryckning;
    private System.Windows.Forms.Label label17;
    private System.Windows.Forms.TextBox textBoxGranMedelhöjd;
    private System.Windows.Forms.Label label18;
    private System.Windows.Forms.TextBox textBoxÖvrigt;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.TextBox textBoxOptimaltBra;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.TextBox textBoxBlekjordsfläck;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.TextBox textBoxVaravBättre;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.TextBox textBoxBra;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.TextBox textBoxOptimalt;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.TextBox textBoxYta;
    private System.Windows.Forms.TextBox textBoxContorta;
    private System.Windows.Forms.Label label19;
    private System.Windows.Forms.Label label20;
    private System.Windows.Forms.TextBox textBoxMHöjd;
    private System.Windows.Forms.Label label21;
    private System.Windows.Forms.TextBox textBoxHst;
    private System.Windows.Forms.Label label22;
    private System.Windows.Forms.TextBox textBoxLövMedelhöjd;
    private System.Windows.Forms.Label label23;
    private System.Windows.Forms.TextBox textBoxContortaMedelhöjd;
    private System.Windows.Forms.Label label24;
    private System.Windows.Forms.TextBox textBoxLöv;
    private System.Windows.Forms.Label label25;
    private System.Windows.Forms.TextBox textBoxSkador;
    private System.Windows.Forms.Label label26;
    private System.Windows.Forms.TextBox textBoxSumma;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.Button buttonTömAllaFilter;
    private System.Windows.Forms.ComboBox comboBoxÅrtal;
    private System.Windows.Forms.Button buttonAvbryt;
    private System.Windows.Forms.Button buttonUppdateraTabell;
    private System.Windows.Forms.ComboBox comboBoxTraktDelId;
    private System.Windows.Forms.ComboBox comboBoxEntreprenörId;
    private System.Windows.Forms.ComboBox comboBoxTraktNr;
    private System.Windows.Forms.ComboBox comboBoxDistriktId;
    private System.Windows.Forms.ComboBox comboBoxRegionId;
    private System.Windows.Forms.Label label37;
    private System.Windows.Forms.Label label38;
    private System.Windows.Forms.Label label39;
    private System.Windows.Forms.Label label40;
    private System.Windows.Forms.Label label41;
    private System.Windows.Forms.Label label42;
    private System.Windows.Forms.ImageList imageListFilter;
    private System.Data.DataSet dataSetFilter;
    private System.Data.DataTable dataTableRegion;
    private System.Data.DataColumn dataColumnFilterRegionId;
    private System.Data.DataColumn dataColumnFilterRegionNamn;
    private System.Data.DataTable dataTableDistrikt;
    private System.Data.DataColumn dataColumnFilterDistriktRegionId;
    private System.Data.DataColumn dataColumnFilterDistriktId;
    private System.Data.DataColumn dataColumnFilterDistriktNamn;
    private System.Data.DataTable dataTableTaktDel;
    private System.Data.DataColumn dataColumnFilterTraktDel;
    private System.Data.DataTable dataTableTrakt;
    private System.Data.DataColumn dataColumnFilterTraktId;
    private System.Data.DataColumn dataColumnFilterTraktNamn;
    private System.Data.DataTable dataTableEntreprenör;
    private System.Data.DataColumn dataColumnFilterEntreprenörId;
    private System.Data.DataColumn dataColumnFilterEntreprenörNamn;
    private System.Data.DataTable dataTableRapportTyp;
    private System.Data.DataColumn dataColumnFilterRapportTypId;
    private System.Data.DataColumn dataColumnFilterRapportTypNamn;
    private System.Data.DataTable dataTableArtal;
    private System.Data.DataColumn dataColumnFilterÅrtalId;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TextBox textBoxStickvbredd;
    private System.Windows.Forms.GroupBox groupBoxFiltreringsalternativ;
    private System.Windows.Forms.RadioButton radioButtonAllaRapportTyper;
    private System.Windows.Forms.RadioButton radioButtonValdRapportTyp;
    private System.Windows.Forms.ComboBox comboBoxInvTypId;
    private System.Data.DataTable dataTableInvTyp;
    private System.Windows.Forms.Label label2;
    private System.Data.DataColumn dataColumnFilterInvTypId;
    private System.Data.DataColumn dataColumnFilterInvTypNamn;

  }
}