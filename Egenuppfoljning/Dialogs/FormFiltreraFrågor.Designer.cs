﻿namespace Egenuppfoljning.Dialogs
{
  partial class FormFiltreraFrågor
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.components = new System.ComponentModel.Container();
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormFiltreraFrågor));
        this.groupBoxTabellData = new System.Windows.Forms.GroupBox();
        this.comboBoxFråga21 = new System.Windows.Forms.ComboBox();
        this.dataSetFilter = new System.Data.DataSet();
        this.dataTableRegion = new System.Data.DataTable();
        this.dataColumnFilterRegionId = new System.Data.DataColumn();
        this.dataColumnFilterRegionNamn = new System.Data.DataColumn();
        this.dataTableDistrikt = new System.Data.DataTable();
        this.dataColumnFilterDistriktRegionId = new System.Data.DataColumn();
        this.dataColumnFilterDistriktId = new System.Data.DataColumn();
        this.dataColumnFilterDistriktNamn = new System.Data.DataColumn();
        this.dataTableStandort = new System.Data.DataTable();
        this.dataColumnFilterStandortNamn = new System.Data.DataColumn();
        this.dataTableTrakt = new System.Data.DataTable();
        this.dataColumnFilterTraktId = new System.Data.DataColumn();
        this.dataColumnFilterTraktNamn = new System.Data.DataColumn();
        this.dataTableEntreprenör = new System.Data.DataTable();
        this.dataColumnFilterEntreprenörId = new System.Data.DataColumn();
        this.dataColumnFilterEntreprenörNamn = new System.Data.DataColumn();
        this.dataTableRapportTyp = new System.Data.DataTable();
        this.dataColumnFilterRapportTypId = new System.Data.DataColumn();
        this.dataColumnFilterRapportTypNamn = new System.Data.DataColumn();
        this.dataTableArtal = new System.Data.DataTable();
        this.dataColumnFilterÅrtalId = new System.Data.DataColumn();
        this.dataTableFråga1 = new System.Data.DataTable();
        this.dataColumnSvar1 = new System.Data.DataColumn();
        this.dataTableFråga2 = new System.Data.DataTable();
        this.dataColumnSvar2 = new System.Data.DataColumn();
        this.dataTableFråga3 = new System.Data.DataTable();
        this.dataColumnSvar3 = new System.Data.DataColumn();
        this.dataTableFråga4 = new System.Data.DataTable();
        this.dataColumnSvar4 = new System.Data.DataColumn();
        this.dataTableFråga5 = new System.Data.DataTable();
        this.dataColumnSvar5 = new System.Data.DataColumn();
        this.dataTableFråga6 = new System.Data.DataTable();
        this.dataColumnSvar6 = new System.Data.DataColumn();
        this.dataTableFråga7 = new System.Data.DataTable();
        this.dataColumnSvar7 = new System.Data.DataColumn();
        this.dataTableFråga8 = new System.Data.DataTable();
        this.dataColumnSvar8 = new System.Data.DataColumn();
        this.dataTableFråga9 = new System.Data.DataTable();
        this.dataColumnSvar9 = new System.Data.DataColumn();
        this.dataTableFråga10 = new System.Data.DataTable();
        this.dataColumnSvar10 = new System.Data.DataColumn();
        this.dataTableFråga11 = new System.Data.DataTable();
        this.dataColumnSvar11 = new System.Data.DataColumn();
        this.dataTableFråga12 = new System.Data.DataTable();
        this.dataColumnSvar12 = new System.Data.DataColumn();
        this.dataTableFråga13 = new System.Data.DataTable();
        this.dataColumnSvar13 = new System.Data.DataColumn();
        this.dataTableFråga14 = new System.Data.DataTable();
        this.dataColumnFråga14 = new System.Data.DataColumn();
        this.dataTableFråga15 = new System.Data.DataTable();
        this.dataColumnFråga15 = new System.Data.DataColumn();
        this.dataTableFråga16 = new System.Data.DataTable();
        this.dataColumnFråga16 = new System.Data.DataColumn();
        this.dataTableFråga17 = new System.Data.DataTable();
        this.dataColumnFråga17 = new System.Data.DataColumn();
        this.dataTableFråga18 = new System.Data.DataTable();
        this.dataColumnFråga18 = new System.Data.DataColumn();
        this.dataTableFråga19 = new System.Data.DataTable();
        this.dataColumnFråga19 = new System.Data.DataColumn();
        this.dataTableFråga20 = new System.Data.DataTable();
        this.dataColumnFråga20 = new System.Data.DataColumn();
        this.dataTableFråga21 = new System.Data.DataTable();
        this.dataColumnFråga21 = new System.Data.DataColumn();
        this.dataTable1 = new System.Data.DataTable();
        this.dataColumndataSetFilterInvTypId = new System.Data.DataColumn();
        this.dataColumndataSetFilterInvTypNamn = new System.Data.DataColumn();
        this.label21 = new System.Windows.Forms.Label();
        this.comboBoxFråga20 = new System.Windows.Forms.ComboBox();
        this.label20 = new System.Windows.Forms.Label();
        this.comboBoxFråga15 = new System.Windows.Forms.ComboBox();
        this.label19 = new System.Windows.Forms.Label();
        this.comboBoxFråga19 = new System.Windows.Forms.ComboBox();
        this.comboBoxFråga18 = new System.Windows.Forms.ComboBox();
        this.comboBoxFråga17 = new System.Windows.Forms.ComboBox();
        this.comboBoxFråga16 = new System.Windows.Forms.ComboBox();
        this.comboBoxFråga10 = new System.Windows.Forms.ComboBox();
        this.comboBoxFråga14 = new System.Windows.Forms.ComboBox();
        this.comboBoxFråga13 = new System.Windows.Forms.ComboBox();
        this.comboBoxFråga12 = new System.Windows.Forms.ComboBox();
        this.comboBoxFråga11 = new System.Windows.Forms.ComboBox();
        this.label1 = new System.Windows.Forms.Label();
        this.label2 = new System.Windows.Forms.Label();
        this.label3 = new System.Windows.Forms.Label();
        this.label4 = new System.Windows.Forms.Label();
        this.label5 = new System.Windows.Forms.Label();
        this.label6 = new System.Windows.Forms.Label();
        this.label7 = new System.Windows.Forms.Label();
        this.label13 = new System.Windows.Forms.Label();
        this.label17 = new System.Windows.Forms.Label();
        this.comboBoxFråga9 = new System.Windows.Forms.ComboBox();
        this.comboBoxFråga8 = new System.Windows.Forms.ComboBox();
        this.comboBoxFråga7 = new System.Windows.Forms.ComboBox();
        this.comboBoxFråga6 = new System.Windows.Forms.ComboBox();
        this.comboBoxFråga5 = new System.Windows.Forms.ComboBox();
        this.comboBoxFråga4 = new System.Windows.Forms.ComboBox();
        this.comboBoxFråga3 = new System.Windows.Forms.ComboBox();
        this.comboBoxFråga2 = new System.Windows.Forms.ComboBox();
        this.comboBoxFråga1 = new System.Windows.Forms.ComboBox();
        this.label14 = new System.Windows.Forms.Label();
        this.label15 = new System.Windows.Forms.Label();
        this.label16 = new System.Windows.Forms.Label();
        this.label18 = new System.Windows.Forms.Label();
        this.label8 = new System.Windows.Forms.Label();
        this.label9 = new System.Windows.Forms.Label();
        this.label10 = new System.Windows.Forms.Label();
        this.label11 = new System.Windows.Forms.Label();
        this.label12 = new System.Windows.Forms.Label();
        this.groupBox1 = new System.Windows.Forms.GroupBox();
        this.comboBoxInvTypId = new System.Windows.Forms.ComboBox();
        this.label22 = new System.Windows.Forms.Label();
        this.buttonTömFilter = new System.Windows.Forms.Button();
        this.imageListFilter = new System.Windows.Forms.ImageList(this.components);
        this.comboBoxÅrtal = new System.Windows.Forms.ComboBox();
        this.buttonAvbryt = new System.Windows.Forms.Button();
        this.buttonUppdatera = new System.Windows.Forms.Button();
        this.comboBoxStåndortId = new System.Windows.Forms.ComboBox();
        this.comboBoxEntreprenörId = new System.Windows.Forms.ComboBox();
        this.comboBoxTraktNr = new System.Windows.Forms.ComboBox();
        this.comboBoxDistriktId = new System.Windows.Forms.ComboBox();
        this.comboBoxRegionId = new System.Windows.Forms.ComboBox();
        this.label33 = new System.Windows.Forms.Label();
        this.label34 = new System.Windows.Forms.Label();
        this.label35 = new System.Windows.Forms.Label();
        this.label36 = new System.Windows.Forms.Label();
        this.label37 = new System.Windows.Forms.Label();
        this.label38 = new System.Windows.Forms.Label();
        this.groupBoxFiltreringsalternativ = new System.Windows.Forms.GroupBox();
        this.radioButtonAllaRapportTyper = new System.Windows.Forms.RadioButton();
        this.radioButtonValdRapportTyp = new System.Windows.Forms.RadioButton();
        this.groupBoxTabellData.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetFilter)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableRegion)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableDistrikt)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableStandort)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableTrakt)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableEntreprenör)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableRapportTyp)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableArtal)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga1)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga2)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga3)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga4)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga5)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga6)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga7)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga8)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga9)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga10)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga11)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga12)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga13)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga14)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga15)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga16)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga17)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga18)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga19)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga20)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga21)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
        this.groupBox1.SuspendLayout();
        this.groupBoxFiltreringsalternativ.SuspendLayout();
        this.SuspendLayout();
        // 
        // groupBoxTabellData
        // 
        this.groupBoxTabellData.Controls.Add(this.comboBoxFråga21);
        this.groupBoxTabellData.Controls.Add(this.label21);
        this.groupBoxTabellData.Controls.Add(this.comboBoxFråga20);
        this.groupBoxTabellData.Controls.Add(this.label20);
        this.groupBoxTabellData.Controls.Add(this.comboBoxFråga15);
        this.groupBoxTabellData.Controls.Add(this.label19);
        this.groupBoxTabellData.Controls.Add(this.comboBoxFråga19);
        this.groupBoxTabellData.Controls.Add(this.comboBoxFråga18);
        this.groupBoxTabellData.Controls.Add(this.comboBoxFråga17);
        this.groupBoxTabellData.Controls.Add(this.comboBoxFråga16);
        this.groupBoxTabellData.Controls.Add(this.comboBoxFråga10);
        this.groupBoxTabellData.Controls.Add(this.comboBoxFråga14);
        this.groupBoxTabellData.Controls.Add(this.comboBoxFråga13);
        this.groupBoxTabellData.Controls.Add(this.comboBoxFråga12);
        this.groupBoxTabellData.Controls.Add(this.comboBoxFråga11);
        this.groupBoxTabellData.Controls.Add(this.label1);
        this.groupBoxTabellData.Controls.Add(this.label2);
        this.groupBoxTabellData.Controls.Add(this.label3);
        this.groupBoxTabellData.Controls.Add(this.label4);
        this.groupBoxTabellData.Controls.Add(this.label5);
        this.groupBoxTabellData.Controls.Add(this.label6);
        this.groupBoxTabellData.Controls.Add(this.label7);
        this.groupBoxTabellData.Controls.Add(this.label13);
        this.groupBoxTabellData.Controls.Add(this.label17);
        this.groupBoxTabellData.Controls.Add(this.comboBoxFråga9);
        this.groupBoxTabellData.Controls.Add(this.comboBoxFråga8);
        this.groupBoxTabellData.Controls.Add(this.comboBoxFråga7);
        this.groupBoxTabellData.Controls.Add(this.comboBoxFråga6);
        this.groupBoxTabellData.Controls.Add(this.comboBoxFråga5);
        this.groupBoxTabellData.Controls.Add(this.comboBoxFråga4);
        this.groupBoxTabellData.Controls.Add(this.comboBoxFråga3);
        this.groupBoxTabellData.Controls.Add(this.comboBoxFråga2);
        this.groupBoxTabellData.Controls.Add(this.comboBoxFråga1);
        this.groupBoxTabellData.Controls.Add(this.label14);
        this.groupBoxTabellData.Controls.Add(this.label15);
        this.groupBoxTabellData.Controls.Add(this.label16);
        this.groupBoxTabellData.Controls.Add(this.label18);
        this.groupBoxTabellData.Controls.Add(this.label8);
        this.groupBoxTabellData.Controls.Add(this.label9);
        this.groupBoxTabellData.Controls.Add(this.label10);
        this.groupBoxTabellData.Controls.Add(this.label11);
        this.groupBoxTabellData.Controls.Add(this.label12);
        this.groupBoxTabellData.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.groupBoxTabellData.Location = new System.Drawing.Point(0, 110);
        this.groupBoxTabellData.Name = "groupBoxTabellData";
        this.groupBoxTabellData.Size = new System.Drawing.Size(660, 268);
        this.groupBoxTabellData.TabIndex = 12;
        this.groupBoxTabellData.TabStop = false;
        // 
        // comboBoxFråga21
        // 
        this.comboBoxFråga21.DataSource = this.dataSetFilter;
        this.comboBoxFråga21.DisplayMember = "Fråga21.Fråga21";
        this.comboBoxFråga21.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxFråga21.FormattingEnabled = true;
        this.comboBoxFråga21.Location = new System.Drawing.Point(17, 235);
        this.comboBoxFråga21.Name = "comboBoxFråga21";
        this.comboBoxFråga21.Size = new System.Drawing.Size(116, 21);
        this.comboBoxFråga21.TabIndex = 41;
        this.comboBoxFråga21.ValueMember = "Region.Id";
        // 
        // dataSetFilter
        // 
        this.dataSetFilter.DataSetName = "DataSetFilter";
        this.dataSetFilter.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableRegion,
            this.dataTableDistrikt,
            this.dataTableStandort,
            this.dataTableTrakt,
            this.dataTableEntreprenör,
            this.dataTableRapportTyp,
            this.dataTableArtal,
            this.dataTableFråga1,
            this.dataTableFråga2,
            this.dataTableFråga3,
            this.dataTableFråga4,
            this.dataTableFråga5,
            this.dataTableFråga6,
            this.dataTableFråga7,
            this.dataTableFråga8,
            this.dataTableFråga9,
            this.dataTableFråga10,
            this.dataTableFråga11,
            this.dataTableFråga12,
            this.dataTableFråga13,
            this.dataTableFråga14,
            this.dataTableFråga15,
            this.dataTableFråga16,
            this.dataTableFråga17,
            this.dataTableFråga18,
            this.dataTableFråga19,
            this.dataTableFråga20,
            this.dataTableFråga21,
            this.dataTable1});
        // 
        // dataTableRegion
        // 
        this.dataTableRegion.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFilterRegionId,
            this.dataColumnFilterRegionNamn});
        this.dataTableRegion.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, true)});
        this.dataTableRegion.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnFilterRegionId};
        this.dataTableRegion.TableName = "Region";
        // 
        // dataColumnFilterRegionId
        // 
        this.dataColumnFilterRegionId.AllowDBNull = false;
        this.dataColumnFilterRegionId.ColumnName = "Id";
        this.dataColumnFilterRegionId.DataType = typeof(int);
        this.dataColumnFilterRegionId.ReadOnly = true;
        // 
        // dataColumnFilterRegionNamn
        // 
        this.dataColumnFilterRegionNamn.ColumnName = "Namn";
        // 
        // dataTableDistrikt
        // 
        this.dataTableDistrikt.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFilterDistriktRegionId,
            this.dataColumnFilterDistriktId,
            this.dataColumnFilterDistriktNamn});
        this.dataTableDistrikt.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, true)});
        this.dataTableDistrikt.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnFilterDistriktId};
        this.dataTableDistrikt.TableName = "Distrikt";
        // 
        // dataColumnFilterDistriktRegionId
        // 
        this.dataColumnFilterDistriktRegionId.ColumnName = "RegionId";
        this.dataColumnFilterDistriktRegionId.DataType = typeof(int);
        // 
        // dataColumnFilterDistriktId
        // 
        this.dataColumnFilterDistriktId.AllowDBNull = false;
        this.dataColumnFilterDistriktId.ColumnName = "Id";
        this.dataColumnFilterDistriktId.DataType = typeof(int);
        // 
        // dataColumnFilterDistriktNamn
        // 
        this.dataColumnFilterDistriktNamn.ColumnName = "Namn";
        // 
        // dataTableStandort
        // 
        this.dataTableStandort.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFilterStandortNamn});
        this.dataTableStandort.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, true)});
        this.dataTableStandort.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnFilterStandortNamn};
        this.dataTableStandort.TableName = "Standort";
        // 
        // dataColumnFilterStandortNamn
        // 
        this.dataColumnFilterStandortNamn.AllowDBNull = false;
        this.dataColumnFilterStandortNamn.ColumnName = "Id";
        this.dataColumnFilterStandortNamn.DataType = typeof(int);
        // 
        // dataTableTrakt
        // 
        this.dataTableTrakt.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFilterTraktId,
            this.dataColumnFilterTraktNamn});
        this.dataTableTrakt.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, true)});
        this.dataTableTrakt.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnFilterTraktId};
        this.dataTableTrakt.TableName = "Trakt";
        // 
        // dataColumnFilterTraktId
        // 
        this.dataColumnFilterTraktId.AllowDBNull = false;
        this.dataColumnFilterTraktId.ColumnName = "Id";
        this.dataColumnFilterTraktId.DataType = typeof(int);
        // 
        // dataColumnFilterTraktNamn
        // 
        this.dataColumnFilterTraktNamn.ColumnName = "Namn";
        // 
        // dataTableEntreprenör
        // 
        this.dataTableEntreprenör.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFilterEntreprenörId,
            this.dataColumnFilterEntreprenörNamn});
        this.dataTableEntreprenör.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, true)});
        this.dataTableEntreprenör.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnFilterEntreprenörId};
        this.dataTableEntreprenör.TableName = "Entreprenör";
        // 
        // dataColumnFilterEntreprenörId
        // 
        this.dataColumnFilterEntreprenörId.AllowDBNull = false;
        this.dataColumnFilterEntreprenörId.ColumnName = "Id";
        // 
        // dataColumnFilterEntreprenörNamn
        // 
        this.dataColumnFilterEntreprenörNamn.ColumnName = "Namn";
        // 
        // dataTableRapportTyp
        // 
        this.dataTableRapportTyp.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFilterRapportTypId,
            this.dataColumnFilterRapportTypNamn});
        this.dataTableRapportTyp.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, true)});
        this.dataTableRapportTyp.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnFilterRapportTypId};
        this.dataTableRapportTyp.TableName = "RapportTyp";
        // 
        // dataColumnFilterRapportTypId
        // 
        this.dataColumnFilterRapportTypId.AllowDBNull = false;
        this.dataColumnFilterRapportTypId.ColumnName = "Id";
        this.dataColumnFilterRapportTypId.DataType = typeof(int);
        // 
        // dataColumnFilterRapportTypNamn
        // 
        this.dataColumnFilterRapportTypNamn.ColumnName = "Namn";
        // 
        // dataTableArtal
        // 
        this.dataTableArtal.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFilterÅrtalId});
        this.dataTableArtal.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, true)});
        this.dataTableArtal.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnFilterÅrtalId};
        this.dataTableArtal.TableName = "Artal";
        // 
        // dataColumnFilterÅrtalId
        // 
        this.dataColumnFilterÅrtalId.AllowDBNull = false;
        this.dataColumnFilterÅrtalId.ColumnName = "Id";
        this.dataColumnFilterÅrtalId.DataType = typeof(int);
        // 
        // dataTableFråga1
        // 
        this.dataTableFråga1.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSvar1});
        this.dataTableFråga1.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Svar"}, true)});
        this.dataTableFråga1.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnSvar1};
        this.dataTableFråga1.TableName = "Fråga1";
        // 
        // dataColumnSvar1
        // 
        this.dataColumnSvar1.AllowDBNull = false;
        this.dataColumnSvar1.ColumnName = "Svar";
        // 
        // dataTableFråga2
        // 
        this.dataTableFråga2.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSvar2});
        this.dataTableFråga2.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Svar"}, true)});
        this.dataTableFråga2.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnSvar2};
        this.dataTableFråga2.TableName = "Fråga2";
        // 
        // dataColumnSvar2
        // 
        this.dataColumnSvar2.AllowDBNull = false;
        this.dataColumnSvar2.ColumnName = "Svar";
        // 
        // dataTableFråga3
        // 
        this.dataTableFråga3.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSvar3});
        this.dataTableFråga3.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Svar3"}, true)});
        this.dataTableFråga3.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnSvar3};
        this.dataTableFråga3.TableName = "Fråga3";
        // 
        // dataColumnSvar3
        // 
        this.dataColumnSvar3.AllowDBNull = false;
        this.dataColumnSvar3.ColumnName = "Svar3";
        // 
        // dataTableFråga4
        // 
        this.dataTableFråga4.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSvar4});
        this.dataTableFråga4.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Svar4"}, true)});
        this.dataTableFråga4.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnSvar4};
        this.dataTableFråga4.TableName = "Fråga4";
        // 
        // dataColumnSvar4
        // 
        this.dataColumnSvar4.AllowDBNull = false;
        this.dataColumnSvar4.ColumnName = "Svar4";
        // 
        // dataTableFråga5
        // 
        this.dataTableFråga5.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSvar5});
        this.dataTableFråga5.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Svar5"}, true)});
        this.dataTableFråga5.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnSvar5};
        this.dataTableFråga5.TableName = "Fråga5";
        // 
        // dataColumnSvar5
        // 
        this.dataColumnSvar5.AllowDBNull = false;
        this.dataColumnSvar5.ColumnName = "Svar5";
        // 
        // dataTableFråga6
        // 
        this.dataTableFråga6.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSvar6});
        this.dataTableFråga6.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Svar6"}, true)});
        this.dataTableFråga6.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnSvar6};
        this.dataTableFråga6.TableName = "Fråga6";
        // 
        // dataColumnSvar6
        // 
        this.dataColumnSvar6.AllowDBNull = false;
        this.dataColumnSvar6.ColumnName = "Svar6";
        // 
        // dataTableFråga7
        // 
        this.dataTableFråga7.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSvar7});
        this.dataTableFråga7.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Svar7"}, true)});
        this.dataTableFråga7.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnSvar7};
        this.dataTableFråga7.TableName = "Fråga7";
        // 
        // dataColumnSvar7
        // 
        this.dataColumnSvar7.AllowDBNull = false;
        this.dataColumnSvar7.ColumnName = "Svar7";
        // 
        // dataTableFråga8
        // 
        this.dataTableFråga8.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSvar8});
        this.dataTableFråga8.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Svar8"}, true)});
        this.dataTableFråga8.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnSvar8};
        this.dataTableFråga8.TableName = "Fråga8";
        // 
        // dataColumnSvar8
        // 
        this.dataColumnSvar8.AllowDBNull = false;
        this.dataColumnSvar8.ColumnName = "Svar8";
        // 
        // dataTableFråga9
        // 
        this.dataTableFråga9.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSvar9});
        this.dataTableFråga9.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Svar9"}, true)});
        this.dataTableFråga9.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnSvar9};
        this.dataTableFråga9.TableName = "Fråga9";
        // 
        // dataColumnSvar9
        // 
        this.dataColumnSvar9.AllowDBNull = false;
        this.dataColumnSvar9.ColumnName = "Svar9";
        // 
        // dataTableFråga10
        // 
        this.dataTableFråga10.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSvar10});
        this.dataTableFråga10.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Svar10"}, true)});
        this.dataTableFråga10.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnSvar10};
        this.dataTableFråga10.TableName = "Fråga10";
        // 
        // dataColumnSvar10
        // 
        this.dataColumnSvar10.AllowDBNull = false;
        this.dataColumnSvar10.ColumnName = "Svar10";
        // 
        // dataTableFråga11
        // 
        this.dataTableFråga11.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSvar11});
        this.dataTableFråga11.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Svar11"}, true)});
        this.dataTableFråga11.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnSvar11};
        this.dataTableFråga11.TableName = "Fråga11";
        // 
        // dataColumnSvar11
        // 
        this.dataColumnSvar11.AllowDBNull = false;
        this.dataColumnSvar11.ColumnName = "Svar11";
        // 
        // dataTableFråga12
        // 
        this.dataTableFråga12.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSvar12});
        this.dataTableFråga12.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Svar12"}, true)});
        this.dataTableFråga12.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnSvar12};
        this.dataTableFråga12.TableName = "Fråga12";
        // 
        // dataColumnSvar12
        // 
        this.dataColumnSvar12.AllowDBNull = false;
        this.dataColumnSvar12.ColumnName = "Svar12";
        // 
        // dataTableFråga13
        // 
        this.dataTableFråga13.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSvar13});
        this.dataTableFråga13.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Svar13"}, true)});
        this.dataTableFråga13.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnSvar13};
        this.dataTableFråga13.TableName = "Fråga13";
        // 
        // dataColumnSvar13
        // 
        this.dataColumnSvar13.AllowDBNull = false;
        this.dataColumnSvar13.ColumnName = "Svar13";
        // 
        // dataTableFråga14
        // 
        this.dataTableFråga14.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFråga14});
        this.dataTableFråga14.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Fråga14"}, true)});
        this.dataTableFråga14.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnFråga14};
        this.dataTableFråga14.TableName = "Fråga14";
        // 
        // dataColumnFråga14
        // 
        this.dataColumnFråga14.AllowDBNull = false;
        this.dataColumnFråga14.ColumnName = "Fråga14";
        // 
        // dataTableFråga15
        // 
        this.dataTableFråga15.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFråga15});
        this.dataTableFråga15.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Fråga15"}, true)});
        this.dataTableFråga15.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnFråga15};
        this.dataTableFråga15.TableName = "Fråga15";
        // 
        // dataColumnFråga15
        // 
        this.dataColumnFråga15.AllowDBNull = false;
        this.dataColumnFråga15.ColumnName = "Fråga15";
        // 
        // dataTableFråga16
        // 
        this.dataTableFråga16.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFråga16});
        this.dataTableFråga16.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Fråga16"}, true)});
        this.dataTableFråga16.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnFråga16};
        this.dataTableFråga16.TableName = "Fråga16";
        // 
        // dataColumnFråga16
        // 
        this.dataColumnFråga16.AllowDBNull = false;
        this.dataColumnFråga16.ColumnName = "Fråga16";
        // 
        // dataTableFråga17
        // 
        this.dataTableFråga17.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFråga17});
        this.dataTableFråga17.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Fråga17"}, true)});
        this.dataTableFråga17.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnFråga17};
        this.dataTableFråga17.TableName = "Fråga17";
        // 
        // dataColumnFråga17
        // 
        this.dataColumnFråga17.AllowDBNull = false;
        this.dataColumnFråga17.ColumnName = "Fråga17";
        // 
        // dataTableFråga18
        // 
        this.dataTableFråga18.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFråga18});
        this.dataTableFråga18.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Fråga18"}, true)});
        this.dataTableFråga18.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnFråga18};
        this.dataTableFråga18.TableName = "Fråga18";
        // 
        // dataColumnFråga18
        // 
        this.dataColumnFråga18.AllowDBNull = false;
        this.dataColumnFråga18.ColumnName = "Fråga18";
        // 
        // dataTableFråga19
        // 
        this.dataTableFråga19.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFråga19});
        this.dataTableFråga19.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Fråga19"}, true)});
        this.dataTableFråga19.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnFråga19};
        this.dataTableFråga19.TableName = "Fråga19";
        // 
        // dataColumnFråga19
        // 
        this.dataColumnFråga19.AllowDBNull = false;
        this.dataColumnFråga19.ColumnName = "Fråga19";
        // 
        // dataTableFråga20
        // 
        this.dataTableFråga20.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFråga20});
        this.dataTableFråga20.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Fråga20"}, true)});
        this.dataTableFråga20.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnFråga20};
        this.dataTableFråga20.TableName = "Fråga20";
        // 
        // dataColumnFråga20
        // 
        this.dataColumnFråga20.AllowDBNull = false;
        this.dataColumnFråga20.ColumnName = "Fråga20";
        // 
        // dataTableFråga21
        // 
        this.dataTableFråga21.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFråga21});
        this.dataTableFråga21.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Fråga21"}, true)});
        this.dataTableFråga21.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnFråga21};
        this.dataTableFråga21.TableName = "Fråga21";
        // 
        // dataColumnFråga21
        // 
        this.dataColumnFråga21.AllowDBNull = false;
        this.dataColumnFråga21.ColumnName = "Fråga21";
        // 
        // dataTable1
        // 
        this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumndataSetFilterInvTypId,
            this.dataColumndataSetFilterInvTypNamn});
        this.dataTable1.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, true)});
        this.dataTable1.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumndataSetFilterInvTypId};
        this.dataTable1.TableName = "InvTyp";
        // 
        // dataColumndataSetFilterInvTypId
        // 
        this.dataColumndataSetFilterInvTypId.AllowDBNull = false;
        this.dataColumndataSetFilterInvTypId.ColumnName = "Id";
        this.dataColumndataSetFilterInvTypId.DataType = typeof(int);
        this.dataColumndataSetFilterInvTypId.ReadOnly = true;
        // 
        // dataColumndataSetFilterInvTypNamn
        // 
        this.dataColumndataSetFilterInvTypNamn.ColumnName = "Namn";
        // 
        // label21
        // 
        this.label21.AutoSize = true;
        this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label21.Location = new System.Drawing.Point(14, 219);
        this.label21.Name = "label21";
        this.label21.Size = new System.Drawing.Size(59, 13);
        this.label21.TabIndex = 40;
        this.label21.Text = "Fråga 21:";
        // 
        // comboBoxFråga20
        // 
        this.comboBoxFråga20.DataSource = this.dataSetFilter;
        this.comboBoxFråga20.DisplayMember = "Fråga20.Fråga20";
        this.comboBoxFråga20.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxFråga20.FormattingEnabled = true;
        this.comboBoxFråga20.Location = new System.Drawing.Point(528, 185);
        this.comboBoxFråga20.Name = "comboBoxFråga20";
        this.comboBoxFråga20.Size = new System.Drawing.Size(116, 21);
        this.comboBoxFråga20.TabIndex = 39;
        this.comboBoxFråga20.ValueMember = "Region.Id";
        // 
        // label20
        // 
        this.label20.AutoSize = true;
        this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label20.Location = new System.Drawing.Point(525, 169);
        this.label20.Name = "label20";
        this.label20.Size = new System.Drawing.Size(59, 13);
        this.label20.TabIndex = 38;
        this.label20.Text = "Fråga 20:";
        // 
        // comboBoxFråga15
        // 
        this.comboBoxFråga15.DataSource = this.dataSetFilter;
        this.comboBoxFråga15.DisplayMember = "Fråga15.Fråga15";
        this.comboBoxFråga15.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxFråga15.FormattingEnabled = true;
        this.comboBoxFråga15.Location = new System.Drawing.Point(528, 136);
        this.comboBoxFråga15.Name = "comboBoxFråga15";
        this.comboBoxFråga15.Size = new System.Drawing.Size(116, 21);
        this.comboBoxFråga15.TabIndex = 29;
        this.comboBoxFråga15.ValueMember = "Region.Id";
        // 
        // label19
        // 
        this.label19.AutoSize = true;
        this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label19.Location = new System.Drawing.Point(525, 120);
        this.label19.Name = "label19";
        this.label19.Size = new System.Drawing.Size(59, 13);
        this.label19.TabIndex = 28;
        this.label19.Text = "Fråga 15:";
        // 
        // comboBoxFråga19
        // 
        this.comboBoxFråga19.DataSource = this.dataSetFilter;
        this.comboBoxFråga19.DisplayMember = "Fråga19.Fråga19";
        this.comboBoxFråga19.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxFråga19.FormattingEnabled = true;
        this.comboBoxFråga19.Location = new System.Drawing.Point(400, 185);
        this.comboBoxFråga19.Name = "comboBoxFråga19";
        this.comboBoxFråga19.Size = new System.Drawing.Size(116, 21);
        this.comboBoxFråga19.TabIndex = 37;
        this.comboBoxFråga19.ValueMember = "Region.Id";
        // 
        // comboBoxFråga18
        // 
        this.comboBoxFråga18.DataSource = this.dataSetFilter;
        this.comboBoxFråga18.DisplayMember = "Fråga18.Fråga18";
        this.comboBoxFråga18.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxFråga18.FormattingEnabled = true;
        this.comboBoxFråga18.Location = new System.Drawing.Point(272, 185);
        this.comboBoxFråga18.Name = "comboBoxFråga18";
        this.comboBoxFråga18.Size = new System.Drawing.Size(116, 21);
        this.comboBoxFråga18.TabIndex = 35;
        this.comboBoxFråga18.ValueMember = "Region.Id";
        // 
        // comboBoxFråga17
        // 
        this.comboBoxFråga17.DataSource = this.dataSetFilter;
        this.comboBoxFråga17.DisplayMember = "Fråga17.Fråga17";
        this.comboBoxFråga17.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxFråga17.FormattingEnabled = true;
        this.comboBoxFråga17.Location = new System.Drawing.Point(145, 185);
        this.comboBoxFråga17.Name = "comboBoxFråga17";
        this.comboBoxFråga17.Size = new System.Drawing.Size(116, 21);
        this.comboBoxFråga17.TabIndex = 33;
        this.comboBoxFråga17.ValueMember = "Region.Id";
        // 
        // comboBoxFråga16
        // 
        this.comboBoxFråga16.DataSource = this.dataSetFilter;
        this.comboBoxFråga16.DisplayMember = "Fråga16.Fråga16";
        this.comboBoxFråga16.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxFråga16.FormattingEnabled = true;
        this.comboBoxFråga16.Location = new System.Drawing.Point(16, 185);
        this.comboBoxFråga16.Name = "comboBoxFråga16";
        this.comboBoxFråga16.Size = new System.Drawing.Size(116, 21);
        this.comboBoxFråga16.TabIndex = 31;
        this.comboBoxFråga16.ValueMember = "Region.Id";
        // 
        // comboBoxFråga10
        // 
        this.comboBoxFråga10.DataSource = this.dataSetFilter;
        this.comboBoxFråga10.DisplayMember = "Fråga10.Svar10";
        this.comboBoxFråga10.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxFråga10.FormattingEnabled = true;
        this.comboBoxFråga10.Location = new System.Drawing.Point(530, 85);
        this.comboBoxFråga10.Name = "comboBoxFråga10";
        this.comboBoxFråga10.Size = new System.Drawing.Size(116, 21);
        this.comboBoxFråga10.TabIndex = 19;
        this.comboBoxFråga10.ValueMember = "Region.Id";
        // 
        // comboBoxFråga14
        // 
        this.comboBoxFråga14.DataSource = this.dataSetFilter;
        this.comboBoxFråga14.DisplayMember = "Fråga14.Fråga14";
        this.comboBoxFråga14.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxFråga14.FormattingEnabled = true;
        this.comboBoxFråga14.Location = new System.Drawing.Point(400, 136);
        this.comboBoxFråga14.Name = "comboBoxFråga14";
        this.comboBoxFråga14.Size = new System.Drawing.Size(116, 21);
        this.comboBoxFråga14.TabIndex = 27;
        this.comboBoxFråga14.ValueMember = "Region.Id";
        // 
        // comboBoxFråga13
        // 
        this.comboBoxFråga13.DataSource = this.dataSetFilter;
        this.comboBoxFråga13.DisplayMember = "Fråga13.Svar13";
        this.comboBoxFråga13.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxFråga13.FormattingEnabled = true;
        this.comboBoxFråga13.Location = new System.Drawing.Point(272, 136);
        this.comboBoxFråga13.Name = "comboBoxFråga13";
        this.comboBoxFråga13.Size = new System.Drawing.Size(116, 21);
        this.comboBoxFråga13.TabIndex = 25;
        this.comboBoxFråga13.ValueMember = "Region.Id";
        // 
        // comboBoxFråga12
        // 
        this.comboBoxFråga12.DataSource = this.dataSetFilter;
        this.comboBoxFråga12.DisplayMember = "Fråga12.Svar12";
        this.comboBoxFråga12.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxFråga12.FormattingEnabled = true;
        this.comboBoxFråga12.Location = new System.Drawing.Point(145, 136);
        this.comboBoxFråga12.Name = "comboBoxFråga12";
        this.comboBoxFråga12.Size = new System.Drawing.Size(116, 21);
        this.comboBoxFråga12.TabIndex = 23;
        this.comboBoxFråga12.ValueMember = "Region.Id";
        // 
        // comboBoxFråga11
        // 
        this.comboBoxFråga11.DataSource = this.dataSetFilter;
        this.comboBoxFråga11.DisplayMember = "Fråga11.Svar11";
        this.comboBoxFråga11.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxFråga11.FormattingEnabled = true;
        this.comboBoxFråga11.Location = new System.Drawing.Point(15, 136);
        this.comboBoxFråga11.Name = "comboBoxFråga11";
        this.comboBoxFråga11.Size = new System.Drawing.Size(116, 21);
        this.comboBoxFråga11.TabIndex = 21;
        this.comboBoxFråga11.ValueMember = "Region.Id";
        // 
        // label1
        // 
        this.label1.AutoSize = true;
        this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label1.Location = new System.Drawing.Point(397, 169);
        this.label1.Name = "label1";
        this.label1.Size = new System.Drawing.Size(59, 13);
        this.label1.TabIndex = 36;
        this.label1.Text = "Fråga 19:";
        // 
        // label2
        // 
        this.label2.AutoSize = true;
        this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label2.Location = new System.Drawing.Point(268, 169);
        this.label2.Name = "label2";
        this.label2.Size = new System.Drawing.Size(59, 13);
        this.label2.TabIndex = 34;
        this.label2.Text = "Fråga 18:";
        // 
        // label3
        // 
        this.label3.AutoSize = true;
        this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label3.Location = new System.Drawing.Point(141, 169);
        this.label3.Name = "label3";
        this.label3.Size = new System.Drawing.Size(59, 13);
        this.label3.TabIndex = 32;
        this.label3.Text = "Fråga 17:";
        // 
        // label4
        // 
        this.label4.AutoSize = true;
        this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label4.Location = new System.Drawing.Point(15, 169);
        this.label4.Name = "label4";
        this.label4.Size = new System.Drawing.Size(59, 13);
        this.label4.TabIndex = 30;
        this.label4.Text = "Fråga 16:";
        // 
        // label5
        // 
        this.label5.AutoSize = true;
        this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label5.Location = new System.Drawing.Point(527, 69);
        this.label5.Name = "label5";
        this.label5.Size = new System.Drawing.Size(59, 13);
        this.label5.TabIndex = 18;
        this.label5.Text = "Fråga 10:";
        // 
        // label6
        // 
        this.label6.AutoSize = true;
        this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label6.Location = new System.Drawing.Point(397, 120);
        this.label6.Name = "label6";
        this.label6.Size = new System.Drawing.Size(59, 13);
        this.label6.TabIndex = 26;
        this.label6.Text = "Fråga 14:";
        // 
        // label7
        // 
        this.label7.AutoSize = true;
        this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label7.Location = new System.Drawing.Point(268, 120);
        this.label7.Name = "label7";
        this.label7.Size = new System.Drawing.Size(59, 13);
        this.label7.TabIndex = 24;
        this.label7.Text = "Fråga 13:";
        // 
        // label13
        // 
        this.label13.AutoSize = true;
        this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label13.Location = new System.Drawing.Point(141, 120);
        this.label13.Name = "label13";
        this.label13.Size = new System.Drawing.Size(59, 13);
        this.label13.TabIndex = 22;
        this.label13.Text = "Fråga 12:";
        // 
        // label17
        // 
        this.label17.AutoSize = true;
        this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label17.Location = new System.Drawing.Point(14, 120);
        this.label17.Name = "label17";
        this.label17.Size = new System.Drawing.Size(59, 13);
        this.label17.TabIndex = 20;
        this.label17.Text = "Fråga 11:";
        // 
        // comboBoxFråga9
        // 
        this.comboBoxFråga9.DataSource = this.dataSetFilter;
        this.comboBoxFråga9.DisplayMember = "Fråga9.Svar9";
        this.comboBoxFråga9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxFråga9.FormattingEnabled = true;
        this.comboBoxFråga9.Location = new System.Drawing.Point(400, 85);
        this.comboBoxFråga9.Name = "comboBoxFråga9";
        this.comboBoxFråga9.Size = new System.Drawing.Size(116, 21);
        this.comboBoxFråga9.TabIndex = 17;
        this.comboBoxFråga9.ValueMember = "Fråga9.Svar9";
        // 
        // comboBoxFråga8
        // 
        this.comboBoxFråga8.DataSource = this.dataSetFilter;
        this.comboBoxFråga8.DisplayMember = "Fråga8.Svar8";
        this.comboBoxFråga8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxFråga8.FormattingEnabled = true;
        this.comboBoxFråga8.Location = new System.Drawing.Point(272, 85);
        this.comboBoxFråga8.Name = "comboBoxFråga8";
        this.comboBoxFråga8.Size = new System.Drawing.Size(116, 21);
        this.comboBoxFråga8.TabIndex = 15;
        this.comboBoxFråga8.ValueMember = "Fråga8.Svar8";
        // 
        // comboBoxFråga7
        // 
        this.comboBoxFråga7.DataSource = this.dataSetFilter;
        this.comboBoxFråga7.DisplayMember = "Fråga7.Svar7";
        this.comboBoxFråga7.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxFråga7.FormattingEnabled = true;
        this.comboBoxFråga7.Location = new System.Drawing.Point(145, 85);
        this.comboBoxFråga7.Name = "comboBoxFråga7";
        this.comboBoxFråga7.Size = new System.Drawing.Size(116, 21);
        this.comboBoxFråga7.TabIndex = 13;
        this.comboBoxFråga7.ValueMember = "Fråga7.Svar7";
        // 
        // comboBoxFråga6
        // 
        this.comboBoxFråga6.DataSource = this.dataSetFilter;
        this.comboBoxFråga6.DisplayMember = "Fråga6.Svar6";
        this.comboBoxFråga6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxFråga6.FormattingEnabled = true;
        this.comboBoxFråga6.Location = new System.Drawing.Point(16, 85);
        this.comboBoxFråga6.Name = "comboBoxFråga6";
        this.comboBoxFråga6.Size = new System.Drawing.Size(116, 21);
        this.comboBoxFråga6.TabIndex = 11;
        this.comboBoxFråga6.ValueMember = "Fråga6.Svar6";
        // 
        // comboBoxFråga5
        // 
        this.comboBoxFråga5.DataSource = this.dataSetFilter;
        this.comboBoxFråga5.DisplayMember = "Fråga5.Svar5";
        this.comboBoxFråga5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxFråga5.FormattingEnabled = true;
        this.comboBoxFråga5.Location = new System.Drawing.Point(528, 36);
        this.comboBoxFråga5.Name = "comboBoxFråga5";
        this.comboBoxFråga5.Size = new System.Drawing.Size(116, 21);
        this.comboBoxFråga5.TabIndex = 9;
        this.comboBoxFråga5.ValueMember = "Fråga5.Svar5";
        // 
        // comboBoxFråga4
        // 
        this.comboBoxFråga4.DataSource = this.dataSetFilter;
        this.comboBoxFråga4.DisplayMember = "Fråga4.Svar4";
        this.comboBoxFråga4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxFråga4.FormattingEnabled = true;
        this.comboBoxFråga4.Location = new System.Drawing.Point(400, 36);
        this.comboBoxFråga4.Name = "comboBoxFråga4";
        this.comboBoxFråga4.Size = new System.Drawing.Size(116, 21);
        this.comboBoxFråga4.TabIndex = 7;
        this.comboBoxFråga4.ValueMember = "Fråga4.Svar4";
        // 
        // comboBoxFråga3
        // 
        this.comboBoxFråga3.DataSource = this.dataSetFilter;
        this.comboBoxFråga3.DisplayMember = "Fråga3.Svar3";
        this.comboBoxFråga3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxFråga3.FormattingEnabled = true;
        this.comboBoxFråga3.Location = new System.Drawing.Point(272, 36);
        this.comboBoxFråga3.Name = "comboBoxFråga3";
        this.comboBoxFråga3.Size = new System.Drawing.Size(116, 21);
        this.comboBoxFråga3.TabIndex = 5;
        this.comboBoxFråga3.ValueMember = "Fråga3.Svar3";
        // 
        // comboBoxFråga2
        // 
        this.comboBoxFråga2.DataSource = this.dataSetFilter;
        this.comboBoxFråga2.DisplayMember = "Fråga2.Svar";
        this.comboBoxFråga2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxFråga2.FormattingEnabled = true;
        this.comboBoxFråga2.Location = new System.Drawing.Point(145, 36);
        this.comboBoxFråga2.Name = "comboBoxFråga2";
        this.comboBoxFråga2.Size = new System.Drawing.Size(116, 21);
        this.comboBoxFråga2.TabIndex = 3;
        this.comboBoxFråga2.ValueMember = "Fråga2.Svar";
        // 
        // comboBoxFråga1
        // 
        this.comboBoxFråga1.DataSource = this.dataSetFilter;
        this.comboBoxFråga1.DisplayMember = "Fråga1.Svar";
        this.comboBoxFråga1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxFråga1.FormattingEnabled = true;
        this.comboBoxFråga1.Location = new System.Drawing.Point(15, 36);
        this.comboBoxFråga1.Name = "comboBoxFråga1";
        this.comboBoxFråga1.Size = new System.Drawing.Size(116, 21);
        this.comboBoxFråga1.TabIndex = 1;
        this.comboBoxFråga1.ValueMember = "Fråga1.Svar";
        // 
        // label14
        // 
        this.label14.AutoSize = true;
        this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label14.Location = new System.Drawing.Point(397, 69);
        this.label14.Name = "label14";
        this.label14.Size = new System.Drawing.Size(52, 13);
        this.label14.TabIndex = 16;
        this.label14.Text = "Fråga 9:";
        // 
        // label15
        // 
        this.label15.AutoSize = true;
        this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label15.Location = new System.Drawing.Point(268, 69);
        this.label15.Name = "label15";
        this.label15.Size = new System.Drawing.Size(52, 13);
        this.label15.TabIndex = 14;
        this.label15.Text = "Fråga 8:";
        // 
        // label16
        // 
        this.label16.AutoSize = true;
        this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label16.Location = new System.Drawing.Point(141, 69);
        this.label16.Name = "label16";
        this.label16.Size = new System.Drawing.Size(52, 13);
        this.label16.TabIndex = 12;
        this.label16.Text = "Fråga 7:";
        // 
        // label18
        // 
        this.label18.AutoSize = true;
        this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label18.Location = new System.Drawing.Point(15, 69);
        this.label18.Name = "label18";
        this.label18.Size = new System.Drawing.Size(52, 13);
        this.label18.TabIndex = 10;
        this.label18.Text = "Fråga 6:";
        // 
        // label8
        // 
        this.label8.AutoSize = true;
        this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label8.Location = new System.Drawing.Point(525, 20);
        this.label8.Name = "label8";
        this.label8.Size = new System.Drawing.Size(52, 13);
        this.label8.TabIndex = 8;
        this.label8.Text = "Fråga 5:";
        // 
        // label9
        // 
        this.label9.AutoSize = true;
        this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label9.Location = new System.Drawing.Point(397, 20);
        this.label9.Name = "label9";
        this.label9.Size = new System.Drawing.Size(52, 13);
        this.label9.TabIndex = 6;
        this.label9.Text = "Fråga 4:";
        // 
        // label10
        // 
        this.label10.AutoSize = true;
        this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label10.Location = new System.Drawing.Point(268, 20);
        this.label10.Name = "label10";
        this.label10.Size = new System.Drawing.Size(52, 13);
        this.label10.TabIndex = 4;
        this.label10.Text = "Fråga 3:";
        // 
        // label11
        // 
        this.label11.AutoSize = true;
        this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label11.Location = new System.Drawing.Point(141, 20);
        this.label11.Name = "label11";
        this.label11.Size = new System.Drawing.Size(52, 13);
        this.label11.TabIndex = 2;
        this.label11.Text = "Fråga 2:";
        // 
        // label12
        // 
        this.label12.AutoSize = true;
        this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label12.Location = new System.Drawing.Point(14, 20);
        this.label12.Name = "label12";
        this.label12.Size = new System.Drawing.Size(52, 13);
        this.label12.TabIndex = 0;
        this.label12.Text = "Fråga 1:";
        // 
        // groupBox1
        // 
        this.groupBox1.Controls.Add(this.comboBoxInvTypId);
        this.groupBox1.Controls.Add(this.label22);
        this.groupBox1.Controls.Add(this.buttonTömFilter);
        this.groupBox1.Controls.Add(this.groupBoxTabellData);
        this.groupBox1.Controls.Add(this.comboBoxÅrtal);
        this.groupBox1.Controls.Add(this.buttonAvbryt);
        this.groupBox1.Controls.Add(this.buttonUppdatera);
        this.groupBox1.Controls.Add(this.comboBoxStåndortId);
        this.groupBox1.Controls.Add(this.comboBoxEntreprenörId);
        this.groupBox1.Controls.Add(this.comboBoxTraktNr);
        this.groupBox1.Controls.Add(this.comboBoxDistriktId);
        this.groupBox1.Controls.Add(this.comboBoxRegionId);
        this.groupBox1.Controls.Add(this.label33);
        this.groupBox1.Controls.Add(this.label34);
        this.groupBox1.Controls.Add(this.label35);
        this.groupBox1.Controls.Add(this.label36);
        this.groupBox1.Controls.Add(this.label37);
        this.groupBox1.Controls.Add(this.label38);
        this.groupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.groupBox1.Location = new System.Drawing.Point(8, 51);
        this.groupBox1.Name = "groupBox1";
        this.groupBox1.Size = new System.Drawing.Size(660, 429);
        this.groupBox1.TabIndex = 0;
        this.groupBox1.TabStop = false;
        // 
        // comboBoxInvTypId
        // 
        this.comboBoxInvTypId.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.dataSetFilter, "InvTyp.Id", true));
        this.comboBoxInvTypId.DataSource = this.dataSetFilter;
        this.comboBoxInvTypId.DisplayMember = "InvTyp.Namn";
        this.comboBoxInvTypId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxInvTypId.FormattingEnabled = true;
        this.comboBoxInvTypId.Location = new System.Drawing.Point(146, 81);
        this.comboBoxInvTypId.Name = "comboBoxInvTypId";
        this.comboBoxInvTypId.Size = new System.Drawing.Size(116, 21);
        this.comboBoxInvTypId.TabIndex = 17;
        this.comboBoxInvTypId.ValueMember = "InvTyp.Id";
        // 
        // label22
        // 
        this.label22.AutoSize = true;
        this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label22.Location = new System.Drawing.Point(144, 63);
        this.label22.Name = "label22";
        this.label22.Size = new System.Drawing.Size(48, 13);
        this.label22.TabIndex = 16;
        this.label22.Text = "Invtyp:";
        // 
        // buttonTömFilter
        // 
        this.buttonTömFilter.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.buttonTömFilter.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonTömFilter.ImageIndex = 1;
        this.buttonTömFilter.ImageList = this.imageListFilter;
        this.buttonTömFilter.Location = new System.Drawing.Point(266, 388);
        this.buttonTömFilter.Name = "buttonTömFilter";
        this.buttonTömFilter.Size = new System.Drawing.Size(144, 30);
        this.buttonTömFilter.TabIndex = 14;
        this.buttonTömFilter.Text = "Töm alla filter";
        this.buttonTömFilter.UseVisualStyleBackColor = true;
        this.buttonTömFilter.Click += new System.EventHandler(this.buttonTömFilter_Click);
        // 
        // imageListFilter
        // 
        this.imageListFilter.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListFilter.ImageStream")));
        this.imageListFilter.TransparentColor = System.Drawing.Color.Transparent;
        this.imageListFilter.Images.SetKeyName(0, "UppdateraFilter.ico");
        this.imageListFilter.Images.SetKeyName(1, "TömFilter.ico");
        this.imageListFilter.Images.SetKeyName(2, "Neka.ico");
        // 
        // comboBoxÅrtal
        // 
        this.comboBoxÅrtal.DataSource = this.dataSetFilter;
        this.comboBoxÅrtal.DisplayMember = "Artal.Id";
        this.comboBoxÅrtal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxÅrtal.FormattingEnabled = true;
        this.comboBoxÅrtal.Location = new System.Drawing.Point(19, 82);
        this.comboBoxÅrtal.Name = "comboBoxÅrtal";
        this.comboBoxÅrtal.Size = new System.Drawing.Size(116, 21);
        this.comboBoxÅrtal.TabIndex = 11;
        this.comboBoxÅrtal.ValueMember = "Artal.Id";
        // 
        // buttonAvbryt
        // 
        this.buttonAvbryt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.buttonAvbryt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonAvbryt.ImageIndex = 2;
        this.buttonAvbryt.ImageList = this.imageListFilter;
        this.buttonAvbryt.Location = new System.Drawing.Point(502, 388);
        this.buttonAvbryt.Name = "buttonAvbryt";
        this.buttonAvbryt.Size = new System.Drawing.Size(144, 30);
        this.buttonAvbryt.TabIndex = 15;
        this.buttonAvbryt.Text = "Avbryt";
        this.buttonAvbryt.UseVisualStyleBackColor = true;
        this.buttonAvbryt.Click += new System.EventHandler(this.buttonAvbryt_Click);
        // 
        // buttonUppdatera
        // 
        this.buttonUppdatera.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.buttonUppdatera.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonUppdatera.ImageIndex = 0;
        this.buttonUppdatera.ImageList = this.imageListFilter;
        this.buttonUppdatera.Location = new System.Drawing.Point(15, 388);
        this.buttonUppdatera.Name = "buttonUppdatera";
        this.buttonUppdatera.Size = new System.Drawing.Size(144, 30);
        this.buttonUppdatera.TabIndex = 13;
        this.buttonUppdatera.Text = "Uppdatera tabell";
        this.buttonUppdatera.UseVisualStyleBackColor = true;
        this.buttonUppdatera.Click += new System.EventHandler(this.buttonUppdatera_Click);
        // 
        // comboBoxStåndortId
        // 
        this.comboBoxStåndortId.DataSource = this.dataSetFilter;
        this.comboBoxStåndortId.DisplayMember = "Standort.Id";
        this.comboBoxStåndortId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxStåndortId.FormattingEnabled = true;
        this.comboBoxStåndortId.Location = new System.Drawing.Point(530, 34);
        this.comboBoxStåndortId.Name = "comboBoxStåndortId";
        this.comboBoxStåndortId.Size = new System.Drawing.Size(116, 21);
        this.comboBoxStåndortId.TabIndex = 9;
        this.comboBoxStåndortId.ValueMember = "Standort.Id";
        // 
        // comboBoxEntreprenörId
        // 
        this.comboBoxEntreprenörId.DataSource = this.dataSetFilter;
        this.comboBoxEntreprenörId.DisplayMember = "Entreprenör.Id";
        this.comboBoxEntreprenörId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxEntreprenörId.FormattingEnabled = true;
        this.comboBoxEntreprenörId.Location = new System.Drawing.Point(401, 34);
        this.comboBoxEntreprenörId.Name = "comboBoxEntreprenörId";
        this.comboBoxEntreprenörId.Size = new System.Drawing.Size(116, 21);
        this.comboBoxEntreprenörId.TabIndex = 7;
        this.comboBoxEntreprenörId.ValueMember = "Entreprenör.Id";
        // 
        // comboBoxTraktNr
        // 
        this.comboBoxTraktNr.DataSource = this.dataSetFilter;
        this.comboBoxTraktNr.DisplayMember = "Trakt.Id";
        this.comboBoxTraktNr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxTraktNr.FormatString = "000000";
        this.comboBoxTraktNr.FormattingEnabled = true;
        this.comboBoxTraktNr.Location = new System.Drawing.Point(273, 34);
        this.comboBoxTraktNr.Name = "comboBoxTraktNr";
        this.comboBoxTraktNr.Size = new System.Drawing.Size(116, 21);
        this.comboBoxTraktNr.TabIndex = 5;
        this.comboBoxTraktNr.ValueMember = "Trakt.Id";
        // 
        // comboBoxDistriktId
        // 
        this.comboBoxDistriktId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxDistriktId.FormattingEnabled = true;
        this.comboBoxDistriktId.Location = new System.Drawing.Point(146, 34);
        this.comboBoxDistriktId.Name = "comboBoxDistriktId";
        this.comboBoxDistriktId.Size = new System.Drawing.Size(116, 21);
        this.comboBoxDistriktId.TabIndex = 3;
        // 
        // comboBoxRegionId
        // 
        this.comboBoxRegionId.DataSource = this.dataSetFilter;
        this.comboBoxRegionId.DisplayMember = "Region.Id";
        this.comboBoxRegionId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxRegionId.FormattingEnabled = true;
        this.comboBoxRegionId.Location = new System.Drawing.Point(19, 34);
        this.comboBoxRegionId.Name = "comboBoxRegionId";
        this.comboBoxRegionId.Size = new System.Drawing.Size(116, 21);
        this.comboBoxRegionId.TabIndex = 1;
        this.comboBoxRegionId.ValueMember = "Region.Id";
        this.comboBoxRegionId.SelectedIndexChanged += new System.EventHandler(this.comboBoxRegionId_SelectedIndexChanged);
        // 
        // label33
        // 
        this.label33.AutoSize = true;
        this.label33.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label33.Location = new System.Drawing.Point(398, 17);
        this.label33.Name = "label33";
        this.label33.Size = new System.Drawing.Size(90, 13);
        this.label33.TabIndex = 6;
        this.label33.Text = "Entreprenörnr:";
        // 
        // label34
        // 
        this.label34.AutoSize = true;
        this.label34.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label34.Location = new System.Drawing.Point(526, 17);
        this.label34.Name = "label34";
        this.label34.Size = new System.Drawing.Size(58, 13);
        this.label34.TabIndex = 8;
        this.label34.Text = "Traktdel:";
        // 
        // label35
        // 
        this.label35.AutoSize = true;
        this.label35.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label35.Location = new System.Drawing.Point(269, 17);
        this.label35.Name = "label35";
        this.label35.Size = new System.Drawing.Size(56, 13);
        this.label35.TabIndex = 4;
        this.label35.Text = "Trakt Nr:";
        // 
        // label36
        // 
        this.label36.AutoSize = true;
        this.label36.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label36.Location = new System.Drawing.Point(142, 16);
        this.label36.Name = "label36";
        this.label36.Size = new System.Drawing.Size(67, 13);
        this.label36.TabIndex = 2;
        this.label36.Text = "Distrikt Id:";
        // 
        // label37
        // 
        this.label37.AutoSize = true;
        this.label37.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label37.Location = new System.Drawing.Point(17, 16);
        this.label37.Name = "label37";
        this.label37.Size = new System.Drawing.Size(64, 13);
        this.label37.TabIndex = 0;
        this.label37.Text = "Region Id:";
        // 
        // label38
        // 
        this.label38.AutoSize = true;
        this.label38.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label38.Location = new System.Drawing.Point(15, 66);
        this.label38.Name = "label38";
        this.label38.Size = new System.Drawing.Size(38, 13);
        this.label38.TabIndex = 10;
        this.label38.Text = "Årtal:";
        // 
        // groupBoxFiltreringsalternativ
        // 
        this.groupBoxFiltreringsalternativ.Controls.Add(this.radioButtonAllaRapportTyper);
        this.groupBoxFiltreringsalternativ.Controls.Add(this.radioButtonValdRapportTyp);
        this.groupBoxFiltreringsalternativ.Location = new System.Drawing.Point(8, 9);
        this.groupBoxFiltreringsalternativ.Name = "groupBoxFiltreringsalternativ";
        this.groupBoxFiltreringsalternativ.Size = new System.Drawing.Size(660, 50);
        this.groupBoxFiltreringsalternativ.TabIndex = 2;
        this.groupBoxFiltreringsalternativ.TabStop = false;
        this.groupBoxFiltreringsalternativ.Text = "Visa filtreringsalternativ";
        // 
        // radioButtonAllaRapportTyper
        // 
        this.radioButtonAllaRapportTyper.AutoSize = true;
        this.radioButtonAllaRapportTyper.Location = new System.Drawing.Point(188, 21);
        this.radioButtonAllaRapportTyper.Name = "radioButtonAllaRapportTyper";
        this.radioButtonAllaRapportTyper.Size = new System.Drawing.Size(118, 17);
        this.radioButtonAllaRapportTyper.TabIndex = 1;
        this.radioButtonAllaRapportTyper.Text = "Alla rapporttyper";
        this.radioButtonAllaRapportTyper.UseVisualStyleBackColor = true;
        this.radioButtonAllaRapportTyper.CheckedChanged += new System.EventHandler(this.radioButtonAllaRapportTyper_CheckedChanged);
        // 
        // radioButtonValdRapportTyp
        // 
        this.radioButtonValdRapportTyp.AutoSize = true;
        this.radioButtonValdRapportTyp.Checked = true;
        this.radioButtonValdRapportTyp.Location = new System.Drawing.Point(19, 21);
        this.radioButtonValdRapportTyp.Name = "radioButtonValdRapportTyp";
        this.radioButtonValdRapportTyp.Size = new System.Drawing.Size(111, 17);
        this.radioButtonValdRapportTyp.TabIndex = 0;
        this.radioButtonValdRapportTyp.TabStop = true;
        this.radioButtonValdRapportTyp.Text = "Vald rapporttyp";
        this.radioButtonValdRapportTyp.UseVisualStyleBackColor = true;
        this.radioButtonValdRapportTyp.CheckedChanged += new System.EventHandler(this.radioButtonValdRapportTyp_CheckedChanged);
        // 
        // FormFiltreraFrågor
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.ClientSize = new System.Drawing.Size(676, 489);
        this.Controls.Add(this.groupBoxFiltreringsalternativ);
        this.Controls.Add(this.groupBox1);
        this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
        this.Name = "FormFiltreraFrågor";
        this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
        this.Text = "Filtrera frågor för ";
        this.Load += new System.EventHandler(this.FormFiltreraFrågor_Load);
        this.groupBoxTabellData.ResumeLayout(false);
        this.groupBoxTabellData.PerformLayout();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetFilter)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableRegion)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableDistrikt)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableStandort)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableTrakt)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableEntreprenör)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableRapportTyp)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableArtal)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga1)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga2)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga3)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga4)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga5)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga6)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga7)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga8)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga9)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga10)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga11)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga12)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga13)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga14)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga15)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga16)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga17)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga18)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga19)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga20)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFråga21)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
        this.groupBox1.ResumeLayout(false);
        this.groupBox1.PerformLayout();
        this.groupBoxFiltreringsalternativ.ResumeLayout(false);
        this.groupBoxFiltreringsalternativ.PerformLayout();
        this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox groupBoxTabellData;
    private System.Windows.Forms.Label label14;
    private System.Windows.Forms.Label label15;
    private System.Windows.Forms.Label label16;
    private System.Windows.Forms.Label label18;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.Button buttonTömFilter;
    private System.Windows.Forms.ComboBox comboBoxÅrtal;
    private System.Windows.Forms.Button buttonAvbryt;
    private System.Windows.Forms.Button buttonUppdatera;
    private System.Windows.Forms.ComboBox comboBoxStåndortId;
    private System.Windows.Forms.ComboBox comboBoxEntreprenörId;
    private System.Windows.Forms.ComboBox comboBoxTraktNr;
    private System.Windows.Forms.ComboBox comboBoxDistriktId;
    private System.Windows.Forms.ComboBox comboBoxRegionId;
    private System.Windows.Forms.Label label33;
    private System.Windows.Forms.Label label34;
    private System.Windows.Forms.Label label35;
    private System.Windows.Forms.Label label36;
    private System.Windows.Forms.Label label37;
    private System.Windows.Forms.Label label38;
    private System.Windows.Forms.ComboBox comboBoxFråga5;
    private System.Windows.Forms.ComboBox comboBoxFråga4;
    private System.Windows.Forms.ComboBox comboBoxFråga3;
    private System.Windows.Forms.ComboBox comboBoxFråga2;
    private System.Windows.Forms.ComboBox comboBoxFråga1;
    private System.Windows.Forms.ComboBox comboBoxFråga9;
    private System.Windows.Forms.ComboBox comboBoxFråga8;
    private System.Windows.Forms.ComboBox comboBoxFråga7;
    private System.Windows.Forms.ComboBox comboBoxFråga6;
    private System.Windows.Forms.ImageList imageListFilter;
    private System.Data.DataSet dataSetFilter;
    private System.Data.DataTable dataTableRegion;
    private System.Data.DataColumn dataColumnFilterRegionId;
    private System.Data.DataColumn dataColumnFilterRegionNamn;
    private System.Data.DataTable dataTableDistrikt;
    private System.Data.DataColumn dataColumnFilterDistriktRegionId;
    private System.Data.DataColumn dataColumnFilterDistriktId;
    private System.Data.DataColumn dataColumnFilterDistriktNamn;
    private System.Data.DataTable dataTableStandort;
    private System.Data.DataColumn dataColumnFilterStandortNamn;
    private System.Data.DataTable dataTableTrakt;
    private System.Data.DataColumn dataColumnFilterTraktId;
    private System.Data.DataColumn dataColumnFilterTraktNamn;
    private System.Data.DataTable dataTableEntreprenör;
    private System.Data.DataColumn dataColumnFilterEntreprenörId;
    private System.Data.DataColumn dataColumnFilterEntreprenörNamn;
    private System.Data.DataTable dataTableRapportTyp;
    private System.Data.DataColumn dataColumnFilterRapportTypId;
    private System.Data.DataColumn dataColumnFilterRapportTypNamn;
    private System.Data.DataTable dataTableArtal;
    private System.Data.DataColumn dataColumnFilterÅrtalId;
    private System.Data.DataTable dataTableFråga1;
    private System.Data.DataColumn dataColumnSvar1;
    private System.Data.DataTable dataTableFråga2;
    private System.Data.DataColumn dataColumnSvar2;
    private System.Data.DataTable dataTableFråga3;
    private System.Data.DataColumn dataColumnSvar3;
    private System.Data.DataTable dataTableFråga4;
    private System.Data.DataColumn dataColumnSvar4;
    private System.Data.DataTable dataTableFråga5;
    private System.Data.DataColumn dataColumnSvar5;
    private System.Data.DataTable dataTableFråga6;
    private System.Data.DataColumn dataColumnSvar6;
    private System.Data.DataTable dataTableFråga7;
    private System.Data.DataColumn dataColumnSvar7;
    private System.Data.DataTable dataTableFråga8;
    private System.Data.DataColumn dataColumnSvar8;
    private System.Data.DataTable dataTableFråga9;
    private System.Data.DataColumn dataColumnSvar9;
    private System.Windows.Forms.ComboBox comboBoxFråga19;
    private System.Windows.Forms.ComboBox comboBoxFråga18;
    private System.Windows.Forms.ComboBox comboBoxFråga17;
    private System.Windows.Forms.ComboBox comboBoxFråga16;
    private System.Windows.Forms.ComboBox comboBoxFråga10;
    private System.Windows.Forms.ComboBox comboBoxFråga14;
    private System.Windows.Forms.ComboBox comboBoxFråga13;
    private System.Windows.Forms.ComboBox comboBoxFråga12;
    private System.Windows.Forms.ComboBox comboBoxFråga11;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Label label13;
    private System.Windows.Forms.Label label17;
    private System.Windows.Forms.ComboBox comboBoxFråga15;
    private System.Windows.Forms.Label label19;
    private System.Data.DataTable dataTableFråga10;
    private System.Data.DataTable dataTableFråga11;
    private System.Data.DataTable dataTableFråga12;
    private System.Data.DataTable dataTableFråga13;
    private System.Data.DataTable dataTableFråga14;
    private System.Data.DataTable dataTableFråga15;
    private System.Data.DataTable dataTableFråga16;
    private System.Data.DataTable dataTableFråga17;
    private System.Data.DataTable dataTableFråga18;
    private System.Data.DataTable dataTableFråga19;
    private System.Data.DataColumn dataColumnSvar10;
    private System.Data.DataColumn dataColumnSvar11;
    private System.Data.DataColumn dataColumnSvar12;
    private System.Data.DataColumn dataColumnSvar13;
    private System.Data.DataColumn dataColumnFråga14;
    private System.Data.DataColumn dataColumnFråga15;
    private System.Data.DataColumn dataColumnFråga16;
    private System.Data.DataColumn dataColumnFråga17;
    private System.Data.DataColumn dataColumnFråga18;
    private System.Data.DataColumn dataColumnFråga19;
    private System.Windows.Forms.ComboBox comboBoxFråga20;
    private System.Data.DataTable dataTableFråga20;
    private System.Data.DataColumn dataColumnFråga20;
    private System.Data.DataTable dataTableFråga21;
    private System.Windows.Forms.Label label20;
    private System.Data.DataColumn dataColumnFråga21;
    private System.Windows.Forms.ComboBox comboBoxFråga21;
    private System.Windows.Forms.Label label21;
    private System.Windows.Forms.GroupBox groupBoxFiltreringsalternativ;
    private System.Windows.Forms.RadioButton radioButtonAllaRapportTyper;
    private System.Windows.Forms.RadioButton radioButtonValdRapportTyp;
    private System.Data.DataTable dataTable1;
    private System.Data.DataColumn dataColumndataSetFilterInvTypId;
    private System.Data.DataColumn dataColumndataSetFilterInvTypNamn;
    private System.Windows.Forms.ComboBox comboBoxInvTypId;
    private System.Windows.Forms.Label label22;

  }
}