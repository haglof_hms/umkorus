﻿namespace Egenuppfoljning.Dialogs
{
  partial class FormVisaFrågor
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormVisaFrågor));
      this.groupBoxFrågor = new System.Windows.Forms.GroupBox();
      this.richTextBoxFrågor = new System.Windows.Forms.RichTextBox();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.buttonOk = new System.Windows.Forms.Button();
      this.groupBoxFrågor.SuspendLayout();
      this.groupBox2.SuspendLayout();
      this.SuspendLayout();
      // 
      // groupBoxFrågor
      // 
      this.groupBoxFrågor.Controls.Add(this.richTextBoxFrågor);
      this.groupBoxFrågor.Location = new System.Drawing.Point(7, 12);
      this.groupBoxFrågor.Name = "groupBoxFrågor";
      this.groupBoxFrågor.Size = new System.Drawing.Size(413, 395);
      this.groupBoxFrågor.TabIndex = 0;
      this.groupBoxFrågor.TabStop = false;
      // 
      // richTextBoxFrågor
      // 
      this.richTextBoxFrågor.Location = new System.Drawing.Point(15, 20);
      this.richTextBoxFrågor.Name = "richTextBoxFrågor";
      this.richTextBoxFrågor.ReadOnly = true;
      this.richTextBoxFrågor.Size = new System.Drawing.Size(382, 359);
      this.richTextBoxFrågor.TabIndex = 0;
      this.richTextBoxFrågor.Text = "";
      // 
      // groupBox2
      // 
      this.groupBox2.Controls.Add(this.buttonOk);
      this.groupBox2.Location = new System.Drawing.Point(7, 398);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(413, 50);
      this.groupBox2.TabIndex = 1;
      this.groupBox2.TabStop = false;
      // 
      // buttonOk
      // 
      this.buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.buttonOk.Location = new System.Drawing.Point(310, 14);
      this.buttonOk.Name = "buttonOk";
      this.buttonOk.Size = new System.Drawing.Size(87, 30);
      this.buttonOk.TabIndex = 0;
      this.buttonOk.Text = "OK";
      this.buttonOk.UseVisualStyleBackColor = true;
      this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
      // 
      // FormVisaFrågor
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(428, 456);
      this.Controls.Add(this.groupBox2);
      this.Controls.Add(this.groupBoxFrågor);
      this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Name = "FormVisaFrågor";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Frågor";
      this.Load += new System.EventHandler(this.FormVisaFrågor_Load);
      this.groupBoxFrågor.ResumeLayout(false);
      this.groupBox2.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox groupBoxFrågor;
    private System.Windows.Forms.RichTextBox richTextBoxFrågor;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.Button buttonOk;
  }
}