﻿#region

using System;
using System.Windows.Forms;

#endregion

namespace Egenuppfoljning.Dialogs
{
  public partial class FormSendReport : Form
  {
    public FormSendReport()
    {
      InitializeComponent();
    }

    public string Titel
    {
      get { return textBoxTitel.Text; }
    }

    public string Meddelande
    {
      get { return richTextBoxMeddelande.Text; }
    }

    private void buttonSkicka_Click(object sender, EventArgs e)
    {
      DialogResult = DialogResult.OK;
      Close();
    }

    private void buttonAvbryt_Click(object sender, EventArgs e)
    {
      DialogResult = DialogResult.Cancel;
      Close();
    }

    public void InitData(string aEpostSändsVia, string aMottagare, string aTitel, string aMeddelande,
                         string aBifogadRapport)
    {
      labelEpostSändsVia.Text = aEpostSändsVia;
      labelMottagare.Text = aMottagare;
      textBoxTitel.Text = aTitel;
      richTextBoxMeddelande.Text = aMeddelande;
      labelBifogadRapport.Text = aBifogadRapport;
    }
  }
}