﻿namespace Egenuppfoljning.Dialogs
{
  partial class FormFiltreraRapporter
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.components = new System.ComponentModel.Container();
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormFiltreraRapporter));
        this.groupBoxNyckelvärden = new System.Windows.Forms.GroupBox();
        this.label33 = new System.Windows.Forms.Label();
        this.comboBoxInvTypId = new System.Windows.Forms.ComboBox();
        this.dataSetFilter = new System.Data.DataSet();
        this.dataTableRegion = new System.Data.DataTable();
        this.dataColumnFilterRegionId = new System.Data.DataColumn();
        this.dataColumnFilterRegionNamn = new System.Data.DataColumn();
        this.dataTableDistrikt = new System.Data.DataTable();
        this.dataColumnFilterDistriktRegionId = new System.Data.DataColumn();
        this.dataColumnFilterDistriktId = new System.Data.DataColumn();
        this.dataColumnFilterDistriktNamn = new System.Data.DataColumn();
        this.dataTableTraktDel = new System.Data.DataTable();
        this.dataColumnFilterTraktDelId = new System.Data.DataColumn();
        this.dataTableMarkberedningsmetod = new System.Data.DataTable();
        this.dataColumnFilterMarkberedningsmetodNamn = new System.Data.DataColumn();
        this.dataTableUrsprung = new System.Data.DataTable();
        this.dataColumnFilterUrsprungNamn = new System.Data.DataColumn();
        this.dataTableTrakt = new System.Data.DataTable();
        this.dataColumnFilterTraktId = new System.Data.DataColumn();
        this.dataColumnFilterTraktNamn = new System.Data.DataColumn();
        this.dataTableEntreprenör = new System.Data.DataTable();
        this.dataColumnFilterEntreprenörId = new System.Data.DataColumn();
        this.dataColumnFilterEntreprenörNamn = new System.Data.DataColumn();
        this.dataTableRapportTyp = new System.Data.DataTable();
        this.dataColumnFilterRapportTypId = new System.Data.DataColumn();
        this.dataColumnFilterRapportTypNamn = new System.Data.DataColumn();
        this.dataTableArtal = new System.Data.DataTable();
        this.dataColumnFilterÅrtalId = new System.Data.DataColumn();
        this.dataTableStickvagssystem = new System.Data.DataTable();
        this.dataColumnFilterStickvagssystemNamn = new System.Data.DataColumn();
        this.dataTableDatainsamlingsmetod = new System.Data.DataTable();
        this.dataColumnDataColumnFilterMarkberedningsmetodNamn = new System.Data.DataColumn();
        this.dataTableInvTyp = new System.Data.DataTable();
        this.dataColumnFilterInvtypId = new System.Data.DataColumn();
        this.dataColumnFilterInvtypNamn = new System.Data.DataColumn();
        this.comboBoxDatainsamlingsmetod = new System.Windows.Forms.ComboBox();
        this.label32 = new System.Windows.Forms.Label();
        this.comboBoxStickvägssystem = new System.Windows.Forms.ComboBox();
        this.label31 = new System.Windows.Forms.Label();
        this.groupBoxTabellData = new System.Windows.Forms.GroupBox();
        this.label28 = new System.Windows.Forms.Label();
        this.label29 = new System.Windows.Forms.Label();
        this.label30 = new System.Windows.Forms.Label();
        this.groupBox9 = new System.Windows.Forms.GroupBox();
        this.textBoxAvlagg6Ost = new System.Windows.Forms.TextBox();
        this.textBoxAvlagg5Ost = new System.Windows.Forms.TextBox();
        this.textBoxAvlagg4Ost = new System.Windows.Forms.TextBox();
        this.groupBox10 = new System.Windows.Forms.GroupBox();
        this.textBoxAvlagg6Norr = new System.Windows.Forms.TextBox();
        this.textBoxAvlagg5Norr = new System.Windows.Forms.TextBox();
        this.textBoxAvlagg4Norr = new System.Windows.Forms.TextBox();
        this.label25 = new System.Windows.Forms.Label();
        this.label26 = new System.Windows.Forms.Label();
        this.groupBox8 = new System.Windows.Forms.GroupBox();
        this.textBoxAvlagg3Ost = new System.Windows.Forms.TextBox();
        this.textBoxAvlagg2Ost = new System.Windows.Forms.TextBox();
        this.textBoxAvlagg1Ost = new System.Windows.Forms.TextBox();
        this.groupBox7 = new System.Windows.Forms.GroupBox();
        this.textBoxAvlagg3Norr = new System.Windows.Forms.TextBox();
        this.textBoxAvlagg2Norr = new System.Windows.Forms.TextBox();
        this.textBoxAvlagg1Norr = new System.Windows.Forms.TextBox();
        this.label27 = new System.Windows.Forms.Label();
        this.checkBoxTraktordragenHugg = new System.Windows.Forms.CheckBox();
        this.checkBoxLatbilshugg = new System.Windows.Forms.CheckBox();
        this.checkBoxGrotbil = new System.Windows.Forms.CheckBox();
        this.checkBoxSkotarburenHugg = new System.Windows.Forms.CheckBox();
        this.label6 = new System.Windows.Forms.Label();
        this.textBoxAntalVältor = new System.Windows.Forms.TextBox();
        this.label13 = new System.Windows.Forms.Label();
        this.textBoxVägkant = new System.Windows.Forms.TextBox();
        this.label21 = new System.Windows.Forms.Label();
        this.textBoxHygge = new System.Windows.Forms.TextBox();
        this.label22 = new System.Windows.Forms.Label();
        this.textBoxBedömdVolym = new System.Windows.Forms.TextBox();
        this.label23 = new System.Windows.Forms.Label();
        this.textBoxÅker = new System.Windows.Forms.TextBox();
        this.label24 = new System.Windows.Forms.Label();
        this.textBoxAvstånd = new System.Windows.Forms.TextBox();
        this.label19 = new System.Windows.Forms.Label();
        this.textBoxAreal = new System.Windows.Forms.TextBox();
        this.label14 = new System.Windows.Forms.Label();
        this.textBoxProvytestorlek = new System.Windows.Forms.TextBox();
        this.label15 = new System.Windows.Forms.Label();
        this.textBoxGISAreal = new System.Windows.Forms.TextBox();
        this.label16 = new System.Windows.Forms.Label();
        this.textBoxGISSträcka = new System.Windows.Forms.TextBox();
        this.label17 = new System.Windows.Forms.Label();
        this.textBoxStickvägsavstånd = new System.Windows.Forms.TextBox();
        this.label18 = new System.Windows.Forms.Label();
        this.textBoxMålgrundyta = new System.Windows.Forms.TextBox();
        this.buttonTömAllaFilter = new System.Windows.Forms.Button();
        this.imageListFilter = new System.Windows.Forms.ImageList(this.components);
        this.comboBoxMarkberedning = new System.Windows.Forms.ComboBox();
        this.comboBoxUrsprung = new System.Windows.Forms.ComboBox();
        this.comboBoxÅrtal = new System.Windows.Forms.ComboBox();
        this.buttonAvbryt = new System.Windows.Forms.Button();
        this.buttonUppdatera = new System.Windows.Forms.Button();
        this.comboBoxTraktDelId = new System.Windows.Forms.ComboBox();
        this.comboBoxEntreprenörNamn = new System.Windows.Forms.ComboBox();
        this.label20 = new System.Windows.Forms.Label();
        this.comboBoxEntreprenörId = new System.Windows.Forms.ComboBox();
        this.comboBoxTraktNamn = new System.Windows.Forms.ComboBox();
        this.comboBoxTraktNr = new System.Windows.Forms.ComboBox();
        this.comboBoxDistriktId = new System.Windows.Forms.ComboBox();
        this.comboBoxDistriktNamn = new System.Windows.Forms.ComboBox();
        this.label7 = new System.Windows.Forms.Label();
        this.comboBoxRegionNamn = new System.Windows.Forms.ComboBox();
        this.comboBoxRegionId = new System.Windows.Forms.ComboBox();
        this.label9 = new System.Windows.Forms.Label();
        this.label5 = new System.Windows.Forms.Label();
        this.label4 = new System.Windows.Forms.Label();
        this.label3 = new System.Windows.Forms.Label();
        this.label2 = new System.Windows.Forms.Label();
        this.label1 = new System.Windows.Forms.Label();
        this.label8 = new System.Windows.Forms.Label();
        this.label11 = new System.Windows.Forms.Label();
        this.label10 = new System.Windows.Forms.Label();
        this.label12 = new System.Windows.Forms.Label();
        this.groupBoxFiltreringsalternativ = new System.Windows.Forms.GroupBox();
        this.radioButtonAllaRapportTyper = new System.Windows.Forms.RadioButton();
        this.radioButtonValdRapportTyp = new System.Windows.Forms.RadioButton();
        this.groupBoxNyckelvärden.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetFilter)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableRegion)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableDistrikt)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableTraktDel)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableMarkberedningsmetod)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableUrsprung)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableTrakt)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableEntreprenör)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableRapportTyp)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableArtal)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableStickvagssystem)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableDatainsamlingsmetod)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableInvTyp)).BeginInit();
        this.groupBoxTabellData.SuspendLayout();
        this.groupBox9.SuspendLayout();
        this.groupBox10.SuspendLayout();
        this.groupBox8.SuspendLayout();
        this.groupBox7.SuspendLayout();
        this.groupBoxFiltreringsalternativ.SuspendLayout();
        this.SuspendLayout();
        // 
        // groupBoxNyckelvärden
        // 
        this.groupBoxNyckelvärden.Controls.Add(this.label33);
        this.groupBoxNyckelvärden.Controls.Add(this.comboBoxInvTypId);
        this.groupBoxNyckelvärden.Controls.Add(this.comboBoxDatainsamlingsmetod);
        this.groupBoxNyckelvärden.Controls.Add(this.label32);
        this.groupBoxNyckelvärden.Controls.Add(this.comboBoxStickvägssystem);
        this.groupBoxNyckelvärden.Controls.Add(this.label31);
        this.groupBoxNyckelvärden.Controls.Add(this.groupBoxTabellData);
        this.groupBoxNyckelvärden.Controls.Add(this.buttonTömAllaFilter);
        this.groupBoxNyckelvärden.Controls.Add(this.comboBoxMarkberedning);
        this.groupBoxNyckelvärden.Controls.Add(this.comboBoxUrsprung);
        this.groupBoxNyckelvärden.Controls.Add(this.comboBoxÅrtal);
        this.groupBoxNyckelvärden.Controls.Add(this.buttonAvbryt);
        this.groupBoxNyckelvärden.Controls.Add(this.buttonUppdatera);
        this.groupBoxNyckelvärden.Controls.Add(this.comboBoxTraktDelId);
        this.groupBoxNyckelvärden.Controls.Add(this.comboBoxEntreprenörNamn);
        this.groupBoxNyckelvärden.Controls.Add(this.label20);
        this.groupBoxNyckelvärden.Controls.Add(this.comboBoxEntreprenörId);
        this.groupBoxNyckelvärden.Controls.Add(this.comboBoxTraktNamn);
        this.groupBoxNyckelvärden.Controls.Add(this.comboBoxTraktNr);
        this.groupBoxNyckelvärden.Controls.Add(this.comboBoxDistriktId);
        this.groupBoxNyckelvärden.Controls.Add(this.comboBoxDistriktNamn);
        this.groupBoxNyckelvärden.Controls.Add(this.label7);
        this.groupBoxNyckelvärden.Controls.Add(this.comboBoxRegionNamn);
        this.groupBoxNyckelvärden.Controls.Add(this.comboBoxRegionId);
        this.groupBoxNyckelvärden.Controls.Add(this.label9);
        this.groupBoxNyckelvärden.Controls.Add(this.label5);
        this.groupBoxNyckelvärden.Controls.Add(this.label4);
        this.groupBoxNyckelvärden.Controls.Add(this.label3);
        this.groupBoxNyckelvärden.Controls.Add(this.label2);
        this.groupBoxNyckelvärden.Controls.Add(this.label1);
        this.groupBoxNyckelvärden.Controls.Add(this.label8);
        this.groupBoxNyckelvärden.Controls.Add(this.label11);
        this.groupBoxNyckelvärden.Controls.Add(this.label10);
        this.groupBoxNyckelvärden.Controls.Add(this.label12);
        this.groupBoxNyckelvärden.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.groupBoxNyckelvärden.Location = new System.Drawing.Point(7, 41);
        this.groupBoxNyckelvärden.Name = "groupBoxNyckelvärden";
        this.groupBoxNyckelvärden.Size = new System.Drawing.Size(688, 462);
        this.groupBoxNyckelvärden.TabIndex = 0;
        this.groupBoxNyckelvärden.TabStop = false;
        // 
        // label33
        // 
        this.label33.AutoSize = true;
        this.label33.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label33.Location = new System.Drawing.Point(11, 152);
        this.label33.Name = "label33";
        this.label33.Size = new System.Drawing.Size(48, 13);
        this.label33.TabIndex = 33;
        this.label33.Text = "Invtyp:";
        // 
        // comboBoxInvTypId
        // 
        this.comboBoxInvTypId.DataBindings.Add(new System.Windows.Forms.Binding("SelectedValue", this.dataSetFilter, "InvTyp.Id", true));
        this.comboBoxInvTypId.DataSource = this.dataSetFilter;
        this.comboBoxInvTypId.DisplayMember = "Invtyp.Namn";
        this.comboBoxInvTypId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxInvTypId.FormattingEnabled = true;
        this.comboBoxInvTypId.Location = new System.Drawing.Point(13, 171);
        this.comboBoxInvTypId.Name = "comboBoxInvTypId";
        this.comboBoxInvTypId.Size = new System.Drawing.Size(116, 21);
        this.comboBoxInvTypId.TabIndex = 15;
        this.comboBoxInvTypId.ValueMember = "Invtyp.Id";
        // 
        // dataSetFilter
        // 
        this.dataSetFilter.DataSetName = "DataSetFilter";
        this.dataSetFilter.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableRegion,
            this.dataTableDistrikt,
            this.dataTableTraktDel,
            this.dataTableMarkberedningsmetod,
            this.dataTableUrsprung,
            this.dataTableTrakt,
            this.dataTableEntreprenör,
            this.dataTableRapportTyp,
            this.dataTableArtal,
            this.dataTableStickvagssystem,
            this.dataTableDatainsamlingsmetod,
            this.dataTableInvTyp});
        // 
        // dataTableRegion
        // 
        this.dataTableRegion.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFilterRegionId,
            this.dataColumnFilterRegionNamn});
        this.dataTableRegion.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, true)});
        this.dataTableRegion.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnFilterRegionId};
        this.dataTableRegion.TableName = "Region";
        // 
        // dataColumnFilterRegionId
        // 
        this.dataColumnFilterRegionId.AllowDBNull = false;
        this.dataColumnFilterRegionId.ColumnName = "Id";
        this.dataColumnFilterRegionId.DataType = typeof(int);
        this.dataColumnFilterRegionId.ReadOnly = true;
        // 
        // dataColumnFilterRegionNamn
        // 
        this.dataColumnFilterRegionNamn.ColumnName = "Namn";
        // 
        // dataTableDistrikt
        // 
        this.dataTableDistrikt.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFilterDistriktRegionId,
            this.dataColumnFilterDistriktId,
            this.dataColumnFilterDistriktNamn});
        this.dataTableDistrikt.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, true)});
        this.dataTableDistrikt.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnFilterDistriktId};
        this.dataTableDistrikt.TableName = "Distrikt";
        // 
        // dataColumnFilterDistriktRegionId
        // 
        this.dataColumnFilterDistriktRegionId.ColumnName = "RegionId";
        this.dataColumnFilterDistriktRegionId.DataType = typeof(int);
        // 
        // dataColumnFilterDistriktId
        // 
        this.dataColumnFilterDistriktId.AllowDBNull = false;
        this.dataColumnFilterDistriktId.ColumnName = "Id";
        this.dataColumnFilterDistriktId.DataType = typeof(int);
        // 
        // dataColumnFilterDistriktNamn
        // 
        this.dataColumnFilterDistriktNamn.ColumnName = "Namn";
        // 
        // dataTableTraktDel
        // 
        this.dataTableTraktDel.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFilterTraktDelId});
        this.dataTableTraktDel.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, true)});
        this.dataTableTraktDel.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnFilterTraktDelId};
        this.dataTableTraktDel.TableName = "TraktDel";
        // 
        // dataColumnFilterTraktDelId
        // 
        this.dataColumnFilterTraktDelId.AllowDBNull = false;
        this.dataColumnFilterTraktDelId.ColumnName = "Id";
        this.dataColumnFilterTraktDelId.DataType = typeof(int);
        // 
        // dataTableMarkberedningsmetod
        // 
        this.dataTableMarkberedningsmetod.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFilterMarkberedningsmetodNamn});
        this.dataTableMarkberedningsmetod.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Namn"}, true)});
        this.dataTableMarkberedningsmetod.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnFilterMarkberedningsmetodNamn};
        this.dataTableMarkberedningsmetod.TableName = "Markberedningsmetod";
        // 
        // dataColumnFilterMarkberedningsmetodNamn
        // 
        this.dataColumnFilterMarkberedningsmetodNamn.AllowDBNull = false;
        this.dataColumnFilterMarkberedningsmetodNamn.ColumnName = "Namn";
        // 
        // dataTableUrsprung
        // 
        this.dataTableUrsprung.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFilterUrsprungNamn});
        this.dataTableUrsprung.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Namn"}, true)});
        this.dataTableUrsprung.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnFilterUrsprungNamn};
        this.dataTableUrsprung.TableName = "Ursprung";
        // 
        // dataColumnFilterUrsprungNamn
        // 
        this.dataColumnFilterUrsprungNamn.AllowDBNull = false;
        this.dataColumnFilterUrsprungNamn.ColumnName = "Namn";
        // 
        // dataTableTrakt
        // 
        this.dataTableTrakt.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFilterTraktId,
            this.dataColumnFilterTraktNamn});
        this.dataTableTrakt.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, true)});
        this.dataTableTrakt.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnFilterTraktId};
        this.dataTableTrakt.TableName = "Trakt";
        // 
        // dataColumnFilterTraktId
        // 
        this.dataColumnFilterTraktId.AllowDBNull = false;
        this.dataColumnFilterTraktId.ColumnName = "Id";
        this.dataColumnFilterTraktId.DataType = typeof(int);
        // 
        // dataColumnFilterTraktNamn
        // 
        this.dataColumnFilterTraktNamn.ColumnName = "Namn";
        // 
        // dataTableEntreprenör
        // 
        this.dataTableEntreprenör.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFilterEntreprenörId,
            this.dataColumnFilterEntreprenörNamn});
        this.dataTableEntreprenör.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, true)});
        this.dataTableEntreprenör.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnFilterEntreprenörId};
        this.dataTableEntreprenör.TableName = "Entreprenör";
        // 
        // dataColumnFilterEntreprenörId
        // 
        this.dataColumnFilterEntreprenörId.AllowDBNull = false;
        this.dataColumnFilterEntreprenörId.ColumnName = "Id";
        // 
        // dataColumnFilterEntreprenörNamn
        // 
        this.dataColumnFilterEntreprenörNamn.ColumnName = "Namn";
        // 
        // dataTableRapportTyp
        // 
        this.dataTableRapportTyp.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFilterRapportTypId,
            this.dataColumnFilterRapportTypNamn});
        this.dataTableRapportTyp.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, true)});
        this.dataTableRapportTyp.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnFilterRapportTypId};
        this.dataTableRapportTyp.TableName = "RapportTyp";
        // 
        // dataColumnFilterRapportTypId
        // 
        this.dataColumnFilterRapportTypId.AllowDBNull = false;
        this.dataColumnFilterRapportTypId.ColumnName = "Id";
        this.dataColumnFilterRapportTypId.DataType = typeof(int);
        // 
        // dataColumnFilterRapportTypNamn
        // 
        this.dataColumnFilterRapportTypNamn.ColumnName = "Namn";
        // 
        // dataTableArtal
        // 
        this.dataTableArtal.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFilterÅrtalId});
        this.dataTableArtal.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, true)});
        this.dataTableArtal.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnFilterÅrtalId};
        this.dataTableArtal.TableName = "Artal";
        // 
        // dataColumnFilterÅrtalId
        // 
        this.dataColumnFilterÅrtalId.AllowDBNull = false;
        this.dataColumnFilterÅrtalId.ColumnName = "Id";
        this.dataColumnFilterÅrtalId.DataType = typeof(int);
        // 
        // dataTableStickvagssystem
        // 
        this.dataTableStickvagssystem.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFilterStickvagssystemNamn});
        this.dataTableStickvagssystem.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Namn"}, true)});
        this.dataTableStickvagssystem.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnFilterStickvagssystemNamn};
        this.dataTableStickvagssystem.TableName = "Stickvagssystem";
        // 
        // dataColumnFilterStickvagssystemNamn
        // 
        this.dataColumnFilterStickvagssystemNamn.AllowDBNull = false;
        this.dataColumnFilterStickvagssystemNamn.ColumnName = "Namn";
        // 
        // dataTableDatainsamlingsmetod
        // 
        this.dataTableDatainsamlingsmetod.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnDataColumnFilterMarkberedningsmetodNamn});
        this.dataTableDatainsamlingsmetod.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Namn"}, true)});
        this.dataTableDatainsamlingsmetod.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnDataColumnFilterMarkberedningsmetodNamn};
        this.dataTableDatainsamlingsmetod.TableName = "Datainsamlingsmetod";
        // 
        // dataColumnDataColumnFilterMarkberedningsmetodNamn
        // 
        this.dataColumnDataColumnFilterMarkberedningsmetodNamn.AllowDBNull = false;
        this.dataColumnDataColumnFilterMarkberedningsmetodNamn.ColumnName = "Namn";
        // 
        // dataTableInvTyp
        // 
        this.dataTableInvTyp.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFilterInvtypId,
            this.dataColumnFilterInvtypNamn});
        this.dataTableInvTyp.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, true)});
        this.dataTableInvTyp.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumnFilterInvtypId};
        this.dataTableInvTyp.TableName = "InvTyp";
        // 
        // dataColumnFilterInvtypId
        // 
        this.dataColumnFilterInvtypId.AllowDBNull = false;
        this.dataColumnFilterInvtypId.ColumnName = "Id";
        this.dataColumnFilterInvtypId.DataType = typeof(int);
        this.dataColumnFilterInvtypId.ReadOnly = true;
        // 
        // dataColumnFilterInvtypNamn
        // 
        this.dataColumnFilterInvtypNamn.ColumnName = "Namn";
        // 
        // comboBoxDatainsamlingsmetod
        // 
        this.comboBoxDatainsamlingsmetod.DataSource = this.dataSetFilter;
        this.comboBoxDatainsamlingsmetod.DisplayMember = "Datainsamlingsmetod.Namn";
        this.comboBoxDatainsamlingsmetod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxDatainsamlingsmetod.FormattingEnabled = true;
        this.comboBoxDatainsamlingsmetod.Location = new System.Drawing.Point(321, 127);
        this.comboBoxDatainsamlingsmetod.Name = "comboBoxDatainsamlingsmetod";
        this.comboBoxDatainsamlingsmetod.Size = new System.Drawing.Size(158, 21);
        this.comboBoxDatainsamlingsmetod.TabIndex = 13;
        // 
        // label32
        // 
        this.label32.AutoSize = true;
        this.label32.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label32.Location = new System.Drawing.Point(318, 111);
        this.label32.Name = "label32";
        this.label32.Size = new System.Drawing.Size(134, 13);
        this.label32.TabIndex = 30;
        this.label32.Text = "Datainsamlingsmetod:";
        // 
        // comboBoxStickvägssystem
        // 
        this.comboBoxStickvägssystem.DataSource = this.dataSetFilter;
        this.comboBoxStickvägssystem.DisplayMember = "Stickvagssystem.Namn";
        this.comboBoxStickvägssystem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxStickvägssystem.FormattingEnabled = true;
        this.comboBoxStickvägssystem.Location = new System.Drawing.Point(554, 127);
        this.comboBoxStickvägssystem.Name = "comboBoxStickvägssystem";
        this.comboBoxStickvägssystem.Size = new System.Drawing.Size(116, 21);
        this.comboBoxStickvägssystem.TabIndex = 14;
        // 
        // label31
        // 
        this.label31.AutoSize = true;
        this.label31.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label31.Location = new System.Drawing.Point(552, 111);
        this.label31.Name = "label31";
        this.label31.Size = new System.Drawing.Size(107, 13);
        this.label31.TabIndex = 28;
        this.label31.Text = "Stickvägssystem:";
        // 
        // groupBoxTabellData
        // 
        this.groupBoxTabellData.Controls.Add(this.label28);
        this.groupBoxTabellData.Controls.Add(this.label29);
        this.groupBoxTabellData.Controls.Add(this.label30);
        this.groupBoxTabellData.Controls.Add(this.groupBox9);
        this.groupBoxTabellData.Controls.Add(this.groupBox10);
        this.groupBoxTabellData.Controls.Add(this.label25);
        this.groupBoxTabellData.Controls.Add(this.label26);
        this.groupBoxTabellData.Controls.Add(this.groupBox8);
        this.groupBoxTabellData.Controls.Add(this.groupBox7);
        this.groupBoxTabellData.Controls.Add(this.label27);
        this.groupBoxTabellData.Controls.Add(this.checkBoxTraktordragenHugg);
        this.groupBoxTabellData.Controls.Add(this.checkBoxLatbilshugg);
        this.groupBoxTabellData.Controls.Add(this.checkBoxGrotbil);
        this.groupBoxTabellData.Controls.Add(this.checkBoxSkotarburenHugg);
        this.groupBoxTabellData.Controls.Add(this.label6);
        this.groupBoxTabellData.Controls.Add(this.textBoxAntalVältor);
        this.groupBoxTabellData.Controls.Add(this.label13);
        this.groupBoxTabellData.Controls.Add(this.textBoxVägkant);
        this.groupBoxTabellData.Controls.Add(this.label21);
        this.groupBoxTabellData.Controls.Add(this.textBoxHygge);
        this.groupBoxTabellData.Controls.Add(this.label22);
        this.groupBoxTabellData.Controls.Add(this.textBoxBedömdVolym);
        this.groupBoxTabellData.Controls.Add(this.label23);
        this.groupBoxTabellData.Controls.Add(this.textBoxÅker);
        this.groupBoxTabellData.Controls.Add(this.label24);
        this.groupBoxTabellData.Controls.Add(this.textBoxAvstånd);
        this.groupBoxTabellData.Controls.Add(this.label19);
        this.groupBoxTabellData.Controls.Add(this.textBoxAreal);
        this.groupBoxTabellData.Controls.Add(this.label14);
        this.groupBoxTabellData.Controls.Add(this.textBoxProvytestorlek);
        this.groupBoxTabellData.Controls.Add(this.label15);
        this.groupBoxTabellData.Controls.Add(this.textBoxGISAreal);
        this.groupBoxTabellData.Controls.Add(this.label16);
        this.groupBoxTabellData.Controls.Add(this.textBoxGISSträcka);
        this.groupBoxTabellData.Controls.Add(this.label17);
        this.groupBoxTabellData.Controls.Add(this.textBoxStickvägsavstånd);
        this.groupBoxTabellData.Controls.Add(this.label18);
        this.groupBoxTabellData.Controls.Add(this.textBoxMålgrundyta);
        this.groupBoxTabellData.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.groupBoxTabellData.Location = new System.Drawing.Point(0, 194);
        this.groupBoxTabellData.Name = "groupBoxTabellData";
        this.groupBoxTabellData.Size = new System.Drawing.Size(688, 223);
        this.groupBoxTabellData.TabIndex = 24;
        this.groupBoxTabellData.TabStop = false;
        // 
        // label28
        // 
        this.label28.AutoSize = true;
        this.label28.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label28.Location = new System.Drawing.Point(436, 187);
        this.label28.Name = "label28";
        this.label28.Size = new System.Drawing.Size(56, 13);
        this.label28.TabIndex = 35;
        this.label28.Text = "Avlägg6:";
        // 
        // label29
        // 
        this.label29.AutoSize = true;
        this.label29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label29.Location = new System.Drawing.Point(436, 160);
        this.label29.Name = "label29";
        this.label29.Size = new System.Drawing.Size(56, 13);
        this.label29.TabIndex = 34;
        this.label29.Text = "Avlägg5:";
        // 
        // label30
        // 
        this.label30.AutoSize = true;
        this.label30.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label30.Location = new System.Drawing.Point(436, 132);
        this.label30.Name = "label30";
        this.label30.Size = new System.Drawing.Size(56, 13);
        this.label30.TabIndex = 33;
        this.label30.Text = "Avlägg4:";
        // 
        // groupBox9
        // 
        this.groupBox9.Controls.Add(this.textBoxAvlagg6Ost);
        this.groupBox9.Controls.Add(this.textBoxAvlagg5Ost);
        this.groupBox9.Controls.Add(this.textBoxAvlagg4Ost);
        this.groupBox9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.groupBox9.Location = new System.Drawing.Point(579, 109);
        this.groupBox9.Name = "groupBox9";
        this.groupBox9.Size = new System.Drawing.Size(79, 104);
        this.groupBox9.TabIndex = 37;
        this.groupBox9.TabStop = false;
        this.groupBox9.Text = "Ost";
        // 
        // textBoxAvlagg6Ost
        // 
        this.textBoxAvlagg6Ost.Location = new System.Drawing.Point(8, 75);
        this.textBoxAvlagg6Ost.MaxLength = 8;
        this.textBoxAvlagg6Ost.Name = "textBoxAvlagg6Ost";
        this.textBoxAvlagg6Ost.Size = new System.Drawing.Size(63, 21);
        this.textBoxAvlagg6Ost.TabIndex = 2;
        this.textBoxAvlagg6Ost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // textBoxAvlagg5Ost
        // 
        this.textBoxAvlagg5Ost.Location = new System.Drawing.Point(8, 48);
        this.textBoxAvlagg5Ost.MaxLength = 8;
        this.textBoxAvlagg5Ost.Name = "textBoxAvlagg5Ost";
        this.textBoxAvlagg5Ost.Size = new System.Drawing.Size(63, 21);
        this.textBoxAvlagg5Ost.TabIndex = 1;
        this.textBoxAvlagg5Ost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // textBoxAvlagg4Ost
        // 
        this.textBoxAvlagg4Ost.Location = new System.Drawing.Point(8, 20);
        this.textBoxAvlagg4Ost.MaxLength = 8;
        this.textBoxAvlagg4Ost.Name = "textBoxAvlagg4Ost";
        this.textBoxAvlagg4Ost.Size = new System.Drawing.Size(63, 21);
        this.textBoxAvlagg4Ost.TabIndex = 0;
        this.textBoxAvlagg4Ost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // groupBox10
        // 
        this.groupBox10.Controls.Add(this.textBoxAvlagg6Norr);
        this.groupBox10.Controls.Add(this.textBoxAvlagg5Norr);
        this.groupBox10.Controls.Add(this.textBoxAvlagg4Norr);
        this.groupBox10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.groupBox10.Location = new System.Drawing.Point(502, 109);
        this.groupBox10.Name = "groupBox10";
        this.groupBox10.Size = new System.Drawing.Size(79, 104);
        this.groupBox10.TabIndex = 36;
        this.groupBox10.TabStop = false;
        this.groupBox10.Text = "Norr";
        // 
        // textBoxAvlagg6Norr
        // 
        this.textBoxAvlagg6Norr.Location = new System.Drawing.Point(8, 75);
        this.textBoxAvlagg6Norr.MaxLength = 8;
        this.textBoxAvlagg6Norr.Name = "textBoxAvlagg6Norr";
        this.textBoxAvlagg6Norr.Size = new System.Drawing.Size(63, 21);
        this.textBoxAvlagg6Norr.TabIndex = 2;
        this.textBoxAvlagg6Norr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // textBoxAvlagg5Norr
        // 
        this.textBoxAvlagg5Norr.Location = new System.Drawing.Point(8, 48);
        this.textBoxAvlagg5Norr.MaxLength = 8;
        this.textBoxAvlagg5Norr.Name = "textBoxAvlagg5Norr";
        this.textBoxAvlagg5Norr.Size = new System.Drawing.Size(63, 21);
        this.textBoxAvlagg5Norr.TabIndex = 1;
        this.textBoxAvlagg5Norr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // textBoxAvlagg4Norr
        // 
        this.textBoxAvlagg4Norr.Location = new System.Drawing.Point(8, 20);
        this.textBoxAvlagg4Norr.MaxLength = 8;
        this.textBoxAvlagg4Norr.Name = "textBoxAvlagg4Norr";
        this.textBoxAvlagg4Norr.Size = new System.Drawing.Size(63, 21);
        this.textBoxAvlagg4Norr.TabIndex = 0;
        this.textBoxAvlagg4Norr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // label25
        // 
        this.label25.AutoSize = true;
        this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label25.Location = new System.Drawing.Point(192, 188);
        this.label25.Name = "label25";
        this.label25.Size = new System.Drawing.Size(56, 13);
        this.label25.TabIndex = 30;
        this.label25.Text = "Avlägg3:";
        // 
        // label26
        // 
        this.label26.AutoSize = true;
        this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label26.Location = new System.Drawing.Point(192, 161);
        this.label26.Name = "label26";
        this.label26.Size = new System.Drawing.Size(56, 13);
        this.label26.TabIndex = 29;
        this.label26.Text = "Avlägg2:";
        // 
        // groupBox8
        // 
        this.groupBox8.Controls.Add(this.textBoxAvlagg3Ost);
        this.groupBox8.Controls.Add(this.textBoxAvlagg2Ost);
        this.groupBox8.Controls.Add(this.textBoxAvlagg1Ost);
        this.groupBox8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.groupBox8.Location = new System.Drawing.Point(337, 110);
        this.groupBox8.Name = "groupBox8";
        this.groupBox8.Size = new System.Drawing.Size(79, 104);
        this.groupBox8.TabIndex = 32;
        this.groupBox8.TabStop = false;
        this.groupBox8.Text = "Ost";
        // 
        // textBoxAvlagg3Ost
        // 
        this.textBoxAvlagg3Ost.Location = new System.Drawing.Point(8, 75);
        this.textBoxAvlagg3Ost.MaxLength = 8;
        this.textBoxAvlagg3Ost.Name = "textBoxAvlagg3Ost";
        this.textBoxAvlagg3Ost.Size = new System.Drawing.Size(63, 21);
        this.textBoxAvlagg3Ost.TabIndex = 2;
        this.textBoxAvlagg3Ost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // textBoxAvlagg2Ost
        // 
        this.textBoxAvlagg2Ost.Location = new System.Drawing.Point(8, 48);
        this.textBoxAvlagg2Ost.MaxLength = 8;
        this.textBoxAvlagg2Ost.Name = "textBoxAvlagg2Ost";
        this.textBoxAvlagg2Ost.Size = new System.Drawing.Size(63, 21);
        this.textBoxAvlagg2Ost.TabIndex = 1;
        this.textBoxAvlagg2Ost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // textBoxAvlagg1Ost
        // 
        this.textBoxAvlagg1Ost.Location = new System.Drawing.Point(8, 20);
        this.textBoxAvlagg1Ost.MaxLength = 8;
        this.textBoxAvlagg1Ost.Name = "textBoxAvlagg1Ost";
        this.textBoxAvlagg1Ost.Size = new System.Drawing.Size(63, 21);
        this.textBoxAvlagg1Ost.TabIndex = 0;
        this.textBoxAvlagg1Ost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // groupBox7
        // 
        this.groupBox7.Controls.Add(this.textBoxAvlagg3Norr);
        this.groupBox7.Controls.Add(this.textBoxAvlagg2Norr);
        this.groupBox7.Controls.Add(this.textBoxAvlagg1Norr);
        this.groupBox7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.groupBox7.Location = new System.Drawing.Point(260, 110);
        this.groupBox7.Name = "groupBox7";
        this.groupBox7.Size = new System.Drawing.Size(79, 104);
        this.groupBox7.TabIndex = 31;
        this.groupBox7.TabStop = false;
        this.groupBox7.Text = "Norr";
        // 
        // textBoxAvlagg3Norr
        // 
        this.textBoxAvlagg3Norr.Location = new System.Drawing.Point(8, 75);
        this.textBoxAvlagg3Norr.MaxLength = 8;
        this.textBoxAvlagg3Norr.Name = "textBoxAvlagg3Norr";
        this.textBoxAvlagg3Norr.Size = new System.Drawing.Size(63, 21);
        this.textBoxAvlagg3Norr.TabIndex = 2;
        this.textBoxAvlagg3Norr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // textBoxAvlagg2Norr
        // 
        this.textBoxAvlagg2Norr.Location = new System.Drawing.Point(8, 48);
        this.textBoxAvlagg2Norr.MaxLength = 8;
        this.textBoxAvlagg2Norr.Name = "textBoxAvlagg2Norr";
        this.textBoxAvlagg2Norr.Size = new System.Drawing.Size(63, 21);
        this.textBoxAvlagg2Norr.TabIndex = 1;
        this.textBoxAvlagg2Norr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // textBoxAvlagg1Norr
        // 
        this.textBoxAvlagg1Norr.Location = new System.Drawing.Point(8, 18);
        this.textBoxAvlagg1Norr.MaxLength = 8;
        this.textBoxAvlagg1Norr.Name = "textBoxAvlagg1Norr";
        this.textBoxAvlagg1Norr.Size = new System.Drawing.Size(63, 21);
        this.textBoxAvlagg1Norr.TabIndex = 0;
        this.textBoxAvlagg1Norr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // label27
        // 
        this.label27.AutoSize = true;
        this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label27.Location = new System.Drawing.Point(192, 136);
        this.label27.Name = "label27";
        this.label27.Size = new System.Drawing.Size(56, 13);
        this.label27.TabIndex = 28;
        this.label27.Text = "Avlägg1:";
        // 
        // checkBoxTraktordragenHugg
        // 
        this.checkBoxTraktordragenHugg.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
        this.checkBoxTraktordragenHugg.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.checkBoxTraktordragenHugg.Location = new System.Drawing.Point(14, 166);
        this.checkBoxTraktordragenHugg.Name = "checkBoxTraktordragenHugg";
        this.checkBoxTraktordragenHugg.Size = new System.Drawing.Size(144, 17);
        this.checkBoxTraktordragenHugg.TabIndex = 26;
        this.checkBoxTraktordragenHugg.Text = "Traktordragen hugg";
        this.checkBoxTraktordragenHugg.UseVisualStyleBackColor = true;
        // 
        // checkBoxLatbilshugg
        // 
        this.checkBoxLatbilshugg.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
        this.checkBoxLatbilshugg.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.checkBoxLatbilshugg.Location = new System.Drawing.Point(14, 116);
        this.checkBoxLatbilshugg.Name = "checkBoxLatbilshugg";
        this.checkBoxLatbilshugg.Size = new System.Drawing.Size(144, 18);
        this.checkBoxLatbilshugg.TabIndex = 24;
        this.checkBoxLatbilshugg.Text = "Latbilshugg";
        this.checkBoxLatbilshugg.UseVisualStyleBackColor = true;
        // 
        // checkBoxGrotbil
        // 
        this.checkBoxGrotbil.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
        this.checkBoxGrotbil.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.checkBoxGrotbil.Location = new System.Drawing.Point(14, 140);
        this.checkBoxGrotbil.Name = "checkBoxGrotbil";
        this.checkBoxGrotbil.Size = new System.Drawing.Size(144, 17);
        this.checkBoxGrotbil.TabIndex = 25;
        this.checkBoxGrotbil.Text = "Grotbil";
        this.checkBoxGrotbil.UseVisualStyleBackColor = true;
        // 
        // checkBoxSkotarburenHugg
        // 
        this.checkBoxSkotarburenHugg.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
        this.checkBoxSkotarburenHugg.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.checkBoxSkotarburenHugg.Location = new System.Drawing.Point(14, 189);
        this.checkBoxSkotarburenHugg.Name = "checkBoxSkotarburenHugg";
        this.checkBoxSkotarburenHugg.Size = new System.Drawing.Size(144, 17);
        this.checkBoxSkotarburenHugg.TabIndex = 27;
        this.checkBoxSkotarburenHugg.Text = "Skotarburen hugg";
        this.checkBoxSkotarburenHugg.UseVisualStyleBackColor = true;
        // 
        // label6
        // 
        this.label6.AutoSize = true;
        this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label6.Location = new System.Drawing.Point(13, 19);
        this.label6.Name = "label6";
        this.label6.Size = new System.Drawing.Size(40, 13);
        this.label6.TabIndex = 0;
        this.label6.Text = "Areal:";
        // 
        // textBoxAntalVältor
        // 
        this.textBoxAntalVältor.AcceptsReturn = true;
        this.textBoxAntalVältor.Location = new System.Drawing.Point(14, 84);
        this.textBoxAntalVältor.MaxLength = 16;
        this.textBoxAntalVältor.Name = "textBoxAntalVältor";
        this.textBoxAntalVältor.Size = new System.Drawing.Size(90, 21);
        this.textBoxAntalVältor.TabIndex = 13;
        // 
        // label13
        // 
        this.label13.AutoSize = true;
        this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label13.Location = new System.Drawing.Point(453, 19);
        this.label13.Name = "label13";
        this.label13.Size = new System.Drawing.Size(95, 13);
        this.label13.TabIndex = 8;
        this.label13.Text = "Provytestorlek:";
        // 
        // textBoxVägkant
        // 
        this.textBoxVägkant.Location = new System.Drawing.Point(456, 84);
        this.textBoxVägkant.MaxLength = 16;
        this.textBoxVägkant.Name = "textBoxVägkant";
        this.textBoxVägkant.Size = new System.Drawing.Size(90, 21);
        this.textBoxVägkant.TabIndex = 21;
        // 
        // label21
        // 
        this.label21.AutoSize = true;
        this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label21.Location = new System.Drawing.Point(343, 19);
        this.label21.Name = "label21";
        this.label21.Size = new System.Drawing.Size(64, 13);
        this.label21.TabIndex = 6;
        this.label21.Text = "GIS-areal:";
        // 
        // textBoxHygge
        // 
        this.textBoxHygge.Location = new System.Drawing.Point(347, 84);
        this.textBoxHygge.MaxLength = 16;
        this.textBoxHygge.Name = "textBoxHygge";
        this.textBoxHygge.Size = new System.Drawing.Size(90, 21);
        this.textBoxHygge.TabIndex = 19;
        // 
        // label22
        // 
        this.label22.AutoSize = true;
        this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label22.Location = new System.Drawing.Point(233, 19);
        this.label22.Name = "label22";
        this.label22.Size = new System.Drawing.Size(78, 13);
        this.label22.TabIndex = 4;
        this.label22.Text = "GIS-sträcka:";
        // 
        // textBoxBedömdVolym
        // 
        this.textBoxBedömdVolym.AcceptsTab = true;
        this.textBoxBedömdVolym.Location = new System.Drawing.Point(237, 84);
        this.textBoxBedömdVolym.MaxLength = 16;
        this.textBoxBedömdVolym.Name = "textBoxBedömdVolym";
        this.textBoxBedömdVolym.Size = new System.Drawing.Size(90, 21);
        this.textBoxBedömdVolym.TabIndex = 17;
        // 
        // label23
        // 
        this.label23.AutoSize = true;
        this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label23.Location = new System.Drawing.Point(563, 19);
        this.label23.Name = "label23";
        this.label23.Size = new System.Drawing.Size(111, 13);
        this.label23.TabIndex = 10;
        this.label23.Text = "Stickvägsavstånd:";
        // 
        // textBoxÅker
        // 
        this.textBoxÅker.Location = new System.Drawing.Point(569, 84);
        this.textBoxÅker.MaxLength = 16;
        this.textBoxÅker.Name = "textBoxÅker";
        this.textBoxÅker.Size = new System.Drawing.Size(90, 21);
        this.textBoxÅker.TabIndex = 23;
        // 
        // label24
        // 
        this.label24.AutoSize = true;
        this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label24.Location = new System.Drawing.Point(124, 19);
        this.label24.Name = "label24";
        this.label24.Size = new System.Drawing.Size(82, 13);
        this.label24.TabIndex = 2;
        this.label24.Text = "Målgrundyta:";
        // 
        // textBoxAvstånd
        // 
        this.textBoxAvstånd.AcceptsTab = true;
        this.textBoxAvstånd.Location = new System.Drawing.Point(127, 84);
        this.textBoxAvstånd.MaxLength = 16;
        this.textBoxAvstånd.Name = "textBoxAvstånd";
        this.textBoxAvstånd.Size = new System.Drawing.Size(90, 21);
        this.textBoxAvstånd.TabIndex = 15;
        // 
        // label19
        // 
        this.label19.AutoSize = true;
        this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label19.Location = new System.Drawing.Point(14, 68);
        this.label19.Name = "label19";
        this.label19.Size = new System.Drawing.Size(77, 13);
        this.label19.TabIndex = 12;
        this.label19.Text = "Antal vältor:";
        // 
        // textBoxAreal
        // 
        this.textBoxAreal.Location = new System.Drawing.Point(14, 38);
        this.textBoxAreal.MaxLength = 16;
        this.textBoxAreal.Name = "textBoxAreal";
        this.textBoxAreal.Size = new System.Drawing.Size(90, 21);
        this.textBoxAreal.TabIndex = 1;
        // 
        // label14
        // 
        this.label14.AutoSize = true;
        this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label14.Location = new System.Drawing.Point(454, 68);
        this.label14.Name = "label14";
        this.label14.Size = new System.Drawing.Size(57, 13);
        this.label14.TabIndex = 20;
        this.label14.Text = "Vägkant:";
        // 
        // textBoxProvytestorlek
        // 
        this.textBoxProvytestorlek.Location = new System.Drawing.Point(456, 38);
        this.textBoxProvytestorlek.MaxLength = 16;
        this.textBoxProvytestorlek.Name = "textBoxProvytestorlek";
        this.textBoxProvytestorlek.Size = new System.Drawing.Size(90, 21);
        this.textBoxProvytestorlek.TabIndex = 9;
        // 
        // label15
        // 
        this.label15.AutoSize = true;
        this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label15.Location = new System.Drawing.Point(344, 68);
        this.label15.Name = "label15";
        this.label15.Size = new System.Drawing.Size(46, 13);
        this.label15.TabIndex = 18;
        this.label15.Text = "Hygge:";
        // 
        // textBoxGISAreal
        // 
        this.textBoxGISAreal.Location = new System.Drawing.Point(347, 38);
        this.textBoxGISAreal.MaxLength = 16;
        this.textBoxGISAreal.Name = "textBoxGISAreal";
        this.textBoxGISAreal.Size = new System.Drawing.Size(90, 21);
        this.textBoxGISAreal.TabIndex = 7;
        // 
        // label16
        // 
        this.label16.AutoSize = true;
        this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label16.Location = new System.Drawing.Point(234, 68);
        this.label16.Name = "label16";
        this.label16.Size = new System.Drawing.Size(94, 13);
        this.label16.TabIndex = 16;
        this.label16.Text = "Bedömd volym:";
        // 
        // textBoxGISSträcka
        // 
        this.textBoxGISSträcka.Location = new System.Drawing.Point(237, 38);
        this.textBoxGISSträcka.MaxLength = 16;
        this.textBoxGISSträcka.Name = "textBoxGISSträcka";
        this.textBoxGISSträcka.Size = new System.Drawing.Size(90, 21);
        this.textBoxGISSträcka.TabIndex = 5;
        // 
        // label17
        // 
        this.label17.AutoSize = true;
        this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label17.Location = new System.Drawing.Point(564, 68);
        this.label17.Name = "label17";
        this.label17.Size = new System.Drawing.Size(37, 13);
        this.label17.TabIndex = 22;
        this.label17.Text = "Åker:";
        // 
        // textBoxStickvägsavstånd
        // 
        this.textBoxStickvägsavstånd.AcceptsTab = true;
        this.textBoxStickvägsavstånd.Location = new System.Drawing.Point(569, 38);
        this.textBoxStickvägsavstånd.MaxLength = 16;
        this.textBoxStickvägsavstånd.Name = "textBoxStickvägsavstånd";
        this.textBoxStickvägsavstånd.Size = new System.Drawing.Size(90, 21);
        this.textBoxStickvägsavstånd.TabIndex = 11;
        // 
        // label18
        // 
        this.label18.AutoSize = true;
        this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label18.Location = new System.Drawing.Point(125, 68);
        this.label18.Name = "label18";
        this.label18.Size = new System.Drawing.Size(57, 13);
        this.label18.TabIndex = 14;
        this.label18.Text = "Avstånd:";
        // 
        // textBoxMålgrundyta
        // 
        this.textBoxMålgrundyta.Location = new System.Drawing.Point(127, 38);
        this.textBoxMålgrundyta.MaxLength = 16;
        this.textBoxMålgrundyta.Name = "textBoxMålgrundyta";
        this.textBoxMålgrundyta.Size = new System.Drawing.Size(90, 21);
        this.textBoxMålgrundyta.TabIndex = 3;
        // 
        // buttonTömAllaFilter
        // 
        this.buttonTömAllaFilter.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.buttonTömAllaFilter.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonTömAllaFilter.ImageIndex = 1;
        this.buttonTömAllaFilter.ImageList = this.imageListFilter;
        this.buttonTömAllaFilter.Location = new System.Drawing.Point(267, 425);
        this.buttonTömAllaFilter.Name = "buttonTömAllaFilter";
        this.buttonTömAllaFilter.Size = new System.Drawing.Size(144, 30);
        this.buttonTömAllaFilter.TabIndex = 26;
        this.buttonTömAllaFilter.Text = "Töm alla filter";
        this.buttonTömAllaFilter.UseVisualStyleBackColor = true;
        this.buttonTömAllaFilter.Click += new System.EventHandler(this.buttonTömAllaFilter_Click);
        // 
        // imageListFilter
        // 
        this.imageListFilter.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListFilter.ImageStream")));
        this.imageListFilter.TransparentColor = System.Drawing.Color.Transparent;
        this.imageListFilter.Images.SetKeyName(0, "UppdateraFilter.ico");
        this.imageListFilter.Images.SetKeyName(1, "TömFilter.ico");
        this.imageListFilter.Images.SetKeyName(2, "Neka.ico");
        // 
        // comboBoxMarkberedning
        // 
        this.comboBoxMarkberedning.DataSource = this.dataSetFilter;
        this.comboBoxMarkberedning.DisplayMember = "Markberedningsmetod.Namn";
        this.comboBoxMarkberedning.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxMarkberedning.FormattingEnabled = true;
        this.comboBoxMarkberedning.Location = new System.Drawing.Point(148, 127);
        this.comboBoxMarkberedning.Name = "comboBoxMarkberedning";
        this.comboBoxMarkberedning.Size = new System.Drawing.Size(158, 21);
        this.comboBoxMarkberedning.TabIndex = 12;
        // 
        // comboBoxUrsprung
        // 
        this.comboBoxUrsprung.DataSource = this.dataSetFilter;
        this.comboBoxUrsprung.DisplayMember = "Ursprung.Namn";
        this.comboBoxUrsprung.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxUrsprung.FormattingEnabled = true;
        this.comboBoxUrsprung.Location = new System.Drawing.Point(13, 127);
        this.comboBoxUrsprung.Name = "comboBoxUrsprung";
        this.comboBoxUrsprung.Size = new System.Drawing.Size(116, 21);
        this.comboBoxUrsprung.TabIndex = 11;
        // 
        // comboBoxÅrtal
        // 
        this.comboBoxÅrtal.DataSource = this.dataSetFilter;
        this.comboBoxÅrtal.DisplayMember = "Artal.Id";
        this.comboBoxÅrtal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxÅrtal.FormattingEnabled = true;
        this.comboBoxÅrtal.Location = new System.Drawing.Point(554, 79);
        this.comboBoxÅrtal.Name = "comboBoxÅrtal";
        this.comboBoxÅrtal.Size = new System.Drawing.Size(116, 21);
        this.comboBoxÅrtal.TabIndex = 10;
        // 
        // buttonAvbryt
        // 
        this.buttonAvbryt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.buttonAvbryt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonAvbryt.ImageIndex = 2;
        this.buttonAvbryt.ImageList = this.imageListFilter;
        this.buttonAvbryt.Location = new System.Drawing.Point(534, 425);
        this.buttonAvbryt.Name = "buttonAvbryt";
        this.buttonAvbryt.Size = new System.Drawing.Size(144, 30);
        this.buttonAvbryt.TabIndex = 27;
        this.buttonAvbryt.Text = "Avbryt";
        this.buttonAvbryt.UseVisualStyleBackColor = true;
        this.buttonAvbryt.Click += new System.EventHandler(this.buttonAvbryt_Click);
        // 
        // buttonUppdatera
        // 
        this.buttonUppdatera.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.buttonUppdatera.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonUppdatera.ImageIndex = 0;
        this.buttonUppdatera.ImageList = this.imageListFilter;
        this.buttonUppdatera.Location = new System.Drawing.Point(11, 425);
        this.buttonUppdatera.Name = "buttonUppdatera";
        this.buttonUppdatera.Size = new System.Drawing.Size(144, 30);
        this.buttonUppdatera.TabIndex = 25;
        this.buttonUppdatera.Text = "Uppdatera tabell";
        this.buttonUppdatera.UseVisualStyleBackColor = true;
        this.buttonUppdatera.Click += new System.EventHandler(this.buttonUppdatera_Click);
        // 
        // comboBoxTraktDelId
        // 
        this.comboBoxTraktDelId.DataSource = this.dataSetFilter;
        this.comboBoxTraktDelId.DisplayMember = "TraktDel.Id";
        this.comboBoxTraktDelId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxTraktDelId.FormattingEnabled = true;
        this.comboBoxTraktDelId.Location = new System.Drawing.Point(554, 31);
        this.comboBoxTraktDelId.Name = "comboBoxTraktDelId";
        this.comboBoxTraktDelId.Size = new System.Drawing.Size(116, 21);
        this.comboBoxTraktDelId.TabIndex = 5;
        this.comboBoxTraktDelId.ValueMember = "TraktDel.Id";
        // 
        // comboBoxEntreprenörNamn
        // 
        this.comboBoxEntreprenörNamn.DataSource = this.dataSetFilter;
        this.comboBoxEntreprenörNamn.DisplayMember = "Entreprenör.Namn";
        this.comboBoxEntreprenörNamn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxEntreprenörNamn.FormattingEnabled = true;
        this.comboBoxEntreprenörNamn.Location = new System.Drawing.Point(418, 79);
        this.comboBoxEntreprenörNamn.Name = "comboBoxEntreprenörNamn";
        this.comboBoxEntreprenörNamn.Size = new System.Drawing.Size(116, 21);
        this.comboBoxEntreprenörNamn.TabIndex = 9;
        // 
        // label20
        // 
        this.label20.AutoSize = true;
        this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label20.Location = new System.Drawing.Point(415, 63);
        this.label20.Name = "label20";
        this.label20.Size = new System.Drawing.Size(113, 13);
        this.label20.TabIndex = 16;
        this.label20.Text = "Entreprenör Namn:";
        // 
        // comboBoxEntreprenörId
        // 
        this.comboBoxEntreprenörId.DataSource = this.dataSetFilter;
        this.comboBoxEntreprenörId.DisplayMember = "Entreprenör.Id";
        this.comboBoxEntreprenörId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxEntreprenörId.FormattingEnabled = true;
        this.comboBoxEntreprenörId.Location = new System.Drawing.Point(418, 31);
        this.comboBoxEntreprenörId.Name = "comboBoxEntreprenörId";
        this.comboBoxEntreprenörId.Size = new System.Drawing.Size(116, 21);
        this.comboBoxEntreprenörId.TabIndex = 4;
        this.comboBoxEntreprenörId.ValueMember = "Region.Id";
        // 
        // comboBoxTraktNamn
        // 
        this.comboBoxTraktNamn.DataSource = this.dataSetFilter;
        this.comboBoxTraktNamn.DisplayMember = "Trakt.Namn";
        this.comboBoxTraktNamn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxTraktNamn.FormattingEnabled = true;
        this.comboBoxTraktNamn.Location = new System.Drawing.Point(281, 79);
        this.comboBoxTraktNamn.Name = "comboBoxTraktNamn";
        this.comboBoxTraktNamn.Size = new System.Drawing.Size(116, 21);
        this.comboBoxTraktNamn.TabIndex = 8;
        // 
        // comboBoxTraktNr
        // 
        this.comboBoxTraktNr.DataSource = this.dataSetFilter;
        this.comboBoxTraktNr.DisplayMember = "Trakt.Id";
        this.comboBoxTraktNr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxTraktNr.FormatString = "000000";
        this.comboBoxTraktNr.FormattingEnabled = true;
        this.comboBoxTraktNr.Location = new System.Drawing.Point(281, 31);
        this.comboBoxTraktNr.Name = "comboBoxTraktNr";
        this.comboBoxTraktNr.Size = new System.Drawing.Size(116, 21);
        this.comboBoxTraktNr.TabIndex = 3;
        this.comboBoxTraktNr.ValueMember = "Region.Id";
        // 
        // comboBoxDistriktId
        // 
        this.comboBoxDistriktId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxDistriktId.FormattingEnabled = true;
        this.comboBoxDistriktId.Location = new System.Drawing.Point(148, 31);
        this.comboBoxDistriktId.Name = "comboBoxDistriktId";
        this.comboBoxDistriktId.Size = new System.Drawing.Size(116, 21);
        this.comboBoxDistriktId.TabIndex = 2;
        this.comboBoxDistriktId.SelectedIndexChanged += new System.EventHandler(this.comboBoxDistriktId_SelectedIndexChanged);
        // 
        // comboBoxDistriktNamn
        // 
        this.comboBoxDistriktNamn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxDistriktNamn.FormattingEnabled = true;
        this.comboBoxDistriktNamn.Location = new System.Drawing.Point(148, 79);
        this.comboBoxDistriktNamn.Name = "comboBoxDistriktNamn";
        this.comboBoxDistriktNamn.Size = new System.Drawing.Size(116, 21);
        this.comboBoxDistriktNamn.TabIndex = 7;
        this.comboBoxDistriktNamn.SelectedIndexChanged += new System.EventHandler(this.comboBoxDistriktNamn_SelectedIndexChanged);
        // 
        // label7
        // 
        this.label7.AutoSize = true;
        this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label7.Location = new System.Drawing.Point(145, 111);
        this.label7.Name = "label7";
        this.label7.Size = new System.Drawing.Size(139, 13);
        this.label7.TabIndex = 22;
        this.label7.Text = "Markberedningsmetod:";
        // 
        // comboBoxRegionNamn
        // 
        this.comboBoxRegionNamn.DataSource = this.dataSetFilter;
        this.comboBoxRegionNamn.DisplayMember = "Region.Namn";
        this.comboBoxRegionNamn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxRegionNamn.FormattingEnabled = true;
        this.comboBoxRegionNamn.Location = new System.Drawing.Point(14, 79);
        this.comboBoxRegionNamn.Name = "comboBoxRegionNamn";
        this.comboBoxRegionNamn.Size = new System.Drawing.Size(116, 21);
        this.comboBoxRegionNamn.TabIndex = 6;
        this.comboBoxRegionNamn.ValueMember = "Region.Namn";
        // 
        // comboBoxRegionId
        // 
        this.comboBoxRegionId.DataSource = this.dataSetFilter;
        this.comboBoxRegionId.DisplayMember = "Region.Id";
        this.comboBoxRegionId.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxRegionId.FormattingEnabled = true;
        this.comboBoxRegionId.Location = new System.Drawing.Point(14, 31);
        this.comboBoxRegionId.Name = "comboBoxRegionId";
        this.comboBoxRegionId.Size = new System.Drawing.Size(116, 21);
        this.comboBoxRegionId.TabIndex = 1;
        this.comboBoxRegionId.ValueMember = "Region.Id";
        this.comboBoxRegionId.SelectedIndexChanged += new System.EventHandler(this.comboBoxRegionId_SelectedIndexChanged);
        // 
        // label9
        // 
        this.label9.AutoSize = true;
        this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label9.Location = new System.Drawing.Point(11, 111);
        this.label9.Name = "label9";
        this.label9.Size = new System.Drawing.Size(62, 13);
        this.label9.TabIndex = 20;
        this.label9.Text = "Ursprung:";
        // 
        // label5
        // 
        this.label5.AutoSize = true;
        this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label5.Location = new System.Drawing.Point(414, 16);
        this.label5.Name = "label5";
        this.label5.Size = new System.Drawing.Size(90, 13);
        this.label5.TabIndex = 6;
        this.label5.Text = "Entreprenörnr:";
        // 
        // label4
        // 
        this.label4.AutoSize = true;
        this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label4.Location = new System.Drawing.Point(551, 16);
        this.label4.Name = "label4";
        this.label4.Size = new System.Drawing.Size(58, 13);
        this.label4.TabIndex = 8;
        this.label4.Text = "Traktdel:";
        // 
        // label3
        // 
        this.label3.AutoSize = true;
        this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label3.Location = new System.Drawing.Point(278, 16);
        this.label3.Name = "label3";
        this.label3.Size = new System.Drawing.Size(56, 13);
        this.label3.TabIndex = 4;
        this.label3.Text = "Trakt Nr:";
        // 
        // label2
        // 
        this.label2.AutoSize = true;
        this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label2.Location = new System.Drawing.Point(145, 16);
        this.label2.Name = "label2";
        this.label2.Size = new System.Drawing.Size(67, 13);
        this.label2.TabIndex = 2;
        this.label2.Text = "Distrikt Id:";
        // 
        // label1
        // 
        this.label1.AutoSize = true;
        this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label1.Location = new System.Drawing.Point(10, 16);
        this.label1.Name = "label1";
        this.label1.Size = new System.Drawing.Size(64, 13);
        this.label1.TabIndex = 0;
        this.label1.Text = "Region Id:";
        // 
        // label8
        // 
        this.label8.AutoSize = true;
        this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label8.Location = new System.Drawing.Point(551, 63);
        this.label8.Name = "label8";
        this.label8.Size = new System.Drawing.Size(38, 13);
        this.label8.TabIndex = 18;
        this.label8.Text = "Årtal:";
        // 
        // label11
        // 
        this.label11.AutoSize = true;
        this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label11.Location = new System.Drawing.Point(10, 63);
        this.label11.Name = "label11";
        this.label11.Size = new System.Drawing.Size(84, 13);
        this.label11.TabIndex = 10;
        this.label11.Text = "Region Namn:";
        // 
        // label10
        // 
        this.label10.AutoSize = true;
        this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label10.Location = new System.Drawing.Point(145, 63);
        this.label10.Name = "label10";
        this.label10.Size = new System.Drawing.Size(87, 13);
        this.label10.TabIndex = 12;
        this.label10.Text = "Distrikt Namn:";
        // 
        // label12
        // 
        this.label12.AutoSize = true;
        this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label12.Location = new System.Drawing.Point(278, 63);
        this.label12.Name = "label12";
        this.label12.Size = new System.Drawing.Size(76, 13);
        this.label12.TabIndex = 14;
        this.label12.Text = "Trakt Namn:";
        // 
        // groupBoxFiltreringsalternativ
        // 
        this.groupBoxFiltreringsalternativ.Controls.Add(this.radioButtonAllaRapportTyper);
        this.groupBoxFiltreringsalternativ.Controls.Add(this.radioButtonValdRapportTyp);
        this.groupBoxFiltreringsalternativ.Location = new System.Drawing.Point(7, 3);
        this.groupBoxFiltreringsalternativ.Name = "groupBoxFiltreringsalternativ";
        this.groupBoxFiltreringsalternativ.Size = new System.Drawing.Size(688, 50);
        this.groupBoxFiltreringsalternativ.TabIndex = 1;
        this.groupBoxFiltreringsalternativ.TabStop = false;
        this.groupBoxFiltreringsalternativ.Text = "Visa filtreringsalternativ";
        // 
        // radioButtonAllaRapportTyper
        // 
        this.radioButtonAllaRapportTyper.AutoSize = true;
        this.radioButtonAllaRapportTyper.Location = new System.Drawing.Point(188, 21);
        this.radioButtonAllaRapportTyper.Name = "radioButtonAllaRapportTyper";
        this.radioButtonAllaRapportTyper.Size = new System.Drawing.Size(118, 17);
        this.radioButtonAllaRapportTyper.TabIndex = 1;
        this.radioButtonAllaRapportTyper.Text = "Alla rapporttyper";
        this.radioButtonAllaRapportTyper.UseVisualStyleBackColor = true;
        this.radioButtonAllaRapportTyper.CheckedChanged += new System.EventHandler(this.radioButtonAllaRapportTyper_CheckedChanged);
        // 
        // radioButtonValdRapportTyp
        // 
        this.radioButtonValdRapportTyp.AutoSize = true;
        this.radioButtonValdRapportTyp.Checked = true;
        this.radioButtonValdRapportTyp.Location = new System.Drawing.Point(19, 21);
        this.radioButtonValdRapportTyp.Name = "radioButtonValdRapportTyp";
        this.radioButtonValdRapportTyp.Size = new System.Drawing.Size(111, 17);
        this.radioButtonValdRapportTyp.TabIndex = 0;
        this.radioButtonValdRapportTyp.TabStop = true;
        this.radioButtonValdRapportTyp.Text = "Vald rapporttyp";
        this.radioButtonValdRapportTyp.UseVisualStyleBackColor = true;
        this.radioButtonValdRapportTyp.CheckedChanged += new System.EventHandler(this.radioButtonValdRapportTyp_CheckedChanged);
        // 
        // FormFiltreraRapporter
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.ClientSize = new System.Drawing.Size(703, 508);
        this.Controls.Add(this.groupBoxFiltreringsalternativ);
        this.Controls.Add(this.groupBoxNyckelvärden);
        this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
        this.Name = "FormFiltreraRapporter";
        this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
        this.Text = "Filtrera rapporter för ";
        this.Load += new System.EventHandler(this.FormFiltreraRapporter_Load);
        this.groupBoxNyckelvärden.ResumeLayout(false);
        this.groupBoxNyckelvärden.PerformLayout();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetFilter)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableRegion)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableDistrikt)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableTraktDel)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableMarkberedningsmetod)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableUrsprung)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableTrakt)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableEntreprenör)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableRapportTyp)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableArtal)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableStickvagssystem)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableDatainsamlingsmetod)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableInvTyp)).EndInit();
        this.groupBoxTabellData.ResumeLayout(false);
        this.groupBoxTabellData.PerformLayout();
        this.groupBox9.ResumeLayout(false);
        this.groupBox9.PerformLayout();
        this.groupBox10.ResumeLayout(false);
        this.groupBox10.PerformLayout();
        this.groupBox8.ResumeLayout(false);
        this.groupBox8.PerformLayout();
        this.groupBox7.ResumeLayout(false);
        this.groupBox7.PerformLayout();
        this.groupBoxFiltreringsalternativ.ResumeLayout(false);
        this.groupBoxFiltreringsalternativ.PerformLayout();
        this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox groupBoxNyckelvärden;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Button buttonUppdatera;
    private System.Windows.Forms.Button buttonAvbryt;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.GroupBox groupBoxTabellData;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.Label label14;
    private System.Windows.Forms.TextBox textBoxProvytestorlek;
    private System.Windows.Forms.Label label15;
    private System.Windows.Forms.TextBox textBoxGISAreal;
    private System.Windows.Forms.Label label16;
    private System.Windows.Forms.TextBox textBoxGISSträcka;
    private System.Windows.Forms.Label label17;
    private System.Windows.Forms.TextBox textBoxStickvägsavstånd;
    private System.Windows.Forms.Label label18;
    private System.Windows.Forms.TextBox textBoxMålgrundyta;
    private System.Windows.Forms.Button buttonTömAllaFilter;
    private System.Windows.Forms.Label label19;
    private System.Windows.Forms.TextBox textBoxAreal;
    private System.Windows.Forms.Label label20;
    private System.Data.DataTable dataTableRegion;
    private System.Data.DataColumn dataColumnFilterRegionId;
    private System.Data.DataColumn dataColumnFilterRegionNamn;
    private System.Data.DataTable dataTableDistrikt;
    private System.Data.DataColumn dataColumnFilterDistriktRegionId;
    private System.Data.DataColumn dataColumnFilterDistriktId;
    private System.Data.DataColumn dataColumnFilterDistriktNamn;
    private System.Data.DataTable dataTableTraktDel;
    private System.Data.DataColumn dataColumnFilterTraktDelId;
    private System.Data.DataTable dataTableMarkberedningsmetod;
    private System.Data.DataColumn dataColumnFilterMarkberedningsmetodNamn;
    private System.Data.DataTable dataTableUrsprung;
    private System.Data.DataColumn dataColumnFilterUrsprungNamn;
    private System.Windows.Forms.ComboBox comboBoxRegionId;
    private System.Data.DataTable dataTableTrakt;
    private System.Data.DataColumn dataColumnFilterTraktId;
    private System.Data.DataColumn dataColumnFilterTraktNamn;
    private System.Data.DataTable dataTableEntreprenör;
    private System.Data.DataColumn dataColumnFilterEntreprenörId;
    private System.Data.DataColumn dataColumnFilterEntreprenörNamn;
    private System.Data.DataTable dataTableRapportTyp;
    private System.Data.DataColumn dataColumnFilterRapportTypId;
    private System.Data.DataColumn dataColumnFilterRapportTypNamn;
    private System.Data.DataTable dataTableArtal;
    private System.Data.DataColumn dataColumnFilterÅrtalId;
    private System.Windows.Forms.ComboBox comboBoxRegionNamn;
    private System.Windows.Forms.ComboBox comboBoxTraktNr;
    private System.Windows.Forms.ComboBox comboBoxDistriktId;
    private System.Windows.Forms.ComboBox comboBoxDistriktNamn;
    private System.Windows.Forms.ComboBox comboBoxÅrtal;
    private System.Windows.Forms.ComboBox comboBoxTraktDelId;
    private System.Windows.Forms.ComboBox comboBoxEntreprenörNamn;
    private System.Windows.Forms.ComboBox comboBoxEntreprenörId;
    private System.Windows.Forms.ComboBox comboBoxTraktNamn;
    private System.Windows.Forms.ComboBox comboBoxMarkberedning;
    private System.Windows.Forms.ComboBox comboBoxUrsprung;
    private System.Windows.Forms.ImageList imageListFilter;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.TextBox textBoxAntalVältor;
    private System.Windows.Forms.Label label13;
    private System.Windows.Forms.TextBox textBoxVägkant;
    private System.Windows.Forms.Label label21;
    private System.Windows.Forms.TextBox textBoxHygge;
    private System.Windows.Forms.Label label22;
    private System.Windows.Forms.TextBox textBoxBedömdVolym;
    private System.Windows.Forms.Label label23;
    private System.Windows.Forms.TextBox textBoxÅker;
    private System.Windows.Forms.Label label24;
    private System.Windows.Forms.TextBox textBoxAvstånd;
    private System.Windows.Forms.CheckBox checkBoxTraktordragenHugg;
    private System.Windows.Forms.CheckBox checkBoxLatbilshugg;
    private System.Windows.Forms.CheckBox checkBoxGrotbil;
    private System.Windows.Forms.CheckBox checkBoxSkotarburenHugg;
    private System.Windows.Forms.Label label25;
    private System.Windows.Forms.Label label26;
    private System.Windows.Forms.GroupBox groupBox8;
    private System.Windows.Forms.TextBox textBoxAvlagg3Ost;
    private System.Windows.Forms.TextBox textBoxAvlagg2Ost;
    private System.Windows.Forms.TextBox textBoxAvlagg1Ost;
    private System.Windows.Forms.GroupBox groupBox7;
    private System.Windows.Forms.TextBox textBoxAvlagg3Norr;
    private System.Windows.Forms.TextBox textBoxAvlagg2Norr;
    private System.Windows.Forms.TextBox textBoxAvlagg1Norr;
    private System.Windows.Forms.Label label27;
    private System.Windows.Forms.Label label28;
    private System.Windows.Forms.Label label29;
    private System.Windows.Forms.Label label30;
    private System.Windows.Forms.GroupBox groupBox9;
    private System.Windows.Forms.TextBox textBoxAvlagg6Ost;
    private System.Windows.Forms.TextBox textBoxAvlagg5Ost;
    private System.Windows.Forms.TextBox textBoxAvlagg4Ost;
    private System.Windows.Forms.GroupBox groupBox10;
    private System.Windows.Forms.TextBox textBoxAvlagg6Norr;
    private System.Windows.Forms.TextBox textBoxAvlagg5Norr;
    private System.Windows.Forms.TextBox textBoxAvlagg4Norr;
    private System.Windows.Forms.ComboBox comboBoxStickvägssystem;
    private System.Data.DataTable dataTableStickvagssystem;
    private System.Data.DataColumn dataColumnFilterStickvagssystemNamn;
    private System.Windows.Forms.Label label31;
    private System.Data.DataTable dataTableDatainsamlingsmetod;
    private System.Data.DataColumn dataColumnDataColumnFilterMarkberedningsmetodNamn;
    private System.Windows.Forms.ComboBox comboBoxDatainsamlingsmetod;
    private System.Windows.Forms.Label label32;
    public System.Data.DataSet dataSetFilter;
    private System.Windows.Forms.GroupBox groupBoxFiltreringsalternativ;
    private System.Windows.Forms.RadioButton radioButtonAllaRapportTyper;
    private System.Windows.Forms.RadioButton radioButtonValdRapportTyp;
    private System.Windows.Forms.ComboBox comboBoxInvTypId;
    private System.Windows.Forms.Label label33;
    private System.Data.DataTable dataTableInvTyp;
    private System.Data.DataColumn dataColumnFilterInvtypId;
    private System.Data.DataColumn dataColumnFilterInvtypNamn;
  }
}