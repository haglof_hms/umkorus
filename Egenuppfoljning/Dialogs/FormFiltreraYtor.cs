﻿#region

using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Egenuppfoljning.Interfaces;
using Egenuppfoljning.Properties;

#endregion

namespace Egenuppfoljning.Dialogs
{
    public partial class FormFiltreraYtor : Form
    {
        private List<KorusDataEventArgs.Yta> mYtor;

        public FormFiltreraYtor()
        {
            InitializeComponent();
            mYtor = new List<KorusDataEventArgs.Yta>();
            radioButtonValdRapportTyp.Text = DataHelper.GetRapportTyp(Settings.Default.ValdTypYtor);
            if (Text != null) Text += radioButtonValdRapportTyp.Text;
        }

        public override sealed string Text
        {
            get { return base.Text; }
            set { base.Text = value; }
        }

        private void LaddaFiltreringsAlternativ()
        {
            try
            {
                foreach (
                  var yta in
                    mYtor.Where(
                      yta =>
                      radioButtonAllaRapportTyper.Checked ||
                      (radioButtonValdRapportTyp.Checked && yta.rapport_typ == Settings.Default.ValdTypYtor)))
                {
                    if (!dataSetFilter.Tables["InvTyp"].Rows.Contains(yta.invtyp))
                    {
                        dataSetFilter.Tables["InvTyp"].Rows.Add(yta.invtyp, DataSetParser.getInvNamn(yta.invtyp));
                    }
                    if (!dataSetFilter.Tables["Region"].Rows.Contains(yta.regionid))
                    {
                        dataSetFilter.Tables["Region"].Rows.Add(yta.regionid, string.Empty);
                    }
                    if (!dataSetFilter.Tables["Distrikt"].Rows.Contains(yta.distriktid))
                    {
                        dataSetFilter.Tables["Distrikt"].Rows.Add(yta.regionid, yta.distriktid, string.Empty);
                    }
                    if (!dataSetFilter.Tables["TraktDel"].Rows.Contains(yta.standort))
                    {
                        dataSetFilter.Tables["TraktDel"].Rows.Add(yta.standort);
                    }
                    if (!dataSetFilter.Tables["Trakt"].Rows.Contains(yta.traktnr))
                    {
                        dataSetFilter.Tables["Trakt"].Rows.Add(yta.traktnr, string.Empty);
                    }
                    if (!dataSetFilter.Tables["Entreprenör"].Rows.Contains(yta.regionid + "-" + yta.entreprenor))
                    {
                        dataSetFilter.Tables["Entreprenör"].Rows.Add(yta.regionid + "-" + yta.entreprenor, string.Empty);
                    }
                    if (!dataSetFilter.Tables["RapportTyp"].Rows.Contains(yta.rapport_typ))
                    {
                        dataSetFilter.Tables["RapportTyp"].Rows.Add(yta.rapport_typ, DataHelper.GetRapportTyp(yta.rapport_typ));
                    }
                    if (!dataSetFilter.Tables["Artal"].Rows.Contains(yta.årtal))
                    {
                        dataSetFilter.Tables["Artal"].Rows.Add(yta.årtal);
                    }
                }
            }
            catch (Exception ex)
            {
#if !DEBUG
       MessageBox.Show("Ladda filtreringsalternativ misslyckades", "Fel vid filtreringsalternativ", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void SortTables()
        {
            dataSetFilter.Tables["InvTyp"].DefaultView.Sort = "Id";
            dataSetFilter.Tables["Region"].DefaultView.Sort = "Id";
            dataSetFilter.Tables["Distrikt"].DefaultView.Sort = "Id";
            dataSetFilter.Tables["TraktDel"].DefaultView.Sort = "Id";
            dataSetFilter.Tables["Trakt"].DefaultView.Sort = "Id";
            dataSetFilter.Tables["Entreprenör"].DefaultView.Sort = "Id";
            dataSetFilter.Tables["Artal"].DefaultView.Sort = "Id";
        }

        public void InitData(List<KorusDataEventArgs.Yta> aYtor)
        {
            try
            {
                mYtor = aYtor;
                InitDataSet();
                LaddaFiltreringsAlternativ();
                SortTables();
            }
            catch (Exception ex)
            {
#if !DEBUG
     MessageBox.Show(Resources.Misslyckades_lasa_data, Resources.Lasa_data_miss, MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void InitDataSet()
        {
            dataSetFilter.Clear();
            dataSetFilter.Tables["Region"].Rows.Add(0, string.Empty);
            dataSetFilter.Tables["InvTyp"].Rows.Add(Settings.Default.MinusEtt, string.Empty);
            dataSetFilter.Tables["Distrikt"].Rows.Add(0, 0, string.Empty);
            dataSetFilter.Tables["TraktDel"].Rows.Add(0);
            dataSetFilter.Tables["Trakt"].Rows.Add(0, string.Empty);
            dataSetFilter.Tables["Entreprenör"].Rows.Add(0, string.Empty);
            dataSetFilter.Tables["RapportTyp"].Rows.Add(0, string.Empty);
            dataSetFilter.Tables["Artal"].Rows.Add(0);
        }

        private void FormFiltreraYtor_Load(object sender, EventArgs e)
        {
            UppdateraGUI();
        }

        private void UppdateraGUI()
        {
            AktiveraKomponenterBeroendePåRapportTyp();
            if (Settings.Default.FiltreraYtaInvTypId == Settings.Default.MinusEtt)
            {
                comboBoxInvTypId.Text = string.Empty;
            }
            else
                comboBoxInvTypId.Text = DataSetParser.getInvNamn(Settings.Default.FiltreraYtaInvTypId);

            //comboBoxInvTypId.Text = Settings.Default.FiltreraYtaInvTypId.ToString(CultureInfo.InvariantCulture);
            comboBoxRegionId.Text = Settings.Default.FiltreraYtaRegionId.ToString(CultureInfo.InvariantCulture);
            comboBoxDistriktId.Text = Settings.Default.FiltreraYtaDistriktId.ToString(CultureInfo.InvariantCulture);
            comboBoxTraktNr.Text = Settings.Default.FiltreraYtaTraktNr.ToString(CultureInfo.InvariantCulture);
            comboBoxTraktDelId.Text = Settings.Default.FiltreraYtaStandort.ToString(CultureInfo.InvariantCulture);
            comboBoxEntreprenörId.Text = Settings.Default.FiltreraYtaEntreprenor.ToString(CultureInfo.InvariantCulture);
            comboBoxÅrtal.Text = Settings.Default.FiltreraYtaArtal.ToString(CultureInfo.InvariantCulture);

            textBoxYta.Text = Settings.Default.FiltreraYta.ToString(CultureInfo.InvariantCulture);
            textBoxOptimalt.Text = Settings.Default.FiltreraYtaOptimalt.ToString(CultureInfo.InvariantCulture);
            textBoxBra.Text = Settings.Default.FiltreraYtaBra.ToString(CultureInfo.InvariantCulture);
            textBoxVaravBättre.Text = Settings.Default.FiltreraYtaVaravBattre.ToString(CultureInfo.InvariantCulture);
            textBoxBlekjordsfläck.Text = Settings.Default.FiltreraYtaBlekjordsflack.ToString(CultureInfo.InvariantCulture);
            textBoxOptimaltBra.Text = Settings.Default.FiltreraYtaOptimaltBra.ToString(CultureInfo.InvariantCulture);
            textBoxÖvrigt.Text = Settings.Default.FiltreraYtaOvrigt.ToString(CultureInfo.InvariantCulture);
            textBoxTilltryckning.Text = Settings.Default.FiltreraYtaTilltryckning.ToString(CultureInfo.InvariantCulture);
            textBoxStickvbredd.Text = Settings.Default.FiltreraYtaStickvbredd.ToString(CultureInfo.InvariantCulture);
            textBoxTall.Text = Settings.Default.FiltreraYtaTall.ToString(CultureInfo.InvariantCulture);
            textBoxTallMedelhöjd.Text = Settings.Default.FiltreraYtaTallMedelhojd.ToString(CultureInfo.InvariantCulture);
            textBoxGran.Text = Settings.Default.FiltreraYtaGran.ToString(CultureInfo.InvariantCulture);
            textBoxGranMedelhöjd.Text = Settings.Default.FiltreraYtaGranMedelhojd.ToString(CultureInfo.InvariantCulture);
            textBoxLöv.Text = Settings.Default.FiltreraYtaLov.ToString(CultureInfo.InvariantCulture);
            textBoxLövMedelhöjd.Text = Settings.Default.FiltreraYtaLovMedelhojd.ToString(CultureInfo.InvariantCulture);
            textBoxHst.Text = Settings.Default.FiltreraYtaHst.ToString(CultureInfo.InvariantCulture);
            textBoxMHöjd.Text = Settings.Default.FiltreraYtaMHojd.ToString(CultureInfo.InvariantCulture);
            textBoxContorta.Text = Settings.Default.FiltreraYtaContorta.ToString(CultureInfo.InvariantCulture);
            textBoxContortaMedelhöjd.Text =
              Settings.Default.FiltreraYtaContortaMedelhojd.ToString(CultureInfo.InvariantCulture);
            textBoxSumma.Text = Settings.Default.FiltreraYtaSumma.ToString(CultureInfo.InvariantCulture);
            textBoxSkador.Text = Settings.Default.FiltreraYtaSkador.ToString(CultureInfo.InvariantCulture);
        }

        private void SetEnableKomponenter(bool aOptimalt, bool aBra, bool aVaravBättre, bool aBlekjordsfläck,
                                          bool aOptimaltBra,
                                          bool aÖvrigt, bool aTilltryckning, bool aStickvbredd,
                                          bool aTall, bool aTallMedelHöjd,
                                          bool aGran, bool aGranMedelhöjd, bool aLöv, bool aLövMedelHöjd, bool aContorta,
                                          bool aContortaMedelhöjd,
                                          bool aHst, bool aMHöjd, bool aSumma, bool aSkador)
        {
            textBoxOptimalt.Enabled = aOptimalt;
            textBoxBra.Enabled = aBra;
            textBoxVaravBättre.Enabled = aVaravBättre;
            textBoxBlekjordsfläck.Enabled = aBlekjordsfläck;
            textBoxOptimaltBra.Enabled = aOptimaltBra;
            textBoxÖvrigt.Enabled = aÖvrigt;
            textBoxTilltryckning.Enabled = aTilltryckning;
            textBoxStickvbredd.Enabled = aStickvbredd;
            textBoxTall.Enabled = aTall;
            textBoxTallMedelhöjd.Enabled = aTallMedelHöjd;
            textBoxGran.Enabled = aGran;
            textBoxGranMedelhöjd.Enabled = aGranMedelhöjd;
            textBoxLöv.Enabled = aLöv;
            textBoxLövMedelhöjd.Enabled = aLövMedelHöjd;
            textBoxHst.Enabled = aHst;
            textBoxMHöjd.Enabled = aMHöjd;
            textBoxContorta.Enabled = aContorta;
            textBoxContortaMedelhöjd.Enabled = aContortaMedelhöjd;
            textBoxSumma.Enabled = aSumma;
            textBoxSkador.Enabled = aSkador;
        }

        private void AktiveraKomponenterBeroendePåRapportTyp()
        {
            switch (Settings.Default.ValdTypYtor)
            {
                case (int)RapportTyp.Biobränsle:
                    SetEnableKomponenter(false, false, false, false, false, false, false, false, false, false, false, false,
                                         false, false, false, false, false, false, false, false);
                    break;
                case (int)RapportTyp.Gallring:
                    SetEnableKomponenter(false, false, false, false, false, false, false, true, true, false, true, false,
                                         true, false, true, false, false, false, true, true);
                    break;
                case (int)RapportTyp.Markberedning:
                    SetEnableKomponenter(true, true, false, true, true, false, false, false, false, false, false, false,
                                         false, false, false, false, false, false, false, false);
                    break;
                case (int)RapportTyp.Plantering:
                    SetEnableKomponenter(true, true, true, false, false, true, true, false, false, false, false, false, false,
                                         false, false, false, false, false, false, false);
                    break;
                case (int)RapportTyp.Röjning:
                    SetEnableKomponenter(false, false, false, false, false, false, false, false, true, true, true, true,
                                         true, true, true, true, true, true, false, false);
                    break;
                case (int)RapportTyp.Slutavverkning:
                    SetEnableKomponenter(false, false, false, false, false, false, false, false, false, false, false, false,
                                         false, false, false, false, false, false, false, false);
                    break;
                default:
                    SetEnableKomponenter(true, true, true, true, true, true, true, true, true, true, true, true, true, true,
                                         true, true, true, true, true, true);
                    break;
            }
        }

        private void buttonUppdateraTabell_Click(object sender, EventArgs e)
        {
            int invtypid;
            if (comboBoxInvTypId.SelectedIndex < 0)
            {
                Settings.Default.FiltreraYtaInvTypId = -1;
                Settings.Default.FiltreraYtaInvTypNamn = string.Empty;
            }
            else
            {
                invtypid = (Int32)comboBoxInvTypId.SelectedValue;
                Settings.Default.FiltreraYtaInvTypId = invtypid;
                Settings.Default.FiltreraYtaInvTypNamn = comboBoxInvTypId.Text;
            }
            //Settings.Default.FiltreraYtaInvTypId = DataSetParser.GetObjIntValue(comboBoxInvTypId.Text, true);
            Settings.Default.FiltreraYtaRegionId = DataSetParser.GetObjIntValue(comboBoxRegionId.Text, true);
            Settings.Default.FiltreraYtaDistriktId = DataSetParser.GetObjIntValue(comboBoxDistriktId.Text, true);
            Settings.Default.FiltreraYtaTraktNr = DataSetParser.GetObjIntValue(comboBoxTraktNr.Text, true);
            Settings.Default.FiltreraYtaStandort = DataSetParser.GetObjIntValue(comboBoxTraktDelId.Text, true);
            Settings.Default.FiltreraYtaEntreprenor = DataSetParser.GetObjIntValue(comboBoxEntreprenörId.Text, true);
            Settings.Default.FiltreraYtaArtal = DataSetParser.GetObjIntValue(comboBoxÅrtal.Text, true);

            Settings.Default.FiltreraYta = DataSetParser.GetObjIntValue(textBoxYta.Text, true);
            Settings.Default.FiltreraYtaOptimalt = DataSetParser.GetObjIntValue(textBoxOptimalt.Text, true);
            Settings.Default.FiltreraYtaBra = DataSetParser.GetObjIntValue(textBoxBra.Text, true);
            Settings.Default.FiltreraYtaVaravBattre = DataSetParser.GetObjIntValue(textBoxVaravBättre.Text, true);
            Settings.Default.FiltreraYtaBlekjordsflack = DataSetParser.GetObjIntValue(textBoxBlekjordsfläck.Text, true);
            Settings.Default.FiltreraYtaOptimaltBra = DataSetParser.GetObjIntValue(textBoxOptimaltBra.Text, true);
            Settings.Default.FiltreraYtaOvrigt = DataSetParser.GetObjIntValue(textBoxÖvrigt.Text, true);
            Settings.Default.FiltreraYtaTilltryckning = DataSetParser.GetObjIntValue(textBoxTilltryckning.Text, true);
            Settings.Default.FiltreraYtaStickvbredd = DataSetParser.GetObjDoubleValue(textBoxStickvbredd.Text, true);

            Settings.Default.FiltreraYtaTall = DataSetParser.GetObjIntValue(textBoxTall.Text, true);
            Settings.Default.FiltreraYtaTallMedelhojd = DataSetParser.GetObjDoubleValue(textBoxTallMedelhöjd.Text, true);
            Settings.Default.FiltreraYtaGran = DataSetParser.GetObjIntValue(textBoxGran.Text, true);
            Settings.Default.FiltreraYtaGranMedelhojd = DataSetParser.GetObjDoubleValue(textBoxGranMedelhöjd.Text, true);
            Settings.Default.FiltreraYtaLov = DataSetParser.GetObjIntValue(textBoxLöv.Text, true);
            Settings.Default.FiltreraYtaLovMedelhojd = DataSetParser.GetObjDoubleValue(textBoxLövMedelhöjd.Text, true);
            Settings.Default.FiltreraYtaHst = DataSetParser.GetObjIntValue(textBoxHst.Text, true);
            Settings.Default.FiltreraYtaMHojd = DataSetParser.GetObjDoubleValue(textBoxMHöjd.Text, true);
            Settings.Default.FiltreraYtaContorta = DataSetParser.GetObjIntValue(textBoxContorta.Text, true);
            Settings.Default.FiltreraYtaContortaMedelhojd = DataSetParser.GetObjDoubleValue(textBoxContortaMedelhöjd.Text,
                                                                                            true);
            Settings.Default.FiltreraYtaSumma = DataSetParser.GetObjIntValue(textBoxSumma.Text, true);
            Settings.Default.FiltreraYtaSkador = DataSetParser.GetObjIntValue(textBoxSkador.Text, true);

            DialogResult = DialogResult.OK;
            Settings.Default.Save();

            Close();
        }

        private void buttonTömAllaFilter_Click(object sender, EventArgs e)
        {
            comboBoxInvTypId.Text = string.Empty;
            comboBoxRegionId.Text = Resources.Noll;
            comboBoxDistriktId.Text = Resources.Noll;
            comboBoxTraktNr.Text = Resources.Noll;
            comboBoxTraktDelId.Text = Resources.Noll;
            comboBoxEntreprenörId.Text = Resources.Noll;
            comboBoxÅrtal.Text = Resources.Noll;

            textBoxYta.Text = Resources.Noll;
            textBoxOptimalt.Text = Resources.Noll;
            textBoxBra.Text = Resources.Noll;
            textBoxVaravBättre.Text = Resources.Noll;
            textBoxBlekjordsfläck.Text = Resources.Noll;
            textBoxOptimaltBra.Text = Resources.Noll;
            textBoxÖvrigt.Text = Resources.Noll;
            textBoxTilltryckning.Text = Resources.Noll;
            textBoxStickvbredd.Text = Resources.Noll;
            textBoxTall.Text = Resources.Noll;
            textBoxTallMedelhöjd.Text = Resources.Noll;
            textBoxGran.Text = Resources.Noll;
            textBoxGranMedelhöjd.Text = Resources.Noll;
            textBoxLöv.Text = Resources.Noll;
            textBoxLövMedelhöjd.Text = Resources.Noll;
            textBoxHst.Text = Resources.Noll;
            textBoxMHöjd.Text = Resources.Noll;
            textBoxContorta.Text = Resources.Noll;
            textBoxContortaMedelhöjd.Text = Resources.Noll;
            textBoxSumma.Text = Resources.Noll;
            textBoxSkador.Text = Resources.Noll;
        }

        private void buttonAvbryt_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void radioButtonValdRapportTyp_CheckedChanged(object sender, EventArgs e)
        {
            InitDataSet();
            LaddaFiltreringsAlternativ();
            SortTables();
            UppdateraGUI();
        }

        private void radioButtonAllaRapportTyper_CheckedChanged(object sender, EventArgs e)
        {
            InitDataSet();
            LaddaFiltreringsAlternativ();
            SortTables();
            UppdateraGUI();
        }

        private void UpdateDistriktList(ref object sender)
        {
            var comboBox = sender as ComboBox;
            if (comboBox == null) return;
            comboBoxDistriktId.Items.Clear();
            if (comboBox.SelectedIndex < 0) return;
            var activeRegionId = dataSetFilter.Tables["Region"].Rows[comboBox.SelectedIndex]["Id"].ToString();
            foreach (
              var row in
                dataSetFilter.Tables["Distrikt"].Rows.Cast<DataRow>().Where(
                  row => row["RegionId"].ToString().Equals(activeRegionId)))
            {
                comboBoxDistriktId.Items.Add(row["Id"].ToString());
            }
        }

        private void comboBoxRegionId_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateDistriktList(ref sender);
        }
    }
}