﻿#region

using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Egenuppfoljning.Interfaces;
using Egenuppfoljning.Properties;

#endregion

namespace Egenuppfoljning.Dialogs
{
    public partial class FormFiltreraRapporter : Form
    {
        private List<KorusDataEventArgs.Data> mRapporter;

        public FormFiltreraRapporter()
        {
            InitializeComponent();
            mRapporter = new List<KorusDataEventArgs.Data>();
            radioButtonValdRapportTyp.Text = DataHelper.GetRapportTyp(Settings.Default.ValdTypRapport);
            if (Text != null) Text += radioButtonValdRapportTyp.Text;
        }

        public override sealed string Text
        {
            get { return base.Text; }
            set { base.Text = value; }
        }

        public void InitData(List<KorusDataEventArgs.Data> aRapporter)
        {
            try
            {
                mRapporter = aRapporter;
                InitDataSet();
                LaddaFiltreringsAlternativ();
                SortTables();
            }
            catch (Exception ex)
            {
#if !DEBUG
     MessageBox.Show("Misslyckades med att läsa in data", "Läsa data misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void LaddaFiltreringsAlternativ()
        {
            try
            {
                if (!dataSetFilter.Tables["InvTyp"].Rows.Contains(Settings.Default.MinusEtt))
                {
                    dataSetFilter.Tables["InvTyp"].Rows.Add(Settings.Default.MinusEtt, DataSetParser.getInvNamn(Settings.Default.MinusEtt));
                }
                foreach (
                  var rapport in
                    mRapporter.Where(
                      rapport =>
                      radioButtonAllaRapportTyper.Checked ||
                      (radioButtonValdRapportTyp.Checked && rapport.rapport_typ == Settings.Default.ValdTypRapport)))
                {
                    if (!dataSetFilter.Tables["InvTyp"].Rows.Contains(rapport.invtyp))
                    {
                        dataSetFilter.Tables["InvTyp"].Rows.Add(rapport.invtyp, DataSetParser.getInvNamn(rapport.invtyp));
                    }

                    if (!dataSetFilter.Tables["Region"].Rows.Contains(rapport.regionid))
                    {
                        dataSetFilter.Tables["Region"].Rows.Add(rapport.regionid, rapport.regionnamn);
                    }
                    if (!dataSetFilter.Tables["Distrikt"].Rows.Contains(rapport.distriktid))
                    {
                        dataSetFilter.Tables["Distrikt"].Rows.Add(rapport.regionid, rapport.distriktid, rapport.distriktnamn);
                    }
                    if (!dataSetFilter.Tables["TraktDel"].Rows.Contains(rapport.standort))
                    {
                        dataSetFilter.Tables["TraktDel"].Rows.Add(rapport.standort);
                    }
                    if (!dataSetFilter.Tables["Markberedningsmetod"].Rows.Contains(rapport.markberedningsmetod))
                    {
                        dataSetFilter.Tables["Markberedningsmetod"].Rows.Add(rapport.markberedningsmetod);
                    }
                    if (!dataSetFilter.Tables["Datainsamlingsmetod"].Rows.Contains(rapport.datainsamlingsmetod))
                    {
                        dataSetFilter.Tables["Datainsamlingsmetod"].Rows.Add(rapport.datainsamlingsmetod);
                    }
                    if (!dataSetFilter.Tables["Ursprung"].Rows.Contains(rapport.ursprung))
                    {
                        dataSetFilter.Tables["Ursprung"].Rows.Add(rapport.ursprung);
                    }
                    if (!dataSetFilter.Tables["Trakt"].Rows.Contains(rapport.traktnr))
                    {
                        dataSetFilter.Tables["Trakt"].Rows.Add(rapport.traktnr, rapport.traktnamn);
                    }
                    if (!dataSetFilter.Tables["Entreprenör"].Rows.Contains(rapport.regionid + "-" + rapport.entreprenor))
                    {
                        dataSetFilter.Tables["Entreprenör"].Rows.Add(rapport.regionid + "-" + rapport.entreprenor,
                                                                     rapport.entreprenornamn);
                    }
                    if (!dataSetFilter.Tables["RapportTyp"].Rows.Contains(rapport.rapport_typ))
                    {
                        dataSetFilter.Tables["RapportTyp"].Rows.Add(rapport.rapport_typ,
                                                                    DataHelper.GetRapportTyp(rapport.rapport_typ));
                    }
                    if (!dataSetFilter.Tables["Artal"].Rows.Contains(rapport.årtal))
                    {
                        dataSetFilter.Tables["Artal"].Rows.Add(rapport.årtal);
                    }
                    if (!dataSetFilter.Tables["Stickvagssystem"].Rows.Contains(rapport.stickvagssystem))
                    {
                        dataSetFilter.Tables["Stickvagssystem"].Rows.Add(rapport.stickvagssystem);
                    }
                }
            }
            catch (Exception ex)
            {
#if !DEBUG
       MessageBox.Show("Ladda filtreringsalternativ misslyckades", "Fel vid filtreringsalternativ", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }


        /*public string getInvNamn(int nIndex)
        {
            if (nIndex == 0)
                return "Egen";
            else
                if (nIndex == 1)
                    return "Distrikt";
                else
                    return "";
        }*/
        private void SortTables()
        {
            dataSetFilter.Tables["InvTyp"].DefaultView.Sort = "Id";
            dataSetFilter.Tables["Region"].DefaultView.Sort = "Id";
            dataSetFilter.Tables["Distrikt"].DefaultView.Sort = "Id";
            dataSetFilter.Tables["TraktDel"].DefaultView.Sort = "Id";
            dataSetFilter.Tables["Trakt"].DefaultView.Sort = "Id";
            dataSetFilter.Tables["Entreprenör"].DefaultView.Sort = "Id";
            dataSetFilter.Tables["Artal"].DefaultView.Sort = "Id";
        }

        private void InitDataSet()
        {
            dataSetFilter.Clear();
            dataSetFilter.Tables["InvTyp"].Rows.Add(Settings.Default.MinusEtt, string.Empty);
            dataSetFilter.Tables["Region"].Rows.Add(0, string.Empty);
            dataSetFilter.Tables["Distrikt"].Rows.Add(0, 0, string.Empty);
            dataSetFilter.Tables["TraktDel"].Rows.Add(0);
            dataSetFilter.Tables["Markberedningsmetod"].Rows.Add(string.Empty);
            dataSetFilter.Tables["Datainsamlingsmetod"].Rows.Add(string.Empty);
            dataSetFilter.Tables["Ursprung"].Rows.Add(string.Empty);
            dataSetFilter.Tables["Trakt"].Rows.Add(0, string.Empty);
            dataSetFilter.Tables["Entreprenör"].Rows.Add(0, string.Empty);
            dataSetFilter.Tables["RapportTyp"].Rows.Add(0, string.Empty);
            dataSetFilter.Tables["Artal"].Rows.Add(0);
            dataSetFilter.Tables["Stickvagssystem"].Rows.Add(string.Empty);
        }

        private void FormFiltreraRapporter_Load(object sender, EventArgs e)
        {
            UppdateraGUI();
        }

        private void UppdateraGUI()
        {
            AktiveraKomponenterBeroendePåRapportTyp();
            if (Settings.Default.FiltreraRapportInvTypId == Settings.Default.MinusEtt)
            {
                comboBoxInvTypId.Text = string.Empty;
            }
            else
                comboBoxInvTypId.Text = DataSetParser.getInvNamn(Settings.Default.FiltreraRapportInvTypId);
            comboBoxRegionId.Text = Settings.Default.FiltreraRapportRegionId.ToString(CultureInfo.InvariantCulture);
            comboBoxRegionNamn.Text = Settings.Default.FiltreraRapportRegionNamn;
            comboBoxDistriktId.Text = Settings.Default.FiltreraRapportDistriktId.ToString(CultureInfo.InvariantCulture);
            comboBoxDistriktNamn.Text = Settings.Default.FiltreraRapportDistriktNamn;
            comboBoxTraktNr.Text = Settings.Default.FiltreraRapportTraktNr.ToString(CultureInfo.InvariantCulture);
            comboBoxTraktNamn.Text = Settings.Default.FiltreraRapportTraktNamn;
            comboBoxEntreprenörId.Text = Settings.Default.FiltreraRapportEntreprenor.ToString(CultureInfo.InvariantCulture);
            comboBoxEntreprenörNamn.Text = Settings.Default.FiltreraRapportEntreprenorNamn;
            comboBoxTraktDelId.Text = Settings.Default.FiltreraRapportStandort.ToString(CultureInfo.InvariantCulture);
            comboBoxUrsprung.Text = Settings.Default.FiltreraRapportUrsprung;
            comboBoxÅrtal.Text = Settings.Default.FiltreraRapportArtal.ToString(CultureInfo.InvariantCulture);
            comboBoxMarkberedning.Text = Settings.Default.FiltreraRapportMarkberedningsmetod;

            textBoxAreal.Text = Settings.Default.FiltreraRapportAreal.ToString(CultureInfo.InvariantCulture);
            textBoxMålgrundyta.Text = Settings.Default.FiltreraRapportMalgrundyta.ToString(CultureInfo.InvariantCulture);
            textBoxGISSträcka.Text = Settings.Default.FiltreraRapportGISStracka.ToString(CultureInfo.InvariantCulture);
            textBoxGISAreal.Text = Settings.Default.FiltreraRapportGISAreal.ToString(CultureInfo.InvariantCulture);
            textBoxProvytestorlek.Text = Settings.Default.FiltreraRapportProvytestorlek.ToString(CultureInfo.InvariantCulture);
            comboBoxStickvägssystem.Text = Settings.Default.FiltreraRapportStickvagssystem;
            textBoxStickvägsavstånd.Text =
              Settings.Default.FiltreraRapportStickvagsavstand.ToString(CultureInfo.InvariantCulture);

            textBoxAntalVältor.Text = Settings.Default.FiltreraRapportAntalValtor.ToString(CultureInfo.InvariantCulture);
            textBoxAvstånd.Text = Settings.Default.FiltreraRapportAvstand.ToString(CultureInfo.InvariantCulture);
            textBoxBedömdVolym.Text = Settings.Default.FiltreraRapportBedomdVolym.ToString(CultureInfo.InvariantCulture);
            textBoxHygge.Text = Settings.Default.FiltreraRapportHygge.ToString(CultureInfo.InvariantCulture);
            textBoxVägkant.Text = Settings.Default.FiltreraRapportVagkant.ToString(CultureInfo.InvariantCulture);
            textBoxÅker.Text = Settings.Default.FiltreraRapportAker.ToString(CultureInfo.InvariantCulture);

            checkBoxLatbilshugg.Checked = Settings.Default.FiltreraRapportLatbilshugg;
            checkBoxGrotbil.Checked = Settings.Default.FiltreraRapportGrotbil;
            checkBoxTraktordragenHugg.Checked = Settings.Default.FiltreraRapportTraktordragenhugg;
            checkBoxSkotarburenHugg.Checked = Settings.Default.FiltreraRapportSkotarburenhugg;

            textBoxAvlagg1Norr.Text = Settings.Default.FiltreraRapportAvlagg1norr.ToString(CultureInfo.InvariantCulture);
            textBoxAvlagg1Ost.Text = Settings.Default.FiltreraRapportAvlagg1ost.ToString(CultureInfo.InvariantCulture);
            textBoxAvlagg2Norr.Text = Settings.Default.FiltreraRapportAvlagg2norr.ToString(CultureInfo.InvariantCulture);
            textBoxAvlagg2Ost.Text = Settings.Default.FiltreraRapportAvlagg2ost.ToString(CultureInfo.InvariantCulture);
            textBoxAvlagg3Norr.Text = Settings.Default.FiltreraRapportAvlagg3norr.ToString(CultureInfo.InvariantCulture);
            textBoxAvlagg3Ost.Text = Settings.Default.FiltreraRapportAvlagg3ost.ToString(CultureInfo.InvariantCulture);
            textBoxAvlagg4Norr.Text = Settings.Default.FiltreraRapportAvlagg4norr.ToString(CultureInfo.InvariantCulture);
            textBoxAvlagg4Ost.Text = Settings.Default.FiltreraRapportAvlagg4ost.ToString(CultureInfo.InvariantCulture);
            textBoxAvlagg5Norr.Text = Settings.Default.FiltreraRapportAvlagg5norr.ToString(CultureInfo.InvariantCulture);
            textBoxAvlagg5Ost.Text = Settings.Default.FiltreraRapportAvlagg5ost.ToString(CultureInfo.InvariantCulture);
            textBoxAvlagg6Norr.Text = Settings.Default.FiltreraRapportAvlagg6norr.ToString(CultureInfo.InvariantCulture);
            textBoxAvlagg6Ost.Text = Settings.Default.FiltreraRapportAvlagg6ost.ToString(CultureInfo.InvariantCulture);
        }

        private void SetEnableKomponenter(bool aMålgrundyta, bool aMarkberedningsmetod, bool aAreal, bool aStickVägsavstånd,
                                          bool aGISSträcka,
                                          bool aGISAreal, bool aProvytestorlek, bool aBiobränsle)
        {
            textBoxMålgrundyta.Enabled = aMålgrundyta;
            comboBoxMarkberedning.Enabled = aMarkberedningsmetod;
            comboBoxDatainsamlingsmetod.Enabled = aMarkberedningsmetod;
            comboBoxStickvägssystem.Enabled = aMarkberedningsmetod;
            textBoxAreal.Enabled = aAreal;
            comboBoxStickvägssystem.Enabled = aStickVägsavstånd;
            textBoxStickvägsavstånd.Enabled = aStickVägsavstånd;
            textBoxGISSträcka.Enabled = aGISSträcka;
            textBoxGISAreal.Enabled = aGISAreal;
            textBoxProvytestorlek.Enabled = aProvytestorlek;

            textBoxAntalVältor.Enabled = aBiobränsle;
            textBoxAvstånd.Enabled = aBiobränsle;
            textBoxBedömdVolym.Enabled = aBiobränsle;
            textBoxHygge.Enabled = aBiobränsle;
            textBoxVägkant.Enabled = aBiobränsle;
            textBoxÅker.Enabled = aBiobränsle;

            checkBoxLatbilshugg.Enabled = aBiobränsle;
            checkBoxGrotbil.Enabled = aBiobränsle;
            checkBoxTraktordragenHugg.Enabled = aBiobränsle;
            checkBoxSkotarburenHugg.Enabled = aBiobränsle;

            textBoxAvlagg1Norr.Enabled = aBiobränsle;
            textBoxAvlagg1Ost.Enabled = aBiobränsle;
            textBoxAvlagg2Norr.Enabled = aBiobränsle;
            textBoxAvlagg2Ost.Enabled = aBiobränsle;
            textBoxAvlagg3Norr.Enabled = aBiobränsle;
            textBoxAvlagg3Ost.Enabled = aBiobränsle;
            textBoxAvlagg4Norr.Enabled = aBiobränsle;
            textBoxAvlagg4Ost.Enabled = aBiobränsle;
            textBoxAvlagg5Norr.Enabled = aBiobränsle;
            textBoxAvlagg5Ost.Enabled = aBiobränsle;
            textBoxAvlagg6Norr.Enabled = aBiobränsle;
            textBoxAvlagg6Ost.Enabled = aBiobränsle;
        }

        private void AktiveraKomponenterBeroendePåRapportTyp()
        {
            switch (Settings.Default.ValdTypRapport)
            {
                case (int)RapportTyp.Biobränsle:
                    SetEnableKomponenter(false, false, false, false, false, false, false, true);
                    break;
                case (int)RapportTyp.Gallring:
                    SetEnableKomponenter(true, false, true, true, false, false, false, false);
                    break;
                case (int)RapportTyp.Markberedning:
                    SetEnableKomponenter(true, true, false, false, true, true, false, false);
                    break;
                case (int)RapportTyp.Plantering:
                    SetEnableKomponenter(true, false, false, false, false, false, true, false);
                    break;
                case (int)RapportTyp.Röjning:
                    SetEnableKomponenter(true, false, false, false, false, false, true, false);
                    break;
                case (int)RapportTyp.Slutavverkning:
                    SetEnableKomponenter(false, false, false, false, false, false, false, false);
                    break;
                default:
                    SetEnableKomponenter(true, true, true, true, true, true, true, true);
                    break;
            }
        }

        private void buttonUppdatera_Click(object sender, EventArgs e)
        {
            int invtypid;
            if ((int)comboBoxInvTypId.SelectedValue < 0)
            {
                Settings.Default.FiltreraRapportInvTypId = Settings.Default.MinusEtt;
                Settings.Default.FiltreraRapportInvTypNamn = string.Empty;
            }
            else
            {
                invtypid = (Int32)comboBoxInvTypId.SelectedValue;
                Settings.Default.FiltreraRapportInvTypId = invtypid;
                Settings.Default.FiltreraRapportInvTypNamn = comboBoxInvTypId.Text;
                //Settings.Default.FiltreraRapportInvTypId = DataSetParser.GetObjIntValue(comboBoxInvTypId.Text, true);
            }
            //Settings.Default.FiltreraRapportInvTypId = DataSetParser.GetObjIntValue(comboBoxInvTypId.Text, true);
            Settings.Default.FiltreraRapportRegionId = DataSetParser.GetObjIntValue(comboBoxRegionId.Text, true);
            Settings.Default.FiltreraRapportDistriktId = DataSetParser.GetObjIntValue(comboBoxDistriktId.Text, true);
            Settings.Default.FiltreraRapportTraktNr = DataSetParser.GetObjIntValue(comboBoxTraktNr.Text, true);
            Settings.Default.FiltreraRapportStandort = DataSetParser.GetObjIntValue(comboBoxTraktDelId.Text, true);
            Settings.Default.FiltreraRapportEntreprenor = DataSetParser.GetObjIntValue(comboBoxEntreprenörId.Text, true);
            Settings.Default.FiltreraRapportArtal = DataSetParser.GetObjIntValue(comboBoxÅrtal.Text, true);

            Settings.Default.FiltreraRapportRegionNamn = comboBoxRegionNamn.Text;
            Settings.Default.FiltreraRapportTraktNamn = comboBoxTraktNamn.Text;
            Settings.Default.FiltreraRapportDistriktNamn = comboBoxDistriktNamn.Text;
            Settings.Default.FiltreraRapportEntreprenorNamn = comboBoxEntreprenörNamn.Text;
            Settings.Default.FiltreraRapportUrsprung = comboBoxUrsprung.Text;
            Settings.Default.FiltreraRapportMarkberedningsmetod = comboBoxMarkberedning.Text;
            Settings.Default.FiltreraRapportDatainsamlingsmetod = comboBoxDatainsamlingsmetod.Text;

            Settings.Default.FiltreraRapportAreal = DataSetParser.GetObjDoubleValue(textBoxAreal.Text, true);
            Settings.Default.FiltreraRapportMalgrundyta = DataSetParser.GetObjIntValue(textBoxMålgrundyta.Text, true);
            Settings.Default.FiltreraRapportGISStracka = DataSetParser.GetObjIntValue(textBoxGISSträcka.Text, true);
            Settings.Default.FiltreraRapportGISAreal = DataSetParser.GetObjDoubleValue(textBoxGISAreal.Text, true);

            Settings.Default.FiltreraRapportProvytestorlek = DataSetParser.GetObjIntValue(textBoxProvytestorlek.Text, true);
            Settings.Default.FiltreraRapportStickvagssystem = comboBoxStickvägssystem.Text;
            Settings.Default.FiltreraRapportStickvagsavstand = DataSetParser.GetObjIntValue(textBoxStickvägsavstånd.Text, true);

            Settings.Default.FiltreraRapportAntalValtor = DataSetParser.GetObjIntValue(textBoxAntalVältor.Text, true);
            Settings.Default.FiltreraRapportAvstand = DataSetParser.GetObjIntValue(textBoxAvstånd.Text, true);
            Settings.Default.FiltreraRapportBedomdVolym = DataSetParser.GetObjIntValue(textBoxBedömdVolym.Text, true);
            Settings.Default.FiltreraRapportHygge = DataSetParser.GetObjIntValue(textBoxHygge.Text, true);
            Settings.Default.FiltreraRapportVagkant = DataSetParser.GetObjIntValue(textBoxVägkant.Text, true);
            Settings.Default.FiltreraRapportAker = DataSetParser.GetObjIntValue(textBoxÅker.Text, true);

            Settings.Default.FiltreraRapportLatbilshugg = checkBoxLatbilshugg.Checked;
            Settings.Default.FiltreraRapportGrotbil = checkBoxGrotbil.Checked;
            Settings.Default.FiltreraRapportTraktordragenhugg = checkBoxTraktordragenHugg.Checked;
            Settings.Default.FiltreraRapportSkotarburenhugg = checkBoxSkotarburenHugg.Checked;

            Settings.Default.FiltreraRapportAvlagg1norr = DataSetParser.GetObjIntValue(textBoxAvlagg1Norr.Text, true);
            Settings.Default.FiltreraRapportAvlagg1ost = DataSetParser.GetObjIntValue(textBoxAvlagg1Ost.Text, true);
            Settings.Default.FiltreraRapportAvlagg2norr = DataSetParser.GetObjIntValue(textBoxAvlagg2Norr.Text, true);
            Settings.Default.FiltreraRapportAvlagg2ost = DataSetParser.GetObjIntValue(textBoxAvlagg2Ost.Text, true);
            Settings.Default.FiltreraRapportAvlagg3norr = DataSetParser.GetObjIntValue(textBoxAvlagg3Norr.Text, true);
            Settings.Default.FiltreraRapportAvlagg3ost = DataSetParser.GetObjIntValue(textBoxAvlagg3Ost.Text, true);
            Settings.Default.FiltreraRapportAvlagg4norr = DataSetParser.GetObjIntValue(textBoxAvlagg4Norr.Text, true);
            Settings.Default.FiltreraRapportAvlagg4ost = DataSetParser.GetObjIntValue(textBoxAvlagg4Ost.Text, true);
            Settings.Default.FiltreraRapportAvlagg5norr = DataSetParser.GetObjIntValue(textBoxAvlagg5Norr.Text, true);
            Settings.Default.FiltreraRapportAvlagg5ost = DataSetParser.GetObjIntValue(textBoxAvlagg5Ost.Text, true);
            Settings.Default.FiltreraRapportAvlagg6norr = DataSetParser.GetObjIntValue(textBoxAvlagg6Norr.Text, true);
            Settings.Default.FiltreraRapportAvlagg6ost = DataSetParser.GetObjIntValue(textBoxAvlagg6Ost.Text, true);

            DialogResult = DialogResult.OK;
            Settings.Default.Save();

            Close();
        }

        private void buttonAvbryt_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void buttonTömAllaFilter_Click(object sender, EventArgs e)
        {
            comboBoxInvTypId.Text = string.Empty;
            comboBoxRegionId.Text = Resources.Noll;
            comboBoxRegionNamn.Text = string.Empty;
            comboBoxDistriktId.Text = Resources.Noll;
            comboBoxDistriktNamn.Text = string.Empty;
            comboBoxTraktNr.Text = Resources.Noll;
            comboBoxTraktNamn.Text = string.Empty;
            comboBoxEntreprenörId.Text = Resources.Noll;
            comboBoxEntreprenörNamn.Text = string.Empty;
            comboBoxTraktDelId.Text = Resources.Noll;
            comboBoxUrsprung.Text = string.Empty;
            comboBoxÅrtal.Text = Resources.Noll;
            comboBoxMarkberedning.Text = string.Empty;

            textBoxAreal.Text = Resources.Noll;
            textBoxMålgrundyta.Text = Resources.Noll;
            textBoxGISSträcka.Text = Resources.Noll;
            textBoxGISAreal.Text = Resources.Noll;
            textBoxProvytestorlek.Text = Resources.Noll;
            comboBoxStickvägssystem.Text = string.Empty;
            textBoxStickvägsavstånd.Text = Resources.Noll;

            textBoxAntalVältor.Text = Resources.Noll;
            textBoxAvstånd.Text = Resources.Noll;
            textBoxBedömdVolym.Text = Resources.Noll;
            textBoxHygge.Text = Resources.Noll;
            textBoxVägkant.Text = Resources.Noll;
            textBoxÅker.Text = Resources.Noll;

            checkBoxLatbilshugg.Checked = false;
            checkBoxGrotbil.Checked = false;
            checkBoxTraktordragenHugg.Checked = false;
            checkBoxSkotarburenHugg.Checked = false;

            textBoxAvlagg1Norr.Text = Resources.Noll;
            textBoxAvlagg1Ost.Text = Resources.Noll;
            textBoxAvlagg2Norr.Text = Resources.Noll;
            textBoxAvlagg2Ost.Text = Resources.Noll;
            textBoxAvlagg3Norr.Text = Resources.Noll;
            textBoxAvlagg3Ost.Text = Resources.Noll;
            textBoxAvlagg4Norr.Text = Resources.Noll;
            textBoxAvlagg4Ost.Text = Resources.Noll;
            textBoxAvlagg5Norr.Text = Resources.Noll;
            textBoxAvlagg5Ost.Text = Resources.Noll;
            textBoxAvlagg6Norr.Text = Resources.Noll;
            textBoxAvlagg6Ost.Text = Resources.Noll;
        }

        private void radioButtonValdRapportTyp_CheckedChanged(object sender, EventArgs e)
        {
            InitDataSet();
            LaddaFiltreringsAlternativ();
            SortTables();
            UppdateraGUI();
        }

        private void radioButtonAllaRapportTyper_CheckedChanged(object sender, EventArgs e)
        {
            InitDataSet();
            LaddaFiltreringsAlternativ();
            SortTables();
            UppdateraGUI();
        }

        private void UpdateDistriktList(ref object sender)
        {
            var comboBox = sender as ComboBox;
            if (comboBox == null) return;
            comboBoxDistriktId.Items.Clear();
            comboBoxDistriktNamn.Items.Clear();
            if (comboBox.SelectedIndex < 0) return;
            var activeRegionId = dataSetFilter.Tables["Region"].Rows[comboBox.SelectedIndex]["Id"].ToString();
            foreach (
              var row in
                dataSetFilter.Tables["Distrikt"].Rows.Cast<DataRow>().Where(
                  row => row["RegionId"].ToString().Equals(activeRegionId)))
            {
                comboBoxDistriktId.Items.Add(row["Id"].ToString());
                comboBoxDistriktNamn.Items.Add(row["Namn"].ToString());
            }
        }

        private void comboBoxRegionId_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateDistriktList(ref sender);
        }

        private void comboBoxDistriktId_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxDistriktNamn.SelectedIndex = comboBoxDistriktId.SelectedIndex;
        }

        private void comboBoxDistriktNamn_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxDistriktId.SelectedIndex = comboBoxDistriktNamn.SelectedIndex;
        }
    }
}