﻿#region

using System;
using System.Windows.Forms;
using Egenuppfoljning.Interfaces;

#endregion

namespace Egenuppfoljning.Dialogs
{
  public partial class FormProgress : Form
  {
    public FormProgress()
    {
      InitializeComponent();
    }

    public void Börja(int aCount, int aMax)
    {
      Invoke(new BörjaInoker(StartaBörja), new object[] {aCount, aMax});
    }

    public void SättVärde(int värde)
    {
      Invoke(new SättVärdeInvoker(StartaSättVärde), new object[] {värde});
    }

    public void CheckStatus(object aCallback)
    {
      if (progressBarVänta.Value < progressBarVänta.Maximum)
      {
        Öka();
      }
    }

    /// <summary>
    ///   Används för att öka progress värdet.
    /// </summary>
    private void Öka()
    {
      Invoke(new MethodInvoker(StartaÖka));
    }

    /// <summary>
    ///   Uppdaterar titel texten för fönstret
    /// </summary>
    private void UppdateraStatusText()
    {
      Text = String.Format("Laddar.. {0}% klar",
                           (progressBarVänta.Value*100)/(progressBarVänta.Maximum - progressBarVänta.Minimum));
    }

    private void FormProgress_Load(object sender, EventArgs e)
    {
      ControlBox = false;
    }

    private void buttonAvbryt_Click(object sender, EventArgs e)
    {
      var callBack = sender as IProgressCallback;
      if (callBack != null)
      {
        callBack.StängProgressDialog();
      }
    }

    #region Implementation av metoder som anropas av ägar tråden.

    private void StartaBörja(int aCount, int aMax)
    {
      progressBarVänta.Value = aCount;
      progressBarVänta.Maximum = aMax;
      progressBarVänta.Minimum = 0;
    }

    private void StartaÖka()
    {
      progressBarVänta.Increment(1);
      UppdateraStatusText();
    }

    private void StartaSättVärde(int värde)
    {
      progressBarVänta.Value = värde;
    }

    #endregion

    #region Nested type: BörjaInoker

    private delegate void BörjaInoker(int aCount, int aMax);

    #endregion

    #region Nested type: SättVärdeInvoker

    private delegate void SättVärdeInvoker(int värde);

    #endregion
  }
}