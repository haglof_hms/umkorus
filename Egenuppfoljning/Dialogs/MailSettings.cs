﻿#region

using System;
using System.Globalization;
using System.Windows.Forms;

#endregion

namespace Egenuppfoljning.Dialogs
{
  public partial class MailSettings : Form
  {
    /// <summary>
    ///   Dialog for mail settings.
    /// </summary>
    public MailSettings()
    {
      InitializeComponent();
    }

    private void MailSettings_Load(object sender, EventArgs e)
    {
      textBoxNamn.Text = Settings.Default.MailFullName;
      textBoxDinEpost.Text = Settings.Default.MailSendFrom;

      if (!Settings.Default.HamtaEpostFranRapport)
      {
        textBoxMottagarensEpost.Text = Settings.Default.MailSendToManual;
      }
      else
      {
        textBoxMottagarensEpost.Text = Settings.Default.MailSendToManual = string.Empty;
      }

      textBoxSMTP.Text = Settings.Default.MailSMTP;
      textBoxSMTPPort.Text = Settings.Default.MailSMTPPort.ToString(CultureInfo.InvariantCulture);
      textBoxAnvandare.Text = Settings.Default.MailUsername;
      textBoxLosenord.Text = Settings.Default.MailPassword;
      checkBoxHämtaEpostAdresssFrånRapport.Checked = Settings.Default.HamtaEpostFranRapport;
      textBoxTitelGodkänd.Text = Settings.Default.MailSubjectGodkand;
      richTextBoxMeddelandeGodkänd.Text = Settings.Default.MailBodyGodkand;
      checkBoxVisaDialogSändGodkänd.Checked = Settings.Default.VisaDialogSandGodkand;
      textBoxTitelNekad.Text = Settings.Default.MailSubjectNekad;
      richTextBoxMeddelandeNekad.Text = Settings.Default.MailBodyNekad;
      checkBoxVisaDialogSändNekad.Checked = Settings.Default.VisaDialogSandNekad;
      checkBoxAnvändInternEpost.Checked = Settings.Default.AnvandInternEpost;
      groupBoxInternEpost.Enabled = Settings.Default.AnvandInternEpost;
      groupBoxGodkändRapport.Enabled = Settings.Default.SandEpostGodkand;
      groupBoxNekadRapport.Enabled = Settings.Default.SandEpostNekad;
      checkBoxSändGodkändKvittens.Checked = Settings.Default.SandEpostGodkand;
      checkBoxSändNekadRespons.Checked = Settings.Default.SandEpostNekad;
      checkBoxBifogaRapportGodkänd.Checked = Settings.Default.BifogaRapportGodkand;
      checkBoxBifogaRapportNekad.Checked = Settings.Default.BifogaRapportNekad;
    }

    private void buttonAvbryt_Click(object sender, EventArgs e)
    {
      DialogResult = DialogResult.Cancel;
      Close();
    }

    private void buttonOk_Click(object sender, EventArgs e)
    {
      Settings.Default.MailFullName = textBoxNamn.Text;
      Settings.Default.MailSendFrom = textBoxDinEpost.Text;
      Settings.Default.MailSendToManual = textBoxMottagarensEpost.Text;
      Settings.Default.MailSMTP = textBoxSMTP.Text;
      Settings.Default.MailUsername = textBoxAnvandare.Text;
      Settings.Default.MailPassword = textBoxLosenord.Text;
      Settings.Default.HamtaEpostFranRapport = checkBoxHämtaEpostAdresssFrånRapport.Checked;
      Settings.Default.MailSubjectGodkand = textBoxTitelGodkänd.Text;
      Settings.Default.MailBodyGodkand = richTextBoxMeddelandeGodkänd.Text;
      Settings.Default.VisaDialogSandGodkand = checkBoxVisaDialogSändGodkänd.Checked;
      Settings.Default.MailSubjectNekad = textBoxTitelNekad.Text;
      Settings.Default.MailBodyNekad = richTextBoxMeddelandeNekad.Text;
      Settings.Default.VisaDialogSandNekad = checkBoxVisaDialogSändNekad.Checked;
      Settings.Default.AnvandInternEpost = checkBoxAnvändInternEpost.Checked;
      Settings.Default.SandEpostGodkand = checkBoxSändGodkändKvittens.Checked;
      Settings.Default.SandEpostNekad = checkBoxSändNekadRespons.Checked;
      Settings.Default.BifogaRapportGodkand = checkBoxBifogaRapportGodkänd.Checked;
      Settings.Default.BifogaRapportNekad = checkBoxBifogaRapportNekad.Checked;

      int smtpPort;
      int.TryParse(textBoxSMTPPort.Text, out smtpPort);
      Settings.Default.MailSMTPPort = smtpPort;

      DialogResult = DialogResult.OK;
      Settings.Default.Save();

      Close();
    }

    private void checkBoxHämtaEpostAdresssFrånRapport_CheckedChanged(object sender, EventArgs e)
    {
      var hämtaEpost = sender as CheckBox;
      if (hämtaEpost != null)
      {
        textBoxMottagarensEpost.Enabled = !hämtaEpost.Checked;
      }
    }

    private void checkBoxAnvändInternEpost_CheckedChanged(object sender, EventArgs e)
    {
      var användInternEpost = sender as CheckBox;
      if (användInternEpost != null)
      {
        groupBoxInternEpost.Enabled = användInternEpost.Checked;
      }
    }

    private void checkBoxSändGodkändKvittens_CheckedChanged(object sender, EventArgs e)
    {
      var sändGodkändKvittens = sender as CheckBox;
      if (sändGodkändKvittens != null)
      {
        groupBoxGodkändRapport.Enabled = sändGodkändKvittens.Checked;
      }
    }

    private void checkBoxSändNekadRespons_CheckedChanged(object sender, EventArgs e)
    {
      var sändNekadKvittens = sender as CheckBox;
      if (sändNekadKvittens != null)
      {
        groupBoxNekadRapport.Enabled = sändNekadKvittens.Checked;
      }
    }
  }
}