﻿#region

using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Egenuppfoljning.Interfaces;
using Egenuppfoljning.Properties;

#endregion

namespace Egenuppfoljning.Dialogs
{
    public partial class FormFiltreraFrågor : Form
    {
        private List<KorusDataEventArgs.Frågor> mFrågor;

        public FormFiltreraFrågor()
        {
            InitializeComponent();
            mFrågor = new List<KorusDataEventArgs.Frågor>();
            radioButtonValdRapportTyp.Text = DataHelper.GetRapportTyp(Settings.Default.ValdTypFragor);
            if (Text != null) Text += radioButtonValdRapportTyp.Text;
        }

        public override sealed string Text
        {
            get { return base.Text; }
            set { base.Text = value; }
        }


        private void SortTables()
        {
            dataSetFilter.Tables["InvTyp"].DefaultView.Sort = "Id";
            dataSetFilter.Tables["Region"].DefaultView.Sort = "Id";
            dataSetFilter.Tables["Distrikt"].DefaultView.Sort = "Id";
            dataSetFilter.Tables["Standort"].DefaultView.Sort = "Id";
            dataSetFilter.Tables["Trakt"].DefaultView.Sort = "Id";
            dataSetFilter.Tables["Entreprenör"].DefaultView.Sort = "Id";
            dataSetFilter.Tables["Artal"].DefaultView.Sort = "Id";
        }

        private void InitDataSet()
        {

            dataSetFilter.Clear();
            dataSetFilter.Tables["InvTyp"].Rows.Add(-1, string.Empty);
            dataSetFilter.Tables["Region"].Rows.Add(0, string.Empty);
            dataSetFilter.Tables["Distrikt"].Rows.Add(0, 0, string.Empty);
            dataSetFilter.Tables["Standort"].Rows.Add(0);
            dataSetFilter.Tables["Trakt"].Rows.Add(0, string.Empty);
            dataSetFilter.Tables["Entreprenör"].Rows.Add(0, string.Empty);
            dataSetFilter.Tables["RapportTyp"].Rows.Add(0, string.Empty);
            dataSetFilter.Tables["Artal"].Rows.Add(0);
            dataSetFilter.Tables["Fråga1"].Rows.Add(string.Empty);
            dataSetFilter.Tables["Fråga2"].Rows.Add(string.Empty);
            dataSetFilter.Tables["Fråga3"].Rows.Add(string.Empty);
            dataSetFilter.Tables["Fråga4"].Rows.Add(string.Empty);
            dataSetFilter.Tables["Fråga5"].Rows.Add(string.Empty);
            dataSetFilter.Tables["Fråga6"].Rows.Add(string.Empty);
            dataSetFilter.Tables["Fråga7"].Rows.Add(string.Empty);
            dataSetFilter.Tables["Fråga8"].Rows.Add(string.Empty);
            dataSetFilter.Tables["Fråga9"].Rows.Add(string.Empty);
            dataSetFilter.Tables["Fråga10"].Rows.Add(string.Empty);
            dataSetFilter.Tables["Fråga11"].Rows.Add(string.Empty);
            dataSetFilter.Tables["Fråga12"].Rows.Add(string.Empty);
            dataSetFilter.Tables["Fråga13"].Rows.Add(string.Empty);
            dataSetFilter.Tables["Fråga14"].Rows.Add(string.Empty);
            dataSetFilter.Tables["Fråga15"].Rows.Add(string.Empty);
            dataSetFilter.Tables["Fråga16"].Rows.Add(string.Empty);
            dataSetFilter.Tables["Fråga17"].Rows.Add(string.Empty);
            dataSetFilter.Tables["Fråga18"].Rows.Add(string.Empty);
            dataSetFilter.Tables["Fråga19"].Rows.Add(string.Empty);
            dataSetFilter.Tables["Fråga20"].Rows.Add(string.Empty);
            dataSetFilter.Tables["Fråga21"].Rows.Add(string.Empty);
        }

        private void LaddaFiltreringsAlternativ()
        {
            try
            {
                foreach (
                  var fråga in
                    mFrågor.Where(
                      fråga =>
                      radioButtonAllaRapportTyper.Checked ||
                      (radioButtonValdRapportTyp.Checked && fråga.rapport_typ == Settings.Default.ValdTypFragor)))
                {
                    if (!dataSetFilter.Tables["InvTyp"].Rows.Contains(fråga.invtyp))
                    {
                        dataSetFilter.Tables["InvTyp"].Rows.Add(fråga.invtyp, DataSetParser.getInvNamn(fråga.invtyp));
                    }
                    if (!dataSetFilter.Tables["Region"].Rows.Contains(fråga.regionid))
                    {
                        dataSetFilter.Tables["Region"].Rows.Add(fråga.regionid, string.Empty);
                    }
                    if (!dataSetFilter.Tables["Distrikt"].Rows.Contains(fråga.distriktid))
                    {
                        dataSetFilter.Tables["Distrikt"].Rows.Add(fråga.regionid, fråga.distriktid, string.Empty);
                    }
                    if (!dataSetFilter.Tables["Standort"].Rows.Contains(fråga.standort))
                    {
                        dataSetFilter.Tables["Standort"].Rows.Add(fråga.standort);
                    }
                    if (!dataSetFilter.Tables["Trakt"].Rows.Contains(fråga.traktnr))
                    {
                        dataSetFilter.Tables["Trakt"].Rows.Add(fråga.traktnr, string.Empty);
                    }
                    if (!dataSetFilter.Tables["Entreprenör"].Rows.Contains(fråga.regionid + "-" + fråga.entreprenor))
                    {
                        dataSetFilter.Tables["Entreprenör"].Rows.Add(fråga.regionid + "-" + fråga.entreprenor, string.Empty);
                    }
                    if (!dataSetFilter.Tables["RapportTyp"].Rows.Contains(fråga.rapport_typ))
                    {
                        dataSetFilter.Tables["RapportTyp"].Rows.Add(fråga.rapport_typ, DataHelper.GetRapportTyp(fråga.rapport_typ));
                    }
                    if (!dataSetFilter.Tables["Artal"].Rows.Contains(fråga.årtal))
                    {
                        dataSetFilter.Tables["Artal"].Rows.Add(fråga.årtal);
                    }
                    if (!dataSetFilter.Tables["Fråga1"].Rows.Contains(fråga.fraga1))
                    {
                        dataSetFilter.Tables["Fråga1"].Rows.Add(fråga.fraga1);
                    }
                    if (!dataSetFilter.Tables["Fråga2"].Rows.Contains(fråga.fraga2))
                    {
                        dataSetFilter.Tables["Fråga2"].Rows.Add(fråga.fraga2);
                    }
                    if (!dataSetFilter.Tables["Fråga3"].Rows.Contains(fråga.fraga3))
                    {
                        dataSetFilter.Tables["Fråga3"].Rows.Add(fråga.fraga3);
                    }
                    if (!dataSetFilter.Tables["Fråga4"].Rows.Contains(fråga.fraga4))
                    {
                        dataSetFilter.Tables["Fråga4"].Rows.Add(fråga.fraga4);
                    }
                    if (!dataSetFilter.Tables["Fråga5"].Rows.Contains(fråga.fraga5))
                    {
                        dataSetFilter.Tables["Fråga5"].Rows.Add(fråga.fraga5);
                    }
                    if (!dataSetFilter.Tables["Fråga6"].Rows.Contains(fråga.fraga6))
                    {
                        dataSetFilter.Tables["Fråga6"].Rows.Add(fråga.fraga6);
                    }
                    if (!dataSetFilter.Tables["Fråga7"].Rows.Contains(fråga.fraga7))
                    {
                        dataSetFilter.Tables["Fråga7"].Rows.Add(fråga.fraga7);
                    }
                    if (!dataSetFilter.Tables["Fråga8"].Rows.Contains(fråga.fraga8))
                    {
                        dataSetFilter.Tables["Fråga8"].Rows.Add(fråga.fraga8);
                    }
                    if (!dataSetFilter.Tables["Fråga9"].Rows.Contains(fråga.fraga9))
                    {
                        dataSetFilter.Tables["Fråga9"].Rows.Add(fråga.fraga9);
                    }
                    if (!dataSetFilter.Tables["Fråga10"].Rows.Contains(fråga.fraga10))
                    {
                        dataSetFilter.Tables["Fråga10"].Rows.Add(fråga.fraga10);
                    }
                    if (!dataSetFilter.Tables["Fråga11"].Rows.Contains(fråga.fraga11))
                    {
                        dataSetFilter.Tables["Fråga11"].Rows.Add(fråga.fraga11);
                    }
                    if (!dataSetFilter.Tables["Fråga12"].Rows.Contains(fråga.fraga12))
                    {
                        dataSetFilter.Tables["Fråga12"].Rows.Add(fråga.fraga12);
                    }
                    if (!dataSetFilter.Tables["Fråga13"].Rows.Contains(fråga.fraga13))
                    {
                        dataSetFilter.Tables["Fråga13"].Rows.Add(fråga.fraga13);
                    }
                    if (!dataSetFilter.Tables["Fråga14"].Rows.Contains(fråga.fraga14))
                    {
                        dataSetFilter.Tables["Fråga14"].Rows.Add(fråga.fraga14);
                    }
                    if (!dataSetFilter.Tables["Fråga15"].Rows.Contains(fråga.fraga15))
                    {
                        dataSetFilter.Tables["Fråga15"].Rows.Add(fråga.fraga15);
                    }
                    if (!dataSetFilter.Tables["Fråga16"].Rows.Contains(fråga.fraga16))
                    {
                        dataSetFilter.Tables["Fråga16"].Rows.Add(fråga.fraga16);
                    }
                    if (!dataSetFilter.Tables["Fråga17"].Rows.Contains(fråga.fraga17))
                    {
                        dataSetFilter.Tables["Fråga17"].Rows.Add(fråga.fraga17);
                    }
                    if (!dataSetFilter.Tables["Fråga18"].Rows.Contains(fråga.fraga18))
                    {
                        dataSetFilter.Tables["Fråga18"].Rows.Add(fråga.fraga18);
                    }
                    if (!dataSetFilter.Tables["Fråga19"].Rows.Contains(fråga.fraga19))
                    {
                        dataSetFilter.Tables["Fråga19"].Rows.Add(fråga.fraga19);
                    }
                    if (!dataSetFilter.Tables["Fråga20"].Rows.Contains(fråga.fraga20))
                    {
                        dataSetFilter.Tables["Fråga20"].Rows.Add(fråga.fraga20);
                    }
                    if (!dataSetFilter.Tables["Fråga21"].Rows.Contains(fråga.fraga21))
                    {
                        dataSetFilter.Tables["Fråga21"].Rows.Add(fråga.fraga21);
                    }
                }
            }
            catch (Exception ex)
            {
#if !DEBUG
       MessageBox.Show("Ladda filtreringsalternativ misslyckades", "Fel vid filtreringsalternativ", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        public void InitData(List<KorusDataEventArgs.Frågor> aFrågor)
        {
            try
            {
                mFrågor = aFrågor;
                InitDataSet();
                LaddaFiltreringsAlternativ();
                SortTables();
            }
            catch (Exception ex)
            {
#if !DEBUG
     MessageBox.Show(Resources.Misslyckades_lasa_data, Resources.Lasa_data_miss, MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void UppdateraGUI()
        {
            AktiveraKomponenterBeroendePåRapportTyp();
            if (Settings.Default.FiltreraFragaInvTypId == Settings.Default.MinusEtt)
            {
                comboBoxInvTypId.Text = string.Empty;
            }
            else
                comboBoxInvTypId.Text = DataSetParser.getInvNamn(Settings.Default.FiltreraFragaInvTypId);
            comboBoxRegionId.Text = Settings.Default.FiltreraFragaRegionId.ToString(CultureInfo.InvariantCulture);
            comboBoxDistriktId.Text = Settings.Default.FiltreraFragaDistriktId.ToString(CultureInfo.InvariantCulture);
            comboBoxTraktNr.Text = Settings.Default.FiltreraFragaTraktNr.ToString(CultureInfo.InvariantCulture);
            comboBoxStåndortId.Text = Settings.Default.FiltreraFragaStandort.ToString(CultureInfo.InvariantCulture);
            comboBoxEntreprenörId.Text = Settings.Default.FiltreraFragaEntreprenor.ToString(CultureInfo.InvariantCulture);
            comboBoxÅrtal.Text = Settings.Default.FiltreraFragaArtal.ToString(CultureInfo.InvariantCulture);

            comboBoxFråga1.Text = Settings.Default.FiltreraFraga1;
            comboBoxFråga2.Text = Settings.Default.FiltreraFraga2;
            comboBoxFråga3.Text = Settings.Default.FiltreraFraga3;
            comboBoxFråga4.Text = Settings.Default.FiltreraFraga4;
            comboBoxFråga5.Text = Settings.Default.FiltreraFraga5;
            comboBoxFråga6.Text = Settings.Default.FiltreraFraga6;
            comboBoxFråga7.Text = Settings.Default.FiltreraFraga7;
            comboBoxFråga8.Text = Settings.Default.FiltreraFraga8;
            comboBoxFråga9.Text = Settings.Default.FiltreraFraga9;
            comboBoxFråga10.Text = Settings.Default.FiltreraFraga10;
            comboBoxFråga11.Text = Settings.Default.FiltreraFraga11;
            comboBoxFråga12.Text = Settings.Default.FiltreraFraga12;
            comboBoxFråga13.Text = Settings.Default.FiltreraFraga13;
            comboBoxFråga14.Text = Settings.Default.FiltreraFraga14;
            comboBoxFråga15.Text = Settings.Default.FiltreraFraga15;
            comboBoxFråga16.Text = Settings.Default.FiltreraFraga16;
            comboBoxFråga17.Text = Settings.Default.FiltreraFraga17;
            comboBoxFråga18.Text = Settings.Default.FiltreraFraga18;
            comboBoxFråga19.Text = Settings.Default.FiltreraFraga19;
            comboBoxFråga20.Text = Settings.Default.FiltreraFraga20;
            comboBoxFråga21.Text = Settings.Default.FiltreraFraga21;
        }

        private void SetEnableKomponenter(bool aFråga1, bool aFråga2, bool aFråga3, bool aFråga4, bool aFråga5, bool aFråga6,
                                          bool aFråga7, bool aFråga8, bool aFråga9, bool aBiobränsle, bool aSlutavverkning)
        {
            comboBoxFråga1.Enabled = aFråga1;
            comboBoxFråga2.Enabled = aFråga2;
            comboBoxFråga3.Enabled = aFråga3;
            comboBoxFråga4.Enabled = aFråga4;
            comboBoxFråga5.Enabled = aFråga5;
            comboBoxFråga6.Enabled = aFråga6;
            comboBoxFråga7.Enabled = aFråga7;
            comboBoxFråga8.Enabled = aFråga8;
            comboBoxFråga9.Enabled = aFråga9;

            comboBoxFråga10.Enabled = aBiobränsle;
            comboBoxFråga11.Enabled = aBiobränsle;
            comboBoxFråga12.Enabled = aBiobränsle;
            comboBoxFråga13.Enabled = aBiobränsle;
            comboBoxFråga14.Enabled = aBiobränsle;
            comboBoxFråga15.Enabled = aBiobränsle;
            comboBoxFråga16.Enabled = aBiobränsle;
            comboBoxFråga17.Enabled = aBiobränsle;
            comboBoxFråga18.Enabled = aBiobränsle;
            comboBoxFråga19.Enabled = aBiobränsle;
            comboBoxFråga20.Enabled = aSlutavverkning;
            comboBoxFråga21.Enabled = aSlutavverkning;
        }

        private void AktiveraKomponenterBeroendePåRapportTyp()
        {
            switch (Settings.Default.ValdTypFragor)
            {
                case (int)RapportTyp.Biobränsle:
                    SetEnableKomponenter(true, true, true, true, true, true, true, true, true, true, false);
                    break;
                case (int)RapportTyp.Gallring:
                    SetEnableKomponenter(true, true, true, true, true, true, true, true, true, false, false);
                    break;
                case (int)RapportTyp.Markberedning:
                    SetEnableKomponenter(true, true, true, true, true, true, true, false, false, false, false);
                    break;
                case (int)RapportTyp.Plantering:
                    SetEnableKomponenter(true, true, false, false, false, false, false, false, false, false, false);
                    break;
                case (int)RapportTyp.Röjning:
                    SetEnableKomponenter(true, true, true, true, true, true, false, false, false, false, false);
                    break;
                case (int)RapportTyp.Slutavverkning:
                    SetEnableKomponenter(true, true, true, true, true, true, true, true, true, true, true);
                    break;
                default:
                    SetEnableKomponenter(true, true, true, true, true, true, true, true, true, true, true);
                    break;
            }
        }


        private void FormFiltreraFrågor_Load(object sender, EventArgs e)
        {
            UppdateraGUI();
        }

        private void buttonAvbryt_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void buttonUppdatera_Click(object sender, EventArgs e)
        {
            int invtypid;
            if (comboBoxInvTypId.SelectedIndex < 0)
            {
                Settings.Default.FiltreraFragaInvTypId = -1;
                Settings.Default.FiltreraFragaInvTypNamn = string.Empty;
            }
            else
            {
                invtypid = (Int32)comboBoxInvTypId.SelectedValue;
                Settings.Default.FiltreraFragaInvTypId = invtypid;
                Settings.Default.FiltreraFragaInvTypNamn = comboBoxInvTypId.Text;
            }
            Settings.Default.FiltreraFragaRegionId = DataSetParser.GetObjIntValue(comboBoxRegionId.Text, true);
            Settings.Default.FiltreraFragaDistriktId = DataSetParser.GetObjIntValue(comboBoxDistriktId.Text, true);
            Settings.Default.FiltreraFragaTraktNr = DataSetParser.GetObjIntValue(comboBoxTraktNr.Text, true);
            Settings.Default.FiltreraFragaStandort = DataSetParser.GetObjIntValue(comboBoxStåndortId.Text, true);
            Settings.Default.FiltreraFragaEntreprenor = DataSetParser.GetObjIntValue(comboBoxEntreprenörId.Text, true);
            Settings.Default.FiltreraFragaArtal = DataSetParser.GetObjIntValue(comboBoxÅrtal.Text, true);

            Settings.Default.FiltreraFraga1 = comboBoxFråga1.Text;
            Settings.Default.FiltreraFraga2 = comboBoxFråga2.Text;
            Settings.Default.FiltreraFraga3 = comboBoxFråga3.Text;
            Settings.Default.FiltreraFraga4 = comboBoxFråga4.Text;
            Settings.Default.FiltreraFraga5 = comboBoxFråga5.Text;
            Settings.Default.FiltreraFraga6 = comboBoxFråga6.Text;
            Settings.Default.FiltreraFraga7 = comboBoxFråga7.Text;
            Settings.Default.FiltreraFraga8 = comboBoxFråga8.Text;
            Settings.Default.FiltreraFraga9 = comboBoxFråga9.Text;
            Settings.Default.FiltreraFraga10 = comboBoxFråga10.Text;
            Settings.Default.FiltreraFraga11 = comboBoxFråga11.Text;
            Settings.Default.FiltreraFraga12 = comboBoxFråga12.Text;
            Settings.Default.FiltreraFraga13 = comboBoxFråga13.Text;
            Settings.Default.FiltreraFraga14 = comboBoxFråga14.Text;
            Settings.Default.FiltreraFraga15 = comboBoxFråga15.Text;
            Settings.Default.FiltreraFraga16 = comboBoxFråga16.Text;
            Settings.Default.FiltreraFraga17 = comboBoxFråga17.Text;
            Settings.Default.FiltreraFraga18 = comboBoxFråga18.Text;
            Settings.Default.FiltreraFraga19 = comboBoxFråga19.Text;
            Settings.Default.FiltreraFraga20 = comboBoxFråga20.Text;
            Settings.Default.FiltreraFraga21 = comboBoxFråga21.Text;

            DialogResult = DialogResult.OK;
            Settings.Default.Save();

            Close();
        }

        private void buttonTömFilter_Click(object sender, EventArgs e)
        {
            comboBoxInvTypId.Text = string.Empty;
            comboBoxRegionId.Text = Resources.Noll;
            comboBoxDistriktId.Text = Resources.Noll;
            comboBoxTraktNr.Text = Resources.Noll;
            comboBoxStåndortId.Text = Resources.Noll;
            comboBoxEntreprenörId.Text = Resources.Noll;
            comboBoxÅrtal.Text = Resources.Noll;

            comboBoxFråga1.Text = string.Empty;
            comboBoxFråga2.Text = string.Empty;
            comboBoxFråga3.Text = string.Empty;
            comboBoxFråga4.Text = string.Empty;
            comboBoxFråga5.Text = string.Empty;
            comboBoxFråga6.Text = string.Empty;
            comboBoxFråga7.Text = string.Empty;
            comboBoxFråga8.Text = string.Empty;
            comboBoxFråga9.Text = string.Empty;
            comboBoxFråga10.Text = string.Empty;
            comboBoxFråga11.Text = string.Empty;
            comboBoxFråga12.Text = string.Empty;
            comboBoxFråga13.Text = string.Empty;
            comboBoxFråga14.Text = string.Empty;
            comboBoxFråga15.Text = string.Empty;
            comboBoxFråga16.Text = string.Empty;
            comboBoxFråga17.Text = string.Empty;
            comboBoxFråga18.Text = string.Empty;
            comboBoxFråga19.Text = string.Empty;
            comboBoxFråga20.Text = string.Empty;
            comboBoxFråga21.Text = string.Empty;
        }

        private void radioButtonValdRapportTyp_CheckedChanged(object sender, EventArgs e)
        {
            InitDataSet();
            LaddaFiltreringsAlternativ();
            SortTables();
            UppdateraGUI();
        }

        private void radioButtonAllaRapportTyper_CheckedChanged(object sender, EventArgs e)
        {
            InitDataSet();
            LaddaFiltreringsAlternativ();
            SortTables();
            UppdateraGUI();
        }

        private void UpdateDistriktList(ref object sender)
        {
            var comboBox = sender as ComboBox;
            if (comboBox == null) return;
            comboBoxDistriktId.Items.Clear();
            if (comboBox.SelectedIndex < 0) return;
            var activeRegionId = dataSetFilter.Tables["Region"].Rows[comboBox.SelectedIndex]["Id"].ToString();
            foreach (
              var row in
                dataSetFilter.Tables["Distrikt"].Rows.Cast<DataRow>().Where(
                  row => row["RegionId"].ToString().Equals(activeRegionId)))
            {
                comboBoxDistriktId.Items.Add(row["Id"].ToString());
            }
        }

        private void comboBoxRegionId_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateDistriktList(ref sender);
        }
    }
}