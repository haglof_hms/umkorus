﻿namespace Egenuppfoljning.Dialogs
{
  partial class MailSettings
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MailSettings));
      this.groupBoxEpost = new System.Windows.Forms.GroupBox();
      this.checkBoxSändNekadRespons = new System.Windows.Forms.CheckBox();
      this.checkBoxSändGodkändKvittens = new System.Windows.Forms.CheckBox();
      this.checkBoxAnvändInternEpost = new System.Windows.Forms.CheckBox();
      this.checkBoxHämtaEpostAdresssFrånRapport = new System.Windows.Forms.CheckBox();
      this.textBoxDinEpost = new System.Windows.Forms.TextBox();
      this.textBoxNamn = new System.Windows.Forms.TextBox();
      this.textBoxMottagarensEpost = new System.Windows.Forms.TextBox();
      this.label6 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.richTextBoxMeddelandeGodkänd = new System.Windows.Forms.RichTextBox();
      this.label11 = new System.Windows.Forms.Label();
      this.textBoxTitelGodkänd = new System.Windows.Forms.TextBox();
      this.label10 = new System.Windows.Forms.Label();
      this.groupBoxInternEpost = new System.Windows.Forms.GroupBox();
      this.textBoxSMTPPort = new System.Windows.Forms.TextBox();
      this.label12 = new System.Windows.Forms.Label();
      this.textBoxLosenord = new System.Windows.Forms.TextBox();
      this.textBoxAnvandare = new System.Windows.Forms.TextBox();
      this.textBoxSMTP = new System.Windows.Forms.TextBox();
      this.label9 = new System.Windows.Forms.Label();
      this.label8 = new System.Windows.Forms.Label();
      this.label7 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.buttonAvbryt = new System.Windows.Forms.Button();
      this.buttonOk = new System.Windows.Forms.Button();
      this.richTextBoxMeddelandeNekad = new System.Windows.Forms.RichTextBox();
      this.textBoxTitelNekad = new System.Windows.Forms.TextBox();
      this.label14 = new System.Windows.Forms.Label();
      this.label15 = new System.Windows.Forms.Label();
      this.checkBoxVisaDialogSändGodkänd = new System.Windows.Forms.CheckBox();
      this.checkBoxVisaDialogSändNekad = new System.Windows.Forms.CheckBox();
      this.groupBoxNekadRapport = new System.Windows.Forms.GroupBox();
      this.checkBoxBifogaRapportNekad = new System.Windows.Forms.CheckBox();
      this.label16 = new System.Windows.Forms.Label();
      this.groupBoxGodkändRapport = new System.Windows.Forms.GroupBox();
      this.checkBoxBifogaRapportGodkänd = new System.Windows.Forms.CheckBox();
      this.label13 = new System.Windows.Forms.Label();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.groupBoxEpost.SuspendLayout();
      this.groupBoxInternEpost.SuspendLayout();
      this.groupBoxNekadRapport.SuspendLayout();
      this.groupBoxGodkändRapport.SuspendLayout();
      this.groupBox1.SuspendLayout();
      this.SuspendLayout();
      // 
      // groupBoxEpost
      // 
      this.groupBoxEpost.Controls.Add(this.checkBoxSändNekadRespons);
      this.groupBoxEpost.Controls.Add(this.checkBoxSändGodkändKvittens);
      this.groupBoxEpost.Controls.Add(this.checkBoxAnvändInternEpost);
      this.groupBoxEpost.Controls.Add(this.checkBoxHämtaEpostAdresssFrånRapport);
      this.groupBoxEpost.Controls.Add(this.textBoxDinEpost);
      this.groupBoxEpost.Controls.Add(this.textBoxNamn);
      this.groupBoxEpost.Controls.Add(this.textBoxMottagarensEpost);
      this.groupBoxEpost.Controls.Add(this.label6);
      this.groupBoxEpost.Controls.Add(this.label5);
      this.groupBoxEpost.Controls.Add(this.label4);
      this.groupBoxEpost.Controls.Add(this.label1);
      this.groupBoxEpost.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.groupBoxEpost.Location = new System.Drawing.Point(8, 3);
      this.groupBoxEpost.Name = "groupBoxEpost";
      this.groupBoxEpost.Size = new System.Drawing.Size(502, 193);
      this.groupBoxEpost.TabIndex = 0;
      this.groupBoxEpost.TabStop = false;
      this.groupBoxEpost.Text = "E-POST";
      // 
      // checkBoxSändNekadRespons
      // 
      this.checkBoxSändNekadRespons.AutoSize = true;
      this.checkBoxSändNekadRespons.Checked = true;
      this.checkBoxSändNekadRespons.CheckState = System.Windows.Forms.CheckState.Checked;
      this.checkBoxSändNekadRespons.Location = new System.Drawing.Point(42, 159);
      this.checkBoxSändNekadRespons.Name = "checkBoxSändNekadRespons";
      this.checkBoxSändNekadRespons.Size = new System.Drawing.Size(143, 17);
      this.checkBoxSändNekadRespons.TabIndex = 9;
      this.checkBoxSändNekadRespons.Text = "Sänd Nekad Respons";
      this.checkBoxSändNekadRespons.UseVisualStyleBackColor = true;
      this.checkBoxSändNekadRespons.CheckedChanged += new System.EventHandler(this.checkBoxSändNekadRespons_CheckedChanged);
      // 
      // checkBoxSändGodkändKvittens
      // 
      this.checkBoxSändGodkändKvittens.AutoSize = true;
      this.checkBoxSändGodkändKvittens.Checked = true;
      this.checkBoxSändGodkändKvittens.CheckState = System.Windows.Forms.CheckState.Checked;
      this.checkBoxSändGodkändKvittens.Location = new System.Drawing.Point(314, 159);
      this.checkBoxSändGodkändKvittens.Name = "checkBoxSändGodkändKvittens";
      this.checkBoxSändGodkändKvittens.Size = new System.Drawing.Size(157, 17);
      this.checkBoxSändGodkändKvittens.TabIndex = 10;
      this.checkBoxSändGodkändKvittens.Text = "Sänd Godkänd Kvittens";
      this.checkBoxSändGodkändKvittens.UseVisualStyleBackColor = true;
      this.checkBoxSändGodkändKvittens.CheckedChanged += new System.EventHandler(this.checkBoxSändGodkändKvittens_CheckedChanged);
      // 
      // checkBoxAnvändInternEpost
      // 
      this.checkBoxAnvändInternEpost.AutoSize = true;
      this.checkBoxAnvändInternEpost.Checked = true;
      this.checkBoxAnvändInternEpost.CheckState = System.Windows.Forms.CheckState.Checked;
      this.checkBoxAnvändInternEpost.Location = new System.Drawing.Point(314, 133);
      this.checkBoxAnvändInternEpost.Name = "checkBoxAnvändInternEpost";
      this.checkBoxAnvändInternEpost.Size = new System.Drawing.Size(146, 17);
      this.checkBoxAnvändInternEpost.TabIndex = 8;
      this.checkBoxAnvändInternEpost.Text = "Använd intern e-post";
      this.checkBoxAnvändInternEpost.UseVisualStyleBackColor = true;
      this.checkBoxAnvändInternEpost.CheckedChanged += new System.EventHandler(this.checkBoxAnvändInternEpost_CheckedChanged);
      // 
      // checkBoxHämtaEpostAdresssFrånRapport
      // 
      this.checkBoxHämtaEpostAdresssFrånRapport.AutoSize = true;
      this.checkBoxHämtaEpostAdresssFrånRapport.Checked = true;
      this.checkBoxHämtaEpostAdresssFrånRapport.CheckState = System.Windows.Forms.CheckState.Checked;
      this.checkBoxHämtaEpostAdresssFrånRapport.Location = new System.Drawing.Point(42, 134);
      this.checkBoxHämtaEpostAdresssFrånRapport.Name = "checkBoxHämtaEpostAdresssFrånRapport";
      this.checkBoxHämtaEpostAdresssFrånRapport.Size = new System.Drawing.Size(217, 17);
      this.checkBoxHämtaEpostAdresssFrånRapport.TabIndex = 7;
      this.checkBoxHämtaEpostAdresssFrånRapport.Text = "Hämta e-post adress från rapport";
      this.checkBoxHämtaEpostAdresssFrånRapport.UseVisualStyleBackColor = true;
      this.checkBoxHämtaEpostAdresssFrånRapport.CheckedChanged += new System.EventHandler(this.checkBoxHämtaEpostAdresssFrånRapport_CheckedChanged);
      // 
      // textBoxDinEpost
      // 
      this.textBoxDinEpost.Location = new System.Drawing.Point(240, 72);
      this.textBoxDinEpost.MaxLength = 32;
      this.textBoxDinEpost.Name = "textBoxDinEpost";
      this.textBoxDinEpost.Size = new System.Drawing.Size(233, 21);
      this.textBoxDinEpost.TabIndex = 4;
      // 
      // textBoxNamn
      // 
      this.textBoxNamn.Location = new System.Drawing.Point(240, 46);
      this.textBoxNamn.MaxLength = 32;
      this.textBoxNamn.Name = "textBoxNamn";
      this.textBoxNamn.Size = new System.Drawing.Size(233, 21);
      this.textBoxNamn.TabIndex = 2;
      // 
      // textBoxMottagarensEpost
      // 
      this.textBoxMottagarensEpost.Enabled = false;
      this.textBoxMottagarensEpost.Location = new System.Drawing.Point(240, 101);
      this.textBoxMottagarensEpost.MaxLength = 32;
      this.textBoxMottagarensEpost.Name = "textBoxMottagarensEpost";
      this.textBoxMottagarensEpost.Size = new System.Drawing.Size(233, 21);
      this.textBoxMottagarensEpost.TabIndex = 6;
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(38, 104);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(160, 13);
      this.label6.TabIndex = 5;
      this.label6.Text = "Mottagarens E-postadress:";
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(38, 76);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(105, 13);
      this.label5.TabIndex = 3;
      this.label5.Text = "Din E-postadress:";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(38, 49);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(66, 13);
      this.label4.TabIndex = 1;
      this.label4.Text = "Ditt namn:";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.Location = new System.Drawing.Point(26, 26);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(109, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Produktionsledare";
      // 
      // richTextBoxMeddelandeGodkänd
      // 
      this.richTextBoxMeddelandeGodkänd.Location = new System.Drawing.Point(125, 78);
      this.richTextBoxMeddelandeGodkänd.MaxLength = 256;
      this.richTextBoxMeddelandeGodkänd.Name = "richTextBoxMeddelandeGodkänd";
      this.richTextBoxMeddelandeGodkänd.Size = new System.Drawing.Size(347, 96);
      this.richTextBoxMeddelandeGodkänd.TabIndex = 4;
      this.richTextBoxMeddelandeGodkänd.Text = "";
      // 
      // label11
      // 
      this.label11.Location = new System.Drawing.Point(37, 78);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(80, 49);
      this.label11.TabIndex = 3;
      this.label11.Text = "Standard meddelande:";
      // 
      // textBoxTitelGodkänd
      // 
      this.textBoxTitelGodkänd.Location = new System.Drawing.Point(239, 46);
      this.textBoxTitelGodkänd.MaxLength = 32;
      this.textBoxTitelGodkänd.Name = "textBoxTitelGodkänd";
      this.textBoxTitelGodkänd.Size = new System.Drawing.Size(233, 21);
      this.textBoxTitelGodkänd.TabIndex = 2;
      // 
      // label10
      // 
      this.label10.AutoSize = true;
      this.label10.Location = new System.Drawing.Point(37, 49);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(88, 13);
      this.label10.TabIndex = 1;
      this.label10.Text = "Standard titel:";
      // 
      // groupBoxInternEpost
      // 
      this.groupBoxInternEpost.Controls.Add(this.textBoxSMTPPort);
      this.groupBoxInternEpost.Controls.Add(this.label12);
      this.groupBoxInternEpost.Controls.Add(this.textBoxLosenord);
      this.groupBoxInternEpost.Controls.Add(this.textBoxAnvandare);
      this.groupBoxInternEpost.Controls.Add(this.textBoxSMTP);
      this.groupBoxInternEpost.Controls.Add(this.label9);
      this.groupBoxInternEpost.Controls.Add(this.label8);
      this.groupBoxInternEpost.Controls.Add(this.label7);
      this.groupBoxInternEpost.Controls.Add(this.label2);
      this.groupBoxInternEpost.Controls.Add(this.label3);
      this.groupBoxInternEpost.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.groupBoxInternEpost.Location = new System.Drawing.Point(508, 3);
      this.groupBoxInternEpost.Name = "groupBoxInternEpost";
      this.groupBoxInternEpost.Size = new System.Drawing.Size(502, 193);
      this.groupBoxInternEpost.TabIndex = 1;
      this.groupBoxInternEpost.TabStop = false;
      this.groupBoxInternEpost.Text = "INTERN E-POST";
      // 
      // textBoxSMTPPort
      // 
      this.textBoxSMTPPort.Location = new System.Drawing.Point(240, 72);
      this.textBoxSMTPPort.MaxLength = 8;
      this.textBoxSMTPPort.Name = "textBoxSMTPPort";
      this.textBoxSMTPPort.Size = new System.Drawing.Size(51, 21);
      this.textBoxSMTPPort.TabIndex = 4;
      // 
      // label12
      // 
      this.label12.AutoSize = true;
      this.label12.Location = new System.Drawing.Point(29, 72);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(34, 13);
      this.label12.TabIndex = 3;
      this.label12.Text = "Port:";
      // 
      // textBoxLosenord
      // 
      this.textBoxLosenord.Location = new System.Drawing.Point(240, 156);
      this.textBoxLosenord.MaxLength = 32;
      this.textBoxLosenord.Name = "textBoxLosenord";
      this.textBoxLosenord.PasswordChar = '*';
      this.textBoxLosenord.Size = new System.Drawing.Size(233, 21);
      this.textBoxLosenord.TabIndex = 9;
      // 
      // textBoxAnvandare
      // 
      this.textBoxAnvandare.Location = new System.Drawing.Point(240, 130);
      this.textBoxAnvandare.MaxLength = 32;
      this.textBoxAnvandare.Name = "textBoxAnvandare";
      this.textBoxAnvandare.Size = new System.Drawing.Size(233, 21);
      this.textBoxAnvandare.TabIndex = 7;
      // 
      // textBoxSMTP
      // 
      this.textBoxSMTP.Location = new System.Drawing.Point(240, 45);
      this.textBoxSMTP.MaxLength = 32;
      this.textBoxSMTP.Name = "textBoxSMTP";
      this.textBoxSMTP.Size = new System.Drawing.Size(233, 21);
      this.textBoxSMTP.TabIndex = 2;
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Location = new System.Drawing.Point(29, 160);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(62, 13);
      this.label9.TabIndex = 8;
      this.label9.Text = "Lösenord:";
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(29, 137);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(97, 13);
      this.label8.TabIndex = 6;
      this.label8.Text = "Användarnamn:";
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(29, 48);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(208, 13);
      this.label7.TabIndex = 1;
      this.label7.Text = "Server för utgående e-post (SMTP):";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label2.Location = new System.Drawing.Point(26, 25);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(106, 13);
      this.label2.TabIndex = 0;
      this.label2.Text = "Serverinformation";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label3.Location = new System.Drawing.Point(26, 108);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(134, 13);
      this.label3.TabIndex = 5;
      this.label3.Text = "Inloggningsinformation";
      // 
      // buttonAvbryt
      // 
      this.buttonAvbryt.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.buttonAvbryt.Location = new System.Drawing.Point(903, 17);
      this.buttonAvbryt.Name = "buttonAvbryt";
      this.buttonAvbryt.Size = new System.Drawing.Size(87, 30);
      this.buttonAvbryt.TabIndex = 1;
      this.buttonAvbryt.Text = "Avbryt";
      this.buttonAvbryt.UseVisualStyleBackColor = true;
      this.buttonAvbryt.Click += new System.EventHandler(this.buttonAvbryt_Click);
      // 
      // buttonOk
      // 
      this.buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.buttonOk.Location = new System.Drawing.Point(12, 17);
      this.buttonOk.Name = "buttonOk";
      this.buttonOk.Size = new System.Drawing.Size(87, 30);
      this.buttonOk.TabIndex = 0;
      this.buttonOk.Text = "OK";
      this.buttonOk.UseVisualStyleBackColor = true;
      this.buttonOk.Click += new System.EventHandler(this.buttonOk_Click);
      // 
      // richTextBoxMeddelandeNekad
      // 
      this.richTextBoxMeddelandeNekad.Location = new System.Drawing.Point(125, 78);
      this.richTextBoxMeddelandeNekad.MaxLength = 256;
      this.richTextBoxMeddelandeNekad.Name = "richTextBoxMeddelandeNekad";
      this.richTextBoxMeddelandeNekad.Size = new System.Drawing.Size(347, 96);
      this.richTextBoxMeddelandeNekad.TabIndex = 4;
      this.richTextBoxMeddelandeNekad.Text = "";
      // 
      // textBoxTitelNekad
      // 
      this.textBoxTitelNekad.Location = new System.Drawing.Point(239, 46);
      this.textBoxTitelNekad.MaxLength = 32;
      this.textBoxTitelNekad.Name = "textBoxTitelNekad";
      this.textBoxTitelNekad.Size = new System.Drawing.Size(233, 21);
      this.textBoxTitelNekad.TabIndex = 2;
      // 
      // label14
      // 
      this.label14.AutoSize = true;
      this.label14.Location = new System.Drawing.Point(37, 49);
      this.label14.Name = "label14";
      this.label14.Size = new System.Drawing.Size(88, 13);
      this.label14.TabIndex = 1;
      this.label14.Text = "Standard titel:";
      // 
      // label15
      // 
      this.label15.Location = new System.Drawing.Point(37, 81);
      this.label15.Name = "label15";
      this.label15.Size = new System.Drawing.Size(80, 47);
      this.label15.TabIndex = 3;
      this.label15.Text = "Standard meddelande:";
      // 
      // checkBoxVisaDialogSändGodkänd
      // 
      this.checkBoxVisaDialogSändGodkänd.AutoSize = true;
      this.checkBoxVisaDialogSändGodkänd.Checked = true;
      this.checkBoxVisaDialogSändGodkänd.CheckState = System.Windows.Forms.CheckState.Checked;
      this.checkBoxVisaDialogSändGodkänd.Location = new System.Drawing.Point(125, 180);
      this.checkBoxVisaDialogSändGodkänd.Name = "checkBoxVisaDialogSändGodkänd";
      this.checkBoxVisaDialogSändGodkänd.Size = new System.Drawing.Size(160, 17);
      this.checkBoxVisaDialogSändGodkänd.TabIndex = 5;
      this.checkBoxVisaDialogSändGodkänd.Text = "Visa dialog vid sändning";
      this.checkBoxVisaDialogSändGodkänd.UseVisualStyleBackColor = true;
      // 
      // checkBoxVisaDialogSändNekad
      // 
      this.checkBoxVisaDialogSändNekad.AutoSize = true;
      this.checkBoxVisaDialogSändNekad.Checked = true;
      this.checkBoxVisaDialogSändNekad.CheckState = System.Windows.Forms.CheckState.Checked;
      this.checkBoxVisaDialogSändNekad.Location = new System.Drawing.Point(125, 180);
      this.checkBoxVisaDialogSändNekad.Name = "checkBoxVisaDialogSändNekad";
      this.checkBoxVisaDialogSändNekad.Size = new System.Drawing.Size(160, 17);
      this.checkBoxVisaDialogSändNekad.TabIndex = 5;
      this.checkBoxVisaDialogSändNekad.Text = "Visa dialog vid sändning";
      this.checkBoxVisaDialogSändNekad.UseVisualStyleBackColor = true;
      // 
      // groupBoxNekadRapport
      // 
      this.groupBoxNekadRapport.Controls.Add(this.checkBoxBifogaRapportNekad);
      this.groupBoxNekadRapport.Controls.Add(this.label16);
      this.groupBoxNekadRapport.Controls.Add(this.checkBoxVisaDialogSändNekad);
      this.groupBoxNekadRapport.Controls.Add(this.richTextBoxMeddelandeNekad);
      this.groupBoxNekadRapport.Controls.Add(this.label14);
      this.groupBoxNekadRapport.Controls.Add(this.label15);
      this.groupBoxNekadRapport.Controls.Add(this.textBoxTitelNekad);
      this.groupBoxNekadRapport.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.groupBoxNekadRapport.Location = new System.Drawing.Point(8, 188);
      this.groupBoxNekadRapport.Name = "groupBoxNekadRapport";
      this.groupBoxNekadRapport.Size = new System.Drawing.Size(502, 204);
      this.groupBoxNekadRapport.TabIndex = 2;
      this.groupBoxNekadRapport.TabStop = false;
      this.groupBoxNekadRapport.Text = "NEKAD RAPPORT";
      // 
      // checkBoxBifogaRapportNekad
      // 
      this.checkBoxBifogaRapportNekad.AutoSize = true;
      this.checkBoxBifogaRapportNekad.Checked = true;
      this.checkBoxBifogaRapportNekad.CheckState = System.Windows.Forms.CheckState.Checked;
      this.checkBoxBifogaRapportNekad.Location = new System.Drawing.Point(318, 181);
      this.checkBoxBifogaRapportNekad.Name = "checkBoxBifogaRapportNekad";
      this.checkBoxBifogaRapportNekad.Size = new System.Drawing.Size(154, 17);
      this.checkBoxBifogaRapportNekad.TabIndex = 6;
      this.checkBoxBifogaRapportNekad.Text = "Bifoga Rapport (*.keg)";
      this.checkBoxBifogaRapportNekad.UseVisualStyleBackColor = true;
      // 
      // label16
      // 
      this.label16.AutoSize = true;
      this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label16.Location = new System.Drawing.Point(28, 23);
      this.label16.Name = "label16";
      this.label16.Size = new System.Drawing.Size(56, 13);
      this.label16.TabIndex = 0;
      this.label16.Text = "Respons";
      // 
      // groupBoxGodkändRapport
      // 
      this.groupBoxGodkändRapport.Controls.Add(this.checkBoxBifogaRapportGodkänd);
      this.groupBoxGodkändRapport.Controls.Add(this.label13);
      this.groupBoxGodkändRapport.Controls.Add(this.checkBoxVisaDialogSändGodkänd);
      this.groupBoxGodkändRapport.Controls.Add(this.richTextBoxMeddelandeGodkänd);
      this.groupBoxGodkändRapport.Controls.Add(this.label10);
      this.groupBoxGodkändRapport.Controls.Add(this.textBoxTitelGodkänd);
      this.groupBoxGodkändRapport.Controls.Add(this.label11);
      this.groupBoxGodkändRapport.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.groupBoxGodkändRapport.Location = new System.Drawing.Point(508, 188);
      this.groupBoxGodkändRapport.Name = "groupBoxGodkändRapport";
      this.groupBoxGodkändRapport.Size = new System.Drawing.Size(502, 204);
      this.groupBoxGodkändRapport.TabIndex = 3;
      this.groupBoxGodkändRapport.TabStop = false;
      this.groupBoxGodkändRapport.Text = "GODKÄND RAPPORT";
      // 
      // checkBoxBifogaRapportGodkänd
      // 
      this.checkBoxBifogaRapportGodkänd.AutoSize = true;
      this.checkBoxBifogaRapportGodkänd.Checked = true;
      this.checkBoxBifogaRapportGodkänd.CheckState = System.Windows.Forms.CheckState.Checked;
      this.checkBoxBifogaRapportGodkänd.Location = new System.Drawing.Point(314, 180);
      this.checkBoxBifogaRapportGodkänd.Name = "checkBoxBifogaRapportGodkänd";
      this.checkBoxBifogaRapportGodkänd.Size = new System.Drawing.Size(154, 17);
      this.checkBoxBifogaRapportGodkänd.TabIndex = 6;
      this.checkBoxBifogaRapportGodkänd.Text = "Bifoga Rapport (*.keg)";
      this.checkBoxBifogaRapportGodkänd.UseVisualStyleBackColor = true;
      // 
      // label13
      // 
      this.label13.AutoSize = true;
      this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label13.Location = new System.Drawing.Point(24, 23);
      this.label13.Name = "label13";
      this.label13.Size = new System.Drawing.Size(53, 13);
      this.label13.TabIndex = 0;
      this.label13.Text = "Kvittens";
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.buttonOk);
      this.groupBox1.Controls.Add(this.buttonAvbryt);
      this.groupBox1.Location = new System.Drawing.Point(8, 387);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(1002, 57);
      this.groupBox1.TabIndex = 4;
      this.groupBox1.TabStop = false;
      // 
      // MailSettings
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1016, 451);
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this.groupBoxGodkändRapport);
      this.Controls.Add(this.groupBoxNekadRapport);
      this.Controls.Add(this.groupBoxEpost);
      this.Controls.Add(this.groupBoxInternEpost);
      this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Name = "MailSettings";
      this.RightToLeftLayout = true;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "E-post inställningar för produktionsledare";
      this.Load += new System.EventHandler(this.MailSettings_Load);
      this.groupBoxEpost.ResumeLayout(false);
      this.groupBoxEpost.PerformLayout();
      this.groupBoxInternEpost.ResumeLayout(false);
      this.groupBoxInternEpost.PerformLayout();
      this.groupBoxNekadRapport.ResumeLayout(false);
      this.groupBoxNekadRapport.PerformLayout();
      this.groupBoxGodkändRapport.ResumeLayout(false);
      this.groupBoxGodkändRapport.PerformLayout();
      this.groupBox1.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox groupBoxEpost;
    private System.Windows.Forms.GroupBox groupBoxInternEpost;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.TextBox textBoxMottagarensEpost;
    private System.Windows.Forms.TextBox textBoxDinEpost;
    private System.Windows.Forms.TextBox textBoxNamn;
    private System.Windows.Forms.TextBox textBoxSMTP;
    private System.Windows.Forms.TextBox textBoxLosenord;
    private System.Windows.Forms.TextBox textBoxAnvandare;
    private System.Windows.Forms.Button buttonAvbryt;
    private System.Windows.Forms.Button buttonOk;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.TextBox textBoxTitelGodkänd;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.RichTextBox richTextBoxMeddelandeGodkänd;
    private System.Windows.Forms.TextBox textBoxSMTPPort;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.CheckBox checkBoxHämtaEpostAdresssFrånRapport;
    private System.Windows.Forms.Label label15;
    private System.Windows.Forms.RichTextBox richTextBoxMeddelandeNekad;
    private System.Windows.Forms.TextBox textBoxTitelNekad;
    private System.Windows.Forms.Label label14;
    private System.Windows.Forms.CheckBox checkBoxVisaDialogSändNekad;
    private System.Windows.Forms.CheckBox checkBoxVisaDialogSändGodkänd;
    private System.Windows.Forms.GroupBox groupBoxNekadRapport;
    private System.Windows.Forms.GroupBox groupBoxGodkändRapport;
    private System.Windows.Forms.Label label13;
    private System.Windows.Forms.Label label16;
    private System.Windows.Forms.CheckBox checkBoxAnvändInternEpost;
    private System.Windows.Forms.CheckBox checkBoxSändNekadRespons;
    private System.Windows.Forms.CheckBox checkBoxSändGodkändKvittens;
    private System.Windows.Forms.CheckBox checkBoxBifogaRapportGodkänd;
    private System.Windows.Forms.CheckBox checkBoxBifogaRapportNekad;
    private System.Windows.Forms.GroupBox groupBox1;
  }
}