﻿namespace Egenuppfoljning.Dialogs
{
  partial class FormSparaEntreprenör
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSparaEntreprenör));
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.labelRegion = new System.Windows.Forms.Label();
      this.label43 = new System.Windows.Forms.Label();
      this.labelEntreprenörNr = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.textBoxEntpreprenörNamn = new System.Windows.Forms.TextBox();
      this.label2 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.buttonSpara = new System.Windows.Forms.Button();
      this.imageListSpara = new System.Windows.Forms.ImageList(this.components);
      this.buttonAvbryt = new System.Windows.Forms.Button();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.groupBox1.SuspendLayout();
      this.groupBox2.SuspendLayout();
      this.SuspendLayout();
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.labelRegion);
      this.groupBox1.Controls.Add(this.label43);
      this.groupBox1.Controls.Add(this.labelEntreprenörNr);
      this.groupBox1.Controls.Add(this.label1);
      this.groupBox1.Controls.Add(this.textBoxEntpreprenörNamn);
      this.groupBox1.Controls.Add(this.label2);
      this.groupBox1.Controls.Add(this.label3);
      this.groupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.groupBox1.Location = new System.Drawing.Point(7, 3);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(455, 187);
      this.groupBox1.TabIndex = 0;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Entreprenör / Maskin";
      // 
      // labelRegion
      // 
      this.labelRegion.AutoSize = true;
      this.labelRegion.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelRegion.Location = new System.Drawing.Point(64, 93);
      this.labelRegion.Name = "labelRegion";
      this.labelRegion.Size = new System.Drawing.Size(14, 13);
      this.labelRegion.TabIndex = 6;
      this.labelRegion.Text = "_";
      // 
      // label43
      // 
      this.label43.AutoSize = true;
      this.label43.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label43.Location = new System.Drawing.Point(15, 93);
      this.label43.Name = "label43";
      this.label43.Size = new System.Drawing.Size(49, 13);
      this.label43.TabIndex = 5;
      this.label43.Text = "Region:";
      // 
      // labelEntreprenörNr
      // 
      this.labelEntreprenörNr.AutoSize = true;
      this.labelEntreprenörNr.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelEntreprenörNr.Location = new System.Drawing.Point(162, 66);
      this.labelEntreprenörNr.Name = "labelEntreprenörNr";
      this.labelEntreprenörNr.Size = new System.Drawing.Size(14, 13);
      this.labelEntreprenörNr.TabIndex = 2;
      this.labelEntreprenörNr.Text = "_";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.Location = new System.Drawing.Point(15, 125);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(243, 13);
      this.label1.TabIndex = 3;
      this.label1.Text = "Skriv in ett namn för Entreprenör/Maskin:";
      // 
      // textBoxEntpreprenörNamn
      // 
      this.textBoxEntpreprenörNamn.Location = new System.Drawing.Point(18, 150);
      this.textBoxEntpreprenörNamn.MaxLength = 32;
      this.textBoxEntpreprenörNamn.Name = "textBoxEntpreprenörNamn";
      this.textBoxEntpreprenörNamn.Size = new System.Drawing.Size(418, 21);
      this.textBoxEntpreprenörNamn.TabIndex = 4;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label2.Location = new System.Drawing.Point(15, 66);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(148, 13);
      this.label2.TabIndex = 1;
      this.label2.Text = "Entreprenörnr/Maskinnr:";
      // 
      // label3
      // 
      this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label3.Location = new System.Drawing.Point(15, 21);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(422, 35);
      this.label3.TabIndex = 0;
      this.label3.Text = "Entreprenörnr/Maskinr för denna region saknar ett tillhörande namn vilket krävs. " +
          "Vill du lägga till ett namn eller avbryta att lagra rapporten?";
      // 
      // buttonSpara
      // 
      this.buttonSpara.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.buttonSpara.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.buttonSpara.ImageIndex = 0;
      this.buttonSpara.ImageList = this.imageListSpara;
      this.buttonSpara.Location = new System.Drawing.Point(18, 16);
      this.buttonSpara.Name = "buttonSpara";
      this.buttonSpara.Size = new System.Drawing.Size(137, 30);
      this.buttonSpara.TabIndex = 0;
      this.buttonSpara.Text = "Spara";
      this.buttonSpara.UseVisualStyleBackColor = true;
      this.buttonSpara.Click += new System.EventHandler(this.buttonSpara_Click);
      // 
      // imageListSpara
      // 
      this.imageListSpara.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListSpara.ImageStream")));
      this.imageListSpara.TransparentColor = System.Drawing.Color.Transparent;
      this.imageListSpara.Images.SetKeyName(0, "Lagra.ico");
      this.imageListSpara.Images.SetKeyName(1, "Neka.ico");
      // 
      // buttonAvbryt
      // 
      this.buttonAvbryt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.buttonAvbryt.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.buttonAvbryt.ImageIndex = 1;
      this.buttonAvbryt.ImageList = this.imageListSpara;
      this.buttonAvbryt.Location = new System.Drawing.Point(300, 16);
      this.buttonAvbryt.Name = "buttonAvbryt";
      this.buttonAvbryt.Size = new System.Drawing.Size(137, 30);
      this.buttonAvbryt.TabIndex = 1;
      this.buttonAvbryt.Text = "Avbryt";
      this.buttonAvbryt.UseVisualStyleBackColor = true;
      this.buttonAvbryt.Click += new System.EventHandler(this.buttonAvbryt_Click);
      // 
      // groupBox2
      // 
      this.groupBox2.Controls.Add(this.buttonAvbryt);
      this.groupBox2.Controls.Add(this.buttonSpara);
      this.groupBox2.Location = new System.Drawing.Point(7, 180);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(455, 55);
      this.groupBox2.TabIndex = 1;
      this.groupBox2.TabStop = false;
      // 
      // FormSparaEntreprenör
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(470, 245);
      this.Controls.Add(this.groupBox2);
      this.Controls.Add(this.groupBox1);
      this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Name = "FormSparaEntreprenör";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Spara namn för entreprenör / maskin";
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.groupBox2.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Button buttonSpara;
    private System.Windows.Forms.Button buttonAvbryt;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TextBox textBoxEntpreprenörNamn;
    private System.Windows.Forms.Label labelEntreprenörNr;
    private System.Windows.Forms.ImageList imageListSpara;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.Label labelRegion;
    private System.Windows.Forms.Label label43;
  }
}