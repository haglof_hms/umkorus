﻿namespace Egenuppfoljning.Dialogs
{
  partial class FormProgress
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormProgress));
      this.progressBarVänta = new System.Windows.Forms.ProgressBar();
      this.labelProgress = new System.Windows.Forms.Label();
      this.buttonAvbryt = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // progressBarVänta
      // 
      this.progressBarVänta.Location = new System.Drawing.Point(14, 73);
      this.progressBarVänta.Name = "progressBarVänta";
      this.progressBarVänta.Size = new System.Drawing.Size(318, 23);
      this.progressBarVänta.TabIndex = 1;
      // 
      // labelProgress
      // 
      this.labelProgress.AutoSize = true;
      this.labelProgress.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelProgress.Location = new System.Drawing.Point(15, 13);
      this.labelProgress.Name = "labelProgress";
      this.labelProgress.Size = new System.Drawing.Size(257, 13);
      this.labelProgress.TabIndex = 0;
      this.labelProgress.Text = "Laddar Rapport för KORUS Egenuppföljning...";
      // 
      // buttonAvbryt
      // 
      this.buttonAvbryt.Location = new System.Drawing.Point(345, 73);
      this.buttonAvbryt.Name = "buttonAvbryt";
      this.buttonAvbryt.Size = new System.Drawing.Size(87, 23);
      this.buttonAvbryt.TabIndex = 2;
      this.buttonAvbryt.Text = "Avbryt";
      this.buttonAvbryt.UseVisualStyleBackColor = true;
      this.buttonAvbryt.Click += new System.EventHandler(this.buttonAvbryt_Click);
      // 
      // FormProgress
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(437, 108);
      this.Controls.Add(this.buttonAvbryt);
      this.Controls.Add(this.labelProgress);
      this.Controls.Add(this.progressBarVänta);
      this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.Name = "FormProgress";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Laddar..";
      this.Load += new System.EventHandler(this.FormProgress_Load);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.ProgressBar progressBarVänta;
    private System.Windows.Forms.Label labelProgress;
    private System.Windows.Forms.Button buttonAvbryt;
  }
}