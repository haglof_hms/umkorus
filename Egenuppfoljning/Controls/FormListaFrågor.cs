﻿#region

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Egenuppfoljning.Dialogs;
using Egenuppfoljning.Interfaces;
using Egenuppfoljning.Properties;

#endregion

namespace Egenuppfoljning.Controls
{
    // ReSharper disable UnusedMember.Global
    public partial class FormListaFrågor : UserControl
    // ReSharper restore UnusedMember.Global
    {
        private List<KorusDataEventArgs.Frågor> mFrågor;

        public FormListaFrågor()
        {
            InitializeComponent();
            mFrågor = new List<KorusDataEventArgs.Frågor>();
        }

        // ReSharper disable EventNeverSubscribedTo.Global
        public event StatusEventHandler StatusEventRefresh;
        public event DataEventHandler DataEventGåTillRapport;
        public event DataEventHandler DataEventTaBortFråga;
        public event FrågorEventHandler DataEventLagraÄndradeFrågor;
        // ReSharper restore EventNeverSubscribedTo.Global
        // ReSharper disable UnusedMember.Global
        public void Close()
        // ReSharper restore UnusedMember.Global
        {
            Settings.Default.Save();
            Hide();
        }

        // ReSharper disable UnusedMember.Global
        public void UppdateraFrågeformulärLista(List<KorusDataEventArgs.Frågor> frågor)
        // ReSharper restore UnusedMember.Global
        {
            mFrågor = frågor;
            UppdateraDataSet();
        }

        private static int GetObjIntValue(object aObjValue)
        {
            var intValue = -1;
            if (aObjValue != null && !aObjValue.ToString().Equals(string.Empty))
            {
                if (!(int.TryParse(aObjValue.ToString(), out intValue)))
                {
                    MessageBox.Show(
                      Resources.Vardet + aObjValue + Resources.ar_ett_felaktigt_nyckelvarde,
                      Resources.Felaktigt_nyckelvarde, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            return intValue;
        }


        private KorusDataEventArgs GetSelectedKORUSNyckelData()
        {
            if (dataGridViewFrågor.SelectedCells.Count > 0)
            {
                var row = dataGridViewFrågor.SelectedCells[0].OwningRow;
                var nyckelData = new KorusDataEventArgs
                  {
                      data =
                        {
                            invtyp = GetObjIntValue(row.Cells[invtypIdDataGridViewTextBoxColumn.Name].Value),
                            regionid = GetObjIntValue(row.Cells[regionIdDataGridViewTextBoxColumn.Name].Value),
                            distriktid = GetObjIntValue(row.Cells[distriktIdDataGridViewTextBoxColumn.Name].Value),
                            traktnr = GetObjIntValue(row.Cells[traktnrDataGridViewTextBoxColumn.Name].Value),
                            standort = GetObjIntValue(row.Cells[standortDataGridViewTextBoxColumn.Name].Value),
                            entreprenor =
                              GetObjIntValue(row.Cells[entreprenorDataGridViewTextBoxColumn.Name].Value),
                            rapport_typ =
                              DataHelper.GetRapportTyp(row.Cells[rapportTypDataGridViewTextBoxColumn.Name].Value),
                            årtal = GetObjIntValue(row.Cells[artalDataGridViewTextBoxColumn.Name].Value)
                        }
                  };

                if (nyckelData.KorrektaNyckelvärden)
                {
                    return nyckelData;
                }
            }
            return null;
        }

        // ReSharper disable MemberCanBePrivate.Global
        public void LagraÄndradeKORUSFrågor()
        // ReSharper restore MemberCanBePrivate.Global
        {
            if (MessageBox.Show(Resources.Vill_du_uppdatera_alla,
                                Resources.Uppdatera_alla_KORUS, MessageBoxButtons.YesNo, MessageBoxIcon.Question) !=
                DialogResult.Yes) return;
            var frågor = GetAllKORUSFrågor();

            if (frågor.Count > 0)
            {
                DataEventLagraÄndradeFrågor(this, frågor);
            }
        }

        private List<KorusDataEventArgs.Frågor> GetAllKORUSFrågor()
        {
            var frågor = new List<KorusDataEventArgs.Frågor>();
            if (dataGridViewFrågor.Rows.Count > 0)
            {
                frågor.AddRange(from DataGridViewRow row in dataGridViewFrågor.Rows
                                select new KorusDataEventArgs.Frågor
                                  {
                                      invtyp = GetObjIntValue(row.Cells[invtypIdDataGridViewTextBoxColumn.Name].Value),
                                      regionid = GetObjIntValue(row.Cells[regionIdDataGridViewTextBoxColumn.Name].Value),
                                      distriktid =
                                        GetObjIntValue(row.Cells[distriktIdDataGridViewTextBoxColumn.Name].Value),
                                      traktnr = GetObjIntValue(row.Cells[traktnrDataGridViewTextBoxColumn.Name].Value),
                                      standort = GetObjIntValue(row.Cells[standortDataGridViewTextBoxColumn.Name].Value),
                                      entreprenor =
                                        GetObjIntValue(row.Cells[entreprenorDataGridViewTextBoxColumn.Name].Value),
                                      rapport_typ =
                                        DataHelper.GetRapportTyp(row.Cells[rapportTypDataGridViewTextBoxColumn.Name].Value),
                                      årtal = GetObjIntValue(row.Cells[artalDataGridViewTextBoxColumn.Name].Value),
                                      fraga1 = row.Cells[fraga1DataGridViewTextBoxColumn.Name].Value.ToString(),
                                      fraga2 = row.Cells[fraga2DataGridViewTextBoxColumn.Name].Value.ToString(),
                                      fraga3 = row.Cells[fraga3DataGridViewTextBoxColumn.Name].Value.ToString(),
                                      fraga4 = row.Cells[fraga4DataGridViewTextBoxColumn.Name].Value.ToString(),
                                      fraga5 = row.Cells[fraga5DataGridViewTextBoxColumn.Name].Value.ToString(),
                                      fraga6 = row.Cells[fraga6DataGridViewTextBoxColumn.Name].Value.ToString(),
                                      fraga7 = row.Cells[fraga7DataGridViewTextBoxColumn.Name].Value.ToString(),
                                      fraga8 = row.Cells[fraga8DataGridViewTextBoxColumn.Name].Value.ToString(),
                                      fraga9 = row.Cells[fraga9DataGridViewTextBoxColumn.Name].Value.ToString(),
                                      fraga10 = row.Cells[fraga10DataGridViewTextBoxColumn.Name].Value.ToString(),
                                      fraga11 = row.Cells[fraga11DataGridViewTextBoxColumn.Name].Value.ToString(),
                                      fraga12 = row.Cells[fraga12DataGridViewTextBoxColumn.Name].Value.ToString(),
                                      fraga13 = row.Cells[fraga13DataGridViewTextBoxColumn.Name].Value.ToString(),
                                      fraga14 = row.Cells[fraga14DataGridViewTextBoxColumn.Name].Value.ToString(),
                                      fraga15 = row.Cells[fraga15DataGridViewTextBoxColumn.Name].Value.ToString(),
                                      fraga16 = row.Cells[fraga16DataGridViewTextBoxColumn.Name].Value.ToString(),
                                      fraga17 = row.Cells[fraga17DataGridViewTextBoxColumn.Name].Value.ToString(),
                                      fraga18 = row.Cells[fraga18DataGridViewTextBoxColumn.Name].Value.ToString(),
                                      fraga19 = row.Cells[fraga19DataGridViewTextBoxColumn.Name].Value.ToString(),
                                      fraga20 = row.Cells[fraga20DataGridViewTextBoxColumn.Name].Value.ToString(),
                                      fraga21 = row.Cells[fraga21DataGridViewTextBoxColumn.Name].Value.ToString(),
                                      ovrigt = row.Cells[ovrigtDataGridViewTextBoxColumn.Name].Value.ToString()
                                  });
            }
            return frågor;
        }

        // ReSharper disable MemberCanBePrivate.Global
        public void GåTillRapportData()
        // ReSharper restore MemberCanBePrivate.Global
        {
            var data = GetSelectedKORUSNyckelData();

            if (data != null)
            {
                DataEventGåTillRapport(this, data);
            }
        }

        // ReSharper disable MemberCanBePrivate.Global
        public void TaBortKORUSFråga()
        // ReSharper restore MemberCanBePrivate.Global
        {
            var data = GetSelectedKORUSNyckelData();

            if (data != null)
            {
                DataEventTaBortFråga(this, data);
            }
        }

        // ReSharper disable UnusedMember.Global
        public void TaBortKORUSDataLyckades()
        // ReSharper restore UnusedMember.Global
        {
            if (dataGridViewFrågor.SelectedCells.Count > 0)
            {
                dataGridViewFrågor.Rows.RemoveAt(dataGridViewFrågor.SelectedCells[0].OwningRow.Index);
            }
        }

        private static bool Filter(KorusDataEventArgs.Frågor aFråga)
        {
            if (Settings.Default.FiltreraFragaInvTypId > 0 && (aFråga.invtyp != Settings.Default.FiltreraFragaInvTypId))
            {
                return false;
            }
            if (Settings.Default.FiltreraFragaRegionId > 0 && (aFråga.regionid != Settings.Default.FiltreraFragaRegionId))
            {
                return false;
            }
            if (Settings.Default.FiltreraFragaDistriktId > 0 &&
                (aFråga.distriktid != Settings.Default.FiltreraFragaDistriktId))
            {
                return false;
            }
            if (Settings.Default.FiltreraFragaTraktNr > 0 && (aFråga.traktnr != Settings.Default.FiltreraFragaTraktNr))
            {
                return false;
            }
            if (Settings.Default.FiltreraFragaStandort > 0 && (aFråga.standort != Settings.Default.FiltreraFragaStandort))
            {
                return false;
            }
            if (Settings.Default.FiltreraFragaEntreprenor > 0 &&
                (aFråga.entreprenor != Settings.Default.FiltreraFragaEntreprenor))
            {
                return false;
            }
            if (Settings.Default.FiltreraFragaArtal > 0 && (aFråga.årtal != Settings.Default.FiltreraFragaArtal))
            {
                return false;
            }

            if (!Settings.Default.FiltreraFragaRapportTypNamn.Equals(string.Empty) &&
                !(DataHelper.GetRapportTyp(aFråga.rapport_typ).Equals(Settings.Default.FiltreraFragaRapportTypNamn)))
            {
                return false;
            }
            if (!Settings.Default.FiltreraFraga1.Equals(string.Empty) &&
                !(aFråga.fraga1.Equals(Settings.Default.FiltreraFraga1)))
            {
                return false;
            }
            if (!Settings.Default.FiltreraFraga2.Equals(string.Empty) &&
                !(aFråga.fraga2.Equals(Settings.Default.FiltreraFraga2)))
            {
                return false;
            }
            if (!Settings.Default.FiltreraFraga3.Equals(string.Empty) &&
                !(aFråga.fraga3.Equals(Settings.Default.FiltreraFraga3)))
            {
                return false;
            }
            if (!Settings.Default.FiltreraFraga4.Equals(string.Empty) &&
                !(aFråga.fraga4.Equals(Settings.Default.FiltreraFraga4)))
            {
                return false;
            }
            if (!Settings.Default.FiltreraFraga5.Equals(string.Empty) &&
                !(aFråga.fraga5.Equals(Settings.Default.FiltreraFraga5)))
            {
                return false;
            }
            if (!Settings.Default.FiltreraFraga6.Equals(string.Empty) &&
                !(aFråga.fraga6.Equals(Settings.Default.FiltreraFraga6)))
            {
                return false;
            }
            if (!Settings.Default.FiltreraFraga7.Equals(string.Empty) &&
                !(aFråga.fraga7.Equals(Settings.Default.FiltreraFraga7)))
            {
                return false;
            }
            if (!Settings.Default.FiltreraFraga8.Equals(string.Empty) &&
                !(aFråga.fraga8.Equals(Settings.Default.FiltreraFraga8)))
            {
                return false;
            }
            if (!Settings.Default.FiltreraFraga9.Equals(string.Empty) &&
                !(aFråga.fraga9.Equals(Settings.Default.FiltreraFraga9)))
            {
                return false;
            }
            if (!Settings.Default.FiltreraFraga10.Equals(string.Empty) &&
                !(aFråga.fraga10.Equals(Settings.Default.FiltreraFraga10)))
            {
                return false;
            }
            if (!Settings.Default.FiltreraFraga11.Equals(string.Empty) &&
                !(aFråga.fraga11.Equals(Settings.Default.FiltreraFraga11)))
            {
                return false;
            }
            if (!Settings.Default.FiltreraFraga12.Equals(string.Empty) &&
                !(aFråga.fraga12.Equals(Settings.Default.FiltreraFraga12)))
            {
                return false;
            }
            if (!Settings.Default.FiltreraFraga13.Equals(string.Empty) &&
                !(aFråga.fraga13.Equals(Settings.Default.FiltreraFraga13)))
            {
                return false;
            }
            if (!Settings.Default.FiltreraFraga14.Equals(string.Empty) &&
                !(aFråga.fraga14.Equals(Settings.Default.FiltreraFraga14)))
            {
                return false;
            }
            if (!Settings.Default.FiltreraFraga15.Equals(string.Empty) &&
                !(aFråga.fraga15.Equals(Settings.Default.FiltreraFraga15)))
            {
                return false;
            }
            if (!Settings.Default.FiltreraFraga16.Equals(string.Empty) &&
                !(aFråga.fraga16.Equals(Settings.Default.FiltreraFraga16)))
            {
                return false;
            }
            if (!Settings.Default.FiltreraFraga17.Equals(string.Empty) &&
                !(aFråga.fraga17.Equals(Settings.Default.FiltreraFraga17)))
            {
                return false;
            }
            if (!Settings.Default.FiltreraFraga18.Equals(string.Empty) &&
                !(aFråga.fraga18.Equals(Settings.Default.FiltreraFraga18)))
            {
                return false;
            }
            if (!Settings.Default.FiltreraFraga19.Equals(string.Empty) &&
                !(aFråga.fraga19.Equals(Settings.Default.FiltreraFraga19)))
            {
                return false;
            }
            if (!Settings.Default.FiltreraFraga20.Equals(string.Empty) &&
                !(aFråga.fraga20.Equals(Settings.Default.FiltreraFraga20)))
            {
                return false;
            }
            if (!Settings.Default.FiltreraFraga21.Equals(string.Empty) &&
                !(aFråga.fraga21.Equals(Settings.Default.FiltreraFraga21)))
            {
                return false;
            }

            return Settings.Default.FiltreraFragaOvrigt.Equals(string.Empty) ||
                   (aFråga.ovrigt.Equals(Settings.Default.FiltreraFragaOvrigt));
        }

        private void UppdateraFilterText()
        {
            var filterText = "Filter: ";
            if (Settings.Default.FiltreraFragaInvTypId > 0)
            {
                filterText += " InvTypId: " + Settings.Default.FiltreraFragaInvTypNamn + ",";
            }

            if (Settings.Default.FiltreraFragaRegionId > 0)
            {
                filterText += " RegionId: " + Settings.Default.FiltreraFragaRegionId.ToString(CultureInfo.InvariantCulture) +
                              ",";
            }
            if (Settings.Default.FiltreraFragaDistriktId > 0)
            {
                filterText += " DistriktId: " + Settings.Default.FiltreraFragaDistriktId.ToString(CultureInfo.InvariantCulture) +
                              ",";
            }
            if (Settings.Default.FiltreraFragaTraktNr > 0)
            {
                filterText += " TraktNr: " + Settings.Default.FiltreraFragaTraktNr.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraFragaStandort > 0)
            {
                filterText += " Traktdel: " + Settings.Default.FiltreraFragaStandort.ToString(CultureInfo.InvariantCulture) +
                              ",";
            }
            if (Settings.Default.FiltreraFragaEntreprenor > 0)
            {
                filterText += " Entreprenör: " +
                              Settings.Default.FiltreraFragaEntreprenor.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraFragaArtal > 0)
            {
                filterText += " Artal: " + Settings.Default.FiltreraFragaArtal.ToString(CultureInfo.InvariantCulture) + ",";
            }

            if (!Settings.Default.FiltreraFragaRapportTypNamn.Equals(string.Empty))
            {
                filterText += " RapportTyp: " + Settings.Default.FiltreraFragaRapportTypNamn + ",";
            }
            if (!Settings.Default.FiltreraFraga1.Equals(string.Empty))
            {
                filterText += " Fråga1: " + Settings.Default.FiltreraFraga1 + ",";
            }
            if (!Settings.Default.FiltreraFraga2.Equals(string.Empty))
            {
                filterText += " Fråga2: " + Settings.Default.FiltreraFraga2 + ",";
            }
            if (!Settings.Default.FiltreraFraga3.Equals(string.Empty))
            {
                filterText += " Fråga3: " + Settings.Default.FiltreraFraga3 + ",";
            }
            if (!Settings.Default.FiltreraFraga4.Equals(string.Empty))
            {
                filterText += " Fråga4: " + Settings.Default.FiltreraFraga4 + ",";
            }
            if (!Settings.Default.FiltreraFraga5.Equals(string.Empty))
            {
                filterText += " Fråga5: " + Settings.Default.FiltreraFraga5 + ",";
            }
            if (!Settings.Default.FiltreraFraga6.Equals(string.Empty))
            {
                filterText += " Fråga6: " + Settings.Default.FiltreraFraga6 + ",";
            }
            if (!Settings.Default.FiltreraFraga7.Equals(string.Empty))
            {
                filterText += " Fråga7: " + Settings.Default.FiltreraFraga7 + ",";
            }
            if (!Settings.Default.FiltreraFraga8.Equals(string.Empty))
            {
                filterText += " Fråga8: " + Settings.Default.FiltreraFraga8 + ",";
            }
            if (!Settings.Default.FiltreraFraga9.Equals(string.Empty))
            {
                filterText += " Fråga9: " + Settings.Default.FiltreraFraga9 + ",";
            }
            if (!Settings.Default.FiltreraFraga10.Equals(string.Empty))
            {
                filterText += " Fråga10: " + Settings.Default.FiltreraFraga10 + ",";
            }
            if (!Settings.Default.FiltreraFraga11.Equals(string.Empty))
            {
                filterText += " Fråga11: " + Settings.Default.FiltreraFraga11 + ",";
            }
            if (!Settings.Default.FiltreraFraga12.Equals(string.Empty))
            {
                filterText += " Fråga12: " + Settings.Default.FiltreraFraga12 + ",";
            }
            if (!Settings.Default.FiltreraFraga13.Equals(string.Empty))
            {
                filterText += " Fråga13: " + Settings.Default.FiltreraFraga13 + ",";
            }
            if (!Settings.Default.FiltreraFraga14.Equals(string.Empty))
            {
                filterText += " Fråga14: " + Settings.Default.FiltreraFraga14 + ",";
            }
            if (!Settings.Default.FiltreraFraga15.Equals(string.Empty))
            {
                filterText += " Fråga15: " + Settings.Default.FiltreraFraga15 + ",";
            }
            if (!Settings.Default.FiltreraFraga16.Equals(string.Empty))
            {
                filterText += " Fråga16: " + Settings.Default.FiltreraFraga16 + ",";
            }
            if (!Settings.Default.FiltreraFraga17.Equals(string.Empty))
            {
                filterText += " Fråga17: " + Settings.Default.FiltreraFraga17 + ",";
            }
            if (!Settings.Default.FiltreraFraga18.Equals(string.Empty))
            {
                filterText += " Fråga18: " + Settings.Default.FiltreraFraga18 + ",";
            }
            if (!Settings.Default.FiltreraFraga19.Equals(string.Empty))
            {
                filterText += " Fråga19: " + Settings.Default.FiltreraFraga19 + ",";
            }
            if (!Settings.Default.FiltreraFraga20.Equals(string.Empty))
            {
                filterText += " Fråga20: " + Settings.Default.FiltreraFraga20 + ",";
            }
            if (!Settings.Default.FiltreraFraga21.Equals(string.Empty))
            {
                filterText += " Fråga21: " + Settings.Default.FiltreraFraga21 + ",";
            }

            if (filterText.Equals("Filter: "))
            {
                labelFiltrering.Text = filterText + Resources.Inget;
            }
            else
            {
                labelFiltrering.Text = filterText.Substring(0, filterText.Length - 1);
            }
        }


        private void UppdateraDataSet()
        {
            try
            {
                UppdateraFilterText();
                dataSetFrågor.Clear();

                //ytor
                foreach (var frågeformulär in mFrågor.Where(Filter))
                {
                    dataSetFrågor.Tables["Frågor"].Rows.Add(frågeformulär.regionid, frågeformulär.distriktid,
                                                            frågeformulär.traktnr, frågeformulär.standort,
                                                            frågeformulär.entreprenor,
                                                            DataHelper.GetRapportTyp(frågeformulär.rapport_typ),
                                                            frågeformulär.fraga1, frågeformulär.fraga2, frågeformulär.fraga3,
                                                            frågeformulär.fraga4, frågeformulär.fraga5, frågeformulär.fraga6,
                                                            frågeformulär.fraga7, frågeformulär.fraga8, frågeformulär.fraga9,
                                                            frågeformulär.fraga10, frågeformulär.fraga11, frågeformulär.fraga12,
                                                            frågeformulär.fraga13, frågeformulär.fraga14, frågeformulär.fraga15,
                                                            frågeformulär.fraga16, frågeformulär.fraga17, frågeformulär.fraga18,
                                                            frågeformulär.fraga19, frågeformulär.fraga20, frågeformulär.fraga21,
                                                            frågeformulär.ovrigt, frågeformulär.årtal, frågeformulär.fragorid,frågeformulär.invtyp,DataSetParser.getInvNamn(frågeformulär.invtyp));
                }
            }
            catch (Exception ex)
            {
#if !DEBUG
     MessageBox.Show("Misslyckades med att läsa in data", "Läsa data misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private static void SättKomboboxSvar(DataGridViewComboBoxColumn aKolumn, string aFleraSvar)
        {
            try
            {
                if (aFleraSvar == null || !aKolumn.Visible) return;

                aKolumn.Items.Clear();
                var flerasvar = aFleraSvar.Split('/');

                foreach (var svar in flerasvar)
                {
                    aKolumn.Items.Add(svar);
                }
            }
            catch (Exception ex)
            {
#if !DEBUG
     MessageBox.Show("Misslyckades med att fylla svar i komboboxes", "Lägga in svar misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void SättKomboboxes(string aFleraSvar1, string aFleraSvar2, string aFleraSvar3, string aFleraSvar4,
                                    string aFleraSvar5, string aFleraSvar6
                                    , string aFleraSvar7, string aFleraSvar8, string aFleraSvar9, string aFleraSvar10,
                                    string aFleraSvar11, string aFleraSvar12, string aFleraSvar13
                                    , string aFleraSvar14, string aFleraSvar15, string aFleraSvar16, string aFleraSvar17,
                                    string aFleraSvar18, string aFleraSvar19, string aFleraSvar20
                                    , string aFleraSvar21)
        {
            SättKomboboxSvar(fraga1DataGridViewTextBoxColumn, aFleraSvar1);
            SättKomboboxSvar(fraga2DataGridViewTextBoxColumn, aFleraSvar2);
            SättKomboboxSvar(fraga3DataGridViewTextBoxColumn, aFleraSvar3);
            SättKomboboxSvar(fraga4DataGridViewTextBoxColumn, aFleraSvar4);
            SättKomboboxSvar(fraga5DataGridViewTextBoxColumn, aFleraSvar5);
            SättKomboboxSvar(fraga6DataGridViewTextBoxColumn, aFleraSvar6);
            SättKomboboxSvar(fraga7DataGridViewTextBoxColumn, aFleraSvar7);
            SättKomboboxSvar(fraga8DataGridViewTextBoxColumn, aFleraSvar8);
            SättKomboboxSvar(fraga9DataGridViewTextBoxColumn, aFleraSvar9);
            SättKomboboxSvar(fraga10DataGridViewTextBoxColumn, aFleraSvar10);
            SättKomboboxSvar(fraga11DataGridViewTextBoxColumn, aFleraSvar11);
            SättKomboboxSvar(fraga12DataGridViewTextBoxColumn, aFleraSvar12);
            SättKomboboxSvar(fraga13DataGridViewTextBoxColumn, aFleraSvar13);
            SättKomboboxSvar(fraga14DataGridViewTextBoxColumn, aFleraSvar14);
            SättKomboboxSvar(fraga15DataGridViewTextBoxColumn, aFleraSvar15);
            SättKomboboxSvar(fraga16DataGridViewTextBoxColumn, aFleraSvar16);
            SättKomboboxSvar(fraga17DataGridViewTextBoxColumn, aFleraSvar17);
            SättKomboboxSvar(fraga18DataGridViewTextBoxColumn, aFleraSvar18);
            SättKomboboxSvar(fraga19DataGridViewTextBoxColumn, aFleraSvar19);
            SättKomboboxSvar(fraga20DataGridViewTextBoxColumn, aFleraSvar20);
            SättKomboboxSvar(fraga21DataGridViewTextBoxColumn, aFleraSvar21);
        }

        private void FormatRapportTyp(DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                if (dataGridViewFrågor == null || e.ColumnIndex < 0 || e.ColumnIndex >= dataGridViewFrågor.Columns.Count)
                    return;

                if (dataGridViewFrågor.Columns[e.ColumnIndex].DataPropertyName != "RapportTyp") return;

                if (e.Value == null || e.Value.ToString().Trim().Equals(string.Empty)) return;

                var stringValue = e.Value.ToString();
                var rapportTyp = DataHelper.GetRapportTyp(stringValue);
                var row = dataGridViewFrågor.Rows[e.RowIndex];
                switch (rapportTyp)
                {
                    case (int)RapportTyp.Markberedning:
                        e.CellStyle.BackColor = Color.LightBlue;
                        SetKolumnStatus(true, true, true, true, true, true, true, false, false, false, false, row);
                        e.FormattingApplied = true;
                        break;
                    case (int)RapportTyp.Plantering:
                        e.CellStyle.BackColor = Color.LightGreen;
                        SetKolumnStatus(true, true, false, false, false, false, false, false, false, false, false, row);
                        e.FormattingApplied = true;
                        break;
                    case (int)RapportTyp.Gallring:
                        e.CellStyle.BackColor = Color.LightSalmon;
                        SetKolumnStatus(true, true, true, true, true, true, true, true, true, false, false, row);
                        e.FormattingApplied = true;
                        break;
                    case (int)RapportTyp.Röjning:
                        e.CellStyle.BackColor = Color.LightCyan;
                        SetKolumnStatus(true, true, true, true, true, true, false, false, false, false, false, row);
                        e.FormattingApplied = true;
                        break;
                    case (int)RapportTyp.Biobränsle:
                        e.CellStyle.BackColor = Color.LightCoral;
                        SetKolumnStatus(true, true, true, true, true, true, true, true, true, true, false, row);
                        e.FormattingApplied = true;
                        break;
                    case (int)RapportTyp.Slutavverkning:
                        e.CellStyle.BackColor = Color.LightGray;
                        SetKolumnStatus(true, true, true, true, true, true, true, true, true, true, true, row);
                        e.FormattingApplied = true;
                        break;
                }
            }
            catch (Exception ex)
            {
#if !DEBUG
     MessageBox.Show("Misslyckades med att formattera rapporttypen", "Formattera rapporttyp misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void buttonUppdateraData_Click(object sender, EventArgs e)
        {
            StatusEventRefresh(this, null);
        }

        private void buttonGåTillRapportData_Click(object sender, EventArgs e)
        {
            GåTillRapportData();
        }

        private void buttonTaBortData_Click(object sender, EventArgs e)
        {
            TaBortKORUSFråga();
        }

        private void dataGridViewFrågor_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            TaBortKORUSFråga();
            //avbryt alltid delete av raden i GUI, det sköts istället från ett event när delete lyckades i databasen.
            e.Cancel = true;
        }

        private void dataGridViewFrågor_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if (!((e.Exception) is FormatException)) return;
            var view = (DataGridView)sender;
            view.Rows[e.RowIndex].ErrorText = Resources.EndastPositiva;
            view.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = Resources.EndastPositiva;

            e.ThrowException = false;
        }

        private void dataGridViewFrågor_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            dataGridViewFrågor.Rows[e.RowIndex].ErrorText = "";
            dataGridViewFrågor.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "";
        }

        private void buttonLagraÄndratData_Click(object sender, EventArgs e)
        {
            LagraÄndradeKORUSFrågor();
        }

        private void buttonFiltreraData_Click(object sender, EventArgs e)
        {
            var filterDialog = new FormFiltreraFrågor();
            filterDialog.InitData(mFrågor);
            if (filterDialog.ShowDialog(this) == DialogResult.OK)
            {
                UppdateraDataSet();
            }
        }

        private void FormatEntreprenör(DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                if (dataGridViewFrågor == null || e.ColumnIndex < 0 || e.ColumnIndex >= dataGridViewFrågor.Columns.Count)
                    return;

                if (dataGridViewFrågor.Columns[e.ColumnIndex].DataPropertyName != "Entreprenor") return;

                if (e.Value == null || e.Value.ToString().Trim().Equals(string.Empty)) return;

                var stringValue = e.Value.ToString();
                var regionid =
                  dataGridViewFrågor.Rows[e.RowIndex].Cells[regionIdDataGridViewTextBoxColumn.Name].Value.ToString();
                e.Value = regionid + "-" + stringValue;
            }
            catch (Exception ex)
            {
#if !DEBUG
     MessageBox.Show("Misslyckades med att formattera entreprenör namn", "Formatera entreprenör misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void dataGridViewFrågor_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            FormatRapportTyp(e);
            FormatEntreprenör(e);
        }

        private static void SättCellStatus(DataGridViewRow aRow, string aKolumnNamn, bool aReadOnly)
        {
            if (!aRow.Cells[aKolumnNamn].Visible) return;
            aRow.Cells[aKolumnNamn].ReadOnly = aReadOnly;
            aRow.Cells[aKolumnNamn].Style.BackColor = aReadOnly ? Color.LemonChiffon : Color.White;
            aRow.Cells[aKolumnNamn].Style.ForeColor = aReadOnly ? Color.LemonChiffon : Color.Black;
            if (!aReadOnly) return;
            aRow.Cells[aKolumnNamn].Style.SelectionForeColor = Color.LemonChiffon;
            aRow.Cells[aKolumnNamn].Style.SelectionBackColor = Color.LemonChiffon;
        }

        private void SetKolumnStatus(bool aFråga1, bool aFråga2, bool aFråga3, bool aFråga4, bool aFråga5, bool aFråga6,
                                     bool aFråga7, bool aFråga8, bool aFråga9, bool aBiobränsle, bool aSlutavverking,
                                     DataGridViewRow aRow)
        {
            if (aRow != null)
            {
                SättCellStatus(aRow, fraga1DataGridViewTextBoxColumn.Name, !aFråga1);
                SättCellStatus(aRow, fraga2DataGridViewTextBoxColumn.Name, !aFråga2);
                SättCellStatus(aRow, fraga3DataGridViewTextBoxColumn.Name, !aFråga3);
                SättCellStatus(aRow, fraga4DataGridViewTextBoxColumn.Name, !aFråga4);
                SättCellStatus(aRow, fraga5DataGridViewTextBoxColumn.Name, !aFråga5);
                SättCellStatus(aRow, fraga6DataGridViewTextBoxColumn.Name, !aFråga6);
                SättCellStatus(aRow, fraga7DataGridViewTextBoxColumn.Name, !aFråga7);
                SättCellStatus(aRow, fraga8DataGridViewTextBoxColumn.Name, !aFråga8);
                SättCellStatus(aRow, fraga9DataGridViewTextBoxColumn.Name, !aFråga9);
                SättCellStatus(aRow, fraga10DataGridViewTextBoxColumn.Name, !aBiobränsle);
                SättCellStatus(aRow, fraga11DataGridViewTextBoxColumn.Name, !aBiobränsle);
                SättCellStatus(aRow, fraga12DataGridViewTextBoxColumn.Name, !aBiobränsle);
                SättCellStatus(aRow, fraga13DataGridViewTextBoxColumn.Name, !aBiobränsle);
                SättCellStatus(aRow, fraga14DataGridViewTextBoxColumn.Name, !aBiobränsle);
                SättCellStatus(aRow, fraga15DataGridViewTextBoxColumn.Name, !aBiobränsle);
                SättCellStatus(aRow, fraga16DataGridViewTextBoxColumn.Name, !aBiobränsle);
                SättCellStatus(aRow, fraga17DataGridViewTextBoxColumn.Name, !aBiobränsle);
                SättCellStatus(aRow, fraga18DataGridViewTextBoxColumn.Name, !aBiobränsle);
                SättCellStatus(aRow, fraga19DataGridViewTextBoxColumn.Name, !aBiobränsle);
                SättCellStatus(aRow, fraga20DataGridViewTextBoxColumn.Name, !aSlutavverking);
                SättCellStatus(aRow, fraga21DataGridViewTextBoxColumn.Name, !aSlutavverking);
            }
            else
            {
                fraga1DataGridViewTextBoxColumn.Visible = aFråga1;
                fraga2DataGridViewTextBoxColumn.Visible = aFråga2;
                fraga3DataGridViewTextBoxColumn.Visible = aFråga3;
                fraga4DataGridViewTextBoxColumn.Visible = aFråga4;
                fraga5DataGridViewTextBoxColumn.Visible = aFråga5;
                fraga6DataGridViewTextBoxColumn.Visible = aFråga6;
                fraga7DataGridViewTextBoxColumn.Visible = aFråga7;
                fraga8DataGridViewTextBoxColumn.Visible = aFråga8;
                fraga9DataGridViewTextBoxColumn.Visible = aFråga9;
                fraga10DataGridViewTextBoxColumn.Visible = aBiobränsle;
                fraga11DataGridViewTextBoxColumn.Visible = aBiobränsle;
                fraga12DataGridViewTextBoxColumn.Visible = aBiobränsle;
                fraga13DataGridViewTextBoxColumn.Visible = aBiobränsle;
                fraga14DataGridViewTextBoxColumn.Visible = aBiobränsle;
                fraga15DataGridViewTextBoxColumn.Visible = aBiobränsle;
                fraga16DataGridViewTextBoxColumn.Visible = aBiobränsle;
                fraga17DataGridViewTextBoxColumn.Visible = aBiobränsle;
                fraga18DataGridViewTextBoxColumn.Visible = aBiobränsle;
                fraga19DataGridViewTextBoxColumn.Visible = aBiobränsle;
                fraga20DataGridViewTextBoxColumn.Visible = aSlutavverking;
                fraga21DataGridViewTextBoxColumn.Visible = aSlutavverking;
            }
        }

        private void StällInToolTipFörFrågorBeroendePåRapportTyp()
        {
            switch (Settings.Default.ValdTypFragor)
            {
                case (int)RapportTyp.Gallring:
                    fraga1DataGridViewTextBoxColumn.ToolTipText = Resources.GallringFraga1;
                    fraga2DataGridViewTextBoxColumn.ToolTipText = Resources.GallringFraga2;
                    fraga3DataGridViewTextBoxColumn.ToolTipText = Resources.GallringFraga3;
                    fraga4DataGridViewTextBoxColumn.ToolTipText = Resources.GallringFraga4;
                    fraga5DataGridViewTextBoxColumn.ToolTipText = Resources.GallringFraga5;
                    fraga6DataGridViewTextBoxColumn.ToolTipText = Resources.GallringFraga6;
                    fraga7DataGridViewTextBoxColumn.ToolTipText = Resources.GallringFraga7;
                    fraga8DataGridViewTextBoxColumn.ToolTipText = Resources.GallringFraga8;
                    fraga9DataGridViewTextBoxColumn.ToolTipText = Resources.GallringFraga9;
                    fraga10DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga11DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga12DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga13DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga14DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga15DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga16DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga17DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga18DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga19DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga20DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga21DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    break;
                case (int)RapportTyp.Markberedning:
                    fraga1DataGridViewTextBoxColumn.ToolTipText = Resources.MarkberedningFraga1;
                    fraga2DataGridViewTextBoxColumn.ToolTipText = Resources.MarkberedningFraga2;
                    fraga3DataGridViewTextBoxColumn.ToolTipText = Resources.MarkberedningFraga3;
                    fraga4DataGridViewTextBoxColumn.ToolTipText = Resources.MarkberedningFraga4;
                    fraga5DataGridViewTextBoxColumn.ToolTipText = Resources.MarkberedningFraga5;
                    fraga6DataGridViewTextBoxColumn.ToolTipText = Resources.MarkberedningFraga6;
                    fraga7DataGridViewTextBoxColumn.ToolTipText = Resources.MarkberedningFraga7;
                    fraga8DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga9DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga10DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga11DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga12DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga13DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga14DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga15DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga16DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga17DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga18DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga19DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga20DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga21DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    break;
                case (int)RapportTyp.Plantering:
                    fraga1DataGridViewTextBoxColumn.ToolTipText = Resources.PlanteringFraga1;
                    fraga2DataGridViewTextBoxColumn.ToolTipText = Resources.PlanteringFraga2;
                    fraga3DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga4DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga5DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga6DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga7DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga8DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga9DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga10DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga11DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga12DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga13DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga14DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga15DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga16DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga17DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga18DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga19DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga20DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga21DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    break;
                case (int)RapportTyp.Röjning:
                    fraga1DataGridViewTextBoxColumn.ToolTipText = Resources.RojningFraga1;
                    fraga2DataGridViewTextBoxColumn.ToolTipText = Resources.RojningFraga2;
                    fraga3DataGridViewTextBoxColumn.ToolTipText = Resources.RojningFraga3;
                    fraga4DataGridViewTextBoxColumn.ToolTipText = Resources.RojningFraga4;
                    fraga5DataGridViewTextBoxColumn.ToolTipText = Resources.RojningFraga5;
                    fraga6DataGridViewTextBoxColumn.ToolTipText = Resources.RojningFraga6;
                    fraga7DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga8DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga9DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga10DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga11DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga12DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga13DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga14DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga15DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga16DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga17DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga18DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga19DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga20DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga21DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    break;
                case (int)RapportTyp.Biobränsle:
                    fraga1DataGridViewTextBoxColumn.ToolTipText = Resources.BiobransleFraga1;
                    fraga2DataGridViewTextBoxColumn.ToolTipText = Resources.BiobransleFraga2;
                    fraga3DataGridViewTextBoxColumn.ToolTipText = Resources.BiobransleFraga3;
                    fraga4DataGridViewTextBoxColumn.ToolTipText = Resources.BiobransleFraga4;
                    fraga5DataGridViewTextBoxColumn.ToolTipText = Resources.BiobransleFraga5;
                    fraga6DataGridViewTextBoxColumn.ToolTipText = Resources.BiobransleFraga6;
                    fraga7DataGridViewTextBoxColumn.ToolTipText = Resources.BiobransleFraga7;
                    fraga8DataGridViewTextBoxColumn.ToolTipText = Resources.BiobransleFraga8;
                    fraga9DataGridViewTextBoxColumn.ToolTipText = Resources.BiobransleFraga9;
                    fraga10DataGridViewTextBoxColumn.ToolTipText = Resources.BiobransleFraga10;
                    fraga11DataGridViewTextBoxColumn.ToolTipText = Resources.BiobransleFraga11;
                    fraga12DataGridViewTextBoxColumn.ToolTipText = Resources.BiobransleFraga12;
                    fraga13DataGridViewTextBoxColumn.ToolTipText = Resources.BiobransleFraga13;
                    fraga14DataGridViewTextBoxColumn.ToolTipText = Resources.BiobransleFraga14;
                    fraga15DataGridViewTextBoxColumn.ToolTipText = Resources.BiobransleFraga15;
                    fraga16DataGridViewTextBoxColumn.ToolTipText = Resources.BiobransleFraga16;
                    fraga17DataGridViewTextBoxColumn.ToolTipText = Resources.BiobransleFraga17;
                    fraga18DataGridViewTextBoxColumn.ToolTipText = Resources.BiobransleFraga18;
                    fraga19DataGridViewTextBoxColumn.ToolTipText = Resources.BiobransleFraga19;
                    fraga20DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    fraga21DataGridViewTextBoxColumn.ToolTipText = string.Empty;
                    break;
                case (int)RapportTyp.Slutavverkning:
                    fraga1DataGridViewTextBoxColumn.ToolTipText = Resources.SlutavverkningFraga1a;
                    fraga2DataGridViewTextBoxColumn.ToolTipText = Resources.SlutavverkningFraga1b;
                    fraga3DataGridViewTextBoxColumn.ToolTipText = Resources.SlutavverkningFraga2a;
                    fraga4DataGridViewTextBoxColumn.ToolTipText = Resources.SlutavverkningFraga2b;
                    fraga5DataGridViewTextBoxColumn.ToolTipText = Resources.SlutavverkningFraga2c;
                    fraga6DataGridViewTextBoxColumn.ToolTipText = Resources.SlutavverkningFraga2d;
                    fraga7DataGridViewTextBoxColumn.ToolTipText = Resources.SlutavverkningFraga2e;
                    fraga8DataGridViewTextBoxColumn.ToolTipText = Resources.SlutavverkningFraga2f;
                    fraga9DataGridViewTextBoxColumn.ToolTipText = Resources.SlutavverkningFraga2g;
                    fraga10DataGridViewTextBoxColumn.ToolTipText = Resources.SlutavverkningFraga2h;
                    fraga11DataGridViewTextBoxColumn.ToolTipText = Resources.SlutavverkningFraga3;
                    fraga12DataGridViewTextBoxColumn.ToolTipText = Resources.SlutavverkningFraga4a;
                    fraga13DataGridViewTextBoxColumn.ToolTipText = Resources.SlutavverkningFraga4b;
                    fraga14DataGridViewTextBoxColumn.ToolTipText = Resources.SlutavverkningFraga5;
                    fraga15DataGridViewTextBoxColumn.ToolTipText = Resources.SlutavverkningFraga6;
                    fraga16DataGridViewTextBoxColumn.ToolTipText = Resources.SlutavverkningFraga7;
                    fraga17DataGridViewTextBoxColumn.ToolTipText = Resources.SlutavverkningFraga8;
                    fraga18DataGridViewTextBoxColumn.ToolTipText = Resources.SlutavverkningFraga9;
                    fraga19DataGridViewTextBoxColumn.ToolTipText = Resources.SlutavverkningFraga10;
                    fraga20DataGridViewTextBoxColumn.ToolTipText = Resources.SlutavverkningFraga11;
                    fraga21DataGridViewTextBoxColumn.ToolTipText = Resources.SlutavverkningFraga12;
                    break;
                default:
                    fraga1DataGridViewTextBoxColumn.ToolTipText = Resources.AllaFraga;
                    fraga2DataGridViewTextBoxColumn.ToolTipText = Resources.AllaFraga;
                    fraga3DataGridViewTextBoxColumn.ToolTipText = Resources.AllaFraga;
                    fraga4DataGridViewTextBoxColumn.ToolTipText = Resources.AllaFraga;
                    fraga5DataGridViewTextBoxColumn.ToolTipText = Resources.AllaFraga;
                    fraga6DataGridViewTextBoxColumn.ToolTipText = Resources.AllaFraga;
                    fraga7DataGridViewTextBoxColumn.ToolTipText = Resources.AllaFraga;
                    fraga8DataGridViewTextBoxColumn.ToolTipText = Resources.AllaFraga;
                    fraga9DataGridViewTextBoxColumn.ToolTipText = Resources.AllaFraga;
                    fraga10DataGridViewTextBoxColumn.ToolTipText = Resources.AllaFraga;
                    fraga11DataGridViewTextBoxColumn.ToolTipText = Resources.AllaFraga;
                    fraga12DataGridViewTextBoxColumn.ToolTipText = Resources.AllaFraga;
                    fraga13DataGridViewTextBoxColumn.ToolTipText = Resources.AllaFraga;
                    fraga14DataGridViewTextBoxColumn.ToolTipText = Resources.AllaFraga;
                    fraga15DataGridViewTextBoxColumn.ToolTipText = Resources.AllaFraga;
                    fraga16DataGridViewTextBoxColumn.ToolTipText = Resources.AllaFraga;
                    fraga17DataGridViewTextBoxColumn.ToolTipText = Resources.AllaFraga;
                    fraga18DataGridViewTextBoxColumn.ToolTipText = Resources.AllaFraga;
                    fraga19DataGridViewTextBoxColumn.ToolTipText = Resources.AllaFraga;
                    break;
            }
        }

        private void radioButtonMarkberedning_CheckedChanged(object sender, EventArgs e)
        {
            var rButton = sender as RadioButton;
            if (rButton == null) return;
            if (!rButton.Checked) return;
            //Aktiv, filtrera tabeller.
            var dataView = dataSetFrågor.Tables["Frågor"].DefaultView;
            dataView.RowFilter = "RapportTyp = '" + RapportTyp.Markberedning + "'";
            dataGridViewFrågor.DataSource = dataView;
            Settings.Default.ValdTypFragor = (int)RapportTyp.Markberedning;
            buttonFrågor.Enabled = true;
            SetKolumnStatus(true, true, true, true, true, true, true, false, false, false, false, null);
            SättKomboboxes("Ja/Nej", "Ja/Nej", "Ja/Nej", "Ja/Nej", "Ja/Nej", "Ja/Nej", "Ja/Nej", null, null, null, null,
                           null, null, null, null, null, null, null, null, null, null);
            StällInToolTipFörFrågorBeroendePåRapportTyp();
        }

        private void radioButtonPlantering_CheckedChanged(object sender, EventArgs e)
        {
            var rButton = sender as RadioButton;
            if (rButton == null) return;
            if (!rButton.Checked) return;
            //Aktiv, filtrera tabeller.
            var dataView = dataSetFrågor.Tables["Frågor"].DefaultView;
            dataView.RowFilter = "RapportTyp = '" + RapportTyp.Plantering + "'";
            dataGridViewFrågor.DataSource = dataView;
            Settings.Default.ValdTypFragor = (int)RapportTyp.Plantering;
            buttonFrågor.Enabled = true;
            SetKolumnStatus(true, true, false, false, false, false, false, false, false, false, false, null);
            SättKomboboxes("Ja/Nej", "Ja/Nej", null, null, null, null, null, null, null, null, null, null, null, null,
                           null, null, null, null, null, null, null);
            StällInToolTipFörFrågorBeroendePåRapportTyp();
        }

        private void radioButtonRöjning_CheckedChanged(object sender, EventArgs e)
        {
            var rButton = sender as RadioButton;
            if (rButton == null) return;
            if (!rButton.Checked) return;
            //Aktiv, filtrera tabeller.
            var dataView = dataSetFrågor.Tables["Frågor"].DefaultView;
            dataView.RowFilter = "RapportTyp = '" + RapportTyp.Röjning + "'";
            dataGridViewFrågor.DataSource = dataView;
            Settings.Default.ValdTypFragor = (int)RapportTyp.Röjning;
            buttonFrågor.Enabled = true;
            SetKolumnStatus(true, true, true, true, true, true, false, false, false, false, false, null);
            SättKomboboxes("Ja/Nej", "Ja/Nej/Ej aktuellt", "Ja/Nej/Ej aktuellt", "Ja/Nej/Ej aktuellt",
                           "Ja/Nej/Ej aktuellt", "Ja/Nej/Ej aktuellt", null, null, null, null, null, null, null, null,
                           null, null, null, null, null, null, null);
            StällInToolTipFörFrågorBeroendePåRapportTyp();
        }

        private void radioButtonGallring_CheckedChanged(object sender, EventArgs e)
        {
            var rButton = sender as RadioButton;
            if (rButton == null) return;
            if (!rButton.Checked) return;
            //Aktiv, filtrera tabeller.
            var dataView = dataSetFrågor.Tables["Frågor"].DefaultView;
            dataView.RowFilter = "RapportTyp = '" + RapportTyp.Gallring + "'";
            dataGridViewFrågor.DataSource = dataView;
            Settings.Default.ValdTypFragor = (int)RapportTyp.Gallring;
            buttonFrågor.Enabled = true;
            SetKolumnStatus(true, true, true, true, true, true, true, true, true, false, false, null);
            SättKomboboxes("Ja/Nej", "Ja/Nej/Ej aktuellt", "Ja/Nej/Ej aktuellt", "Ja/Nej", "Ja/Nej/Ej aktuellt", "Ja/Nej",
                           "Ja/Nej/Ej aktuellt", "Ja/Nej", "Ja/Nej", null, null, null, null, null, null, null, null, null,
                           null, null, null);
            StällInToolTipFörFrågorBeroendePåRapportTyp();
        }

        private void radioButtonBiobränsle_CheckedChanged(object sender, EventArgs e)
        {
            var rButton = sender as RadioButton;
            if (rButton == null) return;
            if (!rButton.Checked) return;
            //Aktiv, filtrera tabeller.
            var dataView = dataSetFrågor.Tables["Frågor"].DefaultView;
            dataView.RowFilter = "RapportTyp = '" + RapportTyp.Biobränsle + "'";
            dataGridViewFrågor.DataSource = dataView;
            Settings.Default.ValdTypFragor = (int)RapportTyp.Biobränsle;
            buttonFrågor.Enabled = true;
            SetKolumnStatus(true, true, true, true, true, true, true, true, true, true, false, null);
            SättKomboboxes("Ja/Nej", "Ja/Nej", "Bra/Dåligt", "Bra/Dåligt", "Bra/Dåligt", "Bra/Dåligt/Ej aktuellt",
                           "Bra/Dåligt", "Bra/Dåligt", "Ja/Nej", "Ja/Nej", "Ja/Nej", "Ja/Nej", "Ja/Nej", "Ja/Nej",
                           "Ja/Nej", "Ja/Nej", "Ja/Nej", "Ja/Nej", "Ja/Nej", null, null);
            StällInToolTipFörFrågorBeroendePåRapportTyp();
        }

        private void radioButtonSlutavverkning_CheckedChanged(object sender, EventArgs e)
        {
            var rButton = sender as RadioButton;
            if (rButton == null) return;
            if (!rButton.Checked) return;
            //Aktiv, filtrera tabeller.
            var dataView = dataSetFrågor.Tables["Frågor"].DefaultView;
            dataView.RowFilter = "RapportTyp = '" + RapportTyp.Slutavverkning + "'";
            dataGridViewFrågor.DataSource = dataView;
            Settings.Default.ValdTypFragor = (int)RapportTyp.Slutavverkning;
            buttonFrågor.Enabled = true;
            SetKolumnStatus(true, true, true, true, true, true, true, true, true, true, true, null);
            SättKomboboxes("Ja/Nej", "Ja/Nej", "Ja/Nej/Ej aktuellt", "Ja/Nej", "Ja/Nej", "Ja/Nej/Ej aktuellt", "Ja/Nej",
                           "Ja/Nej", "Ja/Nej", "Ja/Nej", "Ja/Nej/Ej aktuellt", "Ja/Nej", "Ja/Nej", "Ja/Nej", "Ja/Nej",
                           "Ja/Nej", "Ja/Nej/Ej aktuellt", "Ja/Nej", "Ja/Nej", "Ja/Nej", "Ja/Nej");
            StällInToolTipFörFrågorBeroendePåRapportTyp();
        }

        private void FormListaFrågor_Load(object sender, EventArgs e)
        {
            switch (Settings.Default.ValdTypFragor)
            {
                case (int)RapportTyp.Biobränsle:
                    radioButtonBiobränsle.Checked = true;
                    break;
                case (int)RapportTyp.Gallring:
                    radioButtonGallring.Checked = true;
                    break;
                case (int)RapportTyp.Markberedning:
                    radioButtonMarkberedning.Checked = true;
                    break;
                case (int)RapportTyp.Plantering:
                    radioButtonPlantering.Checked = true;
                    break;
                case (int)RapportTyp.Röjning:
                    radioButtonRöjning.Checked = true;
                    break;
                case (int)RapportTyp.Slutavverkning:
                    radioButtonSlutavverkning.Checked = true;
                    break;
                default:
                    radioButtonMarkberedning.Checked = true;
                    break;
            }
        }

        private void buttonFrågor_Click(object sender, EventArgs e)
        {
            var frågor = new FormVisaFrågor();
            frågor.ShowDialog();
        }
    }
}