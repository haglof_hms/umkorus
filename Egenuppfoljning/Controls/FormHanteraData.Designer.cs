﻿namespace Egenuppfoljning.Controls
{
  partial class FormHanteraData
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.components = new System.ComponentModel.Container();
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormHanteraData));
        this.groupBoxRapportInfo = new System.Windows.Forms.GroupBox();
        this.buttonRapport = new System.Windows.Forms.Button();
        this.imageList32 = new System.Windows.Forms.ImageList(this.components);
        this.labelDatainsamlingsmetod = new System.Windows.Forms.Label();
        this.labelDatabasPosition = new System.Windows.Forms.Label();
        this.groupBoxData = new System.Windows.Forms.GroupBox();
        this.labelStickvägssystem = new System.Windows.Forms.Label();
        this.labelVägkant = new System.Windows.Forms.Label();
        this.labelHygge = new System.Windows.Forms.Label();
        this.labelAvstånd = new System.Windows.Forms.Label();
        this.labelBedömdVolym = new System.Windows.Forms.Label();
        this.labelÅker = new System.Windows.Forms.Label();
        this.labelAntalVältor = new System.Windows.Forms.Label();
        this.labelAreal = new System.Windows.Forms.Label();
        this.labelTomDatabas = new System.Windows.Forms.Label();
        this.labelGISAreal = new System.Windows.Forms.Label();
        this.labelStickvägsavstånd = new System.Windows.Forms.Label();
        this.labelProvytestorlek = new System.Windows.Forms.Label();
        this.labelGISSträcka = new System.Windows.Forms.Label();
        this.labelMålgrundyta = new System.Windows.Forms.Label();
        this.labelUrsprung = new System.Windows.Forms.Label();
        this.buttonUppdateraData = new System.Windows.Forms.Button();
        this.buttonExportera = new System.Windows.Forms.Button();
        this.groupBox2 = new System.Windows.Forms.GroupBox();
        this.richTextBoxKommentar = new System.Windows.Forms.RichTextBox();
        this.labelTitelKommentar = new System.Windows.Forms.Label();
        this.labelTotalaAntaletFrågor = new System.Windows.Forms.Label();
        this.buttonTaBortData = new System.Windows.Forms.Button();
        this.labelDatum = new System.Windows.Forms.Label();
        this.labelStandort = new System.Windows.Forms.Label();
        this.buttonRedigeraData = new System.Windows.Forms.Button();
        this.labelTotalaAntaletYtor = new System.Windows.Forms.Label();
        this.labelEgenuppföljningstyp = new System.Windows.Forms.Label();
        this.groupBox4 = new System.Windows.Forms.GroupBox();
        this.labelInvtypID = new System.Windows.Forms.Label();
        this.labelEntreprenörNamn = new System.Windows.Forms.Label();
        this.labelEntreprenörNr = new System.Windows.Forms.Label();
        this.labelTraktNamn = new System.Windows.Forms.Label();
        this.labelDistriktNamn = new System.Windows.Forms.Label();
        this.labelRegionNamn = new System.Windows.Forms.Label();
        this.labelTraktnr = new System.Windows.Forms.Label();
        this.labelDistriktID = new System.Windows.Forms.Label();
        this.labelRegionID = new System.Windows.Forms.Label();
        this.labelMarkberedningsMetod = new System.Windows.Forms.Label();
        this.contextMenuStripExportera = new System.Windows.Forms.ContextMenuStrip(this.components);
        this.toolStripMenuItemExcel2007 = new System.Windows.Forms.ToolStripMenuItem();
        this.toolStripMenuItemPdf = new System.Windows.Forms.ToolStripMenuItem();
        this.labelAtgAreal = new System.Windows.Forms.Label();
        this.groupBoxRapportInfo.SuspendLayout();
        this.groupBoxData.SuspendLayout();
        this.groupBox2.SuspendLayout();
        this.groupBox4.SuspendLayout();
        this.contextMenuStripExportera.SuspendLayout();
        this.SuspendLayout();
        // 
        // groupBoxRapportInfo
        // 
        this.groupBoxRapportInfo.Controls.Add(this.buttonRapport);
        this.groupBoxRapportInfo.Controls.Add(this.labelDatainsamlingsmetod);
        this.groupBoxRapportInfo.Controls.Add(this.labelDatabasPosition);
        this.groupBoxRapportInfo.Controls.Add(this.groupBoxData);
        this.groupBoxRapportInfo.Controls.Add(this.buttonUppdateraData);
        this.groupBoxRapportInfo.Controls.Add(this.buttonExportera);
        this.groupBoxRapportInfo.Controls.Add(this.groupBox2);
        this.groupBoxRapportInfo.Controls.Add(this.labelTotalaAntaletFrågor);
        this.groupBoxRapportInfo.Controls.Add(this.buttonTaBortData);
        this.groupBoxRapportInfo.Controls.Add(this.labelDatum);
        this.groupBoxRapportInfo.Controls.Add(this.labelStandort);
        this.groupBoxRapportInfo.Controls.Add(this.buttonRedigeraData);
        this.groupBoxRapportInfo.Controls.Add(this.labelTotalaAntaletYtor);
        this.groupBoxRapportInfo.Controls.Add(this.labelEgenuppföljningstyp);
        this.groupBoxRapportInfo.Controls.Add(this.groupBox4);
        this.groupBoxRapportInfo.Controls.Add(this.labelMarkberedningsMetod);
        this.groupBoxRapportInfo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.groupBoxRapportInfo.Location = new System.Drawing.Point(9, 6);
        this.groupBoxRapportInfo.Name = "groupBoxRapportInfo";
        this.groupBoxRapportInfo.Size = new System.Drawing.Size(970, 561);
        this.groupBoxRapportInfo.TabIndex = 0;
        this.groupBoxRapportInfo.TabStop = false;
        this.groupBoxRapportInfo.Text = "LAGRAT KORUS DATA";
        // 
        // buttonRapport
        // 
        this.buttonRapport.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.buttonRapport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonRapport.ImageIndex = 0;
        this.buttonRapport.ImageList = this.imageList32;
        this.buttonRapport.Location = new System.Drawing.Point(408, 515);
        this.buttonRapport.Name = "buttonRapport";
        this.buttonRapport.Size = new System.Drawing.Size(160, 40);
        this.buttonRapport.TabIndex = 15;
        this.buttonRapport.Text = "Rapport";
        this.buttonRapport.UseVisualStyleBackColor = true;
        this.buttonRapport.Click += new System.EventHandler(this.buttonRapport_Click);
        // 
        // imageList32
        // 
        this.imageList32.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList32.ImageStream")));
        this.imageList32.TransparentColor = System.Drawing.SystemColors.ActiveCaption;
        this.imageList32.Images.SetKeyName(0, "Förhandsgranska.ico");
        this.imageList32.Images.SetKeyName(1, "Redigera.ico");
        this.imageList32.Images.SetKeyName(2, "TabortData.ico");
        this.imageList32.Images.SetKeyName(3, "Uppdatera.ico");
        this.imageList32.Images.SetKeyName(4, "Exportera.ico");
        // 
        // labelDatainsamlingsmetod
        // 
        this.labelDatainsamlingsmetod.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelDatainsamlingsmetod.ForeColor = System.Drawing.Color.Black;
        this.labelDatainsamlingsmetod.Location = new System.Drawing.Point(335, 110);
        this.labelDatainsamlingsmetod.Name = "labelDatainsamlingsmetod";
        this.labelDatainsamlingsmetod.Size = new System.Drawing.Size(290, 26);
        this.labelDatainsamlingsmetod.TabIndex = 14;
        this.labelDatainsamlingsmetod.Text = "_";
        // 
        // labelDatabasPosition
        // 
        this.labelDatabasPosition.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelDatabasPosition.ForeColor = System.Drawing.Color.Black;
        this.labelDatabasPosition.Location = new System.Drawing.Point(25, 70);
        this.labelDatabasPosition.Name = "labelDatabasPosition";
        this.labelDatabasPosition.Size = new System.Drawing.Size(290, 26);
        this.labelDatabasPosition.TabIndex = 4;
        this.labelDatabasPosition.Text = "_";
        // 
        // groupBoxData
        // 
        this.groupBoxData.Controls.Add(this.labelStickvägssystem);
        this.groupBoxData.Controls.Add(this.labelVägkant);
        this.groupBoxData.Controls.Add(this.labelHygge);
        this.groupBoxData.Controls.Add(this.labelAvstånd);
        this.groupBoxData.Controls.Add(this.labelBedömdVolym);
        this.groupBoxData.Controls.Add(this.labelÅker);
        this.groupBoxData.Controls.Add(this.labelAntalVältor);
        this.groupBoxData.Controls.Add(this.labelAreal);
        this.groupBoxData.Controls.Add(this.labelTomDatabas);
        this.groupBoxData.Controls.Add(this.labelGISAreal);
        this.groupBoxData.Controls.Add(this.labelStickvägsavstånd);
        this.groupBoxData.Controls.Add(this.labelProvytestorlek);
        this.groupBoxData.Controls.Add(this.labelGISSträcka);
        this.groupBoxData.Controls.Add(this.labelMålgrundyta);
        this.groupBoxData.Controls.Add(this.labelUrsprung);
        this.groupBoxData.Location = new System.Drawing.Point(0, 262);
        this.groupBoxData.Name = "groupBoxData";
        this.groupBoxData.Size = new System.Drawing.Size(970, 110);
        this.groupBoxData.TabIndex = 8;
        this.groupBoxData.TabStop = false;
        // 
        // labelStickvägssystem
        // 
        this.labelStickvägssystem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelStickvägssystem.ForeColor = System.Drawing.Color.Black;
        this.labelStickvägssystem.Location = new System.Drawing.Point(715, 69);
        this.labelStickvägssystem.Name = "labelStickvägssystem";
        this.labelStickvägssystem.Size = new System.Drawing.Size(200, 26);
        this.labelStickvägssystem.TabIndex = 29;
        this.labelStickvägssystem.Text = "_";
        // 
        // labelVägkant
        // 
        this.labelVägkant.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelVägkant.ForeColor = System.Drawing.Color.Black;
        this.labelVägkant.Location = new System.Drawing.Point(715, 69);
        this.labelVägkant.Name = "labelVägkant";
        this.labelVägkant.Size = new System.Drawing.Size(200, 26);
        this.labelVägkant.TabIndex = 9;
        this.labelVägkant.Text = "_";
        // 
        // labelHygge
        // 
        this.labelHygge.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelHygge.ForeColor = System.Drawing.Color.Black;
        this.labelHygge.Location = new System.Drawing.Point(485, 69);
        this.labelHygge.Name = "labelHygge";
        this.labelHygge.Size = new System.Drawing.Size(200, 26);
        this.labelHygge.TabIndex = 7;
        this.labelHygge.Text = "_";
        // 
        // labelAvstånd
        // 
        this.labelAvstånd.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelAvstånd.ForeColor = System.Drawing.Color.Black;
        this.labelAvstånd.Location = new System.Drawing.Point(25, 70);
        this.labelAvstånd.Name = "labelAvstånd";
        this.labelAvstånd.Size = new System.Drawing.Size(200, 26);
        this.labelAvstånd.TabIndex = 4;
        this.labelAvstånd.Text = "_";
        // 
        // labelBedömdVolym
        // 
        this.labelBedömdVolym.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelBedömdVolym.ForeColor = System.Drawing.Color.Black;
        this.labelBedömdVolym.Location = new System.Drawing.Point(255, 69);
        this.labelBedömdVolym.Name = "labelBedömdVolym";
        this.labelBedömdVolym.Size = new System.Drawing.Size(200, 26);
        this.labelBedömdVolym.TabIndex = 6;
        this.labelBedömdVolym.Text = "_";
        // 
        // labelÅker
        // 
        this.labelÅker.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelÅker.ForeColor = System.Drawing.Color.Black;
        this.labelÅker.Location = new System.Drawing.Point(710, 70);
        this.labelÅker.Name = "labelÅker";
        this.labelÅker.Size = new System.Drawing.Size(200, 26);
        this.labelÅker.TabIndex = 10;
        this.labelÅker.Text = "_";
        // 
        // labelAntalVältor
        // 
        this.labelAntalVältor.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelAntalVältor.ForeColor = System.Drawing.Color.Black;
        this.labelAntalVältor.Location = new System.Drawing.Point(710, 70);
        this.labelAntalVältor.Name = "labelAntalVältor";
        this.labelAntalVältor.Size = new System.Drawing.Size(200, 26);
        this.labelAntalVältor.TabIndex = 28;
        this.labelAntalVältor.Text = "_";
        // 
        // labelAreal
        // 
        this.labelAreal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelAreal.ForeColor = System.Drawing.Color.Black;
        this.labelAreal.Location = new System.Drawing.Point(25, 70);
        this.labelAreal.Name = "labelAreal";
        this.labelAreal.Size = new System.Drawing.Size(200, 26);
        this.labelAreal.TabIndex = 5;
        this.labelAreal.Text = "_";
        // 
        // labelTomDatabas
        // 
        this.labelTomDatabas.AutoSize = true;
        this.labelTomDatabas.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelTomDatabas.ForeColor = System.Drawing.Color.Red;
        this.labelTomDatabas.Location = new System.Drawing.Point(193, 56);
        this.labelTomDatabas.Name = "labelTomDatabas";
        this.labelTomDatabas.Size = new System.Drawing.Size(425, 13);
        this.labelTomDatabas.TabIndex = 5;
        this.labelTomDatabas.Text = "Databasen är tom, välj importera KORUS rapporter för att lagra nytt data.";
        this.labelTomDatabas.Visible = false;
        // 
        // labelGISAreal
        // 
        this.labelGISAreal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelGISAreal.ForeColor = System.Drawing.Color.Black;
        this.labelGISAreal.Location = new System.Drawing.Point(715, 30);
        this.labelGISAreal.Name = "labelGISAreal";
        this.labelGISAreal.Size = new System.Drawing.Size(200, 26);
        this.labelGISAreal.TabIndex = 3;
        this.labelGISAreal.Text = "_";
        // 
        // labelStickvägsavstånd
        // 
        this.labelStickvägsavstånd.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelStickvägsavstånd.ForeColor = System.Drawing.Color.Black;
        this.labelStickvägsavstånd.Location = new System.Drawing.Point(485, 70);
        this.labelStickvägsavstånd.Name = "labelStickvägsavstånd";
        this.labelStickvägsavstånd.Size = new System.Drawing.Size(200, 26);
        this.labelStickvägsavstånd.TabIndex = 6;
        this.labelStickvägsavstånd.Text = "_";
        // 
        // labelProvytestorlek
        // 
        this.labelProvytestorlek.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelProvytestorlek.ForeColor = System.Drawing.Color.Black;
        this.labelProvytestorlek.Location = new System.Drawing.Point(255, 30);
        this.labelProvytestorlek.Name = "labelProvytestorlek";
        this.labelProvytestorlek.Size = new System.Drawing.Size(200, 26);
        this.labelProvytestorlek.TabIndex = 1;
        this.labelProvytestorlek.Text = "_";
        // 
        // labelGISSträcka
        // 
        this.labelGISSträcka.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelGISSträcka.ForeColor = System.Drawing.Color.Black;
        this.labelGISSträcka.Location = new System.Drawing.Point(485, 30);
        this.labelGISSträcka.Name = "labelGISSträcka";
        this.labelGISSträcka.Size = new System.Drawing.Size(200, 26);
        this.labelGISSträcka.TabIndex = 2;
        this.labelGISSträcka.Text = "_";
        // 
        // labelMålgrundyta
        // 
        this.labelMålgrundyta.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelMålgrundyta.ForeColor = System.Drawing.Color.Black;
        this.labelMålgrundyta.Location = new System.Drawing.Point(255, 70);
        this.labelMålgrundyta.Name = "labelMålgrundyta";
        this.labelMålgrundyta.Size = new System.Drawing.Size(200, 26);
        this.labelMålgrundyta.TabIndex = 4;
        this.labelMålgrundyta.Text = "_";
        // 
        // labelUrsprung
        // 
        this.labelUrsprung.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelUrsprung.ForeColor = System.Drawing.Color.Black;
        this.labelUrsprung.Location = new System.Drawing.Point(25, 30);
        this.labelUrsprung.Name = "labelUrsprung";
        this.labelUrsprung.Size = new System.Drawing.Size(200, 26);
        this.labelUrsprung.TabIndex = 0;
        this.labelUrsprung.Text = "_";
        // 
        // buttonUppdateraData
        // 
        this.buttonUppdateraData.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.buttonUppdateraData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonUppdateraData.ImageIndex = 3;
        this.buttonUppdateraData.ImageList = this.imageList32;
        this.buttonUppdateraData.Location = new System.Drawing.Point(14, 515);
        this.buttonUppdateraData.Name = "buttonUppdateraData";
        this.buttonUppdateraData.Size = new System.Drawing.Size(160, 40);
        this.buttonUppdateraData.TabIndex = 10;
        this.buttonUppdateraData.Text = "Uppdatera";
        this.buttonUppdateraData.UseVisualStyleBackColor = true;
        this.buttonUppdateraData.Click += new System.EventHandler(this.buttonUppdateraData_Click);
        // 
        // buttonExportera
        // 
        this.buttonExportera.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.buttonExportera.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonExportera.ImageIndex = 4;
        this.buttonExportera.ImageList = this.imageList32;
        this.buttonExportera.Location = new System.Drawing.Point(213, 515);
        this.buttonExportera.Name = "buttonExportera";
        this.buttonExportera.Size = new System.Drawing.Size(160, 40);
        this.buttonExportera.TabIndex = 11;
        this.buttonExportera.Text = "Exportera";
        this.buttonExportera.UseVisualStyleBackColor = true;
        this.buttonExportera.Click += new System.EventHandler(this.buttonExportera_Click);
        // 
        // groupBox2
        // 
        this.groupBox2.Controls.Add(this.richTextBoxKommentar);
        this.groupBox2.Controls.Add(this.labelTitelKommentar);
        this.groupBox2.Location = new System.Drawing.Point(0, 370);
        this.groupBox2.Name = "groupBox2";
        this.groupBox2.Size = new System.Drawing.Size(970, 137);
        this.groupBox2.TabIndex = 9;
        this.groupBox2.TabStop = false;
        // 
        // richTextBoxKommentar
        // 
        this.richTextBoxKommentar.Location = new System.Drawing.Point(14, 31);
        this.richTextBoxKommentar.Name = "richTextBoxKommentar";
        this.richTextBoxKommentar.ReadOnly = true;
        this.richTextBoxKommentar.Size = new System.Drawing.Size(943, 96);
        this.richTextBoxKommentar.TabIndex = 1;
        this.richTextBoxKommentar.Text = "";
        // 
        // labelTitelKommentar
        // 
        this.labelTitelKommentar.AutoSize = true;
        this.labelTitelKommentar.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelTitelKommentar.Location = new System.Drawing.Point(11, 14);
        this.labelTitelKommentar.Name = "labelTitelKommentar";
        this.labelTitelKommentar.Size = new System.Drawing.Size(89, 13);
        this.labelTitelKommentar.TabIndex = 0;
        this.labelTitelKommentar.Text = "Kommentarer:";
        // 
        // labelTotalaAntaletFrågor
        // 
        this.labelTotalaAntaletFrågor.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelTotalaAntaletFrågor.ForeColor = System.Drawing.Color.Black;
        this.labelTotalaAntaletFrågor.Location = new System.Drawing.Point(335, 30);
        this.labelTotalaAntaletFrågor.Name = "labelTotalaAntaletFrågor";
        this.labelTotalaAntaletFrågor.Size = new System.Drawing.Size(290, 26);
        this.labelTotalaAntaletFrågor.TabIndex = 1;
        this.labelTotalaAntaletFrågor.Text = "_";
        // 
        // buttonTaBortData
        // 
        this.buttonTaBortData.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.buttonTaBortData.ForeColor = System.Drawing.Color.Black;
        this.buttonTaBortData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonTaBortData.ImageIndex = 2;
        this.buttonTaBortData.ImageList = this.imageList32;
        this.buttonTaBortData.Location = new System.Drawing.Point(797, 515);
        this.buttonTaBortData.Name = "buttonTaBortData";
        this.buttonTaBortData.Size = new System.Drawing.Size(160, 40);
        this.buttonTaBortData.TabIndex = 13;
        this.buttonTaBortData.Text = "Ta bort";
        this.buttonTaBortData.UseVisualStyleBackColor = true;
        this.buttonTaBortData.Click += new System.EventHandler(this.buttonTaBortData_Click);
        // 
        // labelDatum
        // 
        this.labelDatum.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelDatum.ForeColor = System.Drawing.Color.Black;
        this.labelDatum.ImageList = this.imageList32;
        this.labelDatum.Location = new System.Drawing.Point(645, 30);
        this.labelDatum.Name = "labelDatum";
        this.labelDatum.RightToLeft = System.Windows.Forms.RightToLeft.No;
        this.labelDatum.Size = new System.Drawing.Size(150, 26);
        this.labelDatum.TabIndex = 2;
        this.labelDatum.Text = "_";
        // 
        // labelStandort
        // 
        this.labelStandort.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelStandort.ForeColor = System.Drawing.Color.Black;
        this.labelStandort.Location = new System.Drawing.Point(845, 30);
        this.labelStandort.Name = "labelStandort";
        this.labelStandort.Size = new System.Drawing.Size(111, 26);
        this.labelStandort.TabIndex = 3;
        this.labelStandort.Text = "_";
        // 
        // buttonRedigeraData
        // 
        this.buttonRedigeraData.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.buttonRedigeraData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonRedigeraData.ImageIndex = 1;
        this.buttonRedigeraData.ImageList = this.imageList32;
        this.buttonRedigeraData.Location = new System.Drawing.Point(602, 515);
        this.buttonRedigeraData.Name = "buttonRedigeraData";
        this.buttonRedigeraData.Size = new System.Drawing.Size(160, 40);
        this.buttonRedigeraData.TabIndex = 12;
        this.buttonRedigeraData.Text = "Redigera";
        this.buttonRedigeraData.UseVisualStyleBackColor = true;
        this.buttonRedigeraData.Click += new System.EventHandler(this.buttonRedigeraData_Click);
        // 
        // labelTotalaAntaletYtor
        // 
        this.labelTotalaAntaletYtor.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelTotalaAntaletYtor.ForeColor = System.Drawing.Color.Black;
        this.labelTotalaAntaletYtor.Location = new System.Drawing.Point(335, 70);
        this.labelTotalaAntaletYtor.Name = "labelTotalaAntaletYtor";
        this.labelTotalaAntaletYtor.RightToLeft = System.Windows.Forms.RightToLeft.No;
        this.labelTotalaAntaletYtor.Size = new System.Drawing.Size(290, 26);
        this.labelTotalaAntaletYtor.TabIndex = 5;
        this.labelTotalaAntaletYtor.Text = "_";
        // 
        // labelEgenuppföljningstyp
        // 
        this.labelEgenuppföljningstyp.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelEgenuppföljningstyp.ForeColor = System.Drawing.Color.Black;
        this.labelEgenuppföljningstyp.Location = new System.Drawing.Point(25, 30);
        this.labelEgenuppföljningstyp.Name = "labelEgenuppföljningstyp";
        this.labelEgenuppföljningstyp.Size = new System.Drawing.Size(290, 26);
        this.labelEgenuppföljningstyp.TabIndex = 0;
        this.labelEgenuppföljningstyp.Text = "_";
        // 
        // groupBox4
        // 
        this.groupBox4.Controls.Add(this.labelAtgAreal);
        this.groupBox4.Controls.Add(this.labelInvtypID);
        this.groupBox4.Controls.Add(this.labelEntreprenörNamn);
        this.groupBox4.Controls.Add(this.labelEntreprenörNr);
        this.groupBox4.Controls.Add(this.labelTraktNamn);
        this.groupBox4.Controls.Add(this.labelDistriktNamn);
        this.groupBox4.Controls.Add(this.labelRegionNamn);
        this.groupBox4.Controls.Add(this.labelTraktnr);
        this.groupBox4.Controls.Add(this.labelDistriktID);
        this.groupBox4.Controls.Add(this.labelRegionID);
        this.groupBox4.Location = new System.Drawing.Point(0, 139);
        this.groupBox4.Name = "groupBox4";
        this.groupBox4.Size = new System.Drawing.Size(970, 127);
        this.groupBox4.TabIndex = 7;
        this.groupBox4.TabStop = false;
        // 
        // labelInvtypID
        // 
        this.labelInvtypID.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelInvtypID.ForeColor = System.Drawing.Color.Black;
        this.labelInvtypID.Location = new System.Drawing.Point(25, 15);
        this.labelInvtypID.Name = "labelInvtypID";
        this.labelInvtypID.Size = new System.Drawing.Size(200, 26);
        this.labelInvtypID.TabIndex = 8;
        this.labelInvtypID.Text = "_";
        // 
        // labelEntreprenörNamn
        // 
        this.labelEntreprenörNamn.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelEntreprenörNamn.ForeColor = System.Drawing.Color.Black;
        this.labelEntreprenörNamn.Location = new System.Drawing.Point(25, 91);
        this.labelEntreprenörNamn.Name = "labelEntreprenörNamn";
        this.labelEntreprenörNamn.Size = new System.Drawing.Size(241, 26);
        this.labelEntreprenörNamn.TabIndex = 7;
        this.labelEntreprenörNamn.Text = "_";
        // 
        // labelEntreprenörNr
        // 
        this.labelEntreprenörNr.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelEntreprenörNr.ForeColor = System.Drawing.Color.Black;
        this.labelEntreprenörNr.Location = new System.Drawing.Point(25, 53);
        this.labelEntreprenörNr.Name = "labelEntreprenörNr";
        this.labelEntreprenörNr.Size = new System.Drawing.Size(233, 26);
        this.labelEntreprenörNr.TabIndex = 3;
        this.labelEntreprenörNr.Text = "_";
        // 
        // labelTraktNamn
        // 
        this.labelTraktNamn.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelTraktNamn.ForeColor = System.Drawing.Color.Black;
        this.labelTraktNamn.Location = new System.Drawing.Point(715, 53);
        this.labelTraktNamn.Name = "labelTraktNamn";
        this.labelTraktNamn.Size = new System.Drawing.Size(200, 26);
        this.labelTraktNamn.TabIndex = 6;
        this.labelTraktNamn.Text = "_";
        // 
        // labelDistriktNamn
        // 
        this.labelDistriktNamn.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelDistriktNamn.ForeColor = System.Drawing.Color.Black;
        this.labelDistriktNamn.Location = new System.Drawing.Point(485, 53);
        this.labelDistriktNamn.Name = "labelDistriktNamn";
        this.labelDistriktNamn.Size = new System.Drawing.Size(200, 26);
        this.labelDistriktNamn.TabIndex = 5;
        this.labelDistriktNamn.Text = "_";
        // 
        // labelRegionNamn
        // 
        this.labelRegionNamn.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelRegionNamn.ForeColor = System.Drawing.Color.Black;
        this.labelRegionNamn.Location = new System.Drawing.Point(255, 53);
        this.labelRegionNamn.Name = "labelRegionNamn";
        this.labelRegionNamn.Size = new System.Drawing.Size(200, 26);
        this.labelRegionNamn.TabIndex = 4;
        this.labelRegionNamn.Text = "_";
        // 
        // labelTraktnr
        // 
        this.labelTraktnr.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelTraktnr.ForeColor = System.Drawing.Color.Black;
        this.labelTraktnr.Location = new System.Drawing.Point(715, 15);
        this.labelTraktnr.Name = "labelTraktnr";
        this.labelTraktnr.Size = new System.Drawing.Size(200, 26);
        this.labelTraktnr.TabIndex = 2;
        this.labelTraktnr.Text = "_";
        // 
        // labelDistriktID
        // 
        this.labelDistriktID.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelDistriktID.ForeColor = System.Drawing.Color.Black;
        this.labelDistriktID.Location = new System.Drawing.Point(485, 15);
        this.labelDistriktID.Name = "labelDistriktID";
        this.labelDistriktID.Size = new System.Drawing.Size(200, 26);
        this.labelDistriktID.TabIndex = 1;
        this.labelDistriktID.Text = "_";
        // 
        // labelRegionID
        // 
        this.labelRegionID.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelRegionID.ForeColor = System.Drawing.Color.Black;
        this.labelRegionID.Location = new System.Drawing.Point(255, 15);
        this.labelRegionID.Name = "labelRegionID";
        this.labelRegionID.Size = new System.Drawing.Size(200, 26);
        this.labelRegionID.TabIndex = 0;
        this.labelRegionID.Text = "_";
        // 
        // labelMarkberedningsMetod
        // 
        this.labelMarkberedningsMetod.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelMarkberedningsMetod.ForeColor = System.Drawing.Color.Black;
        this.labelMarkberedningsMetod.Location = new System.Drawing.Point(25, 110);
        this.labelMarkberedningsMetod.Name = "labelMarkberedningsMetod";
        this.labelMarkberedningsMetod.Size = new System.Drawing.Size(290, 26);
        this.labelMarkberedningsMetod.TabIndex = 6;
        this.labelMarkberedningsMetod.Text = "_";
        // 
        // contextMenuStripExportera
        // 
        this.contextMenuStripExportera.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemExcel2007,
            this.toolStripMenuItemPdf});
        this.contextMenuStripExportera.Name = "contextMenuStripExportera";
        this.contextMenuStripExportera.Size = new System.Drawing.Size(128, 48);
        // 
        // toolStripMenuItemExcel2007
        // 
        this.toolStripMenuItemExcel2007.Name = "toolStripMenuItemExcel2007";
        this.toolStripMenuItemExcel2007.Size = new System.Drawing.Size(127, 22);
        this.toolStripMenuItemExcel2007.Text = "Excel 2007";
        this.toolStripMenuItemExcel2007.Click += new System.EventHandler(this.toolStripMenuItemExcel2007_Click);
        // 
        // toolStripMenuItemPdf
        // 
        this.toolStripMenuItemPdf.Name = "toolStripMenuItemPdf";
        this.toolStripMenuItemPdf.Size = new System.Drawing.Size(127, 22);
        this.toolStripMenuItemPdf.Text = "Adobe PDF";
        this.toolStripMenuItemPdf.Click += new System.EventHandler(this.toolStripMenuItemPdf_Click);
        // 
        // labelAtgAreal
        // 
        this.labelAtgAreal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelAtgAreal.ForeColor = System.Drawing.Color.Black;
        this.labelAtgAreal.Location = new System.Drawing.Point(255, 91);
        this.labelAtgAreal.Name = "labelAtgAreal";
        this.labelAtgAreal.Size = new System.Drawing.Size(200, 26);
        this.labelAtgAreal.TabIndex = 9;
        this.labelAtgAreal.Text = "_";
        this.labelAtgAreal.Visible = false;
        // 
        // FormHanteraData
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.Controls.Add(this.groupBoxRapportInfo);
        this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.Name = "FormHanteraData";
        this.Size = new System.Drawing.Size(989, 570);
        this.groupBoxRapportInfo.ResumeLayout(false);
        this.groupBoxData.ResumeLayout(false);
        this.groupBoxData.PerformLayout();
        this.groupBox2.ResumeLayout(false);
        this.groupBox2.PerformLayout();
        this.groupBox4.ResumeLayout(false);
        this.contextMenuStripExportera.ResumeLayout(false);
        this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox groupBoxRapportInfo;
    private System.Windows.Forms.Label labelTotalaAntaletYtor;
    private System.Windows.Forms.Label labelEgenuppföljningstyp;
    private System.Windows.Forms.GroupBox groupBox4;
    private System.Windows.Forms.Label labelEntreprenörNr;
    private System.Windows.Forms.Label labelTraktNamn;
    private System.Windows.Forms.Label labelDistriktNamn;
    private System.Windows.Forms.Label labelRegionNamn;
    private System.Windows.Forms.Label labelStandort;
    private System.Windows.Forms.Label labelTraktnr;
    private System.Windows.Forms.Label labelDistriktID;
    private System.Windows.Forms.Label labelRegionID;
    private System.Windows.Forms.Button buttonRedigeraData;
    private System.Windows.Forms.Button buttonExportera;
    private System.Windows.Forms.Button buttonTaBortData;
    private System.Windows.Forms.ImageList imageList32;
    private System.Windows.Forms.Label labelTotalaAntaletFrågor;
    private System.Windows.Forms.GroupBox groupBoxData;
    private System.Windows.Forms.Label labelGISAreal;
    private System.Windows.Forms.Label labelStickvägsavstånd;
    private System.Windows.Forms.Label labelProvytestorlek;
    private System.Windows.Forms.Label labelDatum;
    private System.Windows.Forms.Label labelGISSträcka;
    private System.Windows.Forms.Label labelMålgrundyta;
    private System.Windows.Forms.Label labelMarkberedningsMetod;
    private System.Windows.Forms.Label labelUrsprung;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.Label labelTitelKommentar;
    private System.Windows.Forms.Label labelTomDatabas;
    private System.Windows.Forms.Button buttonUppdateraData;
    private System.Windows.Forms.Label labelDatabasPosition;
    private System.Windows.Forms.Label labelAreal;
    private System.Windows.Forms.Label labelEntreprenörNamn;
    private System.Windows.Forms.Label labelVägkant;
    private System.Windows.Forms.Label labelHygge;
    private System.Windows.Forms.Label labelAvstånd;
    private System.Windows.Forms.Label labelBedömdVolym;
    private System.Windows.Forms.Label labelÅker;
    private System.Windows.Forms.Label labelAntalVältor;
    private System.Windows.Forms.Label labelStickvägssystem;
    private System.Windows.Forms.Label labelDatainsamlingsmetod;
    private System.Windows.Forms.RichTextBox richTextBoxKommentar;
    private System.Windows.Forms.Button buttonRapport;
    private System.Windows.Forms.ContextMenuStrip contextMenuStripExportera;
    private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemExcel2007;
    private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemPdf;
    private System.Windows.Forms.Label labelInvtypID;
    private System.Windows.Forms.Label labelAtgAreal;
  }
}
