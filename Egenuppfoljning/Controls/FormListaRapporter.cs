﻿#region

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Egenuppfoljning.Dialogs;
using Egenuppfoljning.Interfaces;
using Egenuppfoljning.Properties;
using FastReport;
using FastReport.Export.OoXML;
using FastReport.Export.Pdf;

#endregion

namespace Egenuppfoljning.Controls
{
    // ReSharper disable UnusedMember.Global
    public partial class FormListaRapporter : UserControl
    // ReSharper restore UnusedMember.Global
    {
        private List<KorusDataEventArgs.Frågor> mFrågor;
        private List<KorusDataEventArgs.Data> mRapporter;
        private List<KorusDataEventArgs.Yta> mYtor;

        public FormListaRapporter()
        {
            InitializeComponent();
            mRapporter = new List<KorusDataEventArgs.Data>();
            mFrågor = new List<KorusDataEventArgs.Frågor>();
            mYtor = new List<KorusDataEventArgs.Yta>();

            reportExporteraSammanstallning.RegisterData(dataSetSammanstallningar, "dataSetSammanstallningar");
            reportExporteraSammanstallning.RegisterData(dataSetValdFiltrering, "dataSetValdFiltrering");
        }

        // ReSharper disable EventNeverSubscribedTo.Global
        public event DataEventHandler HämtaFrågorFörValtFilter;
        public event DataEventHandler HämtaYtorFörValtFilter;

        public event StatusEventHandler StatusEventRefresh;
        public event DataEventHandler DataEventGåTillRapport;
        public event DataEventHandler DataEventTaBortRapport;
        public event RapportEventHandler DataEventLagraÄndradeRapporter;
        // ReSharper restore EventNeverSubscribedTo.Global
        // ReSharper disable UnusedMember.Global
        public void Close()
        // ReSharper restore UnusedMember.Global
        {
            Settings.Default.Save();
            Hide();
        }

        private static bool Filter(KorusDataEventArgs.Data aRapport)
        {
            if (Settings.Default.FiltreraRapportInvTypId >= 0 &&
                (aRapport.invtyp != Settings.Default.FiltreraRapportInvTypId))
            {
                return false;
            }

            if (Settings.Default.FiltreraRapportRegionId > 0 &&
                (aRapport.regionid != Settings.Default.FiltreraRapportRegionId))
            {
                return false;
            }
            if (Settings.Default.FiltreraRapportDistriktId > 0 &&
                (aRapport.distriktid != Settings.Default.FiltreraRapportDistriktId))
            {
                return false;
            }
            if (Settings.Default.FiltreraRapportTraktNr > 0 && (aRapport.traktnr != Settings.Default.FiltreraRapportTraktNr))
            {
                return false;
            }
            if (Settings.Default.FiltreraRapportStandort > 0 &&
                (aRapport.standort != Settings.Default.FiltreraRapportStandort))
            {
                return false;
            }
            if (Settings.Default.FiltreraRapportEntreprenor > 0 &&
                (aRapport.entreprenor != Settings.Default.FiltreraRapportEntreprenor))
            {
                return false;
            }
            if (Settings.Default.FiltreraRapportArtal > 0 && (aRapport.årtal != Settings.Default.FiltreraRapportArtal))
            {
                return false;
            }

            if (!Settings.Default.FiltreraRapportTraktNamn.Equals(string.Empty) &&
                !(aRapport.traktnamn.Equals(Settings.Default.FiltreraRapportTraktNamn)))
            {
                return false;
            }
            if (!Settings.Default.FiltreraRapportRegionNamn.Equals(string.Empty) &&
                !(aRapport.regionnamn.Equals(Settings.Default.FiltreraRapportRegionNamn)))
            {
                return false;
            }
            if (!Settings.Default.FiltreraRapportDistriktNamn.Equals(string.Empty) &&
                !(aRapport.distriktnamn.Equals(Settings.Default.FiltreraRapportDistriktNamn)))
            {
                return false;
            }
            if (!Settings.Default.FiltreraRapportEntreprenorNamn.Equals(string.Empty) &&
                !(aRapport.entreprenornamn.Equals(Settings.Default.FiltreraRapportEntreprenorNamn)))
            {
                return false;
            }
            /* if (aRapport.rapport_typ != Settings.Default.ValdTypRapport)
            {
              return false;
            }*/
            if (!Settings.Default.FiltreraRapportUrsprung.Equals(string.Empty) &&
                !(aRapport.ursprung.Equals(Settings.Default.FiltreraRapportUrsprung)))
            {
                return false;
            }
            if (!Settings.Default.FiltreraRapportMarkberedningsmetod.Equals(string.Empty) &&
                !(aRapport.markberedningsmetod.Equals(Settings.Default.FiltreraRapportMarkberedningsmetod)))
            {
                return false;
            }
            if (!Settings.Default.FiltreraRapportDatainsamlingsmetod.Equals(string.Empty) &&
                !(aRapport.datainsamlingsmetod.Equals(Settings.Default.FiltreraRapportDatainsamlingsmetod)))
            {
                return false;
            }

            if (Settings.Default.FiltreraRapportAreal > 0 &&
                (Math.Abs(aRapport.areal - Settings.Default.FiltreraRapportAreal) > 0.1))
            {
                return false;
            }
            if (Settings.Default.FiltreraRapportMalgrundyta > 0 &&
                (aRapport.malgrundyta != Settings.Default.FiltreraRapportMalgrundyta))
            {
                return false;
            }
            if (Settings.Default.FiltreraRapportGISStracka > 0 &&
                (aRapport.gisstracka != Settings.Default.FiltreraRapportGISStracka))
            {
                return false;
            }
            if (Settings.Default.FiltreraRapportGISAreal > 0 &&
                (Math.Abs(aRapport.gisareal - Settings.Default.FiltreraRapportGISAreal) > 0.1))
            {
                return false;
            }
            if (Settings.Default.FiltreraRapportProvytestorlek > 0 &&
                (aRapport.provytestorlek != Settings.Default.FiltreraRapportProvytestorlek))
            {
                return false;
            }

            if (!Settings.Default.FiltreraRapportKommentar.Equals(string.Empty) &&
                !(aRapport.kommentar.Equals(Settings.Default.FiltreraRapportKommentar)))
            {
                return false;
            }

            if (!Settings.Default.FiltreraRapportStickvagssystem.Equals(string.Empty) &&
                !(aRapport.stickvagssystem.Equals(Settings.Default.FiltreraRapportStickvagssystem)))
            {
                return false;
            }
            if (Settings.Default.FiltreraRapportStickvagsavstand > 0 &&
                (aRapport.stickvagsavstand != Settings.Default.FiltreraRapportStickvagsavstand))
            {
                return false;
            }


            if (Settings.Default.FiltreraRapportAntalValtor > 0 &&
                (aRapport.antalvaltor != Settings.Default.FiltreraRapportAntalValtor))
            {
                return false;
            }
            if (Settings.Default.FiltreraRapportAvstand > 0 && (aRapport.avstand != Settings.Default.FiltreraRapportAvstand))
            {
                return false;
            }
            if (Settings.Default.FiltreraRapportBedomdVolym > 0 &&
                (aRapport.bedomdvolym != Settings.Default.FiltreraRapportBedomdVolym))
            {
                return false;
            }
            if (Settings.Default.FiltreraRapportHygge > 0 && (aRapport.hygge != Settings.Default.FiltreraRapportHygge))
            {
                return false;
            }
            if (Settings.Default.FiltreraRapportVagkant > 0 && (aRapport.vagkant != Settings.Default.FiltreraRapportVagkant))
            {
                return false;
            }
            if (Settings.Default.FiltreraRapportAker > 0 && (aRapport.aker != Settings.Default.FiltreraRapportAker))
            {
                return false;
            }
            if (Settings.Default.FiltreraRapportLatbilshugg && !aRapport.latbilshugg)
            {
                return false;
            }
            if (Settings.Default.FiltreraRapportGrotbil && !aRapport.grotbil)
            {
                return false;
            }
            if (Settings.Default.FiltreraRapportTraktordragenhugg && !aRapport.traktordragenhugg)
            {
                return false;
            }
            if (Settings.Default.FiltreraRapportSkotarburenhugg && !aRapport.skotarburenhugg)
            {
                return false;
            }
            if (Settings.Default.FiltreraRapportAvlagg1norr > 0 &&
                (aRapport.avlagg1norr != Settings.Default.FiltreraRapportAvlagg1norr))
            {
                return false;
            }
            if (Settings.Default.FiltreraRapportAvlagg1ost > 0 &&
                (aRapport.avlagg1ost != Settings.Default.FiltreraRapportAvlagg1ost))
            {
                return false;
            }
            if (Settings.Default.FiltreraRapportAvlagg2norr > 0 &&
                (aRapport.avlagg2norr != Settings.Default.FiltreraRapportAvlagg2norr))
            {
                return false;
            }
            if (Settings.Default.FiltreraRapportAvlagg2ost > 0 &&
                (aRapport.avlagg2ost != Settings.Default.FiltreraRapportAvlagg2ost))
            {
                return false;
            }
            if (Settings.Default.FiltreraRapportAvlagg3norr > 0 &&
                (aRapport.avlagg3norr != Settings.Default.FiltreraRapportAvlagg3norr))
            {
                return false;
            }
            if (Settings.Default.FiltreraRapportAvlagg3ost > 0 &&
                (aRapport.avlagg3ost != Settings.Default.FiltreraRapportAvlagg3ost))
            {
                return false;
            }
            if (Settings.Default.FiltreraRapportAvlagg4norr > 0 &&
                (aRapport.avlagg4norr != Settings.Default.FiltreraRapportAvlagg4norr))
            {
                return false;
            }
            if (Settings.Default.FiltreraRapportAvlagg4ost > 0 &&
                (aRapport.avlagg4ost != Settings.Default.FiltreraRapportAvlagg4ost))
            {
                return false;
            }
            if (Settings.Default.FiltreraRapportAvlagg5norr > 0 &&
                (aRapport.avlagg5norr != Settings.Default.FiltreraRapportAvlagg5norr))
            {
                return false;
            }
            if (Settings.Default.FiltreraRapportAvlagg5ost > 0 &&
                (aRapport.avlagg5ost != Settings.Default.FiltreraRapportAvlagg5ost))
            {
                return false;
            }
            if (Settings.Default.FiltreraRapportAvlagg6norr > 0 &&
                (aRapport.avlagg6norr != Settings.Default.FiltreraRapportAvlagg6norr))
            {
                return false;
            }
            if (Settings.Default.FiltreraRapportInvTypId >= 0 &&
                   (aRapport.invtyp != Settings.Default.FiltreraRapportInvTypId))
            {
                return false;
            }

            return Settings.Default.FiltreraRapportAvlagg6ost <= 0 ||
                   (aRapport.avlagg6ost == Settings.Default.FiltreraRapportAvlagg6ost);
        }

        private void UppdateraFilterText()
        {
            var filterText = "Filter: ";

            if (Settings.Default.FiltreraRapportTraktNr > 0)
            {
                filterText += " TraktNr: " + Settings.Default.FiltreraRapportTraktNr.ToString(CultureInfo.InvariantCulture) +
                              ",";
            }
            if (Settings.Default.FiltreraRapportStandort > 0)
            {
                filterText += " Traktdel: " + Settings.Default.FiltreraRapportStandort.ToString(CultureInfo.InvariantCulture) +
                              ",";
            }
            if (Settings.Default.FiltreraRapportEntreprenor > 0)
            {
                filterText += " Entreprenör: " +
                              Settings.Default.FiltreraRapportEntreprenor.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraRapportArtal > 0)
            {
                filterText += " Artal: " + Settings.Default.FiltreraRapportArtal.ToString(CultureInfo.InvariantCulture) + ",";
            }

            if (!Settings.Default.FiltreraRapportTraktNamn.Equals(string.Empty))
            {
                filterText += " TraktNamn: " + Settings.Default.FiltreraRapportTraktNamn + ",";
            }
            if (!(Settings.Default.FiltreraRapportInvTypId==Settings.Default.MinusEtt))
            {
                filterText += " Invtyp: " + Settings.Default.FiltreraRapportInvTypNamn + ",";
            }

            if (!Settings.Default.FiltreraRapportRegionNamn.Equals(string.Empty))
            {
                filterText += " Region: " + Settings.Default.FiltreraRapportRegionNamn + ",";
            }
            if (!Settings.Default.FiltreraRapportDistriktNamn.Equals(string.Empty))
            {
                filterText += " Distrikt: " + Settings.Default.FiltreraRapportDistriktNamn + ",";
            }
            if (!Settings.Default.FiltreraRapportEntreprenorNamn.Equals(string.Empty))
            {
                filterText += " EntreprenörNamn: " + Settings.Default.FiltreraRapportEntreprenorNamn + ",";
            }
            if (!Settings.Default.FiltreraRapportTypNamn.Equals(string.Empty))
            {
                filterText += " RapportTyp: " + Settings.Default.FiltreraRapportTypNamn + ",";
            }
            if (!Settings.Default.FiltreraRapportUrsprung.Equals(string.Empty))
            {
                filterText += " Ursprung: " + Settings.Default.FiltreraRapportUrsprung + ",";
            }
            if (!Settings.Default.FiltreraRapportMarkberedningsmetod.Equals(string.Empty))
            {
                filterText += " Markberedningsmetod: " + Settings.Default.FiltreraRapportMarkberedningsmetod + ",";
            }
            if (!Settings.Default.FiltreraRapportDatainsamlingsmetod.Equals(string.Empty))
            {
                filterText += " Datainsamlingsmetod: " + Settings.Default.FiltreraRapportDatainsamlingsmetod + ",";
            }

            if (Settings.Default.FiltreraRapportAreal > 0)
            {
                filterText += " Areal: " + Settings.Default.FiltreraRapportAreal.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraRapportMalgrundyta > 0)
            {
                filterText += " Målgrundyta: " +
                              Settings.Default.FiltreraRapportMalgrundyta.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraRapportGISStracka > 0)
            {
                filterText += " GIS-sträcka: " +
                              Settings.Default.FiltreraRapportGISStracka.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraRapportGISAreal > 0)
            {
                filterText += " GIS-areal: " + Settings.Default.FiltreraRapportGISAreal.ToString(CultureInfo.InvariantCulture) +
                              ",";
            }
            if (Settings.Default.FiltreraRapportProvytestorlek > 0)
            {
                filterText += " Provytestorlek: " +
                              Settings.Default.FiltreraRapportProvytestorlek.ToString(CultureInfo.InvariantCulture) + ",";
            }

            if (!Settings.Default.FiltreraRapportKommentar.Equals(string.Empty))
            {
                filterText += " Kommentar: " + Settings.Default.FiltreraRapportKommentar + ",";
            }
            if (!Settings.Default.FiltreraRapportStickvagssystem.Equals(string.Empty))
            {
                filterText += " Stickvägssystem: " + Settings.Default.FiltreraRapportStickvagssystem + ",";
            }
            if (Settings.Default.FiltreraRapportStickvagsavstand > 0)
            {
                filterText += " Stickvägsavstånd: " +
                              Settings.Default.FiltreraRapportStickvagsavstand.ToString(CultureInfo.InvariantCulture) + ",";
            }


            if (Settings.Default.FiltreraRapportAntalValtor > 0)
            {
                filterText += " Antal vältor: " +
                              Settings.Default.FiltreraRapportAntalValtor.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraRapportAvstand > 0)
            {
                filterText += " Avstånd: " + Settings.Default.FiltreraRapportAvstand.ToString(CultureInfo.InvariantCulture) +
                              ",";
            }
            if (Settings.Default.FiltreraRapportBedomdVolym > 0)
            {
                filterText += " Bedömd volym: " +
                              Settings.Default.FiltreraRapportBedomdVolym.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraRapportHygge > 0)
            {
                filterText += " Hygge: " + Settings.Default.FiltreraRapportHygge.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraRapportVagkant > 0)
            {
                filterText += " Vägkant: " + Settings.Default.FiltreraRapportVagkant.ToString(CultureInfo.InvariantCulture) +
                              ",";
            }
            if (Settings.Default.FiltreraRapportAker > 0)
            {
                filterText += " Åker: " + Settings.Default.FiltreraRapportAker.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraRapportLatbilshugg)
            {
                filterText += " Latbilshugg: Ja,";
            }
            if (Settings.Default.FiltreraRapportGrotbil)
            {
                filterText += " Grotbil: Ja, ";
            }
            if (Settings.Default.FiltreraRapportTraktordragenhugg)
            {
                filterText += " Traktordragenhugg: Ja, ";
            }
            if (Settings.Default.FiltreraRapportSkotarburenhugg)
            {
                filterText += " Skotarburenhugg: Ja,";
            }
            if (Settings.Default.FiltreraRapportAvlagg1norr > 0)
            {
                filterText += " Avlägg1 Norr: " +
                              Settings.Default.FiltreraRapportAvlagg1norr.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraRapportAvlagg1ost > 0)
            {
                filterText += " Avlägg1 Ost: " +
                              Settings.Default.FiltreraRapportAvlagg1ost.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraRapportAvlagg2norr > 0)
            {
                filterText += " Avlägg2 Norr: " +
                              Settings.Default.FiltreraRapportAvlagg2norr.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraRapportAvlagg2ost > 0)
            {
                filterText += " Avlägg2 Ost: " +
                              Settings.Default.FiltreraRapportAvlagg2ost.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraRapportAvlagg3norr > 0)
            {
                filterText += " Avlägg3 Norr: " +
                              Settings.Default.FiltreraRapportAvlagg3norr.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraRapportAvlagg3ost > 0)
            {
                filterText += " Avlägg3 Ost: " +
                              Settings.Default.FiltreraRapportAvlagg3ost.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraRapportAvlagg4norr > 0)
            {
                filterText += " Avlägg4 Norr: " +
                              Settings.Default.FiltreraRapportAvlagg4norr.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraRapportAvlagg4ost > 0)
            {
                filterText += " Avlägg4 Ost: " +
                              Settings.Default.FiltreraRapportAvlagg4ost.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraRapportAvlagg5norr > 0)
            {
                filterText += " Avlägg5 Norr: " +
                              Settings.Default.FiltreraRapportAvlagg5norr.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraRapportAvlagg5ost > 0)
            {
                filterText += " Avlägg5 Ost: " +
                              Settings.Default.FiltreraRapportAvlagg5ost.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraRapportAvlagg6norr > 0)
            {
                filterText += " Avlägg6 Norr: " +
                              Settings.Default.FiltreraRapportAvlagg6norr.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraRapportAvlagg6ost > 0)
            {
                filterText += " Avlägg6 Ost: " +
                              Settings.Default.FiltreraRapportAvlagg6ost.ToString(CultureInfo.InvariantCulture) + ",";
            }

            if (filterText.Equals("Filter: "))
            {
                labelFiltrering.Text = filterText + Resources.Inget;
            }
            else
            {
                labelFiltrering.Text = filterText.Substring(0, filterText.Length - 1);
            }
        }

        private static double MedelVärde(double aTäljare, double aNämnare)
        {
            return MedelVärde(aTäljare, aNämnare, true);
        }

        private static double MedelVärde(double aTäljare, double aNämnare, bool aRound)
        {
            var rValue = 0.0;
            if (aTäljare > 0 && aNämnare > 0)
            {
                rValue = aRound ? Math.Round((aTäljare / aNämnare), 1) : (aTäljare / aNämnare);
            }
            return rValue;
        }

        private static void BeräknaRöjningsVärden(List<KorusDataEventArgs.Yta> aYtor, out int aHuvudstammar,
                                                  out double aMedBarrHst, out int aT, out int aG, out int aL, out int aC, out double aMedLovHst, out double aRojstamha)
        {
            aHuvudstammar = 0;
            aMedBarrHst = 0.0;
            aMedLovHst = 0.0;
            aRojstamha = 0.0;
            aT = 0;
            aL = 0;
            aG = 0;
            aC = 0;
            var medTallDecimal = 0.0;
            var medContDecimal = 0.0;
            var medGranDecimal = 0.0;
            var medBjörkDecimal = 0.0;

            var medTallMedelhöjdDecimal = 0.0;
            var medGranMedelhöjdDecimal = 0.0;
            var medContmedelhöjdDecimal = 0.0;
            var medBjörkmedelhöjdDecimal = 0.0;

            var sumHst = 0;
            var sumRojst = 0;

            try
            {
                foreach (var yta in aYtor)
                {
                    medTallDecimal += yta.tall;
                    medContDecimal += yta.contorta;
                    medGranDecimal += yta.gran;
                    medBjörkDecimal += yta.lov;
                    aMedBarrHst += yta.mhojd;
                    aMedLovHst += yta.mhojd;
                    sumHst += yta.hst;
                    sumRojst += yta.rojstam;

                    medTallMedelhöjdDecimal += (yta.tallmedelhojd * yta.tall);
                    medGranMedelhöjdDecimal += (yta.granmedelhojd * yta.gran);
                    medContmedelhöjdDecimal += (yta.contortamedelhojd * yta.contorta);
                    medBjörkmedelhöjdDecimal += (yta.lovmedelhojd * yta.lov);
                }
                var antalYtor = aYtor.Count();

                medTallMedelhöjdDecimal = MedelVärde(medTallMedelhöjdDecimal, medTallDecimal, false);
                medGranMedelhöjdDecimal = MedelVärde(medGranMedelhöjdDecimal, medGranDecimal, false);
                medContmedelhöjdDecimal = MedelVärde(medContmedelhöjdDecimal, medContDecimal, false);
                medBjörkmedelhöjdDecimal = MedelVärde(medBjörkmedelhöjdDecimal,medBjörkDecimal,false);

                aT = (int)Math.Round(((100 * medTallDecimal) / sumHst), 0);
                aC = (int)Math.Round((100 * medContDecimal) / sumHst, 0);
                aG = (int)Math.Round((100 * medGranDecimal) / sumHst, 0);
                aL = (int)Math.Round((100 * medBjörkDecimal) / sumHst, 0);

                var täljare = medTallDecimal * medTallMedelhöjdDecimal
                              + medGranDecimal * medGranMedelhöjdDecimal
                              + medContDecimal * medContmedelhöjdDecimal;

                var nämnare = medTallDecimal + medGranDecimal + medContDecimal;

                if (täljare > 0 && nämnare > 0)
                {
                    aMedBarrHst = Math.Round((täljare / nämnare), 1);
                }

                täljare = medBjörkDecimal * medBjörkmedelhöjdDecimal;
                nämnare = medBjörkDecimal;

                if (täljare > 0 && nämnare > 0)
                {
                    aMedLovHst = Math.Round((täljare / nämnare), 1);
                }

                if (sumRojst > 0 && antalYtor > 0)
                {
                    aRojstamha = (double)Math.Round(sumRojst/((antalYtor * 100) / 10000.0),1);
                }

                medTallDecimal = MedelVärde(medTallDecimal, antalYtor, false);
                medContDecimal = MedelVärde(medContDecimal, antalYtor, false);
                medGranDecimal = MedelVärde(medGranDecimal, antalYtor, false);
                medBjörkDecimal = MedelVärde(medBjörkDecimal, antalYtor, false);
                aHuvudstammar = (int)Math.Round(100 * (medTallDecimal + medContDecimal + medGranDecimal + medBjörkDecimal), 0);
            }
            catch (Exception ex)
            {
#if !DEBUG
       MessageBox.Show("Misslyckades med att beräkna röjningsdata", "Beräkna data misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }


        private static void BeräknaPlanteringsVärden(List<KorusDataEventArgs.Yta> aYtor, int aProvytestorlek,
                                                     out double aMedOptimaltSatta, out double aMedBraSatta,
                                                     out double aMedTotaltSatta, out int aUtnyttjandegrad,
                                                     out int aAndelMedBraTilltryckning, out int aAndelMedBraDjup,
                                                     out int aUPI, out int aAntalSattaPlantor)
        {
            aMedOptimaltSatta = 0;
            aMedBraSatta = 0;
            aMedTotaltSatta = 0;
            aUtnyttjandegrad = 0;
            aAndelMedBraTilltryckning = 0;
            aAndelMedBraDjup = 0;
            aUPI = 0;
            aAntalSattaPlantor = 0;
            var medVaravBättreDecimal = 0.0;
            var medTilltryckningDecimal = 0.0;
            var medPlanteringsDjupfelDecimal = 0.0;
            var andelMedBraTilltryckningDecimal = 0.0;
            var andelMedBraDjupDecimal = 0.0;

            try
            {
                foreach (var yta in aYtor)
                {
                    aMedOptimaltSatta += yta.optimalt;
                    aMedBraSatta += yta.bra;
                    aMedTotaltSatta += (yta.optimalt + yta.bra + yta.ovrigt);
                    medVaravBättreDecimal += yta.varavbattre;
                    medTilltryckningDecimal += yta.tilltryckning;
                    medPlanteringsDjupfelDecimal += yta.planteringsdjupfel;
                }
                var antalYtor = aYtor.Count();

                medVaravBättreDecimal = MedelVärde(medVaravBättreDecimal, antalYtor, false);
                var medTotaltSattaDecimal = MedelVärde(aMedTotaltSatta, antalYtor, false);
                medTilltryckningDecimal = MedelVärde(medTilltryckningDecimal, antalYtor, false);
                medPlanteringsDjupfelDecimal = MedelVärde(medPlanteringsDjupfelDecimal, antalYtor, false);

                aAntalSattaPlantor = (aProvytestorlek > 0)
                                       ? ((int)
                                          Math.Round((medTotaltSattaDecimal * 100 * (100 / (double)aProvytestorlek)), 0))
                                       : 0;

                var uttnyttandeGradDecimal = (medVaravBättreDecimal > 0 && medTotaltSattaDecimal > 0)
                                               ? (100 * (1 - (medVaravBättreDecimal / medTotaltSattaDecimal)))
                                               : 0;
                aUtnyttjandegrad = (int)Math.Round(uttnyttandeGradDecimal, 0);
                if (medVaravBättreDecimal <= 0 && medTotaltSattaDecimal > 0)
                {
                    uttnyttandeGradDecimal = aUtnyttjandegrad = 100;
                }

                if (medTilltryckningDecimal > 0 && medTotaltSattaDecimal > 0)
                {
                    if (medTilltryckningDecimal > medTotaltSattaDecimal)
                    {
                        //andelMedBraTilltryckning kan inte bli negativt
                        medTilltryckningDecimal = medTotaltSattaDecimal;
                    }
                    andelMedBraTilltryckningDecimal = (100 * (1 - (medTilltryckningDecimal / medTotaltSattaDecimal)));
                    aAndelMedBraTilltryckning = (int)Math.Round(andelMedBraTilltryckningDecimal, 0);
                }
                else
                {
                    aAndelMedBraTilltryckning = 100;
                }

                if (medPlanteringsDjupfelDecimal > 0 && medTotaltSattaDecimal > 0)
                {
                    andelMedBraDjupDecimal = (100 * (1 - (medPlanteringsDjupfelDecimal / medTotaltSattaDecimal)));
                    aAndelMedBraDjup = (int)Math.Round(andelMedBraDjupDecimal, 0);
                    if (aAndelMedBraDjup < 0)
                    {
                        aAndelMedBraDjup = 0;
                    }
                }
                else
                {
                    aAndelMedBraDjup = 100;
                }

                if ((aAndelMedBraTilltryckning + aAndelMedBraDjup) > 0)
                {
                    aUPI = (int)Math.Round(
                      (uttnyttandeGradDecimal + ((aAndelMedBraDjup + aAndelMedBraTilltryckning) / 2)) / 2, 0);
                }

                aMedOptimaltSatta = MedelVärde(aMedOptimaltSatta, antalYtor) * 100;
                aMedBraSatta = MedelVärde(aMedBraSatta, antalYtor) * 100;
                aMedTotaltSatta = MedelVärde(aMedTotaltSatta, antalYtor) * 100;
            }
            catch (Exception ex)
            {
#if !DEBUG
       MessageBox.Show("Misslyckades med att beräkna gallringsdata", "Beräkna data misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private static void BeräknaMarkberedningsVärden(List<KorusDataEventArgs.Yta> aYtor, out double aOptimalt,
                                                        out double aOptimaltBra, out double aOptimaltBraDecimal)
        {
            aOptimalt = 0;
            aOptimaltBra = 0;
            aOptimaltBraDecimal = 0;

            try
            {
                foreach (var yta in aYtor)
                {
                    aOptimalt += yta.optimalt;
                    aOptimaltBra += yta.optimaltbra;
                }
                var antalYtor = aYtor.Count();
                aOptimalt = MedelVärde(aOptimalt, antalYtor) * 100;
                if (aOptimaltBra > 0 && antalYtor > 0)
                {
                    aOptimaltBraDecimal = (aOptimaltBra / antalYtor);
                }
                aOptimaltBra = MedelVärde(aOptimaltBra, antalYtor) * 100;
            }
            catch (Exception ex)
            {
#if !DEBUG
       MessageBox.Show("Misslyckades med att beräkna gallringsdata", "Beräkna data misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private static void BeräknaGallringsVärden(List<KorusDataEventArgs.Yta> aYtor, out double aMedVägbredd,
                                                   out double aMedTall, out double aMedGran, out double aMedLöv,
                                                   out double aMedCont, out double aMedSkador, out double aMedSumma)
        {
            aMedVägbredd = 0;
            aMedTall = 0;
            aMedGran = 0;
            aMedLöv = 0;
            aMedCont = 0;
            aMedSkador = 0;
            aMedSumma = 0;

            try
            {
                var antalStickvbredd = 0;

                foreach (var yta in aYtor)
                {
                    if (yta.stickvbredd > 0)
                    {
                        aMedVägbredd += yta.stickvbredd;
                        antalStickvbredd++;
                    }

                    aMedTall += yta.tall;
                    aMedGran += yta.gran;
                    aMedLöv += yta.lov;
                    aMedCont += yta.contorta;
                    aMedSkador += yta.skador;
                    aMedSumma += yta.summa;
                }
                var antalYtor = aYtor.Count();

                aMedVägbredd = MedelVärde(aMedVägbredd, antalStickvbredd);
                aMedTall = MedelVärde(aMedTall, antalYtor);
                aMedGran = MedelVärde(aMedGran, antalYtor);
                aMedLöv = MedelVärde(aMedLöv, antalYtor);
                aMedCont = MedelVärde(aMedCont, antalYtor);
                aMedSkador = MedelVärde(aMedSkador, antalYtor);
                aMedSumma = MedelVärde(aMedSumma, antalYtor);
            }
            catch (Exception ex)
            {
#if !DEBUG
     MessageBox.Show("Misslyckades med att beräkna gallringsdata", "Beräkna data misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void UppdateraDataSetSammanställningar()
        {
            try
            {
                UppdateraFilterText();
                dataSetSammanstallningar.Clear();

                var rapporter = mRapporter.FindAll(
                  aRapport =>
                  (aRapport.rapport_typ == Settings.Default.ValdTypRapport));

                foreach (var rapport in rapporter.Where(Filter))
                {
                    DateTime datum;
                    DateTime.TryParse(rapport.datum, out datum);

                    var fråga = mFrågor.Find(
                      aFråga =>
                      ((aFråga.invtyp == rapport.invtyp) && (aFråga.regionid == rapport.regionid) && (aFråga.distriktid == rapport.distriktid) &&
                       (aFråga.årtal == rapport.årtal)
                       && (aFråga.traktnr == rapport.traktnr) && (aFråga.entreprenor == rapport.entreprenor) &&
                       (aFråga.rapport_typ == Settings.Default.ValdTypRapport)));
                    var ytor = mYtor.FindAll(
                      aYta =>
                      ((aYta.invtyp == rapport.invtyp) && (aYta.regionid == rapport.regionid) && (aYta.distriktid == rapport.distriktid) &&
                       (aYta.årtal == rapport.årtal)
                       && (aYta.traktnr == rapport.traktnr) && (aYta.entreprenor == rapport.entreprenor) &&
                       (aYta.rapport_typ == Settings.Default.ValdTypRapport)));

                    switch (Settings.Default.ValdTypRapport)
                    {
                        case (int)RapportTyp.Gallring:
                            {
                                double medVägbredd, medTall, medGran, medLöv, medCont, medSkador, medSumma;
                                BeräknaGallringsVärden(ytor, out medVägbredd, out medTall, out medGran, out medLöv, out medCont,
                                                       out medSkador, out medSumma);

                                var måluppfyllelse = (rapport.malgrundyta > 0 && medSumma > 0)
                                                       ? (Math.Round((medSumma / rapport.malgrundyta) * 100, 1))
                                                       : 0.0;

                                dataSetSammanstallningar.Tables["Gallring"].Rows.Add(rapport.regionid, rapport.distriktid,
                                                                                     rapport.ursprung,
                                                                                     rapport.årtal, rapport.traktnr, rapport.traktnamn,
                                                                                     rapport.standort, rapport.entreprenor,
                                                                                     rapport.entreprenornamn,
                                                                                     rapport.datum, rapport.stickvagssystem,
                                                                                     rapport.areal,
                                                                                     medVägbredd, rapport.stickvagsavstand,
                                                                                     rapport.malgrundyta, medTall, medGran, medLöv,
                                                                                     medCont, medSkador, medSumma, måluppfyllelse,
                                                                                     fråga.fraga1, fråga.fraga2, fråga.fraga3,
                                                                                     fråga.fraga4, fråga.fraga5, fråga.fraga6,
                                                                                     fråga.fraga7, fråga.fraga8, fråga.fraga9,
                                                                                     rapport.kommentar);
                            }
                            break;
                        case (int)RapportTyp.Markberedning:
                            {
                                var mbSträcka = (rapport.gisstracka > 0 && rapport.gisareal > 0)
                                                  ? (Math.Round((rapport.gisstracka / rapport.gisareal), 1))
                                                  : 0.0;
                                double medOptimalt, medOptimaltBra, medOptimaltBraDecimal;
                                BeräknaMarkberedningsVärden(ytor, out medOptimalt, out medOptimaltBra, out medOptimaltBraDecimal);

                                double antalOptimalaBra = 0;

                                if (rapport.datainsamlingsmetod.ToLower().Equals("sträckmätning (40 m)"))
                                {
                                    if (rapport.gisstracka > 0 && rapport.gisareal > 0)
                                    {
                                        antalOptimalaBra = (Math.Round(((rapport.gisstracka / rapport.gisareal) / 40) * medOptimaltBraDecimal, 0));
                                    }
                                }
                                else
                                {
                                    //Cirkelyta (100 m2)
                                    antalOptimalaBra = (Math.Round((medOptimaltBraDecimal * 100), 0));
                                }

                                var måluppfyllelse = (antalOptimalaBra > 0 && rapport.malgrundyta > 0)
                                                       ? (Math.Round((((antalOptimalaBra) / rapport.malgrundyta) * 100), 1))
                                                       : 0.0;

                                dataSetSammanstallningar.Tables["Markberedning"].Rows.Add(rapport.regionid, rapport.distriktid,
                                                                                          rapport.ursprung,
                                                                                          rapport.årtal, rapport.traktnr,
                                                                                          rapport.traktnamn,
                                                                                          rapport.standort, rapport.entreprenor,
                                                                                          rapport.entreprenornamn,
                                                                                          rapport.datum, rapport.markberedningsmetod,
                                                                                          rapport.datainsamlingsmetod,
                                                                                          rapport.gisareal, rapport.gisstracka,
                                                                                          mbSträcka, rapport.malgrundyta, medOptimalt,
                                                                                          medOptimaltBra, måluppfyllelse, fråga.fraga1,
                                                                                          fråga.fraga2, fråga.fraga3,
                                                                                          fråga.fraga4, fråga.fraga5, fråga.fraga6,
                                                                                          fråga.fraga7, rapport.kommentar,rapport.invtyp);
                            }
                            break;
                        case (int)RapportTyp.Plantering:
                            {
                                double medOptimaltSatta, medBraSatta, medTotaltSatta, måluppfyllelse = 0,atgareal=0;
                                int utnyttjandeGrad, andelMedBraTilltryckning, andelMedBraDjup, upi, antalSattaPlantor;
                                BeräknaPlanteringsVärden(ytor, rapport.provytestorlek, out medOptimaltSatta, out medBraSatta,
                                                         out medTotaltSatta, out utnyttjandeGrad, out andelMedBraTilltryckning,
                                                         out andelMedBraDjup, out upi, out antalSattaPlantor);

                                if (antalSattaPlantor > 0 && rapport.malgrundyta > 0)
                                {
                                    måluppfyllelse =
                                      Math.Round(((antalSattaPlantor / (double)rapport.malgrundyta)) * 100, 1);
                                }
                                atgareal = Math.Round(rapport.atgareal, 1);
                                dataSetSammanstallningar.Tables["Plantering"].Rows.Add(rapport.regionid, rapport.distriktid,
                                                                                       rapport.ursprung,
                                                                                       rapport.årtal, rapport.traktnr, rapport.traktnamn,
                                                                                       rapport.standort, rapport.entreprenor,
                                                                                       rapport.entreprenornamn,
                                                                                       rapport.datum, rapport.provytestorlek,
                                                                                       medOptimaltSatta, medBraSatta, medTotaltSatta,
                                                                                       rapport.malgrundyta, utnyttjandeGrad, upi,
                                                                                       andelMedBraTilltryckning, andelMedBraDjup,
                                                                                       måluppfyllelse, fråga.fraga1, fråga.fraga2,
                                                                                       DataSetParser.GetObjIntValue(fråga.fraga3, true),
                                                                                       fråga.fraga4, fråga.fraga5,
                                                                                       DataSetParser.GetObjIntValue(fråga.fraga6, true),
                                                                                       fråga.fraga7, fråga.fraga8,
                                                                                       DataSetParser.GetObjIntValue(fråga.fraga9, true),
                                                                                       fråga.fraga10, fråga.fraga11,
                                                                                       rapport.kommentar,rapport.invtyp,atgareal);
                            }
                            break;
                        case (int)RapportTyp.Röjning:
                            {
                                double måluppfyllelse = 0, medBarrHst, atgareal = 0, medLovHst=0,rojstamha=0;
                                int huvudstammar, rT, rG, rL, rC;
                                BeräknaRöjningsVärden(ytor, out huvudstammar, out medBarrHst, out rT, out rG, out rL, out rC, out medLovHst, out rojstamha);
                                atgareal = Math.Round(rapport.atgareal, 1);
                                if (huvudstammar > 0 && rapport.malgrundyta > 0)
                                {
                                    måluppfyllelse = Math.Round((huvudstammar / (double)rapport.malgrundyta) * 100, 1);
                                }

                                dataSetSammanstallningar.Tables["Rojning"].Rows.Add(rapport.regionid, rapport.distriktid,
                                                                                    rapport.ursprung,
                                                                                    rapport.årtal, rapport.traktnr, rapport.traktnamn,
                                                                                    rapport.standort, rapport.entreprenor,
                                                                                    rapport.entreprenornamn,
                                                                                    rapport.datum, rapport.provytestorlek,
                                                                                    rapport.malgrundyta, huvudstammar, måluppfyllelse,
                                                                                    medBarrHst, rT, rG, rL, rC,
                                                                                    fråga.fraga1, fråga.fraga2, fråga.fraga3,
                                                                                    fråga.fraga4, fråga.fraga5, fråga.fraga6,
                                                                                    rapport.kommentar, rapport.invtyp, atgareal, medLovHst, rojstamha);
                            }
                            break;
                        case (int)RapportTyp.Slutavverkning:
                            {
                                dataSetSammanstallningar.Tables["Slutavverkning"].Rows.Add(rapport.regionid, rapport.distriktid,
                                                                                           rapport.ursprung,
                                                                                           rapport.årtal, rapport.traktnr,
                                                                                           rapport.traktnamn,
                                                                                           rapport.standort, rapport.entreprenor,
                                                                                           rapport.entreprenornamn,
                                                                                           rapport.datum, fråga.fraga1, fråga.fraga2,
                                                                                           fråga.fraga3, fråga.fraga4, fråga.fraga5,
                                                                                           fråga.fraga6,
                                                                                           fråga.fraga7, fråga.fraga8, fråga.fraga9,
                                                                                           fråga.fraga10, fråga.fraga11, fråga.fraga12,
                                                                                           fråga.fraga13, fråga.fraga14, fråga.fraga15,
                                                                                           fråga.fraga16, fråga.fraga17, fråga.fraga18,
                                                                                           fråga.fraga19, fråga.fraga20,
                                                                                           rapport.kommentar);
                            }
                            break;
                        case (int)RapportTyp.Biobränsle:
                            {
                                dataSetSammanstallningar.Tables["Biobransle"].Rows.Add(rapport.regionid, rapport.distriktid,
                                                                                       rapport.ursprung,
                                                                                       rapport.årtal, rapport.traktnr, rapport.traktnamn,
                                                                                       rapport.standort, rapport.entreprenor,
                                                                                       rapport.entreprenornamn,
                                                                                       rapport.datum, rapport.hygge, rapport.vagkant,
                                                                                       rapport.aker, rapport.latbilshugg,
                                                                                       rapport.grotbil, rapport.traktordragenhugg,
                                                                                       rapport.skotarburenhugg, fråga.fraga1,
                                                                                       fråga.fraga2, fråga.fraga3, fråga.fraga4,
                                                                                       fråga.fraga5, fråga.fraga6,
                                                                                       fråga.fraga7, fråga.fraga8, fråga.fraga9,
                                                                                       fråga.fraga10, fråga.fraga11, fråga.fraga12,
                                                                                       fråga.fraga13, fråga.fraga14, fråga.fraga15,
                                                                                       fråga.fraga16, fråga.fraga17, fråga.fraga18,
                                                                                       fråga.fraga19, rapport.kommentar);
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
#if !DEBUG
     MessageBox.Show("Misslyckades med att uppdatera sammanställningsdata", "Läsa data misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void UppdateraDataSetUserData()
        {
            try
            {
                UppdateraFilterText();
                dataSetUserData.Clear();

                foreach (var rapport in mRapporter.Where(Filter))
                {
                    DateTime datum;
                    DateTime.TryParse(rapport.datum, out datum);

                    dataSetUserData.Tables["UserData"].Rows.Add(rapport.regionid, rapport.distriktid, rapport.traktnr,
                                                                rapport.standort, rapport.entreprenor,
                                                                DataHelper.GetRapportTyp(rapport.rapport_typ), rapport.traktnamn,
                                                                rapport.regionnamn, rapport.distriktnamn, rapport.ursprung,
                                                                datum, rapport.markberedningsmetod, rapport.malgrundyta,
                                                                rapport.stickvagsavstand, rapport.gisstracka, rapport.gisareal,
                                                                rapport.provytestorlek, rapport.kommentar, rapport.årtal,
                                                                rapport.areal, rapport.entreprenornamn, rapport.antalvaltor,
                                                                rapport.avstand, rapport.bedomdvolym, rapport.hygge,
                                                                rapport.vagkant, rapport.aker, rapport.latbilshugg,
                                                                rapport.grotbil, rapport.traktordragenhugg,
                                                                rapport.skotarburenhugg, rapport.avlagg1norr, rapport.avlagg1ost,
                                                                rapport.avlagg2norr, rapport.avlagg2ost, rapport.avlagg3norr,
                                                                rapport.avlagg3ost, rapport.avlagg4norr, rapport.avlagg4ost,
                                                                rapport.avlagg5norr, rapport.avlagg5ost, rapport.avlagg6norr,
                                                                rapport.avlagg6ost, rapport.stickvagssystem,
                                                                rapport.datainsamlingsmetod, rapport.invtyp, DataSetParser.getInvNamn(rapport.invtyp),rapport.atgareal);
                }
            }
            catch (Exception ex)
            {
#if !DEBUG
     MessageBox.Show("Misslyckades med att läsa in tabell data", "Läsa data misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        // ReSharper disable UnusedMember.Global
        public void UppdateraRapportLista(List<KorusDataEventArgs.Data> aRapporter)
        // ReSharper restore UnusedMember.Global
        {
            mRapporter = aRapporter;
            UppdateraDataSetUserData();
        }

        // ReSharper disable UnusedMember.Global
        public void UppdateraFrågorLista(List<KorusDataEventArgs.Frågor> aFrågor)
        // ReSharper restore UnusedMember.Global
        {
            mFrågor = aFrågor;
        }

        // ReSharper disable UnusedMember.Global
        public void UppdateraYtorLista(List<KorusDataEventArgs.Yta> aYtor)
        // ReSharper restore UnusedMember.Global
        {
            mYtor = aYtor;
        }

        private void buttonUppdateraData_Click(object sender, EventArgs e)
        {
            StatusEventRefresh(this, null);
        }

        private static int GetObjIntValue(object aObjValue)
        {
            var intValue = -1;
            if (aObjValue != null && !aObjValue.ToString().Equals(string.Empty))
            {
                if (!(int.TryParse(aObjValue.ToString(), out intValue)))
                {
                    MessageBox.Show(
                      Resources.Vardet + aObjValue + Resources.ar_ett_felaktigt_nyckelvarde,
                      Resources.Felaktigt_nyckelvarde, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            return intValue;
        }

        private List<KorusDataEventArgs.Data> GetAllKORUSRapportData()
        {
            var rapporter = new List<KorusDataEventArgs.Data>();
            if (dataGridViewRapporter.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in dataGridViewRapporter.Rows)
                {
                    var data = new KorusDataEventArgs.Data
                      {
                          invtyp = GetObjIntValue(row.Cells[invtypIdDataGridViewTextBoxColumn.Name].Value),
                          regionid = GetObjIntValue(row.Cells[regionIdDataGridViewTextBoxColumn.Name].Value),
                          distriktid = GetObjIntValue(row.Cells[distriktIdDataGridViewTextBoxColumn.Name].Value),
                          traktnr = GetObjIntValue(row.Cells[traktnrDataGridViewTextBoxColumn.Name].Value),
                          standort = GetObjIntValue(row.Cells[traktdelDataGridViewTextBoxColumn.Name].Value),
                          entreprenor = GetObjIntValue(row.Cells[entreprenorDataGridViewTextBoxColumn.Name].Value),
                          rapport_typ =
                            DataHelper.GetRapportTyp(row.Cells[rapportTypDataGridViewTextBoxColumn.Name].Value),
                          årtal = GetObjIntValue(row.Cells[artalDataGridViewTextBoxColumn.Name].Value),
                          traktnamn = row.Cells[traktNamnDataGridViewTextBoxColumn.Name].Value.ToString(),
                          regionnamn = row.Cells[regionNamnDataGridViewTextBoxColumn.Name].Value.ToString(),
                          distriktnamn = row.Cells[distriktNamnDataGridViewTextBoxColumn.Name].Value.ToString(),
                          entreprenornamn = row.Cells[entreprenorNamnDataGridViewTextBoxColumn.Name].Value.ToString(),
                          ursprung = row.Cells[ursprungDataGridViewTextBoxColumn.Name].Value.ToString(),
                          datum = row.Cells[datumDataGridViewTextBoxColumn.Name].Value.ToString(),
                          markberedningsmetod =
                            row.Cells[markberedningsmetodDataGridViewTextBoxColumn.Name].Value.ToString(),
                          datainsamlingsmetod =
                            row.Cells[datainsamlingsmetodDataGridViewTextBoxColumn.Name].Value.ToString(),
                          malgrundyta = GetObjIntValue(row.Cells[malgrundytaDataGridViewTextBoxColumn.Name].Value),
                          stickvagssystem = row.Cells[stickvagssystemNamnDataGridViewTextBoxColumn.Name].Value.ToString(),
                          stickvagsavstand =
                            GetObjIntValue(row.Cells[stickvagsavstandDataGridViewTextBoxColumn.Name].Value),
                          gisstracka = GetObjIntValue(row.Cells[gISStrackaDataGridViewTextBoxColumn.Name].Value)
                          
                      };

                    float.TryParse(row.Cells[atgarealDataGridViewTextBoxColumn.Name].Value.ToString(), out data.atgareal);
                    float.TryParse(row.Cells[arealDataGridViewTextBoxColumn.Name].Value.ToString(), out data.areal);
                    float.TryParse(row.Cells[gISArealDataGridViewTextBoxColumn.Name].Value.ToString(), out data.gisareal);

                    data.provytestorlek = GetObjIntValue(row.Cells[provytestorlekDataGridViewTextBoxColumn.Name].Value);
                    data.kommentar = row.Cells[kommentarDataGridViewTextBoxColumn.Name].Value.ToString();

                    data.antalvaltor = GetObjIntValue(row.Cells[antalValtorDataGridViewTextBoxColumn.Name].Value);
                    data.avstand = GetObjIntValue(row.Cells[avstandDataGridViewTextBoxColumn.Name].Value);
                    data.bedomdvolym = GetObjIntValue(row.Cells[bedomdVolymDataGridViewTextBoxColumn.Name].Value);
                    data.hygge = GetObjIntValue(row.Cells[hyggeDataGridViewTextBoxColumn.Name].Value);
                    data.vagkant = GetObjIntValue(row.Cells[vagkantDataGridViewTextBoxColumn.Name].Value);
                    data.aker = GetObjIntValue(row.Cells[akerDataGridViewTextBoxColumn.Name].Value);

                    bool.TryParse(row.Cells[latbilshuggDataGridViewTextBoxColumn.Name].Value.ToString(), out data.latbilshugg);
                    bool.TryParse(row.Cells[grotbilDataGridViewTextBoxColumn.Name].Value.ToString(), out data.grotbil);
                    bool.TryParse(row.Cells[traktordragenhuggDataGridViewTextBoxColumn.Name].Value.ToString(),
                                  out data.traktordragenhugg);
                    bool.TryParse(row.Cells[skotarburenhuggDataGridViewTextBoxColumn.Name].Value.ToString(),
                                  out data.skotarburenhugg);

                    data.avlagg1norr = GetObjIntValue(row.Cells[avlagg1NorrDataGridViewTextBoxColumn.Name].Value);
                    data.avlagg1ost = GetObjIntValue(row.Cells[avlagg1OstDataGridViewTextBoxColumn.Name].Value);
                    data.avlagg2norr = GetObjIntValue(row.Cells[avlagg2NorrDataGridViewTextBoxColumn.Name].Value);
                    data.avlagg2ost = GetObjIntValue(row.Cells[avlagg2OstDataGridViewTextBoxColumn.Name].Value);
                    data.avlagg3norr = GetObjIntValue(row.Cells[avlagg3NorrDataGridViewTextBoxColumn.Name].Value);
                    data.avlagg3ost = GetObjIntValue(row.Cells[avlagg3OstDataGridViewTextBoxColumn.Name].Value);
                    data.avlagg4norr = GetObjIntValue(row.Cells[avlagg4NorrDataGridViewTextBoxColumn.Name].Value);
                    data.avlagg4ost = GetObjIntValue(row.Cells[avlagg4OstDataGridViewTextBoxColumn.Name].Value);
                    data.avlagg5norr = GetObjIntValue(row.Cells[avlagg5NorrDataGridViewTextBoxColumn.Name].Value);
                    data.avlagg5ost = GetObjIntValue(row.Cells[avlagg5OstDataGridViewTextBoxColumn.Name].Value);
                    data.avlagg6norr = GetObjIntValue(row.Cells[avlagg6NorrDataGridViewTextBoxColumn.Name].Value);
                    data.avlagg6ost = GetObjIntValue(row.Cells[avlagg6OstDataGridViewTextBoxColumn.Name].Value);
                    
                    rapporter.Add(data);
                }
            }
            return rapporter;
        }

        private KorusDataEventArgs GetSelectedKORUSNyckelData()
        {
            if (dataGridViewRapporter.SelectedCells.Count > 0)
            {
                var row = dataGridViewRapporter.SelectedCells[0].OwningRow;
                var data = new KorusDataEventArgs
                  {
                      data =
                        {
                            invtyp = GetObjIntValue(row.Cells[invtypIdDataGridViewTextBoxColumn.Name].Value),
                            regionid = GetObjIntValue(row.Cells[regionIdDataGridViewTextBoxColumn.Name].Value),
                            distriktid = GetObjIntValue(row.Cells[distriktIdDataGridViewTextBoxColumn.Name].Value),
                            traktnr = GetObjIntValue(row.Cells[traktnrDataGridViewTextBoxColumn.Name].Value),
                            standort = GetObjIntValue(row.Cells[traktdelDataGridViewTextBoxColumn.Name].Value),
                            entreprenor = GetObjIntValue(row.Cells[entreprenorDataGridViewTextBoxColumn.Name].Value),
                            rapport_typ =
                              DataHelper.GetRapportTyp(row.Cells[rapportTypDataGridViewTextBoxColumn.Name].Value),
                            årtal = GetObjIntValue(row.Cells[artalDataGridViewTextBoxColumn.Name].Value)
                        }
                  };

                if (data.KorrektaNyckelvärden)
                {
                    return data;
                }
            }
            return null;
        }

        // ReSharper disable MemberCanBePrivate.Global
        public void GåTillRapportData()
        // ReSharper restore MemberCanBePrivate.Global
        {
            var data = GetSelectedKORUSNyckelData();

            if (data != null)
            {
                DataEventGåTillRapport(this, data);
            }
        }

        // ReSharper disable UnusedMember.Global
        public void TaBortKORUSDataLyckades()
        // ReSharper restore UnusedMember.Global
        {
            if (dataGridViewRapporter.SelectedCells.Count > 0)
            {
                dataGridViewRapporter.Rows.RemoveAt(dataGridViewRapporter.SelectedCells[0].OwningRow.Index);
            }
        }

        // ReSharper disable MemberCanBePrivate.Global
        public void TaBortKORUSRapport()
        // ReSharper restore MemberCanBePrivate.Global
        {
            var data = GetSelectedKORUSNyckelData();

            if (data != null)
            {
                DataEventTaBortRapport(this, data);
            }
        }

        // ReSharper disable MemberCanBePrivate.Global
        public void LagraÄndradeKORUSRapporter()
        // ReSharper restore MemberCanBePrivate.Global
        {
            if (MessageBox.Show(Resources.Vill_du_uppdatera_alla_KORUS,
                                Resources.Uppdatera_alla_KORUS_rapporter, MessageBoxButtons.YesNo, MessageBoxIcon.Question) !=
                DialogResult.Yes) return;
            var rapporter = GetAllKORUSRapportData();

            if (rapporter.Count > 0)
            {
                DataEventLagraÄndradeRapporter(this, rapporter);
            }
        }

        private void buttonGåTillRapportData_Click(object sender, EventArgs e)
        {
            GåTillRapportData();
        }

        private void buttonLagraÄndratData_Click(object sender, EventArgs e)
        {
            LagraÄndradeKORUSRapporter();
        }

        private void buttonTaBortData_Click(object sender, EventArgs e)
        {
            TaBortKORUSRapport();
        }

        private void dataGridViewRapporter_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            TaBortKORUSRapport();
            //avbryt alltid delete av raden i GUI, det sköts istället från ett event när delete lyckades i databasen.
            e.Cancel = true;
        }

        private void dataGridViewRapporter_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                if (dataGridViewRapporter == null || e.FormattedValue == null ||
                    dataGridViewRapporter.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly ||
                    !dataGridViewRapporter.CurrentCell.IsInEditMode) return;

                // Don't try to validate the 'new row' until finished 
                // editing since there
                // is not any point in validating its initial value.
                if (dataGridViewRapporter.Rows[e.RowIndex].IsNewRow)
                {
                    return;
                }

                dataGridViewRapporter.Rows[e.RowIndex].ErrorText = "";
                dataGridViewRapporter.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "";

                var cname = dataGridViewRapporter.CurrentCell.OwningColumn.Name;

                if (cname.Equals(gISArealDataGridViewTextBoxColumn.Name) || cname.Equals(arealDataGridViewTextBoxColumn.Name))
                {
                    float cellDoubleValue;
                    if ((!float.TryParse(e.FormattedValue.ToString(), out cellDoubleValue) || cellDoubleValue < 0.0f))
                    {
                        MessageBox.Show(Resources.EndastPositiva, Resources.FelaktigtVarde, MessageBoxButtons.OK,
                                        MessageBoxIcon.Stop);
                    }
                }
                else if (cname.Equals(malgrundytaDataGridViewTextBoxColumn.Name) ||
                         cname.Equals(stickvagsavstandDataGridViewTextBoxColumn.Name) ||
                         cname.Equals(gISStrackaDataGridViewTextBoxColumn.Name) ||
                         cname.Equals(provytestorlekDataGridViewTextBoxColumn.Name))
                {
                    int cellIntValue;
                    if ((!int.TryParse(e.FormattedValue.ToString(), out cellIntValue) || cellIntValue < 0))
                    {
                        MessageBox.Show(Resources.EndastPositiva, Resources.FelaktigtVarde, MessageBoxButtons.OK,
                                        MessageBoxIcon.Stop);
                    }
                }
            }
            catch (Exception ex)
            {
#if !DEBUG
       MessageBox.Show("Det gick inte att validera värdet på cellen.", "Fel vid cell validering", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void dataGridViewRapporter_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if (!((e.Exception) is FormatException)) return;
            var view = (DataGridView)sender;
            view.CurrentCell.Value = 0;
            e.ThrowException = false;
            e.Cancel = false;
        }

        private void buttonFiltreraData_Click(object sender, EventArgs e)
        {
            var filterDialog = new FormFiltreraRapporter();
            filterDialog.InitData(mRapporter);
            if (filterDialog.ShowDialog(this) == DialogResult.OK)
            {
                UppdateraDataSetUserData();
            }
        }

        private void dataGridViewRapporter_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            FormatRapportTyp(e);
            FormatNamn(e, "RegionNamn");
            FormatNamn(e, "DistriktNamn");
            FormatEntreprenör(e);
            FormatDatum(e);
        }

        private void FormatDatum(DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                if (dataGridViewRapporter == null || e.ColumnIndex < 0 || e.ColumnIndex >= dataGridViewRapporter.Columns.Count)
                    return;

                if (dataGridViewRapporter.Columns[e.ColumnIndex].DataPropertyName != "Datum") return;

                if (e.Value == null || e.Value.ToString().Trim().Equals(string.Empty)) return;

                var stringValue = e.Value.ToString();
                if (!stringValue.Equals(string.Empty) && stringValue.Contains(" "))
                {
                    e.Value = stringValue.Substring(0, stringValue.IndexOf(" ", StringComparison.Ordinal));
                }
            }
            catch (Exception ex)
            {
#if !DEBUG
     MessageBox.Show("Misslyckades med att formattera datum", "Formatera datum misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void FormatEntreprenör(DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                if (dataGridViewRapporter == null || e.ColumnIndex < 0 || e.ColumnIndex >= dataGridViewRapporter.Columns.Count)
                    return;

                if (dataGridViewRapporter.Columns[e.ColumnIndex].DataPropertyName != "Entreprenor") return;

                if (e.Value == null || e.Value.ToString().Trim().Equals(string.Empty)) return;

                var stringValue = e.Value.ToString();
                var regionid =
                  dataGridViewRapporter.Rows[e.RowIndex].Cells[regionIdDataGridViewTextBoxColumn.Name].Value.ToString();
                e.Value = regionid + "-" + stringValue;
            }
            catch (Exception ex)
            {
#if !DEBUG
     MessageBox.Show("Misslyckades med att formattera region/distrikt namn", "Formattera distrikt/region misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void FormatNamn(DataGridViewCellFormattingEventArgs e, string aNamn)
        {
            try
            {
                if (dataGridViewRapporter == null || e.ColumnIndex < 0 || e.ColumnIndex >= dataGridViewRapporter.Columns.Count)
                    return;

                if (dataGridViewRapporter.Columns[e.ColumnIndex].DataPropertyName != aNamn) return;

                if (e.Value == null || e.Value.ToString().Trim().Equals(string.Empty)) return;

                var stringValue = e.Value.ToString();
                if (stringValue.Equals(string.Empty) || !stringValue.Contains(" - ")) return;

                var startPos = stringValue.IndexOf(" - ", StringComparison.Ordinal) + 3;

                e.Value = stringValue.Substring(startPos, stringValue.Length - startPos);
            }
            catch (Exception ex)
            {
#if !DEBUG
     MessageBox.Show("Misslyckades med att formattera region/distrikt namn", "Formattera distrikt/region misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void FormatRapportTyp(DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                if (dataGridViewRapporter == null || e.ColumnIndex < 0 || e.ColumnIndex >= dataGridViewRapporter.Columns.Count)
                    return;

                if (dataGridViewRapporter.Columns[e.ColumnIndex].DataPropertyName != "RapportTyp") return;

                if (e.Value == null || e.Value.ToString().Trim().Equals(string.Empty)) return;

                var stringValue = e.Value.ToString();
                var rapportTyp = DataHelper.GetRapportTyp(stringValue);
                var row = dataGridViewRapporter.Rows[e.RowIndex];
                switch (rapportTyp)
                {
                    case (int)RapportTyp.Markberedning:
                        e.CellStyle.BackColor = Color.LightBlue;
                        SetKolumnStatus(true, false, false, true, true, false, true, false, false, row);
                        e.FormattingApplied = true;
                        break;
                    case (int)RapportTyp.Plantering:
                        e.CellStyle.BackColor = Color.LightGreen;
                        SetKolumnStatus(false, false, false, false, false, true, true, false, true, row);
                        e.FormattingApplied = true;
                        break;
                    case (int)RapportTyp.Gallring:
                        e.CellStyle.BackColor = Color.LightSalmon;
                        SetKolumnStatus(false, true, true, false, false, false, true, false, false, row);
                        e.FormattingApplied = true;
                        break;
                    case (int)RapportTyp.Röjning:
                        e.CellStyle.BackColor = Color.LightCyan;
                        SetKolumnStatus(false, false, false, false, false, true, true, false, true, row);
                        e.FormattingApplied = true;
                        break;
                    case (int)RapportTyp.Biobränsle:
                        e.CellStyle.BackColor = Color.LightCoral;
                        SetKolumnStatus(false, false, false, false, false, false, false, true, false, row);
                        e.FormattingApplied = true;
                        break;
                    case (int)RapportTyp.Slutavverkning:
                        e.CellStyle.BackColor = Color.LightGray;
                        SetKolumnStatus(false, false, false, false, false, false, false, false, false, row);
                        e.FormattingApplied = true;
                        break;
                }
            }
            catch (Exception ex)
            {
#if !DEBUG
     MessageBox.Show("Misslyckades med att formattera rapporttypen", "Formattera rapporttyp misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private KorusDataEventArgs UppdateraValdFiltering(int aRapportFormat)
        {
            if (dataSetValdFiltrering.Tables["TableVal"].Rows.Count == 0)
            {
                dataSetValdFiltrering.Tables["TableVal"].Rows.Add(Resources.Alla, Resources.Alla, Resources.Alla, Resources.Alla,
                                                                  Resources.Alla, Resources.Alla, Resources.Alla, Resources.Alla,
                                                                  Resources.Alla,
                                                                  0,0,Settings.Default.MinusEtt,
                                                                  Resources.Alla);
            }

            DataSetParser.SetDsInt("TableVal", "ValRapportFormat", aRapportFormat, dataSetValdFiltrering);
            DataSetParser.SetDsInt("TableVal", "ValRapportTyp", Settings.Default.ValdTypRapport, dataSetValdFiltrering);

            DataSetParser.SetDsInt("TableVal", "ValInvTypId",  Settings.Default.FiltreraRapportInvTypId, dataSetValdFiltrering);

            DataSetParser.SetDsStr("TableVal", "ValInvTypNamn",
                       (!Settings.Default.FiltreraRapportInvTypNamn.Equals(string.Empty))
                         ? Settings.Default.FiltreraRapportInvTypNamn
                         : Resources.Alla, dataSetValdFiltrering);


            DataSetParser.SetDsStr("TableVal", "ValRegion",
                                   (!Settings.Default.FiltreraRapportRegionNamn.Equals(string.Empty))
                                     ? Settings.Default.FiltreraRapportRegionNamn
                                     : Resources.Alla, dataSetValdFiltrering);
            DataSetParser.SetDsStr("TableVal", "ValDistrikt",
                                   (!Settings.Default.FiltreraRapportDistriktNamn.Equals(string.Empty))
                                     ? Settings.Default.FiltreraRapportDistriktNamn
                                     : Resources.Alla, dataSetValdFiltrering);
            DataSetParser.SetDsStr("TableVal", "ValTraktNr",
                                   (Settings.Default.FiltreraRapportTraktNr > 0)
                                     ? Settings.Default.FiltreraRapportTraktNr.ToString(CultureInfo.InvariantCulture)
                                     : Resources.Alla, dataSetValdFiltrering);
            DataSetParser.SetDsStr("TableVal", "ValTrakt",
                                   (!Settings.Default.FiltreraRapportTraktNamn.Equals(string.Empty))
                                     ? Settings.Default.FiltreraRapportTraktNamn
                                     : Resources.Alla, dataSetValdFiltrering);
            DataSetParser.SetDsStr("TableVal", "ValLagEntrNr",
                                   (Settings.Default.FiltreraRapportEntreprenor > 0)
                                     ? Settings.Default.FiltreraRapportEntreprenor.ToString(CultureInfo.InvariantCulture)
                                     : Resources.Alla, dataSetValdFiltrering);
            DataSetParser.SetDsStr("TableVal", "ValLagEntr",
                                   (!Settings.Default.FiltreraRapportEntreprenorNamn.Equals(string.Empty))
                                     ? Settings.Default.FiltreraRapportEntreprenorNamn
                                     : Resources.Alla, dataSetValdFiltrering);
            DataSetParser.SetDsStr("TableVal", "ValUrsprung",
                                   (!Settings.Default.FiltreraRapportUrsprung.Equals(string.Empty))
                                     ? Settings.Default.FiltreraRapportUrsprung
                                     : Resources.Alla, dataSetValdFiltrering);
            DataSetParser.SetDsStr("TableVal", "ValStandort",
                                   (Settings.Default.FiltreraRapportStandort > 0)
                                     ? Settings.Default.FiltreraRapportStandort.ToString(CultureInfo.InvariantCulture)
                                     : Resources.Alla, dataSetValdFiltrering);
            DataSetParser.SetDsStr("TableVal", "ValAr",
                                   (Settings.Default.FiltreraRapportArtal > 0)
                                     ? Settings.Default.FiltreraRapportArtal.ToString(CultureInfo.InvariantCulture)
                                     : Resources.Alla, dataSetValdFiltrering);

            return new KorusDataEventArgs(Settings.Default.ValdTypRapport, Settings.Default.FiltreraRapportRegionId,
                                          Settings.Default.FiltreraRapportDistriktId, Settings.Default.FiltreraRapportTraktNr,
                                          Settings.Default.FiltreraRapportEntreprenor,
                                          Settings.Default.FiltreraRapportUrsprung, Settings.Default.FiltreraRapportStandort,
                                          Settings.Default.FiltreraRapportArtal, Settings.Default.FiltreraRapportInvTypId);
        }

        private void JusteraRapportStorlek(RapportFormat aRapportFormat)
        {
            try
            {
                var page = (ReportPage)reportExporteraSammanstallning.Pages[0];
                var paperWidth = 297.0f; //AdobePdf

                if (aRapportFormat == RapportFormat.Excel2007Export)
                {
                    switch (Settings.Default.ValdTypRapport)
                    {
                        case (int)RapportTyp.Biobränsle:
                            paperWidth = 1630.0f;
                            break;
                        case (int)RapportTyp.Gallring:
                            paperWidth = 1150.0f;
                            break;
                        case (int)RapportTyp.Markberedning:
                            paperWidth = 1050.0f;
                            break;
                        case (int)RapportTyp.Plantering:
                            paperWidth = 820.0f;
                            break;
                        case (int)RapportTyp.Röjning:
                            paperWidth = 1020.0f;
                            break;
                        case (int)RapportTyp.Slutavverkning:
                            paperWidth = 2100.0f;
                            break;
                    }
                }

                if (Math.Abs(page.PaperWidth - paperWidth) <= 0.1) return;

                page.PaperWidth = paperWidth;
                reportExporteraSammanstallning.Pages.RemoveAt(0);
                reportExporteraSammanstallning.Pages.Add(page);
            }
            catch (Exception ex)
            {
#if !DEBUG
        MessageBox.Show(Resources.Justering_av_rapport_storlek_misslyckades);
#else
                MessageBox.Show(Resources.Justering_av_rapport_storlek_misslyckades + ex);
#endif
            }
        }

        private void VisaRapport(RapportFormat aRapportFormat)
        {
            try
            {
                var valdFiltrering = UppdateraValdFiltering((int)aRapportFormat);
                HämtaFrågorFörValtFilter(this, valdFiltrering);
                HämtaYtorFörValtFilter(this, valdFiltrering);
                UppdateraDataSetSammanställningar();

                JusteraRapportStorlek(aRapportFormat);
                reportExporteraSammanstallning.Show();
            }
            catch (Exception ex)
            {
#if !DEBUG
        MessageBox.Show(Resources.ForhandgranskningFailed);
#else
                MessageBox.Show(Resources.ForhandgranskningFailed + ex);
#endif
            }
        }

        private static void SättCellStatus(DataGridViewRow aRow, string aKolumnNamn, bool aReadOnly)
        {
            aRow.Cells[aKolumnNamn].ReadOnly = aReadOnly;
            aRow.Cells[aKolumnNamn].Style.BackColor = aReadOnly ? Color.LemonChiffon : Color.White;
        }

        private void SetKolumnStatus(bool aMarkberedningsmetod, bool aAreal, bool aStickVägsavstånd, bool aGISSträcka,
                                     bool aGISAreal, bool aProvytestorlek, bool aMålgrundyta, bool aBiobränsle, bool aAtgAreal,
                                     DataGridViewRow aRow)
        {
            if (aRow != null)
            {
                SättCellStatus(aRow, markberedningsmetodDataGridViewTextBoxColumn.Name, !aMarkberedningsmetod);
                SättCellStatus(aRow, datainsamlingsmetodDataGridViewTextBoxColumn.Name, !aMarkberedningsmetod);
                SättCellStatus(aRow, arealDataGridViewTextBoxColumn.Name, !aAreal);
                SättCellStatus(aRow, stickvagssystemNamnDataGridViewTextBoxColumn.Name, !aStickVägsavstånd);
                SättCellStatus(aRow, stickvagsavstandDataGridViewTextBoxColumn.Name, !aStickVägsavstånd);
                SättCellStatus(aRow, gISStrackaDataGridViewTextBoxColumn.Name, !aGISSträcka);
                SättCellStatus(aRow, gISArealDataGridViewTextBoxColumn.Name, !aGISAreal);
                SättCellStatus(aRow, provytestorlekDataGridViewTextBoxColumn.Name, !aProvytestorlek);
                SättCellStatus(aRow, malgrundytaDataGridViewTextBoxColumn.Name, !aMålgrundyta);
                SättCellStatus(aRow, antalValtorDataGridViewTextBoxColumn.Name, !aBiobränsle);
                SättCellStatus(aRow, avstandDataGridViewTextBoxColumn.Name, !aBiobränsle);
                SättCellStatus(aRow, bedomdVolymDataGridViewTextBoxColumn.Name, !aBiobränsle);
                SättCellStatus(aRow, hyggeDataGridViewTextBoxColumn.Name, !aBiobränsle);
                SättCellStatus(aRow, vagkantDataGridViewTextBoxColumn.Name, !aBiobränsle);
                SättCellStatus(aRow, akerDataGridViewTextBoxColumn.Name, !aBiobränsle);
                SättCellStatus(aRow, latbilshuggDataGridViewTextBoxColumn.Name, !aBiobränsle);
                SättCellStatus(aRow, grotbilDataGridViewTextBoxColumn.Name, !aBiobränsle);
                SättCellStatus(aRow, traktordragenhuggDataGridViewTextBoxColumn.Name, !aBiobränsle);
                SättCellStatus(aRow, skotarburenhuggDataGridViewTextBoxColumn.Name, !aBiobränsle);
                SättCellStatus(aRow, avlagg1NorrDataGridViewTextBoxColumn.Name, !aBiobränsle);
                SättCellStatus(aRow, avlagg1OstDataGridViewTextBoxColumn.Name, !aBiobränsle);
                SättCellStatus(aRow, avlagg2NorrDataGridViewTextBoxColumn.Name, !aBiobränsle);
                SättCellStatus(aRow, avlagg2OstDataGridViewTextBoxColumn.Name, !aBiobränsle);
                SättCellStatus(aRow, avlagg3NorrDataGridViewTextBoxColumn.Name, !aBiobränsle);
                SättCellStatus(aRow, avlagg3OstDataGridViewTextBoxColumn.Name, !aBiobränsle);
                SättCellStatus(aRow, avlagg4NorrDataGridViewTextBoxColumn.Name, !aBiobränsle);
                SättCellStatus(aRow, avlagg4OstDataGridViewTextBoxColumn.Name, !aBiobränsle);
                SättCellStatus(aRow, avlagg5NorrDataGridViewTextBoxColumn.Name, !aBiobränsle);
                SättCellStatus(aRow, avlagg5OstDataGridViewTextBoxColumn.Name, !aBiobränsle);
                SättCellStatus(aRow, avlagg6NorrDataGridViewTextBoxColumn.Name, !aBiobränsle);
                SättCellStatus(aRow, avlagg6OstDataGridViewTextBoxColumn.Name, !aBiobränsle);
                SättCellStatus(aRow, atgarealDataGridViewTextBoxColumn.Name, !aAtgAreal);
            }
            else
            {
                markberedningsmetodDataGridViewTextBoxColumn.Visible = aMarkberedningsmetod;
                datainsamlingsmetodDataGridViewTextBoxColumn.Visible = aMarkberedningsmetod;
                arealDataGridViewTextBoxColumn.Visible = aAreal;
                stickvagssystemNamnDataGridViewTextBoxColumn.Visible = aStickVägsavstånd;
                stickvagsavstandDataGridViewTextBoxColumn.Visible = aStickVägsavstånd;
                gISStrackaDataGridViewTextBoxColumn.Visible = aGISSträcka;
                gISArealDataGridViewTextBoxColumn.Visible = aGISAreal;
                provytestorlekDataGridViewTextBoxColumn.Visible = aProvytestorlek;
                malgrundytaDataGridViewTextBoxColumn.Visible = aMålgrundyta;
                antalValtorDataGridViewTextBoxColumn.Visible = aBiobränsle;
                avstandDataGridViewTextBoxColumn.Visible = aBiobränsle;
                bedomdVolymDataGridViewTextBoxColumn.Visible = aBiobränsle;
                hyggeDataGridViewTextBoxColumn.Visible = aBiobränsle;
                vagkantDataGridViewTextBoxColumn.Visible = aBiobränsle;
                akerDataGridViewTextBoxColumn.Visible = aBiobränsle;
                latbilshuggDataGridViewTextBoxColumn.Visible = aBiobränsle;
                grotbilDataGridViewTextBoxColumn.Visible = aBiobränsle;
                traktordragenhuggDataGridViewTextBoxColumn.Visible = aBiobränsle;
                skotarburenhuggDataGridViewTextBoxColumn.Visible = aBiobränsle;
                avlagg1NorrDataGridViewTextBoxColumn.Visible = aBiobränsle;
                avlagg1OstDataGridViewTextBoxColumn.Visible = aBiobränsle;
                avlagg2NorrDataGridViewTextBoxColumn.Visible = aBiobränsle;
                avlagg2OstDataGridViewTextBoxColumn.Visible = aBiobränsle;
                avlagg3NorrDataGridViewTextBoxColumn.Visible = aBiobränsle;
                avlagg3OstDataGridViewTextBoxColumn.Visible = aBiobränsle;
                avlagg4NorrDataGridViewTextBoxColumn.Visible = aBiobränsle;
                avlagg4OstDataGridViewTextBoxColumn.Visible = aBiobränsle;
                avlagg5NorrDataGridViewTextBoxColumn.Visible = aBiobränsle;
                avlagg5OstDataGridViewTextBoxColumn.Visible = aBiobränsle;
                avlagg6NorrDataGridViewTextBoxColumn.Visible = aBiobränsle;
                avlagg6OstDataGridViewTextBoxColumn.Visible = aBiobränsle;
                atgarealDataGridViewTextBoxColumn.Visible = aAtgAreal;
            }
        }

        private void radioButtonMarkberedning_CheckedChanged(object sender, EventArgs e)
        {
            var rButton = sender as RadioButton;
            if (rButton == null) return;
            if (!rButton.Checked) return;
            //Aktiv, filtrera tabeller.
            var dataView = dataSetUserData.Tables["UserData"].DefaultView;
            dataView.RowFilter = "RapportTyp = '" + RapportTyp.Markberedning + "'";
            dataGridViewRapporter.DataSource = dataView;
            SetKolumnStatus(true, false, false, true, true, false, true, false, false, null);
            Settings.Default.ValdTypRapport = (int)RapportTyp.Markberedning;
        }

        private void radioButtonPlantering_CheckedChanged(object sender, EventArgs e)
        {
            var rButton = sender as RadioButton;
            if (rButton == null) return;
            if (!rButton.Checked) return;
            //Aktiv, filtrera tabeller.
            var dataView = dataSetUserData.Tables["UserData"].DefaultView;
            dataView.RowFilter = "RapportTyp = '" + RapportTyp.Plantering + "'";
            dataGridViewRapporter.DataSource = dataView;
            SetKolumnStatus(false, false, false, false, false, true, true, false, true, null);
            Settings.Default.ValdTypRapport = (int)RapportTyp.Plantering;
        }

        private void radioButtonRöjning_CheckedChanged(object sender, EventArgs e)
        {
            var rButton = sender as RadioButton;
            if (rButton == null) return;
            if (!rButton.Checked) return;
            //Aktiv, filtrera tabeller.
            var dataView = dataSetUserData.Tables["UserData"].DefaultView;
            dataView.RowFilter = "RapportTyp = '" + RapportTyp.Röjning + "'";
            dataGridViewRapporter.DataSource = dataView;
            SetKolumnStatus(false, false, false, false, false, true, true, false, true, null);
            Settings.Default.ValdTypRapport = (int)RapportTyp.Röjning;
        }

        private void radioButtonGallring_CheckedChanged(object sender, EventArgs e)
        {
            var rButton = sender as RadioButton;
            if (rButton == null) return;
            if (!rButton.Checked) return;
            //Aktiv, filtrera tabeller. 
            var dataView = dataSetUserData.Tables["UserData"].DefaultView;
            dataView.RowFilter = "RapportTyp = '" + RapportTyp.Gallring + "'";
            dataGridViewRapporter.DataSource = dataView;
            SetKolumnStatus(false, true, true, false, false, false, true, false, false, null);
            Settings.Default.ValdTypRapport = (int)RapportTyp.Gallring;
        }

        private void radioButtonBiobränsle_CheckedChanged(object sender, EventArgs e)
        {
            var rButton = sender as RadioButton;
            if (rButton == null) return;
            if (!rButton.Checked) return;
            //Aktiv, filtrera tabeller.
            var dataView = dataSetUserData.Tables["UserData"].DefaultView;
            dataView.RowFilter = "RapportTyp = '" + RapportTyp.Biobränsle + "'";
            dataGridViewRapporter.DataSource = dataView;
            SetKolumnStatus(false, false, false, false, false, false, false, true, false, null);
            Settings.Default.ValdTypRapport = (int)RapportTyp.Biobränsle;
        }

        private void radioButtonSlutavverkning_CheckedChanged(object sender, EventArgs e)
        {
            var rButton = sender as RadioButton;
            if (rButton == null) return;
            if (!rButton.Checked) return;
            //Aktiv, filtrera tabeller.
            var dataView = dataSetUserData.Tables["UserData"].DefaultView;
            dataView.RowFilter = "RapportTyp = '" + RapportTyp.Slutavverkning + "'";
            dataGridViewRapporter.DataSource = dataView;
            SetKolumnStatus(false, false, false, false, false, false, false, false, false, null);
            Settings.Default.ValdTypRapport = (int)RapportTyp.Slutavverkning;
        }

        private void FormListaRapporter_Load(object sender, EventArgs e)
        {
            switch (Settings.Default.ValdTypRapport)
            {
                case (int)RapportTyp.Gallring:
                    radioButtonGallring.Checked = true;
                    break;
                case (int)RapportTyp.Markberedning:
                    radioButtonMarkberedning.Checked = true;
                    break;
                case (int)RapportTyp.Plantering:
                    radioButtonPlantering1.Checked = true;
                    break;
                case (int)RapportTyp.Röjning:
                    radioButtonRöjning.Checked = true;
                    break;
                case (int)RapportTyp.Biobränsle:
                    radioButtonBiobränsle.Checked = true;
                    break;
                case (int)RapportTyp.Slutavverkning:
                    radioButtonSlutavverkning.Checked = true;
                    break;
                default:
                    radioButtonMarkberedning.Checked = true;
                    break;
            }
        }

        private void dataGridViewRapporter_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dataGridViewRapporter.CurrentCell.Value == null) return;

                var cname = dataGridViewRapporter.CurrentCell.OwningColumn.Name;

                if (cname.Equals(gISArealDataGridViewTextBoxColumn.Name) || cname.Equals(arealDataGridViewTextBoxColumn.Name))
                {
                    float cellDoubleValue;
                    if ((!float.TryParse(dataGridViewRapporter.CurrentCell.Value.ToString(), out cellDoubleValue) ||
                         cellDoubleValue < 0.0f))
                    {
                        dataGridViewRapporter.CurrentCell.Value = 0;
                    }
                }
                else if (cname.Equals(malgrundytaDataGridViewTextBoxColumn.Name) ||
                         cname.Equals(stickvagsavstandDataGridViewTextBoxColumn.Name) ||
                         cname.Equals(gISStrackaDataGridViewTextBoxColumn.Name) ||
                         cname.Equals(provytestorlekDataGridViewTextBoxColumn.Name))
                {
                    int cellIntValue;
                    if ((!int.TryParse(dataGridViewRapporter.CurrentCell.Value.ToString(), out cellIntValue) ||
                         cellIntValue < 0))
                    {
                        dataGridViewRapporter.CurrentCell.Value = 0;
                    }
                }
            }
            catch (Exception ex)
            {
#if !DEBUG
       MessageBox.Show("Det gick inte att kontrollera värdet på cellen.", "Fel vid cell editering", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private string GetSammanstallningReportFileName()
        {
            var filename = DataHelper.GetRapportTyp(Settings.Default.ValdTypRapport); //Rapport typ

            var invtypid = DataSetParser.GetDsStr("TableVal", "ValInvTypId", 0, dataSetValdFiltrering);

            if (!(invtypid.Equals(string.Empty) || invtypid.Equals("-1") || invtypid.ToLower().Equals("alla")))
            {
                filename += Resources.FileSep + invtypid;
            }
            //Region id
            var regionid = DataSetParser.GetDsStr("TableVal", "ValRegion", 0, dataSetValdFiltrering);

            if (!(regionid.Equals(string.Empty) || regionid.Equals("-1") || regionid.ToLower().Equals("alla")))
            {
                filename += Resources.FileSep + regionid;
            }

            //Distrikt id
            var distriktid = DataSetParser.GetDsStr("TableVal", "ValDistrikt", 0, dataSetValdFiltrering);

            if (!(distriktid.Equals(string.Empty) || distriktid.Equals("-1") || distriktid.ToLower().Equals("alla")))
            {
                filename += Resources.FileSep + distriktid;
            }

            var ar = DataSetParser.GetDsStr("TableVal", "ValAr", 0, dataSetValdFiltrering);

            if (!(ar.Equals(string.Empty) || ar.Equals("-1") || ar.ToLower().Equals("alla")))
            {
                filename += Resources.FileSep + ar;
            }

            //Traktnr
            var traktnr = DataSetParser.GetDsStr("TableVal", "ValTraktNr", 0, dataSetValdFiltrering);

            if (!(traktnr.Equals(string.Empty) || traktnr.Equals("-1") || traktnr.ToLower().Equals("alla")))
            {
                filename += Resources.FileSep + traktnr;
            }

            //Ståndort
            var ståndort = DataSetParser.GetDsStr("TableVal", "ValStandort", 0, dataSetValdFiltrering);

            if (!(ståndort.Equals(string.Empty) || ståndort.Equals("-1") || ståndort.ToLower().Equals("alla")))
            {
                filename += Resources.FileSep + ståndort;
            }

            //Entreprenörnr
            var entreprenör = DataSetParser.GetDsStr("TableVal", "ValLagEntrNr", 0, dataSetValdFiltrering);

            if (!(entreprenör.Equals(string.Empty) || entreprenör.Equals("-1") || entreprenör.ToLower().Equals("alla")))
            {
                filename += Resources.FileSep + entreprenör;
            }

            return filename;
        }


        /// <summary>
        ///   Shows the save as dialog to let the user be able to save the report with a custom name.
        /// </summary>
        /// <returns> The file path name. </returns>
        private string ShowSaveAsDialog(string aSelectedPath, string aFileSuffix, string aFilter)
        {
            // folderBrowserSaveReportDialog.RootFolder = Environment.SpecialFolder.MyComputer;

            saveFileDialogExporteraSammanstallningsRapport.InitialDirectory = aSelectedPath;

            var fileName = GetSammanstallningReportFileName();

            if (fileName != null)
            {
                saveFileDialogExporteraSammanstallningsRapport.FileName = fileName + aFileSuffix;
                saveFileDialogExporteraSammanstallningsRapport.Title = Resources.Spara_rapport;
                saveFileDialogExporteraSammanstallningsRapport.Filter = aFilter;

                if (saveFileDialogExporteraSammanstallningsRapport.ShowDialog() == DialogResult.OK)
                {
                    return saveFileDialogExporteraSammanstallningsRapport.FileName;
                }
            }
            return null;
        }

        private void ExporteraExcel2007()
        {
            try
            {
                var valdFiltrering = UppdateraValdFiltering((int)RapportFormat.Excel2007Export);
                HämtaFrågorFörValtFilter(this, valdFiltrering);
                HämtaYtorFörValtFilter(this, valdFiltrering);
                UppdateraDataSetSammanställningar();
                JusteraRapportStorlek(RapportFormat.Excel2007Export);

                var excel2007SammanställningPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                                                   Settings.Default.ReportsPath + Settings.Default.SammanstallningExcel2007Path;

                if (!Directory.Exists(excel2007SammanställningPath))
                {
                    //Report dir does not exists, create it.
                    Directory.CreateDirectory(excel2007SammanställningPath);
                }

                //kolla först så användaren har skrivit in det datat som krävs för ett giltigt filnamn.        
                reportExporteraSammanstallning.Prepare();
                var export = new Excel2007Export();
                // Visa export alternativ
                if (!export.ShowDialog()) return;

                var filePath = ShowSaveAsDialog(excel2007SammanställningPath, Resources.Excel_suffix, "Excel 2007|*.xlsx");
                if (filePath == null) return;
                reportExporteraSammanstallning.Export(export, filePath);
                MessageBox.Show(Resources.ExportRapport + filePath
                                , Resources.Exportering_lyckades, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
#if !DEBUG
          MessageBox.Show("Misslyckades med att exportera ut data till formatet: Excel 2007");
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void ExporteraAdobePDF()
        {
            try
            {
                var valdFiltrering = UppdateraValdFiltering((int)RapportFormat.AdobePdf);
                HämtaFrågorFörValtFilter(this, valdFiltrering);
                HämtaYtorFörValtFilter(this, valdFiltrering);
                UppdateraDataSetSammanställningar();


                JusteraRapportStorlek(RapportFormat.AdobePdf);

                var pdfSammanställningPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                                             Settings.Default.ReportsPath + Settings.Default.SammanstallningAdobePdf;

                if (!Directory.Exists(pdfSammanställningPath))
                {
                    //Report dir does not exists, create it.
                    Directory.CreateDirectory(pdfSammanställningPath);
                }

                //kolla först så användaren har skrivit in det datat som krävs för ett giltigt filnamn.        
                reportExporteraSammanstallning.Prepare();
                var export = new PDFExport();
                // Visa export alternativ
                if (!export.ShowDialog()) return;

                var filePath = ShowSaveAsDialog(pdfSammanställningPath, Resources.Pdf_suffix, "Adobe PDF filer|*.pdf");
                if (filePath == null) return;
                reportExporteraSammanstallning.Export(export, filePath);
                MessageBox.Show(Resources.ExportRapport + filePath
                                , Resources.Exportering_lyckades, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
#if !DEBUG
          MessageBox.Show("Misslyckades med att exportera ut data till formatet: Pdf");
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void buttonExportera_Click(object sender, EventArgs e)
        {
            var button = sender as Button;
            if (button == null) return;

            toolStripMenuItemPdf.Visible = (Settings.Default.ValdTypRapport != (int)RapportTyp.Biobränsle &&
                                            Settings.Default.ValdTypRapport != (int)RapportTyp.Slutavverkning);

            contextMenuStripExportera.Show(button,
                                           toolStripMenuItemPdf.Visible
                                             ? new Point(button.Width, -button.Height)
                                             : new Point(button.Width, -button.Height + toolStripMenuItemPdf.Height));
        }

        private void toolStripMenuItemExcel2007_Click(object sender, EventArgs e)
        {
            ExporteraExcel2007();
        }

        private void buttonForhandsgranska_Click(object sender, EventArgs e)
        {
            var button = sender as Button;
            if (button == null) return;

            toolStripMenuItemForhandsgranskaPdf.Visible = (Settings.Default.ValdTypRapport != (int)RapportTyp.Biobränsle &&
                                                           Settings.Default.ValdTypRapport != (int)RapportTyp.Slutavverkning);

            contextMenuStripForhandsgranska.Show(button,
                                                 toolStripMenuItemForhandsgranskaPdf.Visible
                                                   ? new Point(button.Width, -button.Height)
                                                   : new Point(button.Width, -button.Height + toolStripMenuItemPdf.Height));
        }

        private void toolStripMenuItemForhandsgranskaxcel2007_Click(object sender, EventArgs e)
        {
            VisaRapport(RapportFormat.Excel2007Export);
        }

        private void toolStripMenuItemPdf_Click(object sender, EventArgs e)
        {
            ExporteraAdobePDF();
        }

        private void toolStripMenuItemForhandsgranskaPdf_Click(object sender, EventArgs e)
        {
            VisaRapport(RapportFormat.AdobePdf);
        }
    }
}