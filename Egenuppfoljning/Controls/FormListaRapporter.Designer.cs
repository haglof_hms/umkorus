﻿namespace Egenuppfoljning.Controls
{
  partial class FormListaRapporter
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.components = new System.ComponentModel.Container();
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormListaRapporter));
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle76 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle99 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle100 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle77 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle78 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle79 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle80 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle81 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle82 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle83 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle84 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle85 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle86 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle87 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle88 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle89 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle90 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle91 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle92 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle93 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle94 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle95 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle96 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle97 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle98 = new System.Windows.Forms.DataGridViewCellStyle();
        FastReport.Design.DesignerSettings designerSettings4 = new FastReport.Design.DesignerSettings();
        FastReport.Design.DesignerRestrictions designerRestrictions4 = new FastReport.Design.DesignerRestrictions();
        FastReport.Export.Email.EmailSettings emailSettings4 = new FastReport.Export.Email.EmailSettings();
        FastReport.PreviewSettings previewSettings4 = new FastReport.PreviewSettings();
        FastReport.ReportSettings reportSettings4 = new FastReport.ReportSettings();
        this.groupBoxRapporter = new System.Windows.Forms.GroupBox();
        this.buttonForhandsgranska = new System.Windows.Forms.Button();
        this.imageList32 = new System.Windows.Forms.ImageList(this.components);
        this.buttonExportera = new System.Windows.Forms.Button();
        this.groupBoxRapporttyp = new System.Windows.Forms.GroupBox();
        this.radioButtonSlutavverkning = new System.Windows.Forms.RadioButton();
        this.radioButtonBiobränsle = new System.Windows.Forms.RadioButton();
        this.radioButtonRöjning = new System.Windows.Forms.RadioButton();
        this.radioButtonGallring = new System.Windows.Forms.RadioButton();
        this.radioButtonPlantering1 = new System.Windows.Forms.RadioButton();
        this.radioButtonMarkberedning = new System.Windows.Forms.RadioButton();
        this.labelFiltrering = new System.Windows.Forms.Label();
        this.groupBox1 = new System.Windows.Forms.GroupBox();
        this.buttonTaBortData = new System.Windows.Forms.Button();
        this.buttonFiltreraData = new System.Windows.Forms.Button();
        this.buttonGåTillHanteraData = new System.Windows.Forms.Button();
        this.buttonLagraÄndratData = new System.Windows.Forms.Button();
        this.buttonUppdateraData = new System.Windows.Forms.Button();
        this.dataGridViewRapporter = new System.Windows.Forms.DataGridView();
        this.rapportTypDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.invtypIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.invtypNamnDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.regionIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.regionNamnDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.distriktIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.distriktNamnDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.traktnrDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.traktNamnDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.entreprenorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.entreprenorNamnDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.traktdelDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.artalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.ursprungDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
        this.datumDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.atgarealDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.markberedningsmetodDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
        this.datainsamlingsmetodDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
        this.malgrundytaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.stickvagssystemNamnDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
        this.stickvagsavstandDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.arealDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.gISArealDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.provytestorlekDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.gISStrackaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.antalValtorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.avstandDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.bedomdVolymDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.hyggeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.vagkantDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.akerDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.latbilshuggDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
        this.grotbilDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
        this.traktordragenhuggDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
        this.skotarburenhuggDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
        this.avlagg1NorrDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.avlagg1OstDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.avlagg2NorrDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.avlagg2OstDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.avlagg3NorrDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.avlagg3OstDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.avlagg4NorrDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.avlagg4OstDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.avlagg5NorrDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.avlagg5OstDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.avlagg6NorrDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.avlagg6OstDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.kommentarDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.dataSetUserData = new System.Data.DataSet();
        this.dataTableUserData = new System.Data.DataTable();
        this.dataColumnRegionId = new System.Data.DataColumn();
        this.dataColumnDistriktId = new System.Data.DataColumn();
        this.dataColumnTraktnr = new System.Data.DataColumn();
        this.dataColumnStåndort = new System.Data.DataColumn();
        this.dataColumnEntreprenör = new System.Data.DataColumn();
        this.dataColumnRapportTyp = new System.Data.DataColumn();
        this.dataColumnTraktNamn = new System.Data.DataColumn();
        this.dataColumnRegionNamn = new System.Data.DataColumn();
        this.dataColumnDistriktNamn = new System.Data.DataColumn();
        this.dataColumnUrsprung = new System.Data.DataColumn();
        this.dataColumnDatum = new System.Data.DataColumn();
        this.dataColumnMarkberedningsmetod = new System.Data.DataColumn();
        this.dataColumnMålgrundyta = new System.Data.DataColumn();
        this.dataColumnStickvägsavstånd = new System.Data.DataColumn();
        this.dataColumnGISSträcka = new System.Data.DataColumn();
        this.dataColumnGISAreal = new System.Data.DataColumn();
        this.dataColumnProvytestorlek = new System.Data.DataColumn();
        this.dataColumnKommentar = new System.Data.DataColumn();
        this.dataColumnArtal = new System.Data.DataColumn();
        this.dataColumnAreal = new System.Data.DataColumn();
        this.dataColumnEntreprenörNamn = new System.Data.DataColumn();
        this.dataColumnAntalValtor = new System.Data.DataColumn();
        this.dataColumnAvstand = new System.Data.DataColumn();
        this.dataColumnBedomdVolym = new System.Data.DataColumn();
        this.dataColumnHygge = new System.Data.DataColumn();
        this.dataColumnVagkant = new System.Data.DataColumn();
        this.dataColumnAker = new System.Data.DataColumn();
        this.dataColumnLatbilshugg = new System.Data.DataColumn();
        this.dataColumnGrotbil = new System.Data.DataColumn();
        this.dataColumnTraktordragenhugg = new System.Data.DataColumn();
        this.dataColumnSkotarburenhugg = new System.Data.DataColumn();
        this.dataColumnAvlagg1Norr = new System.Data.DataColumn();
        this.dataColumnAvlagg1Ost = new System.Data.DataColumn();
        this.dataColumnAvlagg2Norr = new System.Data.DataColumn();
        this.dataColumnAvlagg2Ost = new System.Data.DataColumn();
        this.dataColumnAvlagg3Norr = new System.Data.DataColumn();
        this.dataColumnAvlagg3Ost = new System.Data.DataColumn();
        this.dataColumnAvlagg4Norr = new System.Data.DataColumn();
        this.dataColumnAvlagg4Ost = new System.Data.DataColumn();
        this.dataColumnAvlagg5Norr = new System.Data.DataColumn();
        this.dataColumnAvlagg5Ost = new System.Data.DataColumn();
        this.dataColumnAvlagg6Norr = new System.Data.DataColumn();
        this.dataColumnAvlagg6Ost = new System.Data.DataColumn();
        this.dataColumnStickvagssystem = new System.Data.DataColumn();
        this.dataColumnDatainsamlingsmetod = new System.Data.DataColumn();
        this.dataColumnInvTypId = new System.Data.DataColumn();
        this.dataColumnInvTypNamn = new System.Data.DataColumn();
        this.dataColumnAtgAreal = new System.Data.DataColumn();
        this.dataSetValdFiltrering = new System.Data.DataSet();
        this.dataTableVal = new System.Data.DataTable();
        this.dataColumnValRegion = new System.Data.DataColumn();
        this.dataColumnValDistrikt = new System.Data.DataColumn();
        this.dataColumnValTraktNr = new System.Data.DataColumn();
        this.dataColumnValTrakt = new System.Data.DataColumn();
        this.dataColumnValLagEntrNr = new System.Data.DataColumn();
        this.dataColumnValLagEntr = new System.Data.DataColumn();
        this.dataColumnValUrsprung = new System.Data.DataColumn();
        this.dataColumnValStandort = new System.Data.DataColumn();
        this.dataColumnValAr = new System.Data.DataColumn();
        this.dataColumnValRapportTyp = new System.Data.DataColumn();
        this.dataColumnValRapportFormat = new System.Data.DataColumn();
        this.dataColumnValInvTypID = new System.Data.DataColumn();
        this.dataColumnValInvTypNamn = new System.Data.DataColumn();
        this.dataSetSammanstallningar = new System.Data.DataSet();
        this.dataTableGallring = new System.Data.DataTable();
        this.dataColumnGRG = new System.Data.DataColumn();
        this.dataColumnGDI = new System.Data.DataColumn();
        this.dataColumnGUrsprung = new System.Data.DataColumn();
        this.dataColumnGAr = new System.Data.DataColumn();
        this.dataColumnGTraktnr = new System.Data.DataColumn();
        this.dataColumnGTraktnamn = new System.Data.DataColumn();
        this.dataColumnGStandort = new System.Data.DataColumn();
        this.dataColumnGEntreprenor = new System.Data.DataColumn();
        this.dataColumnGEntNamn = new System.Data.DataColumn();
        this.dataColumnGDatum = new System.Data.DataColumn();
        this.dataColumnGStickvagssystem = new System.Data.DataColumn();
        this.dataColumnGAreal = new System.Data.DataColumn();
        this.dataColumnGVagbredd = new System.Data.DataColumn();
        this.dataColumnGStickvagsAvstand = new System.Data.DataColumn();
        this.dataColumnGMalgrundyta = new System.Data.DataColumn();
        this.dataColumnGTall = new System.Data.DataColumn();
        this.dataColumnGGran = new System.Data.DataColumn();
        this.dataColumnGLov = new System.Data.DataColumn();
        this.dataColumnGContorta = new System.Data.DataColumn();
        this.dataColumnGSkador = new System.Data.DataColumn();
        this.dataColumnGTotGrundyta = new System.Data.DataColumn();
        this.dataColumnGMaluppfyllelse = new System.Data.DataColumn();
        this.dataColumnGFraga1 = new System.Data.DataColumn();
        this.dataColumnGFraga2 = new System.Data.DataColumn();
        this.dataColumnGFraga3 = new System.Data.DataColumn();
        this.dataColumnGFraga4 = new System.Data.DataColumn();
        this.dataColumnGFraga5 = new System.Data.DataColumn();
        this.dataColumnGFraga6 = new System.Data.DataColumn();
        this.dataColumnGFraga7 = new System.Data.DataColumn();
        this.dataColumnGFraga8 = new System.Data.DataColumn();
        this.dataColumnGFraga9 = new System.Data.DataColumn();
        this.dataColumnGKommentar = new System.Data.DataColumn();
        this.dataTableMarkberedning = new System.Data.DataTable();
        this.dataColumnMRG = new System.Data.DataColumn();
        this.dataColumnMDI = new System.Data.DataColumn();
        this.dataColumnMUrsprung = new System.Data.DataColumn();
        this.dataColumnMAr = new System.Data.DataColumn();
        this.dataColumnMTraktnr = new System.Data.DataColumn();
        this.dataColumnMTraktnamn = new System.Data.DataColumn();
        this.dataColumnMStandort = new System.Data.DataColumn();
        this.dataColumnMEntreprenor = new System.Data.DataColumn();
        this.dataColumnMEntNamn = new System.Data.DataColumn();
        this.dataColumnMDatum = new System.Data.DataColumn();
        this.dataColumnMBMetod = new System.Data.DataColumn();
        this.dataColumnMInsamlmetod = new System.Data.DataColumn();
        this.dataColumnMAreal = new System.Data.DataColumn();
        this.dataColumnMGISStracka = new System.Data.DataColumn();
        this.dataColumnMBStrackaPerHa = new System.Data.DataColumn();
        this.dataColumnMBMal = new System.Data.DataColumn();
        this.dataColumnMOptimalt = new System.Data.DataColumn();
        this.dataColumnMOptimaltBra = new System.Data.DataColumn();
        this.dataColumnMMaluppfyllelse = new System.Data.DataColumn();
        this.dataColumnMFraga1 = new System.Data.DataColumn();
        this.dataColumnMFraga2 = new System.Data.DataColumn();
        this.dataColumnMFraga3 = new System.Data.DataColumn();
        this.dataColumnMFraga4 = new System.Data.DataColumn();
        this.dataColumnMFraga5 = new System.Data.DataColumn();
        this.dataColumnMFraga6 = new System.Data.DataColumn();
        this.dataColumnMFraga7 = new System.Data.DataColumn();
        this.dataColumnMKommentar = new System.Data.DataColumn();
        this.dataColumnMInvTypId = new System.Data.DataColumn();
        this.dataTablePlantering = new System.Data.DataTable();
        this.dataColumnPRG = new System.Data.DataColumn();
        this.dataColumnPDI = new System.Data.DataColumn();
        this.dataColumnPUrsprung = new System.Data.DataColumn();
        this.dataColumnPAr = new System.Data.DataColumn();
        this.dataColumnPTraktnr = new System.Data.DataColumn();
        this.dataColumnPTraktnamn = new System.Data.DataColumn();
        this.dataColumnPStandort = new System.Data.DataColumn();
        this.dataColumnPEntreprenor = new System.Data.DataColumn();
        this.dataColumnPEntnamn = new System.Data.DataColumn();
        this.dataColumnPDatum = new System.Data.DataColumn();
        this.dataColumnPProvytestorlek = new System.Data.DataColumn();
        this.dataColumnPOptimaltSatta = new System.Data.DataColumn();
        this.dataColumnPBraSatta = new System.Data.DataColumn();
        this.dataColumnPTotSatta = new System.Data.DataColumn();
        this.dataColumnPMal = new System.Data.DataColumn();
        this.dataColumnPUtnyttjandegrad = new System.Data.DataColumn();
        this.dataColumnPUPI = new System.Data.DataColumn();
        this.dataColumnPBraTilltryckning = new System.Data.DataColumn();
        this.dataColumnPBraDjup = new System.Data.DataColumn();
        this.dataColumnPMaluppfyllelse = new System.Data.DataColumn();
        this.dataColumnPFraga1 = new System.Data.DataColumn();
        this.dataColumnPFraga2 = new System.Data.DataColumn();
        this.dataColumnPTall = new System.Data.DataColumn();
        this.dataColumnPTPlantTyp = new System.Data.DataColumn();
        this.dataColumnPTStambrev = new System.Data.DataColumn();
        this.dataColumnPGran = new System.Data.DataColumn();
        this.dataColumnPGPlantTyp = new System.Data.DataColumn();
        this.dataColumnPGStamBrev = new System.Data.DataColumn();
        this.dataColumnPContorta = new System.Data.DataColumn();
        this.dataColumnPCPlantTyp = new System.Data.DataColumn();
        this.dataColumnPCStamBrev = new System.Data.DataColumn();
        this.dataColumnPKommentar = new System.Data.DataColumn();
        this.dataColumnPInvTypId = new System.Data.DataColumn();
        this.dataColumnPAtgAreal = new System.Data.DataColumn();
        this.dataTableRojning = new System.Data.DataTable();
        this.dataColumnRRG = new System.Data.DataColumn();
        this.dataColumnRDI = new System.Data.DataColumn();
        this.dataColumnRUrsprung = new System.Data.DataColumn();
        this.dataColumnRAr = new System.Data.DataColumn();
        this.dataColumnRTraktnr = new System.Data.DataColumn();
        this.dataColumnRTraktnamn = new System.Data.DataColumn();
        this.dataColumnRStandort = new System.Data.DataColumn();
        this.dataColumnREntreprenor = new System.Data.DataColumn();
        this.dataColumnREntNamn = new System.Data.DataColumn();
        this.dataColumnRDatum = new System.Data.DataColumn();
        this.dataColumnRProvytestorlek = new System.Data.DataColumn();
        this.dataColumnRMalHuvudstam = new System.Data.DataColumn();
        this.dataColumnRHuvudstammar = new System.Data.DataColumn();
        this.dataColumnRMaluppfyllelse = new System.Data.DataColumn();
        this.dataColumnRBarrhuvudstammarsMedelhojd = new System.Data.DataColumn();
        this.dataColumnRT = new System.Data.DataColumn();
        this.dataColumnRG = new System.Data.DataColumn();
        this.dataColumnRL = new System.Data.DataColumn();
        this.dataColumnRC = new System.Data.DataColumn();
        this.dataColumnRFraga1 = new System.Data.DataColumn();
        this.dataColumnRFraga2 = new System.Data.DataColumn();
        this.dataColumnRFraga3 = new System.Data.DataColumn();
        this.dataColumnRFraga4 = new System.Data.DataColumn();
        this.dataColumnRFraga5 = new System.Data.DataColumn();
        this.dataColumnRFraga6 = new System.Data.DataColumn();
        this.dataColumnRKommentar = new System.Data.DataColumn();
        this.dataColumnRInvTypID = new System.Data.DataColumn();
        this.dataColumnRAtgAreal = new System.Data.DataColumn();
        this.dataTableSlutavverkning = new System.Data.DataTable();
        this.dataColumnSRG = new System.Data.DataColumn();
        this.dataColumnSDI = new System.Data.DataColumn();
        this.dataColumnSUrsprung = new System.Data.DataColumn();
        this.dataColumnSAr = new System.Data.DataColumn();
        this.dataColumnSTraktnr = new System.Data.DataColumn();
        this.dataColumnSTraktnamn = new System.Data.DataColumn();
        this.dataColumnSStandort = new System.Data.DataColumn();
        this.dataColumnSEntreprenor = new System.Data.DataColumn();
        this.dataColumnSEntrnamn = new System.Data.DataColumn();
        this.dataColumnSDatum = new System.Data.DataColumn();
        this.dataColumnSFraga1 = new System.Data.DataColumn();
        this.dataColumnSFraga2a = new System.Data.DataColumn();
        this.dataColumnSFraga2b = new System.Data.DataColumn();
        this.dataColumnSFraga2c = new System.Data.DataColumn();
        this.dataColumnSFraga2d = new System.Data.DataColumn();
        this.dataColumnSFraga2e = new System.Data.DataColumn();
        this.dataColumnSFraga2f = new System.Data.DataColumn();
        this.dataColumnSFraga2g = new System.Data.DataColumn();
        this.dataColumnSFraga2h = new System.Data.DataColumn();
        this.dataColumnSFraga3 = new System.Data.DataColumn();
        this.dataColumnSFraga4a = new System.Data.DataColumn();
        this.dataColumnSFraga4b = new System.Data.DataColumn();
        this.dataColumnSFraga5 = new System.Data.DataColumn();
        this.dataColumnSFraga6 = new System.Data.DataColumn();
        this.dataColumnSFraga7 = new System.Data.DataColumn();
        this.dataColumnSFraga8 = new System.Data.DataColumn();
        this.dataColumnSFraga9 = new System.Data.DataColumn();
        this.dataColumnSFraga10 = new System.Data.DataColumn();
        this.dataColumnSFraga11 = new System.Data.DataColumn();
        this.dataColumnSFraga12 = new System.Data.DataColumn();
        this.dataColumnSKommentar = new System.Data.DataColumn();
        this.dataTableBiobransle = new System.Data.DataTable();
        this.dataColumnBRG = new System.Data.DataColumn();
        this.dataColumnBDI = new System.Data.DataColumn();
        this.dataColumnBUrsprung = new System.Data.DataColumn();
        this.dataColumnBAr = new System.Data.DataColumn();
        this.dataColumnBTraktnr = new System.Data.DataColumn();
        this.dataColumnBTraktnamn = new System.Data.DataColumn();
        this.dataColumnBStandort = new System.Data.DataColumn();
        this.dataColumnBEntreprenor = new System.Data.DataColumn();
        this.dataColumnBEntnamn = new System.Data.DataColumn();
        this.dataColumnBDatum = new System.Data.DataColumn();
        this.dataColumnBValtorPaHygge = new System.Data.DataColumn();
        this.dataColumnBValtorPaVagkant = new System.Data.DataColumn();
        this.dataColumnBValtorPaAker = new System.Data.DataColumn();
        this.dataColumnBLastbilshugg = new System.Data.DataColumn();
        this.dataColumnBGrotbil = new System.Data.DataColumn();
        this.dataColumnBTraktorhugg = new System.Data.DataColumn();
        this.dataColumnBSkotarhugg = new System.Data.DataColumn();
        this.dataColumnBFraga1 = new System.Data.DataColumn();
        this.dataColumnBFraga2 = new System.Data.DataColumn();
        this.dataColumnBFraga3 = new System.Data.DataColumn();
        this.dataColumnBFraga4 = new System.Data.DataColumn();
        this.dataColumnBFraga5 = new System.Data.DataColumn();
        this.dataColumnBFraga6 = new System.Data.DataColumn();
        this.dataColumnBFraga7 = new System.Data.DataColumn();
        this.dataColumnBFraga8 = new System.Data.DataColumn();
        this.dataColumnBFraga9 = new System.Data.DataColumn();
        this.dataColumnBFraga10 = new System.Data.DataColumn();
        this.dataColumnBFraga11 = new System.Data.DataColumn();
        this.dataColumnBFraga12 = new System.Data.DataColumn();
        this.dataColumnBFraga13 = new System.Data.DataColumn();
        this.dataColumnBFraga14 = new System.Data.DataColumn();
        this.dataColumnBFraga15 = new System.Data.DataColumn();
        this.dataColumnBFraga16 = new System.Data.DataColumn();
        this.dataColumnBFraga17 = new System.Data.DataColumn();
        this.dataColumnBFraga18 = new System.Data.DataColumn();
        this.dataColumnBFraga19 = new System.Data.DataColumn();
        this.dataColumnBKommentar = new System.Data.DataColumn();
        this.environmentSettingsExporteraRapporter = new FastReport.EnvironmentSettings();
        this.contextMenuStripExportera = new System.Windows.Forms.ContextMenuStrip(this.components);
        this.toolStripMenuItemExcel2007 = new System.Windows.Forms.ToolStripMenuItem();
        this.toolStripMenuItemPdf = new System.Windows.Forms.ToolStripMenuItem();
        this.contextMenuStripForhandsgranska = new System.Windows.Forms.ContextMenuStrip(this.components);
        this.toolStripMenuItemForhandsgranskaxcel2007 = new System.Windows.Forms.ToolStripMenuItem();
        this.toolStripMenuItemForhandsgranskaPdf = new System.Windows.Forms.ToolStripMenuItem();
        this.saveFileDialogExporteraSammanstallningsRapport = new System.Windows.Forms.SaveFileDialog();
        this.reportExporteraSammanstallning = new FastReport.Report();
        this.dataColumnRLovhuvudstammarsMedelhojd = new System.Data.DataColumn();
        this.dataColumnRRojstamHa = new System.Data.DataColumn();
        this.groupBoxRapporter.SuspendLayout();
        this.groupBoxRapporttyp.SuspendLayout();
        this.groupBox1.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRapporter)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetUserData)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableUserData)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetValdFiltrering)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableVal)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetSammanstallningar)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableGallring)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableMarkberedning)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTablePlantering)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableRojning)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableSlutavverkning)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableBiobransle)).BeginInit();
        this.contextMenuStripExportera.SuspendLayout();
        this.contextMenuStripForhandsgranska.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.reportExporteraSammanstallning)).BeginInit();
        this.SuspendLayout();
        // 
        // groupBoxRapporter
        // 
        this.groupBoxRapporter.Controls.Add(this.buttonForhandsgranska);
        this.groupBoxRapporter.Controls.Add(this.buttonExportera);
        this.groupBoxRapporter.Controls.Add(this.groupBoxRapporttyp);
        this.groupBoxRapporter.Controls.Add(this.labelFiltrering);
        this.groupBoxRapporter.Controls.Add(this.groupBox1);
        this.groupBoxRapporter.Controls.Add(this.dataGridViewRapporter);
        this.groupBoxRapporter.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.groupBoxRapporter.Location = new System.Drawing.Point(9, 5);
        this.groupBoxRapporter.Name = "groupBoxRapporter";
        this.groupBoxRapporter.Size = new System.Drawing.Size(890, 592);
        this.groupBoxRapporter.TabIndex = 0;
        this.groupBoxRapporter.TabStop = false;
        // 
        // buttonForhandsgranska
        // 
        this.buttonForhandsgranska.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.buttonForhandsgranska.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonForhandsgranska.ImageIndex = 7;
        this.buttonForhandsgranska.ImageList = this.imageList32;
        this.buttonForhandsgranska.Location = new System.Drawing.Point(542, 450);
        this.buttonForhandsgranska.Name = "buttonForhandsgranska";
        this.buttonForhandsgranska.Size = new System.Drawing.Size(160, 40);
        this.buttonForhandsgranska.TabIndex = 16;
        this.buttonForhandsgranska.Text = "- Förhandsgranska -";
        this.buttonForhandsgranska.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
        this.buttonForhandsgranska.UseVisualStyleBackColor = true;
        this.buttonForhandsgranska.Click += new System.EventHandler(this.buttonForhandsgranska_Click);
        // 
        // imageList32
        // 
        this.imageList32.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList32.ImageStream")));
        this.imageList32.TransparentColor = System.Drawing.SystemColors.ActiveCaption;
        this.imageList32.Images.SetKeyName(0, "Uppdatera.ico");
        this.imageList32.Images.SetKeyName(1, "Lagra.ico");
        this.imageList32.Images.SetKeyName(2, "HoppaTill.ico");
        this.imageList32.Images.SetKeyName(3, "TabortData.ico");
        this.imageList32.Images.SetKeyName(4, "Filtrera.ico");
        this.imageList32.Images.SetKeyName(5, "Rapport.ico");
        this.imageList32.Images.SetKeyName(6, "Exportera.ico");
        this.imageList32.Images.SetKeyName(7, "Rapport.ico");
        // 
        // buttonExportera
        // 
        this.buttonExportera.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.buttonExportera.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonExportera.ImageIndex = 6;
        this.buttonExportera.ImageList = this.imageList32;
        this.buttonExportera.Location = new System.Drawing.Point(721, 450);
        this.buttonExportera.Name = "buttonExportera";
        this.buttonExportera.Size = new System.Drawing.Size(160, 40);
        this.buttonExportera.TabIndex = 12;
        this.buttonExportera.Text = "Exportera ...";
        this.buttonExportera.UseVisualStyleBackColor = true;
        this.buttonExportera.Click += new System.EventHandler(this.buttonExportera_Click);
        // 
        // groupBoxRapporttyp
        // 
        this.groupBoxRapporttyp.Controls.Add(this.radioButtonSlutavverkning);
        this.groupBoxRapporttyp.Controls.Add(this.radioButtonBiobränsle);
        this.groupBoxRapporttyp.Controls.Add(this.radioButtonRöjning);
        this.groupBoxRapporttyp.Controls.Add(this.radioButtonGallring);
        this.groupBoxRapporttyp.Controls.Add(this.radioButtonPlantering1);
        this.groupBoxRapporttyp.Controls.Add(this.radioButtonMarkberedning);
        this.groupBoxRapporttyp.Location = new System.Drawing.Point(0, 0);
        this.groupBoxRapporttyp.Name = "groupBoxRapporttyp";
        this.groupBoxRapporttyp.Size = new System.Drawing.Size(890, 49);
        this.groupBoxRapporttyp.TabIndex = 0;
        this.groupBoxRapporttyp.TabStop = false;
        this.groupBoxRapporttyp.Text = "Rapporttyp";
        // 
        // radioButtonSlutavverkning
        // 
        this.radioButtonSlutavverkning.AutoSize = true;
        this.radioButtonSlutavverkning.Location = new System.Drawing.Point(751, 20);
        this.radioButtonSlutavverkning.Name = "radioButtonSlutavverkning";
        this.radioButtonSlutavverkning.Size = new System.Drawing.Size(111, 17);
        this.radioButtonSlutavverkning.TabIndex = 6;
        this.radioButtonSlutavverkning.Text = "Slutavverkning";
        this.radioButtonSlutavverkning.UseVisualStyleBackColor = true;
        this.radioButtonSlutavverkning.CheckedChanged += new System.EventHandler(this.radioButtonSlutavverkning_CheckedChanged);
        // 
        // radioButtonBiobränsle
        // 
        this.radioButtonBiobränsle.AutoSize = true;
        this.radioButtonBiobränsle.Location = new System.Drawing.Point(593, 20);
        this.radioButtonBiobränsle.Name = "radioButtonBiobränsle";
        this.radioButtonBiobränsle.Size = new System.Drawing.Size(84, 17);
        this.radioButtonBiobränsle.TabIndex = 5;
        this.radioButtonBiobränsle.Text = "Biobränsle";
        this.radioButtonBiobränsle.UseVisualStyleBackColor = true;
        this.radioButtonBiobränsle.CheckedChanged += new System.EventHandler(this.radioButtonBiobränsle_CheckedChanged);
        // 
        // radioButtonRöjning
        // 
        this.radioButtonRöjning.AutoSize = true;
        this.radioButtonRöjning.Location = new System.Drawing.Point(328, 20);
        this.radioButtonRöjning.Name = "radioButtonRöjning";
        this.radioButtonRöjning.Size = new System.Drawing.Size(68, 17);
        this.radioButtonRöjning.TabIndex = 3;
        this.radioButtonRöjning.Text = "Röjning";
        this.radioButtonRöjning.UseVisualStyleBackColor = true;
        this.radioButtonRöjning.CheckedChanged += new System.EventHandler(this.radioButtonRöjning_CheckedChanged);
        // 
        // radioButtonGallring
        // 
        this.radioButtonGallring.AutoSize = true;
        this.radioButtonGallring.Location = new System.Drawing.Point(456, 20);
        this.radioButtonGallring.Name = "radioButtonGallring";
        this.radioButtonGallring.Size = new System.Drawing.Size(68, 17);
        this.radioButtonGallring.TabIndex = 4;
        this.radioButtonGallring.Text = "Gallring";
        this.radioButtonGallring.UseVisualStyleBackColor = true;
        this.radioButtonGallring.CheckedChanged += new System.EventHandler(this.radioButtonGallring_CheckedChanged);
        // 
        // radioButtonPlantering1
        // 
        this.radioButtonPlantering1.AutoSize = true;
        this.radioButtonPlantering1.Location = new System.Drawing.Point(178, 20);
        this.radioButtonPlantering1.Name = "radioButtonPlantering1";
        this.radioButtonPlantering1.Size = new System.Drawing.Size(83, 17);
        this.radioButtonPlantering1.TabIndex = 2;
        this.radioButtonPlantering1.Text = "Plantering";
        this.radioButtonPlantering1.UseVisualStyleBackColor = true;
        this.radioButtonPlantering1.CheckedChanged += new System.EventHandler(this.radioButtonPlantering_CheckedChanged);
        // 
        // radioButtonMarkberedning
        // 
        this.radioButtonMarkberedning.AutoSize = true;
        this.radioButtonMarkberedning.Location = new System.Drawing.Point(19, 20);
        this.radioButtonMarkberedning.Name = "radioButtonMarkberedning";
        this.radioButtonMarkberedning.Size = new System.Drawing.Size(111, 17);
        this.radioButtonMarkberedning.TabIndex = 1;
        this.radioButtonMarkberedning.Text = "Markberedning";
        this.radioButtonMarkberedning.UseVisualStyleBackColor = true;
        this.radioButtonMarkberedning.CheckedChanged += new System.EventHandler(this.radioButtonMarkberedning_CheckedChanged);
        // 
        // labelFiltrering
        // 
        this.labelFiltrering.Location = new System.Drawing.Point(11, 447);
        this.labelFiltrering.Name = "labelFiltrering";
        this.labelFiltrering.Size = new System.Drawing.Size(520, 81);
        this.labelFiltrering.TabIndex = 2;
        this.labelFiltrering.Text = "Filtrering: Ingen";
        // 
        // groupBox1
        // 
        this.groupBox1.Controls.Add(this.buttonTaBortData);
        this.groupBox1.Controls.Add(this.buttonFiltreraData);
        this.groupBox1.Controls.Add(this.buttonGåTillHanteraData);
        this.groupBox1.Controls.Add(this.buttonLagraÄndratData);
        this.groupBox1.Controls.Add(this.buttonUppdateraData);
        this.groupBox1.Location = new System.Drawing.Point(0, 526);
        this.groupBox1.Name = "groupBox1";
        this.groupBox1.Size = new System.Drawing.Size(890, 66);
        this.groupBox1.TabIndex = 3;
        this.groupBox1.TabStop = false;
        // 
        // buttonTaBortData
        // 
        this.buttonTaBortData.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.buttonTaBortData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonTaBortData.ImageIndex = 3;
        this.buttonTaBortData.ImageList = this.imageList32;
        this.buttonTaBortData.Location = new System.Drawing.Point(364, 16);
        this.buttonTaBortData.Name = "buttonTaBortData";
        this.buttonTaBortData.Size = new System.Drawing.Size(160, 40);
        this.buttonTaBortData.TabIndex = 2;
        this.buttonTaBortData.Text = "Ta bort";
        this.buttonTaBortData.UseVisualStyleBackColor = true;
        this.buttonTaBortData.Click += new System.EventHandler(this.buttonTaBortData_Click);
        // 
        // buttonFiltreraData
        // 
        this.buttonFiltreraData.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.buttonFiltreraData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonFiltreraData.ImageIndex = 4;
        this.buttonFiltreraData.ImageList = this.imageList32;
        this.buttonFiltreraData.Location = new System.Drawing.Point(542, 16);
        this.buttonFiltreraData.Name = "buttonFiltreraData";
        this.buttonFiltreraData.Size = new System.Drawing.Size(160, 40);
        this.buttonFiltreraData.TabIndex = 3;
        this.buttonFiltreraData.Text = "Filtrera";
        this.buttonFiltreraData.UseVisualStyleBackColor = true;
        this.buttonFiltreraData.Click += new System.EventHandler(this.buttonFiltreraData_Click);
        // 
        // buttonGåTillHanteraData
        // 
        this.buttonGåTillHanteraData.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.buttonGåTillHanteraData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonGåTillHanteraData.ImageIndex = 2;
        this.buttonGåTillHanteraData.ImageList = this.imageList32;
        this.buttonGåTillHanteraData.Location = new System.Drawing.Point(721, 16);
        this.buttonGåTillHanteraData.Name = "buttonGåTillHanteraData";
        this.buttonGåTillHanteraData.Size = new System.Drawing.Size(160, 40);
        this.buttonGåTillHanteraData.TabIndex = 4;
        this.buttonGåTillHanteraData.Text = "Hantera Data";
        this.buttonGåTillHanteraData.UseVisualStyleBackColor = true;
        this.buttonGåTillHanteraData.Click += new System.EventHandler(this.buttonGåTillRapportData_Click);
        // 
        // buttonLagraÄndratData
        // 
        this.buttonLagraÄndratData.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.buttonLagraÄndratData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonLagraÄndratData.ImageIndex = 1;
        this.buttonLagraÄndratData.ImageList = this.imageList32;
        this.buttonLagraÄndratData.Location = new System.Drawing.Point(187, 16);
        this.buttonLagraÄndratData.Name = "buttonLagraÄndratData";
        this.buttonLagraÄndratData.Size = new System.Drawing.Size(160, 40);
        this.buttonLagraÄndratData.TabIndex = 1;
        this.buttonLagraÄndratData.Text = "Lagra";
        this.buttonLagraÄndratData.UseVisualStyleBackColor = true;
        this.buttonLagraÄndratData.Click += new System.EventHandler(this.buttonLagraÄndratData_Click);
        // 
        // buttonUppdateraData
        // 
        this.buttonUppdateraData.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.buttonUppdateraData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonUppdateraData.ImageIndex = 0;
        this.buttonUppdateraData.ImageList = this.imageList32;
        this.buttonUppdateraData.Location = new System.Drawing.Point(11, 16);
        this.buttonUppdateraData.Name = "buttonUppdateraData";
        this.buttonUppdateraData.Size = new System.Drawing.Size(160, 40);
        this.buttonUppdateraData.TabIndex = 0;
        this.buttonUppdateraData.Text = "Uppdatera";
        this.buttonUppdateraData.UseVisualStyleBackColor = true;
        this.buttonUppdateraData.Click += new System.EventHandler(this.buttonUppdateraData_Click);
        // 
        // dataGridViewRapporter
        // 
        this.dataGridViewRapporter.AllowUserToAddRows = false;
        this.dataGridViewRapporter.AllowUserToOrderColumns = true;
        this.dataGridViewRapporter.AllowUserToResizeRows = false;
        dataGridViewCellStyle76.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.dataGridViewRapporter.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle76;
        this.dataGridViewRapporter.AutoGenerateColumns = false;
        this.dataGridViewRapporter.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
        this.dataGridViewRapporter.BackgroundColor = System.Drawing.SystemColors.ControlDark;
        this.dataGridViewRapporter.BorderStyle = System.Windows.Forms.BorderStyle.None;
        this.dataGridViewRapporter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
        this.dataGridViewRapporter.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.rapportTypDataGridViewTextBoxColumn,
            this.invtypIdDataGridViewTextBoxColumn,
            this.invtypNamnDataGridViewTextBoxColumn,
            this.regionIdDataGridViewTextBoxColumn,
            this.regionNamnDataGridViewTextBoxColumn,
            this.distriktIdDataGridViewTextBoxColumn,
            this.distriktNamnDataGridViewTextBoxColumn,
            this.traktnrDataGridViewTextBoxColumn,
            this.traktNamnDataGridViewTextBoxColumn,
            this.entreprenorDataGridViewTextBoxColumn,
            this.entreprenorNamnDataGridViewTextBoxColumn,
            this.traktdelDataGridViewTextBoxColumn,
            this.artalDataGridViewTextBoxColumn,
            this.ursprungDataGridViewTextBoxColumn,
            this.datumDataGridViewTextBoxColumn,
            this.atgarealDataGridViewTextBoxColumn,
            this.markberedningsmetodDataGridViewTextBoxColumn,
            this.datainsamlingsmetodDataGridViewTextBoxColumn,
            this.malgrundytaDataGridViewTextBoxColumn,
            this.stickvagssystemNamnDataGridViewTextBoxColumn,
            this.stickvagsavstandDataGridViewTextBoxColumn,
            this.arealDataGridViewTextBoxColumn,
            this.gISArealDataGridViewTextBoxColumn,
            this.provytestorlekDataGridViewTextBoxColumn,
            this.gISStrackaDataGridViewTextBoxColumn,
            this.antalValtorDataGridViewTextBoxColumn,
            this.avstandDataGridViewTextBoxColumn,
            this.bedomdVolymDataGridViewTextBoxColumn,
            this.hyggeDataGridViewTextBoxColumn,
            this.vagkantDataGridViewTextBoxColumn,
            this.akerDataGridViewTextBoxColumn,
            this.latbilshuggDataGridViewTextBoxColumn,
            this.grotbilDataGridViewTextBoxColumn,
            this.traktordragenhuggDataGridViewTextBoxColumn,
            this.skotarburenhuggDataGridViewTextBoxColumn,
            this.avlagg1NorrDataGridViewTextBoxColumn,
            this.avlagg1OstDataGridViewTextBoxColumn,
            this.avlagg2NorrDataGridViewTextBoxColumn,
            this.avlagg2OstDataGridViewTextBoxColumn,
            this.avlagg3NorrDataGridViewTextBoxColumn,
            this.avlagg3OstDataGridViewTextBoxColumn,
            this.avlagg4NorrDataGridViewTextBoxColumn,
            this.avlagg4OstDataGridViewTextBoxColumn,
            this.avlagg5NorrDataGridViewTextBoxColumn,
            this.avlagg5OstDataGridViewTextBoxColumn,
            this.avlagg6NorrDataGridViewTextBoxColumn,
            this.avlagg6OstDataGridViewTextBoxColumn,
            this.kommentarDataGridViewTextBoxColumn});
        this.dataGridViewRapporter.DataMember = "UserData";
        this.dataGridViewRapporter.DataSource = this.dataSetUserData;
        dataGridViewCellStyle99.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle99.BackColor = System.Drawing.SystemColors.Window;
        dataGridViewCellStyle99.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        dataGridViewCellStyle99.ForeColor = System.Drawing.SystemColors.ControlText;
        dataGridViewCellStyle99.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle99.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle99.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
        this.dataGridViewRapporter.DefaultCellStyle = dataGridViewCellStyle99;
        this.dataGridViewRapporter.Location = new System.Drawing.Point(0, 47);
        this.dataGridViewRapporter.MultiSelect = false;
        this.dataGridViewRapporter.Name = "dataGridViewRapporter";
        this.dataGridViewRapporter.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
        dataGridViewCellStyle100.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.dataGridViewRapporter.RowsDefaultCellStyle = dataGridViewCellStyle100;
        this.dataGridViewRapporter.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.dataGridViewRapporter.Size = new System.Drawing.Size(890, 394);
        this.dataGridViewRapporter.TabIndex = 1;
        this.dataGridViewRapporter.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dataGridViewRapporter_UserDeletingRow);
        this.dataGridViewRapporter.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridViewRapporter_CellFormatting);
        this.dataGridViewRapporter.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dataGridViewRapporter_CellValidating);
        this.dataGridViewRapporter.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewRapporter_CellEndEdit);
        this.dataGridViewRapporter.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewRapporter_DataError);
        // 
        // rapportTypDataGridViewTextBoxColumn
        // 
        this.rapportTypDataGridViewTextBoxColumn.DataPropertyName = "RapportTyp";
        dataGridViewCellStyle77.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle77.BackColor = System.Drawing.Color.LemonChiffon;
        dataGridViewCellStyle77.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.rapportTypDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle77;
        this.rapportTypDataGridViewTextBoxColumn.HeaderText = "Rapport typ";
        this.rapportTypDataGridViewTextBoxColumn.Name = "rapportTypDataGridViewTextBoxColumn";
        this.rapportTypDataGridViewTextBoxColumn.ReadOnly = true;
        this.rapportTypDataGridViewTextBoxColumn.Width = 85;
        // 
        // invtypIdDataGridViewTextBoxColumn
        // 
        this.invtypIdDataGridViewTextBoxColumn.DataPropertyName = "InvTypId";
        dataGridViewCellStyle78.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle78.BackColor = System.Drawing.Color.LemonChiffon;
        dataGridViewCellStyle78.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.invtypIdDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle78;
        this.invtypIdDataGridViewTextBoxColumn.HeaderText = "InvTypId";
        this.invtypIdDataGridViewTextBoxColumn.Name = "invtypIdDataGridViewTextBoxColumn";
        this.invtypIdDataGridViewTextBoxColumn.ReadOnly = true;
        this.invtypIdDataGridViewTextBoxColumn.Width = 65;
        // 
        // invtypNamnDataGridViewTextBoxColumn
        // 
        this.invtypNamnDataGridViewTextBoxColumn.DataPropertyName = "InvTypNamn";
        dataGridViewCellStyle79.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle79.BackColor = System.Drawing.Color.LemonChiffon;
        this.invtypNamnDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle79;
        this.invtypNamnDataGridViewTextBoxColumn.HeaderText = "InvTypNamn";
        this.invtypNamnDataGridViewTextBoxColumn.Name = "invtypNamnDataGridViewTextBoxColumn";
        this.invtypNamnDataGridViewTextBoxColumn.ReadOnly = true;
        this.invtypNamnDataGridViewTextBoxColumn.Width = 80;
        // 
        // regionIdDataGridViewTextBoxColumn
        // 
        this.regionIdDataGridViewTextBoxColumn.DataPropertyName = "RegionId";
        dataGridViewCellStyle80.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle80.BackColor = System.Drawing.Color.LemonChiffon;
        dataGridViewCellStyle80.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.regionIdDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle80;
        this.regionIdDataGridViewTextBoxColumn.HeaderText = "Region id";
        this.regionIdDataGridViewTextBoxColumn.Name = "regionIdDataGridViewTextBoxColumn";
        this.regionIdDataGridViewTextBoxColumn.ReadOnly = true;
        this.regionIdDataGridViewTextBoxColumn.Width = 65;
        // 
        // regionNamnDataGridViewTextBoxColumn
        // 
        this.regionNamnDataGridViewTextBoxColumn.DataPropertyName = "RegionNamn";
        dataGridViewCellStyle81.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle81.BackColor = System.Drawing.Color.LemonChiffon;
        dataGridViewCellStyle81.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.regionNamnDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle81;
        this.regionNamnDataGridViewTextBoxColumn.HeaderText = "Region";
        this.regionNamnDataGridViewTextBoxColumn.Name = "regionNamnDataGridViewTextBoxColumn";
        this.regionNamnDataGridViewTextBoxColumn.ReadOnly = true;
        this.regionNamnDataGridViewTextBoxColumn.Width = 110;
        // 
        // distriktIdDataGridViewTextBoxColumn
        // 
        this.distriktIdDataGridViewTextBoxColumn.DataPropertyName = "DistriktId";
        dataGridViewCellStyle82.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle82.BackColor = System.Drawing.Color.LemonChiffon;
        dataGridViewCellStyle82.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.distriktIdDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle82;
        this.distriktIdDataGridViewTextBoxColumn.HeaderText = "Distrikt id";
        this.distriktIdDataGridViewTextBoxColumn.Name = "distriktIdDataGridViewTextBoxColumn";
        this.distriktIdDataGridViewTextBoxColumn.ReadOnly = true;
        this.distriktIdDataGridViewTextBoxColumn.Width = 68;
        // 
        // distriktNamnDataGridViewTextBoxColumn
        // 
        this.distriktNamnDataGridViewTextBoxColumn.DataPropertyName = "DistriktNamn";
        dataGridViewCellStyle83.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle83.BackColor = System.Drawing.Color.LemonChiffon;
        dataGridViewCellStyle83.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.distriktNamnDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle83;
        this.distriktNamnDataGridViewTextBoxColumn.HeaderText = "Distrikt";
        this.distriktNamnDataGridViewTextBoxColumn.Name = "distriktNamnDataGridViewTextBoxColumn";
        this.distriktNamnDataGridViewTextBoxColumn.ReadOnly = true;
        this.distriktNamnDataGridViewTextBoxColumn.Width = 110;
        // 
        // traktnrDataGridViewTextBoxColumn
        // 
        this.traktnrDataGridViewTextBoxColumn.DataPropertyName = "Traktnr";
        dataGridViewCellStyle84.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle84.BackColor = System.Drawing.Color.LemonChiffon;
        dataGridViewCellStyle84.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        dataGridViewCellStyle84.Format = "000000";
        dataGridViewCellStyle84.NullValue = null;
        this.traktnrDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle84;
        this.traktnrDataGridViewTextBoxColumn.HeaderText = "Trakt nr";
        this.traktnrDataGridViewTextBoxColumn.Name = "traktnrDataGridViewTextBoxColumn";
        this.traktnrDataGridViewTextBoxColumn.ReadOnly = true;
        this.traktnrDataGridViewTextBoxColumn.Width = 65;
        // 
        // traktNamnDataGridViewTextBoxColumn
        // 
        this.traktNamnDataGridViewTextBoxColumn.DataPropertyName = "TraktNamn";
        dataGridViewCellStyle85.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle85.BackColor = System.Drawing.Color.LemonChiffon;
        dataGridViewCellStyle85.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.traktNamnDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle85;
        this.traktNamnDataGridViewTextBoxColumn.HeaderText = "Trakt";
        this.traktNamnDataGridViewTextBoxColumn.Name = "traktNamnDataGridViewTextBoxColumn";
        this.traktNamnDataGridViewTextBoxColumn.ReadOnly = true;
        this.traktNamnDataGridViewTextBoxColumn.Width = 110;
        // 
        // entreprenorDataGridViewTextBoxColumn
        // 
        this.entreprenorDataGridViewTextBoxColumn.DataPropertyName = "Entreprenor";
        dataGridViewCellStyle86.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle86.BackColor = System.Drawing.Color.LemonChiffon;
        dataGridViewCellStyle86.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        dataGridViewCellStyle86.Format = "0000";
        dataGridViewCellStyle86.NullValue = null;
        this.entreprenorDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle86;
        this.entreprenorDataGridViewTextBoxColumn.HeaderText = "Entreprenörnr";
        this.entreprenorDataGridViewTextBoxColumn.Name = "entreprenorDataGridViewTextBoxColumn";
        this.entreprenorDataGridViewTextBoxColumn.ReadOnly = true;
        this.entreprenorDataGridViewTextBoxColumn.Width = 95;
        // 
        // entreprenorNamnDataGridViewTextBoxColumn
        // 
        this.entreprenorNamnDataGridViewTextBoxColumn.DataPropertyName = "EntreprenorNamn";
        dataGridViewCellStyle87.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle87.BackColor = System.Drawing.Color.LemonChiffon;
        dataGridViewCellStyle87.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.entreprenorNamnDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle87;
        this.entreprenorNamnDataGridViewTextBoxColumn.HeaderText = "Entreprenör";
        this.entreprenorNamnDataGridViewTextBoxColumn.Name = "entreprenorNamnDataGridViewTextBoxColumn";
        this.entreprenorNamnDataGridViewTextBoxColumn.ReadOnly = true;
        // 
        // traktdelDataGridViewTextBoxColumn
        // 
        this.traktdelDataGridViewTextBoxColumn.DataPropertyName = "Standort";
        dataGridViewCellStyle88.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle88.BackColor = System.Drawing.Color.LemonChiffon;
        dataGridViewCellStyle88.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.traktdelDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle88;
        this.traktdelDataGridViewTextBoxColumn.HeaderText = "Traktdel";
        this.traktdelDataGridViewTextBoxColumn.Name = "traktdelDataGridViewTextBoxColumn";
        this.traktdelDataGridViewTextBoxColumn.ReadOnly = true;
        this.traktdelDataGridViewTextBoxColumn.Width = 65;
        // 
        // artalDataGridViewTextBoxColumn
        // 
        this.artalDataGridViewTextBoxColumn.DataPropertyName = "Artal";
        dataGridViewCellStyle89.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle89.BackColor = System.Drawing.Color.LemonChiffon;
        dataGridViewCellStyle89.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.artalDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle89;
        this.artalDataGridViewTextBoxColumn.HeaderText = "Årtal";
        this.artalDataGridViewTextBoxColumn.Name = "artalDataGridViewTextBoxColumn";
        this.artalDataGridViewTextBoxColumn.ReadOnly = true;
        this.artalDataGridViewTextBoxColumn.Width = 55;
        // 
        // ursprungDataGridViewTextBoxColumn
        // 
        this.ursprungDataGridViewTextBoxColumn.DataPropertyName = "Ursprung";
        dataGridViewCellStyle90.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle90.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.ursprungDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle90;
        this.ursprungDataGridViewTextBoxColumn.HeaderText = "Ursprung";
        this.ursprungDataGridViewTextBoxColumn.Items.AddRange(new object[] {
            "Köp",
            "Eget"});
        this.ursprungDataGridViewTextBoxColumn.Name = "ursprungDataGridViewTextBoxColumn";
        this.ursprungDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
        this.ursprungDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
        this.ursprungDataGridViewTextBoxColumn.Width = 60;
        // 
        // datumDataGridViewTextBoxColumn
        // 
        this.datumDataGridViewTextBoxColumn.DataPropertyName = "Datum";
        dataGridViewCellStyle91.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle91.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        dataGridViewCellStyle91.Format = "d";
        dataGridViewCellStyle91.NullValue = null;
        this.datumDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle91;
        this.datumDataGridViewTextBoxColumn.HeaderText = "Datum";
        this.datumDataGridViewTextBoxColumn.Name = "datumDataGridViewTextBoxColumn";
        this.datumDataGridViewTextBoxColumn.Width = 80;
        // 
        // atgarealDataGridViewTextBoxColumn
        // 
        this.atgarealDataGridViewTextBoxColumn.DataPropertyName = "AtgAreal";
        dataGridViewCellStyle92.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle92.BackColor = System.Drawing.Color.LemonChiffon;
        dataGridViewCellStyle92.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.atgarealDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle92;
        this.atgarealDataGridViewTextBoxColumn.HeaderText = "Åtg areal";
        this.atgarealDataGridViewTextBoxColumn.Name = "atgarealDataGridViewTextBoxColumn";
        this.atgarealDataGridViewTextBoxColumn.Width = 90;
        // 
        // markberedningsmetodDataGridViewTextBoxColumn
        // 
        this.markberedningsmetodDataGridViewTextBoxColumn.DataPropertyName = "Markberedningsmetod";
        dataGridViewCellStyle93.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle93.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.markberedningsmetodDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle93;
        this.markberedningsmetodDataGridViewTextBoxColumn.HeaderText = "Markberedningsmetod";
        this.markberedningsmetodDataGridViewTextBoxColumn.Items.AddRange(new object[] {
            "Kontinuerligt harv",
            "Högläggning",
            "För sådd"});
        this.markberedningsmetodDataGridViewTextBoxColumn.Name = "markberedningsmetodDataGridViewTextBoxColumn";
        this.markberedningsmetodDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
        this.markberedningsmetodDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
        this.markberedningsmetodDataGridViewTextBoxColumn.Width = 150;
        // 
        // datainsamlingsmetodDataGridViewTextBoxColumn
        // 
        this.datainsamlingsmetodDataGridViewTextBoxColumn.DataPropertyName = "Datainsamlingsmetod";
        dataGridViewCellStyle94.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle94.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.datainsamlingsmetodDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle94;
        this.datainsamlingsmetodDataGridViewTextBoxColumn.HeaderText = "Datainsamlingsmetod";
        this.datainsamlingsmetodDataGridViewTextBoxColumn.Items.AddRange(new object[] {
            "Cirkelyta (100 m2)",
            "Sträckmätning (40 m)"});
        this.datainsamlingsmetodDataGridViewTextBoxColumn.Name = "datainsamlingsmetodDataGridViewTextBoxColumn";
        this.datainsamlingsmetodDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
        this.datainsamlingsmetodDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
        this.datainsamlingsmetodDataGridViewTextBoxColumn.Width = 150;
        // 
        // malgrundytaDataGridViewTextBoxColumn
        // 
        this.malgrundytaDataGridViewTextBoxColumn.DataPropertyName = "Malgrundyta";
        this.malgrundytaDataGridViewTextBoxColumn.HeaderText = "Målgrundyta";
        this.malgrundytaDataGridViewTextBoxColumn.Name = "malgrundytaDataGridViewTextBoxColumn";
        // 
        // stickvagssystemNamnDataGridViewTextBoxColumn
        // 
        this.stickvagssystemNamnDataGridViewTextBoxColumn.DataPropertyName = "Stickvagssystem";
        this.stickvagssystemNamnDataGridViewTextBoxColumn.HeaderText = "Stickvägssystem";
        this.stickvagssystemNamnDataGridViewTextBoxColumn.Items.AddRange(new object[] {
            "Stickväg",
            "Slingerväg"});
        this.stickvagssystemNamnDataGridViewTextBoxColumn.Name = "stickvagssystemNamnDataGridViewTextBoxColumn";
        this.stickvagssystemNamnDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
        this.stickvagssystemNamnDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
        this.stickvagssystemNamnDataGridViewTextBoxColumn.Width = 120;
        // 
        // stickvagsavstandDataGridViewTextBoxColumn
        // 
        this.stickvagsavstandDataGridViewTextBoxColumn.DataPropertyName = "Stickvagsavstand";
        this.stickvagsavstandDataGridViewTextBoxColumn.HeaderText = "Stickvägsavstånd";
        this.stickvagsavstandDataGridViewTextBoxColumn.Name = "stickvagsavstandDataGridViewTextBoxColumn";
        this.stickvagsavstandDataGridViewTextBoxColumn.Width = 130;
        // 
        // arealDataGridViewTextBoxColumn
        // 
        this.arealDataGridViewTextBoxColumn.DataPropertyName = "Areal";
        dataGridViewCellStyle95.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle95.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        dataGridViewCellStyle95.Format = "N1";
        dataGridViewCellStyle95.NullValue = null;
        this.arealDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle95;
        this.arealDataGridViewTextBoxColumn.HeaderText = "Areal";
        this.arealDataGridViewTextBoxColumn.Name = "arealDataGridViewTextBoxColumn";
        this.arealDataGridViewTextBoxColumn.Width = 55;
        // 
        // gISArealDataGridViewTextBoxColumn
        // 
        this.gISArealDataGridViewTextBoxColumn.DataPropertyName = "GISAreal";
        dataGridViewCellStyle96.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle96.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        dataGridViewCellStyle96.Format = "N1";
        dataGridViewCellStyle96.NullValue = null;
        this.gISArealDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle96;
        this.gISArealDataGridViewTextBoxColumn.HeaderText = "GIS areal";
        this.gISArealDataGridViewTextBoxColumn.Name = "gISArealDataGridViewTextBoxColumn";
        this.gISArealDataGridViewTextBoxColumn.Width = 65;
        // 
        // provytestorlekDataGridViewTextBoxColumn
        // 
        this.provytestorlekDataGridViewTextBoxColumn.DataPropertyName = "Provytestorlek";
        dataGridViewCellStyle97.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle97.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.provytestorlekDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle97;
        this.provytestorlekDataGridViewTextBoxColumn.HeaderText = "Provytestorlek";
        this.provytestorlekDataGridViewTextBoxColumn.Name = "provytestorlekDataGridViewTextBoxColumn";
        // 
        // gISStrackaDataGridViewTextBoxColumn
        // 
        this.gISStrackaDataGridViewTextBoxColumn.DataPropertyName = "GISStracka";
        this.gISStrackaDataGridViewTextBoxColumn.HeaderText = "GIS sträcka";
        this.gISStrackaDataGridViewTextBoxColumn.Name = "gISStrackaDataGridViewTextBoxColumn";
        // 
        // antalValtorDataGridViewTextBoxColumn
        // 
        this.antalValtorDataGridViewTextBoxColumn.DataPropertyName = "AntalValtor";
        this.antalValtorDataGridViewTextBoxColumn.HeaderText = "Antal vältor";
        this.antalValtorDataGridViewTextBoxColumn.Name = "antalValtorDataGridViewTextBoxColumn";
        // 
        // avstandDataGridViewTextBoxColumn
        // 
        this.avstandDataGridViewTextBoxColumn.DataPropertyName = "Avstand";
        this.avstandDataGridViewTextBoxColumn.HeaderText = "Avstånd";
        this.avstandDataGridViewTextBoxColumn.Name = "avstandDataGridViewTextBoxColumn";
        // 
        // bedomdVolymDataGridViewTextBoxColumn
        // 
        this.bedomdVolymDataGridViewTextBoxColumn.DataPropertyName = "BedomdVolym";
        this.bedomdVolymDataGridViewTextBoxColumn.HeaderText = "Bedömd volym";
        this.bedomdVolymDataGridViewTextBoxColumn.Name = "bedomdVolymDataGridViewTextBoxColumn";
        // 
        // hyggeDataGridViewTextBoxColumn
        // 
        this.hyggeDataGridViewTextBoxColumn.DataPropertyName = "Hygge";
        this.hyggeDataGridViewTextBoxColumn.HeaderText = "Hygge";
        this.hyggeDataGridViewTextBoxColumn.Name = "hyggeDataGridViewTextBoxColumn";
        // 
        // vagkantDataGridViewTextBoxColumn
        // 
        this.vagkantDataGridViewTextBoxColumn.DataPropertyName = "Vagkant";
        this.vagkantDataGridViewTextBoxColumn.HeaderText = "Vägkant";
        this.vagkantDataGridViewTextBoxColumn.Name = "vagkantDataGridViewTextBoxColumn";
        // 
        // akerDataGridViewTextBoxColumn
        // 
        this.akerDataGridViewTextBoxColumn.DataPropertyName = "Aker";
        this.akerDataGridViewTextBoxColumn.HeaderText = "Åker";
        this.akerDataGridViewTextBoxColumn.Name = "akerDataGridViewTextBoxColumn";
        // 
        // latbilshuggDataGridViewTextBoxColumn
        // 
        this.latbilshuggDataGridViewTextBoxColumn.DataPropertyName = "Latbilshugg";
        this.latbilshuggDataGridViewTextBoxColumn.HeaderText = "Latbilshugg";
        this.latbilshuggDataGridViewTextBoxColumn.Name = "latbilshuggDataGridViewTextBoxColumn";
        // 
        // grotbilDataGridViewTextBoxColumn
        // 
        this.grotbilDataGridViewTextBoxColumn.DataPropertyName = "Grotbil";
        this.grotbilDataGridViewTextBoxColumn.HeaderText = "Grotbil";
        this.grotbilDataGridViewTextBoxColumn.Name = "grotbilDataGridViewTextBoxColumn";
        // 
        // traktordragenhuggDataGridViewTextBoxColumn
        // 
        this.traktordragenhuggDataGridViewTextBoxColumn.DataPropertyName = "Traktordragenhugg";
        this.traktordragenhuggDataGridViewTextBoxColumn.HeaderText = "Traktordragenhugg";
        this.traktordragenhuggDataGridViewTextBoxColumn.Name = "traktordragenhuggDataGridViewTextBoxColumn";
        this.traktordragenhuggDataGridViewTextBoxColumn.Width = 140;
        // 
        // skotarburenhuggDataGridViewTextBoxColumn
        // 
        this.skotarburenhuggDataGridViewTextBoxColumn.DataPropertyName = "Skotarburenhugg";
        this.skotarburenhuggDataGridViewTextBoxColumn.HeaderText = "Skotarburenhugg";
        this.skotarburenhuggDataGridViewTextBoxColumn.Name = "skotarburenhuggDataGridViewTextBoxColumn";
        this.skotarburenhuggDataGridViewTextBoxColumn.Width = 135;
        // 
        // avlagg1NorrDataGridViewTextBoxColumn
        // 
        this.avlagg1NorrDataGridViewTextBoxColumn.DataPropertyName = "Avlagg1Norr";
        this.avlagg1NorrDataGridViewTextBoxColumn.HeaderText = "Avlägg1Norr";
        this.avlagg1NorrDataGridViewTextBoxColumn.Name = "avlagg1NorrDataGridViewTextBoxColumn";
        // 
        // avlagg1OstDataGridViewTextBoxColumn
        // 
        this.avlagg1OstDataGridViewTextBoxColumn.DataPropertyName = "Avlagg1Ost";
        this.avlagg1OstDataGridViewTextBoxColumn.HeaderText = "Avlägg1Ost";
        this.avlagg1OstDataGridViewTextBoxColumn.Name = "avlagg1OstDataGridViewTextBoxColumn";
        // 
        // avlagg2NorrDataGridViewTextBoxColumn
        // 
        this.avlagg2NorrDataGridViewTextBoxColumn.DataPropertyName = "Avlagg2Norr";
        this.avlagg2NorrDataGridViewTextBoxColumn.HeaderText = "Avlägg2Norr";
        this.avlagg2NorrDataGridViewTextBoxColumn.Name = "avlagg2NorrDataGridViewTextBoxColumn";
        // 
        // avlagg2OstDataGridViewTextBoxColumn
        // 
        this.avlagg2OstDataGridViewTextBoxColumn.DataPropertyName = "Avlagg2Ost";
        this.avlagg2OstDataGridViewTextBoxColumn.HeaderText = "Avlägg2Ost";
        this.avlagg2OstDataGridViewTextBoxColumn.Name = "avlagg2OstDataGridViewTextBoxColumn";
        // 
        // avlagg3NorrDataGridViewTextBoxColumn
        // 
        this.avlagg3NorrDataGridViewTextBoxColumn.DataPropertyName = "Avlagg3Norr";
        this.avlagg3NorrDataGridViewTextBoxColumn.HeaderText = "Avlägg3Norr";
        this.avlagg3NorrDataGridViewTextBoxColumn.Name = "avlagg3NorrDataGridViewTextBoxColumn";
        // 
        // avlagg3OstDataGridViewTextBoxColumn
        // 
        this.avlagg3OstDataGridViewTextBoxColumn.DataPropertyName = "Avlagg3Ost";
        this.avlagg3OstDataGridViewTextBoxColumn.HeaderText = "Avlägg3Ost";
        this.avlagg3OstDataGridViewTextBoxColumn.Name = "avlagg3OstDataGridViewTextBoxColumn";
        // 
        // avlagg4NorrDataGridViewTextBoxColumn
        // 
        this.avlagg4NorrDataGridViewTextBoxColumn.DataPropertyName = "Avlagg4Norr";
        this.avlagg4NorrDataGridViewTextBoxColumn.HeaderText = "Avlägg4Norr";
        this.avlagg4NorrDataGridViewTextBoxColumn.Name = "avlagg4NorrDataGridViewTextBoxColumn";
        // 
        // avlagg4OstDataGridViewTextBoxColumn
        // 
        this.avlagg4OstDataGridViewTextBoxColumn.DataPropertyName = "Avlagg4Ost";
        this.avlagg4OstDataGridViewTextBoxColumn.HeaderText = "Avlägg4Ost";
        this.avlagg4OstDataGridViewTextBoxColumn.Name = "avlagg4OstDataGridViewTextBoxColumn";
        // 
        // avlagg5NorrDataGridViewTextBoxColumn
        // 
        this.avlagg5NorrDataGridViewTextBoxColumn.DataPropertyName = "Avlagg5Norr";
        this.avlagg5NorrDataGridViewTextBoxColumn.HeaderText = "Avlägg5Norr";
        this.avlagg5NorrDataGridViewTextBoxColumn.Name = "avlagg5NorrDataGridViewTextBoxColumn";
        // 
        // avlagg5OstDataGridViewTextBoxColumn
        // 
        this.avlagg5OstDataGridViewTextBoxColumn.DataPropertyName = "Avlagg5Ost";
        this.avlagg5OstDataGridViewTextBoxColumn.HeaderText = "Avlägg5Ost";
        this.avlagg5OstDataGridViewTextBoxColumn.Name = "avlagg5OstDataGridViewTextBoxColumn";
        // 
        // avlagg6NorrDataGridViewTextBoxColumn
        // 
        this.avlagg6NorrDataGridViewTextBoxColumn.DataPropertyName = "Avlagg6Norr";
        this.avlagg6NorrDataGridViewTextBoxColumn.HeaderText = "Avlägg6Norr";
        this.avlagg6NorrDataGridViewTextBoxColumn.Name = "avlagg6NorrDataGridViewTextBoxColumn";
        // 
        // avlagg6OstDataGridViewTextBoxColumn
        // 
        this.avlagg6OstDataGridViewTextBoxColumn.DataPropertyName = "Avlagg6Ost";
        this.avlagg6OstDataGridViewTextBoxColumn.HeaderText = "Avlägg6Ost";
        this.avlagg6OstDataGridViewTextBoxColumn.Name = "avlagg6OstDataGridViewTextBoxColumn";
        // 
        // kommentarDataGridViewTextBoxColumn
        // 
        this.kommentarDataGridViewTextBoxColumn.DataPropertyName = "Kommentar";
        dataGridViewCellStyle98.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        dataGridViewCellStyle98.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.kommentarDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle98;
        this.kommentarDataGridViewTextBoxColumn.HeaderText = "Kommentar";
        this.kommentarDataGridViewTextBoxColumn.Name = "kommentarDataGridViewTextBoxColumn";
        this.kommentarDataGridViewTextBoxColumn.Width = 500;
        // 
        // dataSetUserData
        // 
        this.dataSetUserData.DataSetName = "DataSetUserData";
        this.dataSetUserData.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableUserData});
        // 
        // dataTableUserData
        // 
        this.dataTableUserData.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnRegionId,
            this.dataColumnDistriktId,
            this.dataColumnTraktnr,
            this.dataColumnStåndort,
            this.dataColumnEntreprenör,
            this.dataColumnRapportTyp,
            this.dataColumnTraktNamn,
            this.dataColumnRegionNamn,
            this.dataColumnDistriktNamn,
            this.dataColumnUrsprung,
            this.dataColumnDatum,
            this.dataColumnMarkberedningsmetod,
            this.dataColumnMålgrundyta,
            this.dataColumnStickvägsavstånd,
            this.dataColumnGISSträcka,
            this.dataColumnGISAreal,
            this.dataColumnProvytestorlek,
            this.dataColumnKommentar,
            this.dataColumnArtal,
            this.dataColumnAreal,
            this.dataColumnEntreprenörNamn,
            this.dataColumnAntalValtor,
            this.dataColumnAvstand,
            this.dataColumnBedomdVolym,
            this.dataColumnHygge,
            this.dataColumnVagkant,
            this.dataColumnAker,
            this.dataColumnLatbilshugg,
            this.dataColumnGrotbil,
            this.dataColumnTraktordragenhugg,
            this.dataColumnSkotarburenhugg,
            this.dataColumnAvlagg1Norr,
            this.dataColumnAvlagg1Ost,
            this.dataColumnAvlagg2Norr,
            this.dataColumnAvlagg2Ost,
            this.dataColumnAvlagg3Norr,
            this.dataColumnAvlagg3Ost,
            this.dataColumnAvlagg4Norr,
            this.dataColumnAvlagg4Ost,
            this.dataColumnAvlagg5Norr,
            this.dataColumnAvlagg5Ost,
            this.dataColumnAvlagg6Norr,
            this.dataColumnAvlagg6Ost,
            this.dataColumnStickvagssystem,
            this.dataColumnDatainsamlingsmetod,
            this.dataColumnInvTypId,
            this.dataColumnInvTypNamn,
            this.dataColumnAtgAreal});
        this.dataTableUserData.TableName = "UserData";
        // 
        // dataColumnRegionId
        // 
        this.dataColumnRegionId.ColumnName = "RegionId";
        this.dataColumnRegionId.DataType = typeof(int);
        // 
        // dataColumnDistriktId
        // 
        this.dataColumnDistriktId.ColumnName = "DistriktId";
        this.dataColumnDistriktId.DataType = typeof(int);
        // 
        // dataColumnTraktnr
        // 
        this.dataColumnTraktnr.ColumnName = "Traktnr";
        this.dataColumnTraktnr.DataType = typeof(int);
        // 
        // dataColumnStåndort
        // 
        this.dataColumnStåndort.ColumnName = "Standort";
        this.dataColumnStåndort.DataType = typeof(int);
        // 
        // dataColumnEntreprenör
        // 
        this.dataColumnEntreprenör.ColumnName = "Entreprenor";
        this.dataColumnEntreprenör.DataType = typeof(int);
        // 
        // dataColumnRapportTyp
        // 
        this.dataColumnRapportTyp.ColumnName = "RapportTyp";
        // 
        // dataColumnTraktNamn
        // 
        this.dataColumnTraktNamn.ColumnName = "TraktNamn";
        // 
        // dataColumnRegionNamn
        // 
        this.dataColumnRegionNamn.ColumnName = "RegionNamn";
        // 
        // dataColumnDistriktNamn
        // 
        this.dataColumnDistriktNamn.ColumnName = "DistriktNamn";
        // 
        // dataColumnUrsprung
        // 
        this.dataColumnUrsprung.ColumnName = "Ursprung";
        // 
        // dataColumnDatum
        // 
        this.dataColumnDatum.ColumnName = "Datum";
        this.dataColumnDatum.DataType = typeof(System.DateTime);
        // 
        // dataColumnMarkberedningsmetod
        // 
        this.dataColumnMarkberedningsmetod.ColumnName = "Markberedningsmetod";
        // 
        // dataColumnMålgrundyta
        // 
        this.dataColumnMålgrundyta.ColumnName = "Malgrundyta";
        this.dataColumnMålgrundyta.DataType = typeof(int);
        // 
        // dataColumnStickvägsavstånd
        // 
        this.dataColumnStickvägsavstånd.ColumnName = "Stickvagsavstand";
        this.dataColumnStickvägsavstånd.DataType = typeof(int);
        // 
        // dataColumnGISSträcka
        // 
        this.dataColumnGISSträcka.ColumnName = "GISStracka";
        this.dataColumnGISSträcka.DataType = typeof(int);
        // 
        // dataColumnGISAreal
        // 
        this.dataColumnGISAreal.ColumnName = "GISAreal";
        this.dataColumnGISAreal.DataType = typeof(double);
        // 
        // dataColumnProvytestorlek
        // 
        this.dataColumnProvytestorlek.ColumnName = "Provytestorlek";
        this.dataColumnProvytestorlek.DataType = typeof(int);
        // 
        // dataColumnKommentar
        // 
        this.dataColumnKommentar.ColumnName = "Kommentar";
        // 
        // dataColumnArtal
        // 
        this.dataColumnArtal.ColumnName = "Artal";
        this.dataColumnArtal.DataType = typeof(int);
        // 
        // dataColumnAreal
        // 
        this.dataColumnAreal.ColumnName = "Areal";
        this.dataColumnAreal.DataType = typeof(double);
        // 
        // dataColumnEntreprenörNamn
        // 
        this.dataColumnEntreprenörNamn.ColumnName = "EntreprenorNamn";
        // 
        // dataColumnAntalValtor
        // 
        this.dataColumnAntalValtor.ColumnName = "AntalValtor";
        this.dataColumnAntalValtor.DataType = typeof(int);
        // 
        // dataColumnAvstand
        // 
        this.dataColumnAvstand.ColumnName = "Avstand";
        this.dataColumnAvstand.DataType = typeof(int);
        // 
        // dataColumnBedomdVolym
        // 
        this.dataColumnBedomdVolym.ColumnName = "BedomdVolym";
        this.dataColumnBedomdVolym.DataType = typeof(int);
        // 
        // dataColumnHygge
        // 
        this.dataColumnHygge.ColumnName = "Hygge";
        this.dataColumnHygge.DataType = typeof(int);
        // 
        // dataColumnVagkant
        // 
        this.dataColumnVagkant.ColumnName = "Vagkant";
        this.dataColumnVagkant.DataType = typeof(int);
        // 
        // dataColumnAker
        // 
        this.dataColumnAker.ColumnName = "Aker";
        this.dataColumnAker.DataType = typeof(int);
        // 
        // dataColumnLatbilshugg
        // 
        this.dataColumnLatbilshugg.ColumnName = "Latbilshugg";
        this.dataColumnLatbilshugg.DataType = typeof(bool);
        // 
        // dataColumnGrotbil
        // 
        this.dataColumnGrotbil.ColumnName = "Grotbil";
        this.dataColumnGrotbil.DataType = typeof(bool);
        // 
        // dataColumnTraktordragenhugg
        // 
        this.dataColumnTraktordragenhugg.ColumnName = "Traktordragenhugg";
        this.dataColumnTraktordragenhugg.DataType = typeof(bool);
        // 
        // dataColumnSkotarburenhugg
        // 
        this.dataColumnSkotarburenhugg.ColumnName = "Skotarburenhugg";
        this.dataColumnSkotarburenhugg.DataType = typeof(bool);
        // 
        // dataColumnAvlagg1Norr
        // 
        this.dataColumnAvlagg1Norr.ColumnName = "Avlagg1Norr";
        this.dataColumnAvlagg1Norr.DataType = typeof(int);
        // 
        // dataColumnAvlagg1Ost
        // 
        this.dataColumnAvlagg1Ost.ColumnName = "Avlagg1Ost";
        this.dataColumnAvlagg1Ost.DataType = typeof(int);
        // 
        // dataColumnAvlagg2Norr
        // 
        this.dataColumnAvlagg2Norr.ColumnName = "Avlagg2Norr";
        this.dataColumnAvlagg2Norr.DataType = typeof(int);
        // 
        // dataColumnAvlagg2Ost
        // 
        this.dataColumnAvlagg2Ost.ColumnName = "Avlagg2Ost";
        this.dataColumnAvlagg2Ost.DataType = typeof(int);
        // 
        // dataColumnAvlagg3Norr
        // 
        this.dataColumnAvlagg3Norr.ColumnName = "Avlagg3Norr";
        this.dataColumnAvlagg3Norr.DataType = typeof(int);
        // 
        // dataColumnAvlagg3Ost
        // 
        this.dataColumnAvlagg3Ost.ColumnName = "Avlagg3Ost";
        this.dataColumnAvlagg3Ost.DataType = typeof(int);
        // 
        // dataColumnAvlagg4Norr
        // 
        this.dataColumnAvlagg4Norr.ColumnName = "Avlagg4Norr";
        this.dataColumnAvlagg4Norr.DataType = typeof(int);
        // 
        // dataColumnAvlagg4Ost
        // 
        this.dataColumnAvlagg4Ost.ColumnName = "Avlagg4Ost";
        this.dataColumnAvlagg4Ost.DataType = typeof(int);
        // 
        // dataColumnAvlagg5Norr
        // 
        this.dataColumnAvlagg5Norr.ColumnName = "Avlagg5Norr";
        this.dataColumnAvlagg5Norr.DataType = typeof(int);
        // 
        // dataColumnAvlagg5Ost
        // 
        this.dataColumnAvlagg5Ost.ColumnName = "Avlagg5Ost";
        this.dataColumnAvlagg5Ost.DataType = typeof(int);
        // 
        // dataColumnAvlagg6Norr
        // 
        this.dataColumnAvlagg6Norr.ColumnName = "Avlagg6Norr";
        this.dataColumnAvlagg6Norr.DataType = typeof(int);
        // 
        // dataColumnAvlagg6Ost
        // 
        this.dataColumnAvlagg6Ost.ColumnName = "Avlagg6Ost";
        this.dataColumnAvlagg6Ost.DataType = typeof(int);
        // 
        // dataColumnStickvagssystem
        // 
        this.dataColumnStickvagssystem.ColumnName = "Stickvagssystem";
        // 
        // dataColumnDatainsamlingsmetod
        // 
        this.dataColumnDatainsamlingsmetod.ColumnName = "Datainsamlingsmetod";
        // 
        // dataColumnInvTypId
        // 
        this.dataColumnInvTypId.ColumnName = "InvTypId";
        this.dataColumnInvTypId.DataType = typeof(int);
        // 
        // dataColumnInvTypNamn
        // 
        this.dataColumnInvTypNamn.ColumnName = "InvTypNamn";
        // 
        // dataColumnAtgAreal
        // 
        this.dataColumnAtgAreal.Caption = "Åtg areal";
        this.dataColumnAtgAreal.ColumnName = "AtgAreal";
        this.dataColumnAtgAreal.DataType = typeof(double);
        // 
        // dataSetValdFiltrering
        // 
        this.dataSetValdFiltrering.DataSetName = "DataSetValdFiltrering";
        this.dataSetValdFiltrering.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableVal});
        // 
        // dataTableVal
        // 
        this.dataTableVal.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnValRegion,
            this.dataColumnValDistrikt,
            this.dataColumnValTraktNr,
            this.dataColumnValTrakt,
            this.dataColumnValLagEntrNr,
            this.dataColumnValLagEntr,
            this.dataColumnValUrsprung,
            this.dataColumnValStandort,
            this.dataColumnValAr,
            this.dataColumnValRapportTyp,
            this.dataColumnValRapportFormat,
            this.dataColumnValInvTypID,
            this.dataColumnValInvTypNamn});
        this.dataTableVal.TableName = "TableVal";
        // 
        // dataColumnValRegion
        // 
        this.dataColumnValRegion.ColumnName = "ValRegion";
        // 
        // dataColumnValDistrikt
        // 
        this.dataColumnValDistrikt.ColumnName = "ValDistrikt";
        // 
        // dataColumnValTraktNr
        // 
        this.dataColumnValTraktNr.ColumnName = "ValTraktNr";
        // 
        // dataColumnValTrakt
        // 
        this.dataColumnValTrakt.ColumnName = "ValTrakt";
        // 
        // dataColumnValLagEntrNr
        // 
        this.dataColumnValLagEntrNr.ColumnName = "ValLagEntrNr";
        // 
        // dataColumnValLagEntr
        // 
        this.dataColumnValLagEntr.ColumnName = "ValLagEntr";
        // 
        // dataColumnValUrsprung
        // 
        this.dataColumnValUrsprung.ColumnName = "ValUrsprung";
        // 
        // dataColumnValStandort
        // 
        this.dataColumnValStandort.ColumnName = "ValStandort";
        // 
        // dataColumnValAr
        // 
        this.dataColumnValAr.ColumnName = "ValAr";
        // 
        // dataColumnValRapportTyp
        // 
        this.dataColumnValRapportTyp.ColumnName = "ValRapportTyp";
        this.dataColumnValRapportTyp.DataType = typeof(int);
        // 
        // dataColumnValRapportFormat
        // 
        this.dataColumnValRapportFormat.ColumnName = "ValRapportFormat";
        this.dataColumnValRapportFormat.DataType = typeof(int);
        // 
        // dataColumnValInvTypID
        // 
        this.dataColumnValInvTypID.ColumnName = "ValInvTypId";
        this.dataColumnValInvTypID.DataType = typeof(int);
        // 
        // dataColumnValInvTypNamn
        // 
        this.dataColumnValInvTypNamn.ColumnName = "ValInvTypNamn";
        // 
        // dataSetSammanstallningar
        // 
        this.dataSetSammanstallningar.DataSetName = "DataSetSammanstallningar";
        this.dataSetSammanstallningar.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableGallring,
            this.dataTableMarkberedning,
            this.dataTablePlantering,
            this.dataTableRojning,
            this.dataTableSlutavverkning,
            this.dataTableBiobransle});
        // 
        // dataTableGallring
        // 
        this.dataTableGallring.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnGRG,
            this.dataColumnGDI,
            this.dataColumnGUrsprung,
            this.dataColumnGAr,
            this.dataColumnGTraktnr,
            this.dataColumnGTraktnamn,
            this.dataColumnGStandort,
            this.dataColumnGEntreprenor,
            this.dataColumnGEntNamn,
            this.dataColumnGDatum,
            this.dataColumnGStickvagssystem,
            this.dataColumnGAreal,
            this.dataColumnGVagbredd,
            this.dataColumnGStickvagsAvstand,
            this.dataColumnGMalgrundyta,
            this.dataColumnGTall,
            this.dataColumnGGran,
            this.dataColumnGLov,
            this.dataColumnGContorta,
            this.dataColumnGSkador,
            this.dataColumnGTotGrundyta,
            this.dataColumnGMaluppfyllelse,
            this.dataColumnGFraga1,
            this.dataColumnGFraga2,
            this.dataColumnGFraga3,
            this.dataColumnGFraga4,
            this.dataColumnGFraga5,
            this.dataColumnGFraga6,
            this.dataColumnGFraga7,
            this.dataColumnGFraga8,
            this.dataColumnGFraga9,
            this.dataColumnGKommentar});
        this.dataTableGallring.TableName = "Gallring";
        // 
        // dataColumnGRG
        // 
        this.dataColumnGRG.ColumnName = "GRG";
        this.dataColumnGRG.DataType = typeof(int);
        // 
        // dataColumnGDI
        // 
        this.dataColumnGDI.ColumnName = "GDI";
        this.dataColumnGDI.DataType = typeof(int);
        // 
        // dataColumnGUrsprung
        // 
        this.dataColumnGUrsprung.ColumnName = "GUrsprung";
        // 
        // dataColumnGAr
        // 
        this.dataColumnGAr.ColumnName = "GAr";
        this.dataColumnGAr.DataType = typeof(int);
        // 
        // dataColumnGTraktnr
        // 
        this.dataColumnGTraktnr.ColumnName = "GTraktnr";
        this.dataColumnGTraktnr.DataType = typeof(int);
        // 
        // dataColumnGTraktnamn
        // 
        this.dataColumnGTraktnamn.ColumnName = "GTraktnamn";
        // 
        // dataColumnGStandort
        // 
        this.dataColumnGStandort.ColumnName = "GStandort";
        this.dataColumnGStandort.DataType = typeof(int);
        // 
        // dataColumnGEntreprenor
        // 
        this.dataColumnGEntreprenor.ColumnName = "GEntreprenor";
        this.dataColumnGEntreprenor.DataType = typeof(int);
        // 
        // dataColumnGEntNamn
        // 
        this.dataColumnGEntNamn.ColumnName = "GEntNamn";
        // 
        // dataColumnGDatum
        // 
        this.dataColumnGDatum.ColumnName = "GDatum";
        this.dataColumnGDatum.DataType = typeof(System.DateTime);
        // 
        // dataColumnGStickvagssystem
        // 
        this.dataColumnGStickvagssystem.ColumnName = "GStickvagssystem";
        // 
        // dataColumnGAreal
        // 
        this.dataColumnGAreal.ColumnName = "GAreal";
        this.dataColumnGAreal.DataType = typeof(double);
        // 
        // dataColumnGVagbredd
        // 
        this.dataColumnGVagbredd.ColumnName = "GVagbredd";
        this.dataColumnGVagbredd.DataType = typeof(double);
        // 
        // dataColumnGStickvagsAvstand
        // 
        this.dataColumnGStickvagsAvstand.ColumnName = "GStickvagsAvstand";
        this.dataColumnGStickvagsAvstand.DataType = typeof(int);
        // 
        // dataColumnGMalgrundyta
        // 
        this.dataColumnGMalgrundyta.ColumnName = "GMalgrundyta";
        this.dataColumnGMalgrundyta.DataType = typeof(int);
        // 
        // dataColumnGTall
        // 
        this.dataColumnGTall.ColumnName = "GTall";
        this.dataColumnGTall.DataType = typeof(double);
        // 
        // dataColumnGGran
        // 
        this.dataColumnGGran.ColumnName = "GGran";
        this.dataColumnGGran.DataType = typeof(double);
        // 
        // dataColumnGLov
        // 
        this.dataColumnGLov.ColumnName = "GLov";
        this.dataColumnGLov.DataType = typeof(double);
        // 
        // dataColumnGContorta
        // 
        this.dataColumnGContorta.ColumnName = "GContorta";
        this.dataColumnGContorta.DataType = typeof(double);
        // 
        // dataColumnGSkador
        // 
        this.dataColumnGSkador.ColumnName = "GSkador";
        this.dataColumnGSkador.DataType = typeof(double);
        // 
        // dataColumnGTotGrundyta
        // 
        this.dataColumnGTotGrundyta.ColumnName = "GTotGrundyta";
        this.dataColumnGTotGrundyta.DataType = typeof(double);
        // 
        // dataColumnGMaluppfyllelse
        // 
        this.dataColumnGMaluppfyllelse.ColumnName = "GMaluppfyllelse";
        this.dataColumnGMaluppfyllelse.DataType = typeof(int);
        // 
        // dataColumnGFraga1
        // 
        this.dataColumnGFraga1.ColumnName = "GFraga1";
        // 
        // dataColumnGFraga2
        // 
        this.dataColumnGFraga2.ColumnName = "GFraga2";
        // 
        // dataColumnGFraga3
        // 
        this.dataColumnGFraga3.ColumnName = "GFraga3";
        // 
        // dataColumnGFraga4
        // 
        this.dataColumnGFraga4.ColumnName = "GFraga4";
        // 
        // dataColumnGFraga5
        // 
        this.dataColumnGFraga5.ColumnName = "GFraga5";
        // 
        // dataColumnGFraga6
        // 
        this.dataColumnGFraga6.ColumnName = "GFraga6";
        // 
        // dataColumnGFraga7
        // 
        this.dataColumnGFraga7.ColumnName = "GFraga7";
        // 
        // dataColumnGFraga8
        // 
        this.dataColumnGFraga8.ColumnName = "GFraga8";
        // 
        // dataColumnGFraga9
        // 
        this.dataColumnGFraga9.ColumnName = "GFraga9";
        // 
        // dataColumnGKommentar
        // 
        this.dataColumnGKommentar.ColumnName = "GKommentar";
        // 
        // dataTableMarkberedning
        // 
        this.dataTableMarkberedning.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnMRG,
            this.dataColumnMDI,
            this.dataColumnMUrsprung,
            this.dataColumnMAr,
            this.dataColumnMTraktnr,
            this.dataColumnMTraktnamn,
            this.dataColumnMStandort,
            this.dataColumnMEntreprenor,
            this.dataColumnMEntNamn,
            this.dataColumnMDatum,
            this.dataColumnMBMetod,
            this.dataColumnMInsamlmetod,
            this.dataColumnMAreal,
            this.dataColumnMGISStracka,
            this.dataColumnMBStrackaPerHa,
            this.dataColumnMBMal,
            this.dataColumnMOptimalt,
            this.dataColumnMOptimaltBra,
            this.dataColumnMMaluppfyllelse,
            this.dataColumnMFraga1,
            this.dataColumnMFraga2,
            this.dataColumnMFraga3,
            this.dataColumnMFraga4,
            this.dataColumnMFraga5,
            this.dataColumnMFraga6,
            this.dataColumnMFraga7,
            this.dataColumnMKommentar,
            this.dataColumnMInvTypId});
        this.dataTableMarkberedning.TableName = "Markberedning";
        // 
        // dataColumnMRG
        // 
        this.dataColumnMRG.ColumnName = "MRG";
        this.dataColumnMRG.DataType = typeof(int);
        // 
        // dataColumnMDI
        // 
        this.dataColumnMDI.ColumnName = "MDI";
        this.dataColumnMDI.DataType = typeof(int);
        // 
        // dataColumnMUrsprung
        // 
        this.dataColumnMUrsprung.ColumnName = "MUrsprung";
        // 
        // dataColumnMAr
        // 
        this.dataColumnMAr.ColumnName = "MAr";
        this.dataColumnMAr.DataType = typeof(int);
        // 
        // dataColumnMTraktnr
        // 
        this.dataColumnMTraktnr.ColumnName = "MTraktnr";
        this.dataColumnMTraktnr.DataType = typeof(int);
        // 
        // dataColumnMTraktnamn
        // 
        this.dataColumnMTraktnamn.ColumnName = "MTraktnamn";
        // 
        // dataColumnMStandort
        // 
        this.dataColumnMStandort.ColumnName = "MStandort";
        this.dataColumnMStandort.DataType = typeof(int);
        // 
        // dataColumnMEntreprenor
        // 
        this.dataColumnMEntreprenor.ColumnName = "MEntreprenor";
        this.dataColumnMEntreprenor.DataType = typeof(int);
        // 
        // dataColumnMEntNamn
        // 
        this.dataColumnMEntNamn.ColumnName = "MEntNamn";
        // 
        // dataColumnMDatum
        // 
        this.dataColumnMDatum.ColumnName = "MDatum";
        this.dataColumnMDatum.DataType = typeof(System.DateTime);
        // 
        // dataColumnMBMetod
        // 
        this.dataColumnMBMetod.Caption = "MBMetod";
        this.dataColumnMBMetod.ColumnName = "MBMetod";
        // 
        // dataColumnMInsamlmetod
        // 
        this.dataColumnMInsamlmetod.ColumnName = "MInsamlmetod";
        // 
        // dataColumnMAreal
        // 
        this.dataColumnMAreal.ColumnName = "MAreal";
        this.dataColumnMAreal.DataType = typeof(double);
        // 
        // dataColumnMGISStracka
        // 
        this.dataColumnMGISStracka.ColumnName = "MGISStracka";
        this.dataColumnMGISStracka.DataType = typeof(int);
        // 
        // dataColumnMBStrackaPerHa
        // 
        this.dataColumnMBStrackaPerHa.ColumnName = "MBStrackaPerHa";
        this.dataColumnMBStrackaPerHa.DataType = typeof(double);
        // 
        // dataColumnMBMal
        // 
        this.dataColumnMBMal.ColumnName = "MBMal";
        this.dataColumnMBMal.DataType = typeof(int);
        // 
        // dataColumnMOptimalt
        // 
        this.dataColumnMOptimalt.ColumnName = "MOptimalt";
        this.dataColumnMOptimalt.DataType = typeof(double);
        // 
        // dataColumnMOptimaltBra
        // 
        this.dataColumnMOptimaltBra.ColumnName = "MOptimaltBra";
        this.dataColumnMOptimaltBra.DataType = typeof(double);
        // 
        // dataColumnMMaluppfyllelse
        // 
        this.dataColumnMMaluppfyllelse.ColumnName = "MMaluppfyllelse";
        this.dataColumnMMaluppfyllelse.DataType = typeof(double);
        // 
        // dataColumnMFraga1
        // 
        this.dataColumnMFraga1.ColumnName = "MFraga1";
        // 
        // dataColumnMFraga2
        // 
        this.dataColumnMFraga2.ColumnName = "MFraga2";
        // 
        // dataColumnMFraga3
        // 
        this.dataColumnMFraga3.ColumnName = "MFraga3";
        // 
        // dataColumnMFraga4
        // 
        this.dataColumnMFraga4.ColumnName = "MFraga4";
        // 
        // dataColumnMFraga5
        // 
        this.dataColumnMFraga5.ColumnName = "MFraga5";
        // 
        // dataColumnMFraga6
        // 
        this.dataColumnMFraga6.ColumnName = "MFraga6";
        // 
        // dataColumnMFraga7
        // 
        this.dataColumnMFraga7.ColumnName = "MFraga7";
        // 
        // dataColumnMKommentar
        // 
        this.dataColumnMKommentar.ColumnName = "MKommentar";
        // 
        // dataColumnMInvTypId
        // 
        this.dataColumnMInvTypId.ColumnName = "MInvTypId";
        this.dataColumnMInvTypId.DataType = typeof(int);
        // 
        // dataTablePlantering
        // 
        this.dataTablePlantering.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnPRG,
            this.dataColumnPDI,
            this.dataColumnPUrsprung,
            this.dataColumnPAr,
            this.dataColumnPTraktnr,
            this.dataColumnPTraktnamn,
            this.dataColumnPStandort,
            this.dataColumnPEntreprenor,
            this.dataColumnPEntnamn,
            this.dataColumnPDatum,
            this.dataColumnPProvytestorlek,
            this.dataColumnPOptimaltSatta,
            this.dataColumnPBraSatta,
            this.dataColumnPTotSatta,
            this.dataColumnPMal,
            this.dataColumnPUtnyttjandegrad,
            this.dataColumnPUPI,
            this.dataColumnPBraTilltryckning,
            this.dataColumnPBraDjup,
            this.dataColumnPMaluppfyllelse,
            this.dataColumnPFraga1,
            this.dataColumnPFraga2,
            this.dataColumnPTall,
            this.dataColumnPTPlantTyp,
            this.dataColumnPTStambrev,
            this.dataColumnPGran,
            this.dataColumnPGPlantTyp,
            this.dataColumnPGStamBrev,
            this.dataColumnPContorta,
            this.dataColumnPCPlantTyp,
            this.dataColumnPCStamBrev,
            this.dataColumnPKommentar,
            this.dataColumnPInvTypId,
            this.dataColumnPAtgAreal});
        this.dataTablePlantering.TableName = "Plantering";
        // 
        // dataColumnPRG
        // 
        this.dataColumnPRG.ColumnName = "PRG";
        this.dataColumnPRG.DataType = typeof(int);
        // 
        // dataColumnPDI
        // 
        this.dataColumnPDI.ColumnName = "PDI";
        this.dataColumnPDI.DataType = typeof(int);
        // 
        // dataColumnPUrsprung
        // 
        this.dataColumnPUrsprung.ColumnName = "PUrsprung";
        // 
        // dataColumnPAr
        // 
        this.dataColumnPAr.ColumnName = "PAr";
        this.dataColumnPAr.DataType = typeof(int);
        // 
        // dataColumnPTraktnr
        // 
        this.dataColumnPTraktnr.ColumnName = "PTraktnr";
        this.dataColumnPTraktnr.DataType = typeof(int);
        // 
        // dataColumnPTraktnamn
        // 
        this.dataColumnPTraktnamn.ColumnName = "PTraktnamn";
        // 
        // dataColumnPStandort
        // 
        this.dataColumnPStandort.ColumnName = "PStandort";
        this.dataColumnPStandort.DataType = typeof(int);
        // 
        // dataColumnPEntreprenor
        // 
        this.dataColumnPEntreprenor.ColumnName = "PEntreprenor";
        this.dataColumnPEntreprenor.DataType = typeof(int);
        // 
        // dataColumnPEntnamn
        // 
        this.dataColumnPEntnamn.ColumnName = "PEntnamn";
        // 
        // dataColumnPDatum
        // 
        this.dataColumnPDatum.ColumnName = "PDatum";
        this.dataColumnPDatum.DataType = typeof(System.DateTime);
        // 
        // dataColumnPProvytestorlek
        // 
        this.dataColumnPProvytestorlek.ColumnName = "PProvytestorlek";
        this.dataColumnPProvytestorlek.DataType = typeof(int);
        // 
        // dataColumnPOptimaltSatta
        // 
        this.dataColumnPOptimaltSatta.ColumnName = "POptimaltSatta";
        this.dataColumnPOptimaltSatta.DataType = typeof(double);
        // 
        // dataColumnPBraSatta
        // 
        this.dataColumnPBraSatta.ColumnName = "PBraSatta";
        this.dataColumnPBraSatta.DataType = typeof(double);
        // 
        // dataColumnPTotSatta
        // 
        this.dataColumnPTotSatta.ColumnName = "PTotSatta";
        this.dataColumnPTotSatta.DataType = typeof(double);
        // 
        // dataColumnPMal
        // 
        this.dataColumnPMal.ColumnName = "PMal";
        this.dataColumnPMal.DataType = typeof(int);
        // 
        // dataColumnPUtnyttjandegrad
        // 
        this.dataColumnPUtnyttjandegrad.ColumnName = "PUtnyttjandegrad";
        this.dataColumnPUtnyttjandegrad.DataType = typeof(int);
        // 
        // dataColumnPUPI
        // 
        this.dataColumnPUPI.ColumnName = "PUPI";
        this.dataColumnPUPI.DataType = typeof(int);
        // 
        // dataColumnPBraTilltryckning
        // 
        this.dataColumnPBraTilltryckning.ColumnName = "PBraTilltryckning";
        this.dataColumnPBraTilltryckning.DataType = typeof(int);
        // 
        // dataColumnPBraDjup
        // 
        this.dataColumnPBraDjup.ColumnName = "PBraDjup";
        this.dataColumnPBraDjup.DataType = typeof(int);
        // 
        // dataColumnPMaluppfyllelse
        // 
        this.dataColumnPMaluppfyllelse.ColumnName = "PMaluppfyllelse";
        this.dataColumnPMaluppfyllelse.DataType = typeof(double);
        // 
        // dataColumnPFraga1
        // 
        this.dataColumnPFraga1.ColumnName = "PFraga1";
        // 
        // dataColumnPFraga2
        // 
        this.dataColumnPFraga2.ColumnName = "PFraga2";
        // 
        // dataColumnPTall
        // 
        this.dataColumnPTall.ColumnName = "PTall";
        this.dataColumnPTall.DataType = typeof(int);
        // 
        // dataColumnPTPlantTyp
        // 
        this.dataColumnPTPlantTyp.Caption = "PTPlantTyp";
        this.dataColumnPTPlantTyp.ColumnName = "PTPlantTyp";
        // 
        // dataColumnPTStambrev
        // 
        this.dataColumnPTStambrev.ColumnName = "PTStambrev";
        // 
        // dataColumnPGran
        // 
        this.dataColumnPGran.ColumnName = "PGran";
        this.dataColumnPGran.DataType = typeof(int);
        // 
        // dataColumnPGPlantTyp
        // 
        this.dataColumnPGPlantTyp.ColumnName = "PGPlantTyp";
        // 
        // dataColumnPGStamBrev
        // 
        this.dataColumnPGStamBrev.ColumnName = "PGStamBrev";
        // 
        // dataColumnPContorta
        // 
        this.dataColumnPContorta.ColumnName = "PContorta";
        this.dataColumnPContorta.DataType = typeof(int);
        // 
        // dataColumnPCPlantTyp
        // 
        this.dataColumnPCPlantTyp.ColumnName = "PCPlantTyp";
        // 
        // dataColumnPCStamBrev
        // 
        this.dataColumnPCStamBrev.ColumnName = "PCStamBrev";
        // 
        // dataColumnPKommentar
        // 
        this.dataColumnPKommentar.ColumnName = "PKommentar";
        // 
        // dataColumnPInvTypId
        // 
        this.dataColumnPInvTypId.ColumnName = "PInvTypId";
        this.dataColumnPInvTypId.DataType = typeof(int);
        // 
        // dataColumnPAtgAreal
        // 
        this.dataColumnPAtgAreal.ColumnName = "PAtgAreal";
        this.dataColumnPAtgAreal.DataType = typeof(double);
        // 
        // dataTableRojning
        // 
        this.dataTableRojning.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnRRG,
            this.dataColumnRDI,
            this.dataColumnRUrsprung,
            this.dataColumnRAr,
            this.dataColumnRTraktnr,
            this.dataColumnRTraktnamn,
            this.dataColumnRStandort,
            this.dataColumnREntreprenor,
            this.dataColumnREntNamn,
            this.dataColumnRDatum,
            this.dataColumnRProvytestorlek,
            this.dataColumnRMalHuvudstam,
            this.dataColumnRHuvudstammar,
            this.dataColumnRMaluppfyllelse,
            this.dataColumnRBarrhuvudstammarsMedelhojd,
            this.dataColumnRT,
            this.dataColumnRG,
            this.dataColumnRL,
            this.dataColumnRC,
            this.dataColumnRFraga1,
            this.dataColumnRFraga2,
            this.dataColumnRFraga3,
            this.dataColumnRFraga4,
            this.dataColumnRFraga5,
            this.dataColumnRFraga6,
            this.dataColumnRKommentar,
            this.dataColumnRInvTypID,
            this.dataColumnRAtgAreal,
            this.dataColumnRLovhuvudstammarsMedelhojd,
            this.dataColumnRRojstamHa});
        this.dataTableRojning.TableName = "Rojning";
        // 
        // dataColumnRRG
        // 
        this.dataColumnRRG.ColumnName = "RRG";
        this.dataColumnRRG.DataType = typeof(int);
        // 
        // dataColumnRDI
        // 
        this.dataColumnRDI.ColumnName = "RDI";
        this.dataColumnRDI.DataType = typeof(int);
        // 
        // dataColumnRUrsprung
        // 
        this.dataColumnRUrsprung.ColumnName = "RUrsprung";
        // 
        // dataColumnRAr
        // 
        this.dataColumnRAr.ColumnName = "RAr";
        this.dataColumnRAr.DataType = typeof(int);
        // 
        // dataColumnRTraktnr
        // 
        this.dataColumnRTraktnr.ColumnName = "RTraktnr";
        this.dataColumnRTraktnr.DataType = typeof(int);
        // 
        // dataColumnRTraktnamn
        // 
        this.dataColumnRTraktnamn.ColumnName = "RTraktnamn";
        // 
        // dataColumnRStandort
        // 
        this.dataColumnRStandort.ColumnName = "RStandort";
        this.dataColumnRStandort.DataType = typeof(int);
        // 
        // dataColumnREntreprenor
        // 
        this.dataColumnREntreprenor.ColumnName = "REntreprenor";
        this.dataColumnREntreprenor.DataType = typeof(int);
        // 
        // dataColumnREntNamn
        // 
        this.dataColumnREntNamn.ColumnName = "REntNamn";
        // 
        // dataColumnRDatum
        // 
        this.dataColumnRDatum.ColumnName = "RDatum";
        this.dataColumnRDatum.DataType = typeof(System.DateTime);
        // 
        // dataColumnRProvytestorlek
        // 
        this.dataColumnRProvytestorlek.ColumnName = "RProvytestorlek";
        this.dataColumnRProvytestorlek.DataType = typeof(int);
        // 
        // dataColumnRMalHuvudstam
        // 
        this.dataColumnRMalHuvudstam.ColumnName = "RMalHuvudstam";
        this.dataColumnRMalHuvudstam.DataType = typeof(int);
        // 
        // dataColumnRHuvudstammar
        // 
        this.dataColumnRHuvudstammar.Caption = "RHuvudstammar";
        this.dataColumnRHuvudstammar.ColumnName = "RHuvudstammar";
        this.dataColumnRHuvudstammar.DataType = typeof(int);
        // 
        // dataColumnRMaluppfyllelse
        // 
        this.dataColumnRMaluppfyllelse.ColumnName = "RMaluppfyllelse";
        this.dataColumnRMaluppfyllelse.DataType = typeof(double);
        // 
        // dataColumnRBarrhuvudstammarsMedelhojd
        // 
        this.dataColumnRBarrhuvudstammarsMedelhojd.ColumnName = "RBarrhuvudstammarsMedelhojd";
        this.dataColumnRBarrhuvudstammarsMedelhojd.DataType = typeof(double);
        // 
        // dataColumnRT
        // 
        this.dataColumnRT.ColumnName = "RT";
        this.dataColumnRT.DataType = typeof(int);
        // 
        // dataColumnRG
        // 
        this.dataColumnRG.ColumnName = "RG";
        this.dataColumnRG.DataType = typeof(int);
        // 
        // dataColumnRL
        // 
        this.dataColumnRL.ColumnName = "RL";
        this.dataColumnRL.DataType = typeof(int);
        // 
        // dataColumnRC
        // 
        this.dataColumnRC.ColumnName = "RC";
        this.dataColumnRC.DataType = typeof(int);
        // 
        // dataColumnRFraga1
        // 
        this.dataColumnRFraga1.Caption = "RFraga1";
        this.dataColumnRFraga1.ColumnName = "RFraga1";
        // 
        // dataColumnRFraga2
        // 
        this.dataColumnRFraga2.ColumnName = "RFraga2";
        // 
        // dataColumnRFraga3
        // 
        this.dataColumnRFraga3.ColumnName = "RFraga3";
        // 
        // dataColumnRFraga4
        // 
        this.dataColumnRFraga4.ColumnName = "RFraga4";
        // 
        // dataColumnRFraga5
        // 
        this.dataColumnRFraga5.ColumnName = "RFraga5";
        // 
        // dataColumnRFraga6
        // 
        this.dataColumnRFraga6.ColumnName = "RFraga6";
        // 
        // dataColumnRKommentar
        // 
        this.dataColumnRKommentar.ColumnName = "RKommentar";
        // 
        // dataColumnRInvTypID
        // 
        this.dataColumnRInvTypID.ColumnName = "RInvTypId";
        this.dataColumnRInvTypID.DataType = typeof(int);
        // 
        // dataColumnRAtgAreal
        // 
        this.dataColumnRAtgAreal.ColumnName = "RAtgAreal";
        this.dataColumnRAtgAreal.DataType = typeof(double);
        // 
        // dataTableSlutavverkning
        // 
        this.dataTableSlutavverkning.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSRG,
            this.dataColumnSDI,
            this.dataColumnSUrsprung,
            this.dataColumnSAr,
            this.dataColumnSTraktnr,
            this.dataColumnSTraktnamn,
            this.dataColumnSStandort,
            this.dataColumnSEntreprenor,
            this.dataColumnSEntrnamn,
            this.dataColumnSDatum,
            this.dataColumnSFraga1,
            this.dataColumnSFraga2a,
            this.dataColumnSFraga2b,
            this.dataColumnSFraga2c,
            this.dataColumnSFraga2d,
            this.dataColumnSFraga2e,
            this.dataColumnSFraga2f,
            this.dataColumnSFraga2g,
            this.dataColumnSFraga2h,
            this.dataColumnSFraga3,
            this.dataColumnSFraga4a,
            this.dataColumnSFraga4b,
            this.dataColumnSFraga5,
            this.dataColumnSFraga6,
            this.dataColumnSFraga7,
            this.dataColumnSFraga8,
            this.dataColumnSFraga9,
            this.dataColumnSFraga10,
            this.dataColumnSFraga11,
            this.dataColumnSFraga12,
            this.dataColumnSKommentar});
        this.dataTableSlutavverkning.TableName = "Slutavverkning";
        // 
        // dataColumnSRG
        // 
        this.dataColumnSRG.ColumnName = "SRG";
        this.dataColumnSRG.DataType = typeof(int);
        // 
        // dataColumnSDI
        // 
        this.dataColumnSDI.ColumnName = "SDI";
        this.dataColumnSDI.DataType = typeof(int);
        // 
        // dataColumnSUrsprung
        // 
        this.dataColumnSUrsprung.ColumnName = "SUrsprung";
        // 
        // dataColumnSAr
        // 
        this.dataColumnSAr.ColumnName = "SAr";
        this.dataColumnSAr.DataType = typeof(int);
        // 
        // dataColumnSTraktnr
        // 
        this.dataColumnSTraktnr.ColumnName = "STraktnr";
        this.dataColumnSTraktnr.DataType = typeof(int);
        // 
        // dataColumnSTraktnamn
        // 
        this.dataColumnSTraktnamn.ColumnName = "STraktnamn";
        // 
        // dataColumnSStandort
        // 
        this.dataColumnSStandort.ColumnName = "SStandort";
        this.dataColumnSStandort.DataType = typeof(int);
        // 
        // dataColumnSEntreprenor
        // 
        this.dataColumnSEntreprenor.ColumnName = "SEntreprenor";
        this.dataColumnSEntreprenor.DataType = typeof(int);
        // 
        // dataColumnSEntrnamn
        // 
        this.dataColumnSEntrnamn.ColumnName = "SEntrnamn";
        // 
        // dataColumnSDatum
        // 
        this.dataColumnSDatum.ColumnName = "SDatum";
        this.dataColumnSDatum.DataType = typeof(System.DateTime);
        // 
        // dataColumnSFraga1
        // 
        this.dataColumnSFraga1.ColumnName = "SFraga1";
        // 
        // dataColumnSFraga2a
        // 
        this.dataColumnSFraga2a.ColumnName = "SFraga2a";
        // 
        // dataColumnSFraga2b
        // 
        this.dataColumnSFraga2b.ColumnName = "SFraga2b";
        // 
        // dataColumnSFraga2c
        // 
        this.dataColumnSFraga2c.ColumnName = "SFraga2c";
        // 
        // dataColumnSFraga2d
        // 
        this.dataColumnSFraga2d.ColumnName = "SFraga2d";
        // 
        // dataColumnSFraga2e
        // 
        this.dataColumnSFraga2e.ColumnName = "SFraga2e";
        // 
        // dataColumnSFraga2f
        // 
        this.dataColumnSFraga2f.ColumnName = "SFraga2f";
        // 
        // dataColumnSFraga2g
        // 
        this.dataColumnSFraga2g.ColumnName = "SFraga2g";
        // 
        // dataColumnSFraga2h
        // 
        this.dataColumnSFraga2h.ColumnName = "SFraga2h";
        // 
        // dataColumnSFraga3
        // 
        this.dataColumnSFraga3.ColumnName = "SFraga3";
        // 
        // dataColumnSFraga4a
        // 
        this.dataColumnSFraga4a.ColumnName = "SFraga4a";
        // 
        // dataColumnSFraga4b
        // 
        this.dataColumnSFraga4b.ColumnName = "SFraga4b";
        // 
        // dataColumnSFraga5
        // 
        this.dataColumnSFraga5.ColumnName = "SFraga5";
        // 
        // dataColumnSFraga6
        // 
        this.dataColumnSFraga6.ColumnName = "SFraga6";
        // 
        // dataColumnSFraga7
        // 
        this.dataColumnSFraga7.ColumnName = "SFraga7";
        // 
        // dataColumnSFraga8
        // 
        this.dataColumnSFraga8.ColumnName = "SFraga8";
        // 
        // dataColumnSFraga9
        // 
        this.dataColumnSFraga9.ColumnName = "SFraga9";
        // 
        // dataColumnSFraga10
        // 
        this.dataColumnSFraga10.ColumnName = "SFraga10";
        // 
        // dataColumnSFraga11
        // 
        this.dataColumnSFraga11.ColumnName = "SFraga11";
        // 
        // dataColumnSFraga12
        // 
        this.dataColumnSFraga12.ColumnName = "SFraga12";
        // 
        // dataColumnSKommentar
        // 
        this.dataColumnSKommentar.ColumnName = "SKommentar";
        // 
        // dataTableBiobransle
        // 
        this.dataTableBiobransle.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnBRG,
            this.dataColumnBDI,
            this.dataColumnBUrsprung,
            this.dataColumnBAr,
            this.dataColumnBTraktnr,
            this.dataColumnBTraktnamn,
            this.dataColumnBStandort,
            this.dataColumnBEntreprenor,
            this.dataColumnBEntnamn,
            this.dataColumnBDatum,
            this.dataColumnBValtorPaHygge,
            this.dataColumnBValtorPaVagkant,
            this.dataColumnBValtorPaAker,
            this.dataColumnBLastbilshugg,
            this.dataColumnBGrotbil,
            this.dataColumnBTraktorhugg,
            this.dataColumnBSkotarhugg,
            this.dataColumnBFraga1,
            this.dataColumnBFraga2,
            this.dataColumnBFraga3,
            this.dataColumnBFraga4,
            this.dataColumnBFraga5,
            this.dataColumnBFraga6,
            this.dataColumnBFraga7,
            this.dataColumnBFraga8,
            this.dataColumnBFraga9,
            this.dataColumnBFraga10,
            this.dataColumnBFraga11,
            this.dataColumnBFraga12,
            this.dataColumnBFraga13,
            this.dataColumnBFraga14,
            this.dataColumnBFraga15,
            this.dataColumnBFraga16,
            this.dataColumnBFraga17,
            this.dataColumnBFraga18,
            this.dataColumnBFraga19,
            this.dataColumnBKommentar});
        this.dataTableBiobransle.TableName = "Biobransle";
        // 
        // dataColumnBRG
        // 
        this.dataColumnBRG.ColumnName = "BRG";
        this.dataColumnBRG.DataType = typeof(int);
        // 
        // dataColumnBDI
        // 
        this.dataColumnBDI.ColumnName = "BDI";
        this.dataColumnBDI.DataType = typeof(int);
        // 
        // dataColumnBUrsprung
        // 
        this.dataColumnBUrsprung.ColumnName = "BUrsprung";
        // 
        // dataColumnBAr
        // 
        this.dataColumnBAr.ColumnName = "BAr";
        // 
        // dataColumnBTraktnr
        // 
        this.dataColumnBTraktnr.ColumnName = "BTraktnr";
        this.dataColumnBTraktnr.DataType = typeof(int);
        // 
        // dataColumnBTraktnamn
        // 
        this.dataColumnBTraktnamn.ColumnName = "BTraktnamn";
        // 
        // dataColumnBStandort
        // 
        this.dataColumnBStandort.ColumnName = "BStandort";
        this.dataColumnBStandort.DataType = typeof(int);
        // 
        // dataColumnBEntreprenor
        // 
        this.dataColumnBEntreprenor.ColumnName = "BEntreprenor";
        this.dataColumnBEntreprenor.DataType = typeof(int);
        // 
        // dataColumnBEntnamn
        // 
        this.dataColumnBEntnamn.ColumnName = "BEntnamn";
        // 
        // dataColumnBDatum
        // 
        this.dataColumnBDatum.ColumnName = "BDatum";
        this.dataColumnBDatum.DataType = typeof(System.DateTime);
        // 
        // dataColumnBValtorPaHygge
        // 
        this.dataColumnBValtorPaHygge.ColumnName = "BValtorPaHygge";
        this.dataColumnBValtorPaHygge.DataType = typeof(int);
        // 
        // dataColumnBValtorPaVagkant
        // 
        this.dataColumnBValtorPaVagkant.ColumnName = "BValtorPaVagkant";
        this.dataColumnBValtorPaVagkant.DataType = typeof(int);
        // 
        // dataColumnBValtorPaAker
        // 
        this.dataColumnBValtorPaAker.ColumnName = "BValtorPaAker";
        this.dataColumnBValtorPaAker.DataType = typeof(int);
        // 
        // dataColumnBLastbilshugg
        // 
        this.dataColumnBLastbilshugg.ColumnName = "BLastbilshugg";
        this.dataColumnBLastbilshugg.DataType = typeof(bool);
        // 
        // dataColumnBGrotbil
        // 
        this.dataColumnBGrotbil.ColumnName = "BGrotbil";
        this.dataColumnBGrotbil.DataType = typeof(bool);
        // 
        // dataColumnBTraktorhugg
        // 
        this.dataColumnBTraktorhugg.ColumnName = "BTraktorhugg";
        this.dataColumnBTraktorhugg.DataType = typeof(bool);
        // 
        // dataColumnBSkotarhugg
        // 
        this.dataColumnBSkotarhugg.ColumnName = "BSkotarhugg";
        this.dataColumnBSkotarhugg.DataType = typeof(bool);
        // 
        // dataColumnBFraga1
        // 
        this.dataColumnBFraga1.ColumnName = "BFraga1";
        // 
        // dataColumnBFraga2
        // 
        this.dataColumnBFraga2.ColumnName = "BFraga2";
        // 
        // dataColumnBFraga3
        // 
        this.dataColumnBFraga3.ColumnName = "BFraga3";
        // 
        // dataColumnBFraga4
        // 
        this.dataColumnBFraga4.ColumnName = "BFraga4";
        // 
        // dataColumnBFraga5
        // 
        this.dataColumnBFraga5.ColumnName = "BFraga5";
        // 
        // dataColumnBFraga6
        // 
        this.dataColumnBFraga6.ColumnName = "BFraga6";
        // 
        // dataColumnBFraga7
        // 
        this.dataColumnBFraga7.ColumnName = "BFraga7";
        // 
        // dataColumnBFraga8
        // 
        this.dataColumnBFraga8.ColumnName = "BFraga8";
        // 
        // dataColumnBFraga9
        // 
        this.dataColumnBFraga9.ColumnName = "BFraga9";
        // 
        // dataColumnBFraga10
        // 
        this.dataColumnBFraga10.ColumnName = "BFraga10";
        // 
        // dataColumnBFraga11
        // 
        this.dataColumnBFraga11.ColumnName = "BFraga11";
        // 
        // dataColumnBFraga12
        // 
        this.dataColumnBFraga12.ColumnName = "BFraga12";
        // 
        // dataColumnBFraga13
        // 
        this.dataColumnBFraga13.ColumnName = "BFraga13";
        // 
        // dataColumnBFraga14
        // 
        this.dataColumnBFraga14.ColumnName = "BFraga14";
        // 
        // dataColumnBFraga15
        // 
        this.dataColumnBFraga15.ColumnName = "BFraga15";
        // 
        // dataColumnBFraga16
        // 
        this.dataColumnBFraga16.ColumnName = "BFraga16";
        // 
        // dataColumnBFraga17
        // 
        this.dataColumnBFraga17.ColumnName = "BFraga17";
        // 
        // dataColumnBFraga18
        // 
        this.dataColumnBFraga18.ColumnName = "BFraga18";
        // 
        // dataColumnBFraga19
        // 
        this.dataColumnBFraga19.ColumnName = "BFraga19";
        // 
        // dataColumnBKommentar
        // 
        this.dataColumnBKommentar.ColumnName = "BKommentar";
        // 
        // environmentSettingsExporteraRapporter
        // 
        designerSettings4.ApplicationConnection = null;
        designerSettings4.DefaultFont = new System.Drawing.Font("Arial", 10F);
        designerSettings4.Icon = ((System.Drawing.Icon)(resources.GetObject("designerSettings4.Icon")));
        designerSettings4.Restrictions = designerRestrictions4;
        designerSettings4.Text = "";
        this.environmentSettingsExporteraRapporter.DesignerSettings = designerSettings4;
        emailSettings4.Address = "";
        emailSettings4.Host = "";
        emailSettings4.MessageTemplate = "";
        emailSettings4.Name = "";
        emailSettings4.Password = "";
        emailSettings4.Port = 49;
        emailSettings4.UserName = "";
        this.environmentSettingsExporteraRapporter.EmailSettings = emailSettings4;
        previewSettings4.Buttons = ((FastReport.PreviewButtons)((((((FastReport.PreviewButtons.Print | FastReport.PreviewButtons.Save)
                    | FastReport.PreviewButtons.Find)
                    | FastReport.PreviewButtons.Zoom)
                    | FastReport.PreviewButtons.Navigator)
                    | FastReport.PreviewButtons.Close)));
        previewSettings4.Icon = ((System.Drawing.Icon)(resources.GetObject("previewSettings4.Icon")));
        previewSettings4.Text = "";
        this.environmentSettingsExporteraRapporter.PreviewSettings = previewSettings4;
        this.environmentSettingsExporteraRapporter.ReportSettings = reportSettings4;
        this.environmentSettingsExporteraRapporter.UIStyle = FastReport.Utils.UIStyle.Office2007Black;
        // 
        // contextMenuStripExportera
        // 
        this.contextMenuStripExportera.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemExcel2007,
            this.toolStripMenuItemPdf});
        this.contextMenuStripExportera.Name = "contextMenuStripExportera";
        this.contextMenuStripExportera.Size = new System.Drawing.Size(128, 48);
        // 
        // toolStripMenuItemExcel2007
        // 
        this.toolStripMenuItemExcel2007.Name = "toolStripMenuItemExcel2007";
        this.toolStripMenuItemExcel2007.Size = new System.Drawing.Size(127, 22);
        this.toolStripMenuItemExcel2007.Text = "Excel 2007";
        this.toolStripMenuItemExcel2007.Click += new System.EventHandler(this.toolStripMenuItemExcel2007_Click);
        // 
        // toolStripMenuItemPdf
        // 
        this.toolStripMenuItemPdf.Name = "toolStripMenuItemPdf";
        this.toolStripMenuItemPdf.Size = new System.Drawing.Size(127, 22);
        this.toolStripMenuItemPdf.Text = "Adobe PDF";
        this.toolStripMenuItemPdf.Click += new System.EventHandler(this.toolStripMenuItemPdf_Click);
        // 
        // contextMenuStripForhandsgranska
        // 
        this.contextMenuStripForhandsgranska.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemForhandsgranskaxcel2007,
            this.toolStripMenuItemForhandsgranskaPdf});
        this.contextMenuStripForhandsgranska.Name = "contextMenuStripExportera";
        this.contextMenuStripForhandsgranska.Size = new System.Drawing.Size(128, 48);
        // 
        // toolStripMenuItemForhandsgranskaxcel2007
        // 
        this.toolStripMenuItemForhandsgranskaxcel2007.Name = "toolStripMenuItemForhandsgranskaxcel2007";
        this.toolStripMenuItemForhandsgranskaxcel2007.Size = new System.Drawing.Size(127, 22);
        this.toolStripMenuItemForhandsgranskaxcel2007.Text = "Excel 2007";
        this.toolStripMenuItemForhandsgranskaxcel2007.Click += new System.EventHandler(this.toolStripMenuItemForhandsgranskaxcel2007_Click);
        // 
        // toolStripMenuItemForhandsgranskaPdf
        // 
        this.toolStripMenuItemForhandsgranskaPdf.Name = "toolStripMenuItemForhandsgranskaPdf";
        this.toolStripMenuItemForhandsgranskaPdf.Size = new System.Drawing.Size(127, 22);
        this.toolStripMenuItemForhandsgranskaPdf.Text = "Adobe PDF";
        this.toolStripMenuItemForhandsgranskaPdf.Click += new System.EventHandler(this.toolStripMenuItemForhandsgranskaPdf_Click);
        // 
        // reportExporteraSammanstallning
        // 
        this.reportExporteraSammanstallning.ReportResourceString = resources.GetString("reportExporteraSammanstallning.ReportResourceString");
        this.reportExporteraSammanstallning.RegisterData(this.dataSetSammanstallningar, "dataSetSammanstallningar");
        this.reportExporteraSammanstallning.RegisterData(this.dataSetValdFiltrering, "dataSetValdFiltrering");
        // 
        // dataColumnRLovhuvudstammarsMedelhojd
        // 
        this.dataColumnRLovhuvudstammarsMedelhojd.ColumnName = "RLovhuvudstammarsMedelhojd";
        this.dataColumnRLovhuvudstammarsMedelhojd.DataType = typeof(double);
        // 
        // dataColumnRRojstamHa
        // 
        this.dataColumnRRojstamHa.ColumnName = "RRojstamHa";
        this.dataColumnRRojstamHa.DataType = typeof(double);
        // 
        // FormListaRapporter
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.Controls.Add(this.groupBoxRapporter);
        this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.Name = "FormListaRapporter";
        this.Size = new System.Drawing.Size(908, 607);
        this.Load += new System.EventHandler(this.FormListaRapporter_Load);
        this.groupBoxRapporter.ResumeLayout(false);
        this.groupBoxRapporttyp.ResumeLayout(false);
        this.groupBoxRapporttyp.PerformLayout();
        this.groupBox1.ResumeLayout(false);
        ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRapporter)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetUserData)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableUserData)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetValdFiltrering)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableVal)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetSammanstallningar)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableGallring)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableMarkberedning)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTablePlantering)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableRojning)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableSlutavverkning)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableBiobransle)).EndInit();
        this.contextMenuStripExportera.ResumeLayout(false);
        this.contextMenuStripForhandsgranska.ResumeLayout(false);
        ((System.ComponentModel.ISupportInitialize)(this.reportExporteraSammanstallning)).EndInit();
        this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox groupBoxRapporter;
    private System.Windows.Forms.DataGridView dataGridViewRapporter;
    private System.Data.DataSet dataSetUserData;
    private System.Data.DataTable dataTableUserData;
    private System.Data.DataColumn dataColumnRegionId;
    private System.Data.DataColumn dataColumnDistriktId;
    private System.Data.DataColumn dataColumnTraktnr;
    private System.Data.DataColumn dataColumnStåndort;
    private System.Data.DataColumn dataColumnEntreprenör;
    private System.Data.DataColumn dataColumnRapportTyp;
    private System.Data.DataColumn dataColumnTraktNamn;
    private System.Data.DataColumn dataColumnRegionNamn;
    private System.Data.DataColumn dataColumnDistriktNamn;
    private System.Data.DataColumn dataColumnUrsprung;
    private System.Data.DataColumn dataColumnDatum;
    private System.Data.DataColumn dataColumnMarkberedningsmetod;
    private System.Data.DataColumn dataColumnMålgrundyta;
    private System.Data.DataColumn dataColumnStickvägsavstånd;
    private System.Data.DataColumn dataColumnGISSträcka;
    private System.Data.DataColumn dataColumnGISAreal;
    private System.Data.DataColumn dataColumnProvytestorlek;
    private System.Data.DataColumn dataColumnKommentar;
    private System.Windows.Forms.ImageList imageList32;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.Button buttonGåTillHanteraData;
    private System.Windows.Forms.Button buttonLagraÄndratData;
    private System.Windows.Forms.Button buttonUppdateraData;
    private System.Windows.Forms.Button buttonFiltreraData;
    private System.Windows.Forms.Button buttonTaBortData;
    private System.Windows.Forms.Label labelFiltrering;
    private System.Data.DataColumn dataColumnArtal;
    private System.Data.DataColumn dataColumnAreal;
    private System.Data.DataColumn dataColumnEntreprenörNamn;
    private System.Windows.Forms.GroupBox groupBoxRapporttyp;
    private System.Windows.Forms.RadioButton radioButtonRöjning;
    private System.Windows.Forms.RadioButton radioButtonGallring;
    private System.Windows.Forms.RadioButton radioButtonPlantering1;
    private System.Windows.Forms.RadioButton radioButtonMarkberedning;
    private System.Windows.Forms.RadioButton radioButtonSlutavverkning;
    private System.Windows.Forms.RadioButton radioButtonBiobränsle;
    private System.Data.DataColumn dataColumnAntalValtor;
    private System.Data.DataColumn dataColumnAvstand;
    private System.Data.DataColumn dataColumnBedomdVolym;
    private System.Data.DataColumn dataColumnHygge;
    private System.Data.DataColumn dataColumnVagkant;
    private System.Data.DataColumn dataColumnAker;
    private System.Data.DataColumn dataColumnLatbilshugg;
    private System.Data.DataColumn dataColumnGrotbil;
    private System.Data.DataColumn dataColumnTraktordragenhugg;
    private System.Data.DataColumn dataColumnSkotarburenhugg;
    private System.Data.DataColumn dataColumnAvlagg1Norr;
    private System.Data.DataColumn dataColumnAvlagg1Ost;
    private System.Data.DataColumn dataColumnAvlagg2Norr;
    private System.Data.DataColumn dataColumnAvlagg2Ost;
    private System.Data.DataColumn dataColumnAvlagg3Norr;
    private System.Data.DataColumn dataColumnAvlagg3Ost;
    private System.Data.DataColumn dataColumnAvlagg4Norr;
    private System.Data.DataColumn dataColumnAvlagg4Ost;
    private System.Data.DataColumn dataColumnAvlagg5Norr;
    private System.Data.DataColumn dataColumnAvlagg5Ost;
    private System.Data.DataColumn dataColumnAvlagg6Norr;
    private System.Data.DataColumn dataColumnAvlagg6Ost;
    private System.Data.DataColumn dataColumnStickvagssystem;
    private System.Data.DataColumn dataColumnDatainsamlingsmetod;
    private System.Windows.Forms.Button buttonExportera;
    private FastReport.EnvironmentSettings environmentSettingsExporteraRapporter;
    private System.Data.DataSet dataSetValdFiltrering;
    private System.Data.DataTable dataTableVal;
    private System.Data.DataColumn dataColumnValRegion;
    private System.Data.DataColumn dataColumnValDistrikt;
    private System.Data.DataColumn dataColumnValTraktNr;
    private System.Data.DataColumn dataColumnValTrakt;
    private System.Data.DataColumn dataColumnValLagEntrNr;
    private System.Data.DataColumn dataColumnValLagEntr;
    private System.Data.DataColumn dataColumnValUrsprung;
    private System.Data.DataColumn dataColumnValStandort;
    private System.Data.DataColumn dataColumnValAr;
    private System.Data.DataColumn dataColumnValRapportTyp;
    private System.Data.DataColumn dataColumnValRapportFormat;
    private System.Windows.Forms.ContextMenuStrip contextMenuStripExportera;
    private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemExcel2007;
    private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemPdf;
    private System.Windows.Forms.Button buttonForhandsgranska;
    private System.Windows.Forms.ContextMenuStrip contextMenuStripForhandsgranska;
    private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemForhandsgranskaxcel2007;
    private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemForhandsgranskaPdf;
    private System.Windows.Forms.SaveFileDialog saveFileDialogExporteraSammanstallningsRapport;
    private System.Data.DataSet dataSetSammanstallningar;
    private System.Data.DataTable dataTableGallring;
    private System.Data.DataColumn dataColumnGRG;
    private System.Data.DataColumn dataColumnGDI;
    private System.Data.DataColumn dataColumnGUrsprung;
    private System.Data.DataColumn dataColumnGAr;
    private System.Data.DataColumn dataColumnGTraktnr;
    private System.Data.DataColumn dataColumnGTraktnamn;
    private System.Data.DataColumn dataColumnGStandort;
    private System.Data.DataColumn dataColumnGEntreprenor;
    private System.Data.DataColumn dataColumnGEntNamn;
    private System.Data.DataColumn dataColumnGDatum;
    private System.Data.DataColumn dataColumnGStickvagssystem;
    private System.Data.DataColumn dataColumnGAreal;
    private System.Data.DataColumn dataColumnGVagbredd;
    private System.Data.DataColumn dataColumnGStickvagsAvstand;
    private System.Data.DataColumn dataColumnGMalgrundyta;
    private System.Data.DataColumn dataColumnGTall;
    private System.Data.DataColumn dataColumnGGran;
    private System.Data.DataColumn dataColumnGLov;
    private System.Data.DataColumn dataColumnGContorta;
    private System.Data.DataColumn dataColumnGSkador;
    private System.Data.DataColumn dataColumnGTotGrundyta;
    private System.Data.DataColumn dataColumnGMaluppfyllelse;
    private System.Data.DataColumn dataColumnGFraga1;
    private System.Data.DataColumn dataColumnGFraga2;
    private System.Data.DataColumn dataColumnGFraga3;
    private System.Data.DataColumn dataColumnGFraga4;
    private System.Data.DataColumn dataColumnGFraga5;
    private System.Data.DataColumn dataColumnGFraga6;
    private System.Data.DataColumn dataColumnGFraga7;
    private System.Data.DataColumn dataColumnGFraga8;
    private System.Data.DataColumn dataColumnGFraga9;
    private System.Data.DataColumn dataColumnGKommentar;
    private FastReport.Report reportExporteraSammanstallning;
    private System.Data.DataTable dataTableMarkberedning;
    private System.Data.DataColumn dataColumnMRG;
    private System.Data.DataColumn dataColumnMDI;
    private System.Data.DataColumn dataColumnMUrsprung;
    private System.Data.DataColumn dataColumnMAr;
    private System.Data.DataColumn dataColumnMTraktnr;
    private System.Data.DataColumn dataColumnMTraktnamn;
    private System.Data.DataColumn dataColumnMStandort;
    private System.Data.DataColumn dataColumnMEntreprenor;
    private System.Data.DataColumn dataColumnMEntNamn;
    private System.Data.DataColumn dataColumnMDatum;
    private System.Data.DataColumn dataColumnMBMetod;
    private System.Data.DataColumn dataColumnMInsamlmetod;
    private System.Data.DataColumn dataColumnMAreal;
    private System.Data.DataColumn dataColumnMGISStracka;
    private System.Data.DataColumn dataColumnMBStrackaPerHa;
    private System.Data.DataColumn dataColumnMBMal;
    private System.Data.DataColumn dataColumnMOptimalt;
    private System.Data.DataColumn dataColumnMOptimaltBra;
    private System.Data.DataColumn dataColumnMMaluppfyllelse;
    private System.Data.DataColumn dataColumnMFraga1;
    private System.Data.DataColumn dataColumnMFraga2;
    private System.Data.DataColumn dataColumnMFraga3;
    private System.Data.DataColumn dataColumnMFraga4;
    private System.Data.DataColumn dataColumnMFraga5;
    private System.Data.DataColumn dataColumnMFraga6;
    private System.Data.DataColumn dataColumnMFraga7;
    private System.Data.DataColumn dataColumnMKommentar;
    private System.Data.DataTable dataTablePlantering;
    private System.Data.DataColumn dataColumnPRG;
    private System.Data.DataColumn dataColumnPDI;
    private System.Data.DataColumn dataColumnPUrsprung;
    private System.Data.DataColumn dataColumnPAr;
    private System.Data.DataColumn dataColumnPTraktnr;
    private System.Data.DataColumn dataColumnPTraktnamn;
    private System.Data.DataColumn dataColumnPStandort;
    private System.Data.DataColumn dataColumnPEntreprenor;
    private System.Data.DataColumn dataColumnPEntnamn;
    private System.Data.DataColumn dataColumnPDatum;
    private System.Data.DataColumn dataColumnPProvytestorlek;
    private System.Data.DataColumn dataColumnPOptimaltSatta;
    private System.Data.DataColumn dataColumnPBraSatta;
    private System.Data.DataColumn dataColumnPTotSatta;
    private System.Data.DataColumn dataColumnPMal;
    private System.Data.DataColumn dataColumnPUtnyttjandegrad;
    private System.Data.DataColumn dataColumnPUPI;
    private System.Data.DataColumn dataColumnPBraTilltryckning;
    private System.Data.DataColumn dataColumnPBraDjup;
    private System.Data.DataColumn dataColumnPMaluppfyllelse;
    private System.Data.DataColumn dataColumnPFraga1;
    private System.Data.DataColumn dataColumnPFraga2;
    private System.Data.DataColumn dataColumnPTall;
    private System.Data.DataColumn dataColumnPTPlantTyp;
    private System.Data.DataColumn dataColumnPTStambrev;
    private System.Data.DataColumn dataColumnPGran;
    private System.Data.DataColumn dataColumnPGPlantTyp;
    private System.Data.DataColumn dataColumnPGStamBrev;
    private System.Data.DataColumn dataColumnPContorta;
    private System.Data.DataColumn dataColumnPCPlantTyp;
    private System.Data.DataColumn dataColumnPCStamBrev;
    private System.Data.DataColumn dataColumnPKommentar;
    private System.Data.DataTable dataTableRojning;
    private System.Data.DataColumn dataColumnRRG;
    private System.Data.DataColumn dataColumnRDI;
    private System.Data.DataColumn dataColumnRUrsprung;
    private System.Data.DataColumn dataColumnRAr;
    private System.Data.DataColumn dataColumnRTraktnr;
    private System.Data.DataColumn dataColumnRTraktnamn;
    private System.Data.DataColumn dataColumnRStandort;
    private System.Data.DataColumn dataColumnREntreprenor;
    private System.Data.DataColumn dataColumnREntNamn;
    private System.Data.DataColumn dataColumnRDatum;
    private System.Data.DataColumn dataColumnRProvytestorlek;
    private System.Data.DataColumn dataColumnRMalHuvudstam;
    private System.Data.DataColumn dataColumnRHuvudstammar;
    private System.Data.DataColumn dataColumnRMaluppfyllelse;
    private System.Data.DataColumn dataColumnRBarrhuvudstammarsMedelhojd;
    private System.Data.DataColumn dataColumnRT;
    private System.Data.DataColumn dataColumnRG;
    private System.Data.DataColumn dataColumnRL;
    private System.Data.DataColumn dataColumnRC;
    private System.Data.DataColumn dataColumnRFraga1;
    private System.Data.DataColumn dataColumnRFraga2;
    private System.Data.DataColumn dataColumnRFraga3;
    private System.Data.DataColumn dataColumnRFraga4;
    private System.Data.DataColumn dataColumnRFraga5;
    private System.Data.DataColumn dataColumnRFraga6;
    private System.Data.DataColumn dataColumnRKommentar;
    private System.Data.DataTable dataTableSlutavverkning;
    private System.Data.DataColumn dataColumnSRG;
    private System.Data.DataColumn dataColumnSDI;
    private System.Data.DataColumn dataColumnSUrsprung;
    private System.Data.DataColumn dataColumnSAr;
    private System.Data.DataColumn dataColumnSTraktnr;
    private System.Data.DataColumn dataColumnSTraktnamn;
    private System.Data.DataColumn dataColumnSStandort;
    private System.Data.DataColumn dataColumnSEntreprenor;
    private System.Data.DataColumn dataColumnSEntrnamn;
    private System.Data.DataColumn dataColumnSDatum;
    private System.Data.DataColumn dataColumnSFraga1;
    private System.Data.DataColumn dataColumnSFraga2a;
    private System.Data.DataColumn dataColumnSFraga2b;
    private System.Data.DataColumn dataColumnSFraga2c;
    private System.Data.DataColumn dataColumnSFraga2d;
    private System.Data.DataColumn dataColumnSFraga2e;
    private System.Data.DataColumn dataColumnSFraga2f;
    private System.Data.DataColumn dataColumnSFraga2g;
    private System.Data.DataColumn dataColumnSFraga2h;
    private System.Data.DataColumn dataColumnSFraga3;
    private System.Data.DataColumn dataColumnSFraga4a;
    private System.Data.DataColumn dataColumnSFraga4b;
    private System.Data.DataColumn dataColumnSFraga5;
    private System.Data.DataColumn dataColumnSFraga6;
    private System.Data.DataColumn dataColumnSFraga7;
    private System.Data.DataColumn dataColumnSFraga8;
    private System.Data.DataColumn dataColumnSFraga9;
    private System.Data.DataColumn dataColumnSFraga10;
    private System.Data.DataColumn dataColumnSFraga11;
    private System.Data.DataColumn dataColumnSFraga12;
    private System.Data.DataColumn dataColumnSKommentar;
    private System.Data.DataTable dataTableBiobransle;
    private System.Data.DataColumn dataColumnBRG;
    private System.Data.DataColumn dataColumnBDI;
    private System.Data.DataColumn dataColumnBUrsprung;
    private System.Data.DataColumn dataColumnBAr;
    private System.Data.DataColumn dataColumnBTraktnr;
    private System.Data.DataColumn dataColumnBTraktnamn;
    private System.Data.DataColumn dataColumnBStandort;
    private System.Data.DataColumn dataColumnBEntreprenor;
    private System.Data.DataColumn dataColumnBEntnamn;
    private System.Data.DataColumn dataColumnBDatum;
    private System.Data.DataColumn dataColumnBValtorPaHygge;
    private System.Data.DataColumn dataColumnBValtorPaVagkant;
    private System.Data.DataColumn dataColumnBValtorPaAker;
    private System.Data.DataColumn dataColumnBLastbilshugg;
    private System.Data.DataColumn dataColumnBGrotbil;
    private System.Data.DataColumn dataColumnBTraktorhugg;
    private System.Data.DataColumn dataColumnBSkotarhugg;
    private System.Data.DataColumn dataColumnBFraga1;
    private System.Data.DataColumn dataColumnBFraga2;
    private System.Data.DataColumn dataColumnBFraga3;
    private System.Data.DataColumn dataColumnBFraga4;
    private System.Data.DataColumn dataColumnBFraga5;
    private System.Data.DataColumn dataColumnBFraga6;
    private System.Data.DataColumn dataColumnBFraga7;
    private System.Data.DataColumn dataColumnBFraga8;
    private System.Data.DataColumn dataColumnBFraga9;
    private System.Data.DataColumn dataColumnBFraga10;
    private System.Data.DataColumn dataColumnBFraga11;
    private System.Data.DataColumn dataColumnBFraga12;
    private System.Data.DataColumn dataColumnBFraga13;
    private System.Data.DataColumn dataColumnBFraga14;
    private System.Data.DataColumn dataColumnBFraga15;
    private System.Data.DataColumn dataColumnBFraga16;
    private System.Data.DataColumn dataColumnBFraga17;
    private System.Data.DataColumn dataColumnBFraga18;
    private System.Data.DataColumn dataColumnBFraga19;
    private System.Data.DataColumn dataColumnBKommentar;
    private System.Data.DataColumn dataColumnInvTypId;
    private System.Data.DataColumn dataColumnMInvTypId;
    private System.Data.DataColumn dataColumnPInvTypId;
    private System.Data.DataColumn dataColumnRInvTypID;
    private System.Data.DataColumn dataColumnValInvTypID;
    private System.Data.DataColumn dataColumnInvTypNamn;
    private System.Data.DataColumn dataColumnValInvTypNamn;
    private System.Data.DataColumn dataColumnAtgAreal;
    private System.Data.DataColumn dataColumnPAtgAreal;
    private System.Data.DataColumn dataColumnRAtgAreal;
    private System.Windows.Forms.DataGridViewTextBoxColumn rapportTypDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn invtypIdDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn invtypNamnDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn regionIdDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn regionNamnDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn distriktIdDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn distriktNamnDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn traktnrDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn traktNamnDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn entreprenorDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn entreprenorNamnDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn traktdelDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn artalDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewComboBoxColumn ursprungDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn datumDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn atgarealDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewComboBoxColumn markberedningsmetodDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewComboBoxColumn datainsamlingsmetodDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn malgrundytaDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewComboBoxColumn stickvagssystemNamnDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn stickvagsavstandDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn arealDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn gISArealDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn provytestorlekDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn gISStrackaDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn antalValtorDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn avstandDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn bedomdVolymDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn hyggeDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn vagkantDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn akerDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewCheckBoxColumn latbilshuggDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewCheckBoxColumn grotbilDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewCheckBoxColumn traktordragenhuggDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewCheckBoxColumn skotarburenhuggDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn avlagg1NorrDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn avlagg1OstDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn avlagg2NorrDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn avlagg2OstDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn avlagg3NorrDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn avlagg3OstDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn avlagg4NorrDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn avlagg4OstDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn avlagg5NorrDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn avlagg5OstDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn avlagg6NorrDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn avlagg6OstDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn kommentarDataGridViewTextBoxColumn;
    private System.Data.DataColumn dataColumnRLovhuvudstammarsMedelhojd;
    private System.Data.DataColumn dataColumnRRojstamHa;
  }
}
