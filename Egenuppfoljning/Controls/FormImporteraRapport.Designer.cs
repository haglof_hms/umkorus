﻿namespace Egenuppfoljning.Controls
{
  partial class FormImporteraRapport
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.components = new System.ComponentModel.Container();
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormImporteraRapport));
        this.label1 = new System.Windows.Forms.Label();
        this.buttonVisaRapport = new System.Windows.Forms.Button();
        this.imageList32 = new System.Windows.Forms.ImageList(this.components);
        this.buttonRedigeraData = new System.Windows.Forms.Button();
        this.groupBox1 = new System.Windows.Forms.GroupBox();
        this.groupBoxRapportInfo = new System.Windows.Forms.GroupBox();
        this.labelTotalaAntaletYtor = new System.Windows.Forms.Label();
        this.labelEgenuppföljningstyp = new System.Windows.Forms.Label();
        this.groupBox4 = new System.Windows.Forms.GroupBox();
        this.labelAtgArealText = new System.Windows.Forms.Label();
        this.labelAtgAreal = new System.Windows.Forms.Label();
        this.label7 = new System.Windows.Forms.Label();
        this.labelInvtyp = new System.Windows.Forms.Label();
        this.label16 = new System.Windows.Forms.Label();
        this.label2 = new System.Windows.Forms.Label();
        this.label4 = new System.Windows.Forms.Label();
        this.label5 = new System.Windows.Forms.Label();
        this.labelMaskinnr = new System.Windows.Forms.Label();
        this.labelStåndort = new System.Windows.Forms.Label();
        this.label10 = new System.Windows.Forms.Label();
        this.labelTraktnamn = new System.Windows.Forms.Label();
        this.label9 = new System.Windows.Forms.Label();
        this.labelTraktnr = new System.Windows.Forms.Label();
        this.label8 = new System.Windows.Forms.Label();
        this.labelUrsprung = new System.Windows.Forms.Label();
        this.labelDistrikt = new System.Windows.Forms.Label();
        this.labelRegion = new System.Windows.Forms.Label();
        this.labelBefintligData = new System.Windows.Forms.Label();
        this.label6 = new System.Windows.Forms.Label();
        this.label13 = new System.Windows.Forms.Label();
        this.label12 = new System.Windows.Forms.Label();
        this.label3 = new System.Windows.Forms.Label();
        this.labelEntreprenörDatum = new System.Windows.Forms.Label();
        this.buttonBläddraRapport = new System.Windows.Forms.Button();
        this.imageList16 = new System.Windows.Forms.ImageList(this.components);
        this.textBoxSokvag = new System.Windows.Forms.TextBox();
        this.groupBox5 = new System.Windows.Forms.GroupBox();
        this.buttonÅterställ = new System.Windows.Forms.Button();
        this.label14 = new System.Windows.Forms.Label();
        this.labelProduktionsledareDatum = new System.Windows.Forms.Label();
        this.labelProduktionsledareStatus = new System.Windows.Forms.Label();
        this.label15 = new System.Windows.Forms.Label();
        this.groupBox2 = new System.Windows.Forms.GroupBox();
        this.buttonGodkannRapport = new System.Windows.Forms.Button();
        this.buttonNekaRapport = new System.Windows.Forms.Button();
        this.checkBoxSändEpostNekad = new System.Windows.Forms.CheckBox();
        this.checkBoxSändEpostGodkänd = new System.Windows.Forms.CheckBox();
        this.labelProduktionsledareSökväg = new System.Windows.Forms.Label();
        this.label18 = new System.Windows.Forms.Label();
        this.labelMottagareEpost = new System.Windows.Forms.Label();
        this.label19 = new System.Windows.Forms.Label();
        this.openFileDialogKORUSRapport = new System.Windows.Forms.OpenFileDialog();
        this.groupBox3 = new System.Windows.Forms.GroupBox();
        this.groupBox6 = new System.Windows.Forms.GroupBox();
        this.groupBox1.SuspendLayout();
        this.groupBoxRapportInfo.SuspendLayout();
        this.groupBox4.SuspendLayout();
        this.groupBox5.SuspendLayout();
        this.groupBox2.SuspendLayout();
        this.groupBox3.SuspendLayout();
        this.groupBox6.SuspendLayout();
        this.SuspendLayout();
        // 
        // label1
        // 
        this.label1.AutoSize = true;
        this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label1.Location = new System.Drawing.Point(22, 28);
        this.label1.Name = "label1";
        this.label1.Size = new System.Drawing.Size(335, 13);
        this.label1.TabIndex = 1;
        this.label1.Text = "Välj en rapport av filtypen: *.keg (KORUS Egenuppföljning)";
        // 
        // buttonVisaRapport
        // 
        this.buttonVisaRapport.Enabled = false;
        this.buttonVisaRapport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonVisaRapport.ImageIndex = 0;
        this.buttonVisaRapport.ImageList = this.imageList32;
        this.buttonVisaRapport.Location = new System.Drawing.Point(378, 22);
        this.buttonVisaRapport.Name = "buttonVisaRapport";
        this.buttonVisaRapport.Size = new System.Drawing.Size(160, 40);
        this.buttonVisaRapport.TabIndex = 1;
        this.buttonVisaRapport.Text = "Rapport";
        this.buttonVisaRapport.UseVisualStyleBackColor = true;
        this.buttonVisaRapport.Click += new System.EventHandler(this.buttonForhandsgranska_Click);
        // 
        // imageList32
        // 
        this.imageList32.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList32.ImageStream")));
        this.imageList32.TransparentColor = System.Drawing.SystemColors.ActiveCaption;
        this.imageList32.Images.SetKeyName(0, "Förhandsgranska.ico");
        this.imageList32.Images.SetKeyName(1, "Godkänn.ico");
        this.imageList32.Images.SetKeyName(2, "Neka.ico");
        this.imageList32.Images.SetKeyName(3, "Redigera.ico");
        this.imageList32.Images.SetKeyName(4, "Bläddra.ico");
        this.imageList32.Images.SetKeyName(5, "Återställ.ico");
        // 
        // buttonRedigeraData
        // 
        this.buttonRedigeraData.Enabled = false;
        this.buttonRedigeraData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonRedigeraData.ImageIndex = 3;
        this.buttonRedigeraData.ImageList = this.imageList32;
        this.buttonRedigeraData.Location = new System.Drawing.Point(378, 68);
        this.buttonRedigeraData.Name = "buttonRedigeraData";
        this.buttonRedigeraData.Size = new System.Drawing.Size(160, 40);
        this.buttonRedigeraData.TabIndex = 4;
        this.buttonRedigeraData.Text = "Redigera";
        this.buttonRedigeraData.UseVisualStyleBackColor = true;
        this.buttonRedigeraData.Click += new System.EventHandler(this.buttonRedigeraData_Click);
        // 
        // groupBox1
        // 
        this.groupBox1.Controls.Add(this.groupBoxRapportInfo);
        this.groupBox1.Controls.Add(this.label1);
        this.groupBox1.Controls.Add(this.buttonBläddraRapport);
        this.groupBox1.Controls.Add(this.textBoxSokvag);
        this.groupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.groupBox1.Location = new System.Drawing.Point(14, 8);
        this.groupBox1.Name = "groupBox1";
        this.groupBox1.Size = new System.Drawing.Size(918, 276);
        this.groupBox1.TabIndex = 0;
        this.groupBox1.TabStop = false;
        this.groupBox1.Text = "IMPORTERA RAPPORT FÖR KORUS EGENUPPFÖLJNING";
        // 
        // groupBoxRapportInfo
        // 
        this.groupBoxRapportInfo.Controls.Add(this.labelTotalaAntaletYtor);
        this.groupBoxRapportInfo.Controls.Add(this.labelEgenuppföljningstyp);
        this.groupBoxRapportInfo.Controls.Add(this.groupBox4);
        this.groupBoxRapportInfo.Controls.Add(this.labelBefintligData);
        this.groupBoxRapportInfo.Controls.Add(this.label6);
        this.groupBoxRapportInfo.Controls.Add(this.label13);
        this.groupBoxRapportInfo.Controls.Add(this.label12);
        this.groupBoxRapportInfo.Controls.Add(this.label3);
        this.groupBoxRapportInfo.Controls.Add(this.labelEntreprenörDatum);
        this.groupBoxRapportInfo.Location = new System.Drawing.Point(0, 90);
        this.groupBoxRapportInfo.Name = "groupBoxRapportInfo";
        this.groupBoxRapportInfo.Size = new System.Drawing.Size(918, 186);
        this.groupBoxRapportInfo.TabIndex = 0;
        this.groupBoxRapportInfo.TabStop = false;
        this.groupBoxRapportInfo.Text = "IMPORTERAD KORUS DATA";
        // 
        // labelTotalaAntaletYtor
        // 
        this.labelTotalaAntaletYtor.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelTotalaAntaletYtor.ForeColor = System.Drawing.Color.Black;
        this.labelTotalaAntaletYtor.Location = new System.Drawing.Point(349, 30);
        this.labelTotalaAntaletYtor.Name = "labelTotalaAntaletYtor";
        this.labelTotalaAntaletYtor.Size = new System.Drawing.Size(79, 26);
        this.labelTotalaAntaletYtor.TabIndex = 3;
        this.labelTotalaAntaletYtor.Text = "_";
        // 
        // labelEgenuppföljningstyp
        // 
        this.labelEgenuppföljningstyp.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelEgenuppföljningstyp.ForeColor = System.Drawing.Color.Black;
        this.labelEgenuppföljningstyp.Location = new System.Drawing.Point(530, 30);
        this.labelEgenuppföljningstyp.Name = "labelEgenuppföljningstyp";
        this.labelEgenuppföljningstyp.Size = new System.Drawing.Size(128, 26);
        this.labelEgenuppföljningstyp.TabIndex = 5;
        this.labelEgenuppföljningstyp.Text = "_";
        // 
        // groupBox4
        // 
        this.groupBox4.Controls.Add(this.labelAtgArealText);
        this.groupBox4.Controls.Add(this.labelAtgAreal);
        this.groupBox4.Controls.Add(this.label7);
        this.groupBox4.Controls.Add(this.labelInvtyp);
        this.groupBox4.Controls.Add(this.label16);
        this.groupBox4.Controls.Add(this.label2);
        this.groupBox4.Controls.Add(this.label4);
        this.groupBox4.Controls.Add(this.label5);
        this.groupBox4.Controls.Add(this.labelMaskinnr);
        this.groupBox4.Controls.Add(this.labelStåndort);
        this.groupBox4.Controls.Add(this.label10);
        this.groupBox4.Controls.Add(this.labelTraktnamn);
        this.groupBox4.Controls.Add(this.label9);
        this.groupBox4.Controls.Add(this.labelTraktnr);
        this.groupBox4.Controls.Add(this.label8);
        this.groupBox4.Controls.Add(this.labelUrsprung);
        this.groupBox4.Controls.Add(this.labelDistrikt);
        this.groupBox4.Controls.Add(this.labelRegion);
        this.groupBox4.Location = new System.Drawing.Point(0, 58);
        this.groupBox4.Name = "groupBox4";
        this.groupBox4.Size = new System.Drawing.Size(918, 130);
        this.groupBox4.TabIndex = 8;
        this.groupBox4.TabStop = false;
        // 
        // labelAtgArealText
        // 
        this.labelAtgArealText.AutoSize = true;
        this.labelAtgArealText.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelAtgArealText.Location = new System.Drawing.Point(24, 104);
        this.labelAtgArealText.Name = "labelAtgArealText";
        this.labelAtgArealText.Size = new System.Drawing.Size(62, 13);
        this.labelAtgArealText.TabIndex = 16;
        this.labelAtgArealText.Text = "Åtg areal:";
        this.labelAtgArealText.Visible = false;
        // 
        // labelAtgAreal
        // 
        this.labelAtgAreal.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelAtgAreal.ForeColor = System.Drawing.Color.Black;
        this.labelAtgAreal.Location = new System.Drawing.Point(85, 104);
        this.labelAtgAreal.Name = "labelAtgAreal";
        this.labelAtgAreal.Size = new System.Drawing.Size(142, 26);
        this.labelAtgAreal.TabIndex = 17;
        this.labelAtgAreal.Text = "_";
        this.labelAtgAreal.Visible = false;
        // 
        // label7
        // 
        this.label7.AutoSize = true;
        this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label7.Location = new System.Drawing.Point(24, 32);
        this.label7.Name = "label7";
        this.label7.Size = new System.Drawing.Size(48, 13);
        this.label7.TabIndex = 14;
        this.label7.Text = "Invtyp:";
        // 
        // labelInvtyp
        // 
        this.labelInvtyp.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelInvtyp.ForeColor = System.Drawing.Color.Black;
        this.labelInvtyp.Location = new System.Drawing.Point(73, 32);
        this.labelInvtyp.Name = "labelInvtyp";
        this.labelInvtyp.Size = new System.Drawing.Size(142, 26);
        this.labelInvtyp.TabIndex = 15;
        this.labelInvtyp.Text = "_";
        // 
        // label16
        // 
        this.label16.AutoSize = true;
        this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label16.Location = new System.Drawing.Point(24, 69);
        this.label16.Name = "label16";
        this.label16.Size = new System.Drawing.Size(109, 13);
        this.label16.TabIndex = 12;
        this.label16.Text = "Maskinnr/Entr.nr: ";
        // 
        // label2
        // 
        this.label2.AutoSize = true;
        this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label2.Location = new System.Drawing.Point(232, 32);
        this.label2.Name = "label2";
        this.label2.Size = new System.Drawing.Size(49, 13);
        this.label2.TabIndex = 0;
        this.label2.Text = "Region:";
        // 
        // label4
        // 
        this.label4.AutoSize = true;
        this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label4.Location = new System.Drawing.Point(453, 32);
        this.label4.Name = "label4";
        this.label4.Size = new System.Drawing.Size(52, 13);
        this.label4.TabIndex = 2;
        this.label4.Text = "Distrikt:";
        // 
        // label5
        // 
        this.label5.AutoSize = true;
        this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label5.Location = new System.Drawing.Point(672, 32);
        this.label5.Name = "label5";
        this.label5.Size = new System.Drawing.Size(62, 13);
        this.label5.TabIndex = 4;
        this.label5.Text = "Ursprung:";
        // 
        // labelMaskinnr
        // 
        this.labelMaskinnr.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelMaskinnr.ForeColor = System.Drawing.Color.Black;
        this.labelMaskinnr.Location = new System.Drawing.Point(132, 69);
        this.labelMaskinnr.Name = "labelMaskinnr";
        this.labelMaskinnr.Size = new System.Drawing.Size(93, 26);
        this.labelMaskinnr.TabIndex = 13;
        this.labelMaskinnr.Text = "_";
        // 
        // labelStåndort
        // 
        this.labelStåndort.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelStåndort.ForeColor = System.Drawing.Color.Black;
        this.labelStåndort.Location = new System.Drawing.Point(728, 69);
        this.labelStåndort.Name = "labelStåndort";
        this.labelStåndort.Size = new System.Drawing.Size(93, 26);
        this.labelStåndort.TabIndex = 11;
        this.labelStåndort.Text = "_";
        // 
        // label10
        // 
        this.label10.AutoSize = true;
        this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label10.Location = new System.Drawing.Point(232, 69);
        this.label10.Name = "label10";
        this.label10.Size = new System.Drawing.Size(53, 13);
        this.label10.TabIndex = 6;
        this.label10.Text = "Traktnr:";
        // 
        // labelTraktnamn
        // 
        this.labelTraktnamn.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelTraktnamn.ForeColor = System.Drawing.Color.Black;
        this.labelTraktnamn.Location = new System.Drawing.Point(525, 69);
        this.labelTraktnamn.Name = "labelTraktnamn";
        this.labelTraktnamn.Size = new System.Drawing.Size(132, 26);
        this.labelTraktnamn.TabIndex = 9;
        this.labelTraktnamn.Text = "_";
        // 
        // label9
        // 
        this.label9.AutoSize = true;
        this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label9.Location = new System.Drawing.Point(453, 69);
        this.label9.Name = "label9";
        this.label9.Size = new System.Drawing.Size(73, 13);
        this.label9.TabIndex = 8;
        this.label9.Text = "Traktnamn:";
        // 
        // labelTraktnr
        // 
        this.labelTraktnr.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelTraktnr.ForeColor = System.Drawing.Color.Black;
        this.labelTraktnr.Location = new System.Drawing.Point(284, 69);
        this.labelTraktnr.Name = "labelTraktnr";
        this.labelTraktnr.Size = new System.Drawing.Size(142, 26);
        this.labelTraktnr.TabIndex = 7;
        this.labelTraktnr.Text = "_";
        // 
        // label8
        // 
        this.label8.AutoSize = true;
        this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label8.Location = new System.Drawing.Point(672, 69);
        this.label8.Name = "label8";
        this.label8.Size = new System.Drawing.Size(58, 13);
        this.label8.TabIndex = 10;
        this.label8.Text = "Traktdel:";
        // 
        // labelUrsprung
        // 
        this.labelUrsprung.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelUrsprung.ForeColor = System.Drawing.Color.Black;
        this.labelUrsprung.Location = new System.Drawing.Point(733, 32);
        this.labelUrsprung.Name = "labelUrsprung";
        this.labelUrsprung.Size = new System.Drawing.Size(133, 26);
        this.labelUrsprung.TabIndex = 5;
        this.labelUrsprung.Text = "_";
        // 
        // labelDistrikt
        // 
        this.labelDistrikt.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelDistrikt.ForeColor = System.Drawing.Color.Black;
        this.labelDistrikt.Location = new System.Drawing.Point(505, 31);
        this.labelDistrikt.Name = "labelDistrikt";
        this.labelDistrikt.Size = new System.Drawing.Size(157, 26);
        this.labelDistrikt.TabIndex = 3;
        this.labelDistrikt.Text = "_";
        // 
        // labelRegion
        // 
        this.labelRegion.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelRegion.ForeColor = System.Drawing.Color.Black;
        this.labelRegion.Location = new System.Drawing.Point(281, 32);
        this.labelRegion.Name = "labelRegion";
        this.labelRegion.Size = new System.Drawing.Size(142, 26);
        this.labelRegion.TabIndex = 1;
        this.labelRegion.Text = "_";
        // 
        // labelBefintligData
        // 
        this.labelBefintligData.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelBefintligData.ForeColor = System.Drawing.Color.Black;
        this.labelBefintligData.Location = new System.Drawing.Point(108, 30);
        this.labelBefintligData.Name = "labelBefintligData";
        this.labelBefintligData.Size = new System.Drawing.Size(103, 26);
        this.labelBefintligData.TabIndex = 1;
        this.labelBefintligData.Text = "_";
        // 
        // label6
        // 
        this.label6.AutoSize = true;
        this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label6.Location = new System.Drawing.Point(672, 30);
        this.label6.Name = "label6";
        this.label6.Size = new System.Drawing.Size(48, 13);
        this.label6.TabIndex = 6;
        this.label6.Text = "Datum:";
        // 
        // label13
        // 
        this.label13.AutoSize = true;
        this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label13.Location = new System.Drawing.Point(232, 30);
        this.label13.Name = "label13";
        this.label13.Size = new System.Drawing.Size(117, 13);
        this.label13.TabIndex = 2;
        this.label13.Text = "Totala antalet ytor:";
        // 
        // label12
        // 
        this.label12.AutoSize = true;
        this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label12.Location = new System.Drawing.Point(24, 30);
        this.label12.Name = "label12";
        this.label12.Size = new System.Drawing.Size(85, 13);
        this.label12.TabIndex = 0;
        this.label12.Text = "Befintlig data:";
        // 
        // label3
        // 
        this.label3.AutoSize = true;
        this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label3.Location = new System.Drawing.Point(453, 30);
        this.label3.Name = "label3";
        this.label3.Size = new System.Drawing.Size(78, 13);
        this.label3.TabIndex = 4;
        this.label3.Text = "Rapport typ:";
        // 
        // labelEntreprenörDatum
        // 
        this.labelEntreprenörDatum.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelEntreprenörDatum.ForeColor = System.Drawing.Color.Black;
        this.labelEntreprenörDatum.Location = new System.Drawing.Point(719, 30);
        this.labelEntreprenörDatum.Name = "labelEntreprenörDatum";
        this.labelEntreprenörDatum.Size = new System.Drawing.Size(176, 26);
        this.labelEntreprenörDatum.TabIndex = 7;
        this.labelEntreprenörDatum.Text = "_";
        // 
        // buttonBläddraRapport
        // 
        this.buttonBläddraRapport.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.buttonBläddraRapport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonBläddraRapport.ImageIndex = 0;
        this.buttonBläddraRapport.ImageList = this.imageList16;
        this.buttonBläddraRapport.Location = new System.Drawing.Point(26, 52);
        this.buttonBläddraRapport.Name = "buttonBläddraRapport";
        this.buttonBläddraRapport.Size = new System.Drawing.Size(119, 23);
        this.buttonBläddraRapport.TabIndex = 2;
        this.buttonBläddraRapport.Text = "Bläddra";
        this.buttonBläddraRapport.UseVisualStyleBackColor = true;
        this.buttonBläddraRapport.Click += new System.EventHandler(this.buttonBladdra_Click);
        // 
        // imageList16
        // 
        this.imageList16.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList16.ImageStream")));
        this.imageList16.TransparentColor = System.Drawing.Color.Transparent;
        this.imageList16.Images.SetKeyName(0, "Bläddra.ico");
        // 
        // textBoxSokvag
        // 
        this.textBoxSokvag.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.textBoxSokvag.Location = new System.Drawing.Point(152, 52);
        this.textBoxSokvag.Name = "textBoxSokvag";
        this.textBoxSokvag.Size = new System.Drawing.Size(744, 21);
        this.textBoxSokvag.TabIndex = 3;
        this.textBoxSokvag.TextChanged += new System.EventHandler(this.textBoxSokvag_TextChanged);
        // 
        // groupBox5
        // 
        this.groupBox5.Controls.Add(this.buttonÅterställ);
        this.groupBox5.Controls.Add(this.label14);
        this.groupBox5.Controls.Add(this.labelProduktionsledareDatum);
        this.groupBox5.Controls.Add(this.labelProduktionsledareStatus);
        this.groupBox5.Controls.Add(this.label15);
        this.groupBox5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.groupBox5.Location = new System.Drawing.Point(14, 286);
        this.groupBox5.Name = "groupBox5";
        this.groupBox5.Size = new System.Drawing.Size(918, 263);
        this.groupBox5.TabIndex = 1;
        this.groupBox5.TabStop = false;
        this.groupBox5.Text = "PRODUKTIONSLEDARE";
        // 
        // buttonÅterställ
        // 
        this.buttonÅterställ.Enabled = false;
        this.buttonÅterställ.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.buttonÅterställ.ForeColor = System.Drawing.Color.Black;
        this.buttonÅterställ.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonÅterställ.ImageIndex = 5;
        this.buttonÅterställ.ImageList = this.imageList32;
        this.buttonÅterställ.Location = new System.Drawing.Point(706, 21);
        this.buttonÅterställ.Name = "buttonÅterställ";
        this.buttonÅterställ.Size = new System.Drawing.Size(190, 40);
        this.buttonÅterställ.TabIndex = 4;
        this.buttonÅterställ.Text = "Återställ";
        this.buttonÅterställ.UseVisualStyleBackColor = true;
        this.buttonÅterställ.Click += new System.EventHandler(this.buttonÅterställ_Click);
        // 
        // label14
        // 
        this.label14.AutoSize = true;
        this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label14.Location = new System.Drawing.Point(24, 31);
        this.label14.Name = "label14";
        this.label14.Size = new System.Drawing.Size(110, 13);
        this.label14.TabIndex = 0;
        this.label14.Text = "Nuvarande status:";
        // 
        // labelProduktionsledareDatum
        // 
        this.labelProduktionsledareDatum.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelProduktionsledareDatum.ForeColor = System.Drawing.Color.Black;
        this.labelProduktionsledareDatum.Location = new System.Drawing.Point(469, 31);
        this.labelProduktionsledareDatum.Name = "labelProduktionsledareDatum";
        this.labelProduktionsledareDatum.Size = new System.Drawing.Size(181, 26);
        this.labelProduktionsledareDatum.TabIndex = 3;
        this.labelProduktionsledareDatum.Text = "_";
        // 
        // labelProduktionsledareStatus
        // 
        this.labelProduktionsledareStatus.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelProduktionsledareStatus.ForeColor = System.Drawing.Color.Black;
        this.labelProduktionsledareStatus.Location = new System.Drawing.Point(132, 31);
        this.labelProduktionsledareStatus.Name = "labelProduktionsledareStatus";
        this.labelProduktionsledareStatus.Size = new System.Drawing.Size(197, 26);
        this.labelProduktionsledareStatus.TabIndex = 1;
        this.labelProduktionsledareStatus.Text = "_";
        // 
        // label15
        // 
        this.label15.AutoSize = true;
        this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label15.Location = new System.Drawing.Point(356, 31);
        this.label15.Name = "label15";
        this.label15.Size = new System.Drawing.Size(115, 13);
        this.label15.TabIndex = 2;
        this.label15.Text = "Behandlingsdatum:";
        // 
        // groupBox2
        // 
        this.groupBox2.Controls.Add(this.buttonGodkannRapport);
        this.groupBox2.Controls.Add(this.buttonRedigeraData);
        this.groupBox2.Controls.Add(this.buttonVisaRapport);
        this.groupBox2.Controls.Add(this.buttonNekaRapport);
        this.groupBox2.Controls.Add(this.checkBoxSändEpostNekad);
        this.groupBox2.Controls.Add(this.checkBoxSändEpostGodkänd);
        this.groupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.groupBox2.Location = new System.Drawing.Point(14, 405);
        this.groupBox2.Name = "groupBox2";
        this.groupBox2.Size = new System.Drawing.Size(918, 128);
        this.groupBox2.TabIndex = 3;
        this.groupBox2.TabStop = false;
        // 
        // buttonGodkannRapport
        // 
        this.buttonGodkannRapport.Enabled = false;
        this.buttonGodkannRapport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonGodkannRapport.ImageIndex = 1;
        this.buttonGodkannRapport.ImageList = this.imageList32;
        this.buttonGodkannRapport.Location = new System.Drawing.Point(706, 22);
        this.buttonGodkannRapport.Name = "buttonGodkannRapport";
        this.buttonGodkannRapport.Size = new System.Drawing.Size(190, 40);
        this.buttonGodkannRapport.TabIndex = 2;
        this.buttonGodkannRapport.Text = "Godkänn/Lagra";
        this.buttonGodkannRapport.UseVisualStyleBackColor = true;
        this.buttonGodkannRapport.Click += new System.EventHandler(this.buttonGodkannRapport_Click);
        // 
        // buttonNekaRapport
        // 
        this.buttonNekaRapport.Enabled = false;
        this.buttonNekaRapport.ForeColor = System.Drawing.Color.Black;
        this.buttonNekaRapport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonNekaRapport.ImageIndex = 2;
        this.buttonNekaRapport.ImageList = this.imageList32;
        this.buttonNekaRapport.Location = new System.Drawing.Point(21, 26);
        this.buttonNekaRapport.Name = "buttonNekaRapport";
        this.buttonNekaRapport.Size = new System.Drawing.Size(190, 40);
        this.buttonNekaRapport.TabIndex = 0;
        this.buttonNekaRapport.Text = "Avvisa";
        this.buttonNekaRapport.UseVisualStyleBackColor = true;
        this.buttonNekaRapport.Click += new System.EventHandler(this.buttonNekaRapport_Click);
        // 
        // checkBoxSändEpostNekad
        // 
        this.checkBoxSändEpostNekad.AutoSize = true;
        this.checkBoxSändEpostNekad.Checked = true;
        this.checkBoxSändEpostNekad.CheckState = System.Windows.Forms.CheckState.Checked;
        this.checkBoxSändEpostNekad.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.checkBoxSändEpostNekad.Location = new System.Drawing.Point(21, 73);
        this.checkBoxSändEpostNekad.Name = "checkBoxSändEpostNekad";
        this.checkBoxSändEpostNekad.Size = new System.Drawing.Size(161, 17);
        this.checkBoxSändEpostNekad.TabIndex = 3;
        this.checkBoxSändEpostNekad.Text = "Sänd e-post för respons";
        this.checkBoxSändEpostNekad.UseVisualStyleBackColor = true;
        this.checkBoxSändEpostNekad.CheckedChanged += new System.EventHandler(this.checkBoxSändEpostNekad_CheckedChanged);
        // 
        // checkBoxSändEpostGodkänd
        // 
        this.checkBoxSändEpostGodkänd.AutoSize = true;
        this.checkBoxSändEpostGodkänd.Checked = true;
        this.checkBoxSändEpostGodkänd.CheckState = System.Windows.Forms.CheckState.Checked;
        this.checkBoxSändEpostGodkänd.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.checkBoxSändEpostGodkänd.Location = new System.Drawing.Point(706, 73);
        this.checkBoxSändEpostGodkänd.Name = "checkBoxSändEpostGodkänd";
        this.checkBoxSändEpostGodkänd.Size = new System.Drawing.Size(163, 17);
        this.checkBoxSändEpostGodkänd.TabIndex = 5;
        this.checkBoxSändEpostGodkänd.Text = "Sänd e-post för kvittens";
        this.checkBoxSändEpostGodkänd.UseVisualStyleBackColor = true;
        this.checkBoxSändEpostGodkänd.CheckedChanged += new System.EventHandler(this.checkBoxSändEpostGodkänd_CheckedChanged);
        // 
        // labelProduktionsledareSökväg
        // 
        this.labelProduktionsledareSökväg.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelProduktionsledareSökväg.ForeColor = System.Drawing.Color.Black;
        this.labelProduktionsledareSökväg.Location = new System.Drawing.Point(74, 11);
        this.labelProduktionsledareSökväg.Name = "labelProduktionsledareSökväg";
        this.labelProduktionsledareSökväg.Size = new System.Drawing.Size(821, 39);
        this.labelProduktionsledareSökväg.TabIndex = 1;
        this.labelProduktionsledareSökväg.Text = "_";
        // 
        // label18
        // 
        this.label18.AutoSize = true;
        this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label18.Location = new System.Drawing.Point(24, 12);
        this.label18.Name = "label18";
        this.label18.Size = new System.Drawing.Size(52, 13);
        this.label18.TabIndex = 0;
        this.label18.Text = "Sökväg:";
        // 
        // labelMottagareEpost
        // 
        this.labelMottagareEpost.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelMottagareEpost.ForeColor = System.Drawing.Color.Black;
        this.labelMottagareEpost.Location = new System.Drawing.Point(187, 32);
        this.labelMottagareEpost.Name = "labelMottagareEpost";
        this.labelMottagareEpost.Size = new System.Drawing.Size(673, 26);
        this.labelMottagareEpost.TabIndex = 1;
        this.labelMottagareEpost.Text = "_";
        // 
        // label19
        // 
        this.label19.AutoSize = true;
        this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label19.Location = new System.Drawing.Point(24, 31);
        this.label19.Name = "label19";
        this.label19.Size = new System.Drawing.Size(164, 13);
        this.label19.TabIndex = 0;
        this.label19.Text = "Mottagarens e-post adress:";
        // 
        // groupBox3
        // 
        this.groupBox3.Controls.Add(this.label19);
        this.groupBox3.Controls.Add(this.labelMottagareEpost);
        this.groupBox3.Location = new System.Drawing.Point(14, 509);
        this.groupBox3.Name = "groupBox3";
        this.groupBox3.Size = new System.Drawing.Size(918, 67);
        this.groupBox3.TabIndex = 4;
        this.groupBox3.TabStop = false;
        // 
        // groupBox6
        // 
        this.groupBox6.Controls.Add(this.labelProduktionsledareSökväg);
        this.groupBox6.Controls.Add(this.label18);
        this.groupBox6.Location = new System.Drawing.Point(14, 357);
        this.groupBox6.Name = "groupBox6";
        this.groupBox6.Size = new System.Drawing.Size(918, 56);
        this.groupBox6.TabIndex = 2;
        this.groupBox6.TabStop = false;
        // 
        // FormImporteraRapport
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
        this.Controls.Add(this.groupBox6);
        this.Controls.Add(this.groupBox2);
        this.Controls.Add(this.groupBox3);
        this.Controls.Add(this.groupBox5);
        this.Controls.Add(this.groupBox1);
        this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.Name = "FormImporteraRapport";
        this.Size = new System.Drawing.Size(950, 583);
        this.groupBox1.ResumeLayout(false);
        this.groupBox1.PerformLayout();
        this.groupBoxRapportInfo.ResumeLayout(false);
        this.groupBoxRapportInfo.PerformLayout();
        this.groupBox4.ResumeLayout(false);
        this.groupBox4.PerformLayout();
        this.groupBox5.ResumeLayout(false);
        this.groupBox5.PerformLayout();
        this.groupBox2.ResumeLayout(false);
        this.groupBox2.PerformLayout();
        this.groupBox3.ResumeLayout(false);
        this.groupBox3.PerformLayout();
        this.groupBox6.ResumeLayout(false);
        this.groupBox6.PerformLayout();
        this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Button buttonVisaRapport;
    private System.Windows.Forms.Button buttonRedigeraData;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.Button buttonBläddraRapport;
    private System.Windows.Forms.TextBox textBoxSokvag;
    private System.Windows.Forms.Button buttonNekaRapport;
    private System.Windows.Forms.Button buttonGodkannRapport;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.OpenFileDialog openFileDialogKORUSRapport;
    private System.Windows.Forms.GroupBox groupBoxRapportInfo;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label labelDistrikt;
    private System.Windows.Forms.Label labelRegion;
    private System.Windows.Forms.Label labelMaskinnr;
    private System.Windows.Forms.Label labelStåndort;
    private System.Windows.Forms.Label labelTraktnamn;
    private System.Windows.Forms.Label labelTraktnr;
    private System.Windows.Forms.Label labelEntreprenörDatum;
    private System.Windows.Forms.Label labelUrsprung;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.Label label13;
    private System.Windows.Forms.GroupBox groupBox4;
    private System.Windows.Forms.Label labelTotalaAntaletYtor;
    private System.Windows.Forms.Label labelEgenuppföljningstyp;
    private System.Windows.Forms.Label labelBefintligData;
    private System.Windows.Forms.Label label15;
    private System.Windows.Forms.Label label14;
    private System.Windows.Forms.Label labelProduktionsledareStatus;
    private System.Windows.Forms.Label labelProduktionsledareDatum;
    private System.Windows.Forms.GroupBox groupBox5;
    private System.Windows.Forms.Button buttonÅterställ;
    private System.Windows.Forms.ImageList imageList32;
    private System.Windows.Forms.ImageList imageList16;
    private System.Windows.Forms.Label label18;
    private System.Windows.Forms.Label labelProduktionsledareSökväg;
    private System.Windows.Forms.CheckBox checkBoxSändEpostNekad;
    private System.Windows.Forms.CheckBox checkBoxSändEpostGodkänd;
    private System.Windows.Forms.Label label19;
    private System.Windows.Forms.Label labelMottagareEpost;
    private System.Windows.Forms.GroupBox groupBox3;
    private System.Windows.Forms.GroupBox groupBox6;
    private System.Windows.Forms.Label label16;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Label labelInvtyp;
    private System.Windows.Forms.Label labelAtgArealText;
    private System.Windows.Forms.Label labelAtgAreal;
  }
}