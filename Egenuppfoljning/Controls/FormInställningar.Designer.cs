﻿namespace Egenuppfoljning.Controls
{
  partial class FormInställningar
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormInställningar));
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
      this.checkBoxFlyttaÅterställdaRapporter = new System.Windows.Forms.CheckBox();
      this.buttonBläddraÅterställda = new System.Windows.Forms.Button();
      this.imageList16 = new System.Windows.Forms.ImageList(this.components);
      this.textBoxÅterställdaSökväg = new System.Windows.Forms.TextBox();
      this.checkBoxFlyttaNekadeRapporter = new System.Windows.Forms.CheckBox();
      this.checkBoxFlyttaGodkändaRapporter = new System.Windows.Forms.CheckBox();
      this.buttonBläddraNekad = new System.Windows.Forms.Button();
      this.textBoxNekadSökväg = new System.Windows.Forms.TextBox();
      this.buttonBläddraGodkänd = new System.Windows.Forms.Button();
      this.textBoxGodkändSökväg = new System.Windows.Forms.TextBox();
      this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.buttonEpost = new System.Windows.Forms.Button();
      this.imageList32 = new System.Windows.Forms.ImageList(this.components);
      this.buttonLagraÄndratData = new System.Windows.Forms.Button();
      this.buttonUppdateraData = new System.Windows.Forms.Button();
      this.label2 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.label12 = new System.Windows.Forms.Label();
      this.groupBox2 = new System.Windows.Forms.GroupBox();
      this.dataGridViewDistrikt = new System.Windows.Forms.DataGridView();
      this.distriktIdDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.RegionId = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.distriktNamnDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataSetSettings = new System.Data.DataSet();
      this.dataTableRegion = new System.Data.DataTable();
      this.dataColumnSettingsRegionId = new System.Data.DataColumn();
      this.dataColumnSettingsRegionNamn = new System.Data.DataColumn();
      this.dataTable1 = new System.Data.DataTable();
      this.dataColumnSettingsDistriktRegionId = new System.Data.DataColumn();
      this.dataColumnSettingsDistriktId = new System.Data.DataColumn();
      this.dataColumnSettingsDistriktNamn = new System.Data.DataColumn();
      this.dataTableStandort = new System.Data.DataTable();
      this.dataColumnSettingsStandortNamn = new System.Data.DataColumn();
      this.dataTableMarkberedningsmetod = new System.Data.DataTable();
      this.dataColumnSettingsMarkberedningsmetodNamn = new System.Data.DataColumn();
      this.dataTableUrsprung = new System.Data.DataTable();
      this.dataColumnSettingsUrsprungNamn = new System.Data.DataColumn();
      this.dataGridViewRegioner = new System.Windows.Forms.DataGridView();
      this.idDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.namnDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataGridViewEntreprenörer = new System.Windows.Forms.DataGridView();
      this.entreprenorIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.entreprenorRegionIdGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.entreprenorNamnDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.dataSetEntreprenörer = new System.Data.DataSet();
      this.dataTableEntreprenörer = new System.Data.DataTable();
      this.dataColumnEntreprenörId = new System.Data.DataColumn();
      this.dataColumnEntreprenörNamn = new System.Data.DataColumn();
      this.dataColumnEntreprenörRegionId = new System.Data.DataColumn();
      this.dataTableRegioner = new System.Data.DataTable();
      this.dataColumnRegionId = new System.Data.DataColumn();
      this.dataColumnRegionNamn = new System.Data.DataColumn();
      this.dataTableDistrikt = new System.Data.DataTable();
      this.dataColumnDistriktId = new System.Data.DataColumn();
      this.dataColumnDistriktNamn = new System.Data.DataColumn();
      this.groupBox1.SuspendLayout();
      this.groupBox2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDistrikt)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataSetSettings)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataTableRegion)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataTableStandort)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataTableMarkberedningsmetod)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataTableUrsprung)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRegioner)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEntreprenörer)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataSetEntreprenörer)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataTableEntreprenörer)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataTableRegioner)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataTableDistrikt)).BeginInit();
      this.SuspendLayout();
      // 
      // checkBoxFlyttaÅterställdaRapporter
      // 
      this.checkBoxFlyttaÅterställdaRapporter.AutoSize = true;
      this.checkBoxFlyttaÅterställdaRapporter.Checked = true;
      this.checkBoxFlyttaÅterställdaRapporter.CheckState = System.Windows.Forms.CheckState.Checked;
      this.checkBoxFlyttaÅterställdaRapporter.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.checkBoxFlyttaÅterställdaRapporter.Location = new System.Drawing.Point(21, 147);
      this.checkBoxFlyttaÅterställdaRapporter.Name = "checkBoxFlyttaÅterställdaRapporter";
      this.checkBoxFlyttaÅterställdaRapporter.Size = new System.Drawing.Size(207, 17);
      this.checkBoxFlyttaÅterställdaRapporter.TabIndex = 6;
      this.checkBoxFlyttaÅterställdaRapporter.Text = "Återställda rapporter flyttas till:";
      this.checkBoxFlyttaÅterställdaRapporter.UseVisualStyleBackColor = true;
      this.checkBoxFlyttaÅterställdaRapporter.CheckedChanged += new System.EventHandler(this.checkBoxFlyttaÅterställdaRapporter_CheckedChanged);
      // 
      // buttonBläddraÅterställda
      // 
      this.buttonBläddraÅterställda.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.buttonBläddraÅterställda.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.buttonBläddraÅterställda.ImageIndex = 0;
      this.buttonBläddraÅterställda.ImageList = this.imageList16;
      this.buttonBläddraÅterställda.Location = new System.Drawing.Point(21, 170);
      this.buttonBläddraÅterställda.Name = "buttonBläddraÅterställda";
      this.buttonBläddraÅterställda.Size = new System.Drawing.Size(95, 23);
      this.buttonBläddraÅterställda.TabIndex = 7;
      this.buttonBläddraÅterställda.Text = "Bläddra";
      this.buttonBläddraÅterställda.UseVisualStyleBackColor = true;
      this.buttonBläddraÅterställda.Click += new System.EventHandler(this.buttonBläddraÅterställda_Click);
      // 
      // imageList16
      // 
      this.imageList16.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList16.ImageStream")));
      this.imageList16.TransparentColor = System.Drawing.Color.Transparent;
      this.imageList16.Images.SetKeyName(0, "Bläddra.ico");
      // 
      // textBoxÅterställdaSökväg
      // 
      this.textBoxÅterställdaSökväg.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.textBoxÅterställdaSökväg.Location = new System.Drawing.Point(122, 170);
      this.textBoxÅterställdaSökväg.Name = "textBoxÅterställdaSökväg";
      this.textBoxÅterställdaSökväg.Size = new System.Drawing.Size(783, 21);
      this.textBoxÅterställdaSökväg.TabIndex = 8;
      this.textBoxÅterställdaSökväg.TextChanged += new System.EventHandler(this.textBoxÅterställdaSökväg_TextChanged);
      // 
      // checkBoxFlyttaNekadeRapporter
      // 
      this.checkBoxFlyttaNekadeRapporter.AutoSize = true;
      this.checkBoxFlyttaNekadeRapporter.Checked = true;
      this.checkBoxFlyttaNekadeRapporter.CheckState = System.Windows.Forms.CheckState.Checked;
      this.checkBoxFlyttaNekadeRapporter.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.checkBoxFlyttaNekadeRapporter.Location = new System.Drawing.Point(21, 90);
      this.checkBoxFlyttaNekadeRapporter.Name = "checkBoxFlyttaNekadeRapporter";
      this.checkBoxFlyttaNekadeRapporter.Size = new System.Drawing.Size(186, 17);
      this.checkBoxFlyttaNekadeRapporter.TabIndex = 3;
      this.checkBoxFlyttaNekadeRapporter.Text = "Nekade rapporter flyttas till:";
      this.checkBoxFlyttaNekadeRapporter.UseVisualStyleBackColor = true;
      this.checkBoxFlyttaNekadeRapporter.CheckedChanged += new System.EventHandler(this.checkBoxFlyttaNekadeRapporter_CheckedChanged);
      // 
      // checkBoxFlyttaGodkändaRapporter
      // 
      this.checkBoxFlyttaGodkändaRapporter.AutoSize = true;
      this.checkBoxFlyttaGodkändaRapporter.Checked = true;
      this.checkBoxFlyttaGodkändaRapporter.CheckState = System.Windows.Forms.CheckState.Checked;
      this.checkBoxFlyttaGodkändaRapporter.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.checkBoxFlyttaGodkändaRapporter.Location = new System.Drawing.Point(21, 30);
      this.checkBoxFlyttaGodkändaRapporter.Name = "checkBoxFlyttaGodkändaRapporter";
      this.checkBoxFlyttaGodkändaRapporter.Size = new System.Drawing.Size(201, 17);
      this.checkBoxFlyttaGodkändaRapporter.TabIndex = 0;
      this.checkBoxFlyttaGodkändaRapporter.Text = "Godkända rapporter flyttas till:";
      this.checkBoxFlyttaGodkändaRapporter.UseVisualStyleBackColor = true;
      this.checkBoxFlyttaGodkändaRapporter.CheckedChanged += new System.EventHandler(this.checkBoxFlyttaGodkändaRapporter_CheckedChanged);
      // 
      // buttonBläddraNekad
      // 
      this.buttonBläddraNekad.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.buttonBläddraNekad.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.buttonBläddraNekad.ImageIndex = 0;
      this.buttonBläddraNekad.ImageList = this.imageList16;
      this.buttonBläddraNekad.Location = new System.Drawing.Point(21, 113);
      this.buttonBläddraNekad.Name = "buttonBläddraNekad";
      this.buttonBläddraNekad.Size = new System.Drawing.Size(95, 23);
      this.buttonBläddraNekad.TabIndex = 4;
      this.buttonBläddraNekad.Text = "Bläddra";
      this.buttonBläddraNekad.UseVisualStyleBackColor = true;
      this.buttonBläddraNekad.Click += new System.EventHandler(this.buttonBläddraNekad_Click);
      // 
      // textBoxNekadSökväg
      // 
      this.textBoxNekadSökväg.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.textBoxNekadSökväg.Location = new System.Drawing.Point(122, 113);
      this.textBoxNekadSökväg.Name = "textBoxNekadSökväg";
      this.textBoxNekadSökväg.Size = new System.Drawing.Size(783, 21);
      this.textBoxNekadSökväg.TabIndex = 5;
      this.textBoxNekadSökväg.TextChanged += new System.EventHandler(this.textBoxNekadSökväg_TextChanged);
      // 
      // buttonBläddraGodkänd
      // 
      this.buttonBläddraGodkänd.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.buttonBläddraGodkänd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.buttonBläddraGodkänd.ImageIndex = 0;
      this.buttonBläddraGodkänd.ImageList = this.imageList16;
      this.buttonBläddraGodkänd.Location = new System.Drawing.Point(21, 52);
      this.buttonBläddraGodkänd.Name = "buttonBläddraGodkänd";
      this.buttonBläddraGodkänd.Size = new System.Drawing.Size(95, 23);
      this.buttonBläddraGodkänd.TabIndex = 1;
      this.buttonBläddraGodkänd.Text = "Bläddra";
      this.buttonBläddraGodkänd.UseVisualStyleBackColor = true;
      this.buttonBläddraGodkänd.Click += new System.EventHandler(this.buttonBläddraGodkänd_Click);
      // 
      // textBoxGodkändSökväg
      // 
      this.textBoxGodkändSökväg.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.textBoxGodkändSökväg.Location = new System.Drawing.Point(122, 52);
      this.textBoxGodkändSökväg.Name = "textBoxGodkändSökväg";
      this.textBoxGodkändSökväg.Size = new System.Drawing.Size(783, 21);
      this.textBoxGodkändSökväg.TabIndex = 2;
      this.textBoxGodkändSökväg.TextChanged += new System.EventHandler(this.textBoxGodkändSökväg_TextChanged);
      // 
      // folderBrowserDialog
      // 
      this.folderBrowserDialog.RootFolder = System.Environment.SpecialFolder.MyComputer;
      this.folderBrowserDialog.ShowNewFolderButton = false;
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.buttonEpost);
      this.groupBox1.Controls.Add(this.buttonLagraÄndratData);
      this.groupBox1.Controls.Add(this.buttonUppdateraData);
      this.groupBox1.Controls.Add(this.label2);
      this.groupBox1.Controls.Add(this.label1);
      this.groupBox1.Controls.Add(this.label12);
      this.groupBox1.Controls.Add(this.groupBox2);
      this.groupBox1.Controls.Add(this.dataGridViewDistrikt);
      this.groupBox1.Controls.Add(this.dataGridViewRegioner);
      this.groupBox1.Controls.Add(this.dataGridViewEntreprenörer);
      this.groupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.groupBox1.Location = new System.Drawing.Point(15, 12);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(916, 587);
      this.groupBox1.TabIndex = 0;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "INSTÄLLNINGAR";
      // 
      // buttonEpost
      // 
      this.buttonEpost.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.buttonEpost.ForeColor = System.Drawing.Color.Black;
      this.buttonEpost.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.buttonEpost.ImageIndex = 0;
      this.buttonEpost.ImageList = this.imageList32;
      this.buttonEpost.Location = new System.Drawing.Point(745, 536);
      this.buttonEpost.Name = "buttonEpost";
      this.buttonEpost.Size = new System.Drawing.Size(160, 40);
      this.buttonEpost.TabIndex = 9;
      this.buttonEpost.Text = "E-post";
      this.buttonEpost.UseVisualStyleBackColor = true;
      this.buttonEpost.Click += new System.EventHandler(this.buttonEpost_Click);
      // 
      // imageList32
      // 
      this.imageList32.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList32.ImageStream")));
      this.imageList32.TransparentColor = System.Drawing.Color.Transparent;
      this.imageList32.Images.SetKeyName(0, "Epost.ico");
      this.imageList32.Images.SetKeyName(1, "Lagra.ico");
      this.imageList32.Images.SetKeyName(2, "Uppdatera.ico");
      this.imageList32.Images.SetKeyName(3, "Neka.ico");
      // 
      // buttonLagraÄndratData
      // 
      this.buttonLagraÄndratData.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.buttonLagraÄndratData.ForeColor = System.Drawing.Color.Black;
      this.buttonLagraÄndratData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.buttonLagraÄndratData.ImageIndex = 1;
      this.buttonLagraÄndratData.ImageList = this.imageList32;
      this.buttonLagraÄndratData.Location = new System.Drawing.Point(189, 536);
      this.buttonLagraÄndratData.Name = "buttonLagraÄndratData";
      this.buttonLagraÄndratData.Size = new System.Drawing.Size(160, 40);
      this.buttonLagraÄndratData.TabIndex = 8;
      this.buttonLagraÄndratData.Text = "Lagra";
      this.buttonLagraÄndratData.UseVisualStyleBackColor = true;
      this.buttonLagraÄndratData.Click += new System.EventHandler(this.buttonLagraÄndratData_Click);
      // 
      // buttonUppdateraData
      // 
      this.buttonUppdateraData.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.buttonUppdateraData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.buttonUppdateraData.ImageIndex = 2;
      this.buttonUppdateraData.ImageList = this.imageList32;
      this.buttonUppdateraData.Location = new System.Drawing.Point(12, 536);
      this.buttonUppdateraData.Name = "buttonUppdateraData";
      this.buttonUppdateraData.Size = new System.Drawing.Size(160, 40);
      this.buttonUppdateraData.TabIndex = 7;
      this.buttonUppdateraData.Text = "Uppdatera";
      this.buttonUppdateraData.UseVisualStyleBackColor = true;
      this.buttonUppdateraData.Click += new System.EventHandler(this.buttonUppdateraData_Click);
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label2.Location = new System.Drawing.Point(612, 20);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(52, 13);
      this.label2.TabIndex = 4;
      this.label2.Text = "Distrikt:";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.Location = new System.Drawing.Point(315, 20);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(61, 13);
      this.label1.TabIndex = 2;
      this.label1.Text = "Regioner:";
      // 
      // label12
      // 
      this.label12.AutoSize = true;
      this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label12.Location = new System.Drawing.Point(17, 20);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(90, 13);
      this.label12.TabIndex = 0;
      this.label12.Text = "Entreprenörer:";
      // 
      // groupBox2
      // 
      this.groupBox2.Controls.Add(this.textBoxÅterställdaSökväg);
      this.groupBox2.Controls.Add(this.checkBoxFlyttaGodkändaRapporter);
      this.groupBox2.Controls.Add(this.buttonBläddraNekad);
      this.groupBox2.Controls.Add(this.checkBoxFlyttaÅterställdaRapporter);
      this.groupBox2.Controls.Add(this.checkBoxFlyttaNekadeRapporter);
      this.groupBox2.Controls.Add(this.textBoxGodkändSökväg);
      this.groupBox2.Controls.Add(this.textBoxNekadSökväg);
      this.groupBox2.Controls.Add(this.buttonBläddraÅterställda);
      this.groupBox2.Controls.Add(this.buttonBläddraGodkänd);
      this.groupBox2.Location = new System.Drawing.Point(0, 316);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new System.Drawing.Size(916, 211);
      this.groupBox2.TabIndex = 6;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "SÖKVÄGAR";
      // 
      // dataGridViewDistrikt
      // 
      this.dataGridViewDistrikt.AllowUserToAddRows = false;
      this.dataGridViewDistrikt.AllowUserToDeleteRows = false;
      this.dataGridViewDistrikt.AllowUserToOrderColumns = true;
      this.dataGridViewDistrikt.AllowUserToResizeColumns = false;
      this.dataGridViewDistrikt.AllowUserToResizeRows = false;
      dataGridViewCellStyle15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.dataGridViewDistrikt.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle15;
      this.dataGridViewDistrikt.AutoGenerateColumns = false;
      this.dataGridViewDistrikt.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
      this.dataGridViewDistrikt.BackgroundColor = System.Drawing.SystemColors.ControlDark;
      this.dataGridViewDistrikt.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dataGridViewDistrikt.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dataGridViewDistrikt.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.distriktIdDataGridViewTextBoxColumn2,
            this.RegionId,
            this.distriktNamnDataGridViewTextBoxColumn2});
      this.dataGridViewDistrikt.DataMember = "Distrikt";
      this.dataGridViewDistrikt.DataSource = this.dataSetSettings;
      this.dataGridViewDistrikt.Location = new System.Drawing.Point(616, 38);
      this.dataGridViewDistrikt.MultiSelect = false;
      this.dataGridViewDistrikt.Name = "dataGridViewDistrikt";
      this.dataGridViewDistrikt.ReadOnly = true;
      this.dataGridViewDistrikt.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
      dataGridViewCellStyle19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.dataGridViewDistrikt.RowsDefaultCellStyle = dataGridViewCellStyle19;
      this.dataGridViewDistrikt.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.dataGridViewDistrikt.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dataGridViewDistrikt.Size = new System.Drawing.Size(280, 270);
      this.dataGridViewDistrikt.TabIndex = 5;
      // 
      // distriktIdDataGridViewTextBoxColumn2
      // 
      this.distriktIdDataGridViewTextBoxColumn2.DataPropertyName = "Id";
      dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
      dataGridViewCellStyle16.BackColor = System.Drawing.Color.LemonChiffon;
      this.distriktIdDataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle16;
      this.distriktIdDataGridViewTextBoxColumn2.HeaderText = "Id";
      this.distriktIdDataGridViewTextBoxColumn2.Name = "distriktIdDataGridViewTextBoxColumn2";
      this.distriktIdDataGridViewTextBoxColumn2.ReadOnly = true;
      this.distriktIdDataGridViewTextBoxColumn2.Width = 40;
      // 
      // RegionId
      // 
      this.RegionId.DataPropertyName = "RegionId";
      dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
      dataGridViewCellStyle17.BackColor = System.Drawing.Color.LemonChiffon;
      this.RegionId.DefaultCellStyle = dataGridViewCellStyle17;
      this.RegionId.HeaderText = "Region";
      this.RegionId.Name = "RegionId";
      this.RegionId.ReadOnly = true;
      this.RegionId.Width = 50;
      // 
      // distriktNamnDataGridViewTextBoxColumn2
      // 
      this.distriktNamnDataGridViewTextBoxColumn2.DataPropertyName = "Namn";
      dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      dataGridViewCellStyle18.BackColor = System.Drawing.Color.LemonChiffon;
      this.distriktNamnDataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle18;
      this.distriktNamnDataGridViewTextBoxColumn2.HeaderText = "Namn";
      this.distriktNamnDataGridViewTextBoxColumn2.Name = "distriktNamnDataGridViewTextBoxColumn2";
      this.distriktNamnDataGridViewTextBoxColumn2.ReadOnly = true;
      this.distriktNamnDataGridViewTextBoxColumn2.Width = 145;
      // 
      // dataSetSettings
      // 
      this.dataSetSettings.DataSetName = "DataSetSettings";
      this.dataSetSettings.Relations.AddRange(new System.Data.DataRelation[] {
            new System.Data.DataRelation("RelationRegionDistrikt", "Region", "Distrikt", new string[] {
                        "Id"}, new string[] {
                        "RegionId"}, false)});
      this.dataSetSettings.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableRegion,
            this.dataTable1,
            this.dataTableStandort,
            this.dataTableMarkberedningsmetod,
            this.dataTableUrsprung});
      // 
      // dataTableRegion
      // 
      this.dataTableRegion.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSettingsRegionId,
            this.dataColumnSettingsRegionNamn});
      this.dataTableRegion.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Namn"}, false),
            new System.Data.UniqueConstraint("Constraint2", new string[] {
                        "Id"}, false)});
      this.dataTableRegion.TableName = "Region";
      // 
      // dataColumnSettingsRegionId
      // 
      this.dataColumnSettingsRegionId.ColumnName = "Id";
      this.dataColumnSettingsRegionId.DataType = typeof(int);
      // 
      // dataColumnSettingsRegionNamn
      // 
      this.dataColumnSettingsRegionNamn.ColumnName = "Namn";
      // 
      // dataTable1
      // 
      this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSettingsDistriktRegionId,
            this.dataColumnSettingsDistriktId,
            this.dataColumnSettingsDistriktNamn});
      this.dataTable1.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.ForeignKeyConstraint("RelationRegionDistrikt", "Region", new string[] {
                        "Id"}, new string[] {
                        "RegionId"}, System.Data.AcceptRejectRule.None, System.Data.Rule.Cascade, System.Data.Rule.Cascade)});
      this.dataTable1.TableName = "Distrikt";
      // 
      // dataColumnSettingsDistriktRegionId
      // 
      this.dataColumnSettingsDistriktRegionId.ColumnName = "RegionId";
      this.dataColumnSettingsDistriktRegionId.DataType = typeof(int);
      // 
      // dataColumnSettingsDistriktId
      // 
      this.dataColumnSettingsDistriktId.ColumnName = "Id";
      this.dataColumnSettingsDistriktId.DataType = typeof(int);
      // 
      // dataColumnSettingsDistriktNamn
      // 
      this.dataColumnSettingsDistriktNamn.ColumnName = "Namn";
      // 
      // dataTableStandort
      // 
      this.dataTableStandort.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSettingsStandortNamn});
      this.dataTableStandort.TableName = "Standort";
      // 
      // dataColumnSettingsStandortNamn
      // 
      this.dataColumnSettingsStandortNamn.ColumnName = "Id";
      this.dataColumnSettingsStandortNamn.DataType = typeof(int);
      // 
      // dataTableMarkberedningsmetod
      // 
      this.dataTableMarkberedningsmetod.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSettingsMarkberedningsmetodNamn});
      this.dataTableMarkberedningsmetod.TableName = "Markberedningsmetod";
      // 
      // dataColumnSettingsMarkberedningsmetodNamn
      // 
      this.dataColumnSettingsMarkberedningsmetodNamn.ColumnName = "Namn";
      // 
      // dataTableUrsprung
      // 
      this.dataTableUrsprung.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSettingsUrsprungNamn});
      this.dataTableUrsprung.TableName = "Ursprung";
      // 
      // dataColumnSettingsUrsprungNamn
      // 
      this.dataColumnSettingsUrsprungNamn.ColumnName = "Namn";
      // 
      // dataGridViewRegioner
      // 
      this.dataGridViewRegioner.AllowUserToAddRows = false;
      this.dataGridViewRegioner.AllowUserToDeleteRows = false;
      this.dataGridViewRegioner.AllowUserToOrderColumns = true;
      this.dataGridViewRegioner.AllowUserToResizeColumns = false;
      this.dataGridViewRegioner.AllowUserToResizeRows = false;
      dataGridViewCellStyle20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.dataGridViewRegioner.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle20;
      this.dataGridViewRegioner.AutoGenerateColumns = false;
      this.dataGridViewRegioner.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
      this.dataGridViewRegioner.BackgroundColor = System.Drawing.SystemColors.ControlDark;
      this.dataGridViewRegioner.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dataGridViewRegioner.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dataGridViewRegioner.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idDataGridViewTextBoxColumn,
            this.namnDataGridViewTextBoxColumn});
      this.dataGridViewRegioner.DataMember = "Region";
      this.dataGridViewRegioner.DataSource = this.dataSetSettings;
      this.dataGridViewRegioner.Location = new System.Drawing.Point(318, 38);
      this.dataGridViewRegioner.MultiSelect = false;
      this.dataGridViewRegioner.Name = "dataGridViewRegioner";
      this.dataGridViewRegioner.ReadOnly = true;
      this.dataGridViewRegioner.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
      dataGridViewCellStyle23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.dataGridViewRegioner.RowsDefaultCellStyle = dataGridViewCellStyle23;
      this.dataGridViewRegioner.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.dataGridViewRegioner.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dataGridViewRegioner.Size = new System.Drawing.Size(280, 270);
      this.dataGridViewRegioner.TabIndex = 3;
      // 
      // idDataGridViewTextBoxColumn
      // 
      this.idDataGridViewTextBoxColumn.DataPropertyName = "Id";
      dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
      dataGridViewCellStyle21.BackColor = System.Drawing.Color.LemonChiffon;
      this.idDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle21;
      this.idDataGridViewTextBoxColumn.HeaderText = "Id";
      this.idDataGridViewTextBoxColumn.Name = "idDataGridViewTextBoxColumn";
      this.idDataGridViewTextBoxColumn.ReadOnly = true;
      this.idDataGridViewTextBoxColumn.Width = 40;
      // 
      // namnDataGridViewTextBoxColumn
      // 
      this.namnDataGridViewTextBoxColumn.DataPropertyName = "Namn";
      dataGridViewCellStyle22.BackColor = System.Drawing.Color.LemonChiffon;
      this.namnDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle22;
      this.namnDataGridViewTextBoxColumn.HeaderText = "Namn";
      this.namnDataGridViewTextBoxColumn.Name = "namnDataGridViewTextBoxColumn";
      this.namnDataGridViewTextBoxColumn.ReadOnly = true;
      this.namnDataGridViewTextBoxColumn.Width = 195;
      // 
      // dataGridViewEntreprenörer
      // 
      this.dataGridViewEntreprenörer.AllowUserToAddRows = false;
      this.dataGridViewEntreprenörer.AllowUserToDeleteRows = false;
      this.dataGridViewEntreprenörer.AllowUserToOrderColumns = true;
      this.dataGridViewEntreprenörer.AllowUserToResizeColumns = false;
      this.dataGridViewEntreprenörer.AllowUserToResizeRows = false;
      dataGridViewCellStyle24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.dataGridViewEntreprenörer.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle24;
      this.dataGridViewEntreprenörer.AutoGenerateColumns = false;
      this.dataGridViewEntreprenörer.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
      this.dataGridViewEntreprenörer.BackgroundColor = System.Drawing.SystemColors.ControlDark;
      this.dataGridViewEntreprenörer.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dataGridViewEntreprenörer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dataGridViewEntreprenörer.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.entreprenorIdDataGridViewTextBoxColumn,
            this.entreprenorRegionIdGridViewTextBoxColumn,
            this.entreprenorNamnDataGridViewTextBoxColumn});
      this.dataGridViewEntreprenörer.DataMember = "Entreprenörer";
      this.dataGridViewEntreprenörer.DataSource = this.dataSetEntreprenörer;
      this.dataGridViewEntreprenörer.Location = new System.Drawing.Point(21, 38);
      this.dataGridViewEntreprenörer.MultiSelect = false;
      this.dataGridViewEntreprenörer.Name = "dataGridViewEntreprenörer";
      this.dataGridViewEntreprenörer.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
      dataGridViewCellStyle28.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.dataGridViewEntreprenörer.RowsDefaultCellStyle = dataGridViewCellStyle28;
      this.dataGridViewEntreprenörer.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.dataGridViewEntreprenörer.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dataGridViewEntreprenörer.Size = new System.Drawing.Size(280, 270);
      this.dataGridViewEntreprenörer.TabIndex = 1;
      // 
      // entreprenorIdDataGridViewTextBoxColumn
      // 
      this.entreprenorIdDataGridViewTextBoxColumn.DataPropertyName = "Id";
      dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
      dataGridViewCellStyle25.BackColor = System.Drawing.Color.LemonChiffon;
      this.entreprenorIdDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle25;
      this.entreprenorIdDataGridViewTextBoxColumn.HeaderText = "Id";
      this.entreprenorIdDataGridViewTextBoxColumn.Name = "entreprenorIdDataGridViewTextBoxColumn";
      this.entreprenorIdDataGridViewTextBoxColumn.ReadOnly = true;
      this.entreprenorIdDataGridViewTextBoxColumn.Width = 45;
      // 
      // entreprenorRegionIdGridViewTextBoxColumn
      // 
      this.entreprenorRegionIdGridViewTextBoxColumn.DataPropertyName = "RegionId";
      dataGridViewCellStyle26.BackColor = System.Drawing.Color.LemonChiffon;
      this.entreprenorRegionIdGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle26;
      this.entreprenorRegionIdGridViewTextBoxColumn.HeaderText = "Region";
      this.entreprenorRegionIdGridViewTextBoxColumn.Name = "entreprenorRegionIdGridViewTextBoxColumn";
      this.entreprenorRegionIdGridViewTextBoxColumn.ReadOnly = true;
      this.entreprenorRegionIdGridViewTextBoxColumn.Width = 50;
      // 
      // entreprenorNamnDataGridViewTextBoxColumn
      // 
      this.entreprenorNamnDataGridViewTextBoxColumn.DataPropertyName = "Namn";
      dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
      this.entreprenorNamnDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle27;
      this.entreprenorNamnDataGridViewTextBoxColumn.HeaderText = "Namn";
      this.entreprenorNamnDataGridViewTextBoxColumn.Name = "entreprenorNamnDataGridViewTextBoxColumn";
      this.entreprenorNamnDataGridViewTextBoxColumn.Width = 190;
      // 
      // dataSetEntreprenörer
      // 
      this.dataSetEntreprenörer.DataSetName = "DataSetEntreprenörer";
      this.dataSetEntreprenörer.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableEntreprenörer,
            this.dataTableRegioner,
            this.dataTableDistrikt});
      // 
      // dataTableEntreprenörer
      // 
      this.dataTableEntreprenörer.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnEntreprenörId,
            this.dataColumnEntreprenörNamn,
            this.dataColumnEntreprenörRegionId});
      this.dataTableEntreprenörer.TableName = "Entreprenörer";
      // 
      // dataColumnEntreprenörId
      // 
      this.dataColumnEntreprenörId.ColumnName = "Id";
      this.dataColumnEntreprenörId.DataType = typeof(int);
      // 
      // dataColumnEntreprenörNamn
      // 
      this.dataColumnEntreprenörNamn.ColumnName = "Namn";
      // 
      // dataColumnEntreprenörRegionId
      // 
      this.dataColumnEntreprenörRegionId.ColumnName = "RegionId";
      this.dataColumnEntreprenörRegionId.DataType = typeof(int);
      // 
      // dataTableRegioner
      // 
      this.dataTableRegioner.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnRegionId,
            this.dataColumnRegionNamn});
      this.dataTableRegioner.TableName = "Regioner";
      // 
      // dataColumnRegionId
      // 
      this.dataColumnRegionId.ColumnName = "Id";
      this.dataColumnRegionId.DataType = typeof(int);
      // 
      // dataColumnRegionNamn
      // 
      this.dataColumnRegionNamn.ColumnName = "Namn";
      // 
      // dataTableDistrikt
      // 
      this.dataTableDistrikt.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnDistriktId,
            this.dataColumnDistriktNamn});
      this.dataTableDistrikt.TableName = "Distrikt";
      // 
      // dataColumnDistriktId
      // 
      this.dataColumnDistriktId.ColumnName = "Id";
      this.dataColumnDistriktId.DataType = typeof(int);
      // 
      // dataColumnDistriktNamn
      // 
      this.dataColumnDistriktNamn.ColumnName = "Namn";
      // 
      // FormInställningar
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.groupBox1);
      this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.Name = "FormInställningar";
      this.Size = new System.Drawing.Size(950, 612);
      this.Load += new System.EventHandler(this.FormInställningar_Load);
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridViewDistrikt)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataSetSettings)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataTableRegion)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataTableStandort)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataTableMarkberedningsmetod)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataTableUrsprung)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRegioner)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataGridViewEntreprenörer)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataSetEntreprenörer)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataTableEntreprenörer)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataTableRegioner)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.dataTableDistrikt)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.CheckBox checkBoxFlyttaÅterställdaRapporter;
    private System.Windows.Forms.Button buttonBläddraÅterställda;
    private System.Windows.Forms.TextBox textBoxÅterställdaSökväg;
    private System.Windows.Forms.CheckBox checkBoxFlyttaNekadeRapporter;
    private System.Windows.Forms.CheckBox checkBoxFlyttaGodkändaRapporter;
    private System.Windows.Forms.Button buttonBläddraNekad;
    private System.Windows.Forms.TextBox textBoxNekadSökväg;
    private System.Windows.Forms.Button buttonBläddraGodkänd;
    private System.Windows.Forms.TextBox textBoxGodkändSökväg;
    private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
    private System.Windows.Forms.ImageList imageList16;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.Button buttonEpost;
    private System.Windows.Forms.ImageList imageList32;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.DataGridView dataGridViewEntreprenörer;
    private System.Windows.Forms.DataGridView dataGridViewDistrikt;
    private System.Windows.Forms.DataGridView dataGridViewRegioner;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.Button buttonLagraÄndratData;
    private System.Windows.Forms.Button buttonUppdateraData;
    private System.Data.DataSet dataSetEntreprenörer;
    private System.Data.DataTable dataTableEntreprenörer;
    private System.Data.DataColumn dataColumnEntreprenörId;
    private System.Data.DataColumn dataColumnEntreprenörNamn;
    private System.Data.DataTable dataTableRegioner;
    private System.Data.DataColumn dataColumnRegionId;
    private System.Data.DataColumn dataColumnRegionNamn;
    private System.Data.DataTable dataTableDistrikt;
    private System.Data.DataColumn dataColumnDistriktId;
    private System.Data.DataColumn dataColumnDistriktNamn;
    private System.Data.DataSet dataSetSettings;
    private System.Data.DataTable dataTableRegion;
    private System.Data.DataColumn dataColumnSettingsRegionId;
    private System.Data.DataColumn dataColumnSettingsRegionNamn;
    private System.Data.DataTable dataTable1;
    private System.Data.DataColumn dataColumnSettingsDistriktRegionId;
    private System.Data.DataColumn dataColumnSettingsDistriktId;
    private System.Data.DataColumn dataColumnSettingsDistriktNamn;
    private System.Data.DataTable dataTableStandort;
    private System.Data.DataColumn dataColumnSettingsStandortNamn;
    private System.Data.DataTable dataTableMarkberedningsmetod;
    private System.Data.DataColumn dataColumnSettingsMarkberedningsmetodNamn;
    private System.Data.DataTable dataTableUrsprung;
    private System.Data.DataColumn dataColumnSettingsUrsprungNamn;
    private System.Windows.Forms.DataGridViewTextBoxColumn distriktIdDataGridViewTextBoxColumn2;
    private System.Windows.Forms.DataGridViewTextBoxColumn RegionId;
    private System.Windows.Forms.DataGridViewTextBoxColumn distriktNamnDataGridViewTextBoxColumn2;
    private System.Windows.Forms.DataGridViewTextBoxColumn idDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn namnDataGridViewTextBoxColumn;
    private System.Data.DataColumn dataColumnEntreprenörRegionId;
    private System.Windows.Forms.DataGridViewTextBoxColumn entreprenorIdDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn entreprenorRegionIdGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn entreprenorNamnDataGridViewTextBoxColumn;
  }
}
