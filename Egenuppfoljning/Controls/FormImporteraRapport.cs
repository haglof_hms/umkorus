﻿#region

using System;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using Egenuppfoljning.Applications;
using Egenuppfoljning.Dialogs;
using Egenuppfoljning.Interfaces;
using Egenuppfoljning.Properties;
using Timer = System.Threading.Timer;

#endregion

namespace Egenuppfoljning.Controls
{
    public partial class FormImporteraRapport : UserControl, IProgressCallback
    {
        private IKorusRapport mAktivRapportForm;

        private string mAktivRapportSökväg;
        private string mAktivRapportTyp;
        private FormProgress mFormProgress;
        private Thread mLoadingThread;
        private Timer mStateTimer;

        public FormImporteraRapport()
        {
            InitializeComponent();
            Init();
        }


        /// <summary>
        ///   Property för den aktiva e-post dit kvitto/resons ska sändas.
        /// </summary>
        private static string AktivMottagarEpostAdress
        {
            get
            {
                return Settings.Default.HamtaEpostFranRapport
                         ? Settings.Default.MailSendToAuto
                         : Settings.Default.MailSendToManual;
            }
            set
            {
                if (Settings.Default.HamtaEpostFranRapport)
                {
                    Settings.Default.MailSendToAuto = value;
                }
                else
                {
                    Settings.Default.MailSendToManual = value;
                }
            }
        }

        private static string AktivMailTyp
        {
            get { return Settings.Default.AnvandInternEpost ? "Intern e-post" : "Outlook"; }
        }

        #region IProgressCallback Members

        public void StängProgressDialog()
        {
            if (mStateTimer != null)
            {
                mStateTimer.Dispose();
                mStateTimer = null;
            }

            if (mFormProgress != null)
            {
                mFormProgress.Hide();
            }
        }

        #endregion

        // ReSharper disable EventNeverSubscribedTo.Global
        public event StatusEventHandler StatusEvent;
        public event DataEventHandler DataEvent;
        // ReSharper restore EventNeverSubscribedTo.Global

        private void Init()
        {
            if (Settings.Default.ReportFilePath.Equals(string.Empty))
            {
                Settings.Default.ReportFilePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                                                  Settings.Default.ReportsPath;
            }
            if (Settings.Default.GodkandSelectedPath.Equals(string.Empty))
            {
                Settings.Default.GodkandSelectedPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                                                       Settings.Default.GodkandPath;
                Settings.Default.FlyttaGodkandaRapporter = true;
            }

            if (Settings.Default.NekadSelectedPath.Equals(string.Empty))
            {
                Settings.Default.NekadSelectedPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                                                     Settings.Default.NekadPath;
                Settings.Default.FlyttaNekadeRapporter = true;
            }

            if (Settings.Default.AterstallSelectedPath.Equals(string.Empty))
            {
                Settings.Default.AterstallSelectedPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                                                         Settings.Default.AterstallPath;
                Settings.Default.FlyttaAterstalldaRapporter = true;
            }

            if (Settings.Default.ReportBrowsePath.Equals(string.Empty))
            {
                Settings.Default.ReportBrowsePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                                                    Settings.Default.ReportsPath;
            }

            textBoxSokvag.Text = Settings.Default.ReportBrowsePath;

            Settings.Default.MailSendToAuto = string.Empty;
            labelMottagareEpost.Text = AktivMottagarEpostAdress;


            checkBoxSändEpostGodkänd.Checked = Settings.Default.SandEpostGodkand;
            checkBoxSändEpostNekad.Checked = Settings.Default.SandEpostNekad;
        }

        // ReSharper disable UnusedMember.Global
        public void Close()
        // ReSharper restore UnusedMember.Global
        {
            Settings.Default.Save();
            Hide();
        }

        // ReSharper disable MemberCanBePrivate.Global
        public void FörhandsgranskaRapport()
        // ReSharper restore MemberCanBePrivate.Global
        {
            if (mAktivRapportForm == null) return;
            var rapport = mAktivRapportForm;
            rapport.ShowReport();
        }

        // ReSharper disable MemberCanBePrivate.Global
        public void InvokeRapportInformation()
        // ReSharper restore MemberCanBePrivate.Global
        {
            Invoke(new MethodInvoker(RapportInformation));
        }

        /// <summary>
        ///   Öppnar upp en fildialog där användaren kan välja den KORUS rapport som ska importeras.
        /// </summary>
        // ReSharper disable MemberCanBePrivate.Global
        public void ImporteraRapport()
        // ReSharper restore MemberCanBePrivate.Global
        {
            var användStandardSökväg = false;
            if (!Directory.Exists(Settings.Default.ReportBrowsePath))
            {
                //skapa en default katalog om den inte finns
                if (
                  MessageBox.Show(
                    Resources.Katalog_sokvagen + Settings.Default.ReportBrowsePath + Resources.finns_inte__vill_du_skapa_den,
                    Resources.Den_angivna_katalogen_finns_inte, MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
                  DialogResult.Yes)
                {
                    try
                    {
                        Directory.CreateDirectory(Settings.Default.ReportBrowsePath);
                    }
                    catch (Exception ex)
                    {
#if !DEBUG
     MessageBox.Show("Den angivna sökvägen är felaktig, och kunde inte skapas. ", "Skapa katalog misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                        MessageBox.Show(ex.ToString());
#endif
                        användStandardSökväg = true;
                    }
                }
                else
                {
                    användStandardSökväg = true;
                }
            }

            openFileDialogKORUSRapport.InitialDirectory = !användStandardSökväg ? Settings.Default.ReportBrowsePath : "C:\\";

            openFileDialogKORUSRapport.Filter = Resources.FilterHolmenKeg;
            openFileDialogKORUSRapport.Title = Resources.OppnaRapport;

            if (openFileDialogKORUSRapport.ShowDialog() == DialogResult.OK)
            {
                ÖppnaRapport(openFileDialogKORUSRapport.FileName);
            }
        }

        // ReSharper disable UnusedMember.Global
        public string SparaEntreprenör(int aEntreprenörId, int aRegionId)
        // ReSharper restore UnusedMember.Global
        {
            var sparaEntreprenör = new FormSparaEntreprenör();
            sparaEntreprenör.InitData(aEntreprenörId, aRegionId);
            sparaEntreprenör.ShowDialog(this);
            return Settings.Default.EntreprenorNamn;
        }

        // ReSharper disable UnusedMember.Global
        public void MailInställningar()
        {
            var mailSettings = new MailSettings();
            if (mailSettings.ShowDialog(this) != DialogResult.OK) return;
            Settings.Default.Save();
            InvokeRapportInformation();
        }

        public void SkrivUtRapport()
        {
            if (mAktivRapportForm == null) return;
            var rapport = mAktivRapportForm;
            rapport.SkrivUt();
        }

        // ReSharper restore UnusedMember.Global

        // ReSharper disable MemberCanBePrivate.Global
        public void RedigeraRapport()
        // ReSharper restore MemberCanBePrivate.Global
        {
            if (mAktivRapportForm == null) return;
            BringToFront();
            Invoke(new MethodFlagInvoker(mAktivRapportForm.ShowEditDialog), new object[] { false });
            InvokeRapportInformation();
            Focus();
            BringToFront();
        }


        private static void LabelCheck(Control aLabel)
        {
            if (aLabel.Text.Equals(string.Empty))
            {
                aLabel.Text = Resources.Saknas;
                aLabel.ForeColor = Color.Red;
            }
            else
            {
                aLabel.ForeColor = Color.Black;
            }
        }

        /// <summary>
        ///   Kallad från den aktiva rapporten när inladdningen är klar, för att visa information om den inladdade rapporten.
        /// </summary>
        private void RapportInformation()
    {
      Progress(30);

      if (mAktivRapportForm != null)
      {
        labelEgenuppföljningstyp.Text = mAktivRapportForm.GetEgenuppföljningsTyp();
        var markberedningsMetod = mAktivRapportForm.GetDsStr("UserData", "Markberedningsmetod");
        if (!markberedningsMetod.Equals(string.Empty))
        {
          labelEgenuppföljningstyp.Text += markberedningsMetod;
        }

        labelTotalaAntaletYtor.Text = mAktivRapportForm.GetTotalaAntaletYtor().ToString(CultureInfo.InvariantCulture);
        labelBefintligData.Text = String.Format("{0}% Klar", mAktivRapportForm.GetProcentStatus());
        labelInvtyp.Text = mAktivRapportForm.GetDsStr("UserData", "InvTyp");
        labelRegion.Text = mAktivRapportForm.GetDsStr("UserData", "Region");
        labelDistrikt.Text = mAktivRapportForm.GetDsStr("UserData", "Distrikt");
        labelUrsprung.Text = mAktivRapportForm.GetDsStr("UserData", "Ursprung");

        if (mAktivRapportTyp.Equals(Resources.PlanteringPrefix) || mAktivRapportTyp.Equals(Resources.RojningPrefix))
        {
            labelAtgArealText.Visible = true;
            labelAtgAreal.Visible = true;
            double fAtgAreal;
            double.TryParse(mAktivRapportForm.GetDsStr("UserData", "AtgAreal"), out fAtgAreal);
            labelAtgAreal.Text = Math.Round(fAtgAreal, 1).ToString(CultureInfo.InvariantCulture);
            //string test = mAktivRapportForm.getGetDsStr("UserData", "AtgAreal");
            //labelAtgAreal.Text = mAktivRapportForm.GetDsStr("UserData", "AtgAreal");
        }
        else
        {
            labelAtgArealText.Visible = false;
            labelAtgAreal.Visible = false;
            labelAtgAreal.Text = string.Empty;
        }

        var datum = mAktivRapportForm.GetDsStr("UserData", "Datum");
        labelEntreprenörDatum.Text = datum.Substring(0, datum.IndexOf(" ", StringComparison.Ordinal));

        int traktNr;
        int.TryParse(mAktivRapportForm.GetDsStr("UserData", "Traktnr"), out traktNr);
        labelTraktnr.Text = string.Format("{0:000000}", traktNr);
        labelTraktnamn.Text = mAktivRapportForm.GetDsStr("UserData", "Traktnamn");
        labelStåndort.Text = mAktivRapportForm.GetDsStr("UserData", "Standort");
        int maskinNr;
        int.TryParse(mAktivRapportForm.GetDsStr("UserData", "Entreprenor"), out maskinNr);
        labelMaskinnr.Text = mAktivRapportForm.GetDsStr("UserData", "RegionId") + Resources.Bindelstreck +
                             string.Format("{0:0000}", maskinNr);

        labelProduktionsledareStatus.Text = mAktivRapportForm.GetDsStr("UserData", "Status");
        labelProduktionsledareDatum.Text = mAktivRapportForm.GetDsStr("UserData", "Status_Datum");
        if (Settings.Default.HamtaEpostFranRapport)
        {
          AktivMottagarEpostAdress = mAktivRapportForm.GetDsStr("UserData", "MailFrom");
        }

        UpdateGUIStatus(mAktivRapportForm.GetProcentStatus());

        StatusEvent(this, new KorusStatusEventArgs(buttonRedigeraData.Enabled, "")); //Call back to the KORUSView class
      }

      labelProduktionsledareSökväg.Text = Settings.Default.ReportFilePath;
      labelMottagareEpost.Text = AktivMottagarEpostAdress;
      checkBoxSändEpostNekad.Checked = Settings.Default.SandEpostNekad;
      checkBoxSändEpostGodkänd.Checked = Settings.Default.SandEpostGodkand;

      StängProgressDialog();
      Application.DoEvents();
    }

        private void Progress(int värde)
        {
            if (mFormProgress != null)
            {
                mFormProgress.SättVärde(värde);
            }
        }

        private void StängÖppenRapport()
        {
            if (mAktivRapportForm == null) return;
            //stäng om en Form redan finns.
            Invoke(new MethodInvoker(mAktivRapportForm.Close));
            mAktivRapportForm = null;
        }

        private void StartaProgressDialog()
        {
            //Start up the progress dialog
            mFormProgress = new FormProgress();

            TimerCallback tcb = mFormProgress.CheckStatus;
            mFormProgress.Show();
            mFormProgress.Börja(0, 30);
            mStateTimer = new Timer(tcb, this, 125, 125);
        }

        private void TrådLaddaRapport()
        {
            if (mAktivRapportTyp.Equals(Resources.MBPrefix))
            {
                mAktivRapportForm = new MarkberedningForm();
            }
            else if (mAktivRapportTyp.Equals(Resources.PlanteringPrefix))
            {
                mAktivRapportForm = new PlanteringForm();
            }
            else if (mAktivRapportTyp.Equals(Resources.RojningPrefix))
            {
                mAktivRapportForm = new RöjningForm();
            }
            else if (mAktivRapportTyp.Equals(Resources.GallringPrefix))
            {
                mAktivRapportForm = new GallringForm();
            }
            else if (mAktivRapportTyp.Equals(Resources.BiobranslePrefix))
            {
                mAktivRapportForm = new BiobränsleForm();
            }
            else if (mAktivRapportTyp.Equals(Resources.SlutavverkningPrefix))
            {
                mAktivRapportForm = new SlutavverkningForm();
            }
            else
            {
                MessageBox.Show(
                  Resources.Filnamnet_maste_stamma,
                  Resources.Felaktigt_filnamn, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            Progress(20);
            mAktivRapportForm.LaddaForm(mAktivRapportSökväg);
            InvokeRapportInformation();
        }

        private void UpdateGUIStatus(int aStatus)
        {
            LabelCheck(labelEgenuppföljningstyp);
            LabelCheck(labelTotalaAntaletYtor);
            LabelCheck(labelRegion);
            LabelCheck(labelDistrikt);
            LabelCheck(labelUrsprung);
            LabelCheck(labelEntreprenörDatum);
            LabelCheck(labelTraktnr);
            LabelCheck(labelTraktnamn);
            LabelCheck(labelStåndort);
            LabelCheck(labelMaskinnr);
            LabelCheck(labelProduktionsledareStatus);

            if (labelProduktionsledareDatum.Text.Equals(DateTime.MinValue.ToString(CultureInfo.InvariantCulture)))
            {
                labelProduktionsledareDatum.Text = string.Empty;
            }

            LabelCheck(labelProduktionsledareDatum);

            if (labelProduktionsledareStatus.Text.Equals(string.Empty) ||
                labelProduktionsledareStatus.Text.Equals(Resources.Obehandlad))
            {
                labelProduktionsledareStatus.Text = Resources.Obehandlad;
                labelProduktionsledareStatus.ForeColor = Color.Blue;
                labelProduktionsledareDatum.ForeColor = Color.DarkBlue;
                labelProduktionsledareSökväg.ForeColor = Color.DarkBlue;
                buttonÅterställ.Enabled = false;
                buttonNekaRapport.Enabled = true;
                buttonGodkannRapport.Enabled = true;
                buttonRedigeraData.Enabled = true;
                groupBoxRapportInfo.Enabled = true;
            }
            else if (labelProduktionsledareStatus.Text.Equals(Resources.Nekad))
            {
                labelProduktionsledareStatus.ForeColor = Color.Red;
                labelProduktionsledareDatum.ForeColor = Color.DarkRed;
                labelProduktionsledareSökväg.ForeColor = Color.DarkRed;
                buttonÅterställ.Enabled = true;
                buttonNekaRapport.Enabled = false;
                buttonGodkannRapport.Enabled = false;
                buttonRedigeraData.Enabled = false;
                groupBoxRapportInfo.Enabled = false;
            }
            else
            {
                labelProduktionsledareStatus.ForeColor = Color.Green;
                labelProduktionsledareDatum.ForeColor = Color.DarkGreen;
                labelProduktionsledareSökväg.ForeColor = Color.DarkGreen;
                buttonÅterställ.Enabled = true;
                buttonNekaRapport.Enabled = false;
                buttonGodkannRapport.Enabled = false;
                buttonRedigeraData.Enabled = false;
                groupBoxRapportInfo.Enabled = false;
            }
            buttonVisaRapport.Enabled = true;

            labelBefintligData.ForeColor = aStatus >= 100 ? Color.Green : Color.Red;
        }

        private bool UpdateReport(string aReportStatus, string aReportStatusDatum)
        {
            var rValue = false;
            try
            {
                if (mAktivRapportForm != null)
                {
                    var reportName = mAktivRapportForm.GetReportFileName();

                    if (reportName != null)
                    {
                        var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(Settings.Default.ReportFilePath);
                        if (fileNameWithoutExtension != null && !fileNameWithoutExtension.Equals(reportName))
                        {
                            if (
                              MessageBox.Show(Resources.Id_s_for_rapporten,
                                              Resources.Andrade_id_s__rapporten, MessageBoxButtons.YesNo,
                                              MessageBoxIcon.Question) == DialogResult.No)
                            {
                                //Checka om rapport filnamnet överrenstämmer med id:s, om inte, visa en dialog, uppdatera inte rapport om Nej väljs
                                return false;
                            }
                        }
                    }
                    else
                    {
                        return false;
                    }

                    //skriv ner rapport
                    if (mAktivRapportForm.WriteDataToReportXmlFile(Settings.Default.ReportFilePath, false, false, aReportStatus,
                                                                   aReportStatusDatum))
                    {
                        textBoxSokvag.Text = Settings.Default.ReportBrowsePath;
                        rValue = true;
                    }
                }
            }
            catch (Exception ex)
            {
#if !DEBUG
   MessageBox.Show("Skriva till rapport filen misslyckades. Se till så rapport filen finns på sökvägen och inte är låst med att den är öppen i ett annat program.", "Skriva status misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }

            if (rValue)
            {
                //Uppdatering för rapporten är lyckad och klar, uppdatera status
                Settings.Default.ReportStatus = aReportStatus;
                Settings.Default.ReportStatusDatum = aReportStatusDatum;
            }

            return rValue;
        }

        private static bool MoveReport(string aSelectedPath)
        {
            var rValue = false;
            try
            {
                //Först kolla om katalogen finns, finns den inte så skapa den först.
                if (!Directory.Exists(aSelectedPath))
                {
                    Directory.CreateDirectory(aSelectedPath);
                }

                var reportDir = Path.GetDirectoryName(Settings.Default.ReportFilePath);

                if (reportDir != null && !Directory.Exists(reportDir))
                {
                    Directory.CreateDirectory(reportDir);
                }

                if (!aSelectedPath.EndsWith(Path.DirectorySeparatorChar.ToString(CultureInfo.InvariantCulture)))
                {
                    //Eftersom dessa sökvägar kan väljas av användaren, så kan sökvägen sluta på / eller inte, se till så att / finns i sökvägen.
                    aSelectedPath += Path.DirectorySeparatorChar;
                }

                var moveToFilePath = aSelectedPath + Path.GetFileName(Settings.Default.ReportFilePath);

                if (!moveToFilePath.Equals(Settings.Default.ReportFilePath))
                {
                    try
                    {
                        if (File.Exists(moveToFilePath))
                        {
                            if (
                              MessageBox.Show(
                                Resources.En_rapport_fil_med_samma + moveToFilePath +
                                Resources.Vill_du_skriva_over, Resources.KORUS_Rapporten_finns_redan, MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                File.Copy(Settings.Default.ReportFilePath, moveToFilePath, true); //skriv över destinations-fil
                                File.Delete(Settings.Default.ReportFilePath); //plocka bort käll-filen.
                                rValue = true;
                                Settings.Default.ReportFilePath = moveToFilePath;
                            }
                        }
                        else
                        {
                            File.Move(Settings.Default.ReportFilePath, moveToFilePath);
                            rValue = true;
                            Settings.Default.ReportFilePath = moveToFilePath;
                        }
                    }
                    catch (Exception ex)
                    {
#if !DEBUG
   MessageBox.Show("Skriva eller flytta rapport filen misslyckades. Se till så rapport sökvägarna är korrekta under inställningar och att filen inte är låst för att den är öppen i ett annat program.", "Skriva status/flytta misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                        MessageBox.Show(ex.ToString());
#endif
                    }
                }
                else
                {
                    //ingen move behöver göras filen finns redan på destination
                    rValue = true;
                }
            }
            catch (Exception ex)
            {
#if !DEBUG
   MessageBox.Show("Flytta rapport filen misslyckades. Se till så rapport filen finns på sökvägen och inte är låst med att den är öppen i ett annat program.", "Flytta rapporten misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }

            return rValue;
        }

        private bool UppdateraOchFlyttaRapport(string aSelectedPath, bool aFlyttaRapport, string aReportStatus,
                                               string aReportStatusDatum)
        {
            var rValue = false;
            if (mAktivRapportForm != null)
            {
                //Skriv ner ändringarna till rapporten
                if (UpdateReport(aReportStatus, aReportStatusDatum))
                {
                    if (aFlyttaRapport)
                    {
                        //Flytta rapportfilen
                        rValue = MoveReport(aSelectedPath);
                    }
                }
            }

            if (rValue)
            {
                //Status och path uppdaterad.
                InvokeRapportInformation();
            }

            return rValue;
        }

        /// <summary>
        ///   En check görs mot prefix i filnamnet för att plocka ut vilken typ av rapport som ska laddas. Sedan skapas dialog och rapport beroende på vilken typen är. Rapporten laddas då in för den klassen.
        /// </summary>
        /// <param name="aSökväg"> Sökvägen till filen </param>
        // ReSharper disable MemberCanBePrivate.Global
        public void ÖppnaRapport(string aSökväg)
        // ReSharper restore MemberCanBePrivate.Global
        {
            try
            {
                if (mLoadingThread != null)
                {
                    //vänta tills den förra laddartråden är klar.
                    mLoadingThread.Join();
                }

                var filNamn = Path.GetFileNameWithoutExtension(aSökväg);
                if (filNamn == null) return;

                var typSeparator = filNamn.IndexOf("_", StringComparison.Ordinal);

                if (typSeparator >= 0 && typSeparator < filNamn.Length)
                {
                    mAktivRapportTyp = filNamn.Substring(0, typSeparator);

                    StängProgressDialog();
                    StängÖppenRapport();
                    StartaProgressDialog();
                    mAktivRapportSökväg = aSökväg;

                    //Uppdatera textfältet för sökväg.
                    textBoxSokvag.Text = aSökväg;
                    Settings.Default.ReportBrowsePath = Path.GetDirectoryName(aSökväg);

                    Progress(10);
                    mLoadingThread = new Thread(TrådLaddaRapport);

                    // Start the loading thread
                    mLoadingThread.Start();
                }
                else
                {
                    MessageBox.Show(
                      Resources.Filnamnet_maste_stamma,
                      Resources.Felaktigt_filnamn, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
#if !DEBUG
       MessageBox.Show("Ett fel inträffade vid inläsning av rapporten.", "Öppna rapporten misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void buttonRedigeraData_Click(object sender, EventArgs e)
        {
            RedigeraRapport();
        }

        private void buttonBladdra_Click(object sender, EventArgs e)
        {
            ImporteraRapport();
        }

        private void buttonForhandsgranska_Click(object sender, EventArgs e)
        {
            FörhandsgranskaRapport();
        }

        private void buttonGodkannRapport_Click(object sender, EventArgs e)
        {
            if (mAktivRapportForm == null) return;
            var procent = mAktivRapportForm.GetProcentStatus();
            var klarResult = DialogResult.Yes;
            if (mAktivRapportForm != null && procent < 100)
            {
                //Visa dialog ifall inte status är 100% Klar på rapporten
                klarResult =
                  MessageBox.Show(
                    Resources.Det_saknas_data + procent +
                    Resources.klar__Ska_rapporten, Resources.Godkanna_rapport_trots,
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            }

            if (klarResult == DialogResult.Yes &&
                UpdateReport(Settings.Default.ReportStatus, Settings.Default.ReportStatusDatum))
            {
                DataEvent(this, mAktivRapportForm.GetKORUSData()); //Sänd tillbaka till KORUSView för att lagra det nya datat
            }
        }

        // ReSharper disable UnusedMember.Global
        public void LyckadesAttLagraData()
        // ReSharper restore UnusedMember.Global
        {
            if (UppdateraOchFlyttaRapport(Settings.Default.GodkandSelectedPath, Settings.Default.FlyttaGodkandaRapporter,
                                          Resources.Godkand, DateTime.Now.ToString(CultureInfo.InvariantCulture)))
            {
                if (Settings.Default.SandEpostGodkand)
                {
                    if (Settings.Default.VisaDialogSandGodkand)
                    {
                        var sändRapport = new FormSendReport();
                        var bifogRapport = Settings.Default.BifogaRapportGodkand
                                             ? Path.GetFileName(Settings.Default.ReportFilePath)
                                             : "Ingen";
                        sändRapport.InitData(AktivMailTyp, AktivMottagarEpostAdress, Settings.Default.MailSubjectGodkand,
                                             Settings.Default.MailBodyGodkand, bifogRapport);

                        if (sändRapport.ShowDialog(this) == DialogResult.OK)
                        {
                            mAktivRapportForm.SändEpost(AktivMottagarEpostAdress, sändRapport.Titel, sändRapport.Meddelande,
                                                        Settings.Default.AnvandInternEpost, Settings.Default.BifogaRapportGodkand);
                        }
                    }
                    else
                    {
                        mAktivRapportForm.SändEpost(AktivMottagarEpostAdress, Settings.Default.MailSubjectGodkand,
                                                    Settings.Default.MailBodyGodkand, Settings.Default.AnvandInternEpost,
                                                    Settings.Default.BifogaRapportGodkand);
                    }
                }
            }
            MessageBox.Show(Resources.Datat_fran_KORUS_Rapporten,
                            Resources.Rapporten_ar_godkand_och_lagrad, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void buttonNekaRapport_Click(object sender, EventArgs e)
        {
            if (!UppdateraOchFlyttaRapport(Settings.Default.NekadSelectedPath, Settings.Default.FlyttaNekadeRapporter,
                                           Resources.Nekad, DateTime.Now.ToString(CultureInfo.InvariantCulture))) return;
            if (!Settings.Default.SandEpostNekad || mAktivRapportForm == null) return;
            if (Settings.Default.VisaDialogSandNekad)
            {
                //TODO: Lägg till dialog
                var sändRapport = new FormSendReport();
                var bifogRapport = Settings.Default.BifogaRapportNekad
                                     ? Path.GetFileName(Settings.Default.ReportFilePath)
                                     : "Ingen";
                sändRapport.InitData(AktivMailTyp, AktivMottagarEpostAdress, Settings.Default.MailSubjectNekad,
                                     Settings.Default.MailBodyNekad, bifogRapport);
                if (sändRapport.ShowDialog(this) == DialogResult.OK)
                {
                    mAktivRapportForm.SändEpost(AktivMottagarEpostAdress, sändRapport.Titel, sändRapport.Meddelande,
                                                Settings.Default.AnvandInternEpost, Settings.Default.BifogaRapportNekad);
                }
            }
            else
            {
                mAktivRapportForm.SändEpost(AktivMottagarEpostAdress, Settings.Default.MailSubjectNekad,
                                            Settings.Default.MailBodyNekad, Settings.Default.AnvandInternEpost,
                                            Settings.Default.BifogaRapportNekad);
            }
        }

        private void buttonÅterställ_Click(object sender, EventArgs e)
        {
            UppdateraOchFlyttaRapport(Settings.Default.AterstallSelectedPath, Settings.Default.FlyttaAterstalldaRapporter,
                                      Resources.Obehandlad, DateTime.MinValue.ToString(CultureInfo.InvariantCulture));
        }

        private void checkBoxSändEpostNekad_CheckedChanged(object sender, EventArgs e)
        {
            var checkBoxNekadEpost = sender as CheckBox;
            if (checkBoxNekadEpost != null)
            {
                Settings.Default.SandEpostNekad = checkBoxNekadEpost.Checked;
            }
        }

        private void checkBoxSändEpostGodkänd_CheckedChanged(object sender, EventArgs e)
        {
            var checkBoxGodkändEpost = sender as CheckBox;
            if (checkBoxGodkändEpost != null)
            {
                Settings.Default.SandEpostGodkand = checkBoxGodkändEpost.Checked;
            }
        }

        private void textBoxSokvag_TextChanged(object sender, EventArgs e)
        {
            var reportBrowseTextBox = sender as TextBox;
            if (reportBrowseTextBox != null)
            {
                Settings.Default.ReportBrowsePath = reportBrowseTextBox.Text;
            }
        }

    }
}