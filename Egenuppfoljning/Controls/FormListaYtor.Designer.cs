﻿using Egenuppfoljning.Interfaces;

namespace Egenuppfoljning.Controls
{
  partial class FormListaYtor
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.components = new System.ComponentModel.Container();
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormListaYtor));
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
        FastReport.Design.DesignerSettings designerSettings1 = new FastReport.Design.DesignerSettings();
        FastReport.Design.DesignerRestrictions designerRestrictions1 = new FastReport.Design.DesignerRestrictions();
        FastReport.Export.Email.EmailSettings emailSettings1 = new FastReport.Export.Email.EmailSettings();
        FastReport.PreviewSettings previewSettings1 = new FastReport.PreviewSettings();
        FastReport.ReportSettings reportSettings1 = new FastReport.ReportSettings();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
        this.groupBoxYtor = new System.Windows.Forms.GroupBox();
        this.groupBoxRapporttyp = new System.Windows.Forms.GroupBox();
        this.radioButtonRöjning = new System.Windows.Forms.RadioButton();
        this.radioButtonGallring = new System.Windows.Forms.RadioButton();
        this.radioButtonPlantering = new System.Windows.Forms.RadioButton();
        this.radioButtonMarkberedning = new System.Windows.Forms.RadioButton();
        this.labelFiltrering = new System.Windows.Forms.Label();
        this.groupBox1 = new System.Windows.Forms.GroupBox();
        this.buttonTaBortData = new System.Windows.Forms.Button();
        this.imageList32 = new System.Windows.Forms.ImageList(this.components);
        this.buttonFiltreraData = new System.Windows.Forms.Button();
        this.buttonGåTillHanteraData = new System.Windows.Forms.Button();
        this.buttonLagraÄndratData = new System.Windows.Forms.Button();
        this.buttonUppdateraData = new System.Windows.Forms.Button();
        this.dataGridViewYtor = new System.Windows.Forms.DataGridView();
        this.dataSetYtor = new System.Data.DataSet();
        this.dataTableYtor = new System.Data.DataTable();
        this.dataColumnRegionId = new System.Data.DataColumn();
        this.dataColumnDistriktId = new System.Data.DataColumn();
        this.dataColumnTraktnr = new System.Data.DataColumn();
        this.dataColumnStåndort = new System.Data.DataColumn();
        this.dataColumnEntreprenör = new System.Data.DataColumn();
        this.dataColumnRapportTyp = new System.Data.DataColumn();
        this.dataColumnYta = new System.Data.DataColumn();
        this.dataColumnOptimalt = new System.Data.DataColumn();
        this.dataColumnBra = new System.Data.DataColumn();
        this.dataColumnVaravBättre = new System.Data.DataColumn();
        this.dataColumnBlekJordsFläck = new System.Data.DataColumn();
        this.dataColumnOptimaltBra = new System.Data.DataColumn();
        this.dataColumnÖvrigt = new System.Data.DataColumn();
        this.dataColumnPlanterade = new System.Data.DataColumn();
        this.dataColumnPlanteringsDjupFel = new System.Data.DataColumn();
        this.dataColumnTilltryckning = new System.Data.DataColumn();
        this.dataColumnTall = new System.Data.DataColumn();
        this.dataColumnTallMedelHöjd = new System.Data.DataColumn();
        this.dataColumnGran = new System.Data.DataColumn();
        this.dataColumnGranMedelHöjd = new System.Data.DataColumn();
        this.dataColumnLöv = new System.Data.DataColumn();
        this.dataColumnLövMedelHöjd = new System.Data.DataColumn();
        this.dataColumnContorta = new System.Data.DataColumn();
        this.dataColumnContortaMedelHöjd = new System.Data.DataColumn();
        this.dataColumnHst = new System.Data.DataColumn();
        this.dataColumnMHöjd = new System.Data.DataColumn();
        this.dataColumnSumma = new System.Data.DataColumn();
        this.dataColumnSkador = new System.Data.DataColumn();
        this.dataColumnArtal = new System.Data.DataColumn();
        this.dataColumnStickvbredd = new System.Data.DataColumn();
        this.dataColumnInvTypId = new System.Data.DataColumn();
        this.dataColumnRojstam = new System.Data.DataColumn();
        this.environmentSettingsSammanställningRapporter = new FastReport.EnvironmentSettings();
        this.dataColumnInvTypNamn = new System.Data.DataColumn();
        this.rapportTypDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.invtypIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.invtypNamnDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.regionIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.distriktIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.traktnrDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.entreprenorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.standortDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.artalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.ytaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.optimaltDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.braDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.varavBattreDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.blekJordsFlackDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.optimaltBraDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.ovrigtDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.planteradeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.planteringsDjupFelDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.tillTryckningDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.stickvbreddDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.tallDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.tallMedelHojdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.granDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.granMedelHojdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.lovDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.lovMedelHojdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.contortaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.contortaMedelHojdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.hstDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.mHojdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.summaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.skadorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.RojstamDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.groupBoxYtor.SuspendLayout();
        this.groupBoxRapporttyp.SuspendLayout();
        this.groupBox1.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.dataGridViewYtor)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetYtor)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableYtor)).BeginInit();
        this.SuspendLayout();
        // 
        // groupBoxYtor
        // 
        this.groupBoxYtor.Controls.Add(this.groupBoxRapporttyp);
        this.groupBoxYtor.Controls.Add(this.labelFiltrering);
        this.groupBoxYtor.Controls.Add(this.groupBox1);
        this.groupBoxYtor.Controls.Add(this.dataGridViewYtor);
        this.groupBoxYtor.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.groupBoxYtor.Location = new System.Drawing.Point(9, 5);
        this.groupBoxYtor.Name = "groupBoxYtor";
        this.groupBoxYtor.Size = new System.Drawing.Size(890, 568);
        this.groupBoxYtor.TabIndex = 0;
        this.groupBoxYtor.TabStop = false;
        // 
        // groupBoxRapporttyp
        // 
        this.groupBoxRapporttyp.Controls.Add(this.radioButtonRöjning);
        this.groupBoxRapporttyp.Controls.Add(this.radioButtonGallring);
        this.groupBoxRapporttyp.Controls.Add(this.radioButtonPlantering);
        this.groupBoxRapporttyp.Controls.Add(this.radioButtonMarkberedning);
        this.groupBoxRapporttyp.Location = new System.Drawing.Point(0, 0);
        this.groupBoxRapporttyp.Name = "groupBoxRapporttyp";
        this.groupBoxRapporttyp.Size = new System.Drawing.Size(890, 49);
        this.groupBoxRapporttyp.TabIndex = 0;
        this.groupBoxRapporttyp.TabStop = false;
        this.groupBoxRapporttyp.Text = "Rapporttyp";
        // 
        // radioButtonRöjning
        // 
        this.radioButtonRöjning.AutoSize = true;
        this.radioButtonRöjning.Location = new System.Drawing.Point(350, 20);
        this.radioButtonRöjning.Name = "radioButtonRöjning";
        this.radioButtonRöjning.Size = new System.Drawing.Size(68, 17);
        this.radioButtonRöjning.TabIndex = 3;
        this.radioButtonRöjning.Text = "Röjning";
        this.radioButtonRöjning.UseVisualStyleBackColor = true;
        this.radioButtonRöjning.CheckedChanged += new System.EventHandler(this.radioButtonRöjning_CheckedChanged);
        // 
        // radioButtonGallring
        // 
        this.radioButtonGallring.AutoSize = true;
        this.radioButtonGallring.Location = new System.Drawing.Point(494, 21);
        this.radioButtonGallring.Name = "radioButtonGallring";
        this.radioButtonGallring.Size = new System.Drawing.Size(68, 17);
        this.radioButtonGallring.TabIndex = 4;
        this.radioButtonGallring.Text = "Gallring";
        this.radioButtonGallring.UseVisualStyleBackColor = true;
        this.radioButtonGallring.CheckedChanged += new System.EventHandler(this.radioButtonGallring_CheckedChanged);
        // 
        // radioButtonPlantering
        // 
        this.radioButtonPlantering.AutoSize = true;
        this.radioButtonPlantering.Location = new System.Drawing.Point(201, 20);
        this.radioButtonPlantering.Name = "radioButtonPlantering";
        this.radioButtonPlantering.Size = new System.Drawing.Size(83, 17);
        this.radioButtonPlantering.TabIndex = 2;
        this.radioButtonPlantering.Text = "Plantering";
        this.radioButtonPlantering.UseVisualStyleBackColor = true;
        this.radioButtonPlantering.CheckedChanged += new System.EventHandler(this.radioButtonPlantering_CheckedChanged);
        // 
        // radioButtonMarkberedning
        // 
        this.radioButtonMarkberedning.AutoSize = true;
        this.radioButtonMarkberedning.Location = new System.Drawing.Point(35, 21);
        this.radioButtonMarkberedning.Name = "radioButtonMarkberedning";
        this.radioButtonMarkberedning.Size = new System.Drawing.Size(111, 17);
        this.radioButtonMarkberedning.TabIndex = 1;
        this.radioButtonMarkberedning.Text = "Markberedning";
        this.radioButtonMarkberedning.UseVisualStyleBackColor = true;
        this.radioButtonMarkberedning.CheckedChanged += new System.EventHandler(this.radioButtonMarkberedning_CheckedChanged);
        // 
        // labelFiltrering
        // 
        this.labelFiltrering.Location = new System.Drawing.Point(9, 440);
        this.labelFiltrering.Name = "labelFiltrering";
        this.labelFiltrering.Size = new System.Drawing.Size(870, 60);
        this.labelFiltrering.TabIndex = 2;
        this.labelFiltrering.Text = "Filtrering: Ingen";
        // 
        // groupBox1
        // 
        this.groupBox1.Controls.Add(this.buttonTaBortData);
        this.groupBox1.Controls.Add(this.buttonFiltreraData);
        this.groupBox1.Controls.Add(this.buttonGåTillHanteraData);
        this.groupBox1.Controls.Add(this.buttonLagraÄndratData);
        this.groupBox1.Controls.Add(this.buttonUppdateraData);
        this.groupBox1.Location = new System.Drawing.Point(0, 502);
        this.groupBox1.Name = "groupBox1";
        this.groupBox1.Size = new System.Drawing.Size(890, 66);
        this.groupBox1.TabIndex = 3;
        this.groupBox1.TabStop = false;
        // 
        // buttonTaBortData
        // 
        this.buttonTaBortData.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.buttonTaBortData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonTaBortData.ImageIndex = 3;
        this.buttonTaBortData.ImageList = this.imageList32;
        this.buttonTaBortData.Location = new System.Drawing.Point(361, 16);
        this.buttonTaBortData.Name = "buttonTaBortData";
        this.buttonTaBortData.Size = new System.Drawing.Size(160, 40);
        this.buttonTaBortData.TabIndex = 2;
        this.buttonTaBortData.Text = "Ta bort";
        this.buttonTaBortData.UseVisualStyleBackColor = true;
        this.buttonTaBortData.Click += new System.EventHandler(this.buttonTaBortData_Click);
        // 
        // imageList32
        // 
        this.imageList32.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList32.ImageStream")));
        this.imageList32.TransparentColor = System.Drawing.SystemColors.ActiveCaption;
        this.imageList32.Images.SetKeyName(0, "Uppdatera.ico");
        this.imageList32.Images.SetKeyName(1, "Lagra.ico");
        this.imageList32.Images.SetKeyName(2, "HoppaTill.ico");
        this.imageList32.Images.SetKeyName(3, "TabortData.ico");
        this.imageList32.Images.SetKeyName(4, "Filtrera.ico");
        this.imageList32.Images.SetKeyName(5, "Rapport.ico");
        // 
        // buttonFiltreraData
        // 
        this.buttonFiltreraData.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.buttonFiltreraData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonFiltreraData.ImageIndex = 4;
        this.buttonFiltreraData.ImageList = this.imageList32;
        this.buttonFiltreraData.Location = new System.Drawing.Point(543, 16);
        this.buttonFiltreraData.Name = "buttonFiltreraData";
        this.buttonFiltreraData.Size = new System.Drawing.Size(160, 40);
        this.buttonFiltreraData.TabIndex = 3;
        this.buttonFiltreraData.Text = "Filtrera";
        this.buttonFiltreraData.UseVisualStyleBackColor = true;
        this.buttonFiltreraData.Click += new System.EventHandler(this.buttonFiltreraData_Click);
        // 
        // buttonGåTillHanteraData
        // 
        this.buttonGåTillHanteraData.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.buttonGåTillHanteraData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonGåTillHanteraData.ImageIndex = 2;
        this.buttonGåTillHanteraData.ImageList = this.imageList32;
        this.buttonGåTillHanteraData.Location = new System.Drawing.Point(719, 16);
        this.buttonGåTillHanteraData.Name = "buttonGåTillHanteraData";
        this.buttonGåTillHanteraData.Size = new System.Drawing.Size(160, 40);
        this.buttonGåTillHanteraData.TabIndex = 4;
        this.buttonGåTillHanteraData.Text = "Hantera Data";
        this.buttonGåTillHanteraData.UseVisualStyleBackColor = true;
        this.buttonGåTillHanteraData.Click += new System.EventHandler(this.buttonGåTillRapportData_Click);
        // 
        // buttonLagraÄndratData
        // 
        this.buttonLagraÄndratData.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.buttonLagraÄndratData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonLagraÄndratData.ImageIndex = 1;
        this.buttonLagraÄndratData.ImageList = this.imageList32;
        this.buttonLagraÄndratData.Location = new System.Drawing.Point(183, 16);
        this.buttonLagraÄndratData.Name = "buttonLagraÄndratData";
        this.buttonLagraÄndratData.Size = new System.Drawing.Size(160, 40);
        this.buttonLagraÄndratData.TabIndex = 1;
        this.buttonLagraÄndratData.Text = "Lagra";
        this.buttonLagraÄndratData.UseVisualStyleBackColor = true;
        this.buttonLagraÄndratData.Click += new System.EventHandler(this.buttonLagraÄndratData_Click);
        // 
        // buttonUppdateraData
        // 
        this.buttonUppdateraData.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.buttonUppdateraData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonUppdateraData.ImageIndex = 0;
        this.buttonUppdateraData.ImageList = this.imageList32;
        this.buttonUppdateraData.Location = new System.Drawing.Point(9, 16);
        this.buttonUppdateraData.Name = "buttonUppdateraData";
        this.buttonUppdateraData.Size = new System.Drawing.Size(160, 40);
        this.buttonUppdateraData.TabIndex = 0;
        this.buttonUppdateraData.Text = "Uppdatera";
        this.buttonUppdateraData.UseVisualStyleBackColor = true;
        this.buttonUppdateraData.Click += new System.EventHandler(this.buttonUppdateraData_Click);
        // 
        // dataGridViewYtor
        // 
        this.dataGridViewYtor.AllowUserToAddRows = false;
        this.dataGridViewYtor.AllowUserToOrderColumns = true;
        this.dataGridViewYtor.AllowUserToResizeRows = false;
        dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.dataGridViewYtor.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
        this.dataGridViewYtor.AutoGenerateColumns = false;
        this.dataGridViewYtor.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
        this.dataGridViewYtor.BackgroundColor = System.Drawing.SystemColors.ControlDark;
        this.dataGridViewYtor.BorderStyle = System.Windows.Forms.BorderStyle.None;
        dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
        dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
        dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        this.dataGridViewYtor.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
        this.dataGridViewYtor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
        this.dataGridViewYtor.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.rapportTypDataGridViewTextBoxColumn,
            this.invtypIdDataGridViewTextBoxColumn,
            this.invtypNamnDataGridViewTextBoxColumn,
            this.regionIdDataGridViewTextBoxColumn,
            this.distriktIdDataGridViewTextBoxColumn,
            this.traktnrDataGridViewTextBoxColumn,
            this.entreprenorDataGridViewTextBoxColumn,
            this.standortDataGridViewTextBoxColumn,
            this.artalDataGridViewTextBoxColumn,
            this.ytaDataGridViewTextBoxColumn,
            this.optimaltDataGridViewTextBoxColumn,
            this.braDataGridViewTextBoxColumn,
            this.varavBattreDataGridViewTextBoxColumn,
            this.blekJordsFlackDataGridViewTextBoxColumn,
            this.optimaltBraDataGridViewTextBoxColumn,
            this.ovrigtDataGridViewTextBoxColumn,
            this.planteradeDataGridViewTextBoxColumn,
            this.planteringsDjupFelDataGridViewTextBoxColumn,
            this.tillTryckningDataGridViewTextBoxColumn,
            this.stickvbreddDataGridViewTextBoxColumn,
            this.tallDataGridViewTextBoxColumn,
            this.tallMedelHojdDataGridViewTextBoxColumn,
            this.granDataGridViewTextBoxColumn,
            this.granMedelHojdDataGridViewTextBoxColumn,
            this.lovDataGridViewTextBoxColumn,
            this.lovMedelHojdDataGridViewTextBoxColumn,
            this.contortaDataGridViewTextBoxColumn,
            this.contortaMedelHojdDataGridViewTextBoxColumn,
            this.hstDataGridViewTextBoxColumn,
            this.mHojdDataGridViewTextBoxColumn,
            this.summaDataGridViewTextBoxColumn,
            this.skadorDataGridViewTextBoxColumn,
            this.RojstamDataGridViewTextBoxColumn});
        this.dataGridViewYtor.DataMember = "Ytor";
        this.dataGridViewYtor.DataSource = this.dataSetYtor;
        dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle19.BackColor = System.Drawing.SystemColors.Window;
        dataGridViewCellStyle19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        dataGridViewCellStyle19.ForeColor = System.Drawing.SystemColors.ControlText;
        dataGridViewCellStyle19.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle19.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
        this.dataGridViewYtor.DefaultCellStyle = dataGridViewCellStyle19;
        this.dataGridViewYtor.Location = new System.Drawing.Point(0, 47);
        this.dataGridViewYtor.MultiSelect = false;
        this.dataGridViewYtor.Name = "dataGridViewYtor";
        this.dataGridViewYtor.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
        dataGridViewCellStyle20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.dataGridViewYtor.RowsDefaultCellStyle = dataGridViewCellStyle20;
        this.dataGridViewYtor.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.dataGridViewYtor.Size = new System.Drawing.Size(890, 385);
        this.dataGridViewYtor.TabIndex = 1;
        this.dataGridViewYtor.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dataGridViewYtor_UserDeletingRow);
        this.dataGridViewYtor.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridViewYtor_CellFormatting);
        this.dataGridViewYtor.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dataGridViewYtor_CellValidating);
        this.dataGridViewYtor.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewYtor_CellEndEdit);
        this.dataGridViewYtor.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewYtor_DataError);
        // 
        // dataSetYtor
        // 
        this.dataSetYtor.DataSetName = "DataSetYtor";
        this.dataSetYtor.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableYtor});
        // 
        // dataTableYtor
        // 
        this.dataTableYtor.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnRegionId,
            this.dataColumnDistriktId,
            this.dataColumnTraktnr,
            this.dataColumnStåndort,
            this.dataColumnEntreprenör,
            this.dataColumnRapportTyp,
            this.dataColumnYta,
            this.dataColumnOptimalt,
            this.dataColumnBra,
            this.dataColumnVaravBättre,
            this.dataColumnBlekJordsFläck,
            this.dataColumnOptimaltBra,
            this.dataColumnÖvrigt,
            this.dataColumnPlanterade,
            this.dataColumnPlanteringsDjupFel,
            this.dataColumnTilltryckning,
            this.dataColumnTall,
            this.dataColumnTallMedelHöjd,
            this.dataColumnGran,
            this.dataColumnGranMedelHöjd,
            this.dataColumnLöv,
            this.dataColumnLövMedelHöjd,
            this.dataColumnContorta,
            this.dataColumnContortaMedelHöjd,
            this.dataColumnHst,
            this.dataColumnMHöjd,
            this.dataColumnSumma,
            this.dataColumnSkador,
            this.dataColumnArtal,
            this.dataColumnStickvbredd,
            this.dataColumnInvTypId,
            this.dataColumnRojstam,
            this.dataColumnInvTypNamn});
        this.dataTableYtor.TableName = "Ytor";
        // 
        // dataColumnRegionId
        // 
        this.dataColumnRegionId.ColumnName = "RegionId";
        this.dataColumnRegionId.DataType = typeof(int);
        // 
        // dataColumnDistriktId
        // 
        this.dataColumnDistriktId.ColumnName = "DistriktId";
        this.dataColumnDistriktId.DataType = typeof(int);
        // 
        // dataColumnTraktnr
        // 
        this.dataColumnTraktnr.ColumnName = "Traktnr";
        this.dataColumnTraktnr.DataType = typeof(int);
        // 
        // dataColumnStåndort
        // 
        this.dataColumnStåndort.ColumnName = "Standort";
        this.dataColumnStåndort.DataType = typeof(int);
        // 
        // dataColumnEntreprenör
        // 
        this.dataColumnEntreprenör.ColumnName = "Entreprenor";
        this.dataColumnEntreprenör.DataType = typeof(int);
        // 
        // dataColumnRapportTyp
        // 
        this.dataColumnRapportTyp.ColumnName = "RapportTyp";
        // 
        // dataColumnYta
        // 
        this.dataColumnYta.ColumnName = "Yta";
        this.dataColumnYta.DataType = typeof(int);
        // 
        // dataColumnOptimalt
        // 
        this.dataColumnOptimalt.ColumnName = "Optimalt";
        this.dataColumnOptimalt.DataType = typeof(int);
        // 
        // dataColumnBra
        // 
        this.dataColumnBra.ColumnName = "Bra";
        this.dataColumnBra.DataType = typeof(int);
        // 
        // dataColumnVaravBättre
        // 
        this.dataColumnVaravBättre.ColumnName = "VaravBattre";
        this.dataColumnVaravBättre.DataType = typeof(int);
        // 
        // dataColumnBlekJordsFläck
        // 
        this.dataColumnBlekJordsFläck.ColumnName = "BlekJordsFlack";
        this.dataColumnBlekJordsFläck.DataType = typeof(int);
        // 
        // dataColumnOptimaltBra
        // 
        this.dataColumnOptimaltBra.ColumnName = "OptimaltBra";
        this.dataColumnOptimaltBra.DataType = typeof(int);
        // 
        // dataColumnÖvrigt
        // 
        this.dataColumnÖvrigt.ColumnName = "Ovrigt";
        this.dataColumnÖvrigt.DataType = typeof(int);
        // 
        // dataColumnPlanterade
        // 
        this.dataColumnPlanterade.ColumnName = "Planterade";
        this.dataColumnPlanterade.DataType = typeof(int);
        // 
        // dataColumnPlanteringsDjupFel
        // 
        this.dataColumnPlanteringsDjupFel.ColumnName = "PlanteringsDjupFel";
        this.dataColumnPlanteringsDjupFel.DataType = typeof(int);
        // 
        // dataColumnTilltryckning
        // 
        this.dataColumnTilltryckning.ColumnName = "TillTryckning";
        this.dataColumnTilltryckning.DataType = typeof(int);
        // 
        // dataColumnTall
        // 
        this.dataColumnTall.ColumnName = "Tall";
        this.dataColumnTall.DataType = typeof(int);
        // 
        // dataColumnTallMedelHöjd
        // 
        this.dataColumnTallMedelHöjd.ColumnName = "TallMedelHojd";
        this.dataColumnTallMedelHöjd.DataType = typeof(double);
        // 
        // dataColumnGran
        // 
        this.dataColumnGran.ColumnName = "Gran";
        this.dataColumnGran.DataType = typeof(int);
        // 
        // dataColumnGranMedelHöjd
        // 
        this.dataColumnGranMedelHöjd.ColumnName = "GranMedelHojd";
        this.dataColumnGranMedelHöjd.DataType = typeof(double);
        // 
        // dataColumnLöv
        // 
        this.dataColumnLöv.ColumnName = "Lov";
        this.dataColumnLöv.DataType = typeof(int);
        // 
        // dataColumnLövMedelHöjd
        // 
        this.dataColumnLövMedelHöjd.ColumnName = "LovMedelHojd";
        this.dataColumnLövMedelHöjd.DataType = typeof(double);
        // 
        // dataColumnContorta
        // 
        this.dataColumnContorta.ColumnName = "Contorta";
        this.dataColumnContorta.DataType = typeof(int);
        // 
        // dataColumnContortaMedelHöjd
        // 
        this.dataColumnContortaMedelHöjd.ColumnName = "ContortaMedelHojd";
        this.dataColumnContortaMedelHöjd.DataType = typeof(double);
        // 
        // dataColumnHst
        // 
        this.dataColumnHst.ColumnName = "Hst";
        this.dataColumnHst.DataType = typeof(int);
        // 
        // dataColumnMHöjd
        // 
        this.dataColumnMHöjd.ColumnName = "MHojd";
        this.dataColumnMHöjd.DataType = typeof(double);
        // 
        // dataColumnSumma
        // 
        this.dataColumnSumma.ColumnName = "Summa";
        this.dataColumnSumma.DataType = typeof(int);
        // 
        // dataColumnSkador
        // 
        this.dataColumnSkador.ColumnName = "Skador";
        this.dataColumnSkador.DataType = typeof(int);
        // 
        // dataColumnArtal
        // 
        this.dataColumnArtal.ColumnName = "Artal";
        this.dataColumnArtal.DataType = typeof(int);
        // 
        // dataColumnStickvbredd
        // 
        this.dataColumnStickvbredd.ColumnName = "Stickvbredd";
        this.dataColumnStickvbredd.DataType = typeof(double);
        // 
        // dataColumnInvTypId
        // 
        this.dataColumnInvTypId.ColumnName = "InvTypId";
        this.dataColumnInvTypId.DataType = typeof(int);
        // 
        // dataColumnRojstam
        // 
        this.dataColumnRojstam.ColumnName = "Rojstam";
        this.dataColumnRojstam.DataType = typeof(int);
        // 
        // environmentSettingsSammanställningRapporter
        // 
        designerSettings1.ApplicationConnection = null;
        designerSettings1.DefaultFont = new System.Drawing.Font("Arial", 10F);
        designerSettings1.Icon = ((System.Drawing.Icon)(resources.GetObject("designerSettings1.Icon")));
        designerSettings1.Restrictions = designerRestrictions1;
        designerSettings1.Text = "";
        this.environmentSettingsSammanställningRapporter.DesignerSettings = designerSettings1;
        emailSettings1.Address = "";
        emailSettings1.Host = "";
        emailSettings1.MessageTemplate = "";
        emailSettings1.Name = "";
        emailSettings1.Password = "";
        emailSettings1.Port = 49;
        emailSettings1.UserName = "";
        this.environmentSettingsSammanställningRapporter.EmailSettings = emailSettings1;
        previewSettings1.Buttons = ((FastReport.PreviewButtons)((((((FastReport.PreviewButtons.Print | FastReport.PreviewButtons.Save)
                    | FastReport.PreviewButtons.Find)
                    | FastReport.PreviewButtons.Zoom)
                    | FastReport.PreviewButtons.Navigator)
                    | FastReport.PreviewButtons.Close)));
        previewSettings1.Icon = ((System.Drawing.Icon)(resources.GetObject("previewSettings1.Icon")));
        previewSettings1.Text = "";
        this.environmentSettingsSammanställningRapporter.PreviewSettings = previewSettings1;
        this.environmentSettingsSammanställningRapporter.ReportSettings = reportSettings1;
        this.environmentSettingsSammanställningRapporter.UIStyle = FastReport.Utils.UIStyle.Office2007Black;
        // 
        // dataColumnInvTypNamn
        // 
        this.dataColumnInvTypNamn.Caption = "InvTypNamn";
        this.dataColumnInvTypNamn.ColumnName = "InvTypNamn";
        // 
        // rapportTypDataGridViewTextBoxColumn
        // 
        this.rapportTypDataGridViewTextBoxColumn.DataPropertyName = "RapportTyp";
        dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle3.BackColor = System.Drawing.Color.LemonChiffon;
        this.rapportTypDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
        this.rapportTypDataGridViewTextBoxColumn.HeaderText = "Rapport typ";
        this.rapportTypDataGridViewTextBoxColumn.Name = "rapportTypDataGridViewTextBoxColumn";
        this.rapportTypDataGridViewTextBoxColumn.ReadOnly = true;
        this.rapportTypDataGridViewTextBoxColumn.Width = 90;
        // 
        // invtypIdDataGridViewTextBoxColumn
        // 
        this.invtypIdDataGridViewTextBoxColumn.DataPropertyName = "InvTypId";
        dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle4.BackColor = System.Drawing.Color.LemonChiffon;
        this.invtypIdDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
        this.invtypIdDataGridViewTextBoxColumn.HeaderText = "InvTyp";
        this.invtypIdDataGridViewTextBoxColumn.Name = "invtypIdDataGridViewTextBoxColumn";
        this.invtypIdDataGridViewTextBoxColumn.ReadOnly = true;
        this.invtypIdDataGridViewTextBoxColumn.Width = 70;
        // 
        // invtypNamnDataGridViewTextBoxColumn
        // 
        this.invtypNamnDataGridViewTextBoxColumn.DataPropertyName = "InvTypNamn";
        dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle5.BackColor = System.Drawing.Color.LemonChiffon;
        this.invtypNamnDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle5;
        this.invtypNamnDataGridViewTextBoxColumn.HeaderText = "InvTypNamn";
        this.invtypNamnDataGridViewTextBoxColumn.Name = "invtypNamnDataGridViewTextBoxColumn";
        this.invtypNamnDataGridViewTextBoxColumn.ReadOnly = true;
        this.invtypNamnDataGridViewTextBoxColumn.Width = 80;
        // 
        // regionIdDataGridViewTextBoxColumn
        // 
        this.regionIdDataGridViewTextBoxColumn.DataPropertyName = "RegionId";
        dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle6.BackColor = System.Drawing.Color.LemonChiffon;
        this.regionIdDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle6;
        this.regionIdDataGridViewTextBoxColumn.HeaderText = "Region id";
        this.regionIdDataGridViewTextBoxColumn.Name = "regionIdDataGridViewTextBoxColumn";
        this.regionIdDataGridViewTextBoxColumn.ReadOnly = true;
        this.regionIdDataGridViewTextBoxColumn.Width = 70;
        // 
        // distriktIdDataGridViewTextBoxColumn
        // 
        this.distriktIdDataGridViewTextBoxColumn.DataPropertyName = "DistriktId";
        dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle7.BackColor = System.Drawing.Color.LemonChiffon;
        this.distriktIdDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle7;
        this.distriktIdDataGridViewTextBoxColumn.HeaderText = "Distrikt id";
        this.distriktIdDataGridViewTextBoxColumn.Name = "distriktIdDataGridViewTextBoxColumn";
        this.distriktIdDataGridViewTextBoxColumn.ReadOnly = true;
        this.distriktIdDataGridViewTextBoxColumn.Width = 70;
        // 
        // traktnrDataGridViewTextBoxColumn
        // 
        this.traktnrDataGridViewTextBoxColumn.DataPropertyName = "Traktnr";
        dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle8.BackColor = System.Drawing.Color.LemonChiffon;
        dataGridViewCellStyle8.Format = "000000";
        dataGridViewCellStyle8.NullValue = null;
        this.traktnrDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle8;
        this.traktnrDataGridViewTextBoxColumn.HeaderText = "Trakt nr";
        this.traktnrDataGridViewTextBoxColumn.Name = "traktnrDataGridViewTextBoxColumn";
        this.traktnrDataGridViewTextBoxColumn.ReadOnly = true;
        this.traktnrDataGridViewTextBoxColumn.Width = 60;
        // 
        // entreprenorDataGridViewTextBoxColumn
        // 
        this.entreprenorDataGridViewTextBoxColumn.DataPropertyName = "Entreprenor";
        dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle9.BackColor = System.Drawing.Color.LemonChiffon;
        dataGridViewCellStyle9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        dataGridViewCellStyle9.Format = "0000";
        dataGridViewCellStyle9.NullValue = null;
        this.entreprenorDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle9;
        this.entreprenorDataGridViewTextBoxColumn.HeaderText = "Entreprenörnr";
        this.entreprenorDataGridViewTextBoxColumn.Name = "entreprenorDataGridViewTextBoxColumn";
        this.entreprenorDataGridViewTextBoxColumn.ReadOnly = true;
        this.entreprenorDataGridViewTextBoxColumn.Width = 95;
        // 
        // standortDataGridViewTextBoxColumn
        // 
        this.standortDataGridViewTextBoxColumn.DataPropertyName = "Standort";
        dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle10.BackColor = System.Drawing.Color.LemonChiffon;
        dataGridViewCellStyle10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.standortDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle10;
        this.standortDataGridViewTextBoxColumn.HeaderText = "Traktdel";
        this.standortDataGridViewTextBoxColumn.Name = "standortDataGridViewTextBoxColumn";
        this.standortDataGridViewTextBoxColumn.ReadOnly = true;
        this.standortDataGridViewTextBoxColumn.Width = 65;
        // 
        // artalDataGridViewTextBoxColumn
        // 
        this.artalDataGridViewTextBoxColumn.DataPropertyName = "Artal";
        dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle11.BackColor = System.Drawing.Color.LemonChiffon;
        this.artalDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle11;
        this.artalDataGridViewTextBoxColumn.HeaderText = "Årtal";
        this.artalDataGridViewTextBoxColumn.Name = "artalDataGridViewTextBoxColumn";
        this.artalDataGridViewTextBoxColumn.Width = 50;
        // 
        // ytaDataGridViewTextBoxColumn
        // 
        this.ytaDataGridViewTextBoxColumn.DataPropertyName = "Yta";
        dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle12.BackColor = System.Drawing.Color.Khaki;
        dataGridViewCellStyle12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.ytaDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle12;
        this.ytaDataGridViewTextBoxColumn.HeaderText = "Yta";
        this.ytaDataGridViewTextBoxColumn.Name = "ytaDataGridViewTextBoxColumn";
        this.ytaDataGridViewTextBoxColumn.ReadOnly = true;
        this.ytaDataGridViewTextBoxColumn.Width = 40;
        // 
        // optimaltDataGridViewTextBoxColumn
        // 
        this.optimaltDataGridViewTextBoxColumn.DataPropertyName = "Optimalt";
        this.optimaltDataGridViewTextBoxColumn.HeaderText = "Optimalt";
        this.optimaltDataGridViewTextBoxColumn.Name = "optimaltDataGridViewTextBoxColumn";
        this.optimaltDataGridViewTextBoxColumn.Width = 60;
        // 
        // braDataGridViewTextBoxColumn
        // 
        this.braDataGridViewTextBoxColumn.DataPropertyName = "Bra";
        this.braDataGridViewTextBoxColumn.HeaderText = "Bra";
        this.braDataGridViewTextBoxColumn.Name = "braDataGridViewTextBoxColumn";
        this.braDataGridViewTextBoxColumn.Width = 40;
        // 
        // varavBattreDataGridViewTextBoxColumn
        // 
        this.varavBattreDataGridViewTextBoxColumn.DataPropertyName = "VaravBattre";
        this.varavBattreDataGridViewTextBoxColumn.HeaderText = "Varav bättre";
        this.varavBattreDataGridViewTextBoxColumn.Name = "varavBattreDataGridViewTextBoxColumn";
        this.varavBattreDataGridViewTextBoxColumn.Width = 85;
        // 
        // blekJordsFlackDataGridViewTextBoxColumn
        // 
        this.blekJordsFlackDataGridViewTextBoxColumn.DataPropertyName = "BlekJordsFlack";
        this.blekJordsFlackDataGridViewTextBoxColumn.HeaderText = "Blekjordsfläck";
        this.blekJordsFlackDataGridViewTextBoxColumn.Name = "blekJordsFlackDataGridViewTextBoxColumn";
        // 
        // optimaltBraDataGridViewTextBoxColumn
        // 
        this.optimaltBraDataGridViewTextBoxColumn.DataPropertyName = "OptimaltBra";
        this.optimaltBraDataGridViewTextBoxColumn.HeaderText = "Optimalt bra";
        this.optimaltBraDataGridViewTextBoxColumn.Name = "optimaltBraDataGridViewTextBoxColumn";
        this.optimaltBraDataGridViewTextBoxColumn.Visible = false;
        this.optimaltBraDataGridViewTextBoxColumn.Width = 90;
        // 
        // ovrigtDataGridViewTextBoxColumn
        // 
        this.ovrigtDataGridViewTextBoxColumn.DataPropertyName = "Ovrigt";
        this.ovrigtDataGridViewTextBoxColumn.HeaderText = "Övrigt";
        this.ovrigtDataGridViewTextBoxColumn.Name = "ovrigtDataGridViewTextBoxColumn";
        this.ovrigtDataGridViewTextBoxColumn.Width = 60;
        // 
        // planteradeDataGridViewTextBoxColumn
        // 
        this.planteradeDataGridViewTextBoxColumn.DataPropertyName = "Planterade";
        this.planteradeDataGridViewTextBoxColumn.HeaderText = "Planterade";
        this.planteradeDataGridViewTextBoxColumn.Name = "planteradeDataGridViewTextBoxColumn";
        this.planteradeDataGridViewTextBoxColumn.Visible = false;
        this.planteradeDataGridViewTextBoxColumn.Width = 75;
        // 
        // planteringsDjupFelDataGridViewTextBoxColumn
        // 
        this.planteringsDjupFelDataGridViewTextBoxColumn.DataPropertyName = "PlanteringsDjupFel";
        this.planteringsDjupFelDataGridViewTextBoxColumn.HeaderText = "Planteringsdjupfel";
        this.planteringsDjupFelDataGridViewTextBoxColumn.Name = "planteringsDjupFelDataGridViewTextBoxColumn";
        this.planteringsDjupFelDataGridViewTextBoxColumn.Width = 120;
        // 
        // tillTryckningDataGridViewTextBoxColumn
        // 
        this.tillTryckningDataGridViewTextBoxColumn.DataPropertyName = "TillTryckning";
        this.tillTryckningDataGridViewTextBoxColumn.HeaderText = "Tilltryckning";
        this.tillTryckningDataGridViewTextBoxColumn.Name = "tillTryckningDataGridViewTextBoxColumn";
        this.tillTryckningDataGridViewTextBoxColumn.Width = 90;
        // 
        // stickvbreddDataGridViewTextBoxColumn
        // 
        this.stickvbreddDataGridViewTextBoxColumn.DataPropertyName = "Stickvbredd";
        dataGridViewCellStyle13.Format = "N1";
        dataGridViewCellStyle13.NullValue = null;
        this.stickvbreddDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle13;
        this.stickvbreddDataGridViewTextBoxColumn.HeaderText = "Stickvbredd";
        this.stickvbreddDataGridViewTextBoxColumn.Name = "stickvbreddDataGridViewTextBoxColumn";
        this.stickvbreddDataGridViewTextBoxColumn.Width = 90;
        // 
        // tallDataGridViewTextBoxColumn
        // 
        this.tallDataGridViewTextBoxColumn.DataPropertyName = "Tall";
        this.tallDataGridViewTextBoxColumn.HeaderText = "Tall";
        this.tallDataGridViewTextBoxColumn.Name = "tallDataGridViewTextBoxColumn";
        this.tallDataGridViewTextBoxColumn.Width = 40;
        // 
        // tallMedelHojdDataGridViewTextBoxColumn
        // 
        this.tallMedelHojdDataGridViewTextBoxColumn.DataPropertyName = "TallMedelHojd";
        dataGridViewCellStyle14.Format = "N1";
        dataGridViewCellStyle14.NullValue = null;
        this.tallMedelHojdDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle14;
        this.tallMedelHojdDataGridViewTextBoxColumn.HeaderText = "Tall medelHöjd";
        this.tallMedelHojdDataGridViewTextBoxColumn.Name = "tallMedelHojdDataGridViewTextBoxColumn";
        // 
        // granDataGridViewTextBoxColumn
        // 
        this.granDataGridViewTextBoxColumn.DataPropertyName = "Gran";
        this.granDataGridViewTextBoxColumn.HeaderText = "Gran";
        this.granDataGridViewTextBoxColumn.Name = "granDataGridViewTextBoxColumn";
        this.granDataGridViewTextBoxColumn.Width = 40;
        // 
        // granMedelHojdDataGridViewTextBoxColumn
        // 
        this.granMedelHojdDataGridViewTextBoxColumn.DataPropertyName = "GranMedelHojd";
        dataGridViewCellStyle15.Format = "N1";
        dataGridViewCellStyle15.NullValue = null;
        this.granMedelHojdDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle15;
        this.granMedelHojdDataGridViewTextBoxColumn.HeaderText = "Gran medelHöjd";
        this.granMedelHojdDataGridViewTextBoxColumn.Name = "granMedelHojdDataGridViewTextBoxColumn";
        this.granMedelHojdDataGridViewTextBoxColumn.Width = 110;
        // 
        // lovDataGridViewTextBoxColumn
        // 
        this.lovDataGridViewTextBoxColumn.DataPropertyName = "Lov";
        this.lovDataGridViewTextBoxColumn.HeaderText = "Löv";
        this.lovDataGridViewTextBoxColumn.Name = "lovDataGridViewTextBoxColumn";
        this.lovDataGridViewTextBoxColumn.Width = 40;
        // 
        // lovMedelHojdDataGridViewTextBoxColumn
        // 
        this.lovMedelHojdDataGridViewTextBoxColumn.DataPropertyName = "LovMedelHojd";
        dataGridViewCellStyle16.Format = "N1";
        dataGridViewCellStyle16.NullValue = null;
        this.lovMedelHojdDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle16;
        this.lovMedelHojdDataGridViewTextBoxColumn.HeaderText = "Löv medelhöjd";
        this.lovMedelHojdDataGridViewTextBoxColumn.Name = "lovMedelHojdDataGridViewTextBoxColumn";
        this.lovMedelHojdDataGridViewTextBoxColumn.Width = 110;
        // 
        // contortaDataGridViewTextBoxColumn
        // 
        this.contortaDataGridViewTextBoxColumn.DataPropertyName = "Contorta";
        this.contortaDataGridViewTextBoxColumn.HeaderText = "Contorta";
        this.contortaDataGridViewTextBoxColumn.Name = "contortaDataGridViewTextBoxColumn";
        this.contortaDataGridViewTextBoxColumn.Width = 65;
        // 
        // contortaMedelHojdDataGridViewTextBoxColumn
        // 
        this.contortaMedelHojdDataGridViewTextBoxColumn.DataPropertyName = "ContortaMedelHojd";
        dataGridViewCellStyle17.Format = "N1";
        dataGridViewCellStyle17.NullValue = null;
        this.contortaMedelHojdDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle17;
        this.contortaMedelHojdDataGridViewTextBoxColumn.HeaderText = "Contorta medelhöjd";
        this.contortaMedelHojdDataGridViewTextBoxColumn.Name = "contortaMedelHojdDataGridViewTextBoxColumn";
        this.contortaMedelHojdDataGridViewTextBoxColumn.Width = 130;
        // 
        // hstDataGridViewTextBoxColumn
        // 
        this.hstDataGridViewTextBoxColumn.DataPropertyName = "Hst";
        this.hstDataGridViewTextBoxColumn.HeaderText = "Hst";
        this.hstDataGridViewTextBoxColumn.Name = "hstDataGridViewTextBoxColumn";
        this.hstDataGridViewTextBoxColumn.Visible = false;
        this.hstDataGridViewTextBoxColumn.Width = 40;
        // 
        // mHojdDataGridViewTextBoxColumn
        // 
        this.mHojdDataGridViewTextBoxColumn.DataPropertyName = "MHojd";
        dataGridViewCellStyle18.Format = "N1";
        dataGridViewCellStyle18.NullValue = null;
        this.mHojdDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle18;
        this.mHojdDataGridViewTextBoxColumn.HeaderText = "MHöjd";
        this.mHojdDataGridViewTextBoxColumn.Name = "mHojdDataGridViewTextBoxColumn";
        this.mHojdDataGridViewTextBoxColumn.Visible = false;
        // 
        // summaDataGridViewTextBoxColumn
        // 
        this.summaDataGridViewTextBoxColumn.DataPropertyName = "Summa";
        this.summaDataGridViewTextBoxColumn.HeaderText = "Summa";
        this.summaDataGridViewTextBoxColumn.Name = "summaDataGridViewTextBoxColumn";
        this.summaDataGridViewTextBoxColumn.Visible = false;
        this.summaDataGridViewTextBoxColumn.Width = 55;
        // 
        // skadorDataGridViewTextBoxColumn
        // 
        this.skadorDataGridViewTextBoxColumn.DataPropertyName = "Skador";
        this.skadorDataGridViewTextBoxColumn.HeaderText = "Skador";
        this.skadorDataGridViewTextBoxColumn.Name = "skadorDataGridViewTextBoxColumn";
        this.skadorDataGridViewTextBoxColumn.Width = 55;
        // 
        // RojstamDataGridViewTextBoxColumn
        // 
        this.RojstamDataGridViewTextBoxColumn.DataPropertyName = "Rojstam";
        this.RojstamDataGridViewTextBoxColumn.HeaderText = "Röjstam";
        this.RojstamDataGridViewTextBoxColumn.Name = "RojstamDataGridViewTextBoxColumn";
        this.RojstamDataGridViewTextBoxColumn.Width = 40;
        // 
        // FormListaYtor
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.Controls.Add(this.groupBoxYtor);
        this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.Name = "FormListaYtor";
        this.Size = new System.Drawing.Size(910, 583);
        this.Load += new System.EventHandler(this.FormListaYtor_Load);
        this.groupBoxYtor.ResumeLayout(false);
        this.groupBoxRapporttyp.ResumeLayout(false);
        this.groupBoxRapporttyp.PerformLayout();
        this.groupBox1.ResumeLayout(false);
        ((System.ComponentModel.ISupportInitialize)(this.dataGridViewYtor)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetYtor)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableYtor)).EndInit();
        this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox groupBoxYtor;
    private System.Windows.Forms.DataGridView dataGridViewYtor;
    private System.Data.DataSet dataSetYtor;
    private System.Data.DataTable dataTableYtor;
    private System.Data.DataColumn dataColumnRegionId;
    private System.Data.DataColumn dataColumnDistriktId;
    private System.Data.DataColumn dataColumnTraktnr;
    private System.Data.DataColumn dataColumnStåndort;
    private System.Data.DataColumn dataColumnEntreprenör;
    private System.Data.DataColumn dataColumnRapportTyp;
    private System.Data.DataColumn dataColumnYta;
    private System.Data.DataColumn dataColumnOptimalt;
    private System.Data.DataColumn dataColumnBra;
    private System.Data.DataColumn dataColumnVaravBättre;
    private System.Data.DataColumn dataColumnBlekJordsFläck;
    private System.Data.DataColumn dataColumnOptimaltBra;
    private System.Data.DataColumn dataColumnÖvrigt;
    private System.Data.DataColumn dataColumnPlanterade;
    private System.Data.DataColumn dataColumnPlanteringsDjupFel;
    private System.Data.DataColumn dataColumnTilltryckning;
    private System.Data.DataColumn dataColumnTall;
    private System.Data.DataColumn dataColumnTallMedelHöjd;
    private System.Data.DataColumn dataColumnGran;
    private System.Data.DataColumn dataColumnGranMedelHöjd;
    private System.Data.DataColumn dataColumnLöv;
    private System.Data.DataColumn dataColumnLövMedelHöjd;
    private System.Data.DataColumn dataColumnContorta;
    private System.Data.DataColumn dataColumnContortaMedelHöjd;
    private System.Data.DataColumn dataColumnHst;
    private System.Data.DataColumn dataColumnMHöjd;
    private System.Data.DataColumn dataColumnSumma;
    private System.Data.DataColumn dataColumnSkador;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.ImageList imageList32;
    private System.Windows.Forms.Button buttonUppdateraData;
    private System.Windows.Forms.Button buttonLagraÄndratData;
    private System.Windows.Forms.Button buttonGåTillHanteraData;
    private System.Windows.Forms.Button buttonFiltreraData;
    private System.Windows.Forms.Button buttonTaBortData;
    private System.Windows.Forms.Label labelFiltrering;
    private System.Data.DataColumn dataColumnArtal;
    private System.Windows.Forms.GroupBox groupBoxRapporttyp;
    private System.Windows.Forms.RadioButton radioButtonRöjning;
    private System.Windows.Forms.RadioButton radioButtonGallring;
    private System.Windows.Forms.RadioButton radioButtonPlantering;
    private System.Windows.Forms.RadioButton radioButtonMarkberedning;

    private System.Data.DataColumn dataColumnStickvbredd;
    private FastReport.EnvironmentSettings environmentSettingsSammanställningRapporter;
    private System.Data.DataColumn dataColumnInvTypId;
    private System.Data.DataColumn dataColumnRojstam;
    private System.Data.DataColumn dataColumnInvTypNamn;
    private System.Windows.Forms.DataGridViewTextBoxColumn rapportTypDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn invtypIdDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn invtypNamnDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn regionIdDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn distriktIdDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn traktnrDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn entreprenorDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn standortDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn artalDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn ytaDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn optimaltDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn braDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn varavBattreDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn blekJordsFlackDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn optimaltBraDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn ovrigtDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn planteradeDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn planteringsDjupFelDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn tillTryckningDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn stickvbreddDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn tallDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn tallMedelHojdDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn granDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn granMedelHojdDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn lovDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn lovMedelHojdDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn contortaDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn contortaMedelHojdDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn hstDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn mHojdDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn summaDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn skadorDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn RojstamDataGridViewTextBoxColumn;
  }
}
