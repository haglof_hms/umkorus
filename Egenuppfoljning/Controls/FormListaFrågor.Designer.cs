﻿namespace Egenuppfoljning.Controls
{
  partial class FormListaFrågor
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        this.components = new System.ComponentModel.Container();
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormListaFrågor));
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
        FastReport.Design.DesignerSettings designerSettings1 = new FastReport.Design.DesignerSettings();
        FastReport.Design.DesignerRestrictions designerRestrictions1 = new FastReport.Design.DesignerRestrictions();
        FastReport.Export.Email.EmailSettings emailSettings1 = new FastReport.Export.Email.EmailSettings();
        FastReport.PreviewSettings previewSettings1 = new FastReport.PreviewSettings();
        FastReport.ReportSettings reportSettings1 = new FastReport.ReportSettings();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
        this.groupBoxFrågor = new System.Windows.Forms.GroupBox();
        this.buttonFrågor = new System.Windows.Forms.Button();
        this.imageList32 = new System.Windows.Forms.ImageList(this.components);
        this.groupBoxRapporttyp = new System.Windows.Forms.GroupBox();
        this.radioButtonSlutavverkning = new System.Windows.Forms.RadioButton();
        this.radioButtonBiobränsle = new System.Windows.Forms.RadioButton();
        this.radioButtonRöjning = new System.Windows.Forms.RadioButton();
        this.radioButtonGallring = new System.Windows.Forms.RadioButton();
        this.radioButtonPlantering = new System.Windows.Forms.RadioButton();
        this.radioButtonMarkberedning = new System.Windows.Forms.RadioButton();
        this.labelFiltrering = new System.Windows.Forms.Label();
        this.groupBox1 = new System.Windows.Forms.GroupBox();
        this.buttonTaBortData = new System.Windows.Forms.Button();
        this.buttonFiltreraData = new System.Windows.Forms.Button();
        this.buttonGåTillHanteraData = new System.Windows.Forms.Button();
        this.buttonLagraÄndratData = new System.Windows.Forms.Button();
        this.buttonUppdateraData = new System.Windows.Forms.Button();
        this.dataGridViewFrågor = new System.Windows.Forms.DataGridView();
        this.dataSetFrågor = new System.Data.DataSet();
        this.dataTableFrågor = new System.Data.DataTable();
        this.dataColumnRegionId = new System.Data.DataColumn();
        this.dataColumnDistriktId = new System.Data.DataColumn();
        this.dataColumnTraktnr = new System.Data.DataColumn();
        this.dataColumnStåndort = new System.Data.DataColumn();
        this.dataColumnEntreprenör = new System.Data.DataColumn();
        this.dataColumnRapportTyp = new System.Data.DataColumn();
        this.dataColumnFråga1 = new System.Data.DataColumn();
        this.dataColumnFråga2 = new System.Data.DataColumn();
        this.dataColumnFråga3 = new System.Data.DataColumn();
        this.dataColumnFråga4 = new System.Data.DataColumn();
        this.dataColumnFråga5 = new System.Data.DataColumn();
        this.dataColumnFråga6 = new System.Data.DataColumn();
        this.dataColumnFråga7 = new System.Data.DataColumn();
        this.dataColumnFråga8 = new System.Data.DataColumn();
        this.dataColumnFråga9 = new System.Data.DataColumn();
        this.dataColumnFråga10 = new System.Data.DataColumn();
        this.dataColumnFråga11 = new System.Data.DataColumn();
        this.dataColumnFråga12 = new System.Data.DataColumn();
        this.dataColumnFråga13 = new System.Data.DataColumn();
        this.dataColumnFråga14 = new System.Data.DataColumn();
        this.dataColumnFråga15 = new System.Data.DataColumn();
        this.dataColumnFråga16 = new System.Data.DataColumn();
        this.dataColumnFråga17 = new System.Data.DataColumn();
        this.dataColumnFråga18 = new System.Data.DataColumn();
        this.dataColumnFråga19 = new System.Data.DataColumn();
        this.dataColumnFråga20 = new System.Data.DataColumn();
        this.dataColumnFråga21 = new System.Data.DataColumn();
        this.dataColumnÖvrigt = new System.Data.DataColumn();
        this.dataColumnArtal = new System.Data.DataColumn();
        this.dataColumnFrågorId = new System.Data.DataColumn();
        this.dataColumnInvTypId = new System.Data.DataColumn();
        this.reportSammanställningGallring = new FastReport.Report();
        this.environmentSettingsSammanställningRapporter = new FastReport.EnvironmentSettings();
        this.dataColumnInvTypNamn = new System.Data.DataColumn();
        this.rapportTypDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.invtypIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.invtypNamnDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.regionIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.distriktIdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.traktnrDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.standortDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.entreprenorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.artalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.fraga1DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
        this.fraga2DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
        this.fraga3DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
        this.fraga4DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
        this.fraga5DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
        this.fraga6DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
        this.fraga7DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
        this.fraga8DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
        this.fraga9DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
        this.fraga10DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
        this.fraga11DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
        this.fraga12DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
        this.fraga13DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
        this.fraga14DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
        this.fraga15DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
        this.fraga16DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
        this.fraga17DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
        this.fraga18DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
        this.fraga19DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
        this.fraga20DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
        this.fraga21DataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewComboBoxColumn();
        this.ovrigtDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.groupBoxFrågor.SuspendLayout();
        this.groupBoxRapporttyp.SuspendLayout();
        this.groupBox1.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFrågor)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetFrågor)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFrågor)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.reportSammanställningGallring)).BeginInit();
        this.SuspendLayout();
        // 
        // groupBoxFrågor
        // 
        this.groupBoxFrågor.Controls.Add(this.buttonFrågor);
        this.groupBoxFrågor.Controls.Add(this.groupBoxRapporttyp);
        this.groupBoxFrågor.Controls.Add(this.labelFiltrering);
        this.groupBoxFrågor.Controls.Add(this.groupBox1);
        this.groupBoxFrågor.Controls.Add(this.dataGridViewFrågor);
        this.groupBoxFrågor.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.groupBoxFrågor.Location = new System.Drawing.Point(14, 8);
        this.groupBoxFrågor.Name = "groupBoxFrågor";
        this.groupBoxFrågor.Size = new System.Drawing.Size(890, 571);
        this.groupBoxFrågor.TabIndex = 0;
        this.groupBoxFrågor.TabStop = false;
        // 
        // buttonFrågor
        // 
        this.buttonFrågor.Enabled = false;
        this.buttonFrågor.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.buttonFrågor.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonFrågor.ImageIndex = 5;
        this.buttonFrågor.ImageList = this.imageList32;
        this.buttonFrågor.Location = new System.Drawing.Point(719, 437);
        this.buttonFrågor.Name = "buttonFrågor";
        this.buttonFrågor.Size = new System.Drawing.Size(160, 40);
        this.buttonFrågor.TabIndex = 3;
        this.buttonFrågor.Text = "Frågor";
        this.buttonFrågor.UseVisualStyleBackColor = true;
        this.buttonFrågor.Click += new System.EventHandler(this.buttonFrågor_Click);
        // 
        // imageList32
        // 
        this.imageList32.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList32.ImageStream")));
        this.imageList32.TransparentColor = System.Drawing.SystemColors.ActiveCaption;
        this.imageList32.Images.SetKeyName(0, "Uppdatera.ico");
        this.imageList32.Images.SetKeyName(1, "Lagra.ico");
        this.imageList32.Images.SetKeyName(2, "HoppaTill.ico");
        this.imageList32.Images.SetKeyName(3, "TabortData.ico");
        this.imageList32.Images.SetKeyName(4, "Filtrera.ico");
        this.imageList32.Images.SetKeyName(5, "Frågor.ico");
        this.imageList32.Images.SetKeyName(6, "Rapport.ico");
        // 
        // groupBoxRapporttyp
        // 
        this.groupBoxRapporttyp.Controls.Add(this.radioButtonSlutavverkning);
        this.groupBoxRapporttyp.Controls.Add(this.radioButtonBiobränsle);
        this.groupBoxRapporttyp.Controls.Add(this.radioButtonRöjning);
        this.groupBoxRapporttyp.Controls.Add(this.radioButtonGallring);
        this.groupBoxRapporttyp.Controls.Add(this.radioButtonPlantering);
        this.groupBoxRapporttyp.Controls.Add(this.radioButtonMarkberedning);
        this.groupBoxRapporttyp.Location = new System.Drawing.Point(0, -1);
        this.groupBoxRapporttyp.Name = "groupBoxRapporttyp";
        this.groupBoxRapporttyp.Size = new System.Drawing.Size(890, 49);
        this.groupBoxRapporttyp.TabIndex = 0;
        this.groupBoxRapporttyp.TabStop = false;
        this.groupBoxRapporttyp.Text = "Rapporttyp";
        // 
        // radioButtonSlutavverkning
        // 
        this.radioButtonSlutavverkning.AutoSize = true;
        this.radioButtonSlutavverkning.Location = new System.Drawing.Point(747, 21);
        this.radioButtonSlutavverkning.Name = "radioButtonSlutavverkning";
        this.radioButtonSlutavverkning.Size = new System.Drawing.Size(111, 17);
        this.radioButtonSlutavverkning.TabIndex = 6;
        this.radioButtonSlutavverkning.Text = "Slutavverkning";
        this.radioButtonSlutavverkning.UseVisualStyleBackColor = true;
        this.radioButtonSlutavverkning.CheckedChanged += new System.EventHandler(this.radioButtonSlutavverkning_CheckedChanged);
        // 
        // radioButtonBiobränsle
        // 
        this.radioButtonBiobränsle.AutoSize = true;
        this.radioButtonBiobränsle.Location = new System.Drawing.Point(594, 20);
        this.radioButtonBiobränsle.Name = "radioButtonBiobränsle";
        this.radioButtonBiobränsle.Size = new System.Drawing.Size(84, 17);
        this.radioButtonBiobränsle.TabIndex = 5;
        this.radioButtonBiobränsle.Text = "Biobränsle";
        this.radioButtonBiobränsle.UseVisualStyleBackColor = true;
        this.radioButtonBiobränsle.CheckedChanged += new System.EventHandler(this.radioButtonBiobränsle_CheckedChanged);
        // 
        // radioButtonRöjning
        // 
        this.radioButtonRöjning.AutoSize = true;
        this.radioButtonRöjning.Location = new System.Drawing.Point(330, 20);
        this.radioButtonRöjning.Name = "radioButtonRöjning";
        this.radioButtonRöjning.Size = new System.Drawing.Size(68, 17);
        this.radioButtonRöjning.TabIndex = 3;
        this.radioButtonRöjning.Text = "Röjning";
        this.radioButtonRöjning.UseVisualStyleBackColor = true;
        this.radioButtonRöjning.CheckedChanged += new System.EventHandler(this.radioButtonRöjning_CheckedChanged);
        // 
        // radioButtonGallring
        // 
        this.radioButtonGallring.AutoSize = true;
        this.radioButtonGallring.Location = new System.Drawing.Point(455, 21);
        this.radioButtonGallring.Name = "radioButtonGallring";
        this.radioButtonGallring.Size = new System.Drawing.Size(68, 17);
        this.radioButtonGallring.TabIndex = 4;
        this.radioButtonGallring.Text = "Gallring";
        this.radioButtonGallring.UseVisualStyleBackColor = true;
        this.radioButtonGallring.CheckedChanged += new System.EventHandler(this.radioButtonGallring_CheckedChanged);
        // 
        // radioButtonPlantering
        // 
        this.radioButtonPlantering.AutoSize = true;
        this.radioButtonPlantering.Location = new System.Drawing.Point(187, 20);
        this.radioButtonPlantering.Name = "radioButtonPlantering";
        this.radioButtonPlantering.Size = new System.Drawing.Size(83, 17);
        this.radioButtonPlantering.TabIndex = 2;
        this.radioButtonPlantering.Text = "Plantering";
        this.radioButtonPlantering.UseVisualStyleBackColor = true;
        this.radioButtonPlantering.CheckedChanged += new System.EventHandler(this.radioButtonPlantering_CheckedChanged);
        // 
        // radioButtonMarkberedning
        // 
        this.radioButtonMarkberedning.AutoSize = true;
        this.radioButtonMarkberedning.Location = new System.Drawing.Point(19, 20);
        this.radioButtonMarkberedning.Name = "radioButtonMarkberedning";
        this.radioButtonMarkberedning.Size = new System.Drawing.Size(111, 17);
        this.radioButtonMarkberedning.TabIndex = 1;
        this.radioButtonMarkberedning.Text = "Markberedning";
        this.radioButtonMarkberedning.UseVisualStyleBackColor = true;
        this.radioButtonMarkberedning.CheckedChanged += new System.EventHandler(this.radioButtonMarkberedning_CheckedChanged);
        // 
        // labelFiltrering
        // 
        this.labelFiltrering.Location = new System.Drawing.Point(10, 436);
        this.labelFiltrering.Name = "labelFiltrering";
        this.labelFiltrering.Size = new System.Drawing.Size(691, 65);
        this.labelFiltrering.TabIndex = 2;
        this.labelFiltrering.Text = "Filtrering: Ingen";
        // 
        // groupBox1
        // 
        this.groupBox1.Controls.Add(this.buttonTaBortData);
        this.groupBox1.Controls.Add(this.buttonFiltreraData);
        this.groupBox1.Controls.Add(this.buttonGåTillHanteraData);
        this.groupBox1.Controls.Add(this.buttonLagraÄndratData);
        this.groupBox1.Controls.Add(this.buttonUppdateraData);
        this.groupBox1.Location = new System.Drawing.Point(0, 502);
        this.groupBox1.Name = "groupBox1";
        this.groupBox1.Size = new System.Drawing.Size(890, 69);
        this.groupBox1.TabIndex = 4;
        this.groupBox1.TabStop = false;
        // 
        // buttonTaBortData
        // 
        this.buttonTaBortData.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.buttonTaBortData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonTaBortData.ImageIndex = 3;
        this.buttonTaBortData.ImageList = this.imageList32;
        this.buttonTaBortData.Location = new System.Drawing.Point(363, 17);
        this.buttonTaBortData.Name = "buttonTaBortData";
        this.buttonTaBortData.Size = new System.Drawing.Size(160, 40);
        this.buttonTaBortData.TabIndex = 2;
        this.buttonTaBortData.Text = "Ta bort";
        this.buttonTaBortData.UseVisualStyleBackColor = true;
        this.buttonTaBortData.Click += new System.EventHandler(this.buttonTaBortData_Click);
        // 
        // buttonFiltreraData
        // 
        this.buttonFiltreraData.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.buttonFiltreraData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonFiltreraData.ImageIndex = 4;
        this.buttonFiltreraData.ImageList = this.imageList32;
        this.buttonFiltreraData.Location = new System.Drawing.Point(541, 17);
        this.buttonFiltreraData.Name = "buttonFiltreraData";
        this.buttonFiltreraData.Size = new System.Drawing.Size(160, 40);
        this.buttonFiltreraData.TabIndex = 3;
        this.buttonFiltreraData.Text = "Filtrera";
        this.buttonFiltreraData.UseVisualStyleBackColor = true;
        this.buttonFiltreraData.Click += new System.EventHandler(this.buttonFiltreraData_Click);
        // 
        // buttonGåTillHanteraData
        // 
        this.buttonGåTillHanteraData.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.buttonGåTillHanteraData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonGåTillHanteraData.ImageIndex = 2;
        this.buttonGåTillHanteraData.ImageList = this.imageList32;
        this.buttonGåTillHanteraData.Location = new System.Drawing.Point(719, 17);
        this.buttonGåTillHanteraData.Name = "buttonGåTillHanteraData";
        this.buttonGåTillHanteraData.Size = new System.Drawing.Size(160, 40);
        this.buttonGåTillHanteraData.TabIndex = 4;
        this.buttonGåTillHanteraData.Text = "Hantera Data";
        this.buttonGåTillHanteraData.UseVisualStyleBackColor = true;
        this.buttonGåTillHanteraData.Click += new System.EventHandler(this.buttonGåTillRapportData_Click);
        // 
        // buttonLagraÄndratData
        // 
        this.buttonLagraÄndratData.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.buttonLagraÄndratData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonLagraÄndratData.ImageIndex = 1;
        this.buttonLagraÄndratData.ImageList = this.imageList32;
        this.buttonLagraÄndratData.Location = new System.Drawing.Point(187, 17);
        this.buttonLagraÄndratData.Name = "buttonLagraÄndratData";
        this.buttonLagraÄndratData.Size = new System.Drawing.Size(160, 40);
        this.buttonLagraÄndratData.TabIndex = 1;
        this.buttonLagraÄndratData.Text = "Lagra";
        this.buttonLagraÄndratData.UseVisualStyleBackColor = true;
        this.buttonLagraÄndratData.Click += new System.EventHandler(this.buttonLagraÄndratData_Click);
        // 
        // buttonUppdateraData
        // 
        this.buttonUppdateraData.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.buttonUppdateraData.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
        this.buttonUppdateraData.ImageIndex = 0;
        this.buttonUppdateraData.ImageList = this.imageList32;
        this.buttonUppdateraData.Location = new System.Drawing.Point(10, 17);
        this.buttonUppdateraData.Name = "buttonUppdateraData";
        this.buttonUppdateraData.Size = new System.Drawing.Size(160, 40);
        this.buttonUppdateraData.TabIndex = 0;
        this.buttonUppdateraData.Text = "Uppdatera";
        this.buttonUppdateraData.UseVisualStyleBackColor = true;
        this.buttonUppdateraData.Click += new System.EventHandler(this.buttonUppdateraData_Click);
        // 
        // dataGridViewFrågor
        // 
        this.dataGridViewFrågor.AllowUserToAddRows = false;
        this.dataGridViewFrågor.AllowUserToOrderColumns = true;
        this.dataGridViewFrågor.AllowUserToResizeRows = false;
        dataGridViewCellStyle1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.dataGridViewFrågor.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
        this.dataGridViewFrågor.AutoGenerateColumns = false;
        this.dataGridViewFrågor.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
        this.dataGridViewFrågor.BackgroundColor = System.Drawing.SystemColors.ControlDark;
        this.dataGridViewFrågor.BorderStyle = System.Windows.Forms.BorderStyle.None;
        this.dataGridViewFrågor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
        this.dataGridViewFrågor.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.rapportTypDataGridViewTextBoxColumn,
            this.invtypIdDataGridViewTextBoxColumn,
            this.invtypNamnDataGridViewTextBoxColumn,
            this.regionIdDataGridViewTextBoxColumn,
            this.distriktIdDataGridViewTextBoxColumn,
            this.traktnrDataGridViewTextBoxColumn,
            this.standortDataGridViewTextBoxColumn,
            this.entreprenorDataGridViewTextBoxColumn,
            this.artalDataGridViewTextBoxColumn,
            this.fraga1DataGridViewTextBoxColumn,
            this.fraga2DataGridViewTextBoxColumn,
            this.fraga3DataGridViewTextBoxColumn,
            this.fraga4DataGridViewTextBoxColumn,
            this.fraga5DataGridViewTextBoxColumn,
            this.fraga6DataGridViewTextBoxColumn,
            this.fraga7DataGridViewTextBoxColumn,
            this.fraga8DataGridViewTextBoxColumn,
            this.fraga9DataGridViewTextBoxColumn,
            this.fraga10DataGridViewTextBoxColumn,
            this.fraga11DataGridViewTextBoxColumn,
            this.fraga12DataGridViewTextBoxColumn,
            this.fraga13DataGridViewTextBoxColumn,
            this.fraga14DataGridViewTextBoxColumn,
            this.fraga15DataGridViewTextBoxColumn,
            this.fraga16DataGridViewTextBoxColumn,
            this.fraga17DataGridViewTextBoxColumn,
            this.fraga18DataGridViewTextBoxColumn,
            this.fraga19DataGridViewTextBoxColumn,
            this.fraga20DataGridViewTextBoxColumn,
            this.fraga21DataGridViewTextBoxColumn,
            this.ovrigtDataGridViewTextBoxColumn});
        this.dataGridViewFrågor.DataMember = "Frågor";
        this.dataGridViewFrågor.DataSource = this.dataSetFrågor;
        this.dataGridViewFrågor.Location = new System.Drawing.Point(0, 46);
        this.dataGridViewFrågor.MultiSelect = false;
        this.dataGridViewFrågor.Name = "dataGridViewFrågor";
        this.dataGridViewFrågor.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToDisplayedHeaders;
        dataGridViewCellStyle11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.dataGridViewFrågor.RowsDefaultCellStyle = dataGridViewCellStyle11;
        this.dataGridViewFrågor.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.dataGridViewFrågor.Size = new System.Drawing.Size(890, 383);
        this.dataGridViewFrågor.TabIndex = 1;
        this.dataGridViewFrågor.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dataGridViewFrågor_UserDeletingRow);
        this.dataGridViewFrågor.CellFormatting += new System.Windows.Forms.DataGridViewCellFormattingEventHandler(this.dataGridViewFrågor_CellFormatting);
        this.dataGridViewFrågor.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dataGridViewFrågor_CellValidating);
        this.dataGridViewFrågor.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewFrågor_DataError);
        // 
        // dataSetFrågor
        // 
        this.dataSetFrågor.DataSetName = "DataSetFrågor";
        this.dataSetFrågor.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableFrågor});
        // 
        // dataTableFrågor
        // 
        this.dataTableFrågor.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnRegionId,
            this.dataColumnDistriktId,
            this.dataColumnTraktnr,
            this.dataColumnStåndort,
            this.dataColumnEntreprenör,
            this.dataColumnRapportTyp,
            this.dataColumnFråga1,
            this.dataColumnFråga2,
            this.dataColumnFråga3,
            this.dataColumnFråga4,
            this.dataColumnFråga5,
            this.dataColumnFråga6,
            this.dataColumnFråga7,
            this.dataColumnFråga8,
            this.dataColumnFråga9,
            this.dataColumnFråga10,
            this.dataColumnFråga11,
            this.dataColumnFråga12,
            this.dataColumnFråga13,
            this.dataColumnFråga14,
            this.dataColumnFråga15,
            this.dataColumnFråga16,
            this.dataColumnFråga17,
            this.dataColumnFråga18,
            this.dataColumnFråga19,
            this.dataColumnFråga20,
            this.dataColumnFråga21,
            this.dataColumnÖvrigt,
            this.dataColumnArtal,
            this.dataColumnFrågorId,
            this.dataColumnInvTypId,
            this.dataColumnInvTypNamn});
        this.dataTableFrågor.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "FragorId"}, false)});
        this.dataTableFrågor.TableName = "Frågor";
        // 
        // dataColumnRegionId
        // 
        this.dataColumnRegionId.ColumnName = "RegionId";
        this.dataColumnRegionId.DataType = typeof(int);
        // 
        // dataColumnDistriktId
        // 
        this.dataColumnDistriktId.ColumnName = "DistriktId";
        this.dataColumnDistriktId.DataType = typeof(int);
        // 
        // dataColumnTraktnr
        // 
        this.dataColumnTraktnr.ColumnName = "Traktnr";
        this.dataColumnTraktnr.DataType = typeof(int);
        // 
        // dataColumnStåndort
        // 
        this.dataColumnStåndort.Caption = "Ståndort";
        this.dataColumnStåndort.ColumnName = "Standort";
        this.dataColumnStåndort.DataType = typeof(int);
        // 
        // dataColumnEntreprenör
        // 
        this.dataColumnEntreprenör.Caption = "Entreprenör";
        this.dataColumnEntreprenör.ColumnName = "Entreprenor";
        this.dataColumnEntreprenör.DataType = typeof(int);
        // 
        // dataColumnRapportTyp
        // 
        this.dataColumnRapportTyp.ColumnName = "RapportTyp";
        // 
        // dataColumnFråga1
        // 
        this.dataColumnFråga1.ColumnName = "Fraga1";
        // 
        // dataColumnFråga2
        // 
        this.dataColumnFråga2.ColumnName = "Fraga2";
        // 
        // dataColumnFråga3
        // 
        this.dataColumnFråga3.ColumnName = "Fraga3";
        // 
        // dataColumnFråga4
        // 
        this.dataColumnFråga4.ColumnName = "Fraga4";
        // 
        // dataColumnFråga5
        // 
        this.dataColumnFråga5.ColumnName = "Fraga5";
        // 
        // dataColumnFråga6
        // 
        this.dataColumnFråga6.ColumnName = "Fraga6";
        // 
        // dataColumnFråga7
        // 
        this.dataColumnFråga7.ColumnName = "Fraga7";
        // 
        // dataColumnFråga8
        // 
        this.dataColumnFråga8.ColumnName = "Fraga8";
        // 
        // dataColumnFråga9
        // 
        this.dataColumnFråga9.ColumnName = "Fraga9";
        // 
        // dataColumnFråga10
        // 
        this.dataColumnFråga10.ColumnName = "Fraga10";
        // 
        // dataColumnFråga11
        // 
        this.dataColumnFråga11.ColumnName = "Fraga11";
        // 
        // dataColumnFråga12
        // 
        this.dataColumnFråga12.ColumnName = "Fraga12";
        // 
        // dataColumnFråga13
        // 
        this.dataColumnFråga13.ColumnName = "Fraga13";
        // 
        // dataColumnFråga14
        // 
        this.dataColumnFråga14.ColumnName = "Fraga14";
        // 
        // dataColumnFråga15
        // 
        this.dataColumnFråga15.ColumnName = "Fraga15";
        // 
        // dataColumnFråga16
        // 
        this.dataColumnFråga16.ColumnName = "Fraga16";
        // 
        // dataColumnFråga17
        // 
        this.dataColumnFråga17.ColumnName = "Fraga17";
        // 
        // dataColumnFråga18
        // 
        this.dataColumnFråga18.ColumnName = "Fraga18";
        // 
        // dataColumnFråga19
        // 
        this.dataColumnFråga19.ColumnName = "Fraga19";
        // 
        // dataColumnFråga20
        // 
        this.dataColumnFråga20.ColumnName = "Fraga20";
        // 
        // dataColumnFråga21
        // 
        this.dataColumnFråga21.ColumnName = "Fraga21";
        // 
        // dataColumnÖvrigt
        // 
        this.dataColumnÖvrigt.ColumnName = "Ovrigt";
        // 
        // dataColumnArtal
        // 
        this.dataColumnArtal.ColumnName = "Artal";
        this.dataColumnArtal.DataType = typeof(int);
        // 
        // dataColumnFrågorId
        // 
        this.dataColumnFrågorId.ColumnName = "FragorId";
        this.dataColumnFrågorId.DataType = typeof(int);
        this.dataColumnFrågorId.ReadOnly = true;
        // 
        // dataColumnInvTypId
        // 
        this.dataColumnInvTypId.ColumnName = "InvTypId";
        this.dataColumnInvTypId.DataType = typeof(int);
        // 
        // reportSammanställningGallring
        // 
        this.reportSammanställningGallring.ReportResourceString = resources.GetString("reportSammanställningGallring.ReportResourceString");
        this.reportSammanställningGallring.RegisterData(this.dataSetFrågor, "dataSetFrågor");
        // 
        // environmentSettingsSammanställningRapporter
        // 
        designerSettings1.ApplicationConnection = null;
        designerSettings1.DefaultFont = new System.Drawing.Font("Arial", 10F);
        designerSettings1.Icon = ((System.Drawing.Icon)(resources.GetObject("designerSettings1.Icon")));
        designerSettings1.Restrictions = designerRestrictions1;
        designerSettings1.Text = "";
        this.environmentSettingsSammanställningRapporter.DesignerSettings = designerSettings1;
        emailSettings1.Address = "";
        emailSettings1.Host = "";
        emailSettings1.MessageTemplate = "";
        emailSettings1.Name = "";
        emailSettings1.Password = "";
        emailSettings1.Port = 49;
        emailSettings1.UserName = "";
        this.environmentSettingsSammanställningRapporter.EmailSettings = emailSettings1;
        previewSettings1.Buttons = ((FastReport.PreviewButtons)((((((FastReport.PreviewButtons.Print | FastReport.PreviewButtons.Save)
                    | FastReport.PreviewButtons.Find)
                    | FastReport.PreviewButtons.Zoom)
                    | FastReport.PreviewButtons.Navigator)
                    | FastReport.PreviewButtons.Close)));
        previewSettings1.Icon = ((System.Drawing.Icon)(resources.GetObject("previewSettings1.Icon")));
        previewSettings1.Text = "";
        this.environmentSettingsSammanställningRapporter.PreviewSettings = previewSettings1;
        this.environmentSettingsSammanställningRapporter.ReportSettings = reportSettings1;
        this.environmentSettingsSammanställningRapporter.UIStyle = FastReport.Utils.UIStyle.Office2007Black;
        // 
        // dataColumnInvTypNamn
        // 
        this.dataColumnInvTypNamn.ColumnName = "InvTypNamn";
        // 
        // rapportTypDataGridViewTextBoxColumn
        // 
        this.rapportTypDataGridViewTextBoxColumn.DataPropertyName = "RapportTyp";
        dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle2.BackColor = System.Drawing.Color.LemonChiffon;
        this.rapportTypDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle2;
        this.rapportTypDataGridViewTextBoxColumn.HeaderText = "Rapporttyp";
        this.rapportTypDataGridViewTextBoxColumn.Name = "rapportTypDataGridViewTextBoxColumn";
        this.rapportTypDataGridViewTextBoxColumn.ReadOnly = true;
        this.rapportTypDataGridViewTextBoxColumn.Width = 85;
        // 
        // invtypIdDataGridViewTextBoxColumn
        // 
        this.invtypIdDataGridViewTextBoxColumn.DataPropertyName = "InvTypId";
        dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle3.BackColor = System.Drawing.Color.LemonChiffon;
        this.invtypIdDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
        this.invtypIdDataGridViewTextBoxColumn.HeaderText = "InvTypId";
        this.invtypIdDataGridViewTextBoxColumn.Name = "invtypIdDataGridViewTextBoxColumn";
        this.invtypIdDataGridViewTextBoxColumn.ReadOnly = true;
        this.invtypIdDataGridViewTextBoxColumn.Width = 70;
        // 
        // invtypNamnDataGridViewTextBoxColumn
        // 
        this.invtypNamnDataGridViewTextBoxColumn.DataPropertyName = "InvTypNamn";
        dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle4.BackColor = System.Drawing.Color.LemonChiffon;
        this.invtypNamnDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
        this.invtypNamnDataGridViewTextBoxColumn.HeaderText = "InvTypNamn";
        this.invtypNamnDataGridViewTextBoxColumn.Name = "invtypNamnDataGridViewTextBoxColumn";
        this.invtypNamnDataGridViewTextBoxColumn.ReadOnly = true;
        this.invtypNamnDataGridViewTextBoxColumn.Width = 80;
        // 
        // regionIdDataGridViewTextBoxColumn
        // 
        this.regionIdDataGridViewTextBoxColumn.DataPropertyName = "RegionId";
        dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle5.BackColor = System.Drawing.Color.LemonChiffon;
        this.regionIdDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle5;
        this.regionIdDataGridViewTextBoxColumn.HeaderText = "Region id";
        this.regionIdDataGridViewTextBoxColumn.Name = "regionIdDataGridViewTextBoxColumn";
        this.regionIdDataGridViewTextBoxColumn.ReadOnly = true;
        this.regionIdDataGridViewTextBoxColumn.Width = 70;
        // 
        // distriktIdDataGridViewTextBoxColumn
        // 
        this.distriktIdDataGridViewTextBoxColumn.DataPropertyName = "DistriktId";
        dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
        dataGridViewCellStyle6.BackColor = System.Drawing.Color.LemonChiffon;
        this.distriktIdDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle6;
        this.distriktIdDataGridViewTextBoxColumn.HeaderText = "Distrikt id";
        this.distriktIdDataGridViewTextBoxColumn.Name = "distriktIdDataGridViewTextBoxColumn";
        this.distriktIdDataGridViewTextBoxColumn.ReadOnly = true;
        this.distriktIdDataGridViewTextBoxColumn.Width = 70;
        // 
        // traktnrDataGridViewTextBoxColumn
        // 
        this.traktnrDataGridViewTextBoxColumn.DataPropertyName = "Traktnr";
        dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle7.BackColor = System.Drawing.Color.LemonChiffon;
        dataGridViewCellStyle7.Format = "000000";
        dataGridViewCellStyle7.NullValue = null;
        this.traktnrDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle7;
        this.traktnrDataGridViewTextBoxColumn.HeaderText = "Traktnr";
        this.traktnrDataGridViewTextBoxColumn.Name = "traktnrDataGridViewTextBoxColumn";
        this.traktnrDataGridViewTextBoxColumn.ReadOnly = true;
        this.traktnrDataGridViewTextBoxColumn.Width = 65;
        // 
        // standortDataGridViewTextBoxColumn
        // 
        this.standortDataGridViewTextBoxColumn.DataPropertyName = "Standort";
        dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle8.BackColor = System.Drawing.Color.LemonChiffon;
        dataGridViewCellStyle8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.standortDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle8;
        this.standortDataGridViewTextBoxColumn.HeaderText = "Traktdel";
        this.standortDataGridViewTextBoxColumn.Name = "standortDataGridViewTextBoxColumn";
        this.standortDataGridViewTextBoxColumn.ReadOnly = true;
        // 
        // entreprenorDataGridViewTextBoxColumn
        // 
        this.entreprenorDataGridViewTextBoxColumn.DataPropertyName = "Entreprenor";
        dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle9.BackColor = System.Drawing.Color.LemonChiffon;
        dataGridViewCellStyle9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        dataGridViewCellStyle9.Format = "0000";
        dataGridViewCellStyle9.NullValue = null;
        this.entreprenorDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle9;
        this.entreprenorDataGridViewTextBoxColumn.HeaderText = "Entreprenörnr";
        this.entreprenorDataGridViewTextBoxColumn.Name = "entreprenorDataGridViewTextBoxColumn";
        this.entreprenorDataGridViewTextBoxColumn.ReadOnly = true;
        this.entreprenorDataGridViewTextBoxColumn.Width = 95;
        // 
        // artalDataGridViewTextBoxColumn
        // 
        this.artalDataGridViewTextBoxColumn.DataPropertyName = "Artal";
        dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle10.BackColor = System.Drawing.Color.LemonChiffon;
        this.artalDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle10;
        this.artalDataGridViewTextBoxColumn.HeaderText = "Årtal";
        this.artalDataGridViewTextBoxColumn.Name = "artalDataGridViewTextBoxColumn";
        this.artalDataGridViewTextBoxColumn.Width = 75;
        // 
        // fraga1DataGridViewTextBoxColumn
        // 
        this.fraga1DataGridViewTextBoxColumn.DataPropertyName = "Fraga1";
        this.fraga1DataGridViewTextBoxColumn.HeaderText = "Fråga1";
        this.fraga1DataGridViewTextBoxColumn.Name = "fraga1DataGridViewTextBoxColumn";
        this.fraga1DataGridViewTextBoxColumn.Width = 80;
        // 
        // fraga2DataGridViewTextBoxColumn
        // 
        this.fraga2DataGridViewTextBoxColumn.DataPropertyName = "Fraga2";
        this.fraga2DataGridViewTextBoxColumn.HeaderText = "Fråga2";
        this.fraga2DataGridViewTextBoxColumn.Name = "fraga2DataGridViewTextBoxColumn";
        this.fraga2DataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
        this.fraga2DataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
        this.fraga2DataGridViewTextBoxColumn.Width = 80;
        // 
        // fraga3DataGridViewTextBoxColumn
        // 
        this.fraga3DataGridViewTextBoxColumn.DataPropertyName = "Fraga3";
        this.fraga3DataGridViewTextBoxColumn.HeaderText = "Fråga3";
        this.fraga3DataGridViewTextBoxColumn.Name = "fraga3DataGridViewTextBoxColumn";
        this.fraga3DataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
        this.fraga3DataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
        this.fraga3DataGridViewTextBoxColumn.Width = 80;
        // 
        // fraga4DataGridViewTextBoxColumn
        // 
        this.fraga4DataGridViewTextBoxColumn.DataPropertyName = "Fraga4";
        this.fraga4DataGridViewTextBoxColumn.HeaderText = "Fråga4";
        this.fraga4DataGridViewTextBoxColumn.Name = "fraga4DataGridViewTextBoxColumn";
        this.fraga4DataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
        this.fraga4DataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
        this.fraga4DataGridViewTextBoxColumn.Width = 80;
        // 
        // fraga5DataGridViewTextBoxColumn
        // 
        this.fraga5DataGridViewTextBoxColumn.DataPropertyName = "Fraga5";
        this.fraga5DataGridViewTextBoxColumn.HeaderText = "Fråga5";
        this.fraga5DataGridViewTextBoxColumn.Name = "fraga5DataGridViewTextBoxColumn";
        this.fraga5DataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
        this.fraga5DataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
        this.fraga5DataGridViewTextBoxColumn.Width = 80;
        // 
        // fraga6DataGridViewTextBoxColumn
        // 
        this.fraga6DataGridViewTextBoxColumn.DataPropertyName = "Fraga6";
        this.fraga6DataGridViewTextBoxColumn.HeaderText = "Fråga6";
        this.fraga6DataGridViewTextBoxColumn.Name = "fraga6DataGridViewTextBoxColumn";
        this.fraga6DataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
        this.fraga6DataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
        this.fraga6DataGridViewTextBoxColumn.Width = 80;
        // 
        // fraga7DataGridViewTextBoxColumn
        // 
        this.fraga7DataGridViewTextBoxColumn.DataPropertyName = "Fraga7";
        this.fraga7DataGridViewTextBoxColumn.HeaderText = "Fråga7";
        this.fraga7DataGridViewTextBoxColumn.Name = "fraga7DataGridViewTextBoxColumn";
        this.fraga7DataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
        this.fraga7DataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
        this.fraga7DataGridViewTextBoxColumn.Width = 80;
        // 
        // fraga8DataGridViewTextBoxColumn
        // 
        this.fraga8DataGridViewTextBoxColumn.DataPropertyName = "Fraga8";
        this.fraga8DataGridViewTextBoxColumn.HeaderText = "Fråga8";
        this.fraga8DataGridViewTextBoxColumn.Name = "fraga8DataGridViewTextBoxColumn";
        this.fraga8DataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
        this.fraga8DataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
        this.fraga8DataGridViewTextBoxColumn.Width = 80;
        // 
        // fraga9DataGridViewTextBoxColumn
        // 
        this.fraga9DataGridViewTextBoxColumn.DataPropertyName = "Fraga9";
        this.fraga9DataGridViewTextBoxColumn.HeaderText = "Fråga9";
        this.fraga9DataGridViewTextBoxColumn.Name = "fraga9DataGridViewTextBoxColumn";
        this.fraga9DataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
        this.fraga9DataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
        this.fraga9DataGridViewTextBoxColumn.Width = 80;
        // 
        // fraga10DataGridViewTextBoxColumn
        // 
        this.fraga10DataGridViewTextBoxColumn.DataPropertyName = "Fraga10";
        this.fraga10DataGridViewTextBoxColumn.HeaderText = "Fråga10";
        this.fraga10DataGridViewTextBoxColumn.Name = "fraga10DataGridViewTextBoxColumn";
        this.fraga10DataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
        this.fraga10DataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
        this.fraga10DataGridViewTextBoxColumn.Width = 80;
        // 
        // fraga11DataGridViewTextBoxColumn
        // 
        this.fraga11DataGridViewTextBoxColumn.DataPropertyName = "Fraga11";
        this.fraga11DataGridViewTextBoxColumn.HeaderText = "Fråga11";
        this.fraga11DataGridViewTextBoxColumn.Name = "fraga11DataGridViewTextBoxColumn";
        this.fraga11DataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
        this.fraga11DataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
        this.fraga11DataGridViewTextBoxColumn.Width = 80;
        // 
        // fraga12DataGridViewTextBoxColumn
        // 
        this.fraga12DataGridViewTextBoxColumn.DataPropertyName = "Fraga12";
        this.fraga12DataGridViewTextBoxColumn.HeaderText = "Fråga12";
        this.fraga12DataGridViewTextBoxColumn.Name = "fraga12DataGridViewTextBoxColumn";
        this.fraga12DataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
        this.fraga12DataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
        this.fraga12DataGridViewTextBoxColumn.Width = 80;
        // 
        // fraga13DataGridViewTextBoxColumn
        // 
        this.fraga13DataGridViewTextBoxColumn.DataPropertyName = "Fraga13";
        this.fraga13DataGridViewTextBoxColumn.HeaderText = "Fråga13";
        this.fraga13DataGridViewTextBoxColumn.Name = "fraga13DataGridViewTextBoxColumn";
        this.fraga13DataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
        this.fraga13DataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
        this.fraga13DataGridViewTextBoxColumn.Width = 80;
        // 
        // fraga14DataGridViewTextBoxColumn
        // 
        this.fraga14DataGridViewTextBoxColumn.DataPropertyName = "Fraga14";
        this.fraga14DataGridViewTextBoxColumn.HeaderText = "Fråga14";
        this.fraga14DataGridViewTextBoxColumn.Name = "fraga14DataGridViewTextBoxColumn";
        this.fraga14DataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
        this.fraga14DataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
        this.fraga14DataGridViewTextBoxColumn.Width = 80;
        // 
        // fraga15DataGridViewTextBoxColumn
        // 
        this.fraga15DataGridViewTextBoxColumn.DataPropertyName = "Fraga15";
        this.fraga15DataGridViewTextBoxColumn.HeaderText = "Fråga15";
        this.fraga15DataGridViewTextBoxColumn.Name = "fraga15DataGridViewTextBoxColumn";
        this.fraga15DataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
        this.fraga15DataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
        this.fraga15DataGridViewTextBoxColumn.Width = 80;
        // 
        // fraga16DataGridViewTextBoxColumn
        // 
        this.fraga16DataGridViewTextBoxColumn.DataPropertyName = "Fraga16";
        this.fraga16DataGridViewTextBoxColumn.HeaderText = "Fråga16";
        this.fraga16DataGridViewTextBoxColumn.Name = "fraga16DataGridViewTextBoxColumn";
        this.fraga16DataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
        this.fraga16DataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
        this.fraga16DataGridViewTextBoxColumn.Width = 80;
        // 
        // fraga17DataGridViewTextBoxColumn
        // 
        this.fraga17DataGridViewTextBoxColumn.DataPropertyName = "Fraga17";
        this.fraga17DataGridViewTextBoxColumn.HeaderText = "Fråga17";
        this.fraga17DataGridViewTextBoxColumn.Name = "fraga17DataGridViewTextBoxColumn";
        this.fraga17DataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
        this.fraga17DataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
        this.fraga17DataGridViewTextBoxColumn.Width = 80;
        // 
        // fraga18DataGridViewTextBoxColumn
        // 
        this.fraga18DataGridViewTextBoxColumn.DataPropertyName = "Fraga18";
        this.fraga18DataGridViewTextBoxColumn.HeaderText = "Fråga18";
        this.fraga18DataGridViewTextBoxColumn.Name = "fraga18DataGridViewTextBoxColumn";
        this.fraga18DataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
        this.fraga18DataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
        this.fraga18DataGridViewTextBoxColumn.Width = 80;
        // 
        // fraga19DataGridViewTextBoxColumn
        // 
        this.fraga19DataGridViewTextBoxColumn.DataPropertyName = "Fraga19";
        this.fraga19DataGridViewTextBoxColumn.HeaderText = "Fråga19";
        this.fraga19DataGridViewTextBoxColumn.Name = "fraga19DataGridViewTextBoxColumn";
        this.fraga19DataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
        this.fraga19DataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
        this.fraga19DataGridViewTextBoxColumn.Width = 80;
        // 
        // fraga20DataGridViewTextBoxColumn
        // 
        this.fraga20DataGridViewTextBoxColumn.DataPropertyName = "Fraga20";
        this.fraga20DataGridViewTextBoxColumn.HeaderText = "Fråga20";
        this.fraga20DataGridViewTextBoxColumn.Name = "fraga20DataGridViewTextBoxColumn";
        this.fraga20DataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
        this.fraga20DataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
        this.fraga20DataGridViewTextBoxColumn.Width = 80;
        // 
        // fraga21DataGridViewTextBoxColumn
        // 
        this.fraga21DataGridViewTextBoxColumn.DataPropertyName = "Fraga21";
        this.fraga21DataGridViewTextBoxColumn.HeaderText = "Fråga21";
        this.fraga21DataGridViewTextBoxColumn.Name = "fraga21DataGridViewTextBoxColumn";
        this.fraga21DataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
        this.fraga21DataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
        this.fraga21DataGridViewTextBoxColumn.Width = 80;
        // 
        // ovrigtDataGridViewTextBoxColumn
        // 
        this.ovrigtDataGridViewTextBoxColumn.DataPropertyName = "Ovrigt";
        this.ovrigtDataGridViewTextBoxColumn.HeaderText = "Övrigt";
        this.ovrigtDataGridViewTextBoxColumn.Name = "ovrigtDataGridViewTextBoxColumn";
        this.ovrigtDataGridViewTextBoxColumn.Visible = false;
        this.ovrigtDataGridViewTextBoxColumn.Width = 200;
        // 
        // FormListaFrågor
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.Controls.Add(this.groupBoxFrågor);
        this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.Name = "FormListaFrågor";
        this.Size = new System.Drawing.Size(917, 591);
        this.Load += new System.EventHandler(this.FormListaFrågor_Load);
        this.groupBoxFrågor.ResumeLayout(false);
        this.groupBoxRapporttyp.ResumeLayout(false);
        this.groupBoxRapporttyp.PerformLayout();
        this.groupBox1.ResumeLayout(false);
        ((System.ComponentModel.ISupportInitialize)(this.dataGridViewFrågor)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetFrågor)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFrågor)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.reportSammanställningGallring)).EndInit();
        this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox groupBoxFrågor;
    private System.Windows.Forms.DataGridView dataGridViewFrågor;
    private System.Data.DataSet dataSetFrågor;
    private System.Data.DataTable dataTableFrågor;
    private System.Data.DataColumn dataColumnRegionId;
    private System.Data.DataColumn dataColumnDistriktId;
    private System.Data.DataColumn dataColumnTraktnr;
    private System.Data.DataColumn dataColumnStåndort;
    private System.Data.DataColumn dataColumnEntreprenör;
    private System.Data.DataColumn dataColumnRapportTyp;
    private System.Data.DataColumn dataColumnFråga1;
    private System.Data.DataColumn dataColumnFråga2;
    private System.Data.DataColumn dataColumnFråga3;
    private System.Data.DataColumn dataColumnFråga4;
    private System.Data.DataColumn dataColumnFråga5;
    private System.Data.DataColumn dataColumnFråga6;
    private System.Data.DataColumn dataColumnFråga7;
    private System.Data.DataColumn dataColumnFråga8;
    private System.Data.DataColumn dataColumnFråga9;
    private System.Windows.Forms.ImageList imageList32;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.Button buttonGåTillHanteraData;
    private System.Windows.Forms.Button buttonLagraÄndratData;
    private System.Windows.Forms.Button buttonUppdateraData;
    private System.Windows.Forms.Button buttonTaBortData;
    private System.Windows.Forms.Button buttonFiltreraData;
    private System.Windows.Forms.Label labelFiltrering;
    private System.Windows.Forms.GroupBox groupBoxRapporttyp;
    private System.Windows.Forms.RadioButton radioButtonSlutavverkning;
    private System.Windows.Forms.RadioButton radioButtonBiobränsle;
    private System.Windows.Forms.RadioButton radioButtonRöjning;
    private System.Windows.Forms.RadioButton radioButtonGallring;
    private System.Windows.Forms.RadioButton radioButtonPlantering;
    private System.Windows.Forms.RadioButton radioButtonMarkberedning;
    private System.Windows.Forms.Button buttonFrågor;
    private System.Data.DataColumn dataColumnFråga10;
    private System.Data.DataColumn dataColumnFråga11;
    private System.Data.DataColumn dataColumnFråga12;
    private System.Data.DataColumn dataColumnFråga13;
    private System.Data.DataColumn dataColumnFråga14;
    private System.Data.DataColumn dataColumnFråga15;
    private System.Data.DataColumn dataColumnFråga16;
    private System.Data.DataColumn dataColumnFråga17;
    private System.Data.DataColumn dataColumnFråga18;
    private System.Data.DataColumn dataColumnFråga19;
    private System.Data.DataColumn dataColumnFråga20;
    private System.Data.DataColumn dataColumnFråga21;
    private System.Data.DataColumn dataColumnÖvrigt;
    private System.Data.DataColumn dataColumnArtal;
    private System.Data.DataColumn dataColumnFrågorId;
    private FastReport.Report reportSammanställningGallring;
    private FastReport.EnvironmentSettings environmentSettingsSammanställningRapporter;
    private System.Data.DataColumn dataColumnInvTypId;
    private System.Windows.Forms.DataGridViewTextBoxColumn rapportTypDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn invtypIdDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn invtypNamnDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn regionIdDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn distriktIdDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn traktnrDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn standortDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn entreprenorDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn artalDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewComboBoxColumn fraga1DataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewComboBoxColumn fraga2DataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewComboBoxColumn fraga3DataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewComboBoxColumn fraga4DataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewComboBoxColumn fraga5DataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewComboBoxColumn fraga6DataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewComboBoxColumn fraga7DataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewComboBoxColumn fraga8DataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewComboBoxColumn fraga9DataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewComboBoxColumn fraga10DataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewComboBoxColumn fraga11DataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewComboBoxColumn fraga12DataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewComboBoxColumn fraga13DataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewComboBoxColumn fraga14DataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewComboBoxColumn fraga15DataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewComboBoxColumn fraga16DataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewComboBoxColumn fraga17DataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewComboBoxColumn fraga18DataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewComboBoxColumn fraga19DataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewComboBoxColumn fraga20DataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewComboBoxColumn fraga21DataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn ovrigtDataGridViewTextBoxColumn;
    private System.Data.DataColumn dataColumnInvTypNamn;

  }
}
