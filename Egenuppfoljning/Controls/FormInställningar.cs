﻿#region

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using Egenuppfoljning.Dialogs;
using Egenuppfoljning.Interfaces;
using Egenuppfoljning.Properties;

#endregion

namespace Egenuppfoljning.Controls
{
// ReSharper disable UnusedMember.Global
  public partial class FormInställningar : UserControl
// ReSharper restore UnusedMember.Global
  {
    private string mSSettingsFilePath;
    private string mSSettingsPath;

    public FormInställningar()
    {
      InitializeComponent();
      Init();
    }

    // ReSharper disable EventNeverSubscribedTo.Global
    public event DataEventHandler DataEventRensaDatabas;
    public event StatusEventHandler StatusEventRefresh;
    public event EntreprenörEventHandler DataEventLagraÄndradeEntreprenörer;
    // ReSharper restore EventNeverSubscribedTo.Global
// ReSharper disable MemberCanBePrivate.Global
    public void Init()
// ReSharper restore MemberCanBePrivate.Global
    {
      //mSSettingsPath = Application.StartupPath + Settings.Default.SettingsPath;
      //mSSettingsFilePath = Application.StartupPath + Settings.Default.SettingsPath + Settings.Default.SettingsFilename +
      //                     "." + Settings.Default.SettingsLanguage + ".xml";
        mSSettingsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + Settings.Default.SettingsPath;
        mSSettingsFilePath = mSSettingsPath + Settings.Default.SettingsFilename +
                             "." + Settings.Default.SettingsLanguage + Resources.Xml_suffix;
      if (Settings.Default.ReportFilePath.Equals(string.Empty))
      {
        Settings.Default.ReportFilePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                                          Settings.Default.ReportsPath;
      }

      if (Settings.Default.GodkandSelectedPath.Equals(string.Empty))
      {
        Settings.Default.GodkandSelectedPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                                               Settings.Default.GodkandPath;
        Settings.Default.FlyttaGodkandaRapporter = true;
      }

      if (Settings.Default.NekadSelectedPath.Equals(string.Empty))
      {
        Settings.Default.NekadSelectedPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                                             Settings.Default.NekadPath;
        Settings.Default.FlyttaNekadeRapporter = true;
      }

      if (Settings.Default.AterstallSelectedPath.Equals(string.Empty))
      {
        Settings.Default.AterstallSelectedPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                                                 Settings.Default.AterstallPath;
        Settings.Default.FlyttaAterstalldaRapporter = true;
      }

      if (Settings.Default.ReportBrowsePath.Equals(string.Empty))
      {
        Settings.Default.ReportBrowsePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                                            Settings.Default.ReportsPath;
      }

      SkapaKatalogOmDenInteFinns(Settings.Default.GodkandSelectedPath);
      SkapaKatalogOmDenInteFinns(Settings.Default.NekadSelectedPath);
      SkapaKatalogOmDenInteFinns(Settings.Default.AterstallSelectedPath);

      textBoxGodkändSökväg.Text = Settings.Default.GodkandSelectedPath;
      textBoxNekadSökväg.Text = Settings.Default.NekadSelectedPath;
      textBoxÅterställdaSökväg.Text = Settings.Default.AterstallSelectedPath;

      checkBoxFlyttaGodkändaRapporter.Checked = Settings.Default.FlyttaGodkandaRapporter;
      textBoxGodkändSökväg.Enabled = Settings.Default.FlyttaGodkandaRapporter;
      buttonBläddraGodkänd.Enabled = Settings.Default.FlyttaGodkandaRapporter;

      checkBoxFlyttaNekadeRapporter.Checked = Settings.Default.FlyttaNekadeRapporter;
      textBoxNekadSökväg.Enabled = Settings.Default.FlyttaNekadeRapporter;
      buttonBläddraNekad.Enabled = Settings.Default.FlyttaNekadeRapporter;

      checkBoxFlyttaÅterställdaRapporter.Checked = Settings.Default.FlyttaAterstalldaRapporter;
      textBoxÅterställdaSökväg.Enabled = Settings.Default.FlyttaAterstalldaRapporter;
      buttonBläddraÅterställda.Enabled = Settings.Default.FlyttaAterstalldaRapporter;
    }

    private static bool SkapaKatalogOmDenInteFinns(string aSelectedPath)
    {
      try
      {
        if (!Directory.Exists(aSelectedPath))
        {
          Directory.CreateDirectory(aSelectedPath);
        }
      }
      catch (Exception ex)
      {
#if !DEBUG
     MessageBox.Show("Sökvägen:"+aSelectedPath+" är felaktig, och kunde inte skapas. ", "Skapa katalog misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
        return false;
      }
      return true;
    }

    private bool VäljKatalog(string aSelectedPath, out string aNewSelectedPath)
    {
      if (!Directory.Exists(aSelectedPath))
      {
        //skapa en default katalog om den inte finns
        if (
          MessageBox.Show(Resources.Katalog_sokvagen + aSelectedPath + Resources.finns_inte__vill,
                          Resources.Den_angivna_katalogen_finns_inte, MessageBoxButtons.YesNo, MessageBoxIcon.Question) ==
          DialogResult.Yes)
        {
          if (!SkapaKatalogOmDenInteFinns(aSelectedPath))
          {
            aSelectedPath = "C:\\";
          }
        }
        else
        {
          aSelectedPath = "C:\\";
        }
      }

      folderBrowserDialog.ShowNewFolderButton = true;
      folderBrowserDialog.RootFolder = Environment.SpecialFolder.MyDocuments;
      folderBrowserDialog.SelectedPath = aSelectedPath;

      switch (folderBrowserDialog.ShowDialog())
      {
        case DialogResult.OK:
          aNewSelectedPath = folderBrowserDialog.SelectedPath;
          return true;
        default:
          aNewSelectedPath = null;
          return false;
      }
    }

    /// <summary>
    ///   Dialog visas där användaren kan välja var de godkända eller nekade rapporterna ska ligga.
    /// </summary>
    private void VäljKatalogFör(Rapport aStatus)
    {
      string newPath;
      switch (aStatus)
      {
        case Rapport.Godkänd:
          if (VäljKatalog(Settings.Default.GodkandSelectedPath, out newPath))
          {
            Settings.Default.GodkandSelectedPath = newPath;
            textBoxGodkändSökväg.Text = newPath;
          }
          break;
        case Rapport.Nekad:
          if (VäljKatalog(Settings.Default.NekadSelectedPath, out newPath))
          {
            Settings.Default.NekadSelectedPath = newPath;
            textBoxNekadSökväg.Text = newPath;
          }
          break;
        case Rapport.Obehandlad:
          if (VäljKatalog(Settings.Default.AterstallSelectedPath, out newPath))
          {
            Settings.Default.AterstallSelectedPath = newPath;
            textBoxÅterställdaSökväg.Text = newPath;
          }
          break;
      }

      Settings.Default.Save();
    }

// ReSharper disable UnusedMember.Global
    public void Close()
// ReSharper restore UnusedMember.Global
    {
      Settings.Default.Save();
      Hide();
    }

// ReSharper disable UnusedMember.Global
    public void UppdateraEntreprenörLista(IEnumerable<KorusDataEventArgs.Entreprenör> entreprenörer)
// ReSharper restore UnusedMember.Global
    {
      try
      {
        dataSetEntreprenörer.Clear();

        //ytor
        foreach (var entreprenör in entreprenörer)
        {
          dataSetEntreprenörer.Tables["Entreprenörer"].Rows.Add(entreprenör.id, entreprenör.namn, entreprenör.regionid);
        }
      }
      catch (Exception ex)
      {
#if !DEBUG
     MessageBox.Show("Misslyckades med att läsa in entreprenörer", "Läsa in entreprenörer misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }


    private static int GetObjIntValue(object aObjValue)
    {
      var intValue = -1;
      if (aObjValue != null && !aObjValue.ToString().Equals(string.Empty))
      {
        if (!(int.TryParse(aObjValue.ToString(), out intValue)))
        {
          MessageBox.Show(
            Resources.Vardet + aObjValue + Resources.ar_ett_felaktigt_nyckelvarde,
            Resources.Felaktigt_nyckelvarde, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
      }
      return intValue;
    }

    private List<KorusDataEventArgs.Entreprenör> GetAllKORUSEntreprenörData()
    {
      var entrepröner = new List<KorusDataEventArgs.Entreprenör>();
      if (dataGridViewEntreprenörer.Rows.Count > 0)
      {
        entrepröner.AddRange(from DataGridViewRow row in dataGridViewEntreprenörer.Rows
                             select new KorusDataEventArgs.Entreprenör
                               {
                                 id =
                                   GetObjIntValue(row.Cells[entreprenorIdDataGridViewTextBoxColumn.Name].Value),
                                 regionid =
                                   GetObjIntValue(row.Cells[entreprenorRegionIdGridViewTextBoxColumn.Name].Value),
                                 namn = row.Cells[entreprenorNamnDataGridViewTextBoxColumn.Name].Value.ToString()
                               });
      }
      return entrepröner;
    }

    /// <summary>
    ///   Initializes the settings data tables.
    /// </summary>
    private void InitSettingsData()
    {
      if (dataSetSettings.Tables["Region"].Rows.Count == 0)
      {
        dataSetSettings.Tables["Region"].Rows.Add(0, string.Empty);
      }

      if (dataSetSettings.Tables["Distrikt"].Rows.Count == 0)
      {
        dataSetSettings.Tables["Distrikt"].Rows.Add(0, 0, string.Empty);
      }

      if (dataSetSettings.Tables["Standort"].Rows.Count == 0)
      {
        dataSetSettings.Tables["Standort"].Rows.Add(0);
      }

      if (dataSetSettings.Tables["Markberedningsmetod"].Rows.Count == 0)
      {
        dataSetSettings.Tables["Markberedningsmetod"].Rows.Add(string.Empty);
      }

      if (dataSetSettings.Tables["Ursprung"].Rows.Count == 0)
      {
        dataSetSettings.Tables["Ursprung"].Rows.Add(string.Empty);
      }
    }

    /// <summary>
    ///   Sets a hardcoded default settingsdata.
    /// </summary>
    private void DefaultSettingsData()
    {
        dataSetSettings.Tables["Region"].Rows.Add("11", "11 - Nord");
        dataSetSettings.Tables["Region"].Rows.Add("16", "16 - Mitt");
        dataSetSettings.Tables["Region"].Rows.Add("18", "18 - Syd");

        dataSetSettings.Tables["Distrikt"].Rows.Add("11", "14", "14 - Örnsköldsvik");
        dataSetSettings.Tables["Distrikt"].Rows.Add("11", "19", "19 - Umeå");
        dataSetSettings.Tables["Distrikt"].Rows.Add("16", "12", "12 - Sveg");
        dataSetSettings.Tables["Distrikt"].Rows.Add("16", "14", "14 - Ljusdal");
        dataSetSettings.Tables["Distrikt"].Rows.Add("16", "15", "15 - Delsbo");
        dataSetSettings.Tables["Distrikt"].Rows.Add("16", "16", "16 - Hudiksvall");
        dataSetSettings.Tables["Distrikt"].Rows.Add("16", "18", "18 - Bollnäs");
        dataSetSettings.Tables["Distrikt"].Rows.Add("16", "21", "21 - Uppland");
        dataSetSettings.Tables["Distrikt"].Rows.Add("18", "10", "10 - Västerås");
        dataSetSettings.Tables["Distrikt"].Rows.Add("18", "11", "11 - Örebro");
        dataSetSettings.Tables["Distrikt"].Rows.Add("18", "12", "12 - Nyköping");
        dataSetSettings.Tables["Distrikt"].Rows.Add("18", "13", "13 - Götaland");
        dataSetSettings.Tables["Distrikt"].Rows.Add("18", "21", "21 - Egen Skog");

      /*dataSetSettings.Tables["Standort"].Rows.Add("1");
      dataSetSettings.Tables["Standort"].Rows.Add("2");
      dataSetSettings.Tables["Standort"].Rows.Add("3");
      dataSetSettings.Tables["Standort"].Rows.Add("4");
      dataSetSettings.Tables["Standort"].Rows.Add("5");
      dataSetSettings.Tables["Standort"].Rows.Add("6");*/

      dataSetSettings.Tables["Markberedningsmetod"].Rows.Add("Kontinuerligt harv");
      dataSetSettings.Tables["Markberedningsmetod"].Rows.Add("Högläggning");
      dataSetSettings.Tables["Markberedningsmetod"].Rows.Add("För sådd");

      dataSetSettings.Tables["Ursprung"].Rows.Add("Eget");
      dataSetSettings.Tables["Ursprung"].Rows.Add("Köp");
    }

    /// <summary>
    ///   Checks so the application got the required files and directories otherwise create them.
    /// </summary>
    private void InitApplicationDirFilesStructure()
    {
      if (!Directory.Exists(mSSettingsPath))
      {
        //Settings dir does not exists, create it.
        Directory.CreateDirectory(mSSettingsPath);
      }

      if (!File.Exists(mSSettingsFilePath))
      {
        //Settings xml file does not exist, create a default file.
        DefaultSettingsData();
        WriteSettingsXmlFile(false);
      }
      if (!File.Exists(mSSettingsPath + Settings.Default.SettingsSchemaFilename))
      {
        //The schema settings file is missing, create it.
        WriteSettingsXmlFile(true);
      }
    }

    /// <summary>
    ///   Read the XML file containing the settings for the Application.
    /// </summary>
    private void ReadSettingsXmlFile()
    {
      InitSettingsData();

      var objValidator = new XmlValidator(mSSettingsFilePath,
                                          mSSettingsPath + Settings.Default.SettingsSchemaFilename);

      if (objValidator.ValidateXmlFileFailed())
      {
        //something is wrong with the Settings file then write a new Settings and Schema file.
        WriteSettingsXmlFile(false);
        WriteSettingsXmlFile(true);
      }

      try
      {
        dataSetSettings.Clear();
        dataSetSettings.ReadXml(mSSettingsFilePath);
      }
      catch (Exception ex)
      {
#if !DEBUG
       MessageBox.Show("Läsa Settings data misslyckades", "Läsa misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    private void WriteSettingsXmlFile(bool aWriteSchema)
    {
      XmlTextWriter objXmlSettingsWriter = null;
      XmlTextWriter objXmlSettingsSchemaWriter = null;
      InitSettingsData();

      try
      {
        if (aWriteSchema)
        {
          objXmlSettingsSchemaWriter = new XmlTextWriter(mSSettingsPath + Settings.Default.SettingsSchemaFilename, null)
            {Formatting = Formatting.Indented};
          objXmlSettingsSchemaWriter.WriteStartDocument();
          dataSetSettings.WriteXmlSchema(objXmlSettingsSchemaWriter);
          objXmlSettingsSchemaWriter.WriteEndDocument();
          objXmlSettingsSchemaWriter.Flush();
        }
        else
        {
          objXmlSettingsWriter = new XmlTextWriter(mSSettingsFilePath, null) {Formatting = Formatting.Indented};
          objXmlSettingsWriter.WriteStartDocument();
          dataSetSettings.WriteXml(objXmlSettingsWriter);
          objXmlSettingsWriter.WriteEndDocument();
          objXmlSettingsWriter.Flush();
        }
      }
      catch (Exception ex)
      {
#if !DEBUG
       MessageBox.Show("Spara Settings data misslyckades", "Spara misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
      finally
      {
        if (objXmlSettingsWriter != null)
        {
          objXmlSettingsWriter.Close();
        }
        if (objXmlSettingsSchemaWriter != null)
        {
          objXmlSettingsSchemaWriter.Close();
        }
      }
    }


    private void buttonEpost_Click(object sender, EventArgs e)
    {
      var mailSettings = new MailSettings();
      mailSettings.ShowDialog(this);
    }

    private void buttonBläddraGodkänd_Click(object sender, EventArgs e)
    {
      VäljKatalogFör(Rapport.Godkänd);
    }

    private void buttonBläddraNekad_Click(object sender, EventArgs e)
    {
      VäljKatalogFör(Rapport.Nekad);
    }

    private void buttonBläddraÅterställda_Click(object sender, EventArgs e)
    {
      VäljKatalogFör(Rapport.Obehandlad);
    }

    private void checkBoxFlyttaGodkändaRapporter_CheckedChanged(object sender, EventArgs e)
    {
      var checkBoxGodkänd = sender as CheckBox;
      if (checkBoxGodkänd == null) return;
      textBoxGodkändSökväg.Enabled = checkBoxGodkänd.Checked;
      buttonBläddraGodkänd.Enabled = checkBoxGodkänd.Checked;
      Settings.Default.FlyttaGodkandaRapporter = checkBoxGodkänd.Checked;
    }

    private void checkBoxFlyttaNekadeRapporter_CheckedChanged(object sender, EventArgs e)
    {
      var checkBoxNekad = sender as CheckBox;
      if (checkBoxNekad == null) return;
      textBoxNekadSökväg.Enabled = checkBoxNekad.Checked;
      buttonBläddraNekad.Enabled = checkBoxNekad.Checked;
      Settings.Default.FlyttaNekadeRapporter = checkBoxNekad.Checked;
    }

    private void checkBoxFlyttaÅterställdaRapporter_CheckedChanged(object sender, EventArgs e)
    {
      var checkBoxObehandlad = sender as CheckBox;
      if (checkBoxObehandlad == null) return;
      textBoxÅterställdaSökväg.Enabled = checkBoxObehandlad.Checked;
      buttonBläddraÅterställda.Enabled = checkBoxObehandlad.Checked;
      Settings.Default.FlyttaAterstalldaRapporter = checkBoxObehandlad.Checked;
    }

    private void textBoxGodkändSökväg_TextChanged(object sender, EventArgs e)
    {
      var godkändTextBox = sender as TextBox;
      if (godkändTextBox != null)
      {
        Settings.Default.GodkandSelectedPath = godkändTextBox.Text;
      }
    }

    private void textBoxNekadSökväg_TextChanged(object sender, EventArgs e)
    {
      var nekadTextBox = sender as TextBox;
      if (nekadTextBox != null)
      {
        Settings.Default.NekadSelectedPath = nekadTextBox.Text;
      }
    } 

    private void textBoxÅterställdaSökväg_TextChanged(object sender, EventArgs e)
    {
      var återställTextBox = sender as TextBox;
      if (återställTextBox != null)
      {
        Settings.Default.AterstallSelectedPath = återställTextBox.Text;
      }
    }

    private void buttonLagraÄndratData_Click(object sender, EventArgs e)
    {
      SkapaKatalogOmDenInteFinns(Settings.Default.GodkandSelectedPath);
      SkapaKatalogOmDenInteFinns(Settings.Default.NekadSelectedPath);
      SkapaKatalogOmDenInteFinns(Settings.Default.AterstallSelectedPath);
      DataEventLagraÄndradeEntreprenörer(this, GetAllKORUSEntreprenörData());
    }

    private void buttonUppdateraData_Click(object sender, EventArgs e)
    {
      StatusEventRefresh(this, null);
    }

    private void FormInställningar_Load(object sender, EventArgs e)
    {
      InitApplicationDirFilesStructure();
      ReadSettingsXmlFile();
    }
  }
}