﻿#region

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Windows.Forms;
using Egenuppfoljning.Dialogs;
using Egenuppfoljning.Interfaces;
using Egenuppfoljning.Properties;

#endregion

namespace Egenuppfoljning.Controls
{
    // ReSharper disable UnusedMember.Global
    public partial class FormListaYtor : UserControl
    // ReSharper restore UnusedMember.Global
    {
        private List<KorusDataEventArgs.Yta> mYtor;

        public FormListaYtor()
        {
            InitializeComponent();
            mYtor = new List<KorusDataEventArgs.Yta>();
        }

        // ReSharper disable EventNeverSubscribedTo.Global
        public event StatusEventHandler StatusEventRefresh;
        public event DataEventHandler DataEventGåTillRapport;
        public event DataEventHandler DataEventTaBortYta;
        public event YtorEventHandler DataEventLagraÄndradeYtor;
        // ReSharper restore EventNeverSubscribedTo.Global

        // ReSharper disable UnusedMember.Global
        public void Close()
        // ReSharper restore UnusedMember.Global
        {
            Settings.Default.Save();
            Hide();
        }

        private static bool Filter(KorusDataEventArgs.Yta aYta)
        {
            if (Settings.Default.FiltreraYtaInvTypId >= 0 && (aYta.invtyp != Settings.Default.FiltreraYtaInvTypId))
            {
                return false;
            }
            if (Settings.Default.FiltreraYtaRegionId > 0 && (aYta.regionid != Settings.Default.FiltreraYtaRegionId))
            {
                return false;
            }
            if (Settings.Default.FiltreraYtaDistriktId > 0 && (aYta.distriktid != Settings.Default.FiltreraYtaDistriktId))
            {
                return false;
            }
            if (Settings.Default.FiltreraYtaTraktNr > 0 && (aYta.traktnr != Settings.Default.FiltreraYtaTraktNr))
            {
                return false;
            }
            if (Settings.Default.FiltreraYtaStandort > 0 && (aYta.standort != Settings.Default.FiltreraYtaStandort))
            {
                return false;
            }
            if (Settings.Default.FiltreraYtaEntreprenor > 0 && (aYta.entreprenor != Settings.Default.FiltreraYtaEntreprenor))
            {
                return false;
            }
            if (!Settings.Default.FiltreraYtaRapportTypNamn.Equals(string.Empty) &&
                !(DataHelper.GetRapportTyp(aYta.rapport_typ).Equals(Settings.Default.FiltreraYtaRapportTypNamn)))
            {
                return false;
            }
            if (Settings.Default.FiltreraYtaArtal > 0 && (aYta.årtal != Settings.Default.FiltreraYtaArtal))
            {
                return false;
            }

            if (Settings.Default.FiltreraYta > 0 && (aYta.yta != Settings.Default.FiltreraYta))
            {
                return false;
            }
            if (Settings.Default.FiltreraYtaOptimalt > 0 && (aYta.optimalt != Settings.Default.FiltreraYtaOptimalt))
            {
                return false;
            }
            if (Settings.Default.FiltreraYtaBra > 0 && (aYta.bra != Settings.Default.FiltreraYtaBra))
            {
                return false;
            }
            if (Settings.Default.FiltreraYtaVaravBattre > 0 && (aYta.varavbattre != Settings.Default.FiltreraYtaVaravBattre))
            {
                return false;
            }
            if (Settings.Default.FiltreraYtaBlekjordsflack > 0 &&
                (aYta.blekjordsflack != Settings.Default.FiltreraYtaBlekjordsflack))
            {
                return false;
            }
            if (Settings.Default.FiltreraYtaOptimaltBra > 0 && (aYta.optimaltbra != Settings.Default.FiltreraYtaOptimaltBra))
            {
                return false;
            }
            if (Settings.Default.FiltreraYtaOvrigt > 0 && (aYta.ovrigt != Settings.Default.FiltreraYtaOvrigt))
            {
                return false;
            }
            if (Settings.Default.FiltreraYtaPlanterade > 0 && (aYta.planterade != Settings.Default.FiltreraYtaPlanterade))
            {
                return false;
            }
            if (Settings.Default.FiltreraYtaPlanteringsdjupfel > 0 &&
                (aYta.planterade != Settings.Default.FiltreraYtaPlanteringsdjupfel))
            {
                return false;
            }
            if (Settings.Default.FiltreraYtaTilltryckning > 0 &&
                (aYta.tilltryckning != Settings.Default.FiltreraYtaTilltryckning))
            {
                return false;
            }
            if (Settings.Default.FiltreraYtaStickvbredd > 0 &&
                (Math.Abs(aYta.stickvbredd - Settings.Default.FiltreraYtaStickvbredd) > 0.1))
            {
                return false;
            }
            if (Settings.Default.FiltreraYtaTall > 0 && (aYta.tall != Settings.Default.FiltreraYtaTall))
            {
                return false;
            }
            if (Settings.Default.FiltreraYtaTallMedelhojd > 0 &&
                (Math.Abs(aYta.tallmedelhojd - Settings.Default.FiltreraYtaTallMedelhojd) > 0.1))
            {
                return false;
            }
            if (Settings.Default.FiltreraYtaGran > 0 && (aYta.gran != Settings.Default.FiltreraYtaGran))
            {
                return false;
            }
            if (Settings.Default.FiltreraYtaGranMedelhojd > 0 &&
                (Math.Abs(aYta.granmedelhojd - Settings.Default.FiltreraYtaGranMedelhojd) > 0.1))
            {
                return false;
            }
            if (Settings.Default.FiltreraYtaLov > 0 && (aYta.lov != Settings.Default.FiltreraYtaLov))
            {
                return false;
            }
            if (Settings.Default.FiltreraYtaLovMedelhojd > 0 &&
                (Math.Abs(aYta.lovmedelhojd - Settings.Default.FiltreraYtaLovMedelhojd) > 0.1))
            {
                return false;
            }
            if (Settings.Default.FiltreraYtaContorta > 0 && (aYta.contorta != Settings.Default.FiltreraYtaContorta))
            {
                return false;
            }
            if (Settings.Default.FiltreraYtaContortaMedelhojd > 0 &&
                (Math.Abs(aYta.contortamedelhojd - Settings.Default.FiltreraYtaContortaMedelhojd) > 0.1))
            {
                return false;
            }
            if (Settings.Default.FiltreraYtaHst > 0 && (aYta.hst != Settings.Default.FiltreraYtaHst))
            {
                return false;
            }
            if (Settings.Default.FiltreraYtaMHojd > 0 &&
                (Math.Abs(aYta.mhojd - Settings.Default.FiltreraYtaMHojd) > 0.1))
            {
                return false;
            }
            if (Settings.Default.FiltreraYtaSumma > 0 && (aYta.summa != Settings.Default.FiltreraYtaSumma))
            {
                return false;
            }
            return Settings.Default.FiltreraYtaSkador <= 0 || (aYta.skador == Settings.Default.FiltreraYtaSkador);
        }

        private void UppdateraFilterText()
        {
            var filterText = "Filter: ";
            if (Settings.Default.FiltreraYtaInvTypId >= 0)
            {
                filterText += " InvTypId: " + Settings.Default.FiltreraYtaInvTypNamn + ",";
            }

            if (Settings.Default.FiltreraYtaRegionId > 0)
            {
                filterText += " RegionId: " + Settings.Default.FiltreraYtaRegionId.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraYtaDistriktId > 0)
            {
                filterText += " DistriktId: " + Settings.Default.FiltreraYtaDistriktId.ToString(CultureInfo.InvariantCulture) +
                              ",";
            }
            if (Settings.Default.FiltreraYtaTraktNr > 0)
            {
                filterText += " TraktNr: " + Settings.Default.FiltreraYtaTraktNr.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraYtaStandort > 0)
            {
                filterText += " Traktdel: " + Settings.Default.FiltreraYtaStandort.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraYtaEntreprenor > 0)
            {
                filterText += " Entreprenör: " + Settings.Default.FiltreraYtaEntreprenor.ToString(CultureInfo.InvariantCulture) +
                              ",";
            }
            if (!Settings.Default.FiltreraYtaRapportTypNamn.Equals(string.Empty))
            {
                filterText += " RapportTyp: " + Settings.Default.FiltreraYtaRapportTypNamn + "," + ",";
            }
            if (Settings.Default.FiltreraYtaArtal > 0)
            {
                filterText += " Artal: " + Settings.Default.FiltreraYtaArtal.ToString(CultureInfo.InvariantCulture) + ",";
            }

            if (Settings.Default.FiltreraYta > 0)
            {
                filterText += " Yta: " + Settings.Default.FiltreraYta.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraYtaOptimalt > 0)
            {
                filterText += " Optimalt: " + Settings.Default.FiltreraYtaOptimalt.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraYtaBra > 0)
            {
                filterText += " Bra: " + Settings.Default.FiltreraYtaBra.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraYtaVaravBattre > 0)
            {
                filterText += " VaravBättre: " + Settings.Default.FiltreraYtaVaravBattre.ToString(CultureInfo.InvariantCulture) +
                              ",";
            }
            if (Settings.Default.FiltreraYtaBlekjordsflack > 0)
            {
                filterText += " Blekjordsfläck: " +
                              Settings.Default.FiltreraYtaBlekjordsflack.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraYtaOptimaltBra > 0)
            {
                filterText += " OptimaltBra: " + Settings.Default.FiltreraYtaOptimaltBra.ToString(CultureInfo.InvariantCulture) +
                              ",";
            }
            if (Settings.Default.FiltreraYtaOvrigt > 0)
            {
                filterText += " Övrigt: " + Settings.Default.FiltreraYtaOvrigt.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraYtaPlanterade > 0)
            {
                filterText += " Planterade: " + Settings.Default.FiltreraYtaPlanterade.ToString(CultureInfo.InvariantCulture) +
                              ",";
            }
            if (Settings.Default.FiltreraYtaPlanteringsdjupfel > 0)
            {
                filterText += " Planteringsdjupfel: " +
                              Settings.Default.FiltreraYtaPlanteringsdjupfel.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraYtaTilltryckning > 0)
            {
                filterText += " Tilltryckning: " +
                              Settings.Default.FiltreraYtaTilltryckning.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraYtaTall > 0)
            {
                filterText += " Tall: " + Settings.Default.FiltreraYtaTall.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraYtaTallMedelhojd > 0)
            {
                filterText += " TallMedelhöjd: " +
                              Settings.Default.FiltreraYtaTallMedelhojd.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraYtaGran > 0)
            {
                filterText += " Gran: " + Settings.Default.FiltreraYtaGran.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraYtaGranMedelhojd > 0)
            {
                filterText += " GranMedelhöjd: " +
                              Settings.Default.FiltreraYtaGranMedelhojd.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraYtaLov > 0)
            {
                filterText += " Löv: " + Settings.Default.FiltreraYtaLov.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraYtaLovMedelhojd > 0)
            {
                filterText += " LövMedelhöjd: " +
                              Settings.Default.FiltreraYtaLovMedelhojd.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraYtaContorta > 0)
            {
                filterText += " Contorta: " + Settings.Default.FiltreraYtaContorta.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraYtaContortaMedelhojd > 0)
            {
                filterText += " ContortaMedelhöjd: " +
                              Settings.Default.FiltreraYtaContortaMedelhojd.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraYtaHst > 0)
            {
                filterText += " Hst: " + Settings.Default.FiltreraYtaHst.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraYtaMHojd > 0)
            {
                filterText += " M-Höjd: " + Settings.Default.FiltreraYtaMHojd.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraYtaSumma > 0)
            {
                filterText += " Summa: " + Settings.Default.FiltreraYtaSumma.ToString(CultureInfo.InvariantCulture) + ",";
            }
            if (Settings.Default.FiltreraYtaSkador > 0)
            {
                filterText += " Skador: " + Settings.Default.FiltreraYtaSkador.ToString(CultureInfo.InvariantCulture) + ",";
            }

            if (filterText.Equals("Filter: "))
            {
                labelFiltrering.Text = filterText + Resources.Inget;
            }
            else
            {
                labelFiltrering.Text = filterText.Substring(0, filterText.Length - 1);
            }
        }

        private void UppdateraDataSet()
        {
            try
            {
                UppdateraFilterText();
                dataSetYtor.Clear();

                //ytor
                foreach (var yta in mYtor.Where(Filter))
                {
                    dataSetYtor.Tables["Ytor"].Rows.Add(yta.regionid, yta.distriktid, yta.traktnr, yta.standort, yta.entreprenor,
                                                        DataHelper.GetRapportTyp(yta.rapport_typ), yta.yta, yta.optimalt, yta.bra,
                                                        yta.varavbattre, yta.blekjordsflack, yta.optimaltbra, yta.ovrigt,
                                                        yta.planterade, yta.planteringsdjupfel, yta.tilltryckning, yta.tall,
                                                        yta.tallmedelhojd, yta.gran, yta.granmedelhojd, yta.lov,
                                                        yta.lovmedelhojd, yta.contorta, yta.contortamedelhojd, yta.hst,
                                                        yta.mhojd, yta.summa, yta.skador, yta.årtal, yta.stickvbredd, yta.invtyp, yta.rojstam,DataSetParser.getInvNamn(yta.invtyp));
                }
            }
            catch (Exception ex)
            {
#if !DEBUG
     MessageBox.Show("Misslyckades med att läsa in data", "Läsa data misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        // ReSharper disable UnusedMember.Global
        public void UppdateraYtaLista(List<KorusDataEventArgs.Yta> ytor)
        // ReSharper restore UnusedMember.Global
        {
            mYtor = ytor;
            UppdateraDataSet();
        }

        private static float GetObjFloatValue(object aObjValue)
        {
            var floatValue = -1.0f;
            if (aObjValue != null && !aObjValue.ToString().Equals(string.Empty))
            {
                if (!(float.TryParse(aObjValue.ToString(), out floatValue)))
                {
                    MessageBox.Show(Resources.Vardet + aObjValue + Resources.ar_ett_felaktigt, Resources.Felaktigt_nyckelvarde,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                }
            }
            return floatValue;
        }

        private static int GetObjIntValue(object aObjValue)
        {
            var intValue = -1;
            if (aObjValue != null && !aObjValue.ToString().Equals(string.Empty))
            {
                if (!(int.TryParse(aObjValue.ToString(), out intValue)))
                {
                    MessageBox.Show(Resources.Vardet + aObjValue + Resources.ar_ett_felaktigt, Resources.Felaktigt_nyckelvarde,
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Error);
                }
            }
            return intValue;
        }


        private List<KorusDataEventArgs.Yta> GetAllKORUSYtor()
        {
            var ytor = new List<KorusDataEventArgs.Yta>();
            if (dataGridViewYtor.Rows.Count > 0)
            {
                ytor.AddRange(from DataGridViewRow row in dataGridViewYtor.Rows
                              select new KorusDataEventArgs.Yta
                                {                                    
                                    invtyp = GetObjIntValue(row.Cells[invtypIdDataGridViewTextBoxColumn.Name].Value),
                                    rojstam = GetObjIntValue(row.Cells[RojstamDataGridViewTextBoxColumn.Name].Value),
                                    regionid = GetObjIntValue(row.Cells[regionIdDataGridViewTextBoxColumn.Name].Value),
                                    distriktid = GetObjIntValue(row.Cells[distriktIdDataGridViewTextBoxColumn.Name].Value),
                                    traktnr = GetObjIntValue(row.Cells[traktnrDataGridViewTextBoxColumn.Name].Value),
                                    standort = GetObjIntValue(row.Cells[standortDataGridViewTextBoxColumn.Name].Value),
                                    entreprenor =
                                      GetObjIntValue(row.Cells[entreprenorDataGridViewTextBoxColumn.Name].Value),
                                    rapport_typ =
                                      DataHelper.GetRapportTyp(row.Cells[rapportTypDataGridViewTextBoxColumn.Name].Value),
                                    årtal = GetObjIntValue(row.Cells[artalDataGridViewTextBoxColumn.Name].Value),
                                    yta = GetObjIntValue(row.Cells[ytaDataGridViewTextBoxColumn.Name].Value),
                                    optimalt = GetObjIntValue(row.Cells[optimaltDataGridViewTextBoxColumn.Name].Value),
                                    bra = GetObjIntValue(row.Cells[braDataGridViewTextBoxColumn.Name].Value),
                                    varavbattre =
                                      GetObjIntValue(row.Cells[varavBattreDataGridViewTextBoxColumn.Name].Value),
                                    blekjordsflack =
                                      GetObjIntValue(row.Cells[blekJordsFlackDataGridViewTextBoxColumn.Name].Value),
                                    optimaltbra =
                                      GetObjIntValue(row.Cells[optimaltBraDataGridViewTextBoxColumn.Name].Value),
                                    ovrigt = GetObjIntValue(row.Cells[ovrigtDataGridViewTextBoxColumn.Name].Value),
                                    planterade = GetObjIntValue(row.Cells[planteradeDataGridViewTextBoxColumn.Name].Value),
                                    planteringsdjupfel =
                                      GetObjIntValue(row.Cells[planteringsDjupFelDataGridViewTextBoxColumn.Name].Value),
                                    tilltryckning =
                                      GetObjIntValue(row.Cells[tillTryckningDataGridViewTextBoxColumn.Name].Value),
                                    stickvbredd =
                                      GetObjFloatValue(row.Cells[stickvbreddDataGridViewTextBoxColumn.Name].Value),
                                    tall = GetObjIntValue(row.Cells[tallDataGridViewTextBoxColumn.Name].Value),
                                    tallmedelhojd =
                                      GetObjFloatValue(row.Cells[tallMedelHojdDataGridViewTextBoxColumn.Name].Value),
                                    gran = GetObjIntValue(row.Cells[granDataGridViewTextBoxColumn.Name].Value),
                                    granmedelhojd =
                                      GetObjFloatValue(row.Cells[granMedelHojdDataGridViewTextBoxColumn.Name].Value),
                                    lov = GetObjIntValue(row.Cells[lovDataGridViewTextBoxColumn.Name].Value),
                                    lovmedelhojd =
                                      GetObjFloatValue(row.Cells[lovMedelHojdDataGridViewTextBoxColumn.Name].Value),
                                    contorta = GetObjIntValue(row.Cells[contortaDataGridViewTextBoxColumn.Name].Value),
                                    contortamedelhojd =
                                      GetObjFloatValue(row.Cells[contortaMedelHojdDataGridViewTextBoxColumn.Name].Value),
                                    hst = GetObjIntValue(row.Cells[hstDataGridViewTextBoxColumn.Name].Value),
                                    mhojd = GetObjFloatValue(row.Cells[mHojdDataGridViewTextBoxColumn.Name].Value),
                                    summa = GetObjIntValue(row.Cells[summaDataGridViewTextBoxColumn.Name].Value),
                                    skador = GetObjIntValue(row.Cells[skadorDataGridViewTextBoxColumn.Name].Value)                                    
                                });
            }
            return ytor;
        }

        // ReSharper disable MemberCanBePrivate.Global
        public void LagraÄndradeKORUSYtor()
        // ReSharper restore MemberCanBePrivate.Global
        {
            if (MessageBox.Show(Resources.Vill_du_uppdatera_alla, Resources.Uppdatera_alla_KORUS_ytor,
                                MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes) return;
            var ytor = GetAllKORUSYtor();

            if (ytor.Count > 0)
            {
                DataEventLagraÄndradeYtor(this, ytor);
            }
        }

        // ReSharper disable MemberCanBePrivate.Global
        public void GåTillRapportData()
        // ReSharper restore MemberCanBePrivate.Global
        {
            var data = GetSelectedKORUSNyckelData();

            if (data != null)
            {
                DataEventGåTillRapport(this, data);
            }
        }

        private KorusDataEventArgs GetSelectedKORUSNyckelData()
        {
            if (dataGridViewYtor.SelectedCells.Count > 0)
            {
                var row = dataGridViewYtor.SelectedCells[0].OwningRow;
                var nyckelData = new KorusDataEventArgs
                  {
                      data =
                        {
                            invtyp = GetObjIntValue(row.Cells[invtypIdDataGridViewTextBoxColumn.Name].Value),
                            regionid = GetObjIntValue(row.Cells[regionIdDataGridViewTextBoxColumn.Name].Value),
                            distriktid = GetObjIntValue(row.Cells[distriktIdDataGridViewTextBoxColumn.Name].Value),
                            traktnr = GetObjIntValue(row.Cells[traktnrDataGridViewTextBoxColumn.Name].Value),
                            standort = GetObjIntValue(row.Cells[standortDataGridViewTextBoxColumn.Name].Value),
                            entreprenor =
                              GetObjIntValue(row.Cells[entreprenorDataGridViewTextBoxColumn.Name].Value),
                            rapport_typ =
                              DataHelper.GetRapportTyp(row.Cells[rapportTypDataGridViewTextBoxColumn.Name].Value),
                            årtal = GetObjIntValue(row.Cells[artalDataGridViewTextBoxColumn.Name].Value)
                        },
                      yta = { yta = GetObjIntValue(row.Cells[ytaDataGridViewTextBoxColumn.Name].Value) }
                  };

                if (nyckelData.KorrektaNyckelvärden && nyckelData.yta.yta >= 0)
                {
                    return nyckelData;
                }
            }
            return null;
        }

        // ReSharper disable MemberCanBePrivate.Global
        public void TaBortKORUSYta()
        // ReSharper restore MemberCanBePrivate.Global
        {
            var data = GetSelectedKORUSNyckelData();

            if (data != null)
            {
                DataEventTaBortYta(this, data);
            }
        }

        // ReSharper disable UnusedMember.Global
        public void TaBortKORUSDataLyckades()
        // ReSharper restore UnusedMember.Global
        {
            if (dataGridViewYtor.SelectedCells.Count > 0)
            {
                dataGridViewYtor.Rows.RemoveAt(dataGridViewYtor.SelectedCells[0].OwningRow.Index);
            }
        }

        private void buttonUppdateraData_Click(object sender, EventArgs e)
        {
            StatusEventRefresh(this, null);
        }

        private void buttonGåTillRapportData_Click(object sender, EventArgs e)
        {
            GåTillRapportData();
        }

        private void buttonTaBortData_Click(object sender, EventArgs e)
        {
            TaBortKORUSYta();
        }

        private void dataGridViewYtor_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            TaBortKORUSYta();
            //avbryt alltid delete av raden i GUI, det sköts istället från ett event när delete lyckades i databasen.
            e.Cancel = true;
        }

        private void dataGridViewYtor_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if (!((e.Exception) is FormatException)) return;
            var view = (DataGridView)sender;
            view.CurrentCell.Value = 0;
            e.ThrowException = false;
            e.Cancel = false;
        }

        private void dataGridViewYtor_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                if (dataGridViewYtor == null || e.FormattedValue == null ||
                    dataGridViewYtor.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly ||
                    !dataGridViewYtor.CurrentCell.IsInEditMode) return;

                // Don't try to validate the 'new row' until finished 
                // editing since there
                // is not any point in validating its initial value.
                if (dataGridViewYtor.Rows[e.RowIndex].IsNewRow)
                {
                    return;
                }

                dataGridViewYtor.Rows[e.RowIndex].ErrorText = "";
                dataGridViewYtor.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "";

                var cname = dataGridViewYtor.CurrentCell.OwningColumn.Name;

                if (cname.Equals(tallMedelHojdDataGridViewTextBoxColumn.Name) ||
                    cname.Equals(contortaMedelHojdDataGridViewTextBoxColumn.Name)
                    || cname.Equals(granMedelHojdDataGridViewTextBoxColumn.Name) ||
                    cname.Equals(lovMedelHojdDataGridViewTextBoxColumn.Name)
                    || cname.Equals(stickvbreddDataGridViewTextBoxColumn.Name))
                {
                    double cellDoubleValue;
                    if ((!double.TryParse(e.FormattedValue.ToString(), out cellDoubleValue) || cellDoubleValue < 0.0))
                    {
                        MessageBox.Show(Resources.EndastPositiva, Resources.FelaktigtVarde, MessageBoxButtons.OK,
                                        MessageBoxIcon.Stop);
                    }
                }
                else
                {
                    int cellIntValue;
                    if ((!int.TryParse(e.FormattedValue.ToString(), out cellIntValue) || cellIntValue < 0))
                    {
                        MessageBox.Show(Resources.EndastPositiva, Resources.FelaktigtVarde, MessageBoxButtons.OK,
                                        MessageBoxIcon.Stop);
                    }
                }
            }
            catch (Exception ex)
            {
#if !DEBUG
       MessageBox.Show("Det gick inte att validera värdet på cellen.", "Fel vid cell validering", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void buttonLagraÄndratData_Click(object sender, EventArgs e)
        {
            LagraÄndradeKORUSYtor();
        }

        private void buttonFiltreraData_Click(object sender, EventArgs e)
        {
            var filterDialog = new FormFiltreraYtor();
            filterDialog.InitData(mYtor);
            if (filterDialog.ShowDialog(this) == DialogResult.OK)
            {
                UppdateraDataSet();
            }
        }


        private void FormatRapportTyp(DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                if (dataGridViewYtor == null || e.ColumnIndex < 0 || e.ColumnIndex >= dataGridViewYtor.Columns.Count) return;

                if (dataGridViewYtor.Columns[e.ColumnIndex].DataPropertyName != "RapportTyp") return;

                if (e.Value == null || e.Value.ToString().Trim().Equals(string.Empty)) return;

                var stringValue = e.Value.ToString();
                var rapportTyp = DataHelper.GetRapportTyp(stringValue);
                var row = dataGridViewYtor.Rows[e.RowIndex];
                switch (rapportTyp)
                {
                    case (int)RapportTyp.Markberedning:
                        e.CellStyle.BackColor = Color.LightBlue;
                        SetVisibleKolumn(true, true, false, true, true, false, false, false, false, false, false, false, false,
                                         false, false, false, false, false, false, false, false, false, false, row);
                        e.FormattingApplied = true;
                        break;
                    case (int)RapportTyp.Plantering:
                        e.CellStyle.BackColor = Color.LightGreen;
                        SetVisibleKolumn(true, true, true, false, false, true, true, true, true, false, false, false, false,
                                         false, false, false, false, false, false, false, false, false, false, row);
                        e.FormattingApplied = true;
                        break;
                    case (int)RapportTyp.Gallring:
                        e.CellStyle.BackColor = Color.LightSalmon;
                        SetVisibleKolumn(false, false, false, false, false, false, false, false, false, true, false, true,
                                         false, true, false, true, false, false, false, true, true, true, false, row);
                        e.FormattingApplied = true;
                        break;
                    case (int)RapportTyp.Röjning:
                        e.CellStyle.BackColor = Color.LightCyan;
                        SetVisibleKolumn(false, false, false, false, false, false, false, false, false, true, true, true, true,
                                         true, true, true, true, true, true, false, false, false, true, row);
                        e.FormattingApplied = true;
                        break;
                    case (int)RapportTyp.Biobränsle:
                        e.CellStyle.BackColor = Color.LightCoral;
                        SetVisibleKolumn(false, false, false, false, false, false, false, false, false, false, false, false,
                                         false, false, false, false, false, false, false, false, false, false, false, row);
                        e.FormattingApplied = true;
                        break;
                    case (int)RapportTyp.Slutavverkning:
                        e.CellStyle.BackColor = Color.LightGray;
                        SetVisibleKolumn(false, false, false, false, false, false, false, false, false, false, false, false,
                                         false, false, false, false, false, false, false, false, false, false, false, row);
                        e.FormattingApplied = true;
                        break;
                }
            }
            catch (Exception ex)
            {
#if !DEBUG
     MessageBox.Show("Misslyckades med att formattera rapporttypen", "Formattera rapporttyp misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void FormatEntreprenör(DataGridViewCellFormattingEventArgs e)
        {
            try
            {
                if (dataGridViewYtor == null || e.ColumnIndex < 0 || e.ColumnIndex >= dataGridViewYtor.Columns.Count) return;

                if (dataGridViewYtor.Columns[e.ColumnIndex].DataPropertyName != "Entreprenor") return;

                if (e.Value == null || e.Value.ToString().Trim().Equals(string.Empty)) return;

                var stringValue = e.Value.ToString();
                var regionid =
                  dataGridViewYtor.Rows[e.RowIndex].Cells[regionIdDataGridViewTextBoxColumn.Name].Value.ToString();
                e.Value = regionid + "-" + stringValue;
            }
            catch (Exception ex)
            {
#if !DEBUG
     MessageBox.Show("Misslyckades med att formattera entreprenör namn", "Formatera entreprenör misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void dataGridViewYtor_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            FormatRapportTyp(e);
            FormatEntreprenör(e);
        }

        private static void SättCellStatus(DataGridViewRow aRow, string aKolumnNamn, bool aReadOnly)
        {
            if (aRow == null) return;
            aRow.Cells[aKolumnNamn].ReadOnly = aReadOnly;
            aRow.Cells[aKolumnNamn].Style.BackColor = aReadOnly ? Color.LemonChiffon : Color.White;
        }

        private void SetVisibleKolumn(bool aOptimalt, bool aBra, bool aVaravBättre, bool aBlekjordsfläck, bool aOptimaltBra,
                                      bool aÖvrigt, bool aPlanterade, bool aPlanteringsDjupFel, bool aTilltryckning,
                                      bool aTall, bool aTallMedelHöjd,
                                      bool aGran, bool aGranMedelhöjd, bool aLöv, bool aLövMedelHöjd, bool aContorta,
                                      bool aContortaMedelhöjd,
                                      bool aHst, bool aMHöjd, bool aSumma, bool aSkador, bool aStickvägsbredd, bool aRojstam,DataGridViewRow aRow)
        {
            if (aRow != null)
            {
                SättCellStatus(aRow, optimaltDataGridViewTextBoxColumn.Name, !aOptimalt);
                SättCellStatus(aRow, braDataGridViewTextBoxColumn.Name, !aBra);
                SättCellStatus(aRow, varavBattreDataGridViewTextBoxColumn.Name, !aVaravBättre);
                SättCellStatus(aRow, blekJordsFlackDataGridViewTextBoxColumn.Name, !aBlekjordsfläck);
                SättCellStatus(aRow, optimaltBraDataGridViewTextBoxColumn.Name, !aOptimaltBra);
                SättCellStatus(aRow, ovrigtDataGridViewTextBoxColumn.Name, !aÖvrigt);
                SättCellStatus(aRow, planteradeDataGridViewTextBoxColumn.Name, !aPlanterade);
                SättCellStatus(aRow, planteringsDjupFelDataGridViewTextBoxColumn.Name, !aPlanteringsDjupFel);
                SättCellStatus(aRow, tillTryckningDataGridViewTextBoxColumn.Name, !aTilltryckning);
                SättCellStatus(aRow, tallDataGridViewTextBoxColumn.Name, !aTall);
                SättCellStatus(aRow, tallMedelHojdDataGridViewTextBoxColumn.Name, !aTallMedelHöjd);
                SättCellStatus(aRow, granDataGridViewTextBoxColumn.Name, !aGran);
                SättCellStatus(aRow, granMedelHojdDataGridViewTextBoxColumn.Name, !aGranMedelhöjd);
                SättCellStatus(aRow, lovDataGridViewTextBoxColumn.Name, !aLöv);
                SättCellStatus(aRow, lovMedelHojdDataGridViewTextBoxColumn.Name, !aLövMedelHöjd);
                SättCellStatus(aRow, contortaDataGridViewTextBoxColumn.Name, !aContorta);
                SättCellStatus(aRow, contortaMedelHojdDataGridViewTextBoxColumn.Name, !aContortaMedelhöjd);
                SättCellStatus(aRow, hstDataGridViewTextBoxColumn.Name, !aHst);
                SättCellStatus(aRow, mHojdDataGridViewTextBoxColumn.Name, !aMHöjd);
                SättCellStatus(aRow, summaDataGridViewTextBoxColumn.Name, !aSumma);
                SättCellStatus(aRow, skadorDataGridViewTextBoxColumn.Name, !aSkador);
                SättCellStatus(aRow, stickvbreddDataGridViewTextBoxColumn.Name, !aStickvägsbredd);
                SättCellStatus(aRow, RojstamDataGridViewTextBoxColumn.Name, !aStickvägsbredd);
            }
            else
            {
                optimaltDataGridViewTextBoxColumn.Visible = aOptimalt;
                braDataGridViewTextBoxColumn.Visible = aBra;
                varavBattreDataGridViewTextBoxColumn.Visible = aVaravBättre;
                blekJordsFlackDataGridViewTextBoxColumn.Visible = aBlekjordsfläck;
                optimaltBraDataGridViewTextBoxColumn.Visible = aOptimaltBra;

                ovrigtDataGridViewTextBoxColumn.Visible = aÖvrigt;
                planteradeDataGridViewTextBoxColumn.Visible = aPlanterade;
                planteringsDjupFelDataGridViewTextBoxColumn.Visible = aPlanteringsDjupFel;
                tillTryckningDataGridViewTextBoxColumn.Visible = aTilltryckning;
                tallDataGridViewTextBoxColumn.Visible = aTall;
                tallMedelHojdDataGridViewTextBoxColumn.Visible = aTallMedelHöjd;

                granDataGridViewTextBoxColumn.Visible = aGran;
                granMedelHojdDataGridViewTextBoxColumn.Visible = aGranMedelhöjd;
                lovDataGridViewTextBoxColumn.Visible = aLöv;
                lovMedelHojdDataGridViewTextBoxColumn.Visible = aLövMedelHöjd;
                contortaDataGridViewTextBoxColumn.Visible = aContorta;
                contortaMedelHojdDataGridViewTextBoxColumn.Visible = aContortaMedelhöjd;

                hstDataGridViewTextBoxColumn.Visible = aHst;
                mHojdDataGridViewTextBoxColumn.Visible = aMHöjd;
                summaDataGridViewTextBoxColumn.Visible = aSumma;
                skadorDataGridViewTextBoxColumn.Visible = aSkador;

                stickvbreddDataGridViewTextBoxColumn.Visible = aStickvägsbredd;
            }
        }

        private void radioButtonMarkberedning_CheckedChanged(object sender, EventArgs e)
        {
            var rButton = sender as RadioButton;
            if (rButton == null) return;
            if (!rButton.Checked) return;
            //Aktiv, filtrera tabeller.
            var dataView = dataSetYtor.Tables["Ytor"].DefaultView;
            dataView.RowFilter = "RapportTyp = '" + RapportTyp.Markberedning + "'";
            dataGridViewYtor.DataSource = dataView;
            SetVisibleKolumn(true, true, false, true, true, false, false, false, false, false, false, false, false, false,
                             false, false, false, false, false, false, false, false, false, null);
            Settings.Default.ValdTypYtor = (int)RapportTyp.Markberedning;
        }

        private void radioButtonPlantering_CheckedChanged(object sender, EventArgs e)
        {
            var rButton = sender as RadioButton;
            if (rButton == null) return;
            if (!rButton.Checked) return;
            //Aktiv, filtrera tabeller.
            var dataView = dataSetYtor.Tables["Ytor"].DefaultView;
            dataView.RowFilter = "RapportTyp = '" + RapportTyp.Plantering + "'";
            dataGridViewYtor.DataSource = dataView;
            SetVisibleKolumn(true, true, true, false, false, true, true, true, true, false, false, false, false, false,
                             false, false, false, false, false, false, false, false, false, null);
            Settings.Default.ValdTypYtor = (int)RapportTyp.Plantering;
        }

        private void radioButtonRöjning_CheckedChanged(object sender, EventArgs e)
        {
            var rButton = sender as RadioButton;
            if (rButton == null) return;
            if (!rButton.Checked) return;
            //Aktiv, filtrera tabeller.
            var dataView = dataSetYtor.Tables["Ytor"].DefaultView;
            dataView.RowFilter = "RapportTyp = '" + RapportTyp.Röjning + "'";
            dataGridViewYtor.DataSource = dataView;
            SetVisibleKolumn(false, false, false, false, false, false, false, false, false, true, true, true, true, true,
                             true, true, true, true, true, false, false, false, true, null);
            Settings.Default.ValdTypYtor = (int)RapportTyp.Röjning;
        }

        private void radioButtonGallring_CheckedChanged(object sender, EventArgs e)
        {
            var rButton = sender as RadioButton;
            if (rButton == null) return;
            if (!rButton.Checked) return;
            //Aktiv, filtrera tabeller.
            var dataView = dataSetYtor.Tables["Ytor"].DefaultView;
            dataView.RowFilter = "RapportTyp = '" + RapportTyp.Gallring + "'";
            dataGridViewYtor.DataSource = dataView;
            SetVisibleKolumn(false, false, false, false, false, false, false, false, false, true, false, true, false, true,
                             false, true, false, false, false, true, true, true, false, null);
            Settings.Default.ValdTypYtor = (int)RapportTyp.Gallring;
        }

        private void FormListaYtor_Load(object sender, EventArgs e)
        {
            switch (Settings.Default.ValdTypYtor)
            {
                case (int)RapportTyp.Gallring:
                    radioButtonGallring.Checked = true;
                    break;
                case (int)RapportTyp.Markberedning:
                    radioButtonMarkberedning.Checked = true;
                    break;
                case (int)RapportTyp.Plantering:
                    radioButtonPlantering.Checked = true;
                    break;
                case (int)RapportTyp.Röjning:
                    radioButtonRöjning.Checked = true;
                    break;
                default:
                    radioButtonMarkberedning.Checked = true;
                    break;
            }
        }

        private void dataGridViewYtor_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dataGridViewYtor == null || dataGridViewYtor.CurrentCell.Value == null) return;

                var cname = dataGridViewYtor.CurrentCell.OwningColumn.Name;

                if (cname.Equals(tallMedelHojdDataGridViewTextBoxColumn.Name) ||
                    cname.Equals(contortaMedelHojdDataGridViewTextBoxColumn.Name)
                    || cname.Equals(granMedelHojdDataGridViewTextBoxColumn.Name) ||
                    cname.Equals(lovMedelHojdDataGridViewTextBoxColumn.Name)
                    || cname.Equals(stickvbreddDataGridViewTextBoxColumn.Name))
                {
                    double cellDoubleValue;
                    if ((!double.TryParse(dataGridViewYtor.CurrentCell.Value.ToString(), out cellDoubleValue) ||
                         cellDoubleValue < 0.0))
                    {
                        dataGridViewYtor.CurrentCell.Value = 0;
                    }
                }
                else
                {
                    int cellIntValue;
                    if ((!int.TryParse(dataGridViewYtor.CurrentCell.Value.ToString(), out cellIntValue) || cellIntValue < 0))
                    {
                        dataGridViewYtor.CurrentCell.Value = 0;
                    }
                }
            }
            catch (Exception ex)
            {
#if !DEBUG
       MessageBox.Show("Det gick inte att kontrollera värdet på cellen.", "Fel vid cell editering", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }
    }
}