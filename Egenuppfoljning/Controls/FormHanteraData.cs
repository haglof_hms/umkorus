﻿#region

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Windows.Forms;
using Egenuppfoljning.Applications;
using Egenuppfoljning.Interfaces;
using Egenuppfoljning.Properties;

#endregion

namespace Egenuppfoljning.Controls
{
// ReSharper disable UnusedMember.Global
  public partial class FormHanteraData : UserControl
// ReSharper restore UnusedMember.Global
  {
    private IKorusRapport mAktivRapportForm;
    private int mDbIndex;
    private int mPosX;
    private int mPosY;
    private List<KorusDataEventArgs> mReports;

    public FormHanteraData()
    {
      InitializeComponent();
      mReports = new List<KorusDataEventArgs>();
      mDbIndex = 0;
    }

// ReSharper disable EventNeverSubscribedTo.Global
    public event DataEventHandler DataEventRemove;
    public event DataEventHandler DataEventUpdate;
    public event StatusEventHandler StatusEventRefresh;
    public event StatusEventHandler StatusEventGUIUpdate;
// ReSharper restore EventNeverSubscribedTo.Global

// ReSharper disable UnusedMember.Global
    public void Close()
// ReSharper restore UnusedMember.Global
    {
      Settings.Default.Save();
      Hide();
    }


// ReSharper disable MemberCanBePrivate.Global
    public void VisaRapport()
// ReSharper restore MemberCanBePrivate.Global
    {
      LaddaRapportForm();

      if (mAktivRapportForm != null)
      {
        mAktivRapportForm.ShowReport();
      }
    }

// ReSharper disable UnusedMember.Global
    public void SkrivUtRapport()
// ReSharper restore UnusedMember.Global
    {
      LaddaRapportForm();

      if (mAktivRapportForm != null)
      {
        mAktivRapportForm.SkrivUt();
      }
    }

// ReSharper disable MemberCanBePrivate.Global
    public void RedigeraRapport()
// ReSharper restore MemberCanBePrivate.Global
    {
      try
      {
        LaddaRapportForm();

        if (mAktivRapportForm == null) return;

        BringToFront();
        Invoke(new MethodFlagInvoker(mAktivRapportForm.ShowEditDialog), new object[] {true});

        //Eftersom inte entreprenörnamn används av applikationsdialogerna, så lagras det temporärt undan
        if (mAktivRapportForm.RedigeraLagratData)
        {
          //Datat ska lagras mot databasen 
          var data = mAktivRapportForm.GetKORUSData();
          data.data.entreprenornamn = mReports[mDbIndex].data.entreprenornamn;
          DataEventUpdate(this, data);
          mReports[mDbIndex] = data;
          UppdateraGUI();
        }
        else
        {
          mAktivRapportForm.UppdateraData(mReports[mDbIndex]);
        }
        Focus();
        BringToFront();
      }
      catch (Exception ex)
      {
#if !DEBUG
       MessageBox.Show("Redigera lagrat data misslyckades.", "Redigera misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

// ReSharper disable MemberCanBePrivate.Global
    public void TabortRapport()
// ReSharper restore MemberCanBePrivate.Global
    {
      DataEventRemove(this, mReports[mDbIndex]);
    }

    private void CheckaDBIndex()
    {
      if (mDbIndex < 0)
      {
        mDbIndex = 0;
      }

      if (mDbIndex >= (mReports.Count - 1))
      {
        mDbIndex = (mReports.Count - 1);
      }

      if (mDbIndex >= 0) return; //Databasen är tom, lägg då till ett tomt element.
      mReports.Add(new KorusDataEventArgs());
      mDbIndex = 0;
    }

    private static bool CheckZeroValue(string aNumberString)
    {
      int number;
      try
      {
        number = int.Parse(aNumberString);
      }
      catch (Exception)
      {
        //Not a number
        return false;
      }

      return (number == 0 || number == -1);
    }

    private void VisaEllerGöm(Control aDataLabel, string aTitle)
    {
      var data = aDataLabel.Text.Replace(aTitle, string.Empty).Trim();
      if (data.Equals(string.Empty) ||
          data.Equals("_") || CheckZeroValue(data))
      {
        aDataLabel.Visible = false;
      }
      else
      {
        aDataLabel.Visible = true;

        if (aDataLabel.Parent.Name.Equals("groupBoxData"))
        {
          aDataLabel.Location = new Point(mPosX, mPosY);
          if (mPosX < 710)
          {
            mPosX += 230;
          }
          else
          {
            mPosY += 40;
            mPosX = 25;
          }
        }
      }
    }

    private void CheckaLabelData()
    {
      mPosX = 25;
      mPosY = 30;

      VisaEllerGöm(labelMarkberedningsMetod, "Markberednings metod: ");

      VisaEllerGöm(labelUrsprung, Resources.Ursprung);
      VisaEllerGöm(labelProvytestorlek, Resources.Provytestorlek);
      VisaEllerGöm(labelAreal, Resources.Areal);
      VisaEllerGöm(labelMålgrundyta, Resources.Malgrundyta);
      VisaEllerGöm(labelStickvägssystem, Resources.Stickvagssystem);
      VisaEllerGöm(labelStickvägsavstånd, Resources.Stickvagsavstand);
      VisaEllerGöm(labelGISSträcka, Resources.GIS_Stracka);
      VisaEllerGöm(labelGISAreal, Resources.GIS_Areal);

      VisaEllerGöm(labelAntalVältor, Resources.Antal_valtor);
      VisaEllerGöm(labelAvstånd, Resources.Avstand);
      VisaEllerGöm(labelBedömdVolym, Resources.Bedomd_volym);
      VisaEllerGöm(labelHygge, Resources.Hygge);
      VisaEllerGöm(labelVägkant, Resources.Vagkant);
      VisaEllerGöm(labelÅker, Resources.Aker);
    }

    private string EgenUppföljningsTyp()
    {
      switch (mReports[mDbIndex].data.rapport_typ)
      {
        case (int) RapportTyp.Markberedning:
          return RapportTyp.Markberedning.ToString();
        case (int) RapportTyp.Plantering:
          return RapportTyp.Plantering.ToString();
        case (int) RapportTyp.Gallring:
          return RapportTyp.Gallring.ToString();
        case (int) RapportTyp.Röjning:
          return RapportTyp.Röjning.ToString();
        case (int) RapportTyp.Biobränsle:
          return RapportTyp.Biobränsle.ToString();
        case (int) RapportTyp.Slutavverkning:
          return RapportTyp.Slutavverkning.ToString();
        default:
          return string.Empty;
      }
    }

    private int TotalaAntaletFrågor()
    {
      var antalFrågor = 0;
      if (mReports[mDbIndex].frågeformulär.fraga1 != null &&
          !mReports[mDbIndex].frågeformulär.fraga1.Equals(string.Empty))
        antalFrågor++;
      if (mReports[mDbIndex].frågeformulär.fraga2 != null &&
          !mReports[mDbIndex].frågeformulär.fraga2.Equals(string.Empty))
        antalFrågor++;
      if (mReports[mDbIndex].frågeformulär.fraga3 != null &&
          !mReports[mDbIndex].frågeformulär.fraga3.Equals(string.Empty))
        antalFrågor++;
      if (mReports[mDbIndex].frågeformulär.fraga4 != null &&
          !mReports[mDbIndex].frågeformulär.fraga4.Equals(string.Empty))
        antalFrågor++;
      if (mReports[mDbIndex].frågeformulär.fraga5 != null &&
          !mReports[mDbIndex].frågeformulär.fraga5.Equals(string.Empty))
        antalFrågor++;
      if (mReports[mDbIndex].frågeformulär.fraga6 != null &&
          !mReports[mDbIndex].frågeformulär.fraga6.Equals(string.Empty))
        antalFrågor++;
      if (mReports[mDbIndex].frågeformulär.fraga7 != null &&
          !mReports[mDbIndex].frågeformulär.fraga7.Equals(string.Empty))
        antalFrågor++;
      if (mReports[mDbIndex].frågeformulär.fraga8 != null &&
          !mReports[mDbIndex].frågeformulär.fraga8.Equals(string.Empty))
        antalFrågor++;
      if (mReports[mDbIndex].frågeformulär.fraga9 != null &&
          !mReports[mDbIndex].frågeformulär.fraga9.Equals(string.Empty))
        antalFrågor++;
      if (mReports[mDbIndex].frågeformulär.fraga10 != null &&
          !mReports[mDbIndex].frågeformulär.fraga10.Equals(string.Empty))
        antalFrågor++;
      if (mReports[mDbIndex].frågeformulär.fraga11 != null &&
          !mReports[mDbIndex].frågeformulär.fraga11.Equals(string.Empty))
        antalFrågor++;
      if (mReports[mDbIndex].frågeformulär.fraga12 != null &&
          !mReports[mDbIndex].frågeformulär.fraga12.Equals(string.Empty))
        antalFrågor++;
      if (mReports[mDbIndex].frågeformulär.fraga13 != null &&
          !mReports[mDbIndex].frågeformulär.fraga13.Equals(string.Empty))
        antalFrågor++;
      if (mReports[mDbIndex].frågeformulär.fraga14 != null &&
          !mReports[mDbIndex].frågeformulär.fraga14.Equals(string.Empty))
        antalFrågor++;
      if (mReports[mDbIndex].frågeformulär.fraga15 != null &&
          !mReports[mDbIndex].frågeformulär.fraga15.Equals(string.Empty))
        antalFrågor++;
      if (mReports[mDbIndex].frågeformulär.fraga16 != null &&
          !mReports[mDbIndex].frågeformulär.fraga16.Equals(string.Empty))
        antalFrågor++;
      if (mReports[mDbIndex].frågeformulär.fraga17 != null &&
          !mReports[mDbIndex].frågeformulär.fraga17.Equals(string.Empty))
        antalFrågor++;
      if (mReports[mDbIndex].frågeformulär.fraga18 != null &&
          !mReports[mDbIndex].frågeformulär.fraga18.Equals(string.Empty))
        antalFrågor++;
      if (mReports[mDbIndex].frågeformulär.fraga19 != null &&
          !mReports[mDbIndex].frågeformulär.fraga19.Equals(string.Empty))
        antalFrågor++;
      if (mReports[mDbIndex].frågeformulär.fraga20 != null &&
          !mReports[mDbIndex].frågeformulär.fraga20.Equals(string.Empty))
        antalFrågor++;
      if (mReports[mDbIndex].frågeformulär.fraga21 != null &&
          !mReports[mDbIndex].frågeformulär.fraga21.Equals(string.Empty))
        antalFrågor++;

      if (mAktivRapportForm != null && mAktivRapportForm.GetEgenuppföljningsTyp().Equals(Resources.Gallring) &&
          antalFrågor > 1)
      {
        //Justering till Gallring, som använder 2A, 2B, 2C på en fråga.
        antalFrågor -= 2;
      }

      return antalFrågor;
    }

// ReSharper disable UnusedMember.Global
    public void TaBortKORUSDataLyckades()
// ReSharper restore UnusedMember.Global
    {
      mReports.RemoveAt(mDbIndex); //Ta även bort datat i den cashade vektorn
      GåTillFöregåendeRapport();
    }

    private string RegionNamn()
    {
      var region = mReports[mDbIndex].data.regionnamn;
      if (region != null)
      {
        var a = region.Trim().IndexOf("-", StringComparison.Ordinal) + 1;
        var b = region.Trim().Length - a;

        return region.Substring(a, b);
      }
      return string.Empty;
    }

    private string DistriktNamn()
    {
      var distrikt = mReports[mDbIndex].data.distriktnamn;
      if (distrikt != null)
      {
        var a = distrikt.Trim().IndexOf("-", StringComparison.Ordinal) + 1;
        var b = distrikt.Trim().Length - a;
        return distrikt.Trim().Substring(a, b);
      }
      return string.Empty;
    }

// ReSharper disable MemberCanBePrivate.Global
    public void UppdateraGUI()
// ReSharper restore MemberCanBePrivate.Global
    {
      try
      {
        CheckaDBIndex();

        var tomDatabas = (mReports.Count == 1 && mReports[0].IsEmpty());

        if (!tomDatabas)
        {
          labelTitelKommentar.Visible = true;
          labelEgenuppföljningstyp.Text = Resources.Egenuppfoljningstyp + EgenUppföljningsTyp();
          labelTotalaAntaletFrågor.Text = Resources.Totala_antalet_valda_fragor +
                                          TotalaAntaletFrågor().ToString(CultureInfo.InvariantCulture);
          var datum = mReports[mDbIndex].data.datum;

          if (datum.IndexOf(" ", StringComparison.Ordinal) > 0)
          {
            labelDatum.Text = Resources.Datum + Resources.Colon +
                              datum.Substring(0, datum.IndexOf(" ", StringComparison.Ordinal));
          }
          else
          {
            labelDatum.Text = Resources.Datum + Resources.Colon + datum;
          }

          labelDatabasPosition.Text = Resources.Databas_position + (mDbIndex + 1).ToString(CultureInfo.InvariantCulture);
          labelTotalaAntaletYtor.Text = Resources.Totala_antalet_ifyllda_ytor +
                                        mReports[mDbIndex].ytor.Count.ToString(CultureInfo.InvariantCulture);
          labelMarkberedningsMetod.Text = Resources.Markberedningsmetod + Resources.Colon +
                                          mReports[mDbIndex].data.markberedningsmetod;
          labelDatainsamlingsmetod.Text = Resources.Datainsamlingsmetod + mReports[mDbIndex].data.datainsamlingsmetod;

          labelInvtypID.Text = Resources.Invtypnr + DataSetParser.getInvNamn(mReports[mDbIndex].data.invtyp);

          switch (mReports[mDbIndex].data.rapport_typ)
          {
              case (int)RapportTyp.Plantering:
              case (int)RapportTyp.Röjning:
                  labelAtgAreal.Visible = true;
                  labelAtgAreal.Text = Resources.AtgAreal + Math.Round((double)mReports[mDbIndex].data.atgareal, 1).ToString(CultureInfo.InvariantCulture);
                  break;
              default:
                  labelAtgAreal.Text = string.Empty;
                  labelAtgAreal.Visible = false;
                  break;
          }

          labelRegionID.Text = Resources.Regionnr +
                               mReports[mDbIndex].data.regionid.ToString(CultureInfo.InvariantCulture);
          labelDistriktID.Text = Resources.Distriktnr +
                                 mReports[mDbIndex].data.distriktid.ToString(CultureInfo.InvariantCulture);
          labelTraktnr.Text = Resources.Traktnr + Resources.Colon +
                              string.Format("{0:000000}", mReports[mDbIndex].data.traktnr);
          labelEntreprenörNr.Text = Resources.Entreprenornr +
                                    mReports[mDbIndex].data.regionid.ToString(CultureInfo.InvariantCulture) +
                                    Resources.Bindelstreck +
                                    string.Format("{0:0000}", mReports[mDbIndex].data.entreprenor);

          labelRegionNamn.Text = Resources.Regionnamn + RegionNamn();
          labelDistriktNamn.Text = Resources.Distriktnamn + DistriktNamn();
          labelTraktNamn.Text = Resources.Traktnamn + Resources.Colon + mReports[mDbIndex].data.traktnamn;
          labelEntreprenörNamn.Text = Resources.Entreprenornamn + mReports[mDbIndex].data.entreprenornamn;
          labelStandort.Text = Resources.Standort2 +
                               mReports[mDbIndex].data.standort.ToString(CultureInfo.InvariantCulture);

          labelUrsprung.Text = Resources.Ursprung + Resources.Colon + mReports[mDbIndex].data.ursprung;
          labelProvytestorlek.Text = Resources.Provytestorlek +
                                     mReports[mDbIndex].data.provytestorlek.ToString(CultureInfo.InvariantCulture);
          labelGISSträcka.Text = Resources.GIS_Stracka +
                                 mReports[mDbIndex].data.gisstracka.ToString(CultureInfo.InvariantCulture);
          labelGISAreal.Text = Resources.GIS_Areal +
                               Math.Round(mReports[mDbIndex].data.gisareal, 1).ToString(CultureInfo.InvariantCulture);

          labelAreal.Text = Resources.Areal +
                            Math.Round(mReports[mDbIndex].data.areal, 1).ToString(CultureInfo.InvariantCulture);
          labelMålgrundyta.Text = Resources.Malgrundyta +
                                  mReports[mDbIndex].data.malgrundyta.ToString(CultureInfo.InvariantCulture);
          labelStickvägssystem.Text = Resources.Stickvagssystem + mReports[mDbIndex].data.stickvagssystem;
          labelStickvägsavstånd.Text = Resources.Stickvagsavstand +
                                       mReports[mDbIndex].data.stickvagsavstand.ToString(CultureInfo.InvariantCulture);

          labelAntalVältor.Text = Resources.Antal_valtor +
                                  mReports[mDbIndex].data.antalvaltor.ToString(CultureInfo.InvariantCulture);
          labelAvstånd.Text = Resources.Avstand + mReports[mDbIndex].data.avstand.ToString(CultureInfo.InvariantCulture);
          labelBedömdVolym.Text = Resources.Bedomd_volym +
                                  mReports[mDbIndex].data.bedomdvolym.ToString(CultureInfo.InvariantCulture);
          labelHygge.Text = Resources.Hygge + mReports[mDbIndex].data.hygge.ToString(CultureInfo.InvariantCulture);
          labelVägkant.Text = Resources.Vagkant + mReports[mDbIndex].data.vagkant.ToString(CultureInfo.InvariantCulture);
          labelÅker.Text = Resources.Aker + mReports[mDbIndex].data.aker.ToString(CultureInfo.InvariantCulture);

          richTextBoxKommentar.Text = mReports[mDbIndex].data.kommentar;

          CheckaLabelData();
        }
        else
        {
          labelEgenuppföljningstyp.Text = string.Empty;
          labelTotalaAntaletFrågor.Text = string.Empty;
          labelDatum.Text = string.Empty;

          labelDatabasPosition.Text = string.Empty;
          labelTotalaAntaletYtor.Text = string.Empty;
          labelMarkberedningsMetod.Text = string.Empty;
          labelDatainsamlingsmetod.Text = string.Empty;

          labelInvtypID.Text = string.Empty;
          labelRegionID.Text = string.Empty;
          labelDistriktID.Text = string.Empty;
          labelTraktnr.Text = string.Empty;
          labelEntreprenörNr.Text = string.Empty;
          labelAtgAreal.Text = string.Empty;

          switch (mReports[mDbIndex].data.rapport_typ)
          {
              case (int)RapportTyp.Plantering:
              case (int)RapportTyp.Röjning:
                  labelAtgAreal.Visible = true;
                  labelAtgAreal.Text = Resources.AtgAreal + mReports[mDbIndex].data.atgareal.ToString(CultureInfo.InvariantCulture);

                  break;
              default:
                  labelAtgAreal.Visible = false;
                  break;
          }

          labelRegionNamn.Text = string.Empty;
          labelDistriktNamn.Text = string.Empty;
          labelTraktNamn.Text = string.Empty;
          labelEntreprenörNamn.Text = string.Empty;
          labelStandort.Text = string.Empty;

          labelUrsprung.Text = string.Empty;
          labelProvytestorlek.Text = string.Empty;
          labelGISSträcka.Text = string.Empty;
          labelGISAreal.Text = string.Empty;

          labelAreal.Text = string.Empty;
          labelMålgrundyta.Text = string.Empty;
          labelStickvägssystem.Text = string.Empty;
          labelStickvägsavstånd.Text = string.Empty;

          labelAntalVältor.Text = string.Empty;
          labelAvstånd.Text = string.Empty;
          labelBedömdVolym.Text = string.Empty;
          labelHygge.Text = string.Empty;
          labelVägkant.Text = string.Empty;
          labelÅker.Text = string.Empty;

          richTextBoxKommentar.Text = string.Empty;
          labelTitelKommentar.Visible = false;
        }

        labelTomDatabas.Visible = tomDatabas;
        buttonExportera.Enabled = !tomDatabas;
        buttonRedigeraData.Enabled = !tomDatabas;
        buttonTaBortData.Enabled = !tomDatabas;
        labelDatabasPosition.Visible = !tomDatabas;

        var sistaIndex = 0;
        if (mReports.Count > 0)
        {
          sistaIndex = mReports.Count - 1;
        }

        if (StatusEventGUIUpdate != null)
        {
          StatusEventGUIUpdate(this, new KorusStatusEventArgs(tomDatabas, mDbIndex, sistaIndex));
        }
      }
      catch (Exception ex)
      {
#if !DEBUG
       MessageBox.Show("Uppdatera GUI misslyckades. En del data är felaktiga och kunde inte hanteras.", "Uppdatera GUI misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

// ReSharper disable UnusedMember.Global
    public void GåTillFörstaRapport()

    {
      mDbIndex = 0;
      UppdateraGUI();
    }

    public void GåTillSistaRapport()
    {
      mDbIndex = mReports.Count;
      UppdateraGUI();
    }

    public void GåTillNästaRapport()
    {
      mDbIndex++;
      UppdateraGUI();
    }

    // ReSharper restore UnusedMember.Global

// ReSharper disable MemberCanBePrivate.Global
    public void GåTillFöregåendeRapport()
// ReSharper restore MemberCanBePrivate.Global
    {
      mDbIndex--;
      UppdateraGUI();
    }

    private void GåTillRapportIndex(int aIndex)
    {
      mDbIndex = aIndex;
      UppdateraGUI();
    }

    private bool LetaRättPåRapport(KorusDataEventArgs.Data aData)
    {
      try
      {
        var index = 0;
        foreach (var rapport in mReports)
        {
          if (IdentiskaDataNycklar(rapport.data, aData))
          {
            GåTillRapportIndex(index);
            return true;
          }
          index++;
        }
      }
      catch (Exception ex)
      {
#if !DEBUG
       MessageBox.Show("Rapporten kunde inte hittas.", "Fel vid gå till rapport", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
      return false;
    }

// ReSharper disable UnusedMember.Global
    public void GåTillRapport(KorusDataEventArgs aRapport)
// ReSharper restore UnusedMember.Global
    {
      if (aRapport == null) return;
      if (LetaRättPåRapport(aRapport.data)) return; //Ingen rapport hittades, uppdatera från databasen och prova igen.
      StatusEventRefresh(this, null);
      if (!LetaRättPåRapport(aRapport.data))
      {
        MessageBox.Show(Resources.Den_valda_rapporten_hittades_inte_i_databasen, Resources.Rapporten_finns_inte,
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
      }
    }

// ReSharper disable UnusedMember.Global
    public void UppdateraRapportData(List<KorusDataEventArgs> aReports)
// ReSharper restore UnusedMember.Global
    {
      mReports.Clear();
      mReports = aReports;
      UppdateraGUI();
    }

    private static bool IdentiskaDataNycklar(KorusDataEventArgs.Data aData1, KorusDataEventArgs.Data aData2)
    {
      return ((aData1.regionid == aData2.regionid) &&
              (aData1.distriktid == aData2.distriktid) &&
              (aData1.traktnr == aData2.traktnr) &&
              (aData1.standort == aData2.standort) &&
              (aData1.entreprenor == aData2.entreprenor) &&
              (aData1.rapport_typ == aData2.rapport_typ) &&
              (aData1.årtal == aData2.årtal));
    }

    private void LaddaRapportForm()
    {
      if (mAktivRapportForm != null)
      {
        //först kolla om samma typ av from redan finns inaddad
        var kdata = mAktivRapportForm.GetKORUSData();

        if (mReports[mDbIndex].data.rapport_typ != kdata.data.rapport_typ)
        {
          //det är en annan typ av rapport
          //stäng den redan befintliga rapporten
          Invoke(new MethodInvoker(mAktivRapportForm.Close));
          mAktivRapportForm = null;
        }
      }

      if (mAktivRapportForm == null)
      {
        switch (mReports[mDbIndex].data.rapport_typ)
        {
          case (int) RapportTyp.Markberedning:
            mAktivRapportForm = new MarkberedningForm(true);
            break;
          case (int) RapportTyp.Plantering:
            mAktivRapportForm = new PlanteringForm(true);
            break;
          case (int) RapportTyp.Gallring:
            mAktivRapportForm = new GallringForm(true);
            break;
          case (int) RapportTyp.Röjning:
            mAktivRapportForm = new RöjningForm(true);
            break;
          case (int) RapportTyp.Biobränsle:
            mAktivRapportForm = new BiobränsleForm(true);
            break;
          case (int) RapportTyp.Slutavverkning:
            mAktivRapportForm = new SlutavverkningForm(true);
            break;
        }

        if (mAktivRapportForm != null)
        {
          mAktivRapportForm.LaddaForm(null);
        }
      }

      if (mAktivRapportForm != null)
      {
        //uppdatera med det nya datat
        mAktivRapportForm.UppdateraData(mReports[mDbIndex]);
      }
    }

    private void buttonRedigeraData_Click(object sender, EventArgs e)
    {
      RedigeraRapport();
    }

    private void buttonTaBortData_Click(object sender, EventArgs e)
    {
      TabortRapport();
    }

    private void buttonUppdateraData_Click(object sender, EventArgs e)
    {
      StatusEventRefresh(this, null);
    }

    private void buttonExportera_Click(object sender, EventArgs e)
    {
      var button = sender as Button;
      if (button == null) return;

      contextMenuStripExportera.Show(button, new Point(button.Width, -button.Height));
    }

    private void ExporteraExcel2007()
    {
      LaddaRapportForm();

      if (mAktivRapportForm != null)
      {
        mAktivRapportForm.ExporteraExcel2007();
      }
    }

    private void ExporteraAdobePDF()
    {
      LaddaRapportForm();

      if (mAktivRapportForm != null)
      {
        mAktivRapportForm.ExporteraAdobePDF();
      }
    }

    private void toolStripMenuItemExcel2007_Click(object sender, EventArgs e)
    {
      ExporteraExcel2007();
    }

    private void toolStripMenuItemPdf_Click(object sender, EventArgs e)
    {
      ExporteraAdobePDF();
    }

    private void buttonRapport_Click(object sender, EventArgs e)
    {
      VisaRapport();
    }
  }
}