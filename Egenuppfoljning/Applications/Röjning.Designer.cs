﻿namespace Egenuppfoljning.Applications
{
  partial class RöjningForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RöjningForm));
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
        FastReport.Design.DesignerSettings designerSettings1 = new FastReport.Design.DesignerSettings();
        FastReport.Design.DesignerRestrictions designerRestrictions1 = new FastReport.Design.DesignerRestrictions();
        FastReport.Export.Email.EmailSettings emailSettings1 = new FastReport.Export.Email.EmailSettings();
        FastReport.PreviewSettings previewSettings1 = new FastReport.PreviewSettings();
        FastReport.ReportSettings reportSettings1 = new FastReport.ReportSettings();
        this.dataSetRöjning = new System.Data.DataSet();
        this.dataTableYta = new System.Data.DataTable();
        this.dataColumnYta = new System.Data.DataColumn();
        this.dataColumnTallAntal = new System.Data.DataColumn();
        this.dataColumnTallmedelhojd = new System.Data.DataColumn();
        this.dataColumnContAntal = new System.Data.DataColumn();
        this.dataColumnContMedelhojd = new System.Data.DataColumn();
        this.dataColumnGranAntal = new System.Data.DataColumn();
        this.dataColumnGranMedelhojd = new System.Data.DataColumn();
        this.dataColumnBjorkAntal = new System.Data.DataColumn();
        this.dataColumnBjorkMedelhojd = new System.Data.DataColumn();
        this.dataColumnHst = new System.Data.DataColumn();
        this.dataColumnMHojd = new System.Data.DataColumn();
        this.dataColumnRojstamAntal = new System.Data.DataColumn();
        this.dataTableUserData = new System.Data.DataTable();
        this.dataColumnRegionId = new System.Data.DataColumn();
        this.dataColumnRegion = new System.Data.DataColumn();
        this.dataColumnDistriktId = new System.Data.DataColumn();
        this.dataColumnDistrikt = new System.Data.DataColumn();
        this.dataColumnUrsprung = new System.Data.DataColumn();
        this.dataColumnDatum = new System.Data.DataColumn();
        this.dataColumnTraktnr = new System.Data.DataColumn();
        this.dataColumnTraktnamn = new System.Data.DataColumn();
        this.dataColumnStandort = new System.Data.DataColumn();
        this.dataColumnEntreprenor = new System.Data.DataColumn();
        this.dataColumnKommentar = new System.Data.DataColumn();
        this.dataColumnLanguage = new System.Data.DataColumn();
        this.dataColumnMailFrom = new System.Data.DataColumn();
        this.dataColumnProvytestorlek = new System.Data.DataColumn();
        this.dataColumnStatus = new System.Data.DataColumn();
        this.dataColumnStatus_Datum = new System.Data.DataColumn();
        this.dataColumnArtal = new System.Data.DataColumn();
        this.dataColumnMal = new System.Data.DataColumn();
        this.dataColumnInvTypId = new System.Data.DataColumn();
        this.dataColumnInvTyp = new System.Data.DataColumn();
        this.dataColumnAtgAreal = new System.Data.DataColumn();
        this.dataTableFragor = new System.Data.DataTable();
        this.dataColumnFraga1 = new System.Data.DataColumn();
        this.dataColumnFraga2 = new System.Data.DataColumn();
        this.dataColumnFraga3 = new System.Data.DataColumn();
        this.dataColumnFraga4 = new System.Data.DataColumn();
        this.dataColumnFraga5 = new System.Data.DataColumn();
        this.dataColumnFraga6 = new System.Data.DataColumn();
        this.dataColumnOvrigt = new System.Data.DataColumn();
        this.dataSetSettings = new System.Data.DataSet();
        this.dataTableRegion = new System.Data.DataTable();
        this.dataColumnSettingsRegionId = new System.Data.DataColumn();
        this.dataColumnSettingsRegionNamn = new System.Data.DataColumn();
        this.dataTableDistrikt = new System.Data.DataTable();
        this.dataColumnSettingsDistriktRegionId = new System.Data.DataColumn();
        this.dataColumnSettingsDistriktId = new System.Data.DataColumn();
        this.dataColumnSettingsDistriktNamn = new System.Data.DataColumn();
        this.dataTableMarkberedningsmetod = new System.Data.DataTable();
        this.dataColumnSettingsMarkberedningsmetodNamn = new System.Data.DataColumn();
        this.dataTableUrsprung = new System.Data.DataTable();
        this.dataColumndataColumnSettingsUrsprungNamn = new System.Data.DataColumn();
        this.dataTableInvTyp = new System.Data.DataTable();
        this.dataColumnSettingsInvTypId = new System.Data.DataColumn();
        this.dataColumnSettingsInvTypNamn = new System.Data.DataColumn();
        this.menuStripRöjning = new System.Windows.Forms.MenuStrip();
        this.arkivToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.sparaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.sparaSomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
        this.avslutaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.reportRöjning = new FastReport.Report();
        this.dataSetCalculations = new System.Data.DataSet();
        this.dataTableTableMedelvarde = new System.Data.DataTable();
        this.dataColumnTallMedelAntal = new System.Data.DataColumn();
        this.dataColumnTallMedelMHojd = new System.Data.DataColumn();
        this.dataColumnGranMedelAntal = new System.Data.DataColumn();
        this.dataColumnGranMedelMHojd = new System.Data.DataColumn();
        this.dataColumnBjorkMedelAntal = new System.Data.DataColumn();
        this.dataColumnBjorkMedelMHojd = new System.Data.DataColumn();
        this.dataColumnContMedelAntal = new System.Data.DataColumn();
        this.dataColumnContMedelMHojd = new System.Data.DataColumn();
        this.dataColumnHstMedelAntal = new System.Data.DataColumn();
        this.dataColumnBarrMedelMHojd = new System.Data.DataColumn();
        this.dataColumnRojstamMedelAntal = new System.Data.DataColumn();
        this.dataTableSumma = new System.Data.DataTable();
        this.dataColumnTallSummaAntal = new System.Data.DataColumn();
        this.dataColumnTallSummaMHojd = new System.Data.DataColumn();
        this.dataColumnGranSummaAntal = new System.Data.DataColumn();
        this.dataColumnGranSummaMHojd = new System.Data.DataColumn();
        this.dataColumnBjorkSummaAntal = new System.Data.DataColumn();
        this.dataColumnBjorkSummaMHojd = new System.Data.DataColumn();
        this.dataColumnContSummaAntal = new System.Data.DataColumn();
        this.dataColumnContSummaMHojd = new System.Data.DataColumn();
        this.dataColumnHstSummaAntal = new System.Data.DataColumn();
        this.dataColumnBarrHstMHojd = new System.Data.DataColumn();
        this.dataColumnRojstamSummaAntal = new System.Data.DataColumn();
        this.openReportFileDialog = new System.Windows.Forms.OpenFileDialog();
        this.tabControl1 = new System.Windows.Forms.TabControl();
        this.tabPageRöjning = new System.Windows.Forms.TabPage();
        this.groupBoxRöjning = new System.Windows.Forms.GroupBox();
        this.label1 = new System.Windows.Forms.Label();
        this.textBoxLovhuvudstammarsHojd = new System.Windows.Forms.TextBox();
        this.label5 = new System.Windows.Forms.Label();
        this.labelProcent = new System.Windows.Forms.Label();
        this.textBoxMåluppfyllelse = new System.Windows.Forms.TextBox();
        this.labelMåluppfyllelse = new System.Windows.Forms.Label();
        this.labelStHa = new System.Windows.Forms.Label();
        this.textBoxMål = new System.Windows.Forms.TextBox();
        this.labelMål = new System.Windows.Forms.Label();
        this.groupBox13 = new System.Windows.Forms.GroupBox();
        this.labelTrädslagsblandning = new System.Windows.Forms.Label();
        this.textBoxT = new System.Windows.Forms.TextBox();
        this.textBoxL = new System.Windows.Forms.TextBox();
        this.textBoxC = new System.Windows.Forms.TextBox();
        this.labelC = new System.Windows.Forms.Label();
        this.labelL = new System.Windows.Forms.Label();
        this.labelT = new System.Windows.Forms.Label();
        this.textBoxG = new System.Windows.Forms.TextBox();
        this.labelG = new System.Windows.Forms.Label();
        this.labelM = new System.Windows.Forms.Label();
        this.textBoxBarrhuvudstammarsHojd = new System.Windows.Forms.TextBox();
        this.textBoxProvytestorlekRadie = new System.Windows.Forms.TextBox();
        this.labelMRadie = new System.Windows.Forms.Label();
        this.labelSt = new System.Windows.Forms.Label();
        this.labelM2 = new System.Windows.Forms.Label();
        this.labelProvytestorlek = new System.Windows.Forms.Label();
        this.textBoxTotaltHuvudstammar = new System.Windows.Forms.TextBox();
        this.labelBarrhuvudstammarsMedelhöjd = new System.Windows.Forms.Label();
        this.labelTotaltHuvudstammar = new System.Windows.Forms.Label();
        this.textBoxProvytestorlek = new System.Windows.Forms.TextBox();
        this.groupBoxTrakt = new System.Windows.Forms.GroupBox();
        this.textBoxTraktDel = new System.Windows.Forms.TextBox();
        this.textBoxTraktnr = new System.Windows.Forms.TextBox();
        this.textBoxTraktnamn = new System.Windows.Forms.TextBox();
        this.textBoxEntreprenor = new System.Windows.Forms.TextBox();
        this.labeEntreprenor = new System.Windows.Forms.Label();
        this.labelTraktdel = new System.Windows.Forms.Label();
        this.labelTraktnamn = new System.Windows.Forms.Label();
        this.labelTraktnr = new System.Windows.Forms.Label();
        this.groupBoxRegion = new System.Windows.Forms.GroupBox();
        this.textBoxAtgAreal = new System.Windows.Forms.TextBox();
        this.labelAtgArel = new System.Windows.Forms.Label();
        this.comboBoxInvTyp = new System.Windows.Forms.ComboBox();
        this.labelInvTyp = new System.Windows.Forms.Label();
        this.comboBoxUrsprung = new System.Windows.Forms.ComboBox();
        this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
        this.comboBoxDistrikt = new System.Windows.Forms.ComboBox();
        this.comboBoxRegion = new System.Windows.Forms.ComboBox();
        this.labelDatum = new System.Windows.Forms.Label();
        this.labelUrsprung = new System.Windows.Forms.Label();
        this.labelDistrikt = new System.Windows.Forms.Label();
        this.labelRegion = new System.Windows.Forms.Label();
        this.tabPageYtor = new System.Windows.Forms.TabPage();
        this.groupBoxRöjningsstallen = new System.Windows.Forms.GroupBox();
        this.groupBox3 = new System.Windows.Forms.GroupBox();
        this.label6 = new System.Windows.Forms.Label();
        this.groupBox22 = new System.Windows.Forms.GroupBox();
        this.textBoxMedelvardeRojstamAntal = new System.Windows.Forms.TextBox();
        this.groupBox23 = new System.Windows.Forms.GroupBox();
        this.textBoxSummaRojstamAntal = new System.Windows.Forms.TextBox();
        this.groupBox7 = new System.Windows.Forms.GroupBox();
        this.label4 = new System.Windows.Forms.Label();
        this.groupBox15 = new System.Windows.Forms.GroupBox();
        this.textBoxMedelvardeBarrMHojd = new System.Windows.Forms.TextBox();
        this.groupBox4 = new System.Windows.Forms.GroupBox();
        this.label2 = new System.Windows.Forms.Label();
        this.groupBox14 = new System.Windows.Forms.GroupBox();
        this.textBoxSummaBarrMHojd = new System.Windows.Forms.TextBox();
        this.groupBox16 = new System.Windows.Forms.GroupBox();
        this.textBoxMedelvardeHst = new System.Windows.Forms.TextBox();
        this.groupBox12 = new System.Windows.Forms.GroupBox();
        this.textBoxSummaHst = new System.Windows.Forms.TextBox();
        this.groupBox11 = new System.Windows.Forms.GroupBox();
        this.textBoxSummaContMedelhojd = new System.Windows.Forms.TextBox();
        this.textBoxSummaContAntal = new System.Windows.Forms.TextBox();
        this.groupBox18 = new System.Windows.Forms.GroupBox();
        this.textBoxMedelvardeContMedelhojd = new System.Windows.Forms.TextBox();
        this.textBoxMedelvardeContAntal = new System.Windows.Forms.TextBox();
        this.groupBox10 = new System.Windows.Forms.GroupBox();
        this.textBoxSummaBjorkMedelhojd = new System.Windows.Forms.TextBox();
        this.textBoxSummaBjorkAntal = new System.Windows.Forms.TextBox();
        this.groupBox21 = new System.Windows.Forms.GroupBox();
        this.textBoxMedelvardeTallMedelhojd = new System.Windows.Forms.TextBox();
        this.textBoxMedelvardeTallAntal = new System.Windows.Forms.TextBox();
        this.groupBox19 = new System.Windows.Forms.GroupBox();
        this.textBoxMedelvardeBjorkMedelhojd = new System.Windows.Forms.TextBox();
        this.textBoxMedelvardeBjorkAntal = new System.Windows.Forms.TextBox();
        this.groupBox9 = new System.Windows.Forms.GroupBox();
        this.labelSummaAntal = new System.Windows.Forms.Label();
        this.groupBox20 = new System.Windows.Forms.GroupBox();
        this.textBoxMedelvardeGranMedelhojd = new System.Windows.Forms.TextBox();
        this.textBoxMedelvardeGranAntal = new System.Windows.Forms.TextBox();
        this.groupBox8 = new System.Windows.Forms.GroupBox();
        this.textBoxSummaGranMedelhojd = new System.Windows.Forms.TextBox();
        this.textBoxSummaGranAntal = new System.Windows.Forms.TextBox();
        this.groupBoxSummaTall = new System.Windows.Forms.GroupBox();
        this.textBoxSummaTallMedelhojd = new System.Windows.Forms.TextBox();
        this.textBoxSummaTallAntal = new System.Windows.Forms.TextBox();
        this.groupBox6 = new System.Windows.Forms.GroupBox();
        this.label3 = new System.Windows.Forms.Label();
        this.groupBox2 = new System.Windows.Forms.GroupBox();
        this.labelCont = new System.Windows.Forms.Label();
        this.groupBox5 = new System.Windows.Forms.GroupBox();
        this.labelGran = new System.Windows.Forms.Label();
        this.groupBox1 = new System.Windows.Forms.GroupBox();
        this.labelBjork = new System.Windows.Forms.Label();
        this.groupBoxBra = new System.Windows.Forms.GroupBox();
        this.labelTall = new System.Windows.Forms.Label();
        this.dataGridViewRöjningsstallen = new System.Windows.Forms.DataGridView();
        this.ytaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.tallAntalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.tallMedelhojdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.granAntalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.granMedelhojdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.bjorkAntalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.bjorkMedelhojdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.contAntalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.contMedelhojdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.hstAntalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.barrMedelhojdDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.RojstamAntalDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.tabPageFrågor = new System.Windows.Forms.TabPage();
        this.groupBoxKommentarer = new System.Windows.Forms.GroupBox();
        this.richTextBoxKommentar = new System.Windows.Forms.RichTextBox();
        this.labelNatur = new System.Windows.Forms.Label();
        this.groupBoxFråga6 = new System.Windows.Forms.GroupBox();
        this.radioButton6Ejaktuellt = new System.Windows.Forms.RadioButton();
        this.radioButton6Nej = new System.Windows.Forms.RadioButton();
        this.radioButton6Ja = new System.Windows.Forms.RadioButton();
        this.labelFrågor6 = new System.Windows.Forms.Label();
        this.groupBoxFråga5 = new System.Windows.Forms.GroupBox();
        this.radioButton5Ejaktuellt = new System.Windows.Forms.RadioButton();
        this.radioButton5Nej = new System.Windows.Forms.RadioButton();
        this.radioButton5Ja = new System.Windows.Forms.RadioButton();
        this.labelFrågor5 = new System.Windows.Forms.Label();
        this.groupBoxFråga4 = new System.Windows.Forms.GroupBox();
        this.radioButton4Ejaktuellt = new System.Windows.Forms.RadioButton();
        this.radioButton4Nej = new System.Windows.Forms.RadioButton();
        this.radioButton4Ja = new System.Windows.Forms.RadioButton();
        this.labelFrågor4 = new System.Windows.Forms.Label();
        this.groupBoxOBS = new System.Windows.Forms.GroupBox();
        this.labelFragaObs = new System.Windows.Forms.Label();
        this.labelOBS = new System.Windows.Forms.Label();
        this.groupBoxFråga3 = new System.Windows.Forms.GroupBox();
        this.radioButton3Ejaktuellt = new System.Windows.Forms.RadioButton();
        this.radioButton3Nej = new System.Windows.Forms.RadioButton();
        this.radioButton3Ja = new System.Windows.Forms.RadioButton();
        this.labelFrågor3 = new System.Windows.Forms.Label();
        this.groupBoxFråga2 = new System.Windows.Forms.GroupBox();
        this.radioButton2Ejaktuellt = new System.Windows.Forms.RadioButton();
        this.radioButton2Nej = new System.Windows.Forms.RadioButton();
        this.radioButton2Ja = new System.Windows.Forms.RadioButton();
        this.labelFrågor2 = new System.Windows.Forms.Label();
        this.groupBoxFråga1 = new System.Windows.Forms.GroupBox();
        this.radioButton1Nej = new System.Windows.Forms.RadioButton();
        this.radioButton1Ja = new System.Windows.Forms.RadioButton();
        this.labelFrågor1 = new System.Windows.Forms.Label();
        this.environmentRöjning = new FastReport.EnvironmentSettings();
        this.saveReportFolderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
        this.labelTotaltAntalYtor = new System.Windows.Forms.Label();
        this.labelProgress = new System.Windows.Forms.Label();
        this.progressBarData = new System.Windows.Forms.ProgressBar();
        this.textBoxRegion = new System.Windows.Forms.TextBox();
        this.textBoxDistrikt = new System.Windows.Forms.TextBox();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetRöjning)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableYta)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableUserData)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFragor)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetSettings)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableRegion)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableDistrikt)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableMarkberedningsmetod)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableUrsprung)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableInvTyp)).BeginInit();
        this.menuStripRöjning.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.reportRöjning)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetCalculations)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableTableMedelvarde)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableSumma)).BeginInit();
        this.tabControl1.SuspendLayout();
        this.tabPageRöjning.SuspendLayout();
        this.groupBoxRöjning.SuspendLayout();
        this.groupBox13.SuspendLayout();
        this.groupBoxTrakt.SuspendLayout();
        this.groupBoxRegion.SuspendLayout();
        this.tabPageYtor.SuspendLayout();
        this.groupBoxRöjningsstallen.SuspendLayout();
        this.groupBox3.SuspendLayout();
        this.groupBox22.SuspendLayout();
        this.groupBox23.SuspendLayout();
        this.groupBox7.SuspendLayout();
        this.groupBox15.SuspendLayout();
        this.groupBox4.SuspendLayout();
        this.groupBox14.SuspendLayout();
        this.groupBox16.SuspendLayout();
        this.groupBox12.SuspendLayout();
        this.groupBox11.SuspendLayout();
        this.groupBox18.SuspendLayout();
        this.groupBox10.SuspendLayout();
        this.groupBox21.SuspendLayout();
        this.groupBox19.SuspendLayout();
        this.groupBox9.SuspendLayout();
        this.groupBox20.SuspendLayout();
        this.groupBox8.SuspendLayout();
        this.groupBoxSummaTall.SuspendLayout();
        this.groupBox6.SuspendLayout();
        this.groupBox2.SuspendLayout();
        this.groupBox5.SuspendLayout();
        this.groupBox1.SuspendLayout();
        this.groupBoxBra.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRöjningsstallen)).BeginInit();
        this.tabPageFrågor.SuspendLayout();
        this.groupBoxKommentarer.SuspendLayout();
        this.groupBoxFråga6.SuspendLayout();
        this.groupBoxFråga5.SuspendLayout();
        this.groupBoxFråga4.SuspendLayout();
        this.groupBoxOBS.SuspendLayout();
        this.groupBoxFråga3.SuspendLayout();
        this.groupBoxFråga2.SuspendLayout();
        this.groupBoxFråga1.SuspendLayout();
        this.SuspendLayout();
        // 
        // dataSetRöjning
        // 
        this.dataSetRöjning.DataSetName = "DataSetRöjning";
        this.dataSetRöjning.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableYta,
            this.dataTableUserData,
            this.dataTableFragor});
        // 
        // dataTableYta
        // 
        this.dataTableYta.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnYta,
            this.dataColumnTallAntal,
            this.dataColumnTallmedelhojd,
            this.dataColumnContAntal,
            this.dataColumnContMedelhojd,
            this.dataColumnGranAntal,
            this.dataColumnGranMedelhojd,
            this.dataColumnBjorkAntal,
            this.dataColumnBjorkMedelhojd,
            this.dataColumnHst,
            this.dataColumnMHojd,
            this.dataColumnRojstamAntal});
        this.dataTableYta.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Yta"}, false)});
        this.dataTableYta.TableName = "Yta";
        // 
        // dataColumnYta
        // 
        this.dataColumnYta.AutoIncrementSeed = ((long)(2));
        this.dataColumnYta.ColumnName = "Yta";
        this.dataColumnYta.DataType = typeof(int);
        this.dataColumnYta.ReadOnly = true;
        // 
        // dataColumnTallAntal
        // 
        this.dataColumnTallAntal.ColumnName = "TallAntal";
        this.dataColumnTallAntal.DataType = typeof(int);
        // 
        // dataColumnTallmedelhojd
        // 
        this.dataColumnTallmedelhojd.ColumnName = "TallMedelhojd";
        this.dataColumnTallmedelhojd.DataType = typeof(double);
        // 
        // dataColumnContAntal
        // 
        this.dataColumnContAntal.ColumnName = "ContAntal";
        this.dataColumnContAntal.DataType = typeof(int);
        // 
        // dataColumnContMedelhojd
        // 
        this.dataColumnContMedelhojd.ColumnName = "ContMedelhojd";
        this.dataColumnContMedelhojd.DataType = typeof(double);
        // 
        // dataColumnGranAntal
        // 
        this.dataColumnGranAntal.ColumnName = "GranAntal";
        this.dataColumnGranAntal.DataType = typeof(int);
        // 
        // dataColumnGranMedelhojd
        // 
        this.dataColumnGranMedelhojd.ColumnName = "GranMedelhojd";
        this.dataColumnGranMedelhojd.DataType = typeof(double);
        // 
        // dataColumnBjorkAntal
        // 
        this.dataColumnBjorkAntal.ColumnName = "BjorkAntal";
        this.dataColumnBjorkAntal.DataType = typeof(int);
        // 
        // dataColumnBjorkMedelhojd
        // 
        this.dataColumnBjorkMedelhojd.ColumnName = "BjorkMedelhojd";
        this.dataColumnBjorkMedelhojd.DataType = typeof(double);
        // 
        // dataColumnHst
        // 
        this.dataColumnHst.ColumnName = "Hst";
        this.dataColumnHst.DataType = typeof(int);
        // 
        // dataColumnMHojd
        // 
        this.dataColumnMHojd.ColumnName = "MHojd";
        this.dataColumnMHojd.DataType = typeof(double);
        // 
        // dataColumnRojstamAntal
        // 
        this.dataColumnRojstamAntal.Caption = "Röjst";
        this.dataColumnRojstamAntal.ColumnName = "RojstamAntal";
        this.dataColumnRojstamAntal.DataType = typeof(int);
        // 
        // dataTableUserData
        // 
        this.dataTableUserData.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnRegionId,
            this.dataColumnRegion,
            this.dataColumnDistriktId,
            this.dataColumnDistrikt,
            this.dataColumnUrsprung,
            this.dataColumnDatum,
            this.dataColumnTraktnr,
            this.dataColumnTraktnamn,
            this.dataColumnStandort,
            this.dataColumnEntreprenor,
            this.dataColumnKommentar,
            this.dataColumnLanguage,
            this.dataColumnMailFrom,
            this.dataColumnProvytestorlek,
            this.dataColumnStatus,
            this.dataColumnStatus_Datum,
            this.dataColumnArtal,
            this.dataColumnMal,
            this.dataColumnInvTypId,
            this.dataColumnInvTyp,
            this.dataColumnAtgAreal});
        this.dataTableUserData.TableName = "UserData";
        // 
        // dataColumnRegionId
        // 
        this.dataColumnRegionId.ColumnName = "RegionId";
        this.dataColumnRegionId.DataType = typeof(int);
        // 
        // dataColumnRegion
        // 
        this.dataColumnRegion.ColumnName = "Region";
        // 
        // dataColumnDistriktId
        // 
        this.dataColumnDistriktId.ColumnName = "DistriktId";
        this.dataColumnDistriktId.DataType = typeof(int);
        // 
        // dataColumnDistrikt
        // 
        this.dataColumnDistrikt.ColumnName = "Distrikt";
        // 
        // dataColumnUrsprung
        // 
        this.dataColumnUrsprung.ColumnName = "Ursprung";
        // 
        // dataColumnDatum
        // 
        this.dataColumnDatum.ColumnName = "Datum";
        this.dataColumnDatum.DataType = typeof(System.DateTime);
        // 
        // dataColumnTraktnr
        // 
        this.dataColumnTraktnr.ColumnName = "Traktnr";
        this.dataColumnTraktnr.DataType = typeof(int);
        // 
        // dataColumnTraktnamn
        // 
        this.dataColumnTraktnamn.ColumnName = "Traktnamn";
        // 
        // dataColumnStandort
        // 
        this.dataColumnStandort.ColumnName = "Standort";
        this.dataColumnStandort.DataType = typeof(int);
        // 
        // dataColumnEntreprenor
        // 
        this.dataColumnEntreprenor.ColumnName = "Entreprenor";
        this.dataColumnEntreprenor.DataType = typeof(int);
        // 
        // dataColumnKommentar
        // 
        this.dataColumnKommentar.ColumnName = "Kommentar";
        // 
        // dataColumnLanguage
        // 
        this.dataColumnLanguage.ColumnName = "Language";
        // 
        // dataColumnMailFrom
        // 
        this.dataColumnMailFrom.ColumnName = "MailFrom";
        // 
        // dataColumnProvytestorlek
        // 
        this.dataColumnProvytestorlek.ColumnName = "Provytestorlek";
        this.dataColumnProvytestorlek.DataType = typeof(int);
        // 
        // dataColumnStatus
        // 
        this.dataColumnStatus.ColumnName = "Status";
        // 
        // dataColumnStatus_Datum
        // 
        this.dataColumnStatus_Datum.ColumnName = "Status_Datum";
        this.dataColumnStatus_Datum.DataType = typeof(System.DateTime);
        // 
        // dataColumnArtal
        // 
        this.dataColumnArtal.Caption = "Artal";
        this.dataColumnArtal.ColumnName = "Artal";
        this.dataColumnArtal.DataType = typeof(int);
        // 
        // dataColumnMal
        // 
        this.dataColumnMal.ColumnName = "Mal";
        this.dataColumnMal.DataType = typeof(int);
        // 
        // dataColumnInvTypId
        // 
        this.dataColumnInvTypId.ColumnName = "InvTypId";
        this.dataColumnInvTypId.DataType = typeof(int);
        // 
        // dataColumnInvTyp
        // 
        this.dataColumnInvTyp.ColumnName = "InvTyp";
        // 
        // dataColumnAtgAreal
        // 
        this.dataColumnAtgAreal.ColumnName = "AtgAreal";
        this.dataColumnAtgAreal.DataType = typeof(double);
        // 
        // dataTableFragor
        // 
        this.dataTableFragor.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFraga1,
            this.dataColumnFraga2,
            this.dataColumnFraga3,
            this.dataColumnFraga4,
            this.dataColumnFraga5,
            this.dataColumnFraga6,
            this.dataColumnOvrigt});
        this.dataTableFragor.TableName = "Fragor";
        // 
        // dataColumnFraga1
        // 
        this.dataColumnFraga1.Caption = "Fraga1";
        this.dataColumnFraga1.ColumnName = "Fraga1";
        // 
        // dataColumnFraga2
        // 
        this.dataColumnFraga2.Caption = "Fraga2";
        this.dataColumnFraga2.ColumnName = "Fraga2";
        // 
        // dataColumnFraga3
        // 
        this.dataColumnFraga3.ColumnName = "Fraga3";
        // 
        // dataColumnFraga4
        // 
        this.dataColumnFraga4.ColumnName = "Fraga4";
        // 
        // dataColumnFraga5
        // 
        this.dataColumnFraga5.ColumnName = "Fraga5";
        // 
        // dataColumnFraga6
        // 
        this.dataColumnFraga6.ColumnName = "Fraga6";
        // 
        // dataColumnOvrigt
        // 
        this.dataColumnOvrigt.ColumnName = "Ovrigt";
        // 
        // dataSetSettings
        // 
        this.dataSetSettings.DataSetName = "DataSetSettings";
        this.dataSetSettings.Relations.AddRange(new System.Data.DataRelation[] {
            new System.Data.DataRelation("RelationRegionDistrikt", "Region", "Distrikt", new string[] {
                        "Id"}, new string[] {
                        "RegionId"}, false)});
        this.dataSetSettings.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableRegion,
            this.dataTableDistrikt,
            this.dataTableMarkberedningsmetod,
            this.dataTableUrsprung,
            this.dataTableInvTyp});
        // 
        // dataTableRegion
        // 
        this.dataTableRegion.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSettingsRegionId,
            this.dataColumnSettingsRegionNamn});
        this.dataTableRegion.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, false)});
        this.dataTableRegion.TableName = "Region";
        // 
        // dataColumnSettingsRegionId
        // 
        this.dataColumnSettingsRegionId.ColumnName = "Id";
        this.dataColumnSettingsRegionId.DataType = typeof(int);
        // 
        // dataColumnSettingsRegionNamn
        // 
        this.dataColumnSettingsRegionNamn.ColumnName = "Namn";
        // 
        // dataTableDistrikt
        // 
        this.dataTableDistrikt.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSettingsDistriktRegionId,
            this.dataColumnSettingsDistriktId,
            this.dataColumnSettingsDistriktNamn});
        this.dataTableDistrikt.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.ForeignKeyConstraint("RelationRegionDistrikt", "Region", new string[] {
                        "Id"}, new string[] {
                        "RegionId"}, System.Data.AcceptRejectRule.None, System.Data.Rule.Cascade, System.Data.Rule.Cascade)});
        this.dataTableDistrikt.TableName = "Distrikt";
        // 
        // dataColumnSettingsDistriktRegionId
        // 
        this.dataColumnSettingsDistriktRegionId.ColumnName = "RegionId";
        this.dataColumnSettingsDistriktRegionId.DataType = typeof(int);
        // 
        // dataColumnSettingsDistriktId
        // 
        this.dataColumnSettingsDistriktId.ColumnName = "Id";
        this.dataColumnSettingsDistriktId.DataType = typeof(int);
        // 
        // dataColumnSettingsDistriktNamn
        // 
        this.dataColumnSettingsDistriktNamn.ColumnName = "Namn";
        // 
        // dataTableMarkberedningsmetod
        // 
        this.dataTableMarkberedningsmetod.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSettingsMarkberedningsmetodNamn});
        this.dataTableMarkberedningsmetod.TableName = "Markberedningsmetod";
        // 
        // dataColumnSettingsMarkberedningsmetodNamn
        // 
        this.dataColumnSettingsMarkberedningsmetodNamn.ColumnName = "Namn";
        // 
        // dataTableUrsprung
        // 
        this.dataTableUrsprung.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumndataColumnSettingsUrsprungNamn});
        this.dataTableUrsprung.TableName = "Ursprung";
        // 
        // dataColumndataColumnSettingsUrsprungNamn
        // 
        this.dataColumndataColumnSettingsUrsprungNamn.ColumnName = "Namn";
        // 
        // dataTableInvTyp
        // 
        this.dataTableInvTyp.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSettingsInvTypId,
            this.dataColumnSettingsInvTypNamn});
        this.dataTableInvTyp.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, false)});
        this.dataTableInvTyp.TableName = "InvTyp";
        // 
        // dataColumnSettingsInvTypId
        // 
        this.dataColumnSettingsInvTypId.ColumnName = "Id";
        this.dataColumnSettingsInvTypId.DataType = typeof(int);
        // 
        // dataColumnSettingsInvTypNamn
        // 
        this.dataColumnSettingsInvTypNamn.ColumnName = "Namn";
        // 
        // menuStripRöjning
        // 
        this.menuStripRöjning.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.arkivToolStripMenuItem});
        this.menuStripRöjning.Location = new System.Drawing.Point(0, 0);
        this.menuStripRöjning.Name = "menuStripRöjning";
        this.menuStripRöjning.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
        this.menuStripRöjning.Size = new System.Drawing.Size(801, 24);
        this.menuStripRöjning.TabIndex = 0;
        this.menuStripRöjning.Text = "menuStrip1";
        // 
        // arkivToolStripMenuItem
        // 
        this.arkivToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sparaToolStripMenuItem,
            this.sparaSomToolStripMenuItem,
            this.toolStripSeparator2,
            this.avslutaToolStripMenuItem});
        this.arkivToolStripMenuItem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.arkivToolStripMenuItem.Name = "arkivToolStripMenuItem";
        this.arkivToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
        this.arkivToolStripMenuItem.Text = global::Egenuppfoljning.Properties.Resources.Arkiv;
        // 
        // sparaToolStripMenuItem
        // 
        this.sparaToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("sparaToolStripMenuItem.Image")));
        this.sparaToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
        this.sparaToolStripMenuItem.Name = "sparaToolStripMenuItem";
        this.sparaToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
        this.sparaToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
        this.sparaToolStripMenuItem.Text = global::Egenuppfoljning.Properties.Resources.Spara;
        this.sparaToolStripMenuItem.Click += new System.EventHandler(this.sparaToolStripMenuItem_Click);
        // 
        // sparaSomToolStripMenuItem
        // 
        this.sparaSomToolStripMenuItem.Name = "sparaSomToolStripMenuItem";
        this.sparaSomToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
        this.sparaSomToolStripMenuItem.Text = global::Egenuppfoljning.Properties.Resources.SparaSom;
        this.sparaSomToolStripMenuItem.Click += new System.EventHandler(this.sparaSomToolStripMenuItem_Click);
        // 
        // toolStripSeparator2
        // 
        this.toolStripSeparator2.Name = "toolStripSeparator2";
        this.toolStripSeparator2.Size = new System.Drawing.Size(147, 6);
        // 
        // avslutaToolStripMenuItem
        // 
        this.avslutaToolStripMenuItem.Name = "avslutaToolStripMenuItem";
        this.avslutaToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
        this.avslutaToolStripMenuItem.Text = "Stäng";
        this.avslutaToolStripMenuItem.Click += new System.EventHandler(this.avslutaToolStripMenuItem_Click);
        // 
        // reportRöjning
        // 
        this.reportRöjning.ReportResourceString = resources.GetString("reportRöjning.ReportResourceString");
        this.reportRöjning.RegisterData(this.dataSetRöjning, "dataSetRöjning");
        // 
        // dataSetCalculations
        // 
        this.dataSetCalculations.DataSetName = "DataSetCalculations";
        this.dataSetCalculations.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableTableMedelvarde,
            this.dataTableSumma});
        // 
        // dataTableTableMedelvarde
        // 
        this.dataTableTableMedelvarde.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnTallMedelAntal,
            this.dataColumnTallMedelMHojd,
            this.dataColumnGranMedelAntal,
            this.dataColumnGranMedelMHojd,
            this.dataColumnBjorkMedelAntal,
            this.dataColumnBjorkMedelMHojd,
            this.dataColumnContMedelAntal,
            this.dataColumnContMedelMHojd,
            this.dataColumnHstMedelAntal,
            this.dataColumnBarrMedelMHojd,
            this.dataColumnRojstamMedelAntal});
        this.dataTableTableMedelvarde.TableName = "Medelvarde";
        // 
        // dataColumnTallMedelAntal
        // 
        this.dataColumnTallMedelAntal.ColumnName = "TallMedelAntal";
        this.dataColumnTallMedelAntal.DataType = typeof(double);
        // 
        // dataColumnTallMedelMHojd
        // 
        this.dataColumnTallMedelMHojd.ColumnName = "TallMedelMHojd";
        this.dataColumnTallMedelMHojd.DataType = typeof(double);
        // 
        // dataColumnGranMedelAntal
        // 
        this.dataColumnGranMedelAntal.ColumnName = "GranMedelAntal";
        this.dataColumnGranMedelAntal.DataType = typeof(double);
        // 
        // dataColumnGranMedelMHojd
        // 
        this.dataColumnGranMedelMHojd.ColumnName = "GranMedelMHojd";
        this.dataColumnGranMedelMHojd.DataType = typeof(double);
        // 
        // dataColumnBjorkMedelAntal
        // 
        this.dataColumnBjorkMedelAntal.ColumnName = "BjorkMedelAntal";
        this.dataColumnBjorkMedelAntal.DataType = typeof(double);
        // 
        // dataColumnBjorkMedelMHojd
        // 
        this.dataColumnBjorkMedelMHojd.ColumnName = "BjorkMedelMHojd";
        this.dataColumnBjorkMedelMHojd.DataType = typeof(double);
        // 
        // dataColumnContMedelAntal
        // 
        this.dataColumnContMedelAntal.ColumnName = "ContMedelAntal";
        this.dataColumnContMedelAntal.DataType = typeof(double);
        // 
        // dataColumnContMedelMHojd
        // 
        this.dataColumnContMedelMHojd.ColumnName = "ContMedelMHojd";
        this.dataColumnContMedelMHojd.DataType = typeof(double);
        // 
        // dataColumnHstMedelAntal
        // 
        this.dataColumnHstMedelAntal.ColumnName = "HstMedelAntal";
        this.dataColumnHstMedelAntal.DataType = typeof(double);
        // 
        // dataColumnBarrMedelMHojd
        // 
        this.dataColumnBarrMedelMHojd.ColumnName = "BarrMedelMHojd";
        this.dataColumnBarrMedelMHojd.DataType = typeof(double);
        // 
        // dataColumnRojstamMedelAntal
        // 
        this.dataColumnRojstamMedelAntal.ColumnName = "RojstamMedelAntal";
        this.dataColumnRojstamMedelAntal.DataType = typeof(double);
        // 
        // dataTableSumma
        // 
        this.dataTableSumma.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnTallSummaAntal,
            this.dataColumnTallSummaMHojd,
            this.dataColumnGranSummaAntal,
            this.dataColumnGranSummaMHojd,
            this.dataColumnBjorkSummaAntal,
            this.dataColumnBjorkSummaMHojd,
            this.dataColumnContSummaAntal,
            this.dataColumnContSummaMHojd,
            this.dataColumnHstSummaAntal,
            this.dataColumnBarrHstMHojd,
            this.dataColumnRojstamSummaAntal});
        this.dataTableSumma.TableName = "Summa";
        // 
        // dataColumnTallSummaAntal
        // 
        this.dataColumnTallSummaAntal.ColumnName = "TallSummaAntal";
        this.dataColumnTallSummaAntal.DataType = typeof(int);
        // 
        // dataColumnTallSummaMHojd
        // 
        this.dataColumnTallSummaMHojd.ColumnName = "TallSummaMHojd";
        this.dataColumnTallSummaMHojd.DataType = typeof(double);
        // 
        // dataColumnGranSummaAntal
        // 
        this.dataColumnGranSummaAntal.ColumnName = "GranSummaAntal";
        this.dataColumnGranSummaAntal.DataType = typeof(int);
        // 
        // dataColumnGranSummaMHojd
        // 
        this.dataColumnGranSummaMHojd.ColumnName = "GranSummaMHojd";
        this.dataColumnGranSummaMHojd.DataType = typeof(double);
        // 
        // dataColumnBjorkSummaAntal
        // 
        this.dataColumnBjorkSummaAntal.ColumnName = "BjorkSummaAntal";
        this.dataColumnBjorkSummaAntal.DataType = typeof(int);
        // 
        // dataColumnBjorkSummaMHojd
        // 
        this.dataColumnBjorkSummaMHojd.ColumnName = "BjorkSummaMHojd";
        this.dataColumnBjorkSummaMHojd.DataType = typeof(double);
        // 
        // dataColumnContSummaAntal
        // 
        this.dataColumnContSummaAntal.ColumnName = "ContSummaAntal";
        this.dataColumnContSummaAntal.DataType = typeof(int);
        // 
        // dataColumnContSummaMHojd
        // 
        this.dataColumnContSummaMHojd.ColumnName = "ContSummaMHojd";
        this.dataColumnContSummaMHojd.DataType = typeof(double);
        // 
        // dataColumnHstSummaAntal
        // 
        this.dataColumnHstSummaAntal.ColumnName = "HstSummaAntal";
        this.dataColumnHstSummaAntal.DataType = typeof(int);
        // 
        // dataColumnBarrHstMHojd
        // 
        this.dataColumnBarrHstMHojd.ColumnName = "BarrHstMHojd";
        this.dataColumnBarrHstMHojd.DataType = typeof(double);
        // 
        // dataColumnRojstamSummaAntal
        // 
        this.dataColumnRojstamSummaAntal.ColumnName = "RojstamSummaAntal";
        this.dataColumnRojstamSummaAntal.DataType = typeof(int);
        // 
        // tabControl1
        // 
        this.tabControl1.Controls.Add(this.tabPageRöjning);
        this.tabControl1.Controls.Add(this.tabPageYtor);
        this.tabControl1.Controls.Add(this.tabPageFrågor);
        this.tabControl1.Location = new System.Drawing.Point(0, 27);
        this.tabControl1.Name = "tabControl1";
        this.tabControl1.SelectedIndex = 0;
        this.tabControl1.Size = new System.Drawing.Size(800, 507);
        this.tabControl1.TabIndex = 1;
        // 
        // tabPageRöjning
        // 
        this.tabPageRöjning.Controls.Add(this.groupBoxRöjning);
        this.tabPageRöjning.Controls.Add(this.groupBoxTrakt);
        this.tabPageRöjning.Controls.Add(this.groupBoxRegion);
        this.tabPageRöjning.Location = new System.Drawing.Point(4, 22);
        this.tabPageRöjning.Name = "tabPageRöjning";
        this.tabPageRöjning.Size = new System.Drawing.Size(792, 481);
        this.tabPageRöjning.TabIndex = 2;
        this.tabPageRöjning.Text = "Röjning";
        this.tabPageRöjning.UseVisualStyleBackColor = true;
        // 
        // groupBoxRöjning
        // 
        this.groupBoxRöjning.Controls.Add(this.label1);
        this.groupBoxRöjning.Controls.Add(this.textBoxLovhuvudstammarsHojd);
        this.groupBoxRöjning.Controls.Add(this.label5);
        this.groupBoxRöjning.Controls.Add(this.labelProcent);
        this.groupBoxRöjning.Controls.Add(this.textBoxMåluppfyllelse);
        this.groupBoxRöjning.Controls.Add(this.labelMåluppfyllelse);
        this.groupBoxRöjning.Controls.Add(this.labelStHa);
        this.groupBoxRöjning.Controls.Add(this.textBoxMål);
        this.groupBoxRöjning.Controls.Add(this.labelMål);
        this.groupBoxRöjning.Controls.Add(this.groupBox13);
        this.groupBoxRöjning.Controls.Add(this.labelM);
        this.groupBoxRöjning.Controls.Add(this.textBoxBarrhuvudstammarsHojd);
        this.groupBoxRöjning.Controls.Add(this.textBoxProvytestorlekRadie);
        this.groupBoxRöjning.Controls.Add(this.labelMRadie);
        this.groupBoxRöjning.Controls.Add(this.labelSt);
        this.groupBoxRöjning.Controls.Add(this.labelM2);
        this.groupBoxRöjning.Controls.Add(this.labelProvytestorlek);
        this.groupBoxRöjning.Controls.Add(this.textBoxTotaltHuvudstammar);
        this.groupBoxRöjning.Controls.Add(this.labelBarrhuvudstammarsMedelhöjd);
        this.groupBoxRöjning.Controls.Add(this.labelTotaltHuvudstammar);
        this.groupBoxRöjning.Controls.Add(this.textBoxProvytestorlek);
        this.groupBoxRöjning.Location = new System.Drawing.Point(8, 176);
        this.groupBoxRöjning.Name = "groupBoxRöjning";
        this.groupBoxRöjning.Size = new System.Drawing.Size(544, 292);
        this.groupBoxRöjning.TabIndex = 2;
        this.groupBoxRöjning.TabStop = false;
        // 
        // label1
        // 
        this.label1.AutoSize = true;
        this.label1.Location = new System.Drawing.Point(363, 138);
        this.label1.Name = "label1";
        this.label1.Size = new System.Drawing.Size(18, 13);
        this.label1.TabIndex = 20;
        this.label1.Text = "m";
        // 
        // textBoxLovhuvudstammarsHojd
        // 
        this.textBoxLovhuvudstammarsHojd.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxLovhuvudstammarsHojd.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxLovhuvudstammarsHojd.Location = new System.Drawing.Point(297, 132);
        this.textBoxLovhuvudstammarsHojd.Name = "textBoxLovhuvudstammarsHojd";
        this.textBoxLovhuvudstammarsHojd.ReadOnly = true;
        this.textBoxLovhuvudstammarsHojd.Size = new System.Drawing.Size(63, 21);
        this.textBoxLovhuvudstammarsHojd.TabIndex = 19;
        this.textBoxLovhuvudstammarsHojd.TabStop = false;
        this.textBoxLovhuvudstammarsHojd.Text = "0";
        this.textBoxLovhuvudstammarsHojd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // label5
        // 
        this.label5.AutoSize = true;
        this.label5.Location = new System.Drawing.Point(12, 135);
        this.label5.Name = "label5";
        this.label5.Size = new System.Drawing.Size(183, 13);
        this.label5.TabIndex = 18;
        this.label5.Text = "Lövhuvudstammars medelhöjd";
        // 
        // labelProcent
        // 
        this.labelProcent.AutoSize = true;
        this.labelProcent.Location = new System.Drawing.Point(363, 160);
        this.labelProcent.Name = "labelProcent";
        this.labelProcent.Size = new System.Drawing.Size(20, 13);
        this.labelProcent.TabIndex = 16;
        this.labelProcent.Text = "%";
        // 
        // textBoxMåluppfyllelse
        // 
        this.textBoxMåluppfyllelse.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxMåluppfyllelse.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxMåluppfyllelse.Location = new System.Drawing.Point(297, 157);
        this.textBoxMåluppfyllelse.Name = "textBoxMåluppfyllelse";
        this.textBoxMåluppfyllelse.ReadOnly = true;
        this.textBoxMåluppfyllelse.Size = new System.Drawing.Size(63, 21);
        this.textBoxMåluppfyllelse.TabIndex = 15;
        this.textBoxMåluppfyllelse.TabStop = false;
        this.textBoxMåluppfyllelse.Text = "0";
        this.textBoxMåluppfyllelse.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // labelMåluppfyllelse
        // 
        this.labelMåluppfyllelse.AutoSize = true;
        this.labelMåluppfyllelse.Location = new System.Drawing.Point(12, 160);
        this.labelMåluppfyllelse.Name = "labelMåluppfyllelse";
        this.labelMåluppfyllelse.Size = new System.Drawing.Size(88, 13);
        this.labelMåluppfyllelse.TabIndex = 14;
        this.labelMåluppfyllelse.Text = "Måluppfyllelse";
        // 
        // labelStHa
        // 
        this.labelStHa.AutoSize = true;
        this.labelStHa.Location = new System.Drawing.Point(209, 23);
        this.labelStHa.Name = "labelStHa";
        this.labelStHa.Size = new System.Drawing.Size(38, 13);
        this.labelStHa.TabIndex = 13;
        this.labelStHa.Text = "st/ha";
        // 
        // textBoxMål
        // 
        this.textBoxMål.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetRöjning, "UserData.Mal", true));
        this.textBoxMål.Location = new System.Drawing.Point(145, 20);
        this.textBoxMål.MaxLength = 16;
        this.textBoxMål.Name = "textBoxMål";
        this.textBoxMål.Size = new System.Drawing.Size(63, 21);
        this.textBoxMål.TabIndex = 11;
        this.textBoxMål.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        this.textBoxMål.TextChanged += new System.EventHandler(this.textBoxMål_TextChanged);
        this.textBoxMål.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxMål_KeyDown);
        this.textBoxMål.Leave += new System.EventHandler(this.textBoxMål_Leave);
        // 
        // labelMål
        // 
        this.labelMål.AutoSize = true;
        this.labelMål.Location = new System.Drawing.Point(13, 23);
        this.labelMål.Name = "labelMål";
        this.labelMål.Size = new System.Drawing.Size(27, 13);
        this.labelMål.TabIndex = 11;
        this.labelMål.Text = "Mål";
        // 
        // groupBox13
        // 
        this.groupBox13.Controls.Add(this.labelTrädslagsblandning);
        this.groupBox13.Controls.Add(this.textBoxT);
        this.groupBox13.Controls.Add(this.textBoxL);
        this.groupBox13.Controls.Add(this.textBoxC);
        this.groupBox13.Controls.Add(this.labelC);
        this.groupBox13.Controls.Add(this.labelL);
        this.groupBox13.Controls.Add(this.labelT);
        this.groupBox13.Controls.Add(this.textBoxG);
        this.groupBox13.Controls.Add(this.labelG);
        this.groupBox13.Location = new System.Drawing.Point(0, 193);
        this.groupBox13.Name = "groupBox13";
        this.groupBox13.Size = new System.Drawing.Size(544, 99);
        this.groupBox13.TabIndex = 17;
        this.groupBox13.TabStop = false;
        // 
        // labelTrädslagsblandning
        // 
        this.labelTrädslagsblandning.AutoSize = true;
        this.labelTrädslagsblandning.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelTrädslagsblandning.Location = new System.Drawing.Point(188, 0);
        this.labelTrädslagsblandning.Name = "labelTrädslagsblandning";
        this.labelTrädslagsblandning.Size = new System.Drawing.Size(153, 13);
        this.labelTrädslagsblandning.TabIndex = 0;
        this.labelTrädslagsblandning.Text = "TRÄDSLAGSBLANDNING";
        // 
        // textBoxT
        // 
        this.textBoxT.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxT.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxT.Location = new System.Drawing.Point(39, 45);
        this.textBoxT.Name = "textBoxT";
        this.textBoxT.ReadOnly = true;
        this.textBoxT.Size = new System.Drawing.Size(63, 21);
        this.textBoxT.TabIndex = 2;
        this.textBoxT.TabStop = false;
        this.textBoxT.Text = "0";
        this.textBoxT.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        // 
        // textBoxL
        // 
        this.textBoxL.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxL.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxL.Location = new System.Drawing.Point(307, 45);
        this.textBoxL.Name = "textBoxL";
        this.textBoxL.ReadOnly = true;
        this.textBoxL.Size = new System.Drawing.Size(63, 21);
        this.textBoxL.TabIndex = 8;
        this.textBoxL.TabStop = false;
        this.textBoxL.Text = "0";
        this.textBoxL.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        // 
        // textBoxC
        // 
        this.textBoxC.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxC.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxC.Location = new System.Drawing.Point(434, 45);
        this.textBoxC.Name = "textBoxC";
        this.textBoxC.ReadOnly = true;
        this.textBoxC.Size = new System.Drawing.Size(63, 21);
        this.textBoxC.TabIndex = 4;
        this.textBoxC.TabStop = false;
        this.textBoxC.Text = "0";
        this.textBoxC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        // 
        // labelC
        // 
        this.labelC.AutoSize = true;
        this.labelC.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelC.Location = new System.Drawing.Point(456, 29);
        this.labelC.Name = "labelC";
        this.labelC.Size = new System.Drawing.Size(15, 13);
        this.labelC.TabIndex = 3;
        this.labelC.Text = "C";
        // 
        // labelL
        // 
        this.labelL.AutoSize = true;
        this.labelL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelL.Location = new System.Drawing.Point(331, 29);
        this.labelL.Name = "labelL";
        this.labelL.Size = new System.Drawing.Size(14, 13);
        this.labelL.TabIndex = 7;
        this.labelL.Text = "L";
        // 
        // labelT
        // 
        this.labelT.AutoSize = true;
        this.labelT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelT.Location = new System.Drawing.Point(63, 29);
        this.labelT.Name = "labelT";
        this.labelT.Size = new System.Drawing.Size(15, 13);
        this.labelT.TabIndex = 1;
        this.labelT.Text = "T";
        // 
        // textBoxG
        // 
        this.textBoxG.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxG.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxG.Location = new System.Drawing.Point(173, 45);
        this.textBoxG.Name = "textBoxG";
        this.textBoxG.ReadOnly = true;
        this.textBoxG.Size = new System.Drawing.Size(63, 21);
        this.textBoxG.TabIndex = 6;
        this.textBoxG.TabStop = false;
        this.textBoxG.Text = "0";
        this.textBoxG.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        // 
        // labelG
        // 
        this.labelG.AutoSize = true;
        this.labelG.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelG.Location = new System.Drawing.Point(195, 29);
        this.labelG.Name = "labelG";
        this.labelG.Size = new System.Drawing.Size(16, 13);
        this.labelG.TabIndex = 5;
        this.labelG.Text = "G";
        // 
        // labelM
        // 
        this.labelM.AutoSize = true;
        this.labelM.Location = new System.Drawing.Point(363, 113);
        this.labelM.Name = "labelM";
        this.labelM.Size = new System.Drawing.Size(18, 13);
        this.labelM.TabIndex = 2;
        this.labelM.Text = "m";
        // 
        // textBoxBarrhuvudstammarsHojd
        // 
        this.textBoxBarrhuvudstammarsHojd.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxBarrhuvudstammarsHojd.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxBarrhuvudstammarsHojd.Location = new System.Drawing.Point(297, 107);
        this.textBoxBarrhuvudstammarsHojd.Name = "textBoxBarrhuvudstammarsHojd";
        this.textBoxBarrhuvudstammarsHojd.ReadOnly = true;
        this.textBoxBarrhuvudstammarsHojd.Size = new System.Drawing.Size(63, 21);
        this.textBoxBarrhuvudstammarsHojd.TabIndex = 1;
        this.textBoxBarrhuvudstammarsHojd.TabStop = false;
        this.textBoxBarrhuvudstammarsHojd.Text = "0";
        this.textBoxBarrhuvudstammarsHojd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // textBoxProvytestorlekRadie
        // 
        this.textBoxProvytestorlekRadie.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxProvytestorlekRadie.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxProvytestorlekRadie.Location = new System.Drawing.Point(297, 51);
        this.textBoxProvytestorlekRadie.Name = "textBoxProvytestorlekRadie";
        this.textBoxProvytestorlekRadie.ReadOnly = true;
        this.textBoxProvytestorlekRadie.Size = new System.Drawing.Size(63, 21);
        this.textBoxProvytestorlekRadie.TabIndex = 9;
        this.textBoxProvytestorlekRadie.TabStop = false;
        this.textBoxProvytestorlekRadie.Text = "0";
        this.textBoxProvytestorlekRadie.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // labelMRadie
        // 
        this.labelMRadie.AutoSize = true;
        this.labelMRadie.Location = new System.Drawing.Point(363, 54);
        this.labelMRadie.Name = "labelMRadie";
        this.labelMRadie.Size = new System.Drawing.Size(50, 13);
        this.labelMRadie.TabIndex = 10;
        this.labelMRadie.Text = "m radie";
        // 
        // labelSt
        // 
        this.labelSt.AutoSize = true;
        this.labelSt.Location = new System.Drawing.Point(363, 87);
        this.labelSt.Name = "labelSt";
        this.labelSt.Size = new System.Drawing.Size(18, 13);
        this.labelSt.TabIndex = 5;
        this.labelSt.Text = "st";
        // 
        // labelM2
        // 
        this.labelM2.AutoSize = true;
        this.labelM2.Location = new System.Drawing.Point(211, 54);
        this.labelM2.Name = "labelM2";
        this.labelM2.Size = new System.Drawing.Size(25, 13);
        this.labelM2.TabIndex = 8;
        this.labelM2.Text = "m2";
        // 
        // labelProvytestorlek
        // 
        this.labelProvytestorlek.AutoSize = true;
        this.labelProvytestorlek.Location = new System.Drawing.Point(12, 54);
        this.labelProvytestorlek.Name = "labelProvytestorlek";
        this.labelProvytestorlek.Size = new System.Drawing.Size(92, 13);
        this.labelProvytestorlek.TabIndex = 6;
        this.labelProvytestorlek.Text = "Provytestorlek";
        // 
        // textBoxTotaltHuvudstammar
        // 
        this.textBoxTotaltHuvudstammar.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxTotaltHuvudstammar.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxTotaltHuvudstammar.Location = new System.Drawing.Point(297, 82);
        this.textBoxTotaltHuvudstammar.Name = "textBoxTotaltHuvudstammar";
        this.textBoxTotaltHuvudstammar.ReadOnly = true;
        this.textBoxTotaltHuvudstammar.Size = new System.Drawing.Size(63, 21);
        this.textBoxTotaltHuvudstammar.TabIndex = 4;
        this.textBoxTotaltHuvudstammar.TabStop = false;
        this.textBoxTotaltHuvudstammar.Text = "0";
        this.textBoxTotaltHuvudstammar.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // labelBarrhuvudstammarsMedelhöjd
        // 
        this.labelBarrhuvudstammarsMedelhöjd.AutoSize = true;
        this.labelBarrhuvudstammarsMedelhöjd.Location = new System.Drawing.Point(12, 110);
        this.labelBarrhuvudstammarsMedelhöjd.Name = "labelBarrhuvudstammarsMedelhöjd";
        this.labelBarrhuvudstammarsMedelhöjd.Size = new System.Drawing.Size(187, 13);
        this.labelBarrhuvudstammarsMedelhöjd.TabIndex = 0;
        this.labelBarrhuvudstammarsMedelhöjd.Text = "Barrhuvudstammars medelhöjd";
        // 
        // labelTotaltHuvudstammar
        // 
        this.labelTotaltHuvudstammar.AutoSize = true;
        this.labelTotaltHuvudstammar.Location = new System.Drawing.Point(11, 85);
        this.labelTotaltHuvudstammar.Name = "labelTotaltHuvudstammar";
        this.labelTotaltHuvudstammar.Size = new System.Drawing.Size(131, 13);
        this.labelTotaltHuvudstammar.TabIndex = 3;
        this.labelTotaltHuvudstammar.Text = "Totalt huvudstammar";
        // 
        // textBoxProvytestorlek
        // 
        this.textBoxProvytestorlek.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxProvytestorlek.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetRöjning, "UserData.Provytestorlek", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.textBoxProvytestorlek.Location = new System.Drawing.Point(145, 51);
        this.textBoxProvytestorlek.Name = "textBoxProvytestorlek";
        this.textBoxProvytestorlek.ReadOnly = true;
        this.textBoxProvytestorlek.Size = new System.Drawing.Size(63, 21);
        this.textBoxProvytestorlek.TabIndex = 7;
        this.textBoxProvytestorlek.TabStop = false;
        this.textBoxProvytestorlek.Text = "100";
        this.textBoxProvytestorlek.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        this.textBoxProvytestorlek.TextChanged += new System.EventHandler(this.textBoxProvytestorlek_TextChanged);
        this.textBoxProvytestorlek.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxProvytestorlek_KeyDown);
        this.textBoxProvytestorlek.Leave += new System.EventHandler(this.textBoxProvytestorlek_Leave);
        // 
        // groupBoxTrakt
        // 
        this.groupBoxTrakt.Controls.Add(this.textBoxTraktDel);
        this.groupBoxTrakt.Controls.Add(this.textBoxTraktnr);
        this.groupBoxTrakt.Controls.Add(this.textBoxTraktnamn);
        this.groupBoxTrakt.Controls.Add(this.textBoxEntreprenor);
        this.groupBoxTrakt.Controls.Add(this.labeEntreprenor);
        this.groupBoxTrakt.Controls.Add(this.labelTraktdel);
        this.groupBoxTrakt.Controls.Add(this.labelTraktnamn);
        this.groupBoxTrakt.Controls.Add(this.labelTraktnr);
        this.groupBoxTrakt.Location = new System.Drawing.Point(8, 119);
        this.groupBoxTrakt.Name = "groupBoxTrakt";
        this.groupBoxTrakt.Size = new System.Drawing.Size(544, 66);
        this.groupBoxTrakt.TabIndex = 1;
        this.groupBoxTrakt.TabStop = false;
        // 
        // textBoxTraktDel
        // 
        this.textBoxTraktDel.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetRöjning, "UserData.Standort", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, null, "0000"));
        this.textBoxTraktDel.Location = new System.Drawing.Point(264, 30);
        this.textBoxTraktDel.MaxLength = 4;
        this.textBoxTraktDel.Name = "textBoxTraktDel";
        this.textBoxTraktDel.Size = new System.Drawing.Size(116, 21);
        this.textBoxTraktDel.TabIndex = 9;
        this.textBoxTraktDel.TextChanged += new System.EventHandler(this.textBoxTraktDel_TextChanged);
        this.textBoxTraktDel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxTraktDel_KeyDown);
        this.textBoxTraktDel.Leave += new System.EventHandler(this.textBoxTraktDel_Leave);
        // 
        // textBoxTraktnr
        // 
        this.textBoxTraktnr.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetRöjning, "UserData.Traktnr", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, null, "000000"));
        this.textBoxTraktnr.Location = new System.Drawing.Point(15, 29);
        this.textBoxTraktnr.MaxLength = 6;
        this.textBoxTraktnr.Name = "textBoxTraktnr";
        this.textBoxTraktnr.Size = new System.Drawing.Size(116, 21);
        this.textBoxTraktnr.TabIndex = 7;
        this.textBoxTraktnr.TextChanged += new System.EventHandler(this.textBoxTraktnr_TextChanged);
        this.textBoxTraktnr.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxTraktnr_KeyDown);
        this.textBoxTraktnr.Leave += new System.EventHandler(this.textBoxTraktnr_Leave);
        // 
        // textBoxTraktnamn
        // 
        this.textBoxTraktnamn.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetRöjning, "UserData.Traktnamn", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.textBoxTraktnamn.Location = new System.Drawing.Point(140, 30);
        this.textBoxTraktnamn.MaxLength = 31;
        this.textBoxTraktnamn.Name = "textBoxTraktnamn";
        this.textBoxTraktnamn.Size = new System.Drawing.Size(116, 21);
        this.textBoxTraktnamn.TabIndex = 8;
        this.textBoxTraktnamn.TextChanged += new System.EventHandler(this.textBoxTraktnamn_TextChanged);
        // 
        // textBoxEntreprenor
        // 
        this.textBoxEntreprenor.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetRöjning, "UserData.Entreprenor", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, null, "0000"));
        this.textBoxEntreprenor.Location = new System.Drawing.Point(391, 30);
        this.textBoxEntreprenor.MaxLength = 4;
        this.textBoxEntreprenor.Name = "textBoxEntreprenor";
        this.textBoxEntreprenor.Size = new System.Drawing.Size(116, 21);
        this.textBoxEntreprenor.TabIndex = 10;
        this.textBoxEntreprenor.TextChanged += new System.EventHandler(this.textBoxEntreprenor_TextChanged);
        this.textBoxEntreprenor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxEntreprenor_KeyDown);
        this.textBoxEntreprenor.Leave += new System.EventHandler(this.textBoxEntreprenor_Leave);
        // 
        // labeEntreprenor
        // 
        this.labeEntreprenor.AutoSize = true;
        this.labeEntreprenor.Location = new System.Drawing.Point(391, 14);
        this.labeEntreprenor.Name = "labeEntreprenor";
        this.labeEntreprenor.Size = new System.Drawing.Size(145, 13);
        this.labeEntreprenor.TabIndex = 6;
        this.labeEntreprenor.Text = "Maskinnr/Entreprenörnr";
        // 
        // labelTraktdel
        // 
        this.labelTraktdel.AutoSize = true;
        this.labelTraktdel.Location = new System.Drawing.Point(264, 14);
        this.labelTraktdel.Name = "labelTraktdel";
        this.labelTraktdel.Size = new System.Drawing.Size(55, 13);
        this.labelTraktdel.TabIndex = 4;
        this.labelTraktdel.Text = "Traktdel";
        // 
        // labelTraktnamn
        // 
        this.labelTraktnamn.AutoSize = true;
        this.labelTraktnamn.Location = new System.Drawing.Point(140, 14);
        this.labelTraktnamn.Name = "labelTraktnamn";
        this.labelTraktnamn.Size = new System.Drawing.Size(70, 13);
        this.labelTraktnamn.TabIndex = 2;
        this.labelTraktnamn.Text = "Traktnamn";
        // 
        // labelTraktnr
        // 
        this.labelTraktnr.AutoSize = true;
        this.labelTraktnr.Location = new System.Drawing.Point(13, 14);
        this.labelTraktnr.Name = "labelTraktnr";
        this.labelTraktnr.Size = new System.Drawing.Size(50, 13);
        this.labelTraktnr.TabIndex = 0;
        this.labelTraktnr.Text = "Traktnr";
        // 
        // groupBoxRegion
        // 
        this.groupBoxRegion.Controls.Add(this.textBoxDistrikt);
        this.groupBoxRegion.Controls.Add(this.textBoxRegion);
        this.groupBoxRegion.Controls.Add(this.textBoxAtgAreal);
        this.groupBoxRegion.Controls.Add(this.labelAtgArel);
        this.groupBoxRegion.Controls.Add(this.comboBoxInvTyp);
        this.groupBoxRegion.Controls.Add(this.labelInvTyp);
        this.groupBoxRegion.Controls.Add(this.comboBoxUrsprung);
        this.groupBoxRegion.Controls.Add(this.dateTimePicker);
        this.groupBoxRegion.Controls.Add(this.comboBoxDistrikt);
        this.groupBoxRegion.Controls.Add(this.comboBoxRegion);
        this.groupBoxRegion.Controls.Add(this.labelDatum);
        this.groupBoxRegion.Controls.Add(this.labelUrsprung);
        this.groupBoxRegion.Controls.Add(this.labelDistrikt);
        this.groupBoxRegion.Controls.Add(this.labelRegion);
        this.groupBoxRegion.Location = new System.Drawing.Point(8, 3);
        this.groupBoxRegion.Name = "groupBoxRegion";
        this.groupBoxRegion.Size = new System.Drawing.Size(544, 110);
        this.groupBoxRegion.TabIndex = 0;
        this.groupBoxRegion.TabStop = false;
        // 
        // textBoxAtgAreal
        // 
        this.textBoxAtgAreal.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetRöjning, "UserData.AtgAreal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, null, "N1"));
        this.textBoxAtgAreal.Location = new System.Drawing.Point(140, 76);
        this.textBoxAtgAreal.MaxLength = 16;
        this.textBoxAtgAreal.Name = "textBoxAtgAreal";
        this.textBoxAtgAreal.Size = new System.Drawing.Size(116, 21);
        this.textBoxAtgAreal.TabIndex = 6;
        this.textBoxAtgAreal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        this.textBoxAtgAreal.TextChanged += new System.EventHandler(this.textBoxAtgAreal_TextChanged);
        this.textBoxAtgAreal.Validated += new System.EventHandler(this.textBoxAtgAreal_Validated);
        this.textBoxAtgAreal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxAtgAreal_KeyDown);
        this.textBoxAtgAreal.Leave += new System.EventHandler(this.textBoxAtgAreal_Leave);
        // 
        // labelAtgArel
        // 
        this.labelAtgArel.AutoSize = true;
        this.labelAtgArel.Location = new System.Drawing.Point(140, 57);
        this.labelAtgArel.Name = "labelAtgArel";
        this.labelAtgArel.Size = new System.Drawing.Size(59, 13);
        this.labelAtgArel.TabIndex = 10;
        this.labelAtgArel.Text = "Åtg areal";
        // 
        // comboBoxInvTyp
        // 
        this.comboBoxInvTyp.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetRöjning, "UserData.InvTyp", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.comboBoxInvTyp.DataSource = this.dataSetSettings;
        this.comboBoxInvTyp.DisplayMember = "InvTyp.Namn";
        this.comboBoxInvTyp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxInvTyp.FormattingEnabled = true;
        this.comboBoxInvTyp.Location = new System.Drawing.Point(14, 30);
        this.comboBoxInvTyp.Name = "comboBoxInvTyp";
        this.comboBoxInvTyp.Size = new System.Drawing.Size(116, 21);
        this.comboBoxInvTyp.TabIndex = 1;
        this.comboBoxInvTyp.ValueMember = "InvTyp.Id";
        this.comboBoxInvTyp.SelectionChangeCommitted += new System.EventHandler(this.comboBoxInvTyp_SelectionChangeCommitted);
        this.comboBoxInvTyp.SelectedIndexChanged += new System.EventHandler(this.comboBoxInvTyp_SelectedIndexChanged);
        // 
        // labelInvTyp
        // 
        this.labelInvTyp.AutoSize = true;
        this.labelInvTyp.Location = new System.Drawing.Point(14, 14);
        this.labelInvTyp.Name = "labelInvTyp";
        this.labelInvTyp.Size = new System.Drawing.Size(47, 13);
        this.labelInvTyp.TabIndex = 8;
        this.labelInvTyp.Text = "InvTyp";
        // 
        // comboBoxUrsprung
        // 
        this.comboBoxUrsprung.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetRöjning, "UserData.Ursprung", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.comboBoxUrsprung.DataSource = this.dataSetSettings;
        this.comboBoxUrsprung.DisplayMember = "Ursprung.Namn";
        this.comboBoxUrsprung.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxUrsprung.FormattingEnabled = true;
        this.comboBoxUrsprung.Location = new System.Drawing.Point(391, 30);
        this.comboBoxUrsprung.Name = "comboBoxUrsprung";
        this.comboBoxUrsprung.Size = new System.Drawing.Size(116, 21);
        this.comboBoxUrsprung.TabIndex = 4;
        this.comboBoxUrsprung.SelectionChangeCommitted += new System.EventHandler(this.comboBoxUrsprung_SelectionChangeCommitted);
        this.comboBoxUrsprung.SelectedIndexChanged += new System.EventHandler(this.comboBoxUrsprung_SelectedIndexChanged);
        // 
        // dateTimePicker
        // 
        this.dateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.dataSetRöjning, "UserData.Datum", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.dateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
        this.dateTimePicker.Location = new System.Drawing.Point(13, 76);
        this.dateTimePicker.Name = "dateTimePicker";
        this.dateTimePicker.Size = new System.Drawing.Size(116, 21);
        this.dateTimePicker.TabIndex = 5;
        this.dateTimePicker.ValueChanged += new System.EventHandler(this.dateTimePicker_ValueChanged);
        // 
        // comboBoxDistrikt
        // 
        this.comboBoxDistrikt.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetRöjning, "UserData.Distrikt", true));
        this.comboBoxDistrikt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxDistrikt.FormattingEnabled = true;
        this.comboBoxDistrikt.Location = new System.Drawing.Point(264, 30);
        this.comboBoxDistrikt.Name = "comboBoxDistrikt";
        this.comboBoxDistrikt.Size = new System.Drawing.Size(116, 21);
        this.comboBoxDistrikt.TabIndex = 3;
        this.comboBoxDistrikt.SelectionChangeCommitted += new System.EventHandler(this.comboBoxDistrikt_SelectionChangeCommitted);
        this.comboBoxDistrikt.SelectedIndexChanged += new System.EventHandler(this.comboBoxDistrikt_SelectedIndexChanged);
        // 
        // comboBoxRegion
        // 
        this.comboBoxRegion.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetRöjning, "UserData.Region", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.comboBoxRegion.DataSource = this.dataSetSettings;
        this.comboBoxRegion.DisplayMember = "Region.Namn";
        this.comboBoxRegion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxRegion.FormattingEnabled = true;
        this.comboBoxRegion.Location = new System.Drawing.Point(140, 30);
        this.comboBoxRegion.Name = "comboBoxRegion";
        this.comboBoxRegion.Size = new System.Drawing.Size(116, 21);
        this.comboBoxRegion.TabIndex = 2;
        this.comboBoxRegion.ValueMember = "Region.Id";
        this.comboBoxRegion.SelectionChangeCommitted += new System.EventHandler(this.comboBoxRegion_SelectionChangeCommitted);
        this.comboBoxRegion.SelectedIndexChanged += new System.EventHandler(this.comboBoxRegion_SelectedIndexChanged);
        // 
        // labelDatum
        // 
        this.labelDatum.AutoSize = true;
        this.labelDatum.Location = new System.Drawing.Point(13, 57);
        this.labelDatum.Name = "labelDatum";
        this.labelDatum.Size = new System.Drawing.Size(45, 13);
        this.labelDatum.TabIndex = 6;
        this.labelDatum.Text = "Datum";
        // 
        // labelUrsprung
        // 
        this.labelUrsprung.AutoSize = true;
        this.labelUrsprung.Location = new System.Drawing.Point(391, 14);
        this.labelUrsprung.Name = "labelUrsprung";
        this.labelUrsprung.Size = new System.Drawing.Size(59, 13);
        this.labelUrsprung.TabIndex = 4;
        this.labelUrsprung.Text = "Ursprung";
        // 
        // labelDistrikt
        // 
        this.labelDistrikt.AutoSize = true;
        this.labelDistrikt.Location = new System.Drawing.Point(264, 14);
        this.labelDistrikt.Name = "labelDistrikt";
        this.labelDistrikt.Size = new System.Drawing.Size(49, 13);
        this.labelDistrikt.TabIndex = 2;
        this.labelDistrikt.Text = "Distrikt";
        // 
        // labelRegion
        // 
        this.labelRegion.AutoSize = true;
        this.labelRegion.Location = new System.Drawing.Point(140, 14);
        this.labelRegion.Name = "labelRegion";
        this.labelRegion.Size = new System.Drawing.Size(46, 13);
        this.labelRegion.TabIndex = 0;
        this.labelRegion.Text = "Region";
        // 
        // tabPageYtor
        // 
        this.tabPageYtor.Controls.Add(this.groupBoxRöjningsstallen);
        this.tabPageYtor.Location = new System.Drawing.Point(4, 22);
        this.tabPageYtor.Name = "tabPageYtor";
        this.tabPageYtor.Padding = new System.Windows.Forms.Padding(3);
        this.tabPageYtor.Size = new System.Drawing.Size(792, 481);
        this.tabPageYtor.TabIndex = 0;
        this.tabPageYtor.Text = "Ytor";
        this.tabPageYtor.UseVisualStyleBackColor = true;
        // 
        // groupBoxRöjningsstallen
        // 
        this.groupBoxRöjningsstallen.Controls.Add(this.groupBox3);
        this.groupBoxRöjningsstallen.Controls.Add(this.groupBox22);
        this.groupBoxRöjningsstallen.Controls.Add(this.groupBox23);
        this.groupBoxRöjningsstallen.Controls.Add(this.groupBox7);
        this.groupBoxRöjningsstallen.Controls.Add(this.groupBox15);
        this.groupBoxRöjningsstallen.Controls.Add(this.groupBox4);
        this.groupBoxRöjningsstallen.Controls.Add(this.groupBox14);
        this.groupBoxRöjningsstallen.Controls.Add(this.groupBox16);
        this.groupBoxRöjningsstallen.Controls.Add(this.groupBox12);
        this.groupBoxRöjningsstallen.Controls.Add(this.groupBox11);
        this.groupBoxRöjningsstallen.Controls.Add(this.groupBox18);
        this.groupBoxRöjningsstallen.Controls.Add(this.groupBox10);
        this.groupBoxRöjningsstallen.Controls.Add(this.groupBox21);
        this.groupBoxRöjningsstallen.Controls.Add(this.groupBox19);
        this.groupBoxRöjningsstallen.Controls.Add(this.groupBox9);
        this.groupBoxRöjningsstallen.Controls.Add(this.groupBox20);
        this.groupBoxRöjningsstallen.Controls.Add(this.groupBox8);
        this.groupBoxRöjningsstallen.Controls.Add(this.groupBoxSummaTall);
        this.groupBoxRöjningsstallen.Controls.Add(this.groupBox6);
        this.groupBoxRöjningsstallen.Controls.Add(this.groupBox2);
        this.groupBoxRöjningsstallen.Controls.Add(this.groupBox5);
        this.groupBoxRöjningsstallen.Controls.Add(this.groupBox1);
        this.groupBoxRöjningsstallen.Controls.Add(this.groupBoxBra);
        this.groupBoxRöjningsstallen.Controls.Add(this.dataGridViewRöjningsstallen);
        this.groupBoxRöjningsstallen.Location = new System.Drawing.Point(8, 6);
        this.groupBoxRöjningsstallen.Name = "groupBoxRöjningsstallen";
        this.groupBoxRöjningsstallen.Size = new System.Drawing.Size(778, 469);
        this.groupBoxRöjningsstallen.TabIndex = 0;
        this.groupBoxRöjningsstallen.TabStop = false;
        this.groupBoxRöjningsstallen.Text = "ANTAL HUVUDSTAMMAR";
        // 
        // groupBox3
        // 
        this.groupBox3.BackColor = System.Drawing.SystemColors.Control;
        this.groupBox3.Controls.Add(this.label6);
        this.groupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.groupBox3.Location = new System.Drawing.Point(586, 18);
        this.groupBox3.Name = "groupBox3";
        this.groupBox3.Size = new System.Drawing.Size(62, 36);
        this.groupBox3.TabIndex = 33;
        this.groupBox3.TabStop = false;
        // 
        // label6
        // 
        this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label6.Location = new System.Drawing.Point(5, 14);
        this.label6.Name = "label6";
        this.label6.Size = new System.Drawing.Size(52, 19);
        this.label6.TabIndex = 0;
        this.label6.Text = "h-st";
        this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
        // 
        // groupBox22
        // 
        this.groupBox22.Controls.Add(this.textBoxMedelvardeRojstamAntal);
        this.groupBox22.Location = new System.Drawing.Point(712, 412);
        this.groupBox22.Name = "groupBox22";
        this.groupBox22.Size = new System.Drawing.Size(63, 46);
        this.groupBox22.TabIndex = 32;
        this.groupBox22.TabStop = false;
        // 
        // textBoxMedelvardeRojstamAntal
        // 
        this.textBoxMedelvardeRojstamAntal.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxMedelvardeRojstamAntal.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxMedelvardeRojstamAntal.Location = new System.Drawing.Point(4, 15);
        this.textBoxMedelvardeRojstamAntal.Name = "textBoxMedelvardeRojstamAntal";
        this.textBoxMedelvardeRojstamAntal.ReadOnly = true;
        this.textBoxMedelvardeRojstamAntal.Size = new System.Drawing.Size(55, 21);
        this.textBoxMedelvardeRojstamAntal.TabIndex = 1;
        this.textBoxMedelvardeRojstamAntal.TabStop = false;
        this.textBoxMedelvardeRojstamAntal.Text = "0";
        this.textBoxMedelvardeRojstamAntal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        // 
        // groupBox23
        // 
        this.groupBox23.Controls.Add(this.textBoxSummaRojstamAntal);
        this.groupBox23.Location = new System.Drawing.Point(712, 372);
        this.groupBox23.Name = "groupBox23";
        this.groupBox23.Size = new System.Drawing.Size(63, 48);
        this.groupBox23.TabIndex = 31;
        this.groupBox23.TabStop = false;
        this.groupBox23.Text = "Röjst.";
        // 
        // textBoxSummaRojstamAntal
        // 
        this.textBoxSummaRojstamAntal.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxSummaRojstamAntal.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxSummaRojstamAntal.Location = new System.Drawing.Point(4, 19);
        this.textBoxSummaRojstamAntal.Name = "textBoxSummaRojstamAntal";
        this.textBoxSummaRojstamAntal.ReadOnly = true;
        this.textBoxSummaRojstamAntal.Size = new System.Drawing.Size(55, 21);
        this.textBoxSummaRojstamAntal.TabIndex = 1;
        this.textBoxSummaRojstamAntal.TabStop = false;
        this.textBoxSummaRojstamAntal.Text = "0";
        this.textBoxSummaRojstamAntal.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        // 
        // groupBox7
        // 
        this.groupBox7.BackColor = System.Drawing.SystemColors.Control;
        this.groupBox7.Controls.Add(this.label4);
        this.groupBox7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.groupBox7.Location = new System.Drawing.Point(706, 18);
        this.groupBox7.Name = "groupBox7";
        this.groupBox7.Size = new System.Drawing.Size(60, 36);
        this.groupBox7.TabIndex = 30;
        this.groupBox7.TabStop = false;
        // 
        // label4
        // 
        this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label4.Location = new System.Drawing.Point(13, 14);
        this.label4.Name = "label4";
        this.label4.Size = new System.Drawing.Size(41, 19);
        this.label4.TabIndex = 0;
        this.label4.Text = "Röjst";
        // 
        // groupBox15
        // 
        this.groupBox15.Controls.Add(this.textBoxMedelvardeBarrMHojd);
        this.groupBox15.Location = new System.Drawing.Point(648, 412);
        this.groupBox15.Name = "groupBox15";
        this.groupBox15.Size = new System.Drawing.Size(69, 46);
        this.groupBox15.TabIndex = 29;
        this.groupBox15.TabStop = false;
        // 
        // textBoxMedelvardeBarrMHojd
        // 
        this.textBoxMedelvardeBarrMHojd.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxMedelvardeBarrMHojd.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxMedelvardeBarrMHojd.Location = new System.Drawing.Point(4, 14);
        this.textBoxMedelvardeBarrMHojd.Name = "textBoxMedelvardeBarrMHojd";
        this.textBoxMedelvardeBarrMHojd.ReadOnly = true;
        this.textBoxMedelvardeBarrMHojd.Size = new System.Drawing.Size(55, 21);
        this.textBoxMedelvardeBarrMHojd.TabIndex = 1;
        this.textBoxMedelvardeBarrMHojd.TabStop = false;
        this.textBoxMedelvardeBarrMHojd.Text = "0";
        // 
        // groupBox4
        // 
        this.groupBox4.Controls.Add(this.label2);
        this.groupBox4.Location = new System.Drawing.Point(16, 412);
        this.groupBox4.Name = "groupBox4";
        this.groupBox4.Size = new System.Drawing.Size(90, 46);
        this.groupBox4.TabIndex = 21;
        this.groupBox4.TabStop = false;
        // 
        // label2
        // 
        this.label2.AutoSize = true;
        this.label2.Location = new System.Drawing.Point(3, 21);
        this.label2.Name = "label2";
        this.label2.Size = new System.Drawing.Size(83, 13);
        this.label2.TabIndex = 0;
        this.label2.Text = "MEDELVÄRDE:";
        // 
        // groupBox14
        // 
        this.groupBox14.Controls.Add(this.textBoxSummaBarrMHojd);
        this.groupBox14.Location = new System.Drawing.Point(648, 372);
        this.groupBox14.Name = "groupBox14";
        this.groupBox14.Size = new System.Drawing.Size(79, 48);
        this.groupBox14.TabIndex = 23;
        this.groupBox14.TabStop = false;
        this.groupBox14.Text = "Barr h-st";
        // 
        // textBoxSummaBarrMHojd
        // 
        this.textBoxSummaBarrMHojd.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxSummaBarrMHojd.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxSummaBarrMHojd.Location = new System.Drawing.Point(4, 18);
        this.textBoxSummaBarrMHojd.Name = "textBoxSummaBarrMHojd";
        this.textBoxSummaBarrMHojd.ReadOnly = true;
        this.textBoxSummaBarrMHojd.Size = new System.Drawing.Size(55, 21);
        this.textBoxSummaBarrMHojd.TabIndex = 1;
        this.textBoxSummaBarrMHojd.TabStop = false;
        this.textBoxSummaBarrMHojd.Text = "0";
        // 
        // groupBox16
        // 
        this.groupBox16.Controls.Add(this.textBoxMedelvardeHst);
        this.groupBox16.Location = new System.Drawing.Point(586, 412);
        this.groupBox16.Name = "groupBox16";
        this.groupBox16.Size = new System.Drawing.Size(64, 46);
        this.groupBox16.TabIndex = 28;
        this.groupBox16.TabStop = false;
        // 
        // textBoxMedelvardeHst
        // 
        this.textBoxMedelvardeHst.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxMedelvardeHst.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxMedelvardeHst.Location = new System.Drawing.Point(5, 14);
        this.textBoxMedelvardeHst.Name = "textBoxMedelvardeHst";
        this.textBoxMedelvardeHst.ReadOnly = true;
        this.textBoxMedelvardeHst.Size = new System.Drawing.Size(55, 21);
        this.textBoxMedelvardeHst.TabIndex = 0;
        this.textBoxMedelvardeHst.TabStop = false;
        this.textBoxMedelvardeHst.Text = "0";
        // 
        // groupBox12
        // 
        this.groupBox12.Controls.Add(this.textBoxSummaHst);
        this.groupBox12.Location = new System.Drawing.Point(586, 372);
        this.groupBox12.Name = "groupBox12";
        this.groupBox12.Size = new System.Drawing.Size(64, 48);
        this.groupBox12.TabIndex = 22;
        this.groupBox12.TabStop = false;
        this.groupBox12.Text = "h-st";
        // 
        // textBoxSummaHst
        // 
        this.textBoxSummaHst.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxSummaHst.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxSummaHst.Location = new System.Drawing.Point(5, 18);
        this.textBoxSummaHst.Name = "textBoxSummaHst";
        this.textBoxSummaHst.ReadOnly = true;
        this.textBoxSummaHst.Size = new System.Drawing.Size(55, 21);
        this.textBoxSummaHst.TabIndex = 0;
        this.textBoxSummaHst.TabStop = false;
        this.textBoxSummaHst.Text = "0";
        // 
        // groupBox11
        // 
        this.groupBox11.Controls.Add(this.textBoxSummaContMedelhojd);
        this.groupBox11.Controls.Add(this.textBoxSummaContAntal);
        this.groupBox11.Location = new System.Drawing.Point(468, 372);
        this.groupBox11.Name = "groupBox11";
        this.groupBox11.Size = new System.Drawing.Size(120, 48);
        this.groupBox11.TabIndex = 20;
        this.groupBox11.TabStop = false;
        this.groupBox11.Text = "Cont";
        // 
        // textBoxSummaContMedelhojd
        // 
        this.textBoxSummaContMedelhojd.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxSummaContMedelhojd.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxSummaContMedelhojd.Location = new System.Drawing.Point(62, 18);
        this.textBoxSummaContMedelhojd.Name = "textBoxSummaContMedelhojd";
        this.textBoxSummaContMedelhojd.ReadOnly = true;
        this.textBoxSummaContMedelhojd.Size = new System.Drawing.Size(55, 21);
        this.textBoxSummaContMedelhojd.TabIndex = 1;
        this.textBoxSummaContMedelhojd.TabStop = false;
        this.textBoxSummaContMedelhojd.Text = "0";
        // 
        // textBoxSummaContAntal
        // 
        this.textBoxSummaContAntal.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxSummaContAntal.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxSummaContAntal.Location = new System.Drawing.Point(4, 18);
        this.textBoxSummaContAntal.Name = "textBoxSummaContAntal";
        this.textBoxSummaContAntal.ReadOnly = true;
        this.textBoxSummaContAntal.Size = new System.Drawing.Size(55, 21);
        this.textBoxSummaContAntal.TabIndex = 0;
        this.textBoxSummaContAntal.TabStop = false;
        this.textBoxSummaContAntal.Text = "0";
        // 
        // groupBox18
        // 
        this.groupBox18.Controls.Add(this.textBoxMedelvardeContMedelhojd);
        this.groupBox18.Controls.Add(this.textBoxMedelvardeContAntal);
        this.groupBox18.Location = new System.Drawing.Point(468, 412);
        this.groupBox18.Name = "groupBox18";
        this.groupBox18.Size = new System.Drawing.Size(120, 46);
        this.groupBox18.TabIndex = 27;
        this.groupBox18.TabStop = false;
        // 
        // textBoxMedelvardeContMedelhojd
        // 
        this.textBoxMedelvardeContMedelhojd.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxMedelvardeContMedelhojd.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxMedelvardeContMedelhojd.Location = new System.Drawing.Point(62, 16);
        this.textBoxMedelvardeContMedelhojd.Name = "textBoxMedelvardeContMedelhojd";
        this.textBoxMedelvardeContMedelhojd.ReadOnly = true;
        this.textBoxMedelvardeContMedelhojd.Size = new System.Drawing.Size(55, 21);
        this.textBoxMedelvardeContMedelhojd.TabIndex = 1;
        this.textBoxMedelvardeContMedelhojd.TabStop = false;
        this.textBoxMedelvardeContMedelhojd.Text = "0";
        // 
        // textBoxMedelvardeContAntal
        // 
        this.textBoxMedelvardeContAntal.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxMedelvardeContAntal.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxMedelvardeContAntal.Location = new System.Drawing.Point(4, 16);
        this.textBoxMedelvardeContAntal.Name = "textBoxMedelvardeContAntal";
        this.textBoxMedelvardeContAntal.ReadOnly = true;
        this.textBoxMedelvardeContAntal.Size = new System.Drawing.Size(55, 21);
        this.textBoxMedelvardeContAntal.TabIndex = 0;
        this.textBoxMedelvardeContAntal.TabStop = false;
        this.textBoxMedelvardeContAntal.Text = "0";
        // 
        // groupBox10
        // 
        this.groupBox10.Controls.Add(this.textBoxSummaBjorkMedelhojd);
        this.groupBox10.Controls.Add(this.textBoxSummaBjorkAntal);
        this.groupBox10.Location = new System.Drawing.Point(348, 372);
        this.groupBox10.Name = "groupBox10";
        this.groupBox10.Size = new System.Drawing.Size(120, 48);
        this.groupBox10.TabIndex = 18;
        this.groupBox10.TabStop = false;
        this.groupBox10.Text = "Björk/övrig löv";
        // 
        // textBoxSummaBjorkMedelhojd
        // 
        this.textBoxSummaBjorkMedelhojd.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxSummaBjorkMedelhojd.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxSummaBjorkMedelhojd.Location = new System.Drawing.Point(61, 17);
        this.textBoxSummaBjorkMedelhojd.Name = "textBoxSummaBjorkMedelhojd";
        this.textBoxSummaBjorkMedelhojd.ReadOnly = true;
        this.textBoxSummaBjorkMedelhojd.Size = new System.Drawing.Size(55, 21);
        this.textBoxSummaBjorkMedelhojd.TabIndex = 1;
        this.textBoxSummaBjorkMedelhojd.TabStop = false;
        this.textBoxSummaBjorkMedelhojd.Text = "0";
        // 
        // textBoxSummaBjorkAntal
        // 
        this.textBoxSummaBjorkAntal.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxSummaBjorkAntal.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxSummaBjorkAntal.Location = new System.Drawing.Point(3, 17);
        this.textBoxSummaBjorkAntal.Name = "textBoxSummaBjorkAntal";
        this.textBoxSummaBjorkAntal.ReadOnly = true;
        this.textBoxSummaBjorkAntal.Size = new System.Drawing.Size(55, 21);
        this.textBoxSummaBjorkAntal.TabIndex = 0;
        this.textBoxSummaBjorkAntal.TabStop = false;
        this.textBoxSummaBjorkAntal.Text = "0";
        // 
        // groupBox21
        // 
        this.groupBox21.Controls.Add(this.textBoxMedelvardeTallMedelhojd);
        this.groupBox21.Controls.Add(this.textBoxMedelvardeTallAntal);
        this.groupBox21.Location = new System.Drawing.Point(106, 412);
        this.groupBox21.Name = "groupBox21";
        this.groupBox21.Size = new System.Drawing.Size(122, 46);
        this.groupBox21.TabIndex = 24;
        this.groupBox21.TabStop = false;
        // 
        // textBoxMedelvardeTallMedelhojd
        // 
        this.textBoxMedelvardeTallMedelhojd.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxMedelvardeTallMedelhojd.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxMedelvardeTallMedelhojd.Location = new System.Drawing.Point(62, 16);
        this.textBoxMedelvardeTallMedelhojd.Name = "textBoxMedelvardeTallMedelhojd";
        this.textBoxMedelvardeTallMedelhojd.ReadOnly = true;
        this.textBoxMedelvardeTallMedelhojd.Size = new System.Drawing.Size(55, 21);
        this.textBoxMedelvardeTallMedelhojd.TabIndex = 1;
        this.textBoxMedelvardeTallMedelhojd.TabStop = false;
        this.textBoxMedelvardeTallMedelhojd.Text = "0";
        // 
        // textBoxMedelvardeTallAntal
        // 
        this.textBoxMedelvardeTallAntal.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxMedelvardeTallAntal.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxMedelvardeTallAntal.Location = new System.Drawing.Point(5, 16);
        this.textBoxMedelvardeTallAntal.Name = "textBoxMedelvardeTallAntal";
        this.textBoxMedelvardeTallAntal.ReadOnly = true;
        this.textBoxMedelvardeTallAntal.Size = new System.Drawing.Size(55, 21);
        this.textBoxMedelvardeTallAntal.TabIndex = 0;
        this.textBoxMedelvardeTallAntal.TabStop = false;
        this.textBoxMedelvardeTallAntal.Text = "0";
        // 
        // groupBox19
        // 
        this.groupBox19.Controls.Add(this.textBoxMedelvardeBjorkMedelhojd);
        this.groupBox19.Controls.Add(this.textBoxMedelvardeBjorkAntal);
        this.groupBox19.Location = new System.Drawing.Point(348, 412);
        this.groupBox19.Name = "groupBox19";
        this.groupBox19.Size = new System.Drawing.Size(120, 46);
        this.groupBox19.TabIndex = 26;
        this.groupBox19.TabStop = false;
        // 
        // textBoxMedelvardeBjorkMedelhojd
        // 
        this.textBoxMedelvardeBjorkMedelhojd.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxMedelvardeBjorkMedelhojd.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxMedelvardeBjorkMedelhojd.Location = new System.Drawing.Point(61, 16);
        this.textBoxMedelvardeBjorkMedelhojd.Name = "textBoxMedelvardeBjorkMedelhojd";
        this.textBoxMedelvardeBjorkMedelhojd.ReadOnly = true;
        this.textBoxMedelvardeBjorkMedelhojd.Size = new System.Drawing.Size(55, 21);
        this.textBoxMedelvardeBjorkMedelhojd.TabIndex = 1;
        this.textBoxMedelvardeBjorkMedelhojd.TabStop = false;
        this.textBoxMedelvardeBjorkMedelhojd.Text = "0";
        // 
        // textBoxMedelvardeBjorkAntal
        // 
        this.textBoxMedelvardeBjorkAntal.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxMedelvardeBjorkAntal.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxMedelvardeBjorkAntal.Location = new System.Drawing.Point(3, 16);
        this.textBoxMedelvardeBjorkAntal.Name = "textBoxMedelvardeBjorkAntal";
        this.textBoxMedelvardeBjorkAntal.ReadOnly = true;
        this.textBoxMedelvardeBjorkAntal.Size = new System.Drawing.Size(55, 21);
        this.textBoxMedelvardeBjorkAntal.TabIndex = 0;
        this.textBoxMedelvardeBjorkAntal.TabStop = false;
        this.textBoxMedelvardeBjorkAntal.Text = "0";
        // 
        // groupBox9
        // 
        this.groupBox9.Controls.Add(this.labelSummaAntal);
        this.groupBox9.Location = new System.Drawing.Point(16, 372);
        this.groupBox9.Name = "groupBox9";
        this.groupBox9.Size = new System.Drawing.Size(90, 48);
        this.groupBox9.TabIndex = 19;
        this.groupBox9.TabStop = false;
        // 
        // labelSummaAntal
        // 
        this.labelSummaAntal.AutoSize = true;
        this.labelSummaAntal.Location = new System.Drawing.Point(21, 20);
        this.labelSummaAntal.Name = "labelSummaAntal";
        this.labelSummaAntal.Size = new System.Drawing.Size(53, 13);
        this.labelSummaAntal.TabIndex = 0;
        this.labelSummaAntal.Text = "SUMMA:";
        // 
        // groupBox20
        // 
        this.groupBox20.Controls.Add(this.textBoxMedelvardeGranMedelhojd);
        this.groupBox20.Controls.Add(this.textBoxMedelvardeGranAntal);
        this.groupBox20.Location = new System.Drawing.Point(228, 412);
        this.groupBox20.Name = "groupBox20";
        this.groupBox20.Size = new System.Drawing.Size(120, 46);
        this.groupBox20.TabIndex = 25;
        this.groupBox20.TabStop = false;
        // 
        // textBoxMedelvardeGranMedelhojd
        // 
        this.textBoxMedelvardeGranMedelhojd.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxMedelvardeGranMedelhojd.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxMedelvardeGranMedelhojd.Location = new System.Drawing.Point(61, 16);
        this.textBoxMedelvardeGranMedelhojd.Name = "textBoxMedelvardeGranMedelhojd";
        this.textBoxMedelvardeGranMedelhojd.ReadOnly = true;
        this.textBoxMedelvardeGranMedelhojd.Size = new System.Drawing.Size(55, 21);
        this.textBoxMedelvardeGranMedelhojd.TabIndex = 1;
        this.textBoxMedelvardeGranMedelhojd.TabStop = false;
        this.textBoxMedelvardeGranMedelhojd.Text = "0";
        // 
        // textBoxMedelvardeGranAntal
        // 
        this.textBoxMedelvardeGranAntal.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxMedelvardeGranAntal.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxMedelvardeGranAntal.Location = new System.Drawing.Point(3, 16);
        this.textBoxMedelvardeGranAntal.Name = "textBoxMedelvardeGranAntal";
        this.textBoxMedelvardeGranAntal.ReadOnly = true;
        this.textBoxMedelvardeGranAntal.Size = new System.Drawing.Size(55, 21);
        this.textBoxMedelvardeGranAntal.TabIndex = 0;
        this.textBoxMedelvardeGranAntal.TabStop = false;
        this.textBoxMedelvardeGranAntal.Text = "0";
        // 
        // groupBox8
        // 
        this.groupBox8.Controls.Add(this.textBoxSummaGranMedelhojd);
        this.groupBox8.Controls.Add(this.textBoxSummaGranAntal);
        this.groupBox8.Location = new System.Drawing.Point(228, 372);
        this.groupBox8.Name = "groupBox8";
        this.groupBox8.Size = new System.Drawing.Size(120, 48);
        this.groupBox8.TabIndex = 17;
        this.groupBox8.TabStop = false;
        this.groupBox8.Text = "Gran";
        // 
        // textBoxSummaGranMedelhojd
        // 
        this.textBoxSummaGranMedelhojd.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxSummaGranMedelhojd.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxSummaGranMedelhojd.Location = new System.Drawing.Point(61, 17);
        this.textBoxSummaGranMedelhojd.Name = "textBoxSummaGranMedelhojd";
        this.textBoxSummaGranMedelhojd.ReadOnly = true;
        this.textBoxSummaGranMedelhojd.Size = new System.Drawing.Size(55, 21);
        this.textBoxSummaGranMedelhojd.TabIndex = 1;
        this.textBoxSummaGranMedelhojd.TabStop = false;
        this.textBoxSummaGranMedelhojd.Text = "0";
        // 
        // textBoxSummaGranAntal
        // 
        this.textBoxSummaGranAntal.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxSummaGranAntal.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxSummaGranAntal.Location = new System.Drawing.Point(3, 17);
        this.textBoxSummaGranAntal.Name = "textBoxSummaGranAntal";
        this.textBoxSummaGranAntal.ReadOnly = true;
        this.textBoxSummaGranAntal.Size = new System.Drawing.Size(55, 21);
        this.textBoxSummaGranAntal.TabIndex = 0;
        this.textBoxSummaGranAntal.TabStop = false;
        this.textBoxSummaGranAntal.Text = "0";
        // 
        // groupBoxSummaTall
        // 
        this.groupBoxSummaTall.Controls.Add(this.textBoxSummaTallMedelhojd);
        this.groupBoxSummaTall.Controls.Add(this.textBoxSummaTallAntal);
        this.groupBoxSummaTall.Location = new System.Drawing.Point(106, 372);
        this.groupBoxSummaTall.Name = "groupBoxSummaTall";
        this.groupBoxSummaTall.Size = new System.Drawing.Size(122, 48);
        this.groupBoxSummaTall.TabIndex = 16;
        this.groupBoxSummaTall.TabStop = false;
        this.groupBoxSummaTall.Text = "Tall";
        // 
        // textBoxSummaTallMedelhojd
        // 
        this.textBoxSummaTallMedelhojd.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxSummaTallMedelhojd.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxSummaTallMedelhojd.Location = new System.Drawing.Point(62, 17);
        this.textBoxSummaTallMedelhojd.Name = "textBoxSummaTallMedelhojd";
        this.textBoxSummaTallMedelhojd.ReadOnly = true;
        this.textBoxSummaTallMedelhojd.Size = new System.Drawing.Size(55, 21);
        this.textBoxSummaTallMedelhojd.TabIndex = 1;
        this.textBoxSummaTallMedelhojd.TabStop = false;
        this.textBoxSummaTallMedelhojd.Text = "0";
        // 
        // textBoxSummaTallAntal
        // 
        this.textBoxSummaTallAntal.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxSummaTallAntal.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxSummaTallAntal.Location = new System.Drawing.Point(5, 17);
        this.textBoxSummaTallAntal.Name = "textBoxSummaTallAntal";
        this.textBoxSummaTallAntal.ReadOnly = true;
        this.textBoxSummaTallAntal.Size = new System.Drawing.Size(55, 21);
        this.textBoxSummaTallAntal.TabIndex = 0;
        this.textBoxSummaTallAntal.TabStop = false;
        this.textBoxSummaTallAntal.Text = "0";
        // 
        // groupBox6
        // 
        this.groupBox6.BackColor = System.Drawing.SystemColors.Control;
        this.groupBox6.Controls.Add(this.label3);
        this.groupBox6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.groupBox6.Location = new System.Drawing.Point(646, 18);
        this.groupBox6.Name = "groupBox6";
        this.groupBox6.Size = new System.Drawing.Size(62, 36);
        this.groupBox6.TabIndex = 5;
        this.groupBox6.TabStop = false;
        // 
        // label3
        // 
        this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label3.Location = new System.Drawing.Point(5, 14);
        this.label3.Name = "label3";
        this.label3.Size = new System.Drawing.Size(57, 19);
        this.label3.TabIndex = 0;
        this.label3.Text = "Barr h-st";
        // 
        // groupBox2
        // 
        this.groupBox2.BackColor = System.Drawing.SystemColors.Control;
        this.groupBox2.Controls.Add(this.labelCont);
        this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.groupBox2.Location = new System.Drawing.Point(466, 19);
        this.groupBox2.Name = "groupBox2";
        this.groupBox2.Size = new System.Drawing.Size(122, 35);
        this.groupBox2.TabIndex = 4;
        this.groupBox2.TabStop = false;
        // 
        // labelCont
        // 
        this.labelCont.AutoSize = true;
        this.labelCont.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelCont.Location = new System.Drawing.Point(47, 13);
        this.labelCont.Name = "labelCont";
        this.labelCont.Size = new System.Drawing.Size(33, 13);
        this.labelCont.TabIndex = 0;
        this.labelCont.Text = "Cont";
        // 
        // groupBox5
        // 
        this.groupBox5.BackColor = System.Drawing.SystemColors.Control;
        this.groupBox5.Controls.Add(this.labelGran);
        this.groupBox5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.groupBox5.Location = new System.Drawing.Point(226, 18);
        this.groupBox5.Name = "groupBox5";
        this.groupBox5.Size = new System.Drawing.Size(122, 36);
        this.groupBox5.TabIndex = 2;
        this.groupBox5.TabStop = false;
        // 
        // labelGran
        // 
        this.labelGran.AutoSize = true;
        this.labelGran.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelGran.Location = new System.Drawing.Point(46, 14);
        this.labelGran.Name = "labelGran";
        this.labelGran.Size = new System.Drawing.Size(34, 13);
        this.labelGran.TabIndex = 0;
        this.labelGran.Text = "Gran";
        // 
        // groupBox1
        // 
        this.groupBox1.BackColor = System.Drawing.SystemColors.Control;
        this.groupBox1.Controls.Add(this.labelBjork);
        this.groupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.groupBox1.Location = new System.Drawing.Point(346, 18);
        this.groupBox1.Name = "groupBox1";
        this.groupBox1.Size = new System.Drawing.Size(122, 36);
        this.groupBox1.TabIndex = 3;
        this.groupBox1.TabStop = false;
        // 
        // labelBjork
        // 
        this.labelBjork.AutoSize = true;
        this.labelBjork.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelBjork.Location = new System.Drawing.Point(22, 14);
        this.labelBjork.Name = "labelBjork";
        this.labelBjork.Size = new System.Drawing.Size(92, 13);
        this.labelBjork.TabIndex = 0;
        this.labelBjork.Text = "Björk/övrig löv";
        // 
        // groupBoxBra
        // 
        this.groupBoxBra.BackColor = System.Drawing.SystemColors.Control;
        this.groupBoxBra.Controls.Add(this.labelTall);
        this.groupBoxBra.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.groupBoxBra.Location = new System.Drawing.Point(106, 18);
        this.groupBoxBra.Name = "groupBoxBra";
        this.groupBoxBra.Size = new System.Drawing.Size(122, 36);
        this.groupBoxBra.TabIndex = 1;
        this.groupBoxBra.TabStop = false;
        // 
        // labelTall
        // 
        this.labelTall.AutoSize = true;
        this.labelTall.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelTall.Location = new System.Drawing.Point(48, 14);
        this.labelTall.Name = "labelTall";
        this.labelTall.Size = new System.Drawing.Size(27, 13);
        this.labelTall.TabIndex = 0;
        this.labelTall.Text = "Tall";
        // 
        // dataGridViewRöjningsstallen
        // 
        this.dataGridViewRöjningsstallen.AllowUserToResizeColumns = false;
        this.dataGridViewRöjningsstallen.AllowUserToResizeRows = false;
        dataGridViewCellStyle1.NullValue = null;
        this.dataGridViewRöjningsstallen.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
        this.dataGridViewRöjningsstallen.AutoGenerateColumns = false;
        this.dataGridViewRöjningsstallen.BackgroundColor = System.Drawing.SystemColors.ControlDark;
        this.dataGridViewRöjningsstallen.BorderStyle = System.Windows.Forms.BorderStyle.None;
        dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomCenter;
        dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
        dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
        dataGridViewCellStyle2.NullValue = null;
        dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        this.dataGridViewRöjningsstallen.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
        this.dataGridViewRöjningsstallen.ColumnHeadersHeight = 55;
        this.dataGridViewRöjningsstallen.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
        this.dataGridViewRöjningsstallen.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ytaDataGridViewTextBoxColumn,
            this.tallAntalDataGridViewTextBoxColumn,
            this.tallMedelhojdDataGridViewTextBoxColumn,
            this.granAntalDataGridViewTextBoxColumn,
            this.granMedelhojdDataGridViewTextBoxColumn,
            this.bjorkAntalDataGridViewTextBoxColumn,
            this.bjorkMedelhojdDataGridViewTextBoxColumn,
            this.contAntalDataGridViewTextBoxColumn,
            this.contMedelhojdDataGridViewTextBoxColumn,
            this.hstAntalDataGridViewTextBoxColumn,
            this.barrMedelhojdDataGridViewTextBoxColumn,
            this.RojstamAntalDataGridViewTextBoxColumn});
        this.dataGridViewRöjningsstallen.DataMember = "Yta";
        this.dataGridViewRöjningsstallen.DataSource = this.dataSetRöjning;
        dataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        dataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Window;
        dataGridViewCellStyle15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        dataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.ControlText;
        dataGridViewCellStyle15.NullValue = null;
        dataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
        this.dataGridViewRöjningsstallen.DefaultCellStyle = dataGridViewCellStyle15;
        this.dataGridViewRöjningsstallen.Location = new System.Drawing.Point(16, 24);
        this.dataGridViewRöjningsstallen.Name = "dataGridViewRöjningsstallen";
        this.dataGridViewRöjningsstallen.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
        dataGridViewCellStyle16.NullValue = null;
        this.dataGridViewRöjningsstallen.RowsDefaultCellStyle = dataGridViewCellStyle16;
        this.dataGridViewRöjningsstallen.RowTemplate.DefaultCellStyle.NullValue = null;
        this.dataGridViewRöjningsstallen.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
        this.dataGridViewRöjningsstallen.Size = new System.Drawing.Size(756, 342);
        this.dataGridViewRöjningsstallen.TabIndex = 0;
        this.dataGridViewRöjningsstallen.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dataGridViewRöjningsstallen_UserDeletingRow);
        this.dataGridViewRöjningsstallen.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewRöjningsstallen_CellValidated);
        this.dataGridViewRöjningsstallen.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dataGridViewRöjningsstallen_UserDeletedRow);
        this.dataGridViewRöjningsstallen.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dataGridViewRöjningsstallen_CellValidating);
        this.dataGridViewRöjningsstallen.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dataGridViewRöjningsstallen_RowsAdded);
        this.dataGridViewRöjningsstallen.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewRöjningsstallen_CellEndEdit);
        this.dataGridViewRöjningsstallen.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewRöjningsstallen_DataError);
        // 
        // ytaDataGridViewTextBoxColumn
        // 
        this.ytaDataGridViewTextBoxColumn.DataPropertyName = "Yta";
        dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle3.BackColor = System.Drawing.Color.LemonChiffon;
        dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.ytaDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
        this.ytaDataGridViewTextBoxColumn.HeaderText = "Yta";
        this.ytaDataGridViewTextBoxColumn.Name = "ytaDataGridViewTextBoxColumn";
        this.ytaDataGridViewTextBoxColumn.ReadOnly = true;
        this.ytaDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
        this.ytaDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        this.ytaDataGridViewTextBoxColumn.Width = 50;
        // 
        // tallAntalDataGridViewTextBoxColumn
        // 
        this.tallAntalDataGridViewTextBoxColumn.DataPropertyName = "TallAntal";
        dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
        dataGridViewCellStyle4.NullValue = null;
        this.tallAntalDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
        this.tallAntalDataGridViewTextBoxColumn.HeaderText = "Antal";
        this.tallAntalDataGridViewTextBoxColumn.Name = "tallAntalDataGridViewTextBoxColumn";
        this.tallAntalDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
        this.tallAntalDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        this.tallAntalDataGridViewTextBoxColumn.Width = 60;
        // 
        // tallMedelhojdDataGridViewTextBoxColumn
        // 
        this.tallMedelhojdDataGridViewTextBoxColumn.DataPropertyName = "TallMedelhojd";
        dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
        dataGridViewCellStyle5.Format = "N1";
        dataGridViewCellStyle5.NullValue = null;
        this.tallMedelhojdDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle5;
        this.tallMedelhojdDataGridViewTextBoxColumn.HeaderText = "m-höjd";
        this.tallMedelhojdDataGridViewTextBoxColumn.Name = "tallMedelhojdDataGridViewTextBoxColumn";
        this.tallMedelhojdDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
        this.tallMedelhojdDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        this.tallMedelhojdDataGridViewTextBoxColumn.Width = 60;
        // 
        // granAntalDataGridViewTextBoxColumn
        // 
        this.granAntalDataGridViewTextBoxColumn.DataPropertyName = "GranAntal";
        dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
        this.granAntalDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle6;
        this.granAntalDataGridViewTextBoxColumn.HeaderText = "Antal";
        this.granAntalDataGridViewTextBoxColumn.Name = "granAntalDataGridViewTextBoxColumn";
        this.granAntalDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
        this.granAntalDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        this.granAntalDataGridViewTextBoxColumn.Width = 60;
        // 
        // granMedelhojdDataGridViewTextBoxColumn
        // 
        this.granMedelhojdDataGridViewTextBoxColumn.DataPropertyName = "GranMedelhojd";
        dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
        dataGridViewCellStyle7.Format = "N1";
        dataGridViewCellStyle7.NullValue = null;
        this.granMedelhojdDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle7;
        this.granMedelhojdDataGridViewTextBoxColumn.HeaderText = "m-höjd";
        this.granMedelhojdDataGridViewTextBoxColumn.Name = "granMedelhojdDataGridViewTextBoxColumn";
        this.granMedelhojdDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
        this.granMedelhojdDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        this.granMedelhojdDataGridViewTextBoxColumn.Width = 60;
        // 
        // bjorkAntalDataGridViewTextBoxColumn
        // 
        this.bjorkAntalDataGridViewTextBoxColumn.DataPropertyName = "BjorkAntal";
        dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
        this.bjorkAntalDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle8;
        this.bjorkAntalDataGridViewTextBoxColumn.HeaderText = "Antal";
        this.bjorkAntalDataGridViewTextBoxColumn.Name = "bjorkAntalDataGridViewTextBoxColumn";
        this.bjorkAntalDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
        this.bjorkAntalDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        this.bjorkAntalDataGridViewTextBoxColumn.Width = 60;
        // 
        // bjorkMedelhojdDataGridViewTextBoxColumn
        // 
        this.bjorkMedelhojdDataGridViewTextBoxColumn.DataPropertyName = "BjorkMedelhojd";
        dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
        dataGridViewCellStyle9.Format = "N1";
        dataGridViewCellStyle9.NullValue = null;
        this.bjorkMedelhojdDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle9;
        this.bjorkMedelhojdDataGridViewTextBoxColumn.HeaderText = "m-höjd";
        this.bjorkMedelhojdDataGridViewTextBoxColumn.Name = "bjorkMedelhojdDataGridViewTextBoxColumn";
        this.bjorkMedelhojdDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
        this.bjorkMedelhojdDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        this.bjorkMedelhojdDataGridViewTextBoxColumn.Width = 60;
        // 
        // contAntalDataGridViewTextBoxColumn
        // 
        this.contAntalDataGridViewTextBoxColumn.DataPropertyName = "ContAntal";
        dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
        dataGridViewCellStyle10.NullValue = null;
        this.contAntalDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle10;
        this.contAntalDataGridViewTextBoxColumn.HeaderText = "Antal";
        this.contAntalDataGridViewTextBoxColumn.Name = "contAntalDataGridViewTextBoxColumn";
        this.contAntalDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
        this.contAntalDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        this.contAntalDataGridViewTextBoxColumn.Width = 60;
        // 
        // contMedelhojdDataGridViewTextBoxColumn
        // 
        this.contMedelhojdDataGridViewTextBoxColumn.DataPropertyName = "ContMedelhojd";
        dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle11.BackColor = System.Drawing.Color.White;
        dataGridViewCellStyle11.Format = "N1";
        dataGridViewCellStyle11.NullValue = null;
        this.contMedelhojdDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle11;
        this.contMedelhojdDataGridViewTextBoxColumn.HeaderText = "m-höjd";
        this.contMedelhojdDataGridViewTextBoxColumn.Name = "contMedelhojdDataGridViewTextBoxColumn";
        this.contMedelhojdDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
        this.contMedelhojdDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        this.contMedelhojdDataGridViewTextBoxColumn.Width = 60;
        // 
        // hstAntalDataGridViewTextBoxColumn
        // 
        this.hstAntalDataGridViewTextBoxColumn.DataPropertyName = "Hst";
        dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle12.BackColor = System.Drawing.Color.LemonChiffon;
        this.hstAntalDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle12;
        this.hstAntalDataGridViewTextBoxColumn.HeaderText = "Antal";
        this.hstAntalDataGridViewTextBoxColumn.Name = "hstAntalDataGridViewTextBoxColumn";
        this.hstAntalDataGridViewTextBoxColumn.ReadOnly = true;
        this.hstAntalDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
        this.hstAntalDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        this.hstAntalDataGridViewTextBoxColumn.Width = 60;
        // 
        // barrMedelhojdDataGridViewTextBoxColumn
        // 
        this.barrMedelhojdDataGridViewTextBoxColumn.DataPropertyName = "MHojd";
        dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle13.BackColor = System.Drawing.Color.LemonChiffon;
        dataGridViewCellStyle13.Format = "N1";
        dataGridViewCellStyle13.NullValue = null;
        this.barrMedelhojdDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle13;
        this.barrMedelhojdDataGridViewTextBoxColumn.HeaderText = "m-höjd";
        this.barrMedelhojdDataGridViewTextBoxColumn.Name = "barrMedelhojdDataGridViewTextBoxColumn";
        this.barrMedelhojdDataGridViewTextBoxColumn.ReadOnly = true;
        this.barrMedelhojdDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
        this.barrMedelhojdDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        this.barrMedelhojdDataGridViewTextBoxColumn.Width = 60;
        // 
        // RojstamAntalDataGridViewTextBoxColumn
        // 
        this.RojstamAntalDataGridViewTextBoxColumn.DataPropertyName = "RojstamAntal";
        dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle14.BackColor = System.Drawing.Color.White;
        this.RojstamAntalDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle14;
        this.RojstamAntalDataGridViewTextBoxColumn.HeaderText = "Antal";
        this.RojstamAntalDataGridViewTextBoxColumn.Name = "RojstamAntalDataGridViewTextBoxColumn";
        this.RojstamAntalDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
        this.RojstamAntalDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        this.RojstamAntalDataGridViewTextBoxColumn.Width = 60;
        // 
        // tabPageFrågor
        // 
        this.tabPageFrågor.Controls.Add(this.groupBoxKommentarer);
        this.tabPageFrågor.Controls.Add(this.labelNatur);
        this.tabPageFrågor.Controls.Add(this.groupBoxFråga6);
        this.tabPageFrågor.Controls.Add(this.groupBoxFråga5);
        this.tabPageFrågor.Controls.Add(this.groupBoxFråga4);
        this.tabPageFrågor.Controls.Add(this.groupBoxOBS);
        this.tabPageFrågor.Controls.Add(this.groupBoxFråga3);
        this.tabPageFrågor.Controls.Add(this.groupBoxFråga2);
        this.tabPageFrågor.Controls.Add(this.groupBoxFråga1);
        this.tabPageFrågor.Location = new System.Drawing.Point(4, 22);
        this.tabPageFrågor.Name = "tabPageFrågor";
        this.tabPageFrågor.Padding = new System.Windows.Forms.Padding(3);
        this.tabPageFrågor.Size = new System.Drawing.Size(792, 481);
        this.tabPageFrågor.TabIndex = 1;
        this.tabPageFrågor.Text = "Frågor";
        this.tabPageFrågor.UseVisualStyleBackColor = true;
        // 
        // groupBoxKommentarer
        // 
        this.groupBoxKommentarer.Controls.Add(this.richTextBoxKommentar);
        this.groupBoxKommentarer.Location = new System.Drawing.Point(8, 369);
        this.groupBoxKommentarer.Name = "groupBoxKommentarer";
        this.groupBoxKommentarer.Size = new System.Drawing.Size(576, 100);
        this.groupBoxKommentarer.TabIndex = 17;
        this.groupBoxKommentarer.TabStop = false;
        this.groupBoxKommentarer.Text = "KOMMENTARER";
        // 
        // richTextBoxKommentar
        // 
        this.richTextBoxKommentar.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetRöjning, "UserData.Kommentar", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.richTextBoxKommentar.Location = new System.Drawing.Point(12, 20);
        this.richTextBoxKommentar.MaxLength = 4095;
        this.richTextBoxKommentar.Name = "richTextBoxKommentar";
        this.richTextBoxKommentar.Size = new System.Drawing.Size(553, 68);
        this.richTextBoxKommentar.TabIndex = 0;
        this.richTextBoxKommentar.Text = "";
        this.richTextBoxKommentar.TextChanged += new System.EventHandler(this.richTextBoxKommentar_TextChanged);
        // 
        // labelNatur
        // 
        this.labelNatur.AutoSize = true;
        this.labelNatur.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelNatur.Location = new System.Drawing.Point(8, 8);
        this.labelNatur.Name = "labelNatur";
        this.labelNatur.Size = new System.Drawing.Size(202, 13);
        this.labelNatur.TabIndex = 9;
        this.labelNatur.Text = "NATUR- OCH KULTURMILJÖHÄNSYN";
        // 
        // groupBoxFråga6
        // 
        this.groupBoxFråga6.Controls.Add(this.radioButton6Ejaktuellt);
        this.groupBoxFråga6.Controls.Add(this.radioButton6Nej);
        this.groupBoxFråga6.Controls.Add(this.radioButton6Ja);
        this.groupBoxFråga6.Controls.Add(this.labelFrågor6);
        this.groupBoxFråga6.Location = new System.Drawing.Point(8, 271);
        this.groupBoxFråga6.Name = "groupBoxFråga6";
        this.groupBoxFråga6.Size = new System.Drawing.Size(576, 50);
        this.groupBoxFråga6.TabIndex = 15;
        this.groupBoxFråga6.TabStop = false;
        this.groupBoxFråga6.Text = "6.";
        // 
        // radioButton6Ejaktuellt
        // 
        this.radioButton6Ejaktuellt.AutoSize = true;
        this.radioButton6Ejaktuellt.Location = new System.Drawing.Point(488, 20);
        this.radioButton6Ejaktuellt.Name = "radioButton6Ejaktuellt";
        this.radioButton6Ejaktuellt.Size = new System.Drawing.Size(82, 17);
        this.radioButton6Ejaktuellt.TabIndex = 26;
        this.radioButton6Ejaktuellt.TabStop = true;
        this.radioButton6Ejaktuellt.Text = "Ej aktuellt";
        this.radioButton6Ejaktuellt.UseVisualStyleBackColor = true;
        this.radioButton6Ejaktuellt.CheckedChanged += new System.EventHandler(this.radioButton6Ejaktuellt_CheckedChanged);
        // 
        // radioButton6Nej
        // 
        this.radioButton6Nej.AutoSize = true;
        this.radioButton6Nej.Location = new System.Drawing.Point(439, 20);
        this.radioButton6Nej.Name = "radioButton6Nej";
        this.radioButton6Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton6Nej.TabIndex = 25;
        this.radioButton6Nej.TabStop = true;
        this.radioButton6Nej.Text = "Nej";
        this.radioButton6Nej.UseVisualStyleBackColor = true;
        this.radioButton6Nej.CheckedChanged += new System.EventHandler(this.radioButton6Nej_CheckedChanged);
        // 
        // radioButton6Ja
        // 
        this.radioButton6Ja.AutoSize = true;
        this.radioButton6Ja.Location = new System.Drawing.Point(389, 20);
        this.radioButton6Ja.Name = "radioButton6Ja";
        this.radioButton6Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton6Ja.TabIndex = 24;
        this.radioButton6Ja.TabStop = true;
        this.radioButton6Ja.Text = "Ja";
        this.radioButton6Ja.UseVisualStyleBackColor = true;
        this.radioButton6Ja.CheckedChanged += new System.EventHandler(this.radioButton6Ja_CheckedChanged);
        // 
        // labelFrågor6
        // 
        this.labelFrågor6.AutoSize = true;
        this.labelFrågor6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelFrågor6.Location = new System.Drawing.Point(19, 22);
        this.labelFrågor6.Name = "labelFrågor6";
        this.labelFrågor6.Size = new System.Drawing.Size(192, 13);
        this.labelFrågor6.TabIndex = 0;
        this.labelFrågor6.Text = "Är stigar och leder framkomliga?";
        // 
        // groupBoxFråga5
        // 
        this.groupBoxFråga5.Controls.Add(this.radioButton5Ejaktuellt);
        this.groupBoxFråga5.Controls.Add(this.radioButton5Nej);
        this.groupBoxFråga5.Controls.Add(this.radioButton5Ja);
        this.groupBoxFråga5.Controls.Add(this.labelFrågor5);
        this.groupBoxFråga5.Location = new System.Drawing.Point(8, 229);
        this.groupBoxFråga5.Name = "groupBoxFråga5";
        this.groupBoxFråga5.Size = new System.Drawing.Size(576, 50);
        this.groupBoxFråga5.TabIndex = 14;
        this.groupBoxFråga5.TabStop = false;
        this.groupBoxFråga5.Text = "5.";
        // 
        // radioButton5Ejaktuellt
        // 
        this.radioButton5Ejaktuellt.AutoSize = true;
        this.radioButton5Ejaktuellt.Location = new System.Drawing.Point(488, 20);
        this.radioButton5Ejaktuellt.Name = "radioButton5Ejaktuellt";
        this.radioButton5Ejaktuellt.Size = new System.Drawing.Size(82, 17);
        this.radioButton5Ejaktuellt.TabIndex = 23;
        this.radioButton5Ejaktuellt.TabStop = true;
        this.radioButton5Ejaktuellt.Text = "Ej aktuellt";
        this.radioButton5Ejaktuellt.UseVisualStyleBackColor = true;
        this.radioButton5Ejaktuellt.CheckedChanged += new System.EventHandler(this.radioButton5Ejaktuellt_CheckedChanged);
        // 
        // radioButton5Nej
        // 
        this.radioButton5Nej.AutoSize = true;
        this.radioButton5Nej.Location = new System.Drawing.Point(439, 20);
        this.radioButton5Nej.Name = "radioButton5Nej";
        this.radioButton5Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton5Nej.TabIndex = 22;
        this.radioButton5Nej.TabStop = true;
        this.radioButton5Nej.Text = "Nej";
        this.radioButton5Nej.UseVisualStyleBackColor = true;
        this.radioButton5Nej.CheckedChanged += new System.EventHandler(this.radioButton5Nej_CheckedChanged);
        // 
        // radioButton5Ja
        // 
        this.radioButton5Ja.AutoSize = true;
        this.radioButton5Ja.Location = new System.Drawing.Point(389, 20);
        this.radioButton5Ja.Name = "radioButton5Ja";
        this.radioButton5Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton5Ja.TabIndex = 21;
        this.radioButton5Ja.TabStop = true;
        this.radioButton5Ja.Text = "Ja";
        this.radioButton5Ja.UseVisualStyleBackColor = true;
        this.radioButton5Ja.CheckedChanged += new System.EventHandler(this.radioButton5Ja_CheckedChanged);
        // 
        // labelFrågor5
        // 
        this.labelFrågor5.AutoSize = true;
        this.labelFrågor5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelFrågor5.Location = new System.Drawing.Point(19, 22);
        this.labelFrågor5.Name = "labelFrågor5";
        this.labelFrågor5.Size = new System.Drawing.Size(241, 13);
        this.labelFrågor5.TabIndex = 0;
        this.labelFrågor5.Text = "Har kultur- och fornlämningar röjts fram?";
        // 
        // groupBoxFråga4
        // 
        this.groupBoxFråga4.Controls.Add(this.radioButton4Ejaktuellt);
        this.groupBoxFråga4.Controls.Add(this.radioButton4Nej);
        this.groupBoxFråga4.Controls.Add(this.radioButton4Ja);
        this.groupBoxFråga4.Controls.Add(this.labelFrågor4);
        this.groupBoxFråga4.Location = new System.Drawing.Point(8, 177);
        this.groupBoxFråga4.Name = "groupBoxFråga4";
        this.groupBoxFråga4.Size = new System.Drawing.Size(576, 65);
        this.groupBoxFråga4.TabIndex = 13;
        this.groupBoxFråga4.TabStop = false;
        this.groupBoxFråga4.Text = "4.";
        // 
        // radioButton4Ejaktuellt
        // 
        this.radioButton4Ejaktuellt.AutoSize = true;
        this.radioButton4Ejaktuellt.Location = new System.Drawing.Point(488, 31);
        this.radioButton4Ejaktuellt.Name = "radioButton4Ejaktuellt";
        this.radioButton4Ejaktuellt.Size = new System.Drawing.Size(82, 17);
        this.radioButton4Ejaktuellt.TabIndex = 20;
        this.radioButton4Ejaktuellt.TabStop = true;
        this.radioButton4Ejaktuellt.Text = "Ej aktuellt";
        this.radioButton4Ejaktuellt.UseVisualStyleBackColor = true;
        this.radioButton4Ejaktuellt.CheckedChanged += new System.EventHandler(this.radioButton4Ejaktuellt_CheckedChanged);
        // 
        // radioButton4Nej
        // 
        this.radioButton4Nej.AutoSize = true;
        this.radioButton4Nej.Location = new System.Drawing.Point(439, 31);
        this.radioButton4Nej.Name = "radioButton4Nej";
        this.radioButton4Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton4Nej.TabIndex = 19;
        this.radioButton4Nej.TabStop = true;
        this.radioButton4Nej.Text = "Nej";
        this.radioButton4Nej.UseVisualStyleBackColor = true;
        this.radioButton4Nej.CheckedChanged += new System.EventHandler(this.radioButton4Nej_CheckedChanged);
        // 
        // radioButton4Ja
        // 
        this.radioButton4Ja.AutoSize = true;
        this.radioButton4Ja.Location = new System.Drawing.Point(389, 31);
        this.radioButton4Ja.Name = "radioButton4Ja";
        this.radioButton4Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton4Ja.TabIndex = 18;
        this.radioButton4Ja.TabStop = true;
        this.radioButton4Ja.Text = "Ja";
        this.radioButton4Ja.UseVisualStyleBackColor = true;
        this.radioButton4Ja.CheckedChanged += new System.EventHandler(this.radioButton4Ja_CheckedChanged);
        // 
        // labelFrågor4
        // 
        this.labelFrågor4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelFrågor4.Location = new System.Drawing.Point(19, 22);
        this.labelFrågor4.Name = "labelFrågor4";
        this.labelFrågor4.Size = new System.Drawing.Size(364, 26);
        this.labelFrågor4.TabIndex = 0;
        this.labelFrågor4.Text = "Har röjning gynnat naturvärden i skyddzonen (ex.vis mot vatten, myr)?";
        // 
        // groupBoxOBS
        // 
        this.groupBoxOBS.Controls.Add(this.labelFragaObs);
        this.groupBoxOBS.Controls.Add(this.labelOBS);
        this.groupBoxOBS.Location = new System.Drawing.Point(8, 313);
        this.groupBoxOBS.Name = "groupBoxOBS";
        this.groupBoxOBS.Size = new System.Drawing.Size(576, 62);
        this.groupBoxOBS.TabIndex = 16;
        this.groupBoxOBS.TabStop = false;
        // 
        // labelFragaObs
        // 
        this.labelFragaObs.AutoSize = true;
        this.labelFragaObs.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelFragaObs.Location = new System.Drawing.Point(19, 36);
        this.labelFragaObs.Name = "labelFragaObs";
        this.labelFragaObs.Size = new System.Drawing.Size(456, 13);
        this.labelFragaObs.TabIndex = 1;
        this.labelFragaObs.Text = "Ifall något ”Nej” har valts på någon av frågorna så skall en miljörapport skrivas" +
            ".";
        // 
        // labelOBS
        // 
        this.labelOBS.AutoSize = true;
        this.labelOBS.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelOBS.Location = new System.Drawing.Point(6, 16);
        this.labelOBS.Name = "labelOBS";
        this.labelOBS.Size = new System.Drawing.Size(32, 13);
        this.labelOBS.TabIndex = 0;
        this.labelOBS.Text = "OBS!";
        // 
        // groupBoxFråga3
        // 
        this.groupBoxFråga3.Controls.Add(this.radioButton3Ejaktuellt);
        this.groupBoxFråga3.Controls.Add(this.radioButton3Nej);
        this.groupBoxFråga3.Controls.Add(this.radioButton3Ja);
        this.groupBoxFråga3.Controls.Add(this.labelFrågor3);
        this.groupBoxFråga3.Location = new System.Drawing.Point(8, 135);
        this.groupBoxFråga3.Name = "groupBoxFråga3";
        this.groupBoxFråga3.Size = new System.Drawing.Size(576, 50);
        this.groupBoxFråga3.TabIndex = 12;
        this.groupBoxFråga3.TabStop = false;
        this.groupBoxFråga3.Text = "3.";
        // 
        // radioButton3Ejaktuellt
        // 
        this.radioButton3Ejaktuellt.AutoSize = true;
        this.radioButton3Ejaktuellt.Location = new System.Drawing.Point(488, 20);
        this.radioButton3Ejaktuellt.Name = "radioButton3Ejaktuellt";
        this.radioButton3Ejaktuellt.Size = new System.Drawing.Size(82, 17);
        this.radioButton3Ejaktuellt.TabIndex = 17;
        this.radioButton3Ejaktuellt.TabStop = true;
        this.radioButton3Ejaktuellt.Text = "Ej aktuellt";
        this.radioButton3Ejaktuellt.UseVisualStyleBackColor = true;
        this.radioButton3Ejaktuellt.CheckedChanged += new System.EventHandler(this.radioButton3Ejaktuellt_CheckedChanged);
        // 
        // radioButton3Nej
        // 
        this.radioButton3Nej.AutoSize = true;
        this.radioButton3Nej.Location = new System.Drawing.Point(439, 20);
        this.radioButton3Nej.Name = "radioButton3Nej";
        this.radioButton3Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton3Nej.TabIndex = 16;
        this.radioButton3Nej.TabStop = true;
        this.radioButton3Nej.Text = "Nej";
        this.radioButton3Nej.UseVisualStyleBackColor = true;
        this.radioButton3Nej.CheckedChanged += new System.EventHandler(this.radioButton3Nej_CheckedChanged);
        // 
        // radioButton3Ja
        // 
        this.radioButton3Ja.AutoSize = true;
        this.radioButton3Ja.Location = new System.Drawing.Point(389, 20);
        this.radioButton3Ja.Name = "radioButton3Ja";
        this.radioButton3Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton3Ja.TabIndex = 15;
        this.radioButton3Ja.TabStop = true;
        this.radioButton3Ja.Text = "Ja";
        this.radioButton3Ja.UseVisualStyleBackColor = true;
        this.radioButton3Ja.CheckedChanged += new System.EventHandler(this.radioButton3Ja_CheckedChanged);
        // 
        // labelFrågor3
        // 
        this.labelFrågor3.AutoSize = true;
        this.labelFrågor3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelFrågor3.Location = new System.Drawing.Point(19, 22);
        this.labelFrågor3.Name = "labelFrågor3";
        this.labelFrågor3.Size = new System.Drawing.Size(293, 13);
        this.labelFrågor3.TabIndex = 0;
        this.labelFrågor3.Text = "Har röjning undvikits på impedimentet (häll/myr)?";
        // 
        // groupBoxFråga2
        // 
        this.groupBoxFråga2.Controls.Add(this.radioButton2Ejaktuellt);
        this.groupBoxFråga2.Controls.Add(this.radioButton2Nej);
        this.groupBoxFråga2.Controls.Add(this.radioButton2Ja);
        this.groupBoxFråga2.Controls.Add(this.labelFrågor2);
        this.groupBoxFråga2.Location = new System.Drawing.Point(8, 91);
        this.groupBoxFråga2.Name = "groupBoxFråga2";
        this.groupBoxFråga2.Size = new System.Drawing.Size(576, 50);
        this.groupBoxFråga2.TabIndex = 11;
        this.groupBoxFråga2.TabStop = false;
        this.groupBoxFråga2.Text = "2.";
        // 
        // radioButton2Ejaktuellt
        // 
        this.radioButton2Ejaktuellt.AutoSize = true;
        this.radioButton2Ejaktuellt.Location = new System.Drawing.Point(488, 20);
        this.radioButton2Ejaktuellt.Name = "radioButton2Ejaktuellt";
        this.radioButton2Ejaktuellt.Size = new System.Drawing.Size(82, 17);
        this.radioButton2Ejaktuellt.TabIndex = 14;
        this.radioButton2Ejaktuellt.TabStop = true;
        this.radioButton2Ejaktuellt.Text = "Ej aktuellt";
        this.radioButton2Ejaktuellt.UseVisualStyleBackColor = true;
        this.radioButton2Ejaktuellt.CheckedChanged += new System.EventHandler(this.radioButton2Ejaktuellt_CheckedChanged);
        // 
        // radioButton2Nej
        // 
        this.radioButton2Nej.AutoSize = true;
        this.radioButton2Nej.Location = new System.Drawing.Point(439, 20);
        this.radioButton2Nej.Name = "radioButton2Nej";
        this.radioButton2Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton2Nej.TabIndex = 13;
        this.radioButton2Nej.TabStop = true;
        this.radioButton2Nej.Text = "Nej";
        this.radioButton2Nej.UseVisualStyleBackColor = true;
        this.radioButton2Nej.CheckedChanged += new System.EventHandler(this.radioButton2Nej_CheckedChanged);
        // 
        // radioButton2Ja
        // 
        this.radioButton2Ja.AutoSize = true;
        this.radioButton2Ja.Location = new System.Drawing.Point(389, 20);
        this.radioButton2Ja.Name = "radioButton2Ja";
        this.radioButton2Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton2Ja.TabIndex = 12;
        this.radioButton2Ja.TabStop = true;
        this.radioButton2Ja.Text = "Ja";
        this.radioButton2Ja.UseVisualStyleBackColor = true;
        this.radioButton2Ja.CheckedChanged += new System.EventHandler(this.radioButton2Ja_CheckedChanged);
        // 
        // labelFrågor2
        // 
        this.labelFrågor2.AutoSize = true;
        this.labelFrågor2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelFrågor2.Location = new System.Drawing.Point(19, 21);
        this.labelFrågor2.Name = "labelFrågor2";
        this.labelFrågor2.Size = new System.Drawing.Size(354, 13);
        this.labelFrågor2.TabIndex = 0;
        this.labelFrågor2.Text = "Har röjning gynnat naturvärden i hänsynskrävande biotoper?";
        // 
        // groupBoxFråga1
        // 
        this.groupBoxFråga1.Controls.Add(this.radioButton1Nej);
        this.groupBoxFråga1.Controls.Add(this.radioButton1Ja);
        this.groupBoxFråga1.Controls.Add(this.labelFrågor1);
        this.groupBoxFråga1.Location = new System.Drawing.Point(8, 29);
        this.groupBoxFråga1.Name = "groupBoxFråga1";
        this.groupBoxFråga1.Size = new System.Drawing.Size(576, 50);
        this.groupBoxFråga1.TabIndex = 10;
        this.groupBoxFråga1.TabStop = false;
        this.groupBoxFråga1.Text = "1.";
        // 
        // radioButton1Nej
        // 
        this.radioButton1Nej.AutoSize = true;
        this.radioButton1Nej.Location = new System.Drawing.Point(519, 20);
        this.radioButton1Nej.Name = "radioButton1Nej";
        this.radioButton1Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton1Nej.TabIndex = 11;
        this.radioButton1Nej.TabStop = true;
        this.radioButton1Nej.Text = "Nej";
        this.radioButton1Nej.UseVisualStyleBackColor = true;
        this.radioButton1Nej.CheckedChanged += new System.EventHandler(this.radioButton1Nej_CheckedChanged);
        // 
        // radioButton1Ja
        // 
        this.radioButton1Ja.AutoSize = true;
        this.radioButton1Ja.Location = new System.Drawing.Point(469, 20);
        this.radioButton1Ja.Name = "radioButton1Ja";
        this.radioButton1Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton1Ja.TabIndex = 10;
        this.radioButton1Ja.TabStop = true;
        this.radioButton1Ja.Text = "Ja";
        this.radioButton1Ja.UseVisualStyleBackColor = true;
        this.radioButton1Ja.CheckedChanged += new System.EventHandler(this.radioButton1Ja_CheckedChanged);
        // 
        // labelFrågor1
        // 
        this.labelFrågor1.AutoSize = true;
        this.labelFrågor1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelFrågor1.Location = new System.Drawing.Point(19, 22);
        this.labelFrågor1.Name = "labelFrågor1";
        this.labelFrågor1.Size = new System.Drawing.Size(137, 13);
        this.labelFrågor1.TabIndex = 0;
        this.labelFrågor1.Text = "Är traktdirektiv följda?";
        // 
        // environmentRöjning
        // 
        designerSettings1.ApplicationConnection = null;
        designerSettings1.DefaultFont = new System.Drawing.Font("Arial", 10F);
        designerSettings1.Icon = ((System.Drawing.Icon)(resources.GetObject("designerSettings1.Icon")));
        designerSettings1.Restrictions = designerRestrictions1;
        designerSettings1.Text = "";
        this.environmentRöjning.DesignerSettings = designerSettings1;
        emailSettings1.Address = "";
        emailSettings1.Host = "";
        emailSettings1.MessageTemplate = "";
        emailSettings1.Name = "";
        emailSettings1.Password = "";
        emailSettings1.Port = 49;
        emailSettings1.UserName = "";
        this.environmentRöjning.EmailSettings = emailSettings1;
        previewSettings1.Buttons = ((FastReport.PreviewButtons)((((((FastReport.PreviewButtons.Print | FastReport.PreviewButtons.Save)
                    | FastReport.PreviewButtons.Find)
                    | FastReport.PreviewButtons.Zoom)
                    | FastReport.PreviewButtons.Navigator)
                    | FastReport.PreviewButtons.Close)));
        previewSettings1.Icon = ((System.Drawing.Icon)(resources.GetObject("previewSettings1.Icon")));
        previewSettings1.Text = "";
        this.environmentRöjning.PreviewSettings = previewSettings1;
        this.environmentRöjning.ReportSettings = reportSettings1;
        this.environmentRöjning.UIStyle = FastReport.Utils.UIStyle.Office2007Black;
        // 
        // labelTotaltAntalYtor
        // 
        this.labelTotaltAntalYtor.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelTotaltAntalYtor.Location = new System.Drawing.Point(1, 539);
        this.labelTotaltAntalYtor.Name = "labelTotaltAntalYtor";
        this.labelTotaltAntalYtor.Size = new System.Drawing.Size(158, 13);
        this.labelTotaltAntalYtor.TabIndex = 2;
        this.labelTotaltAntalYtor.Text = "Totalt antal ytor: 0";
        // 
        // labelProgress
        // 
        this.labelProgress.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelProgress.Location = new System.Drawing.Point(712, 539);
        this.labelProgress.Name = "labelProgress";
        this.labelProgress.Size = new System.Drawing.Size(80, 13);
        this.labelProgress.TabIndex = 4;
        this.labelProgress.Text = "0% Klart";
        this.labelProgress.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
        // 
        // progressBarData
        // 
        this.progressBarData.ForeColor = System.Drawing.SystemColors.Desktop;
        this.progressBarData.Location = new System.Drawing.Point(4, 555);
        this.progressBarData.Maximum = 15;
        this.progressBarData.Name = "progressBarData";
        this.progressBarData.RightToLeftLayout = true;
        this.progressBarData.Size = new System.Drawing.Size(786, 20);
        this.progressBarData.Step = 1;
        this.progressBarData.TabIndex = 3;
        // 
        // textBoxRegion
        // 
        this.textBoxRegion.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetRöjning, "UserData.Region", true));
        this.textBoxRegion.Location = new System.Drawing.Point(140, 30);
        this.textBoxRegion.MaxLength = 31;
        this.textBoxRegion.Name = "textBoxRegion";
        this.textBoxRegion.ReadOnly = true;
        this.textBoxRegion.Size = new System.Drawing.Size(116, 21);
        this.textBoxRegion.TabIndex = 11;
        this.textBoxRegion.Visible = false;
        // 
        // textBoxDistrikt
        // 
        this.textBoxDistrikt.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetRöjning, "UserData.Distrikt", true));
        this.textBoxDistrikt.Location = new System.Drawing.Point(264, 30);
        this.textBoxDistrikt.MaxLength = 31;
        this.textBoxDistrikt.Name = "textBoxDistrikt";
        this.textBoxDistrikt.ReadOnly = true;
        this.textBoxDistrikt.Size = new System.Drawing.Size(116, 21);
        this.textBoxDistrikt.TabIndex = 12;
        this.textBoxDistrikt.Visible = false;
        // 
        // RöjningForm
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.AutoScroll = true;
        this.AutoSize = true;
        this.ClientSize = new System.Drawing.Size(801, 583);
        this.Controls.Add(this.labelTotaltAntalYtor);
        this.Controls.Add(this.labelProgress);
        this.Controls.Add(this.progressBarData);
        this.Controls.Add(this.tabControl1);
        this.Controls.Add(this.menuStripRöjning);
        this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
        this.MainMenuStrip = this.menuStripRöjning;
        this.MaximizeBox = false;
        this.MaximumSize = new System.Drawing.Size(820, 710);
        this.MinimizeBox = false;
        this.Name = "RöjningForm";
        this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        this.Text = "KORUS Röjning";
        this.Load += new System.EventHandler(this.Röjning_Load);
        this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Röjning_FormClosing);
        ((System.ComponentModel.ISupportInitialize)(this.dataSetRöjning)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableYta)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableUserData)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFragor)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetSettings)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableRegion)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableDistrikt)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableMarkberedningsmetod)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableUrsprung)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableInvTyp)).EndInit();
        this.menuStripRöjning.ResumeLayout(false);
        this.menuStripRöjning.PerformLayout();
        ((System.ComponentModel.ISupportInitialize)(this.reportRöjning)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetCalculations)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableTableMedelvarde)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableSumma)).EndInit();
        this.tabControl1.ResumeLayout(false);
        this.tabPageRöjning.ResumeLayout(false);
        this.groupBoxRöjning.ResumeLayout(false);
        this.groupBoxRöjning.PerformLayout();
        this.groupBox13.ResumeLayout(false);
        this.groupBox13.PerformLayout();
        this.groupBoxTrakt.ResumeLayout(false);
        this.groupBoxTrakt.PerformLayout();
        this.groupBoxRegion.ResumeLayout(false);
        this.groupBoxRegion.PerformLayout();
        this.tabPageYtor.ResumeLayout(false);
        this.groupBoxRöjningsstallen.ResumeLayout(false);
        this.groupBox3.ResumeLayout(false);
        this.groupBox22.ResumeLayout(false);
        this.groupBox22.PerformLayout();
        this.groupBox23.ResumeLayout(false);
        this.groupBox23.PerformLayout();
        this.groupBox7.ResumeLayout(false);
        this.groupBox15.ResumeLayout(false);
        this.groupBox15.PerformLayout();
        this.groupBox4.ResumeLayout(false);
        this.groupBox4.PerformLayout();
        this.groupBox14.ResumeLayout(false);
        this.groupBox14.PerformLayout();
        this.groupBox16.ResumeLayout(false);
        this.groupBox16.PerformLayout();
        this.groupBox12.ResumeLayout(false);
        this.groupBox12.PerformLayout();
        this.groupBox11.ResumeLayout(false);
        this.groupBox11.PerformLayout();
        this.groupBox18.ResumeLayout(false);
        this.groupBox18.PerformLayout();
        this.groupBox10.ResumeLayout(false);
        this.groupBox10.PerformLayout();
        this.groupBox21.ResumeLayout(false);
        this.groupBox21.PerformLayout();
        this.groupBox19.ResumeLayout(false);
        this.groupBox19.PerformLayout();
        this.groupBox9.ResumeLayout(false);
        this.groupBox9.PerformLayout();
        this.groupBox20.ResumeLayout(false);
        this.groupBox20.PerformLayout();
        this.groupBox8.ResumeLayout(false);
        this.groupBox8.PerformLayout();
        this.groupBoxSummaTall.ResumeLayout(false);
        this.groupBoxSummaTall.PerformLayout();
        this.groupBox6.ResumeLayout(false);
        this.groupBox2.ResumeLayout(false);
        this.groupBox2.PerformLayout();
        this.groupBox5.ResumeLayout(false);
        this.groupBox5.PerformLayout();
        this.groupBox1.ResumeLayout(false);
        this.groupBox1.PerformLayout();
        this.groupBoxBra.ResumeLayout(false);
        this.groupBoxBra.PerformLayout();
        ((System.ComponentModel.ISupportInitialize)(this.dataGridViewRöjningsstallen)).EndInit();
        this.tabPageFrågor.ResumeLayout(false);
        this.tabPageFrågor.PerformLayout();
        this.groupBoxKommentarer.ResumeLayout(false);
        this.groupBoxFråga6.ResumeLayout(false);
        this.groupBoxFråga6.PerformLayout();
        this.groupBoxFråga5.ResumeLayout(false);
        this.groupBoxFråga5.PerformLayout();
        this.groupBoxFråga4.ResumeLayout(false);
        this.groupBoxFråga4.PerformLayout();
        this.groupBoxOBS.ResumeLayout(false);
        this.groupBoxOBS.PerformLayout();
        this.groupBoxFråga3.ResumeLayout(false);
        this.groupBoxFråga3.PerformLayout();
        this.groupBoxFråga2.ResumeLayout(false);
        this.groupBoxFråga2.PerformLayout();
        this.groupBoxFråga1.ResumeLayout(false);
        this.groupBoxFråga1.PerformLayout();
        this.ResumeLayout(false);
        this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.MenuStrip menuStripRöjning;
    private System.Windows.Forms.ToolStripMenuItem arkivToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem sparaToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem sparaSomToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    private System.Windows.Forms.ToolStripMenuItem avslutaToolStripMenuItem;
    private System.Data.DataSet dataSetRöjning;
    private FastReport.Report reportRöjning;
    private System.Data.DataTable dataTableYta;
    private System.Data.DataTable dataTableUserData;
    private System.Data.DataColumn dataColumnYta;
    private System.Data.DataColumn dataColumnTallAntal;
    private System.Data.DataColumn dataColumnTallmedelhojd;
    private System.Data.DataColumn dataColumnContAntal;
    private System.Data.DataColumn dataColumnContMedelhojd;
    private System.Windows.Forms.OpenFileDialog openReportFileDialog;
    private System.Windows.Forms.TabControl tabControl1;
    private System.Windows.Forms.TabPage tabPageYtor;
    private System.Windows.Forms.TabPage tabPageFrågor;
    private System.Data.DataTable dataTableFragor;
    private System.Data.DataColumn dataColumnFraga1;
    private System.Data.DataColumn dataColumnFraga2;
    private System.Data.DataSet dataSetSettings;
    private System.Data.DataTable dataTableRegion;
    private System.Data.DataTable dataTableDistrikt;
    private FastReport.EnvironmentSettings environmentRöjning;
    private System.Data.DataColumn dataColumnSettingsRegionId;
    private System.Data.DataColumn dataColumnSettingsDistriktRegionId;
    private System.Data.DataColumn dataColumnGranAntal;
    private System.Data.DataColumn dataColumnGranMedelhojd;
    private System.Data.DataColumn dataColumnFraga3;
    private System.Data.DataColumn dataColumnFraga4;
    private System.Data.DataColumn dataColumnFraga5;
    private System.Data.DataColumn dataColumnFraga6;
    private System.Data.DataColumn dataColumnOvrigt;
    private System.Data.DataColumn dataColumnBjorkAntal;
    private System.Data.DataColumn dataColumnBjorkMedelhojd;
    private System.Data.DataColumn dataColumnHst;
    private System.Data.DataColumn dataColumnMHojd;
    private System.Data.DataColumn dataColumnSettingsRegionNamn;
    private System.Data.DataColumn dataColumnSettingsDistriktId;
    private System.Data.DataColumn dataColumnSettingsDistriktNamn;
    private System.Data.DataTable dataTableMarkberedningsmetod;
    private System.Data.DataColumn dataColumnSettingsMarkberedningsmetodNamn;
    private System.Data.DataTable dataTableUrsprung;
    private System.Data.DataColumn dataColumndataColumnSettingsUrsprungNamn;
    private System.Data.DataColumn dataColumnRegionId;
    private System.Data.DataColumn dataColumnRegion;
    private System.Data.DataColumn dataColumnDistriktId;
    private System.Data.DataColumn dataColumnDistrikt;
    private System.Data.DataColumn dataColumnUrsprung;
    private System.Data.DataColumn dataColumnDatum;
    private System.Data.DataColumn dataColumnTraktnr;
    private System.Data.DataColumn dataColumnTraktnamn;
    private System.Data.DataColumn dataColumnStandort;
    private System.Data.DataColumn dataColumnEntreprenor;
    private System.Data.DataColumn dataColumnKommentar;
    private System.Data.DataColumn dataColumnLanguage;
    private System.Data.DataColumn dataColumnMailFrom;
    private System.Data.DataColumn dataColumnProvytestorlek;
    private System.Data.DataColumn dataColumnStatus;
    private System.Data.DataColumn dataColumnStatus_Datum;
    private System.Data.DataColumn dataColumnArtal;
    private System.Windows.Forms.FolderBrowserDialog saveReportFolderBrowserDialog;
    private System.Windows.Forms.TabPage tabPageRöjning;
    private System.Windows.Forms.GroupBox groupBoxRegion;
    private System.Windows.Forms.ComboBox comboBoxUrsprung;
    private System.Windows.Forms.DateTimePicker dateTimePicker;
    private System.Windows.Forms.ComboBox comboBoxDistrikt;
    private System.Windows.Forms.ComboBox comboBoxRegion;
    private System.Windows.Forms.Label labelDatum;
    private System.Windows.Forms.Label labelUrsprung;
    private System.Windows.Forms.Label labelDistrikt;
    private System.Windows.Forms.Label labelRegion;
    private System.Windows.Forms.GroupBox groupBoxTrakt;
    private System.Windows.Forms.TextBox textBoxTraktnr;
    private System.Windows.Forms.TextBox textBoxTraktnamn;
    private System.Windows.Forms.TextBox textBoxEntreprenor;
    private System.Windows.Forms.Label labeEntreprenor;
    private System.Windows.Forms.Label labelTraktdel;
    private System.Windows.Forms.Label labelTraktnamn;
    private System.Windows.Forms.Label labelTraktnr;
    private System.Windows.Forms.GroupBox groupBoxRöjning;
    private System.Windows.Forms.Label labelProcent;
    private System.Windows.Forms.TextBox textBoxMåluppfyllelse;
    private System.Windows.Forms.Label labelMåluppfyllelse;
    private System.Windows.Forms.Label labelStHa;
    private System.Windows.Forms.TextBox textBoxMål;
    private System.Windows.Forms.Label labelMål;
    private System.Windows.Forms.GroupBox groupBox13;
    private System.Windows.Forms.Label labelTrädslagsblandning;
    private System.Windows.Forms.TextBox textBoxT;
    private System.Windows.Forms.TextBox textBoxL;
    private System.Windows.Forms.Label labelL;
    private System.Windows.Forms.Label labelT;
    private System.Windows.Forms.TextBox textBoxG;
    private System.Windows.Forms.TextBox textBoxC;
    private System.Windows.Forms.Label labelG;
    private System.Windows.Forms.Label labelC;
    private System.Windows.Forms.Label labelM;
    private System.Windows.Forms.TextBox textBoxBarrhuvudstammarsHojd;
    private System.Windows.Forms.TextBox textBoxProvytestorlekRadie;
    private System.Windows.Forms.Label labelMRadie;
    private System.Windows.Forms.Label labelSt;
    private System.Windows.Forms.Label labelM2;
    private System.Windows.Forms.Label labelProvytestorlek;
    private System.Windows.Forms.TextBox textBoxTotaltHuvudstammar;
    private System.Windows.Forms.Label labelBarrhuvudstammarsMedelhöjd;
    private System.Windows.Forms.Label labelTotaltHuvudstammar;
    private System.Windows.Forms.TextBox textBoxProvytestorlek;
    private System.Data.DataColumn dataColumnMal;
    private System.Windows.Forms.GroupBox groupBoxRöjningsstallen;
    private System.Windows.Forms.GroupBox groupBox6;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.Label labelBjork;
    private System.Windows.Forms.GroupBox groupBox5;
    private System.Windows.Forms.Label labelCont;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.Label labelGran;
    private System.Windows.Forms.GroupBox groupBoxBra;
    private System.Windows.Forms.Label labelTall;
    private System.Windows.Forms.DataGridView dataGridViewRöjningsstallen;
    private System.Windows.Forms.Label labelTotaltAntalYtor;
    private System.Windows.Forms.Label labelProgress;
    private System.Windows.Forms.ProgressBar progressBarData;
    private System.Windows.Forms.Label labelNatur;
    private System.Windows.Forms.GroupBox groupBoxFråga6;
    private System.Windows.Forms.RadioButton radioButton6Ejaktuellt;
    private System.Windows.Forms.RadioButton radioButton6Nej;
    private System.Windows.Forms.RadioButton radioButton6Ja;
    private System.Windows.Forms.Label labelFrågor6;
    private System.Windows.Forms.GroupBox groupBoxFråga5;
    private System.Windows.Forms.RadioButton radioButton5Ejaktuellt;
    private System.Windows.Forms.RadioButton radioButton5Nej;
    private System.Windows.Forms.RadioButton radioButton5Ja;
    private System.Windows.Forms.Label labelFrågor5;
    private System.Windows.Forms.GroupBox groupBoxFråga4;
    private System.Windows.Forms.RadioButton radioButton4Ejaktuellt;
    private System.Windows.Forms.RadioButton radioButton4Nej;
    private System.Windows.Forms.RadioButton radioButton4Ja;
    private System.Windows.Forms.Label labelFrågor4;
    private System.Windows.Forms.GroupBox groupBoxOBS;
    private System.Windows.Forms.Label labelFragaObs;
    private System.Windows.Forms.Label labelOBS;
    private System.Windows.Forms.GroupBox groupBoxFråga3;
    private System.Windows.Forms.RadioButton radioButton3Ejaktuellt;
    private System.Windows.Forms.RadioButton radioButton3Nej;
    private System.Windows.Forms.RadioButton radioButton3Ja;
    private System.Windows.Forms.Label labelFrågor3;
    private System.Windows.Forms.GroupBox groupBoxFråga2;
    private System.Windows.Forms.RadioButton radioButton2Ejaktuellt;
    private System.Windows.Forms.RadioButton radioButton2Nej;
    private System.Windows.Forms.RadioButton radioButton2Ja;
    private System.Windows.Forms.Label labelFrågor2;
    private System.Windows.Forms.GroupBox groupBoxFråga1;
    private System.Windows.Forms.RadioButton radioButton1Nej;
    private System.Windows.Forms.RadioButton radioButton1Ja;
    private System.Windows.Forms.Label labelFrågor1;
    private System.Windows.Forms.GroupBox groupBoxKommentarer;
    private System.Windows.Forms.RichTextBox richTextBoxKommentar;
    private System.Windows.Forms.GroupBox groupBox15;
    private System.Windows.Forms.TextBox textBoxMedelvardeBarrMHojd;
    private System.Windows.Forms.GroupBox groupBox4;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.GroupBox groupBox14;
    private System.Windows.Forms.TextBox textBoxSummaBarrMHojd;
    private System.Windows.Forms.GroupBox groupBox16;
    private System.Windows.Forms.TextBox textBoxMedelvardeHst;
    private System.Windows.Forms.GroupBox groupBox12;
    private System.Windows.Forms.TextBox textBoxSummaHst;
    private System.Windows.Forms.GroupBox groupBox11;
    private System.Windows.Forms.TextBox textBoxSummaContMedelhojd;
    private System.Windows.Forms.TextBox textBoxSummaContAntal;
    private System.Windows.Forms.GroupBox groupBox18;
    private System.Windows.Forms.TextBox textBoxMedelvardeContMedelhojd;
    private System.Windows.Forms.TextBox textBoxMedelvardeContAntal;
    private System.Windows.Forms.GroupBox groupBox10;
    private System.Windows.Forms.TextBox textBoxSummaBjorkMedelhojd;
    private System.Windows.Forms.TextBox textBoxSummaBjorkAntal;
    private System.Windows.Forms.GroupBox groupBox21;
    private System.Windows.Forms.TextBox textBoxMedelvardeTallMedelhojd;
    private System.Windows.Forms.TextBox textBoxMedelvardeTallAntal;
    private System.Windows.Forms.GroupBox groupBox19;
    private System.Windows.Forms.TextBox textBoxMedelvardeBjorkMedelhojd;
    private System.Windows.Forms.TextBox textBoxMedelvardeBjorkAntal;
    private System.Windows.Forms.GroupBox groupBox9;
    private System.Windows.Forms.Label labelSummaAntal;
    private System.Windows.Forms.GroupBox groupBox20;
    private System.Windows.Forms.TextBox textBoxMedelvardeGranMedelhojd;
    private System.Windows.Forms.TextBox textBoxMedelvardeGranAntal;
    private System.Windows.Forms.GroupBox groupBox8;
    private System.Windows.Forms.TextBox textBoxSummaGranMedelhojd;
    private System.Windows.Forms.TextBox textBoxSummaGranAntal;
    private System.Windows.Forms.GroupBox groupBoxSummaTall;
    private System.Windows.Forms.TextBox textBoxSummaTallMedelhojd;
    private System.Windows.Forms.TextBox textBoxSummaTallAntal;
    private System.Data.DataSet dataSetCalculations;
    private System.Data.DataTable dataTableTableMedelvarde;
    private System.Data.DataColumn dataColumnTallMedelAntal;
    private System.Data.DataColumn dataColumnTallMedelMHojd;
    private System.Data.DataColumn dataColumnGranMedelAntal;
    private System.Data.DataColumn dataColumnGranMedelMHojd;
    private System.Data.DataColumn dataColumnBjorkMedelAntal;
    private System.Data.DataColumn dataColumnBjorkMedelMHojd;
    private System.Data.DataColumn dataColumnContMedelAntal;
    private System.Data.DataColumn dataColumnContMedelMHojd;
    private System.Data.DataColumn dataColumnHstMedelAntal;
    private System.Data.DataColumn dataColumnBarrMedelMHojd;
    private System.Data.DataTable dataTableSumma;
    private System.Data.DataColumn dataColumnTallSummaAntal;
    private System.Data.DataColumn dataColumnTallSummaMHojd;
    private System.Data.DataColumn dataColumnGranSummaAntal;
    private System.Data.DataColumn dataColumnGranSummaMHojd;
    private System.Data.DataColumn dataColumnBjorkSummaAntal;
    private System.Data.DataColumn dataColumnBjorkSummaMHojd;
    private System.Data.DataColumn dataColumnContSummaAntal;
    private System.Data.DataColumn dataColumnContSummaMHojd;
    private System.Data.DataColumn dataColumnHstSummaAntal;
    private System.Data.DataColumn dataColumnBarrHstMHojd;
    private System.Windows.Forms.ComboBox comboBoxInvTyp;
    private System.Windows.Forms.Label labelInvTyp;
    private System.Windows.Forms.TextBox textBoxTraktDel;
    private System.Data.DataTable dataTableInvTyp;
    private System.Data.DataColumn dataColumnRojstamAntal;
    private System.Data.DataColumn dataColumnInvTypId;
    private System.Data.DataColumn dataColumnInvTyp;
    private System.Data.DataColumn dataColumnAtgAreal;
    private System.Data.DataColumn dataColumnSettingsInvTypId;
    private System.Data.DataColumn dataColumnSettingsInvTypNamn;
    private System.Windows.Forms.TextBox textBoxAtgAreal;
    private System.Windows.Forms.Label labelAtgArel;
    private System.Windows.Forms.GroupBox groupBox7;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.DataGridViewTextBoxColumn ytaDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn tallAntalDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn tallMedelhojdDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn granAntalDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn granMedelhojdDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn bjorkAntalDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn bjorkMedelhojdDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn contAntalDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn contMedelhojdDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn hstAntalDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn barrMedelhojdDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn RojstamAntalDataGridViewTextBoxColumn;
    private System.Windows.Forms.GroupBox groupBox22;
    private System.Windows.Forms.TextBox textBoxMedelvardeRojstamAntal;
    private System.Windows.Forms.GroupBox groupBox23;
    private System.Windows.Forms.TextBox textBoxSummaRojstamAntal;
    private System.Data.DataColumn dataColumnRojstamMedelAntal;
    private System.Data.DataColumn dataColumnRojstamSummaAntal;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.TextBox textBoxLovhuvudstammarsHojd;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.GroupBox groupBox3;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.TextBox textBoxDistrikt;
    private System.Windows.Forms.TextBox textBoxRegion;
  }
}

