﻿#region

using System;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using Egenuppfoljning.Interfaces;
using Egenuppfoljning.Properties;
using FastReport.Export.OoXML;
using FastReport.Export.Pdf;
using Microsoft.Office.Interop.Outlook;
using Application = System.Windows.Forms.Application;
using Attachment = System.Net.Mail.Attachment;
using Exception = System.Exception;

#endregion

namespace Egenuppfoljning.Applications
{
  /// <summary>
  ///   PC Application for egenuppföljning - Gallring.
  /// </summary>
  public partial class GallringForm : Form, IKorusRapport
  {
    //Epost
    private const int CP_NOCLOSE_BUTTON = 0x200;
    private string mEpostAdress;
    private string mMeddelande;

    private string mSDefaultExcel2007Path;
    private string mSDefaultPdfPath;
    private string mSDefaultReportsPath;
    private string mSSettingsFilePath;
    private string mSSettingsPath;
    private string mTitel;

    /// <summary>
    ///   Constructor, initiate data.
    /// </summary>
    public GallringForm()
    {
      //Select the language that should be used.
      Thread.CurrentThread.CurrentCulture = new CultureInfo(Settings.Default.SettingsLanguage);
      Thread.CurrentThread.CurrentUICulture = new CultureInfo(Settings.Default.SettingsLanguage);
      InitializeComponent();
    }

    public GallringForm(bool aRedigeraLagratData)
    {
      //Select the language that should be used.
      Thread.CurrentThread.CurrentCulture = new CultureInfo(Settings.Default.SettingsLanguage);
      Thread.CurrentThread.CurrentUICulture = new CultureInfo(Settings.Default.SettingsLanguage);
      InitializeComponent();
      sparaSomToolStripMenuItem.Visible = false;

      //nyckelvärden kan ej ändras.
      comboBoxRegion.Enabled = false;
      comboBoxDistrikt.Enabled = false;
      textBoxTraktnr.Enabled = false;
      comboBoxStandort.Enabled = false;
      textBoxEntreprenor.Enabled = false;
      dateTimePicker.Enabled = false;
      sparaToolStripMenuItem.Text = Resources.Lagra_andrat_data;

      RedigeraLagratData = aRedigeraLagratData;

      if (RedigeraLagratData)
      {
          comboBoxRegion.Visible = false;
          comboBoxDistrikt.Visible = false;
          textBoxRegion.Visible = true;
          textBoxDistrikt.Visible = true;
      }
      else
      {
          comboBoxRegion.Visible = true;
          comboBoxDistrikt.Visible = true;
          textBoxRegion.Visible = false;
          textBoxDistrikt.Visible = false;
      }
    }


    // Avvaktivera stäng knappen, för att få kontrollen att kunna välja att inte lagra data ifall Avsluta valet i menyn väljs

    protected override CreateParams CreateParams
    {
      get
      {
        var myCp = base.CreateParams;
        myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
        return myCp;
      }
    }

    #region IKorusRapport Members

    public bool RedigeraLagratData { get; private set; }

    public void LaddaForm(string aRapportSökväg)
    {
      //mSSettingsPath = Application.StartupPath + Settings.Default.SettingsPath;
      //mSSettingsFilePath = Application.StartupPath + Settings.Default.SettingsPath + Settings.Default.SettingsFilename +
      //                     "." + Settings.Default.SettingsLanguage + Resources.Xml_suffix;
        mSSettingsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + Settings.Default.SettingsPath;
        mSSettingsFilePath = mSSettingsPath + Settings.Default.SettingsFilename +
                             "." + Settings.Default.SettingsLanguage + Resources.Xml_suffix;
      
        mSDefaultExcel2007Path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                               Settings.Default.ReportsPath + Settings.Default.Excel2007Path;
      mSDefaultPdfPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                         Settings.Default.ReportsPath + Settings.Default.PdfPath;
      InitFastReportMail();

      if (aRapportSökväg != null)
      {
        Settings.Default.ReportFilePath = aRapportSökväg;
        mSDefaultReportsPath = Application.StartupPath + Settings.Default.ReportsPath;
        LoadGallring();
      }
      else
      {
        ReadSettingsXmlFile();
        InitData();
        InitEmptyTables();
      }
    }

    public void UppdateraData(KorusDataEventArgs aData)
    {
      try
      {
        dataSetGallring.Clear();
        //userdata
        DateTime datum;
        DateTime.TryParse(aData.data.datum, out datum);

        dataSetGallring.Tables["UserData"].Rows.Add(aData.data.regionid, aData.data.regionnamn, aData.data.distriktid,
                                                    aData.data.distriktnamn, aData.data.ursprung, datum,
                                                    aData.data.traktnr, aData.data.traktnamn, aData.data.standort,
                                                    aData.data.entreprenor, aData.data.kommentar, string.Empty,
                                                    string.Empty, aData.data.malgrundyta, aData.data.stickvagsavstand,
                                                    Resources.Lagrad, DateTime.MinValue, aData.data.årtal,
                                                    aData.data.areal, aData.data.stickvagssystem);
        dataSetGallring.Tables["Fragor"].Rows.Add(aData.frågeformulär.fraga1, aData.frågeformulär.fraga2,
                                                  aData.frågeformulär.fraga3, aData.frågeformulär.fraga4,
                                                  aData.frågeformulär.fraga5, aData.frågeformulär.fraga6,
                                                  aData.frågeformulär.fraga7, aData.frågeformulär.fraga8,
                                                  aData.frågeformulär.fraga9, aData.frågeformulär.ovrigt);

        //ytor
        foreach (var yta in aData.ytor)
        {
          dataSetGallring.Tables["Yta"].Rows.Add(yta.yta, yta.tall, yta.gran, yta.lov, yta.contorta, yta.summa,
                                                 yta.skador, yta.stickvbredd);
        }

        UpdateGUIData();
      }
      catch (Exception ex)
      {
#if !DEBUG
     MessageBox.Show("Misslyckades med att läsa in data", "Läsa data misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    public void ShowEditDialog(bool aRedigeraLagratData)
    {
      RedigeraLagratData = aRedigeraLagratData;
      ShowDialog();
    }

    public int GetTotalaAntaletYtor()
    {
      if (dataSetGallring != null && dataSetGallring.Tables["Yta"] != null)
      {
        return dataSetGallring.Tables["Yta"].Rows.Count;
      }
      return 0;
    }

    public int GetProcentStatus()
    {
      return (int) ((progressBarData.Value/(double) progressBarData.Maximum)*100);
    }

    public string GetEgenuppföljningsTyp()
    {
      return Resources.Gallring;
    }

    public void SkrivUt()
    {
      try
      {
        reportGallring.Print();
      }
      catch (Exception ex)
      {
#if !DEBUG
       MessageBox.Show("Skriva ut rapport misslyckades", "Skriva ut misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    public KorusDataEventArgs GetKORUSData()
    {
      var obj = new KorusDataEventArgs();

      //Nyckelvariabler
      int regionid, distriktid, traktnr, standort, entreprenor;
      int.TryParse(GetDsStr("UserData", "RegionId"), out regionid);
      int.TryParse(GetDsStr("UserData", "DistriktId"), out distriktid);
      int.TryParse(GetDsStr("UserData", "Traktnr"), out traktnr);
      int.TryParse(GetDsStr("UserData", "Standort"), out standort);
      int.TryParse(GetDsStr("UserData", "Entreprenor"), out entreprenor);
      var årtal = GetDatumÅr(GetDsStr("UserData", "Datum"));

      //UserData
      obj.data.regionid = regionid; //int
      obj.data.distriktid = distriktid;
      obj.data.traktnr = traktnr;
      obj.data.standort = standort;
      obj.data.entreprenor = entreprenor;
      obj.data.rapport_typ = (int) RapportTyp.Gallring;
      obj.data.årtal = årtal;

      obj.data.traktnamn = GetDsStr("UserData", "Traktnamn");
      obj.data.regionnamn = GetDsStr("UserData", "Region");
      obj.data.distriktnamn = GetDsStr("UserData", "Distrikt");
      obj.data.ursprung = GetDsStr("UserData", "Ursprung");
      obj.data.datum = GetDsStr("UserData", "Datum");
      obj.data.kommentar = GetDsStr("UserData", "Kommentar");
      float.TryParse(GetDsStr("UserData", "Areal"), out obj.data.areal);
      int.TryParse(GetDsStr("UserData", "Malgrundyta"), out obj.data.malgrundyta);
      obj.data.stickvagssystem = GetDsStr("UserData", "Stickvagssystem");
      int.TryParse(GetDsStr("UserData", "Stickvagsavstand"), out obj.data.stickvagsavstand);

      obj.frågeformulär.fraga1 = GetDsStr("Fragor", "Fraga1");
      obj.frågeformulär.fraga2 = GetDsStr("Fragor", "Fraga2");
      obj.frågeformulär.fraga3 = GetDsStr("Fragor", "Fraga3");
      obj.frågeformulär.fraga4 = GetDsStr("Fragor", "Fraga4");
      obj.frågeformulär.fraga5 = GetDsStr("Fragor", "Fraga5");
      obj.frågeformulär.fraga6 = GetDsStr("Fragor", "Fraga6");
      obj.frågeformulär.fraga7 = GetDsStr("Fragor", "Fraga7");
      obj.frågeformulär.fraga8 = GetDsStr("Fragor", "Fraga8");
      obj.frågeformulär.fraga9 = GetDsStr("Fragor", "Fraga9");
      obj.frågeformulär.ovrigt = GetDsStr("Fragor", "Ovrigt");

      //Ytor
      if (GetNrOfRows("Yta") > 0)
      {
        for (var i = 0; i < GetNrOfRows("Yta"); i++)
        {
          var yta = new KorusDataEventArgs.Yta();

          int.TryParse(GetDsStr("Yta", "Yta", i, dataSetGallring), out yta.yta); //löpnummer
          float.TryParse(GetDsStr("Yta", "Stickvbredd", i, dataSetGallring), out yta.stickvbredd);
          int.TryParse(GetDsStr("Yta", "Tall", i, dataSetGallring), out yta.tall);
          int.TryParse(GetDsStr("Yta", "Gran", i, dataSetGallring), out yta.gran);
          int.TryParse(GetDsStr("Yta", "Lov", i, dataSetGallring), out yta.lov);
          int.TryParse(GetDsStr("Yta", "Contorta", i, dataSetGallring), out yta.contorta);
          int.TryParse(GetDsStr("Yta", "Summa", i, dataSetGallring), out yta.summa);
          int.TryParse(GetDsStr("Yta", "Skador", i, dataSetGallring), out yta.skador);

          obj.ytor.Add(yta);
        }
      }

      return obj;
    }

    public void ShowReport()
    {
      try
      {
        reportGallring.Show();
      }
      catch (Exception ex)
      {
#if !DEBUG
        MessageBox.Show(Resources.ForhandgranskningFailed); 
#else
        MessageBox.Show(Resources.ForhandgranskningFailed + ex);
#endif
      }
    }


    public void ExporteraExcel2007()
    {
      try
      {
        InitExportDirs();
        //kolla först så användaren har skrivit in det datat som krävs för ett giltigt filnamn.        
        reportGallring.Prepare();
        var export = new Excel2007Export();
        // Visa export alternativ
        if (!export.ShowDialog()) return;

        var filePath = ShowSaveAsDialog(mSDefaultExcel2007Path, Resources.Excel_suffix);
        if (filePath == null) return;
        reportGallring.Export(export, filePath);
        MessageBox.Show(Resources.ExportRapport + filePath
                        , Resources.Exportering_lyckades, MessageBoxButtons.OK, MessageBoxIcon.Information);
      }
      catch (Exception ex)
      {
#if !DEBUG
          MessageBox.Show("Misslyckades med att exportera ut data till formatet: Excel 2007");
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    public void ExporteraAdobePDF()
    {
      try
      {
        InitExportDirs();
        //kolla först så användaren har skrivit in det datat som krävs för ett giltigt filnamn.        
        reportGallring.Prepare();
        var export = new PDFExport();
        // Visa export alternativ
        if (!export.ShowDialog()) return;

        var filePath = ShowSaveAsDialog(mSDefaultPdfPath, Resources.Pdf_suffix);
        if (filePath == null) return;
        reportGallring.Export(export, filePath);
        MessageBox.Show(Resources.ExportRapport + filePath
                        , Resources.Exportering_lyckades, MessageBoxButtons.OK, MessageBoxIcon.Information);
      }
      catch (Exception ex)
      {
#if !DEBUG
          MessageBox.Show("Misslyckades med att exportera ut data till formatet: Pdf");
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    /// <summary>
    ///   Writes the data that has been selected in the GUI.
    /// </summary>
    public bool WriteDataToReportXmlFile(string aFilePath, bool aShowDialog, bool aWriteSchema, string aReportStatus,
                                         string aReportStatusDatum)
    {
      XmlTextWriter objXmlReportWriter = null;
      XmlTextWriter objXmlSchemReportWriter = null;

      // UpdateDecimal(textBoxAreal);

      if (!aWriteSchema && aFilePath != null)
      {
        //check so the correct data exsist for the report filename
        var reportName = GetReportFileName();
        var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(aFilePath);
        if (fileNameWithoutExtension != null && (reportName != null && !fileNameWithoutExtension.Equals(reportName)))
        {
          aFilePath = ShowSaveAsDialog(Settings.Default.ReportFilePath.Equals(string.Empty)
                                         ? mSDefaultReportsPath
                                         : Path.GetDirectoryName(Settings.Default.ReportFilePath), Resources.keg);
        }
      }

      if (aFilePath == null || aFilePath.Trim().Length <= 0)
      {
        return false;
      }

      InitEmptyTables();

      try
      {
        if (aWriteSchema)
        {
          objXmlSchemReportWriter = new XmlTextWriter(aFilePath, null) {Formatting = Formatting.Indented};
          objXmlSchemReportWriter.WriteStartDocument();
          dataSetGallring.WriteXmlSchema(objXmlSchemReportWriter);
          objXmlSchemReportWriter.WriteEndDocument();
          objXmlSchemReportWriter.Flush();
        }
        else
        {
          objXmlReportWriter = new XmlTextWriter(aFilePath, null) {Formatting = Formatting.Indented};
          objXmlReportWriter.WriteStartDocument();
          SetDsStr("UserData", "Language", Settings.Default.SettingsLanguage);
          SetDsStr("UserData", "Status", aReportStatus);
          SetDsStr("UserData", "Status_Datum", aReportStatusDatum);
          dataSetGallring.WriteXml(objXmlReportWriter);
          objXmlReportWriter.WriteEndDocument();
          objXmlReportWriter.Flush();
          Settings.Default.ReportFilePath = aFilePath;

          if (aShowDialog)
          {
            MessageBox.Show(String.Format(Resources.RapportenSparadMessage, Path.GetFileName(aFilePath))
                            , Resources.RapportSparadTitel, MessageBoxButtons.OK, MessageBoxIcon.Information);
          }
        }
      }
      catch (Exception ex)
      {
#if !DEBUG
     MessageBox.Show("Skriva data till rapportfil misslyckades", "Spara misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
        return false;
      }
      finally
      {
        if (objXmlReportWriter != null)
        {
          objXmlReportWriter.Close();
        }

        if (objXmlSchemReportWriter != null)
        {
          objXmlSchemReportWriter.Close();
        }
      }
      return true;
    }

    /// <summary>
    ///   Checks the values in the GUI and puts together a name from the data as follows: [rapport_typ]_[regionid]_[distriktid]_[traktnr]_[ståndort]_[entreprenör]_[årtal].keg
    /// </summary>
    /// <returns> The filename. </returns>
    public string GetReportFileName()
    {
      var missingData = "";
      var filename = Resources.GallringPrefix; //Rapport typ

      //Region id
      var regionid = GetDsStr("UserData", "RegionId");

      if (regionid.Equals(string.Empty) || regionid.Equals("0") || regionid.Equals("-1"))
      {
        missingData += " " + Resources.RegionId;
      }
      else
      {
        filename += Resources.FileSep + regionid;
      }

      //Distrikt id
      var distriktid = GetDsStr("UserData", "DistriktId");

      if (distriktid.Equals(string.Empty) || distriktid.Equals("0") || distriktid.Equals("-1"))
      {
        missingData += " " + Resources.DistriktId;
      }
      else
      {
        filename += Resources.FileSep + distriktid;
      }

      //Artal
      DateTime date;
      DateTime.TryParse(GetDsStr("UserData", "Datum"), out date);

      if (date.Equals(DateTime.MinValue))
      {
        missingData += " " + Resources.Artal;
      }
      else
      {
        filename += Resources.FileSep + date.Year.ToString(CultureInfo.InvariantCulture);
      }

      //Traktnr
      var traktnr = GetDsStr("UserData", "Traktnr");
      if (traktnr.Equals(string.Empty) || traktnr.Equals("0") || traktnr.Equals("-1"))
      {
        missingData += " " + Resources.Traktnr;
      }
      else
      {
        filename += Resources.FileSep + traktnr;
      }

      //Ståndort
      var ståndort = GetDsStr("UserData", "Standort");
      if (ståndort.Equals(string.Empty) || ståndort.Equals("0") || ståndort.Equals("-1"))
      {
        missingData += " " + Resources.Standort;
      }
      else
      {
        filename += Resources.FileSep + ståndort;
      }


      //Entreprenörnr
      var entreprenör = GetDsStr("UserData", "Entreprenor");
      if (entreprenör.Equals(string.Empty) || entreprenör.Equals("0") || entreprenör.Equals("-1"))
      {
        missingData += " " + Resources.Entreprenor;
      }
      else
      {
        filename += Resources.FileSep + entreprenör;
      }

      if (!missingData.Equals(string.Empty))
      {
        //some data is missing to create the file name
        var errMessage = "Följande data saknas för att kunna spara rapporten: ";
        errMessage += missingData;
        errMessage += "\nFyll i dessa data och välj spara igen.";
        MessageBox.Show(errMessage, Resources.Data_saknas, MessageBoxButtons.OK, MessageBoxIcon.Stop);

        return null;
      }
      return filename;
    }

    public string GetDsStr(string aTable, string aColumn)
    {
      return GetDsStr(aTable, aColumn, 0, dataSetGallring);
    }

    public void SändEpost(string aEpostAdress, string aTitel, string aMeddelande, bool aAnvändInternEpost,
                          bool aBifogRapport)
    {
      mEpostAdress = aEpostAdress;
      mTitel = aTitel;
      mMeddelande = aMeddelande;

      if (aAnvändInternEpost)
      {
        SendInternMail(aBifogRapport);
      }
      else
      {
        SendViaOutlook(aBifogRapport);
      }
    }

    #endregion

    private void avslutaToolStripMenuItem_Click(object sender, EventArgs e)
    {
      DataHelper.TvingaDataGridViewAttValidera(dataGridViewGallringsstallen);
      DataSetParser.TabortFelaktigaYtor(dataSetGallring);

      RedigeraLagratData = false; //uppdatera inte databasen
      UpdateDecimal(textBoxAreal);
      Hide();
    }

    private void WriteSettingsXmlFile(bool aWriteSchema)
    {
      XmlTextWriter objXmlSettingsWriter = null;
      XmlTextWriter objXmlSettingsSchemaWriter = null;
      InitSettingsData();

      try
      {
        if (aWriteSchema)
        {
          objXmlSettingsSchemaWriter = new XmlTextWriter(mSSettingsPath + Settings.Default.SettingsSchemaFilename, null)
            {Formatting = Formatting.Indented};
          objXmlSettingsSchemaWriter.WriteStartDocument();
          dataSetSettings.WriteXmlSchema(objXmlSettingsSchemaWriter);
          objXmlSettingsSchemaWriter.WriteEndDocument();
          objXmlSettingsSchemaWriter.Flush();
        }
        else
        {
          objXmlSettingsWriter = new XmlTextWriter(mSSettingsFilePath, null) {Formatting = Formatting.Indented};
          objXmlSettingsWriter.WriteStartDocument();
          dataSetSettings.WriteXml(objXmlSettingsWriter);
          objXmlSettingsWriter.WriteEndDocument();
          objXmlSettingsWriter.Flush();
        }
      }
      catch (Exception ex)
      {
#if !DEBUG
       MessageBox.Show("Spara Settings data misslyckades", "Spara misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
      finally
      {
        if (objXmlSettingsWriter != null)
        {
          objXmlSettingsWriter.Close();
        }
        if (objXmlSettingsSchemaWriter != null)
        {
          objXmlSettingsSchemaWriter.Close();
        }
      }
    }

    /// <summary>
    ///   Read the XML file containing the settings for the Application.
    /// </summary>
    private void ReadSettingsXmlFile()
    {
      InitSettingsData();

      var objValidator = new XmlValidator(mSSettingsFilePath,
                                          mSSettingsPath + Settings.Default.SettingsSchemaFilename);

      if (objValidator.ValidateXmlFileFailed())
      {
        //something is wrong with the Settings file then write a new Settings and Schema file.
        WriteSettingsXmlFile(false);
        WriteSettingsXmlFile(true);
      }

      try
      {
        dataSetSettings.Clear();
        dataSetSettings.ReadXml(mSSettingsFilePath);
      }
      catch (Exception ex)
      {
#if !DEBUG
       MessageBox.Show("Läsa Settings data misslyckades", "Läsa misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    /// <summary>
    ///   Initializes the report data tables.
    /// </summary>
    private void InitEmptyTables()
    {
      if (dataSetGallring.Tables["UserData"].Rows.Count == 0)
      {
        dataSetGallring.Tables["UserData"].Rows.Add(0, string.Empty, 0, string.Empty, string.Empty, DateTime.Today, 0,
                                                    string.Empty, 0, 0, string.Empty, string.Empty, string.Empty, 0, 0,
                                                    Resources.Obehandlad, DateTime.MinValue, DateTime.Today.Year);
      }

      if (dataSetGallring.Tables["Fragor"].Rows.Count == 0)
      {
        dataSetGallring.Tables["Fragor"].Rows.Add(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty,
                                                  string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);
      }

      if (dataSetGallring.Tables["Yta"].Rows.Count == 0)
      {
//If no rows exist then add a empty one before save.
        dataSetGallring.Tables["Yta"].Rows.Add(1, 0, 0, 0, 0, 0, 0, 0.0);
      }

      if (dataSetCalculations.Tables["Medelvarde"].Rows.Count == 0)
      {
        dataSetCalculations.Tables["Medelvarde"].Rows.Add(0.0);
      }
      StatusFrågor();
    }

    /// <summary>
    ///   Sets a hardcoded default settingsdata.
    /// </summary>
    private void DefaultSettingsData()
    {
        dataSetSettings.Tables["Region"].Rows.Add("11", "11 - Nord");
        dataSetSettings.Tables["Region"].Rows.Add("16", "16 - Mitt");
        dataSetSettings.Tables["Region"].Rows.Add("18", "18 - Syd");

        dataSetSettings.Tables["Distrikt"].Rows.Add("11", "14", "14 - Örnsköldsvik");
        dataSetSettings.Tables["Distrikt"].Rows.Add("11", "19", "19 - Umeå");
        dataSetSettings.Tables["Distrikt"].Rows.Add("16", "12", "12 - Sveg");
        dataSetSettings.Tables["Distrikt"].Rows.Add("16", "14", "14 - Ljusdal");
        dataSetSettings.Tables["Distrikt"].Rows.Add("16", "15", "15 - Delsbo");
        dataSetSettings.Tables["Distrikt"].Rows.Add("16", "16", "16 - Hudiksvall");
        dataSetSettings.Tables["Distrikt"].Rows.Add("16", "18", "18 - Bollnäs");
        dataSetSettings.Tables["Distrikt"].Rows.Add("16", "21", "21 - Uppland");
        dataSetSettings.Tables["Distrikt"].Rows.Add("18", "10", "10 - Västerås");
        dataSetSettings.Tables["Distrikt"].Rows.Add("18", "11", "11 - Örebro");
        dataSetSettings.Tables["Distrikt"].Rows.Add("18", "12", "12 - Nyköping");
        dataSetSettings.Tables["Distrikt"].Rows.Add("18", "13", "13 - Götaland");
        dataSetSettings.Tables["Distrikt"].Rows.Add("18", "21", "21 - Egen Skog");

      dataSetSettings.Tables["Standort"].Rows.Add("1");
      dataSetSettings.Tables["Standort"].Rows.Add("2");
      dataSetSettings.Tables["Standort"].Rows.Add("3");
      dataSetSettings.Tables["Standort"].Rows.Add("4");
      dataSetSettings.Tables["Standort"].Rows.Add("5");
      dataSetSettings.Tables["Standort"].Rows.Add("6");

      dataSetSettings.Tables["Markberedningsmetod"].Rows.Add("Kontinuerligt harv");
      dataSetSettings.Tables["Markberedningsmetod"].Rows.Add("Högläggning");
      dataSetSettings.Tables["Markberedningsmetod"].Rows.Add("För sådd");

      dataSetSettings.Tables["Ursprung"].Rows.Add("Eget");
      dataSetSettings.Tables["Ursprung"].Rows.Add("Köp");
    }

    /// <summary>
    ///   Initializes the settings data tables.
    /// </summary>
    private void InitSettingsData()
    {
      if (dataSetSettings.Tables["Region"].Rows.Count == 0)
      {
        dataSetSettings.Tables["Region"].Rows.Add(0, string.Empty);
      }

      if (dataSetSettings.Tables["Distrikt"].Rows.Count == 0)
      {
        dataSetSettings.Tables["Distrikt"].Rows.Add(0, 0, string.Empty);
      }

      if (dataSetSettings.Tables["Standort"].Rows.Count == 0)
      {
        dataSetSettings.Tables["Standort"].Rows.Add(0);
      }

      if (dataSetSettings.Tables["Markberedningsmetod"].Rows.Count == 0)
      {
        dataSetSettings.Tables["Markberedningsmetod"].Rows.Add(string.Empty);
      }

      if (dataSetSettings.Tables["Ursprung"].Rows.Count == 0)
      {
        dataSetSettings.Tables["Ursprung"].Rows.Add(string.Empty);
      }
    }

    /// <summary>
    ///   first check the language in the file, if it is a new language, then give a question to restart with that file
    /// </summary>
    /// <param name="aFilePath"> File path to the report. </param>
    /// <param name="aStartup"> Flag that indicates if it is load in startup or not </param>
    /// <returns> true if Language check is ignored (same language) false if user selected cancel on the dialog. </returns>
    private static bool CheckLanguage(string aFilePath, bool aStartup)
    {
      if (!aStartup)
      {
        using (var tmpSet = new DataSet())
        {
          tmpSet.ReadXml(aFilePath);
          if (tmpSet.Tables["UserData"].Rows[0]["Language"] != null)
          {
            var language = tmpSet.Tables["UserData"].Rows[0]["Language"].ToString();
            if (!language.Equals(Settings.Default.SettingsLanguage))
            {
              if (MessageBox.Show(
                String.Format(
                  "Rapporten {0} är inställd på språket {1} ({2}). Vill du ändra till det språket och ladda rapporten?",
                  Path.GetFileName(aFilePath), language, GetLanguage(language))
                , Resources.Andra_sprak, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
              {
                Settings.Default.ReportFilePath = aFilePath;
                ChangeLanguage(language);
              }
              else
              {
                //User cancel, then cancel to load of the report, 
                return false;
              }
            }
          }
        }
      }
      return true;
    }

    private void LäggTillHögstaVärdetFörYta(int aRowIndex)
    {
      try
      {
        var slNr =
          DataSetParser.GetObjIntValue(
            dataGridViewGallringsstallen.Rows[aRowIndex].Cells[ytaDataGridViewTextBoxColumn.Name].Value);

        if (slNr >= 1 || dataGridViewGallringsstallen.Rows.Count <= 0) return;

//Yta får inte vara mindre än 1, leta upp det högsta Yta och fortsätt på det
        var maxValue = 0;
        for (var i = 0; i < dataGridViewGallringsstallen.Rows.Count; i++)
        {
          var value =
            DataSetParser.GetObjIntValue(
              dataGridViewGallringsstallen.Rows[i].Cells[ytaDataGridViewTextBoxColumn.Name].Value);

          if (value > maxValue)
          {
            maxValue = value;
          }
        }
        //lägg på +1 och sätt in det i sista raden för slNr
        dataGridViewGallringsstallen.Rows[aRowIndex].Cells[ytaDataGridViewTextBoxColumn.Name].Value = maxValue + 1;

        if (!dataGridViewGallringsstallen.Rows[aRowIndex].Cells[ytaDataGridViewTextBoxColumn.Name].Value.ToString().
                                                                                                   Equals(string.Empty))
          return;

//inget nytt yt nummer kunde anges, en bugg som kan hända i tabellkomponenten, i så fall ta bort raden.
        if (aRowIndex >= 0 && aRowIndex < (dataGridViewGallringsstallen.Rows.Count - 2))
        {
          dataGridViewGallringsstallen.Rows.RemoveAt(aRowIndex);
        }
      }
      catch (Exception ex)
      {
#if !DEBUG
       MessageBox.Show("Misslyckades med att öka nummret för yta.", "Kunde inte sätta yt-numret.", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    private void UpdateGUIData()
    {
      foreach (DataGridViewRow row in dataGridViewGallringsstallen.Rows)
      {
        UpdateSumma(row);
      }

      CalculateValues();
      StatusFrågor();
      ProgressNumberOfDataDone();
    }

    /// <summary>
    ///   Reads the active Report XML document.
    /// </summary>
    private void ReadDataFromReportXmlFile(string aFilePath, bool aStartup)
    {
      InitEmptyTables();

      if (!File.Exists(aFilePath))
      {
        //Return if the file does not exists.
        return;
      }

      if (!File.Exists(mSSettingsPath + Settings.Default.ReportSchemaGallring))
      {
        //Write a new Report Schema file if it does not exist.
        WriteDataToReportXmlFile(mSSettingsPath + Settings.Default.ReportSchemaGallring, false, true,
                                 Settings.Default.ReportStatus, Settings.Default.ReportStatusDatum);
      }

      var objValidator = new XmlValidator(aFilePath, mSSettingsPath + Settings.Default.ReportSchemaGallring);

      if (objValidator.ValidateXmlFileFailed())
      {
        //something is wrong with the XML file.
        if (
          MessageBox.Show(Resources.EjGiltigFil, Resources.FelaktigKegFil, MessageBoxButtons.YesNo,
                          MessageBoxIcon.Question) == DialogResult.No)
        {
          return;
        }
      }

      try
      {
        //first check the language.
        if (!CheckLanguage(aFilePath, aStartup)) return;

        dataSetGallring.Clear();
        dataSetGallring.ReadXml(aFilePath);
        UpdateGUIData();
        Settings.Default.ReportFilePath = aFilePath;
      }
      catch (Exception ex)
      {
#if !DEBUG
     MessageBox.Show("Läsa data från rapport fil misslyckades", "Läsa data misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    private void InitExportDirs()
    {
      if (!Directory.Exists(mSDefaultExcel2007Path))
      {
        //Report dir does not exists, create it.
        Directory.CreateDirectory(mSDefaultExcel2007Path);
      }
      if (!Directory.Exists(mSDefaultPdfPath))
      {
        //Report dir does not exists, create it.
        Directory.CreateDirectory(mSDefaultPdfPath);
      }
    }

    /// <summary>
    ///   Checks so the application got the required files and directories otherwise create them.
    /// </summary>
    private void InitApplicationDirFilesStructure()
    {
      if (!Directory.Exists(mSSettingsPath))
      {
        //Settings dir does not exists, create it.
        Directory.CreateDirectory(mSSettingsPath);
      }

      if (!Directory.Exists(mSDefaultReportsPath))
      {
        //Report dir does not exists, create it.
        Directory.CreateDirectory(mSDefaultReportsPath);
      }

      if (!File.Exists(mSSettingsFilePath))
      {
        //Settings xml file does not exist, create a default file.
        DefaultSettingsData();
        WriteSettingsXmlFile(false);
      }
      if (!File.Exists(mSSettingsPath + Settings.Default.SettingsSchemaFilename))
      {
        //The schema settings file is missing, create it.
        WriteSettingsXmlFile(true);
      }

      if (!File.Exists(mSSettingsPath + Settings.Default.ReportSchemaGallring))
      {
        //The schema report file is missing, create it.
        WriteDataToReportXmlFile(mSSettingsPath + Settings.Default.ReportSchemaGallring, false, true,
                                 Settings.Default.ReportStatus, Settings.Default.ReportStatusDatum);
      }
    }

    private int GetNrOfRows(string aTable)
    {
      try
      {
        if (dataSetGallring != null && dataSetGallring.Tables[aTable] != null)
        {
          return dataSetGallring.Tables[aTable].Rows.Count;
        }
      }
      catch (Exception)
      {
//ignore 
      }
      return 0;
    }

    private static int GetDatumÅr(object aDatum)
    {
      if (aDatum != null && !aDatum.ToString().Equals(string.Empty))
      {
        DateTime datum;
        DateTime.TryParse(aDatum.ToString(), out datum);

        if (!datum.Equals(DateTime.MinValue))
        {
          return datum.Year;
        }
      }
      return 0;
    }

    private void LoadGallring()
    {
      InitApplicationDirFilesStructure();
      ReadSettingsXmlFile();
      InitData();
      ReadDataFromReportXmlFile(Settings.Default.ReportFilePath, true);
    }

    /// <summary>
    ///   Called when the applications starts.
    /// </summary>
    /// <param name="sender"> Sender object </param>
    /// <param name="e"> Event arguments </param>
    private void Gallring_Load(object sender, EventArgs e)
    {
    }

    /// <summary>
    ///   Adds the rows together and calculates the different sums in the table and shows it to the user.
    /// </summary>
    private void UpdateSumValues()
    {
      int sumTall = 0, sumGran = 0, sumLov = 0, sumContorta = 0, sumSumma = 0, sumSkador = 0;
      var sumStickvbredd = 0.0;

      try
      {
        for (var i = 0; i < AntalYtor(); i++)
        {
          sumStickvbredd +=
            DataSetParser.GetObjDoubleValue(
              dataGridViewGallringsstallen.Rows[i].Cells[stickvbreddDataGridViewTextBoxColumn.Name].Value, true);
          sumTall +=
            DataSetParser.GetObjIntValue(
              dataGridViewGallringsstallen.Rows[i].Cells[tallDataGridViewTextBoxColumn.Name].Value, true);
          sumGran +=
            DataSetParser.GetObjIntValue(
              dataGridViewGallringsstallen.Rows[i].Cells[granDataGridViewTextBoxColumn.Name].Value, true);
          sumLov +=
            DataSetParser.GetObjIntValue(
              dataGridViewGallringsstallen.Rows[i].Cells[lovDataGridViewTextBoxColumn.Name].Value, true);
          sumContorta +=
            DataSetParser.GetObjIntValue(
              dataGridViewGallringsstallen.Rows[i].Cells[contortaDataGridViewTextBoxColumn.Name].Value, true);
          sumSumma +=
            DataSetParser.GetObjIntValue(
              dataGridViewGallringsstallen.Rows[i].Cells[summaDataGridViewTextBoxColumn.Name].Value, true);
          sumSkador +=
            DataSetParser.GetObjIntValue(
              dataGridViewGallringsstallen.Rows[i].Cells[skadorDataGridViewTextBoxColumn.Name].Value, true);
        }

        //Update the total sum textboxes 
        textBoxSummaStickvbredd.Text = Math.Round(sumStickvbredd, 1).ToString(CultureInfo.InvariantCulture);
        textBoxSummaTall.Text = sumTall.ToString(CultureInfo.InvariantCulture);
        textBoxSummaGran.Text = sumGran.ToString(CultureInfo.InvariantCulture);
        textBoxSummaLov.Text = sumLov.ToString(CultureInfo.InvariantCulture);
        textBoxSummaContorta.Text = sumContorta.ToString(CultureInfo.InvariantCulture);
        textBoxSumma.Text = sumSumma.ToString(CultureInfo.InvariantCulture);
        textBoxSummaSkador.Text = sumSkador.ToString(CultureInfo.InvariantCulture);

        ProgressNumberOfDataDone();
      }
      catch (Exception ex)
      {
#if !DEBUG
       MessageBox.Show("Beräkna summa värden misslyckades.", "Beräkning misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }


    /// <summary>
    ///   Updates the "Optimalt+Bra" value in the tabel.
    /// </summary>
    /// <param name="aRow"> The DataGridView row </param>
    private void UpdateSumma(DataGridViewRow aRow)
    {
      try
      {
        var valueTall = DataSetParser.GetObjIntValue(aRow.Cells[tallDataGridViewTextBoxColumn.Name].Value, true);
        var valueGran = DataSetParser.GetObjIntValue(aRow.Cells[granDataGridViewTextBoxColumn.Name].Value, true);
        var valueLov = DataSetParser.GetObjIntValue(aRow.Cells[lovDataGridViewTextBoxColumn.Name].Value, true);
        var valueContorta = DataSetParser.GetObjIntValue(aRow.Cells[contortaDataGridViewTextBoxColumn.Name].Value, true);
        aRow.Cells[summaDataGridViewTextBoxColumn.Name].Value =
          (valueTall + valueGran + valueLov + valueContorta).ToString(CultureInfo.InvariantCulture);

        if (DataSetParser.GetObjIntValue(aRow.Cells[skadorDataGridViewTextBoxColumn.Name].Value, true) > 100)
        {
          //Max 100%
          aRow.Cells[skadorDataGridViewTextBoxColumn.Name].Value = 100;
        }

        if (aRow.Cells[summaDataGridViewTextBoxColumn.Name].Value != null &&
            !aRow.Cells[summaDataGridViewTextBoxColumn.Name].Value.ToString().Equals(string.Empty) &&
            aRow.Cells[ytaDataGridViewTextBoxColumn.Name].Value != null &&
            aRow.Cells[ytaDataGridViewTextBoxColumn.Name].Value.ToString().Equals(string.Empty))
        {
          //yt nummer finns inte men summa finns, buggen har då inträffat, ta då bort raden.
          dataGridViewGallringsstallen.Rows.Remove(aRow);
        }
      }
      catch (Exception ex)
      {
#if !DEBUG
       MessageBox.Show("Beräkna Optimalt+Bra misslyckades", "Beräkna misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }


    private double GetMeanValue(double sumValue, bool aAvrunda)
    {
      return GetMeanValue(sumValue, aAvrunda, null);
    }

    private double GetMeanValue(double sumValue, bool aAvrunda, string aColumnNamn)
    {
      var meanValue = 0.0;
      try
      {
        var totalNrOfRows = AntalYtor(aColumnNamn);

        if (sumValue > 0 && totalNrOfRows > 0)
        {
          meanValue = sumValue/totalNrOfRows;
        }

        if (aAvrunda)
        {
          //Round the value 
          meanValue = Math.Round(meanValue, 1);
        }
      }
      catch (Exception ex)
      {
        //Math Round failed.
#if !DEBUG
        meanValue = 0.0;
#else
        MessageBox.Show(ex.ToString());
#endif
      }
      return meanValue;
    }

    /// <summary>
    ///   Updates the meanvalues in the GUI.
    /// </summary>
    private void UpdateMeanValues()
    {
      try
      {
        var medelvärdeStickvBredd = GetMeanValue(
          DataSetParser.GetObjDoubleValue(textBoxSummaStickvbredd.Text, true), false,
          stickvbreddDataGridViewTextBoxColumn.Name);
        textBoxMedelvardeStickvbredd.Text = Math.Round(medelvärdeStickvBredd, 1).ToString(CultureInfo.InvariantCulture);
        DataSetParser.SetDsDouble("Medelvarde", "Stickvbredd", medelvärdeStickvBredd, dataSetCalculations);

        textBoxMedelvardeTall.Text =
          GetMeanValue(DataSetParser.GetObjIntValue(textBoxSummaTall.Text, true), true).ToString(
            CultureInfo.InvariantCulture);
        textBoxMedelvardeGran.Text =
          GetMeanValue(DataSetParser.GetObjIntValue(textBoxSummaGran.Text, true), true).ToString(
            CultureInfo.InvariantCulture);
        textBoxMedelvardeLov.Text =
          GetMeanValue(DataSetParser.GetObjIntValue(textBoxSummaLov.Text, true), true).ToString(
            CultureInfo.InvariantCulture);
        textBoxMedelvardeContorta.Text =
          GetMeanValue(DataSetParser.GetObjIntValue(textBoxSummaContorta.Text, true), true).ToString(
            CultureInfo.InvariantCulture);
        textBoxMedelvardeSumma.Text =
          GetMeanValue(DataSetParser.GetObjIntValue(textBoxSumma.Text, true), true).ToString(
            CultureInfo.InvariantCulture);
        textBoxMedelvardeSkador.Text =
          GetMeanValue(DataSetParser.GetObjIntValue(textBoxSummaSkador.Text, true), true).ToString(
            CultureInfo.InvariantCulture);
      }
      catch (Exception ex)
      {
#if !DEBUG
        MessageBox.Show("Misslyckades med att beräkna medelvärden", "Beräkna medelvärden misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }


    private void UpdateGallringValues()
    {
      try
      {
        var medelvärdeStickvBredd = GetMeanValue(DataSetParser.GetObjDoubleValue(textBoxSummaStickvbredd.Text, true),
                                                 false,
                                                 stickvbreddDataGridViewTextBoxColumn.Name);

        var stickvägsavstånd = DataSetParser.GetObjIntValue(textBoxStickvagsavstand.Text, true);
        var medelVardeSumma = GetMeanValue(DataSetParser.GetObjIntValue(textBoxSumma.Text, true), false);

        if (medelvärdeStickvBredd > 0 && stickvägsavstånd > 0)
        {
          textBoxVagareal.Text =
            (Math.Round(100*(medelvärdeStickvBredd/stickvägsavstånd), 1)).ToString(CultureInfo.InvariantCulture);
          var målgrundyta = DataSetParser.GetObjIntValue(textBoxMalgrundyta.Text);
          textBoxMaluppfyllelse.Text = målgrundyta > 0
                                         ? (Math.Round((medelVardeSumma/målgrundyta)*100, 1)).ToString(
                                           CultureInfo.InvariantCulture)
                                         : Resources.Noll;
        }
        else
        {
          textBoxVagareal.Text = Resources.Noll;
          textBoxMaluppfyllelse.Text = Resources.Noll;
        }
      }
      catch (Exception ex)
      {
#if !DEBUG
        MessageBox.Show("Misslyckades med att beräkna gallrings värden.", "Beräkna gallringsvärden misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }


    private void CalculateValues()
    {
      UpdateSumValues();
      UpdateMeanValues();
      UpdateGallringValues();
    }

    private void sparaToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (RedigeraLagratData)
      {
        UpdateDecimal(textBoxAreal);
      }
      DataHelper.TvingaDataGridViewAttValidera(dataGridViewGallringsstallen);
      DataSetParser.TabortFelaktigaYtor(dataSetGallring);

      UpdateDecimal(textBoxAreal);
      if (RedigeraLagratData)
      {
        Hide();
      }
      else
      {
        WriteDataToReportXmlFile(Settings.Default.ReportFilePath, true, false, Resources.Obehandlad,
                                 DateTime.MinValue.ToString(CultureInfo.InvariantCulture));
      }
    }

    /// <summary>
    ///   Shows the save as dialog to let the user be able to save the report with a custom name.
    /// </summary>
    /// <returns> The file path name. </returns>
    private string ShowSaveAsDialog(string aSelectedPath, string aFileSuffix)
    {
      // folderBrowserSaveReportDialog.RootFolder = Environment.SpecialFolder.MyComputer;

      saveReportfolderBrowserDialog.SelectedPath = aSelectedPath;

      var fileName = GetReportFileName();

      if (fileName != null)
      {
        saveReportfolderBrowserDialog.Description = Resources.Spara_rapport + fileName + aFileSuffix;

        if (saveReportfolderBrowserDialog.ShowDialog() == DialogResult.OK)
        {
          return saveReportfolderBrowserDialog.SelectedPath + Path.DirectorySeparatorChar + fileName + aFileSuffix;
        }
      }
      return null;
    }


    private void sparaSomToolStripMenuItem_Click(object sender, EventArgs e)
    {
      var filePath = ShowSaveAsDialog(Settings.Default.ReportFilePath.Equals(string.Empty)
                                        ? mSDefaultReportsPath
                                        : Path.GetDirectoryName(Settings.Default.ReportFilePath), Resources.keg);
      if (File.Exists(filePath))
      {
        if (
          MessageBox.Show(
            Resources.Skriva_over_redan_befintlig_rapport + Path.GetFileNameWithoutExtension(filePath) +
            Resources.Fragetecken,
            Resources.Skriva_over_rapport, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
        {
          return;
        }
      }
      DataHelper.TvingaDataGridViewAttValidera(dataGridViewGallringsstallen);
      DataSetParser.TabortFelaktigaYtor(dataSetGallring);
      UpdateDecimal(textBoxAreal);
      WriteDataToReportXmlFile(filePath, true, false, Settings.Default.ReportStatus, Settings.Default.ReportStatusDatum);
    }

    private void Gallring_FormClosing(object sender, FormClosingEventArgs e)
    {
      Settings.Default.Save();
      BindingContext[dataSetGallring].EndCurrentEdit();

      e.Cancel = true;
      Hide();
    }

    private void InitData()
    {
      dataSetGallring.Clear();
      dataSetGallring.Tables["Yta"].Columns["Yta"].AutoIncrementStep = -1;
      dataSetGallring.Tables["Yta"].Columns["Yta"].AutoIncrementSeed = -1;
      dataSetGallring.Tables["Yta"].Columns["Yta"].AutoIncrementStep = 1;
      dataSetGallring.Tables["Yta"].Columns["Yta"].AutoIncrementSeed = 2;
      progressBarData.Value = 0;
      labelProgress.Text = Resources.Klar;
      labelTotaltAntalYtor.Text = Resources.Totalt_antal_ytor__0;
      reportGallring.RegisterData(dataSetGallring, "dataSetGallring");
      reportGallring.RegisterData(dataSetCalculations, "dataSetCalculations");
    }

    private void dataGridViewGallringsstallen_DataError(object sender, DataGridViewDataErrorEventArgs e)
    {
      if (!((e.Exception) is FormatException)) return;
      var view = (DataGridView) sender;
      MessageBox.Show(Resources.EndastPositiva, Resources.FelaktigtVarde, MessageBoxButtons.OK,
                      MessageBoxIcon.Stop);
      view.CurrentCell.Value = 0;
      e.ThrowException = false;
      e.Cancel = false;
    }


    private void SendViaOutlook(bool aBifogaRapport)
    {
      if (!CheckMailSettings(false)) return;
      //First save the report
      WriteDataToReportXmlFile(Settings.Default.ReportFilePath, false, false, Settings.Default.ReportStatus,
                               Settings.Default.ReportStatusDatum);
      try
      {
        var oApp = new Microsoft.Office.Interop.Outlook.Application();
        var oMsg = (MailItem) oApp.CreateItem(OlItemType.olMailItem);
        var oRecip = oMsg.Recipients.Add(mEpostAdress);

        oRecip.Resolve();

        oMsg.Subject = mTitel;
        oMsg.Body = mMeddelande;

        if (aBifogaRapport)
        {
          //Add rapport.
          var sDisplayName = Settings.Default.MailAttachmentName;
          var iPosition = oMsg.Body.Length + 1;
          const int ATTACH_TYPE = (int) OlAttachmentType.olByValue;
          oMsg.Attachments.Add(Settings.Default.ReportFilePath, ATTACH_TYPE, iPosition, sDisplayName);
        }
        // Visa meddelandet.
        // oMsg.Display(true);  //modal

        oMsg.Save();
        // ReSharper disable RedundantCast
        ((_MailItem) oMsg).Send();
        // ReSharper restore RedundantCast        
      }
      catch (Exception ex)
      {
#if !DEBUG
       MessageBox.Show("Sända rapporten via Outlook misslyckades. Kolla så epost inställningarna är korrekta.", "Sända misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    private bool CheckMailSettings(bool aInternMail)
    {
      var errMessage = string.Empty;

      if (Settings.Default.MailSendFrom.Equals(string.Empty))
      {
        errMessage += Resources.DinEPostadress + " ";
      }
      if (Settings.Default.MailFullName.Equals(string.Empty))
      {
        errMessage += Resources.DittNamn + " ";
      }
      if (mEpostAdress.Equals(string.Empty))
      {
        errMessage += Resources.MottagarensEPostadress + " ";
      }
      if (mTitel.Equals(string.Empty))
      {
        errMessage += Resources.Titel + " ";
      }
      if (aInternMail)
      {
        if (Settings.Default.MailSMTP.Equals(string.Empty))
        {
          errMessage += Resources.UtgaendeEPostSMTP + " ";
        }
      }

      if (!errMessage.Equals(string.Empty))
      {
        MessageBox.Show(
          String.Format(Resources.Foljande_epost_installningar_saknas,
                        errMessage), Resources.Epost_installningar_saknas, MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }

      return (errMessage.Equals(string.Empty));
    }

    private void SendInternMail(bool aBifogaRapport)
    {
      if (!CheckMailSettings(true)) return;
      //First save the report
      WriteDataToReportXmlFile(Settings.Default.ReportFilePath, false, false, Settings.Default.ReportStatus,
                               Settings.Default.ReportStatusDatum);
      try
      {
        var smtpClient = new SmtpClient(Settings.Default.MailSMTP);
        var fromMail = new MailAddress(Settings.Default.MailSendFrom, Settings.Default.MailFullName,
                                       Encoding.UTF8);
        var toMail = new MailAddress(mEpostAdress);

        using (var message = new MailMessage(fromMail, toMail))
        {
          message.Body = mMeddelande;
          message.BodyEncoding = Encoding.UTF8;
          message.Subject = mTitel;
          message.SubjectEncoding = Encoding.UTF8;

          var smtpUserInfo = new NetworkCredential(Settings.Default.MailUsername,
                                                   Settings.Default.MailPassword);
          smtpClient.UseDefaultCredentials = false;
          smtpClient.Credentials = smtpUserInfo;
          smtpClient.Port = Settings.Default.MailSMTPPort;
          if (aBifogaRapport)
          {
            var attachment = new Attachment(Settings.Default.ReportFilePath);
            message.Attachments.Add(attachment);
          }
          smtpClient.Send(message);
          MessageBox.Show(Resources.RapportenArSand, Resources.RapportSandTitle, MessageBoxButtons.OK,
                          MessageBoxIcon.Information);
        }
      }
      catch (Exception ex)
      {
#if !DEBUG
       MessageBox.Show("Sända rapport via intern mail misslyckades. Kolla så epost inställningarna är korrekta.", "Sända misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    private void InitFastReportMail()
    {
      reportGallring.EmailSettings.Recipients = new[] {mEpostAdress};
      reportGallring.EmailSettings.Subject = mTitel;
      reportGallring.EmailSettings.Message = mMeddelande;
      environmentGallring.EmailSettings.Address = Settings.Default.MailSendFrom;
      environmentGallring.EmailSettings.Name = Settings.Default.MailFullName;
      environmentGallring.EmailSettings.Host = Settings.Default.MailSMTP;
      environmentGallring.EmailSettings.Port = Settings.Default.MailSMTPPort;
      environmentGallring.EmailSettings.UserName = Settings.Default.MailUsername;
      environmentGallring.EmailSettings.Password = Settings.Default.MailPassword;
    }

    /// <summary>
    ///   Changes the language and restarts the application.
    /// </summary>
    /// <param name="aLanguage"> The language </param>
    private static void ChangeLanguage(string aLanguage)
    {
      if (Settings.Default.SettingsLanguage.Equals(aLanguage)) return;

      try
      {
        Settings.Default.SettingsLanguage = aLanguage;
        Application.Restart();
      }
      catch (NotSupportedException nse)
      {
#if !DEBUG
          MessageBox.Show("Applikationen gick inte att starta om, gör detta manuellt istället.");
#else
        MessageBox.Show(nse.ToString());
#endif
      }
    }

    /// <summary>
    ///   Gets the correct language from the language id string.
    /// </summary>
    /// <param name="aLangId"> Language id string </param>
    /// <returns> The language string </returns>
    private static string GetLanguage(string aLangId)
    {
      if (aLangId.Equals(Settings.Default.SprakSv))
      {
        return Resources.Svenska;
      }
      return aLangId.Equals(Settings.Default.SprakEn) ? Resources.Engelska : "Odefinerat språk";
    }


    private static string GetDsStr(string aTable, string aColumn, int aRowIndex, DataSet aDataSet)
    {
      try
      {
        if (aDataSet != null && aDataSet.Tables[aTable] != null)
        {
          if (aDataSet.Tables[aTable].Rows.Count > aRowIndex && aDataSet.Tables[aTable].Rows[aRowIndex][aColumn] != null)
          {
            return aDataSet.Tables[aTable].Rows[aRowIndex][aColumn].ToString();
          }
        }
      }
      catch (Exception)
      {
//ignore 
      }
      return string.Empty;
    }


    private void ProgressNumberOfDataDone()
    {
      var nrOfDataDone = 0;

      if (!GetDsStr("UserData", "Region").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("UserData", "Distrikt").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("UserData", "Ursprung").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("UserData", "Traktnr").Equals(string.Empty) &&
          !GetDsStr("UserData", "Traktnr").Equals(Resources.Noll))
        nrOfDataDone++;
      if (!GetDsStr("UserData", "Traktnamn").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("UserData", "Standort").Equals(string.Empty) &&
          !GetDsStr("UserData", "Standort").Equals(Resources.Noll))
        nrOfDataDone++;
      if (!GetDsStr("UserData", "Entreprenor").Equals(string.Empty) &&
          !GetDsStr("UserData", "Entreprenor").Equals(Resources.Noll))
        nrOfDataDone++;
      if (!GetDsStr("UserData", "Areal").Equals(string.Empty) && !GetDsStr("UserData", "Areal").Equals(Resources.Noll))
        nrOfDataDone++;
      if (!GetDsStr("UserData", "Malgrundyta").Equals(string.Empty) &&
          !GetDsStr("UserData", "Malgrundyta").Equals(Resources.Noll))
        nrOfDataDone++;
      if (!GetDsStr("UserData", "Stickvagsavstand").Equals(string.Empty) &&
          !GetDsStr("UserData", "Stickvagsavstand").Equals(Resources.Noll))
        nrOfDataDone++;
      if (!GetDsStr("UserData", "Stickvagssystem").Equals(string.Empty))
        nrOfDataDone++;

      //Natur-kulturmiljöhänsyn
      if (GetDsStr("Fragor", "Fraga1").Equals("Ja"))
      {
        progressBarData.Maximum = Settings.Default.MaxProgressGallring;
        nrOfDataDone++;
      }
      else if (GetDsStr("Fragor", "Fraga1").Equals("Nej"))
      {
        //Ifall traktdirekt är nej, ökas progress med 1, och en check mot kommentar görs.
        progressBarData.Maximum = Settings.Default.MaxProgressGallring + 1;
        nrOfDataDone++;

        if (!GetDsStr("UserData", "Kommentar").Equals(string.Empty))
          nrOfDataDone++;
      }

      if (!GetDsStr("Fragor", "Fraga2").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("Fragor", "Fraga3").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("Fragor", "Fraga4").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("Fragor", "Fraga5").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("Fragor", "Fraga6").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("Fragor", "Fraga7").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("Fragor", "Fraga8").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("Fragor", "Fraga9").Equals(string.Empty))
        nrOfDataDone++;


      //Check sums if anything has been entered in the table
      if (!textBoxSummaStickvbredd.Text.Equals(Resources.Noll) || !textBoxSummaTall.Text.Equals(Resources.Noll) ||
          !textBoxSummaGran.Text.Equals(Resources.Noll)
          || !textBoxSummaLov.Text.Equals(Resources.Noll) || !textBoxSummaContorta.Text.Equals(Resources.Noll)
          || !textBoxSumma.Text.Equals(Resources.Noll) || !textBoxSummaSkador.Text.Equals(Resources.Noll))
        nrOfDataDone++;


      progressBarData.Value = nrOfDataDone;

      if (progressBarData.Value > 0 && progressBarData.Maximum > 0)
      {
        labelProgress.Text = String.Format("{0}% Klar",
                                           (int) ((progressBarData.Value/(float) progressBarData.Maximum)*100));
      }

      if (progressBarData.Value > 0 && progressBarData.Value == progressBarData.Maximum)
      {
        progressBarData.ForeColor = Color.Green;
      }
      else
      {
        progressBarData.ForeColor = SystemColors.Desktop;
      }

      if (AntalYtor() < 2 && textBoxSummaStickvbredd.Text.Equals(Resources.Noll) &&
          textBoxSummaTall.Text.Equals(Resources.Noll) &&
          textBoxSummaGran.Text.Equals(Resources.Noll)
          && textBoxSummaLov.Text.Equals(Resources.Noll) && textBoxSummaContorta.Text.Equals(Resources.Noll)
          && textBoxSumma.Text.Equals(Resources.Noll) && textBoxSummaSkador.Text.Equals(Resources.Noll))
      {
        labelTotaltAntalYtor.Text = Resources.Totalt_antal_ytor__0;
      }
      else
      {
        labelTotaltAntalYtor.Text = String.Format("Totalt antal ytor: " + AntalYtor());
      }
    }

    /// <summary>
    ///   Checks the combobox or textbox if it is focused in that case update the progressbar.
    /// </summary>
    /// <param name="aSender"> The sending object </param>
    private void ProgressFocusedObject(ref object aSender)
    {
      try
      {
        var control = aSender as Control;

        if (control != null && control.Focused)
        {
          ProgressNumberOfDataDone();
        }
      }
      catch (Exception ex)
      {
#if DEBUG
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    private void UpdateDistriktList(ref object sender)
    {
      var comboBox = sender as ComboBox;
      if (comboBox == null) return;
      comboBoxDistrikt.Items.Clear();
      if (comboBox.SelectedIndex < 0) return;
      var activeRegionId = dataSetSettings.Tables["Region"].Rows[comboBox.SelectedIndex]["Id"].ToString();
      foreach (
        var row in
          dataSetSettings.Tables["Distrikt"].Rows.Cast<DataRow>().Where(
            row => row["RegionId"].ToString().Equals(activeRegionId)))
      {
        comboBoxDistrikt.Items.Add(row["Namn"].ToString());
      }
    }

    private void comboBoxRegion_SelectedIndexChanged(object sender, EventArgs e)
    {
      UpdateDistriktList(ref sender);
      ProgressFocusedObject(ref sender);
    }

    private void comboBoxDistrikt_SelectedIndexChanged(object sender, EventArgs e)
    {
      ProgressFocusedObject(ref sender);
    }

    private void textBoxTraktnr_TextChanged(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box == null) return;
      if (CheckaPositivtHeltal(box))
      {
        ProgressFocusedObject(ref sender);
      }
    }

    private void textBoxTraktnamn_TextChanged(object sender, EventArgs e)
    {
      ProgressFocusedObject(ref sender);
    }

    private void comboBoxStandort_SelectedIndexChanged(object sender, EventArgs e)
    {
      ProgressFocusedObject(ref sender);
    }

    private void textBoxEntreprenor_TextChanged(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box == null) return;
      if (!CheckaPositivtHeltal(box)) return;
      ProgressFocusedObject(ref sender);
    }


    private void comboBoxUrsprung_SelectedIndexChanged(object sender, EventArgs e)
    {
      ProgressFocusedObject(ref sender);
    }

    private void dataGridViewGallringsstallen_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
    {
      try
      {
        if (dataGridViewGallringsstallen == null || e.FormattedValue == null ||
            dataGridViewGallringsstallen.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly) return;

        // Don't try to validate the 'new row' until finished 
        // editing since there
        // is not any point in validating its initial value.
        if (dataGridViewGallringsstallen.Rows[e.RowIndex].IsNewRow)
        {
          return;
        }

        dataGridViewGallringsstallen.Rows[e.RowIndex].ErrorText = "";
        dataGridViewGallringsstallen.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "";

        if (e.FormattedValue.ToString().Trim().Equals(string.Empty)) return;

        var cname = dataGridViewGallringsstallen.CurrentCell.OwningColumn.Name;
        var cellVärde = e.FormattedValue.ToString();

        if (cname.Equals(stickvbreddDataGridViewTextBoxColumn.Name))
        {
          if (!string.IsNullOrEmpty(cellVärde))
          {
            double cellDoubleValue;
            double.TryParse(cellVärde, out cellDoubleValue);
            if (cellDoubleValue < 0.0)
            {
              MessageBox.Show(Resources.EndastPositiva, Resources.FelaktigtVarde, MessageBoxButtons.OK,
                              MessageBoxIcon.Stop);
            }
          }
        }
        else
        {
          if (!string.IsNullOrEmpty(cellVärde))
          {
            int cellIntValue;
            int.TryParse(cellVärde, out cellIntValue);
            if (cellIntValue < 0)
            {
              MessageBox.Show(Resources.EndastPositiva, Resources.FelaktigtVarde, MessageBoxButtons.OK,
                              MessageBoxIcon.Stop);
            }
          }
        }
      }
      catch (Exception ex)
      {
#if !DEBUG
       MessageBox.Show("Det gick inte att validera värdet på cellen.", "Fel vid cell validering", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    private void dataGridViewGallringsstallen_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
    {
      CalculateValues();
      ProgressNumberOfDataDone();
    }


    private int AntalYtor()
    {
      return AntalYtor(null);
    }

    private int AntalYtor(string columnNamn)
    {
      var antalYtor = 0;
      try
      {
        foreach (
          var row in
            dataGridViewGallringsstallen.Rows.Cast<DataGridViewRow>().Where(
              row => DataSetParser.GetObjIntValue(row.Cells[ytaDataGridViewTextBoxColumn.Name].Value) >= 0))
        {
          if (columnNamn != null)
          {
            if (columnNamn.Equals(stickvbreddDataGridViewTextBoxColumn.Name))
            {
              //räkna endast med stickvbredd ifall decimal-värdet är större än 0
              if (DataSetParser.GetObjDoubleValue(row.Cells[stickvbreddDataGridViewTextBoxColumn.Name].Value, true) > 0)
              {
                antalYtor++;
              }
            }
          }
          else
          {
            antalYtor++;
          }
        }
      }
      catch (Exception ex)
      {
#if !DEBUG
        MessageBox.Show("Antal ytor misslyckades.", "Antal ytor misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
      return antalYtor;
    }


    private void dataGridViewGallringsstallen_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
    {
      if (AntalYtor() > 1) return;
      //Update the total sum textboxes, reset to zero
      textBoxSummaStickvbredd.Text = Resources.Noll;
      textBoxSummaTall.Text = Resources.Noll;
      textBoxSummaGran.Text = Resources.Noll;
      textBoxSummaLov.Text = Resources.Noll;
      textBoxSummaContorta.Text = Resources.Noll;
      textBoxSumma.Text = Resources.Noll;
      textBoxSummaSkador.Text = Resources.Noll;
      UpdateMeanValues();
      UpdateGallringValues();
      ProgressNumberOfDataDone();
    }

    private void SetDsStr(string aTable, string aColumn, string aText)
    {
      if (dataSetGallring == null || dataSetGallring.Tables[aTable] == null) return;
      if (dataSetGallring.Tables[aTable].Rows.Count <= 0 || dataSetGallring.Tables[aTable].Rows[0][aColumn] == null ||
          dataSetGallring.Tables[aTable].Rows[0][aColumn].ToString().Equals(aText)) return;
      try
      {
        dataSetGallring.Tables[aTable].Rows[0][aColumn] = aText;
      }
      catch (Exception)
      {
//ignore            
      }
    }

    private void SetSelectedDistriktId()
    {
      int index = 0, settingsIndex = 0;
      foreach (DataRow row in dataSetSettings.Tables["Distrikt"].Rows)
      {
        if (row["RegionId"].ToString().Equals(GetDsStr("Region", "Id", comboBoxRegion.SelectedIndex, dataSetSettings)))
        {
          if (comboBoxDistrikt.SelectedIndex == index)
          {
            SetDsStr("UserData", "DistriktId", GetDsStr("Distrikt", "Id", settingsIndex, dataSetSettings));
            break;
          }
          index++;
        }
        settingsIndex++;
      }
    }

    private static bool CheckaPositivtHeltal(Control aTextBox)
    {
      var rValue = false;
      if (aTextBox != null && !aTextBox.Text.Equals(string.Empty))
      {
        if (aTextBox.Focused && DataSetParser.GetObjIntValue(aTextBox.Text) < 0)
        {
          MessageBox.Show(Resources.Ogiltigt_nummer, Resources.Ogiltigt_nummer, MessageBoxButtons.OK,
                          MessageBoxIcon.Information);
          aTextBox.Text = string.Empty;
        }
        else
        {
          rValue = true;
        }
      }
      return rValue;
    }

    private void dateTimePicker_ValueChanged(object sender, EventArgs e)
    {
      SetDsStr("UserData", "Datum", dateTimePicker.Text);
      var årtal = GetDatumÅr(dateTimePicker.Text);
      SetDsStr("UserData", "Artal", årtal.ToString(CultureInfo.InvariantCulture));
    }

    private void comboBoxDistrikt_SelectionChangeCommitted(object sender, EventArgs e)
    {
      SetDsStr("UserData", "Distrikt", comboBoxDistrikt.Text);
      SetSelectedDistriktId();
    }

    private void comboBoxRegion_SelectionChangeCommitted(object sender, EventArgs e)
    {
      SetDsStr("UserData", "Region", comboBoxRegion.Text);
      var regionid = GetDsStr("Region", "Id", comboBoxRegion.SelectedIndex, dataSetSettings);
      SetDsStr("UserData", "RegionId", regionid);
    }

    private void comboBoxUrsprung_SelectionChangeCommitted(object sender, EventArgs e)
    {
      SetDsStr("UserData", "Ursprung", comboBoxUrsprung.Text);
    }

    private void comboBoxStandort_SelectionChangeCommitted(object sender, EventArgs e)
    {
      SetDsStr("UserData", "Standort", comboBoxStandort.Text);
    }

    private void textBoxStickvagsavstand_TextChanged(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box == null) return;
      if (!CheckaPositivtHeltal(box)) return;
      SetDsStr("UserData", "Stickvagsavstand", textBoxStickvagsavstand.Text);
      ProgressFocusedObject(ref sender);
    }

    private static bool CheckaPositivtDecimaltal(Control aTextBox)
    {
      var rValue = false;
      if (aTextBox != null && !aTextBox.Text.Equals(string.Empty))
      {
        if (aTextBox.Focused && DataSetParser.GetObjFloatValue(aTextBox.Text) < 0)
        {
          MessageBox.Show(Resources.Ogiltigt_nummer, Resources.Ogiltigt_nummer, MessageBoxButtons.OK,
                          MessageBoxIcon.Information);
          aTextBox.Text = string.Empty;
        }
        else
        {
          rValue = true;
        }
      }
      return rValue;
    }

    private void textBoxAreal_TextChanged(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box == null) return;
      if (!CheckaPositivtDecimaltal(box)) return;
      if (textBoxAreal.Text.Contains('.'))
      {
        textBoxAreal.Text = textBoxAreal.Text.Replace('.', ',');
      }
      ProgressFocusedObject(ref sender);
    }

    private void textBoxMalgrundyta_TextChanged(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box == null) return;
      if (!CheckaPositivtHeltal(box)) return;
      SetDsStr("UserData", "Malgrundyta", textBoxMalgrundyta.Text);
      UpdateGallringValues();
      ProgressFocusedObject(ref sender);
    }

    private void dataGridViewGallringsstallen_CellEndEdit(object sender, DataGridViewCellEventArgs e)
    {
      try
      {
        if (dataGridViewGallringsstallen == null || dataGridViewGallringsstallen.CurrentCell.Value == null) return;

        var cname = dataGridViewGallringsstallen.CurrentCell.OwningColumn.Name;
        var cellVärde = dataGridViewGallringsstallen.CurrentCell.Value.ToString().Trim();
        if (string.IsNullOrEmpty(cellVärde)) return;

        if (cname.Equals(stickvbreddDataGridViewTextBoxColumn.Name))
        {
          double cellDoubleValue;
          if ((!double.TryParse(cellVärde, out cellDoubleValue) ||
               cellDoubleValue < 0.0))
          {
            dataGridViewGallringsstallen.CurrentCell.Value = Resources.Noll;
          }
        }
        else
        {
          int cellIntValue;
          if ((!int.TryParse(cellVärde, out cellIntValue) ||
               cellIntValue < 0))
          {
            dataGridViewGallringsstallen.CurrentCell.Value = Resources.Noll;
          }
        }
        UpdateSumma(dataGridViewGallringsstallen.CurrentCell.OwningRow);
        CalculateValues();
      }
      catch (Exception ex)
      {
#if !DEBUG
       MessageBox.Show("Det gick inte att kontrollera värdet på cellen.", "Fel vid cell editering", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    private void textBoxTraktnr_KeyDown(object sender, KeyEventArgs e)
    {
        DataHelper.AccepteraEndastHeltalSomKeyDown(e, false);
    }

    private void textBoxEntreprenor_KeyDown(object sender, KeyEventArgs e)
    {
        DataHelper.AccepteraEndastHeltalSomKeyDown(e, false);
    }

    private void textBoxAreal_KeyDown(object sender, KeyEventArgs e)
    {
      DataHelper.AccepteraEndastHeltalSomKeyDown(e,true);
    }

    private void textBoxStickvagsavstand_KeyDown(object sender, KeyEventArgs e)
    {
        DataHelper.AccepteraEndastHeltalSomKeyDown(e, false);
    }

    private void textBoxMalgrundyta_KeyDown(object sender, KeyEventArgs e)
    {
        DataHelper.AccepteraEndastHeltalSomKeyDown(e, false);
    }

    private void textBoxTraktnr_Leave(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box != null && box.Text.Trim().Equals(string.Empty))
      {
        box.Text = Resources.Noll;
      }
      SetDsStr("UserData", "Traktnr", textBoxTraktnr.Text);
    }

    private void textBoxEntreprenor_Leave(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box != null && box.Text.Trim().Equals(string.Empty))
      {
        box.Text = Resources.Noll;
      }
    }

    private void textBoxAreal_Leave(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box != null && box.Text.Trim().Equals(string.Empty))
      {
        box.Text = Resources.Noll;
      }
      SetDsStr("UserData", "Areal", textBoxAreal.Text);
    }

    private void textBoxStickvagsavstand_Leave(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box != null && box.Text.Trim().Equals(string.Empty))
      {
        box.Text = Resources.Noll;
      }
    }

    private void textBoxMalgrundyta_Leave(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box != null && box.Text.Trim().Equals(string.Empty))
      {
        box.Text = Resources.Noll;
      }
    }

    private void comboBoxStickväg_SelectedIndexChanged(object sender, EventArgs e)
    {
      ProgressFocusedObject(ref sender);
    }

    private void comboBoxStickväg_SelectionChangeCommitted(object sender, EventArgs e)
    {
      SetDsStr("UserData", "Stickvagssystem", comboBoxStickväg.Text);
    }


    private void StatusFrågor()
    {
      try
      {
        radioButton1Ja.Checked = GetDsStr("Fragor", "Fraga1").Equals("Ja");
        radioButton1Nej.Checked = GetDsStr("Fragor", "Fraga1").Equals("Nej");
        radioButton2Ja.Checked = GetDsStr("Fragor", "Fraga2").Equals("Ja");
        radioButton2Nej.Checked = GetDsStr("Fragor", "Fraga2").Equals("Nej");
        radioButton2Ejaktuellt.Checked = GetDsStr("Fragor", "Fraga2").Equals("Ej aktuellt");
        radioButton3Ja.Checked = GetDsStr("Fragor", "Fraga3").Equals("Ja");
        radioButton3Nej.Checked = GetDsStr("Fragor", "Fraga3").Equals("Nej");
        radioButton3Ejaktuellt.Checked = GetDsStr("Fragor", "Fraga3").Equals("Ej aktuellt");
        radioButton4Ja.Checked = GetDsStr("Fragor", "Fraga4").Equals("Ja");
        radioButton4Nej.Checked = GetDsStr("Fragor", "Fraga4").Equals("Nej");
        radioButton5Ja.Checked = GetDsStr("Fragor", "Fraga5").Equals("Ja");
        radioButton5Nej.Checked = GetDsStr("Fragor", "Fraga5").Equals("Nej");
        radioButton5Ejaktuellt.Checked = GetDsStr("Fragor", "Fraga5").Equals("Ej aktuellt");
        radioButton6Ja.Checked = GetDsStr("Fragor", "Fraga6").Equals("Ja");
        radioButton6Nej.Checked = GetDsStr("Fragor", "Fraga6").Equals("Nej");
        radioButton7Ja.Checked = GetDsStr("Fragor", "Fraga7").Equals("Ja");
        radioButton7Nej.Checked = GetDsStr("Fragor", "Fraga7").Equals("Nej");
        radioButton7Ejaktuellt.Checked = GetDsStr("Fragor", "Fraga7").Equals("Ej aktuellt");
        radioButton8Ja.Checked = GetDsStr("Fragor", "Fraga8").Equals("Ja");
        radioButton8Nej.Checked = GetDsStr("Fragor", "Fraga8").Equals("Nej");
        radioButton9Ja.Checked = GetDsStr("Fragor", "Fraga9").Equals("Ja");
        radioButton9Nej.Checked = GetDsStr("Fragor", "Fraga9").Equals("Nej");
      }
      catch (Exception ex)
      {
#if !DEBUG
        MessageBox.Show("Misslyckades med att uppdatera frågornas status.", "Status error", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    private void SättRButtonSvar(object aSender, string aFraga)
    {
      try
      {
        var rbutton = aSender as RadioButton;
        if (rbutton == null || !rbutton.Checked) return;

        SetDsStr("Fragor", aFraga, rbutton.Text);
        ProgressFocusedObject(ref aSender);
      }
      catch (Exception ex)
      {
#if !DEBUG
        MessageBox.Show("Misslyckades med att hämta svaret från radiobutton.");
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    private void radioButton1Ja_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga1");
    }

    private void radioButton1Nej_CheckedChanged(object sender, EventArgs e)
    {
      var button = sender as RadioButton;
      if (button == null) return;
      SättRButtonSvar(sender, "Fraga1");
      if (!button.Checked || !button.Focused) return;
      MessageBox.Show(
        Resources.Nej_har_valts_for_traktdirektiv,
        Resources.En_kommentar_kravs, MessageBoxButtons.OK, MessageBoxIcon.Information);
      ProgressNumberOfDataDone();
    }

    private void radioButton2Ja_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga2");
    }

    private void radioButton2Nej_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga2");
    }

    private void radioButton2Ejaktuellt_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga2");
    }

    private void radioButton3Ja_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga3");
    }

    private void radioButton3Nej_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga3");
    }

    private void radioButton3Ejaktuellt_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga3");
    }

    private void radioButton4Ja_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga4");
    }

    private void radioButton4Nej_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga4");
    }

    private void radioButton5Ja_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga5");
    }

    private void radioButton5Nej_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga5");
    }

    private void radioButton5Ejaktuellt_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga5");
    }

    private void radioButton6Ja_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga6");
    }

    private void radioButton6Nej_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga6");
    }

    private void radioButton7Ja_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga7");
    }

    private void radioButton7Nej_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga7");
    }

    private void radioButton7Ejaktuellt_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga7");
    }

    private void radioButton8Ja_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga8");
    }

    private void radioButton8Nej_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga8");
    }

    private void radioButton9Ja_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga9");
    }

    private void radioButton9Nej_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga9");
    }

    private void dataGridViewGallringsstallen_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
    {
      if (e.RowIndex > 0)
      {
        LäggTillHögstaVärdetFörYta(e.RowIndex - 1);
      }
    }

    private void richTextBoxKommentarer_TextChanged(object sender, EventArgs e)
    {
      var box = sender as RichTextBox;
      if (box == null) return;
      ProgressNumberOfDataDone();
    }

    private void UpdateDecimal(object sender)
    {
      var box = sender as TextBox;

      if (box == null) return;
      var d = (double) Math.Round(DataSetParser.GetObjDecimalValue(box.Text), 1);
      if (d < 0) return;
      DataSetParser.SetDsDouble("UserData", "Areal", d, dataSetGallring);
      box.Text = d.ToString(CultureInfo.InvariantCulture);
    }

    private void textBoxAreal_Validated(object sender, EventArgs e)
    {
      UpdateDecimal(sender);
    }
  }
}