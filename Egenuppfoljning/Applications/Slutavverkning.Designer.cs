﻿namespace Egenuppfoljning.Applications
{
  partial class SlutavverkningForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SlutavverkningForm));
        FastReport.Design.DesignerSettings designerSettings1 = new FastReport.Design.DesignerSettings();
        FastReport.Design.DesignerRestrictions designerRestrictions1 = new FastReport.Design.DesignerRestrictions();
        FastReport.Export.Email.EmailSettings emailSettings1 = new FastReport.Export.Email.EmailSettings();
        FastReport.PreviewSettings previewSettings1 = new FastReport.PreviewSettings();
        FastReport.ReportSettings reportSettings1 = new FastReport.ReportSettings();
        this.dataSetSlutavverkning = new System.Data.DataSet();
        this.dataTableUserData = new System.Data.DataTable();
        this.dataColumnRegionId = new System.Data.DataColumn();
        this.dataColumnRegion = new System.Data.DataColumn();
        this.dataColumnDistriktId = new System.Data.DataColumn();
        this.dataColumnDistrikt = new System.Data.DataColumn();
        this.dataColumnUrsprung = new System.Data.DataColumn();
        this.dataColumnDatum = new System.Data.DataColumn();
        this.dataColumnTraktnr = new System.Data.DataColumn();
        this.dataColumnTraktnamn = new System.Data.DataColumn();
        this.dataColumnStandort = new System.Data.DataColumn();
        this.dataColumnEntreprenor = new System.Data.DataColumn();
        this.dataColumnLanguage = new System.Data.DataColumn();
        this.dataColumnMailFrom = new System.Data.DataColumn();
        this.dataColumnStatus = new System.Data.DataColumn();
        this.dataColumnStatus_Datum = new System.Data.DataColumn();
        this.dataColumnÅrtal = new System.Data.DataColumn();
        this.dataTableFragor = new System.Data.DataTable();
        this.dataColumnFraga1 = new System.Data.DataColumn();
        this.dataColumnFraga2 = new System.Data.DataColumn();
        this.dataColumnFraga3 = new System.Data.DataColumn();
        this.dataColumnFraga4 = new System.Data.DataColumn();
        this.dataColumnFraga5 = new System.Data.DataColumn();
        this.dataColumnFraga6 = new System.Data.DataColumn();
        this.dataColumnFraga7 = new System.Data.DataColumn();
        this.dataColumnFraga8 = new System.Data.DataColumn();
        this.dataColumnFraga9 = new System.Data.DataColumn();
        this.dataColumnFraga10 = new System.Data.DataColumn();
        this.dataColumnFraga11 = new System.Data.DataColumn();
        this.dataColumnFraga12 = new System.Data.DataColumn();
        this.dataColumnFraga13 = new System.Data.DataColumn();
        this.dataColumnFraga14 = new System.Data.DataColumn();
        this.dataColumnFraga15 = new System.Data.DataColumn();
        this.dataColumnFraga16 = new System.Data.DataColumn();
        this.dataColumnFraga17 = new System.Data.DataColumn();
        this.dataColumnFraga18 = new System.Data.DataColumn();
        this.dataColumnFraga19 = new System.Data.DataColumn();
        this.dataColumnFraga20 = new System.Data.DataColumn();
        this.dataColumnFraga21 = new System.Data.DataColumn();
        this.dataColumnOvrigt = new System.Data.DataColumn();
        this.dataSetSettings = new System.Data.DataSet();
        this.dataTableRegion = new System.Data.DataTable();
        this.dataColumnSettingsRegionId = new System.Data.DataColumn();
        this.dataColumnSettingsRegionNamn = new System.Data.DataColumn();
        this.dataTableDistrikt = new System.Data.DataTable();
        this.dataColumnSettingsDistriktRegionId = new System.Data.DataColumn();
        this.dataColumnSettingsDistriktId = new System.Data.DataColumn();
        this.dataColumnSettingsDistriktNamn = new System.Data.DataColumn();
        this.dataTableStandort = new System.Data.DataTable();
        this.dataColumnSettingsStandortNamn = new System.Data.DataColumn();
        this.dataTableMarkberedningsmetod = new System.Data.DataTable();
        this.dataColumnSettingsMarkberedningsmetodNamn = new System.Data.DataColumn();
        this.dataTableUrsprung = new System.Data.DataTable();
        this.dataColumnSettingsUrsprungNamn = new System.Data.DataColumn();
        this.menuStripSlutavverkning = new System.Windows.Forms.MenuStrip();
        this.arkivToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.sparaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.sparaSomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
        this.avslutaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.openReportFileDialog = new System.Windows.Forms.OpenFileDialog();
        this.tabControl = new System.Windows.Forms.TabControl();
        this.tabPageSlutavverkning = new System.Windows.Forms.TabPage();
        this.groupBoxSlutavverkning = new System.Windows.Forms.GroupBox();
        this.groupBoxMinstEnlSVL = new System.Windows.Forms.GroupBox();
        this.radioButton1bNej = new System.Windows.Forms.RadioButton();
        this.radioButton1bJa = new System.Windows.Forms.RadioButton();
        this.groupBoxOBS = new System.Windows.Forms.GroupBox();
        this.label7 = new System.Windows.Forms.Label();
        this.label8 = new System.Windows.Forms.Label();
        this.groupBox1 = new System.Windows.Forms.GroupBox();
        this.label3 = new System.Windows.Forms.Label();
        this.groupBox22 = new System.Windows.Forms.GroupBox();
        this.radioButton1aNej = new System.Windows.Forms.RadioButton();
        this.radioButton1aJa = new System.Windows.Forms.RadioButton();
        this.label4 = new System.Windows.Forms.Label();
        this.groupBoxTrakt = new System.Windows.Forms.GroupBox();
        this.textBoxTraktnr = new System.Windows.Forms.TextBox();
        this.textBoxTraktnamn = new System.Windows.Forms.TextBox();
        this.textBoxEntreprenor = new System.Windows.Forms.TextBox();
        this.comboBoxStandort = new System.Windows.Forms.ComboBox();
        this.labeEntreprenor = new System.Windows.Forms.Label();
        this.labelStandort = new System.Windows.Forms.Label();
        this.labelTraktnamn = new System.Windows.Forms.Label();
        this.labelTraktnr = new System.Windows.Forms.Label();
        this.groupBoxRegion = new System.Windows.Forms.GroupBox();
        this.comboBoxUrsprung = new System.Windows.Forms.ComboBox();
        this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
        this.comboBoxDistrikt = new System.Windows.Forms.ComboBox();
        this.comboBoxRegion = new System.Windows.Forms.ComboBox();
        this.labelDatum = new System.Windows.Forms.Label();
        this.labelUrsprung = new System.Windows.Forms.Label();
        this.labelDistrikt = new System.Windows.Forms.Label();
        this.labelRegion = new System.Windows.Forms.Label();
        this.tabPageFrågor1 = new System.Windows.Forms.TabPage();
        this.label5 = new System.Windows.Forms.Label();
        this.groupBox9 = new System.Windows.Forms.GroupBox();
        this.radioButton2hNej = new System.Windows.Forms.RadioButton();
        this.radioButton2hJa = new System.Windows.Forms.RadioButton();
        this.label15 = new System.Windows.Forms.Label();
        this.groupBox8 = new System.Windows.Forms.GroupBox();
        this.radioButton2gNej = new System.Windows.Forms.RadioButton();
        this.label14 = new System.Windows.Forms.Label();
        this.radioButton2gJa = new System.Windows.Forms.RadioButton();
        this.groupBox7 = new System.Windows.Forms.GroupBox();
        this.radioButton2fNej = new System.Windows.Forms.RadioButton();
        this.radioButton2fJa = new System.Windows.Forms.RadioButton();
        this.label13 = new System.Windows.Forms.Label();
        this.groupBox6 = new System.Windows.Forms.GroupBox();
        this.radioButton2eNej = new System.Windows.Forms.RadioButton();
        this.radioButton2eJa = new System.Windows.Forms.RadioButton();
        this.label12 = new System.Windows.Forms.Label();
        this.groupBox5 = new System.Windows.Forms.GroupBox();
        this.radioButton2dEjaktuellt = new System.Windows.Forms.RadioButton();
        this.radioButton2dNej = new System.Windows.Forms.RadioButton();
        this.radioButton2dJa = new System.Windows.Forms.RadioButton();
        this.label11 = new System.Windows.Forms.Label();
        this.groupBox2 = new System.Windows.Forms.GroupBox();
        this.radioButton2cEjaktuellt = new System.Windows.Forms.RadioButton();
        this.radioButton2cNej = new System.Windows.Forms.RadioButton();
        this.label10 = new System.Windows.Forms.Label();
        this.radioButton2cJa = new System.Windows.Forms.RadioButton();
        this.groupBox4 = new System.Windows.Forms.GroupBox();
        this.radioButton2bNej = new System.Windows.Forms.RadioButton();
        this.radioButton2bJa = new System.Windows.Forms.RadioButton();
        this.label2 = new System.Windows.Forms.Label();
        this.groupBox3 = new System.Windows.Forms.GroupBox();
        this.radioButton2aEjaktuellt = new System.Windows.Forms.RadioButton();
        this.radioButton2aNej = new System.Windows.Forms.RadioButton();
        this.radioButton2aJa = new System.Windows.Forms.RadioButton();
        this.label1 = new System.Windows.Forms.Label();
        this.tabPageFrågor2 = new System.Windows.Forms.TabPage();
        this.groupBox18 = new System.Windows.Forms.GroupBox();
        this.radioButton9Nej = new System.Windows.Forms.RadioButton();
        this.radioButton9Ja = new System.Windows.Forms.RadioButton();
        this.label23 = new System.Windows.Forms.Label();
        this.groupBox17 = new System.Windows.Forms.GroupBox();
        this.radioButton8Ejaktuellt = new System.Windows.Forms.RadioButton();
        this.radioButton8Nej = new System.Windows.Forms.RadioButton();
        this.radioButton8Ja = new System.Windows.Forms.RadioButton();
        this.label22 = new System.Windows.Forms.Label();
        this.groupBox16 = new System.Windows.Forms.GroupBox();
        this.radioButton7Ejaktuellt = new System.Windows.Forms.RadioButton();
        this.radioButton7Nej = new System.Windows.Forms.RadioButton();
        this.radioButton7Ja = new System.Windows.Forms.RadioButton();
        this.label21 = new System.Windows.Forms.Label();
        this.groupBox15 = new System.Windows.Forms.GroupBox();
        this.radioButton6Ejaktuellt = new System.Windows.Forms.RadioButton();
        this.radioButton6Nej = new System.Windows.Forms.RadioButton();
        this.radioButton6Ja = new System.Windows.Forms.RadioButton();
        this.label20 = new System.Windows.Forms.Label();
        this.groupBox14 = new System.Windows.Forms.GroupBox();
        this.radioButton5Ejaktuellt = new System.Windows.Forms.RadioButton();
        this.radioButton5Nej = new System.Windows.Forms.RadioButton();
        this.radioButton5Ja = new System.Windows.Forms.RadioButton();
        this.label19 = new System.Windows.Forms.Label();
        this.groupBox13 = new System.Windows.Forms.GroupBox();
        this.radioButton4bNej = new System.Windows.Forms.RadioButton();
        this.radioButton4bJa = new System.Windows.Forms.RadioButton();
        this.label18 = new System.Windows.Forms.Label();
        this.groupBox12 = new System.Windows.Forms.GroupBox();
        this.radioButton4aNej = new System.Windows.Forms.RadioButton();
        this.radioButton4aJa = new System.Windows.Forms.RadioButton();
        this.label17 = new System.Windows.Forms.Label();
        this.groupBox10 = new System.Windows.Forms.GroupBox();
        this.radioButton3Ejaktuellt = new System.Windows.Forms.RadioButton();
        this.radioButton3Nej = new System.Windows.Forms.RadioButton();
        this.radioButton3Ja = new System.Windows.Forms.RadioButton();
        this.label16 = new System.Windows.Forms.Label();
        this.tabPageFrågor3 = new System.Windows.Forms.TabPage();
        this.groupBox19 = new System.Windows.Forms.GroupBox();
        this.radioButton12Nej = new System.Windows.Forms.RadioButton();
        this.radioButton12Ja = new System.Windows.Forms.RadioButton();
        this.label24 = new System.Windows.Forms.Label();
        this.groupBox21 = new System.Windows.Forms.GroupBox();
        this.radioButton11Nej = new System.Windows.Forms.RadioButton();
        this.radioButton11Ja = new System.Windows.Forms.RadioButton();
        this.label26 = new System.Windows.Forms.Label();
        this.groupBox20 = new System.Windows.Forms.GroupBox();
        this.radioButton10Nej = new System.Windows.Forms.RadioButton();
        this.radioButton10Ja = new System.Windows.Forms.RadioButton();
        this.label25 = new System.Windows.Forms.Label();
        this.groupBox11 = new System.Windows.Forms.GroupBox();
        this.richTextBoxOvrigt = new System.Windows.Forms.RichTextBox();
        this.environmentSlutavverkning = new FastReport.EnvironmentSettings();
        this.progressBarData = new System.Windows.Forms.ProgressBar();
        this.labelProgress = new System.Windows.Forms.Label();
        this.saveReportFolderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
        this.labelFragaObs = new System.Windows.Forms.Label();
        this.labelOBS = new System.Windows.Forms.Label();
        this.reportSlutavverkning = new FastReport.Report();
        this.textBoxRegion = new System.Windows.Forms.TextBox();
        this.textBoxDistrikt = new System.Windows.Forms.TextBox();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetSlutavverkning)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableUserData)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFragor)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetSettings)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableRegion)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableDistrikt)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableStandort)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableMarkberedningsmetod)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableUrsprung)).BeginInit();
        this.menuStripSlutavverkning.SuspendLayout();
        this.tabControl.SuspendLayout();
        this.tabPageSlutavverkning.SuspendLayout();
        this.groupBoxSlutavverkning.SuspendLayout();
        this.groupBoxMinstEnlSVL.SuspendLayout();
        this.groupBoxOBS.SuspendLayout();
        this.groupBox1.SuspendLayout();
        this.groupBox22.SuspendLayout();
        this.groupBoxTrakt.SuspendLayout();
        this.groupBoxRegion.SuspendLayout();
        this.tabPageFrågor1.SuspendLayout();
        this.groupBox9.SuspendLayout();
        this.groupBox8.SuspendLayout();
        this.groupBox7.SuspendLayout();
        this.groupBox6.SuspendLayout();
        this.groupBox5.SuspendLayout();
        this.groupBox2.SuspendLayout();
        this.groupBox4.SuspendLayout();
        this.groupBox3.SuspendLayout();
        this.tabPageFrågor2.SuspendLayout();
        this.groupBox18.SuspendLayout();
        this.groupBox17.SuspendLayout();
        this.groupBox16.SuspendLayout();
        this.groupBox15.SuspendLayout();
        this.groupBox14.SuspendLayout();
        this.groupBox13.SuspendLayout();
        this.groupBox12.SuspendLayout();
        this.groupBox10.SuspendLayout();
        this.tabPageFrågor3.SuspendLayout();
        this.groupBox19.SuspendLayout();
        this.groupBox21.SuspendLayout();
        this.groupBox20.SuspendLayout();
        this.groupBox11.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.reportSlutavverkning)).BeginInit();
        this.SuspendLayout();
        // 
        // dataSetSlutavverkning
        // 
        this.dataSetSlutavverkning.DataSetName = "DataSetSlutavverkning";
        this.dataSetSlutavverkning.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableUserData,
            this.dataTableFragor});
        // 
        // dataTableUserData
        // 
        this.dataTableUserData.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnRegionId,
            this.dataColumnRegion,
            this.dataColumnDistriktId,
            this.dataColumnDistrikt,
            this.dataColumnUrsprung,
            this.dataColumnDatum,
            this.dataColumnTraktnr,
            this.dataColumnTraktnamn,
            this.dataColumnStandort,
            this.dataColumnEntreprenor,
            this.dataColumnLanguage,
            this.dataColumnMailFrom,
            this.dataColumnStatus,
            this.dataColumnStatus_Datum,
            this.dataColumnÅrtal});
        this.dataTableUserData.TableName = "UserData";
        // 
        // dataColumnRegionId
        // 
        this.dataColumnRegionId.ColumnName = "RegionId";
        this.dataColumnRegionId.DataType = typeof(int);
        // 
        // dataColumnRegion
        // 
        this.dataColumnRegion.ColumnName = "Region";
        // 
        // dataColumnDistriktId
        // 
        this.dataColumnDistriktId.ColumnName = "DistriktId";
        this.dataColumnDistriktId.DataType = typeof(int);
        // 
        // dataColumnDistrikt
        // 
        this.dataColumnDistrikt.ColumnName = "Distrikt";
        // 
        // dataColumnUrsprung
        // 
        this.dataColumnUrsprung.ColumnName = "Ursprung";
        // 
        // dataColumnDatum
        // 
        this.dataColumnDatum.ColumnName = "Datum";
        this.dataColumnDatum.DataType = typeof(System.DateTime);
        // 
        // dataColumnTraktnr
        // 
        this.dataColumnTraktnr.ColumnName = "Traktnr";
        this.dataColumnTraktnr.DataType = typeof(int);
        // 
        // dataColumnTraktnamn
        // 
        this.dataColumnTraktnamn.ColumnName = "Traktnamn";
        // 
        // dataColumnStandort
        // 
        this.dataColumnStandort.ColumnName = "Standort";
        this.dataColumnStandort.DataType = typeof(int);
        // 
        // dataColumnEntreprenor
        // 
        this.dataColumnEntreprenor.ColumnName = "Entreprenor";
        this.dataColumnEntreprenor.DataType = typeof(int);
        // 
        // dataColumnLanguage
        // 
        this.dataColumnLanguage.ColumnName = "Language";
        // 
        // dataColumnMailFrom
        // 
        this.dataColumnMailFrom.ColumnName = "MailFrom";
        // 
        // dataColumnStatus
        // 
        this.dataColumnStatus.ColumnName = "Status";
        // 
        // dataColumnStatus_Datum
        // 
        this.dataColumnStatus_Datum.ColumnName = "Status_Datum";
        this.dataColumnStatus_Datum.DataType = typeof(System.DateTime);
        // 
        // dataColumnÅrtal
        // 
        this.dataColumnÅrtal.ColumnName = "Artal";
        this.dataColumnÅrtal.DataType = typeof(int);
        // 
        // dataTableFragor
        // 
        this.dataTableFragor.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFraga1,
            this.dataColumnFraga2,
            this.dataColumnFraga3,
            this.dataColumnFraga4,
            this.dataColumnFraga5,
            this.dataColumnFraga6,
            this.dataColumnFraga7,
            this.dataColumnFraga8,
            this.dataColumnFraga9,
            this.dataColumnFraga10,
            this.dataColumnFraga11,
            this.dataColumnFraga12,
            this.dataColumnFraga13,
            this.dataColumnFraga14,
            this.dataColumnFraga15,
            this.dataColumnFraga16,
            this.dataColumnFraga17,
            this.dataColumnFraga18,
            this.dataColumnFraga19,
            this.dataColumnFraga20,
            this.dataColumnFraga21,
            this.dataColumnOvrigt});
        this.dataTableFragor.TableName = "Fragor";
        // 
        // dataColumnFraga1
        // 
        this.dataColumnFraga1.Caption = "Fraga1";
        this.dataColumnFraga1.ColumnName = "Fraga1";
        // 
        // dataColumnFraga2
        // 
        this.dataColumnFraga2.ColumnName = "Fraga2";
        // 
        // dataColumnFraga3
        // 
        this.dataColumnFraga3.ColumnName = "Fraga3";
        // 
        // dataColumnFraga4
        // 
        this.dataColumnFraga4.ColumnName = "Fraga4";
        // 
        // dataColumnFraga5
        // 
        this.dataColumnFraga5.ColumnName = "Fraga5";
        // 
        // dataColumnFraga6
        // 
        this.dataColumnFraga6.ColumnName = "Fraga6";
        // 
        // dataColumnFraga7
        // 
        this.dataColumnFraga7.ColumnName = "Fraga7";
        // 
        // dataColumnFraga8
        // 
        this.dataColumnFraga8.ColumnName = "Fraga8";
        // 
        // dataColumnFraga9
        // 
        this.dataColumnFraga9.ColumnName = "Fraga9";
        // 
        // dataColumnFraga10
        // 
        this.dataColumnFraga10.ColumnName = "Fraga10";
        // 
        // dataColumnFraga11
        // 
        this.dataColumnFraga11.ColumnName = "Fraga11";
        // 
        // dataColumnFraga12
        // 
        this.dataColumnFraga12.ColumnName = "Fraga12";
        // 
        // dataColumnFraga13
        // 
        this.dataColumnFraga13.ColumnName = "Fraga13";
        // 
        // dataColumnFraga14
        // 
        this.dataColumnFraga14.ColumnName = "Fraga14";
        // 
        // dataColumnFraga15
        // 
        this.dataColumnFraga15.ColumnName = "Fraga15";
        // 
        // dataColumnFraga16
        // 
        this.dataColumnFraga16.ColumnName = "Fraga16";
        // 
        // dataColumnFraga17
        // 
        this.dataColumnFraga17.ColumnName = "Fraga17";
        // 
        // dataColumnFraga18
        // 
        this.dataColumnFraga18.ColumnName = "Fraga18";
        // 
        // dataColumnFraga19
        // 
        this.dataColumnFraga19.ColumnName = "Fraga19";
        // 
        // dataColumnFraga20
        // 
        this.dataColumnFraga20.ColumnName = "Fraga20";
        // 
        // dataColumnFraga21
        // 
        this.dataColumnFraga21.ColumnName = "Fraga21";
        // 
        // dataColumnOvrigt
        // 
        this.dataColumnOvrigt.ColumnName = "Ovrigt";
        // 
        // dataSetSettings
        // 
        this.dataSetSettings.DataSetName = "DataSetSettings";
        this.dataSetSettings.Relations.AddRange(new System.Data.DataRelation[] {
            new System.Data.DataRelation("RelationRegionDistrikt", "Region", "Distrikt", new string[] {
                        "Id"}, new string[] {
                        "RegionId"}, false)});
        this.dataSetSettings.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableRegion,
            this.dataTableDistrikt,
            this.dataTableStandort,
            this.dataTableMarkberedningsmetod,
            this.dataTableUrsprung});
        // 
        // dataTableRegion
        // 
        this.dataTableRegion.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSettingsRegionId,
            this.dataColumnSettingsRegionNamn});
        this.dataTableRegion.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, false)});
        this.dataTableRegion.TableName = "Region";
        // 
        // dataColumnSettingsRegionId
        // 
        this.dataColumnSettingsRegionId.ColumnName = "Id";
        this.dataColumnSettingsRegionId.DataType = typeof(int);
        // 
        // dataColumnSettingsRegionNamn
        // 
        this.dataColumnSettingsRegionNamn.ColumnName = "Namn";
        // 
        // dataTableDistrikt
        // 
        this.dataTableDistrikt.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSettingsDistriktRegionId,
            this.dataColumnSettingsDistriktId,
            this.dataColumnSettingsDistriktNamn});
        this.dataTableDistrikt.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.ForeignKeyConstraint("RelationRegionDistrikt", "Region", new string[] {
                        "Id"}, new string[] {
                        "RegionId"}, System.Data.AcceptRejectRule.None, System.Data.Rule.Cascade, System.Data.Rule.Cascade)});
        this.dataTableDistrikt.TableName = "Distrikt";
        // 
        // dataColumnSettingsDistriktRegionId
        // 
        this.dataColumnSettingsDistriktRegionId.ColumnName = "RegionId";
        this.dataColumnSettingsDistriktRegionId.DataType = typeof(int);
        // 
        // dataColumnSettingsDistriktId
        // 
        this.dataColumnSettingsDistriktId.ColumnName = "Id";
        this.dataColumnSettingsDistriktId.DataType = typeof(int);
        // 
        // dataColumnSettingsDistriktNamn
        // 
        this.dataColumnSettingsDistriktNamn.ColumnName = "Namn";
        // 
        // dataTableStandort
        // 
        this.dataTableStandort.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSettingsStandortNamn});
        this.dataTableStandort.TableName = "Standort";
        // 
        // dataColumnSettingsStandortNamn
        // 
        this.dataColumnSettingsStandortNamn.ColumnName = "Id";
        this.dataColumnSettingsStandortNamn.DataType = typeof(int);
        // 
        // dataTableMarkberedningsmetod
        // 
        this.dataTableMarkberedningsmetod.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSettingsMarkberedningsmetodNamn});
        this.dataTableMarkberedningsmetod.TableName = "Markberedningsmetod";
        // 
        // dataColumnSettingsMarkberedningsmetodNamn
        // 
        this.dataColumnSettingsMarkberedningsmetodNamn.ColumnName = "Namn";
        // 
        // dataTableUrsprung
        // 
        this.dataTableUrsprung.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSettingsUrsprungNamn});
        this.dataTableUrsprung.TableName = "Ursprung";
        // 
        // dataColumnSettingsUrsprungNamn
        // 
        this.dataColumnSettingsUrsprungNamn.ColumnName = "Namn";
        // 
        // menuStripSlutavverkning
        // 
        this.menuStripSlutavverkning.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.arkivToolStripMenuItem});
        this.menuStripSlutavverkning.Location = new System.Drawing.Point(0, 0);
        this.menuStripSlutavverkning.Name = "menuStripSlutavverkning";
        this.menuStripSlutavverkning.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
        this.menuStripSlutavverkning.Size = new System.Drawing.Size(563, 24);
        this.menuStripSlutavverkning.TabIndex = 0;
        this.menuStripSlutavverkning.Text = "menuStrip1";
        // 
        // arkivToolStripMenuItem
        // 
        this.arkivToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sparaToolStripMenuItem,
            this.sparaSomToolStripMenuItem,
            this.toolStripSeparator2,
            this.avslutaToolStripMenuItem});
        this.arkivToolStripMenuItem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.arkivToolStripMenuItem.Name = "arkivToolStripMenuItem";
        this.arkivToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
        this.arkivToolStripMenuItem.Text = global::Egenuppfoljning.Properties.Resources.Arkiv;
        // 
        // sparaToolStripMenuItem
        // 
        this.sparaToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("sparaToolStripMenuItem.Image")));
        this.sparaToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
        this.sparaToolStripMenuItem.Name = "sparaToolStripMenuItem";
        this.sparaToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
        this.sparaToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
        this.sparaToolStripMenuItem.Text = global::Egenuppfoljning.Properties.Resources.Spara;
        this.sparaToolStripMenuItem.Click += new System.EventHandler(this.sparaToolStripMenuItem_Click);
        // 
        // sparaSomToolStripMenuItem
        // 
        this.sparaSomToolStripMenuItem.Name = "sparaSomToolStripMenuItem";
        this.sparaSomToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
        this.sparaSomToolStripMenuItem.Text = global::Egenuppfoljning.Properties.Resources.SparaSom;
        this.sparaSomToolStripMenuItem.Click += new System.EventHandler(this.sparaSomToolStripMenuItem_Click);
        // 
        // toolStripSeparator2
        // 
        this.toolStripSeparator2.Name = "toolStripSeparator2";
        this.toolStripSeparator2.Size = new System.Drawing.Size(147, 6);
        // 
        // avslutaToolStripMenuItem
        // 
        this.avslutaToolStripMenuItem.Name = "avslutaToolStripMenuItem";
        this.avslutaToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
        this.avslutaToolStripMenuItem.Text = "Stäng";
        this.avslutaToolStripMenuItem.Click += new System.EventHandler(this.avslutaToolStripMenuItem_Click);
        // 
        // tabControl
        // 
        this.tabControl.Controls.Add(this.tabPageSlutavverkning);
        this.tabControl.Controls.Add(this.tabPageFrågor1);
        this.tabControl.Controls.Add(this.tabPageFrågor2);
        this.tabControl.Controls.Add(this.tabPageFrågor3);
        this.tabControl.Location = new System.Drawing.Point(0, 27);
        this.tabControl.Name = "tabControl";
        this.tabControl.SelectedIndex = 0;
        this.tabControl.Size = new System.Drawing.Size(559, 494);
        this.tabControl.TabIndex = 1;
        // 
        // tabPageSlutavverkning
        // 
        this.tabPageSlutavverkning.Controls.Add(this.groupBoxSlutavverkning);
        this.tabPageSlutavverkning.Controls.Add(this.groupBoxTrakt);
        this.tabPageSlutavverkning.Controls.Add(this.groupBoxRegion);
        this.tabPageSlutavverkning.Location = new System.Drawing.Point(4, 22);
        this.tabPageSlutavverkning.Name = "tabPageSlutavverkning";
        this.tabPageSlutavverkning.Padding = new System.Windows.Forms.Padding(3);
        this.tabPageSlutavverkning.Size = new System.Drawing.Size(551, 468);
        this.tabPageSlutavverkning.TabIndex = 2;
        this.tabPageSlutavverkning.Text = "Slutavverkning";
        this.tabPageSlutavverkning.UseVisualStyleBackColor = true;
        // 
        // groupBoxSlutavverkning
        // 
        this.groupBoxSlutavverkning.Controls.Add(this.groupBoxMinstEnlSVL);
        this.groupBoxSlutavverkning.Controls.Add(this.groupBoxOBS);
        this.groupBoxSlutavverkning.Controls.Add(this.groupBox1);
        this.groupBoxSlutavverkning.Controls.Add(this.label4);
        this.groupBoxSlutavverkning.Location = new System.Drawing.Point(5, 124);
        this.groupBoxSlutavverkning.Name = "groupBoxSlutavverkning";
        this.groupBoxSlutavverkning.Size = new System.Drawing.Size(540, 338);
        this.groupBoxSlutavverkning.TabIndex = 2;
        this.groupBoxSlutavverkning.TabStop = false;
        // 
        // groupBoxMinstEnlSVL
        // 
        this.groupBoxMinstEnlSVL.Controls.Add(this.radioButton1bNej);
        this.groupBoxMinstEnlSVL.Controls.Add(this.radioButton1bJa);
        this.groupBoxMinstEnlSVL.Location = new System.Drawing.Point(408, 39);
        this.groupBoxMinstEnlSVL.Name = "groupBoxMinstEnlSVL";
        this.groupBoxMinstEnlSVL.Size = new System.Drawing.Size(122, 77);
        this.groupBoxMinstEnlSVL.TabIndex = 7;
        this.groupBoxMinstEnlSVL.TabStop = false;
        this.groupBoxMinstEnlSVL.Text = "Minst enl SVL §30";
        // 
        // radioButton1bNej
        // 
        this.radioButton1bNej.AutoSize = true;
        this.radioButton1bNej.Location = new System.Drawing.Point(62, 31);
        this.radioButton1bNej.Name = "radioButton1bNej";
        this.radioButton1bNej.Size = new System.Drawing.Size(43, 17);
        this.radioButton1bNej.TabIndex = 20;
        this.radioButton1bNej.TabStop = true;
        this.radioButton1bNej.Text = "Nej";
        this.radioButton1bNej.UseVisualStyleBackColor = true;
        this.radioButton1bNej.CheckedChanged += new System.EventHandler(this.radioButton1bNej_CheckedChanged);
        // 
        // radioButton1bJa
        // 
        this.radioButton1bJa.AutoSize = true;
        this.radioButton1bJa.Location = new System.Drawing.Point(18, 31);
        this.radioButton1bJa.Name = "radioButton1bJa";
        this.radioButton1bJa.Size = new System.Drawing.Size(38, 17);
        this.radioButton1bJa.TabIndex = 19;
        this.radioButton1bJa.TabStop = true;
        this.radioButton1bJa.Text = "Ja";
        this.radioButton1bJa.UseVisualStyleBackColor = true;
        this.radioButton1bJa.CheckedChanged += new System.EventHandler(this.radioButton1bJa_CheckedChanged);
        // 
        // groupBoxOBS
        // 
        this.groupBoxOBS.Controls.Add(this.label7);
        this.groupBoxOBS.Controls.Add(this.label8);
        this.groupBoxOBS.Location = new System.Drawing.Point(12, 108);
        this.groupBoxOBS.Name = "groupBoxOBS";
        this.groupBoxOBS.Size = new System.Drawing.Size(518, 66);
        this.groupBoxOBS.TabIndex = 6;
        this.groupBoxOBS.TabStop = false;
        // 
        // label7
        // 
        this.label7.AutoSize = true;
        this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label7.Location = new System.Drawing.Point(24, 38);
        this.label7.Name = "label7";
        this.label7.Size = new System.Drawing.Size(426, 13);
        this.label7.TabIndex = 1;
        this.label7.Text = "Ifall \"Nej\" har valts så ska en  miljörapport skrivas, ange vad som avviker.";
        // 
        // label8
        // 
        this.label8.AutoSize = true;
        this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label8.Location = new System.Drawing.Point(19, 17);
        this.label8.Name = "label8";
        this.label8.Size = new System.Drawing.Size(32, 13);
        this.label8.TabIndex = 0;
        this.label8.Text = "OBS!";
        // 
        // groupBox1
        // 
        this.groupBox1.Controls.Add(this.label3);
        this.groupBox1.Controls.Add(this.groupBox22);
        this.groupBox1.Location = new System.Drawing.Point(12, 39);
        this.groupBox1.Name = "groupBox1";
        this.groupBox1.Size = new System.Drawing.Size(518, 79);
        this.groupBox1.TabIndex = 5;
        this.groupBox1.TabStop = false;
        this.groupBox1.Text = "1.";
        // 
        // label3
        // 
        this.label3.AutoSize = true;
        this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label3.Location = new System.Drawing.Point(24, 33);
        this.label3.Name = "label3";
        this.label3.Size = new System.Drawing.Size(245, 13);
        this.label3.TabIndex = 0;
        this.label3.Text = "Är avverkning utförd enligt traktdirektiv ?";
        // 
        // groupBox22
        // 
        this.groupBox22.Controls.Add(this.radioButton1aNej);
        this.groupBox22.Controls.Add(this.radioButton1aJa);
        this.groupBox22.Location = new System.Drawing.Point(276, 0);
        this.groupBox22.Name = "groupBox22";
        this.groupBox22.Size = new System.Drawing.Size(122, 77);
        this.groupBox22.TabIndex = 3;
        this.groupBox22.TabStop = false;
        this.groupBox22.Text = "Enl RUS";
        // 
        // radioButton1aNej
        // 
        this.radioButton1aNej.AutoSize = true;
        this.radioButton1aNej.Location = new System.Drawing.Point(62, 31);
        this.radioButton1aNej.Name = "radioButton1aNej";
        this.radioButton1aNej.Size = new System.Drawing.Size(43, 17);
        this.radioButton1aNej.TabIndex = 18;
        this.radioButton1aNej.TabStop = true;
        this.radioButton1aNej.Text = "Nej";
        this.radioButton1aNej.UseVisualStyleBackColor = true;
        this.radioButton1aNej.CheckedChanged += new System.EventHandler(this.radioButton1aNej_CheckedChanged);
        // 
        // radioButton1aJa
        // 
        this.radioButton1aJa.AutoSize = true;
        this.radioButton1aJa.Location = new System.Drawing.Point(18, 31);
        this.radioButton1aJa.Name = "radioButton1aJa";
        this.radioButton1aJa.Size = new System.Drawing.Size(38, 17);
        this.radioButton1aJa.TabIndex = 17;
        this.radioButton1aJa.TabStop = true;
        this.radioButton1aJa.Text = "Ja";
        this.radioButton1aJa.UseVisualStyleBackColor = true;
        this.radioButton1aJa.CheckedChanged += new System.EventHandler(this.radioButton1aJa_CheckedChanged);
        // 
        // label4
        // 
        this.label4.AutoSize = true;
        this.label4.Location = new System.Drawing.Point(13, 17);
        this.label4.Name = "label4";
        this.label4.Size = new System.Drawing.Size(53, 13);
        this.label4.TabIndex = 0;
        this.label4.Text = "FRÅGOR";
        // 
        // groupBoxTrakt
        // 
        this.groupBoxTrakt.Controls.Add(this.textBoxTraktnr);
        this.groupBoxTrakt.Controls.Add(this.textBoxTraktnamn);
        this.groupBoxTrakt.Controls.Add(this.textBoxEntreprenor);
        this.groupBoxTrakt.Controls.Add(this.comboBoxStandort);
        this.groupBoxTrakt.Controls.Add(this.labeEntreprenor);
        this.groupBoxTrakt.Controls.Add(this.labelStandort);
        this.groupBoxTrakt.Controls.Add(this.labelTraktnamn);
        this.groupBoxTrakt.Controls.Add(this.labelTraktnr);
        this.groupBoxTrakt.Location = new System.Drawing.Point(5, 60);
        this.groupBoxTrakt.Name = "groupBoxTrakt";
        this.groupBoxTrakt.Size = new System.Drawing.Size(540, 72);
        this.groupBoxTrakt.TabIndex = 1;
        this.groupBoxTrakt.TabStop = false;
        // 
        // textBoxTraktnr
        // 
        this.textBoxTraktnr.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetSlutavverkning, "UserData.Traktnr", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, null, "000000"));
        this.textBoxTraktnr.Location = new System.Drawing.Point(15, 29);
        this.textBoxTraktnr.MaxLength = 6;
        this.textBoxTraktnr.Name = "textBoxTraktnr";
        this.textBoxTraktnr.Size = new System.Drawing.Size(116, 21);
        this.textBoxTraktnr.TabIndex = 1;
        this.textBoxTraktnr.TextChanged += new System.EventHandler(this.textBoxTraktnr_TextChanged);
        this.textBoxTraktnr.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxTraktnr_KeyDown);
        this.textBoxTraktnr.Leave += new System.EventHandler(this.textBoxTraktnr_Leave);
        // 
        // textBoxTraktnamn
        // 
        this.textBoxTraktnamn.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetSlutavverkning, "UserData.Traktnamn", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.textBoxTraktnamn.Location = new System.Drawing.Point(140, 30);
        this.textBoxTraktnamn.MaxLength = 31;
        this.textBoxTraktnamn.Name = "textBoxTraktnamn";
        this.textBoxTraktnamn.Size = new System.Drawing.Size(116, 21);
        this.textBoxTraktnamn.TabIndex = 3;
        this.textBoxTraktnamn.TextChanged += new System.EventHandler(this.textBoxTraktnamn_TextChanged);
        // 
        // textBoxEntreprenor
        // 
        this.textBoxEntreprenor.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetSlutavverkning, "UserData.Entreprenor", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, null, "0000"));
        this.textBoxEntreprenor.Location = new System.Drawing.Point(388, 31);
        this.textBoxEntreprenor.MaxLength = 4;
        this.textBoxEntreprenor.Name = "textBoxEntreprenor";
        this.textBoxEntreprenor.Size = new System.Drawing.Size(116, 21);
        this.textBoxEntreprenor.TabIndex = 7;
        this.textBoxEntreprenor.TextChanged += new System.EventHandler(this.textBoxEntreprenor_TextChanged);
        this.textBoxEntreprenor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxEntreprenor_KeyDown);
        this.textBoxEntreprenor.Leave += new System.EventHandler(this.textBoxEntreprenor_Leave);
        // 
        // comboBoxStandort
        // 
        this.comboBoxStandort.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetSlutavverkning, "UserData.Standort", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.comboBoxStandort.DataSource = this.dataSetSettings;
        this.comboBoxStandort.DisplayMember = "Standort.Id";
        this.comboBoxStandort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxStandort.FormattingEnabled = true;
        this.comboBoxStandort.Location = new System.Drawing.Point(265, 30);
        this.comboBoxStandort.Name = "comboBoxStandort";
        this.comboBoxStandort.Size = new System.Drawing.Size(116, 21);
        this.comboBoxStandort.TabIndex = 5;
        this.comboBoxStandort.SelectionChangeCommitted += new System.EventHandler(this.comboBoxStandort_SelectionChangeCommitted);
        this.comboBoxStandort.SelectedIndexChanged += new System.EventHandler(this.comboBoxStandort_SelectedIndexChanged);
        // 
        // labeEntreprenor
        // 
        this.labeEntreprenor.AutoSize = true;
        this.labeEntreprenor.Location = new System.Drawing.Point(384, 14);
        this.labeEntreprenor.Name = "labeEntreprenor";
        this.labeEntreprenor.Size = new System.Drawing.Size(145, 13);
        this.labeEntreprenor.TabIndex = 6;
        this.labeEntreprenor.Text = "Maskinnr/Entreprenörnr";
        // 
        // labelStandort
        // 
        this.labelStandort.AutoSize = true;
        this.labelStandort.Location = new System.Drawing.Point(262, 14);
        this.labelStandort.Name = "labelStandort";
        this.labelStandort.Size = new System.Drawing.Size(57, 13);
        this.labelStandort.TabIndex = 4;
        this.labelStandort.Text = "Ståndort";
        // 
        // labelTraktnamn
        // 
        this.labelTraktnamn.AutoSize = true;
        this.labelTraktnamn.Location = new System.Drawing.Point(140, 14);
        this.labelTraktnamn.Name = "labelTraktnamn";
        this.labelTraktnamn.Size = new System.Drawing.Size(70, 13);
        this.labelTraktnamn.TabIndex = 2;
        this.labelTraktnamn.Text = "Traktnamn";
        // 
        // labelTraktnr
        // 
        this.labelTraktnr.AutoSize = true;
        this.labelTraktnr.Location = new System.Drawing.Point(13, 14);
        this.labelTraktnr.Name = "labelTraktnr";
        this.labelTraktnr.Size = new System.Drawing.Size(50, 13);
        this.labelTraktnr.TabIndex = 0;
        this.labelTraktnr.Text = "Traktnr";
        // 
        // groupBoxRegion
        // 
        this.groupBoxRegion.Controls.Add(this.textBoxDistrikt);
        this.groupBoxRegion.Controls.Add(this.textBoxRegion);
        this.groupBoxRegion.Controls.Add(this.comboBoxUrsprung);
        this.groupBoxRegion.Controls.Add(this.dateTimePicker);
        this.groupBoxRegion.Controls.Add(this.comboBoxDistrikt);
        this.groupBoxRegion.Controls.Add(this.comboBoxRegion);
        this.groupBoxRegion.Controls.Add(this.labelDatum);
        this.groupBoxRegion.Controls.Add(this.labelUrsprung);
        this.groupBoxRegion.Controls.Add(this.labelDistrikt);
        this.groupBoxRegion.Controls.Add(this.labelRegion);
        this.groupBoxRegion.Location = new System.Drawing.Point(5, 0);
        this.groupBoxRegion.Name = "groupBoxRegion";
        this.groupBoxRegion.Size = new System.Drawing.Size(540, 72);
        this.groupBoxRegion.TabIndex = 0;
        this.groupBoxRegion.TabStop = false;
        // 
        // comboBoxUrsprung
        // 
        this.comboBoxUrsprung.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetSlutavverkning, "UserData.Ursprung", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.comboBoxUrsprung.DataSource = this.dataSetSettings;
        this.comboBoxUrsprung.DisplayMember = "Ursprung.Namn";
        this.comboBoxUrsprung.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxUrsprung.FormattingEnabled = true;
        this.comboBoxUrsprung.Location = new System.Drawing.Point(267, 30);
        this.comboBoxUrsprung.Name = "comboBoxUrsprung";
        this.comboBoxUrsprung.Size = new System.Drawing.Size(116, 21);
        this.comboBoxUrsprung.TabIndex = 5;
        this.comboBoxUrsprung.SelectionChangeCommitted += new System.EventHandler(this.comboBoxUrsprung_SelectionChangeCommitted);
        this.comboBoxUrsprung.SelectedIndexChanged += new System.EventHandler(this.comboBoxUrsprung_SelectedIndexChanged);
        // 
        // dateTimePicker
        // 
        this.dateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.dataSetSlutavverkning, "UserData.Datum", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.dateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
        this.dateTimePicker.Location = new System.Drawing.Point(391, 31);
        this.dateTimePicker.Name = "dateTimePicker";
        this.dateTimePicker.Size = new System.Drawing.Size(116, 21);
        this.dateTimePicker.TabIndex = 7;
        this.dateTimePicker.ValueChanged += new System.EventHandler(this.dateTimePicker_ValueChanged);
        // 
        // comboBoxDistrikt
        // 
        this.comboBoxDistrikt.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetSlutavverkning, "UserData.Distrikt", true));
        this.comboBoxDistrikt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxDistrikt.FormattingEnabled = true;
        this.comboBoxDistrikt.Location = new System.Drawing.Point(140, 30);
        this.comboBoxDistrikt.Name = "comboBoxDistrikt";
        this.comboBoxDistrikt.Size = new System.Drawing.Size(116, 21);
        this.comboBoxDistrikt.TabIndex = 3;
        this.comboBoxDistrikt.SelectionChangeCommitted += new System.EventHandler(this.comboBoxDistrikt_SelectionChangeCommitted);
        this.comboBoxDistrikt.SelectedIndexChanged += new System.EventHandler(this.comboBoxDistrikt_SelectedIndexChanged);
        // 
        // comboBoxRegion
        // 
        this.comboBoxRegion.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetSlutavverkning, "UserData.Region", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.comboBoxRegion.DataSource = this.dataSetSettings;
        this.comboBoxRegion.DisplayMember = "Region.Namn";
        this.comboBoxRegion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxRegion.FormattingEnabled = true;
        this.comboBoxRegion.Location = new System.Drawing.Point(16, 30);
        this.comboBoxRegion.Name = "comboBoxRegion";
        this.comboBoxRegion.Size = new System.Drawing.Size(116, 21);
        this.comboBoxRegion.TabIndex = 1;
        this.comboBoxRegion.ValueMember = "Region.Id";
        this.comboBoxRegion.SelectionChangeCommitted += new System.EventHandler(this.comboBoxRegion_SelectionChangeCommitted);
        this.comboBoxRegion.SelectedIndexChanged += new System.EventHandler(this.comboBoxRegion_SelectedIndexChanged);
        // 
        // labelDatum
        // 
        this.labelDatum.AutoSize = true;
        this.labelDatum.Location = new System.Drawing.Point(387, 14);
        this.labelDatum.Name = "labelDatum";
        this.labelDatum.Size = new System.Drawing.Size(45, 13);
        this.labelDatum.TabIndex = 6;
        this.labelDatum.Text = "Datum";
        // 
        // labelUrsprung
        // 
        this.labelUrsprung.AutoSize = true;
        this.labelUrsprung.Location = new System.Drawing.Point(264, 14);
        this.labelUrsprung.Name = "labelUrsprung";
        this.labelUrsprung.Size = new System.Drawing.Size(59, 13);
        this.labelUrsprung.TabIndex = 4;
        this.labelUrsprung.Text = "Ursprung";
        // 
        // labelDistrikt
        // 
        this.labelDistrikt.AutoSize = true;
        this.labelDistrikt.Location = new System.Drawing.Point(140, 14);
        this.labelDistrikt.Name = "labelDistrikt";
        this.labelDistrikt.Size = new System.Drawing.Size(49, 13);
        this.labelDistrikt.TabIndex = 2;
        this.labelDistrikt.Text = "Distrikt";
        // 
        // labelRegion
        // 
        this.labelRegion.AutoSize = true;
        this.labelRegion.Location = new System.Drawing.Point(13, 14);
        this.labelRegion.Name = "labelRegion";
        this.labelRegion.Size = new System.Drawing.Size(46, 13);
        this.labelRegion.TabIndex = 0;
        this.labelRegion.Text = "Region";
        // 
        // tabPageFrågor1
        // 
        this.tabPageFrågor1.Controls.Add(this.label5);
        this.tabPageFrågor1.Controls.Add(this.groupBox9);
        this.tabPageFrågor1.Controls.Add(this.groupBox8);
        this.tabPageFrågor1.Controls.Add(this.groupBox7);
        this.tabPageFrågor1.Controls.Add(this.groupBox6);
        this.tabPageFrågor1.Controls.Add(this.groupBox5);
        this.tabPageFrågor1.Controls.Add(this.groupBox2);
        this.tabPageFrågor1.Controls.Add(this.groupBox4);
        this.tabPageFrågor1.Controls.Add(this.groupBox3);
        this.tabPageFrågor1.Location = new System.Drawing.Point(4, 22);
        this.tabPageFrågor1.Name = "tabPageFrågor1";
        this.tabPageFrågor1.Padding = new System.Windows.Forms.Padding(3);
        this.tabPageFrågor1.Size = new System.Drawing.Size(551, 468);
        this.tabPageFrågor1.TabIndex = 1;
        this.tabPageFrågor1.Text = "Frågor Sida 1";
        this.tabPageFrågor1.UseVisualStyleBackColor = true;
        // 
        // label5
        // 
        this.label5.AutoSize = true;
        this.label5.Location = new System.Drawing.Point(5, 9);
        this.label5.Name = "label5";
        this.label5.Size = new System.Drawing.Size(52, 13);
        this.label5.TabIndex = 17;
        this.label5.Text = "ENL RUS";
        // 
        // groupBox9
        // 
        this.groupBox9.Controls.Add(this.radioButton2hNej);
        this.groupBox9.Controls.Add(this.radioButton2hJa);
        this.groupBox9.Controls.Add(this.label15);
        this.groupBox9.Location = new System.Drawing.Point(8, 386);
        this.groupBox9.Name = "groupBox9";
        this.groupBox9.Size = new System.Drawing.Size(507, 55);
        this.groupBox9.TabIndex = 16;
        this.groupBox9.TabStop = false;
        this.groupBox9.Text = "2h.";
        // 
        // radioButton2hNej
        // 
        this.radioButton2hNej.AutoSize = true;
        this.radioButton2hNej.Location = new System.Drawing.Point(450, 25);
        this.radioButton2hNej.Name = "radioButton2hNej";
        this.radioButton2hNej.Size = new System.Drawing.Size(43, 17);
        this.radioButton2hNej.TabIndex = 29;
        this.radioButton2hNej.TabStop = true;
        this.radioButton2hNej.Text = "Nej";
        this.radioButton2hNej.UseVisualStyleBackColor = true;
        this.radioButton2hNej.CheckedChanged += new System.EventHandler(this.radioButton2hNej_CheckedChanged);
        // 
        // radioButton2hJa
        // 
        this.radioButton2hJa.AutoSize = true;
        this.radioButton2hJa.Location = new System.Drawing.Point(406, 25);
        this.radioButton2hJa.Name = "radioButton2hJa";
        this.radioButton2hJa.Size = new System.Drawing.Size(38, 17);
        this.radioButton2hJa.TabIndex = 28;
        this.radioButton2hJa.TabStop = true;
        this.radioButton2hJa.Text = "Ja";
        this.radioButton2hJa.UseVisualStyleBackColor = true;
        this.radioButton2hJa.CheckedChanged += new System.EventHandler(this.radioButton2hJa_CheckedChanged);
        // 
        // label15
        // 
        this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label15.Location = new System.Drawing.Point(19, 21);
        this.label15.Name = "label15";
        this.label15.Size = new System.Drawing.Size(376, 26);
        this.label15.TabIndex = 0;
        this.label15.Text = "Har större kalyta undvikits genom att spara trädgrupper ? (max 80 eller 120 m)";
        // 
        // groupBox8
        // 
        this.groupBox8.Controls.Add(this.radioButton2gNej);
        this.groupBox8.Controls.Add(this.label14);
        this.groupBox8.Controls.Add(this.radioButton2gJa);
        this.groupBox8.Location = new System.Drawing.Point(8, 338);
        this.groupBox8.Name = "groupBox8";
        this.groupBox8.Size = new System.Drawing.Size(507, 55);
        this.groupBox8.TabIndex = 15;
        this.groupBox8.TabStop = false;
        this.groupBox8.Text = "2g.";
        // 
        // radioButton2gNej
        // 
        this.radioButton2gNej.AutoSize = true;
        this.radioButton2gNej.Location = new System.Drawing.Point(450, 21);
        this.radioButton2gNej.Name = "radioButton2gNej";
        this.radioButton2gNej.Size = new System.Drawing.Size(43, 17);
        this.radioButton2gNej.TabIndex = 27;
        this.radioButton2gNej.TabStop = true;
        this.radioButton2gNej.Text = "Nej";
        this.radioButton2gNej.UseVisualStyleBackColor = true;
        this.radioButton2gNej.CheckedChanged += new System.EventHandler(this.radioButton2gNej_CheckedChanged);
        // 
        // label14
        // 
        this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label14.Location = new System.Drawing.Point(19, 23);
        this.label14.Name = "label14";
        this.label14.Size = new System.Drawing.Size(376, 26);
        this.label14.TabIndex = 0;
        this.label14.Text = "Har död ved värnats/lämnats ?";
        // 
        // radioButton2gJa
        // 
        this.radioButton2gJa.AutoSize = true;
        this.radioButton2gJa.Location = new System.Drawing.Point(406, 21);
        this.radioButton2gJa.Name = "radioButton2gJa";
        this.radioButton2gJa.Size = new System.Drawing.Size(38, 17);
        this.radioButton2gJa.TabIndex = 26;
        this.radioButton2gJa.TabStop = true;
        this.radioButton2gJa.Text = "Ja";
        this.radioButton2gJa.UseVisualStyleBackColor = true;
        this.radioButton2gJa.CheckedChanged += new System.EventHandler(this.radioButton2gJa_CheckedChanged);
        // 
        // groupBox7
        // 
        this.groupBox7.Controls.Add(this.radioButton2fNej);
        this.groupBox7.Controls.Add(this.radioButton2fJa);
        this.groupBox7.Controls.Add(this.label13);
        this.groupBox7.Location = new System.Drawing.Point(8, 286);
        this.groupBox7.Name = "groupBox7";
        this.groupBox7.Size = new System.Drawing.Size(507, 55);
        this.groupBox7.TabIndex = 14;
        this.groupBox7.TabStop = false;
        this.groupBox7.Text = "2f.";
        // 
        // radioButton2fNej
        // 
        this.radioButton2fNej.AutoSize = true;
        this.radioButton2fNej.Location = new System.Drawing.Point(450, 26);
        this.radioButton2fNej.Name = "radioButton2fNej";
        this.radioButton2fNej.Size = new System.Drawing.Size(43, 17);
        this.radioButton2fNej.TabIndex = 25;
        this.radioButton2fNej.TabStop = true;
        this.radioButton2fNej.Text = "Nej";
        this.radioButton2fNej.UseVisualStyleBackColor = true;
        this.radioButton2fNej.CheckedChanged += new System.EventHandler(this.radioButton2fNej_CheckedChanged);
        // 
        // radioButton2fJa
        // 
        this.radioButton2fJa.AutoSize = true;
        this.radioButton2fJa.Location = new System.Drawing.Point(406, 26);
        this.radioButton2fJa.Name = "radioButton2fJa";
        this.radioButton2fJa.Size = new System.Drawing.Size(38, 17);
        this.radioButton2fJa.TabIndex = 24;
        this.radioButton2fJa.TabStop = true;
        this.radioButton2fJa.Text = "Ja";
        this.radioButton2fJa.UseVisualStyleBackColor = true;
        this.radioButton2fJa.CheckedChanged += new System.EventHandler(this.radioButton2fJa_CheckedChanged);
        // 
        // label13
        // 
        this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label13.Location = new System.Drawing.Point(19, 21);
        this.label13.Name = "label13";
        this.label13.Size = new System.Drawing.Size(353, 26);
        this.label13.TabIndex = 0;
        this.label13.Text = "Är antalet högstubbar minst 3/ha och jämnt fördelat mellan trädslagen  ? ";
        // 
        // groupBox6
        // 
        this.groupBox6.Controls.Add(this.radioButton2eNej);
        this.groupBox6.Controls.Add(this.radioButton2eJa);
        this.groupBox6.Controls.Add(this.label12);
        this.groupBox6.Location = new System.Drawing.Point(8, 233);
        this.groupBox6.Name = "groupBox6";
        this.groupBox6.Size = new System.Drawing.Size(507, 55);
        this.groupBox6.TabIndex = 13;
        this.groupBox6.TabStop = false;
        this.groupBox6.Text = "2e.";
        // 
        // radioButton2eNej
        // 
        this.radioButton2eNej.AutoSize = true;
        this.radioButton2eNej.Location = new System.Drawing.Point(450, 27);
        this.radioButton2eNej.Name = "radioButton2eNej";
        this.radioButton2eNej.Size = new System.Drawing.Size(43, 17);
        this.radioButton2eNej.TabIndex = 23;
        this.radioButton2eNej.TabStop = true;
        this.radioButton2eNej.Text = "Nej";
        this.radioButton2eNej.UseVisualStyleBackColor = true;
        this.radioButton2eNej.CheckedChanged += new System.EventHandler(this.radioButton2eNej_CheckedChanged);
        // 
        // radioButton2eJa
        // 
        this.radioButton2eJa.AutoSize = true;
        this.radioButton2eJa.Location = new System.Drawing.Point(406, 27);
        this.radioButton2eJa.Name = "radioButton2eJa";
        this.radioButton2eJa.Size = new System.Drawing.Size(38, 17);
        this.radioButton2eJa.TabIndex = 22;
        this.radioButton2eJa.TabStop = true;
        this.radioButton2eJa.Text = "Ja";
        this.radioButton2eJa.UseVisualStyleBackColor = true;
        this.radioButton2eJa.CheckedChanged += new System.EventHandler(this.radioButton2eJa_CheckedChanged);
        // 
        // label12
        // 
        this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label12.Location = new System.Drawing.Point(19, 21);
        this.label12.Name = "label12";
        this.label12.Size = new System.Drawing.Size(353, 26);
        this.label12.TabIndex = 0;
        this.label12.Text = "Är antalet framtidsträd som speglar traktens trslfördelning minst 10/ha inkl natu" +
            "rvärdeträd ?";
        // 
        // groupBox5
        // 
        this.groupBox5.Controls.Add(this.radioButton2dEjaktuellt);
        this.groupBox5.Controls.Add(this.radioButton2dNej);
        this.groupBox5.Controls.Add(this.radioButton2dJa);
        this.groupBox5.Controls.Add(this.label11);
        this.groupBox5.Location = new System.Drawing.Point(8, 184);
        this.groupBox5.Name = "groupBox5";
        this.groupBox5.Size = new System.Drawing.Size(507, 55);
        this.groupBox5.TabIndex = 12;
        this.groupBox5.TabStop = false;
        this.groupBox5.Text = "2d.";
        // 
        // radioButton2dEjaktuellt
        // 
        this.radioButton2dEjaktuellt.AutoSize = true;
        this.radioButton2dEjaktuellt.Location = new System.Drawing.Point(419, 22);
        this.radioButton2dEjaktuellt.Name = "radioButton2dEjaktuellt";
        this.radioButton2dEjaktuellt.Size = new System.Drawing.Size(82, 17);
        this.radioButton2dEjaktuellt.TabIndex = 20;
        this.radioButton2dEjaktuellt.TabStop = true;
        this.radioButton2dEjaktuellt.Text = "Ej aktuellt";
        this.radioButton2dEjaktuellt.UseVisualStyleBackColor = true;
        this.radioButton2dEjaktuellt.CheckedChanged += new System.EventHandler(this.radioButton2dEjaktuellt_CheckedChanged);
        // 
        // radioButton2dNej
        // 
        this.radioButton2dNej.AutoSize = true;
        this.radioButton2dNej.Location = new System.Drawing.Point(370, 22);
        this.radioButton2dNej.Name = "radioButton2dNej";
        this.radioButton2dNej.Size = new System.Drawing.Size(43, 17);
        this.radioButton2dNej.TabIndex = 19;
        this.radioButton2dNej.TabStop = true;
        this.radioButton2dNej.Text = "Nej";
        this.radioButton2dNej.UseVisualStyleBackColor = true;
        this.radioButton2dNej.CheckedChanged += new System.EventHandler(this.radioButton2dNej_CheckedChanged);
        // 
        // radioButton2dJa
        // 
        this.radioButton2dJa.AutoSize = true;
        this.radioButton2dJa.Location = new System.Drawing.Point(326, 22);
        this.radioButton2dJa.Name = "radioButton2dJa";
        this.radioButton2dJa.Size = new System.Drawing.Size(38, 17);
        this.radioButton2dJa.TabIndex = 18;
        this.radioButton2dJa.TabStop = true;
        this.radioButton2dJa.Text = "Ja";
        this.radioButton2dJa.UseVisualStyleBackColor = true;
        this.radioButton2dJa.CheckedChanged += new System.EventHandler(this.radioButton2dJa_CheckedChanged);
        // 
        // label11
        // 
        this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label11.Location = new System.Drawing.Point(19, 24);
        this.label11.Name = "label11";
        this.label11.Size = new System.Drawing.Size(223, 26);
        this.label11.TabIndex = 0;
        this.label11.Text = "Är alla naturvärdesträd värnade ?";
        // 
        // groupBox2
        // 
        this.groupBox2.Controls.Add(this.radioButton2cEjaktuellt);
        this.groupBox2.Controls.Add(this.radioButton2cNej);
        this.groupBox2.Controls.Add(this.label10);
        this.groupBox2.Controls.Add(this.radioButton2cJa);
        this.groupBox2.Location = new System.Drawing.Point(8, 130);
        this.groupBox2.Name = "groupBox2";
        this.groupBox2.Size = new System.Drawing.Size(507, 63);
        this.groupBox2.TabIndex = 11;
        this.groupBox2.TabStop = false;
        this.groupBox2.Text = "2c.";
        // 
        // radioButton2cEjaktuellt
        // 
        this.radioButton2cEjaktuellt.AutoSize = true;
        this.radioButton2cEjaktuellt.Location = new System.Drawing.Point(419, 30);
        this.radioButton2cEjaktuellt.Name = "radioButton2cEjaktuellt";
        this.radioButton2cEjaktuellt.Size = new System.Drawing.Size(82, 17);
        this.radioButton2cEjaktuellt.TabIndex = 22;
        this.radioButton2cEjaktuellt.TabStop = true;
        this.radioButton2cEjaktuellt.Text = "Ej aktuellt";
        this.radioButton2cEjaktuellt.UseVisualStyleBackColor = true;
        this.radioButton2cEjaktuellt.CheckedChanged += new System.EventHandler(this.radioButton2cEjaktuellt_CheckedChanged);
        // 
        // radioButton2cNej
        // 
        this.radioButton2cNej.AutoSize = true;
        this.radioButton2cNej.Location = new System.Drawing.Point(370, 30);
        this.radioButton2cNej.Name = "radioButton2cNej";
        this.radioButton2cNej.Size = new System.Drawing.Size(43, 17);
        this.radioButton2cNej.TabIndex = 21;
        this.radioButton2cNej.TabStop = true;
        this.radioButton2cNej.Text = "Nej";
        this.radioButton2cNej.UseVisualStyleBackColor = true;
        this.radioButton2cNej.CheckedChanged += new System.EventHandler(this.radioButton2cNej_CheckedChanged);
        // 
        // label10
        // 
        this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label10.Location = new System.Drawing.Point(19, 21);
        this.label10.Name = "label10";
        this.label10.Size = new System.Drawing.Size(293, 26);
        this.label10.TabIndex = 0;
        this.label10.Text = "Har kantzoner utformats efter variation i markfuktighet och löv gynnats ?";
        // 
        // radioButton2cJa
        // 
        this.radioButton2cJa.AutoSize = true;
        this.radioButton2cJa.Location = new System.Drawing.Point(326, 30);
        this.radioButton2cJa.Name = "radioButton2cJa";
        this.radioButton2cJa.Size = new System.Drawing.Size(38, 17);
        this.radioButton2cJa.TabIndex = 20;
        this.radioButton2cJa.TabStop = true;
        this.radioButton2cJa.Text = "Ja";
        this.radioButton2cJa.UseVisualStyleBackColor = true;
        this.radioButton2cJa.CheckedChanged += new System.EventHandler(this.radioButton2cJa_CheckedChanged);
        // 
        // groupBox4
        // 
        this.groupBox4.Controls.Add(this.radioButton2bNej);
        this.groupBox4.Controls.Add(this.radioButton2bJa);
        this.groupBox4.Controls.Add(this.label2);
        this.groupBox4.Location = new System.Drawing.Point(8, 83);
        this.groupBox4.Name = "groupBox4";
        this.groupBox4.Size = new System.Drawing.Size(507, 55);
        this.groupBox4.TabIndex = 10;
        this.groupBox4.TabStop = false;
        this.groupBox4.Text = "2b.";
        // 
        // radioButton2bNej
        // 
        this.radioButton2bNej.AutoSize = true;
        this.radioButton2bNej.Location = new System.Drawing.Point(450, 21);
        this.radioButton2bNej.Name = "radioButton2bNej";
        this.radioButton2bNej.Size = new System.Drawing.Size(43, 17);
        this.radioButton2bNej.TabIndex = 19;
        this.radioButton2bNej.TabStop = true;
        this.radioButton2bNej.Text = "Nej";
        this.radioButton2bNej.UseVisualStyleBackColor = true;
        this.radioButton2bNej.CheckedChanged += new System.EventHandler(this.radioButton2bNej_CheckedChanged);
        // 
        // radioButton2bJa
        // 
        this.radioButton2bJa.AutoSize = true;
        this.radioButton2bJa.Location = new System.Drawing.Point(406, 21);
        this.radioButton2bJa.Name = "radioButton2bJa";
        this.radioButton2bJa.Size = new System.Drawing.Size(38, 17);
        this.radioButton2bJa.TabIndex = 18;
        this.radioButton2bJa.TabStop = true;
        this.radioButton2bJa.Text = "Ja";
        this.radioButton2bJa.UseVisualStyleBackColor = true;
        this.radioButton2bJa.CheckedChanged += new System.EventHandler(this.radioButton2bJa_CheckedChanged);
        // 
        // label2
        // 
        this.label2.AutoSize = true;
        this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label2.Location = new System.Drawing.Point(19, 23);
        this.label2.Name = "label2";
        this.label2.Size = new System.Drawing.Size(376, 13);
        this.label2.TabIndex = 0;
        this.label2.Text = " Har hänsynskrävande biotoper hanterats enligt traktdirektivet ?";
        // 
        // groupBox3
        // 
        this.groupBox3.Controls.Add(this.radioButton2aEjaktuellt);
        this.groupBox3.Controls.Add(this.radioButton2aNej);
        this.groupBox3.Controls.Add(this.radioButton2aJa);
        this.groupBox3.Controls.Add(this.label1);
        this.groupBox3.Location = new System.Drawing.Point(8, 35);
        this.groupBox3.Name = "groupBox3";
        this.groupBox3.Size = new System.Drawing.Size(507, 55);
        this.groupBox3.TabIndex = 9;
        this.groupBox3.TabStop = false;
        this.groupBox3.Text = "2a.";
        // 
        // radioButton2aEjaktuellt
        // 
        this.radioButton2aEjaktuellt.AutoSize = true;
        this.radioButton2aEjaktuellt.Location = new System.Drawing.Point(419, 22);
        this.radioButton2aEjaktuellt.Name = "radioButton2aEjaktuellt";
        this.radioButton2aEjaktuellt.Size = new System.Drawing.Size(82, 17);
        this.radioButton2aEjaktuellt.TabIndex = 17;
        this.radioButton2aEjaktuellt.TabStop = true;
        this.radioButton2aEjaktuellt.Text = "Ej aktuellt";
        this.radioButton2aEjaktuellt.UseVisualStyleBackColor = true;
        this.radioButton2aEjaktuellt.CheckedChanged += new System.EventHandler(this.radioButton2aEjaktuellt_CheckedChanged);
        // 
        // radioButton2aNej
        // 
        this.radioButton2aNej.AutoSize = true;
        this.radioButton2aNej.Location = new System.Drawing.Point(370, 22);
        this.radioButton2aNej.Name = "radioButton2aNej";
        this.radioButton2aNej.Size = new System.Drawing.Size(43, 17);
        this.radioButton2aNej.TabIndex = 16;
        this.radioButton2aNej.TabStop = true;
        this.radioButton2aNej.Text = "Nej";
        this.radioButton2aNej.UseVisualStyleBackColor = true;
        this.radioButton2aNej.CheckedChanged += new System.EventHandler(this.radioButton2aNej_CheckedChanged);
        // 
        // radioButton2aJa
        // 
        this.radioButton2aJa.AutoSize = true;
        this.radioButton2aJa.Location = new System.Drawing.Point(326, 22);
        this.radioButton2aJa.Name = "radioButton2aJa";
        this.radioButton2aJa.Size = new System.Drawing.Size(38, 17);
        this.radioButton2aJa.TabIndex = 15;
        this.radioButton2aJa.TabStop = true;
        this.radioButton2aJa.Text = "Ja";
        this.radioButton2aJa.UseVisualStyleBackColor = true;
        this.radioButton2aJa.CheckedChanged += new System.EventHandler(this.radioButton2aJa_CheckedChanged);
        // 
        // label1
        // 
        this.label1.AutoSize = true;
        this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label1.Location = new System.Drawing.Point(19, 24);
        this.label1.Name = "label1";
        this.label1.Size = new System.Drawing.Size(223, 13);
        this.label1.TabIndex = 0;
        this.label1.Text = " Har alla träd på impediment sparats ?";
        // 
        // tabPageFrågor2
        // 
        this.tabPageFrågor2.Controls.Add(this.groupBox18);
        this.tabPageFrågor2.Controls.Add(this.groupBox17);
        this.tabPageFrågor2.Controls.Add(this.groupBox16);
        this.tabPageFrågor2.Controls.Add(this.groupBox15);
        this.tabPageFrågor2.Controls.Add(this.groupBox14);
        this.tabPageFrågor2.Controls.Add(this.groupBox13);
        this.tabPageFrågor2.Controls.Add(this.groupBox12);
        this.tabPageFrågor2.Controls.Add(this.groupBox10);
        this.tabPageFrågor2.Location = new System.Drawing.Point(4, 22);
        this.tabPageFrågor2.Name = "tabPageFrågor2";
        this.tabPageFrågor2.Size = new System.Drawing.Size(551, 468);
        this.tabPageFrågor2.TabIndex = 3;
        this.tabPageFrågor2.Text = "Frågor Sida 2";
        this.tabPageFrågor2.UseVisualStyleBackColor = true;
        // 
        // groupBox18
        // 
        this.groupBox18.Controls.Add(this.radioButton9Nej);
        this.groupBox18.Controls.Add(this.radioButton9Ja);
        this.groupBox18.Controls.Add(this.label23);
        this.groupBox18.Location = new System.Drawing.Point(8, 368);
        this.groupBox18.Name = "groupBox18";
        this.groupBox18.Size = new System.Drawing.Size(507, 55);
        this.groupBox18.TabIndex = 15;
        this.groupBox18.TabStop = false;
        this.groupBox18.Text = "9.";
        // 
        // radioButton9Nej
        // 
        this.radioButton9Nej.AutoSize = true;
        this.radioButton9Nej.Location = new System.Drawing.Point(452, 22);
        this.radioButton9Nej.Name = "radioButton9Nej";
        this.radioButton9Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton9Nej.TabIndex = 31;
        this.radioButton9Nej.TabStop = true;
        this.radioButton9Nej.Text = "Nej";
        this.radioButton9Nej.UseVisualStyleBackColor = true;
        this.radioButton9Nej.CheckedChanged += new System.EventHandler(this.radioButton9Nej_CheckedChanged);
        // 
        // radioButton9Ja
        // 
        this.radioButton9Ja.AutoSize = true;
        this.radioButton9Ja.Location = new System.Drawing.Point(408, 22);
        this.radioButton9Ja.Name = "radioButton9Ja";
        this.radioButton9Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton9Ja.TabIndex = 30;
        this.radioButton9Ja.TabStop = true;
        this.radioButton9Ja.Text = "Ja";
        this.radioButton9Ja.UseVisualStyleBackColor = true;
        this.radioButton9Ja.CheckedChanged += new System.EventHandler(this.radioButton9Ja_CheckedChanged);
        // 
        // label23
        // 
        this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label23.Location = new System.Drawing.Point(19, 22);
        this.label23.Name = "label23";
        this.label23.Size = new System.Drawing.Size(375, 26);
        this.label23.TabIndex = 0;
        this.label23.Text = "Är objektet avstädat och fritt från skräp och oljespill ?";
        // 
        // groupBox17
        // 
        this.groupBox17.Controls.Add(this.radioButton8Ejaktuellt);
        this.groupBox17.Controls.Add(this.radioButton8Nej);
        this.groupBox17.Controls.Add(this.radioButton8Ja);
        this.groupBox17.Controls.Add(this.label22);
        this.groupBox17.Location = new System.Drawing.Point(8, 315);
        this.groupBox17.Name = "groupBox17";
        this.groupBox17.Size = new System.Drawing.Size(507, 55);
        this.groupBox17.TabIndex = 14;
        this.groupBox17.TabStop = false;
        this.groupBox17.Text = "8.";
        // 
        // radioButton8Ejaktuellt
        // 
        this.radioButton8Ejaktuellt.AutoSize = true;
        this.radioButton8Ejaktuellt.Location = new System.Drawing.Point(411, 26);
        this.radioButton8Ejaktuellt.Name = "radioButton8Ejaktuellt";
        this.radioButton8Ejaktuellt.Size = new System.Drawing.Size(82, 17);
        this.radioButton8Ejaktuellt.TabIndex = 23;
        this.radioButton8Ejaktuellt.TabStop = true;
        this.radioButton8Ejaktuellt.Text = "Ej aktuellt";
        this.radioButton8Ejaktuellt.UseVisualStyleBackColor = true;
        this.radioButton8Ejaktuellt.CheckedChanged += new System.EventHandler(this.radioButton8Ejaktuellt_CheckedChanged);
        // 
        // radioButton8Nej
        // 
        this.radioButton8Nej.AutoSize = true;
        this.radioButton8Nej.Location = new System.Drawing.Point(362, 26);
        this.radioButton8Nej.Name = "radioButton8Nej";
        this.radioButton8Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton8Nej.TabIndex = 22;
        this.radioButton8Nej.TabStop = true;
        this.radioButton8Nej.Text = "Nej";
        this.radioButton8Nej.UseVisualStyleBackColor = true;
        this.radioButton8Nej.CheckedChanged += new System.EventHandler(this.radioButton8Nej_CheckedChanged);
        // 
        // radioButton8Ja
        // 
        this.radioButton8Ja.AutoSize = true;
        this.radioButton8Ja.Location = new System.Drawing.Point(318, 26);
        this.radioButton8Ja.Name = "radioButton8Ja";
        this.radioButton8Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton8Ja.TabIndex = 21;
        this.radioButton8Ja.TabStop = true;
        this.radioButton8Ja.Text = "Ja";
        this.radioButton8Ja.UseVisualStyleBackColor = true;
        this.radioButton8Ja.CheckedChanged += new System.EventHandler(this.radioButton8Ja_CheckedChanged);
        // 
        // label22
        // 
        this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label22.Location = new System.Drawing.Point(19, 22);
        this.label22.Name = "label22";
        this.label22.Size = new System.Drawing.Size(285, 26);
        this.label22.TabIndex = 0;
        this.label22.Text = "Har man på trakten vidtagit åtgärder för att minska körskador? Risat/markskonare";
        // 
        // groupBox16
        // 
        this.groupBox16.Controls.Add(this.radioButton7Ejaktuellt);
        this.groupBox16.Controls.Add(this.radioButton7Nej);
        this.groupBox16.Controls.Add(this.radioButton7Ja);
        this.groupBox16.Controls.Add(this.label21);
        this.groupBox16.Location = new System.Drawing.Point(8, 270);
        this.groupBox16.Name = "groupBox16";
        this.groupBox16.Size = new System.Drawing.Size(507, 55);
        this.groupBox16.TabIndex = 13;
        this.groupBox16.TabStop = false;
        this.groupBox16.Text = "7.";
        // 
        // radioButton7Ejaktuellt
        // 
        this.radioButton7Ejaktuellt.AutoSize = true;
        this.radioButton7Ejaktuellt.Location = new System.Drawing.Point(413, 20);
        this.radioButton7Ejaktuellt.Name = "radioButton7Ejaktuellt";
        this.radioButton7Ejaktuellt.Size = new System.Drawing.Size(82, 17);
        this.radioButton7Ejaktuellt.TabIndex = 30;
        this.radioButton7Ejaktuellt.TabStop = true;
        this.radioButton7Ejaktuellt.Text = "Ej aktuellt";
        this.radioButton7Ejaktuellt.UseVisualStyleBackColor = true;
        this.radioButton7Ejaktuellt.CheckedChanged += new System.EventHandler(this.radioButton7Ejaktuellt_CheckedChanged);
        // 
        // radioButton7Nej
        // 
        this.radioButton7Nej.AutoSize = true;
        this.radioButton7Nej.Location = new System.Drawing.Point(364, 20);
        this.radioButton7Nej.Name = "radioButton7Nej";
        this.radioButton7Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton7Nej.TabIndex = 29;
        this.radioButton7Nej.TabStop = true;
        this.radioButton7Nej.Text = "Nej";
        this.radioButton7Nej.UseVisualStyleBackColor = true;
        this.radioButton7Nej.CheckedChanged += new System.EventHandler(this.radioButton7Nej_CheckedChanged);
        // 
        // radioButton7Ja
        // 
        this.radioButton7Ja.AutoSize = true;
        this.radioButton7Ja.Location = new System.Drawing.Point(320, 20);
        this.radioButton7Ja.Name = "radioButton7Ja";
        this.radioButton7Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton7Ja.TabIndex = 28;
        this.radioButton7Ja.TabStop = true;
        this.radioButton7Ja.Text = "Ja";
        this.radioButton7Ja.UseVisualStyleBackColor = true;
        this.radioButton7Ja.CheckedChanged += new System.EventHandler(this.radioButton7Ja_CheckedChanged);
        // 
        // label21
        // 
        this.label21.AutoSize = true;
        this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label21.Location = new System.Drawing.Point(19, 22);
        this.label21.Name = "label21";
        this.label21.Size = new System.Drawing.Size(285, 13);
        this.label21.TabIndex = 0;
        this.label21.Text = "Är stigar, spår och leder risfria och framkomliga ?";
        // 
        // groupBox15
        // 
        this.groupBox15.Controls.Add(this.radioButton6Ejaktuellt);
        this.groupBox15.Controls.Add(this.radioButton6Nej);
        this.groupBox15.Controls.Add(this.radioButton6Ja);
        this.groupBox15.Controls.Add(this.label20);
        this.groupBox15.Location = new System.Drawing.Point(8, 203);
        this.groupBox15.Name = "groupBox15";
        this.groupBox15.Size = new System.Drawing.Size(507, 72);
        this.groupBox15.TabIndex = 12;
        this.groupBox15.TabStop = false;
        this.groupBox15.Text = "6.";
        // 
        // radioButton6Ejaktuellt
        // 
        this.radioButton6Ejaktuellt.AutoSize = true;
        this.radioButton6Ejaktuellt.Location = new System.Drawing.Point(413, 49);
        this.radioButton6Ejaktuellt.Name = "radioButton6Ejaktuellt";
        this.radioButton6Ejaktuellt.Size = new System.Drawing.Size(82, 17);
        this.radioButton6Ejaktuellt.TabIndex = 28;
        this.radioButton6Ejaktuellt.TabStop = true;
        this.radioButton6Ejaktuellt.Text = "Ej aktuellt";
        this.radioButton6Ejaktuellt.UseVisualStyleBackColor = true;
        this.radioButton6Ejaktuellt.CheckedChanged += new System.EventHandler(this.radioButton6Ejaktuellt_CheckedChanged);
        // 
        // radioButton6Nej
        // 
        this.radioButton6Nej.AutoSize = true;
        this.radioButton6Nej.Location = new System.Drawing.Point(364, 49);
        this.radioButton6Nej.Name = "radioButton6Nej";
        this.radioButton6Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton6Nej.TabIndex = 27;
        this.radioButton6Nej.TabStop = true;
        this.radioButton6Nej.Text = "Nej";
        this.radioButton6Nej.UseVisualStyleBackColor = true;
        this.radioButton6Nej.CheckedChanged += new System.EventHandler(this.radioButton6Nej_CheckedChanged);
        // 
        // radioButton6Ja
        // 
        this.radioButton6Ja.AutoSize = true;
        this.radioButton6Ja.Location = new System.Drawing.Point(320, 49);
        this.radioButton6Ja.Name = "radioButton6Ja";
        this.radioButton6Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton6Ja.TabIndex = 26;
        this.radioButton6Ja.TabStop = true;
        this.radioButton6Ja.Text = "Ja";
        this.radioButton6Ja.UseVisualStyleBackColor = true;
        this.radioButton6Ja.CheckedChanged += new System.EventHandler(this.radioButton6Ja_CheckedChanged);
        // 
        // label20
        // 
        this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label20.Location = new System.Drawing.Point(19, 22);
        this.label20.Name = "label20";
        this.label20.Size = new System.Drawing.Size(293, 47);
        this.label20.TabIndex = 0;
        this.label20.Text = "Finns tillfällig bro/virke kvar för passage av vattendrag ? (notera i klartextrut" +
            "a om kran behövs för att nyttja detta)";
        // 
        // groupBox14
        // 
        this.groupBox14.Controls.Add(this.radioButton5Ejaktuellt);
        this.groupBox14.Controls.Add(this.radioButton5Nej);
        this.groupBox14.Controls.Add(this.radioButton5Ja);
        this.groupBox14.Controls.Add(this.label19);
        this.groupBox14.Location = new System.Drawing.Point(8, 157);
        this.groupBox14.Name = "groupBox14";
        this.groupBox14.Size = new System.Drawing.Size(507, 55);
        this.groupBox14.TabIndex = 11;
        this.groupBox14.TabStop = false;
        this.groupBox14.Text = "5.";
        // 
        // radioButton5Ejaktuellt
        // 
        this.radioButton5Ejaktuellt.AutoSize = true;
        this.radioButton5Ejaktuellt.Location = new System.Drawing.Point(413, 20);
        this.radioButton5Ejaktuellt.Name = "radioButton5Ejaktuellt";
        this.radioButton5Ejaktuellt.Size = new System.Drawing.Size(82, 17);
        this.radioButton5Ejaktuellt.TabIndex = 26;
        this.radioButton5Ejaktuellt.TabStop = true;
        this.radioButton5Ejaktuellt.Text = "Ej aktuellt";
        this.radioButton5Ejaktuellt.UseVisualStyleBackColor = true;
        this.radioButton5Ejaktuellt.CheckedChanged += new System.EventHandler(this.radioButton5Ejaktuellt_CheckedChanged);
        // 
        // radioButton5Nej
        // 
        this.radioButton5Nej.AutoSize = true;
        this.radioButton5Nej.Location = new System.Drawing.Point(364, 20);
        this.radioButton5Nej.Name = "radioButton5Nej";
        this.radioButton5Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton5Nej.TabIndex = 25;
        this.radioButton5Nej.TabStop = true;
        this.radioButton5Nej.Text = "Nej";
        this.radioButton5Nej.UseVisualStyleBackColor = true;
        this.radioButton5Nej.CheckedChanged += new System.EventHandler(this.radioButton5Nej_CheckedChanged);
        // 
        // radioButton5Ja
        // 
        this.radioButton5Ja.AutoSize = true;
        this.radioButton5Ja.Location = new System.Drawing.Point(320, 20);
        this.radioButton5Ja.Name = "radioButton5Ja";
        this.radioButton5Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton5Ja.TabIndex = 24;
        this.radioButton5Ja.TabStop = true;
        this.radioButton5Ja.Text = "Ja";
        this.radioButton5Ja.UseVisualStyleBackColor = true;
        this.radioButton5Ja.CheckedChanged += new System.EventHandler(this.radioButton5Ja_CheckedChanged);
        // 
        // label19
        // 
        this.label19.AutoSize = true;
        this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label19.Location = new System.Drawing.Point(19, 22);
        this.label19.Name = "label19";
        this.label19.Size = new System.Drawing.Size(224, 13);
        this.label19.TabIndex = 0;
        this.label19.Text = "Är diken rensade från virke/material ?";
        // 
        // groupBox13
        // 
        this.groupBox13.Controls.Add(this.radioButton4bNej);
        this.groupBox13.Controls.Add(this.radioButton4bJa);
        this.groupBox13.Controls.Add(this.label18);
        this.groupBox13.Location = new System.Drawing.Point(8, 110);
        this.groupBox13.Name = "groupBox13";
        this.groupBox13.Size = new System.Drawing.Size(507, 55);
        this.groupBox13.TabIndex = 10;
        this.groupBox13.TabStop = false;
        this.groupBox13.Text = "4b.";
        // 
        // radioButton4bNej
        // 
        this.radioButton4bNej.AutoSize = true;
        this.radioButton4bNej.Location = new System.Drawing.Point(450, 20);
        this.radioButton4bNej.Name = "radioButton4bNej";
        this.radioButton4bNej.Size = new System.Drawing.Size(43, 17);
        this.radioButton4bNej.TabIndex = 23;
        this.radioButton4bNej.TabStop = true;
        this.radioButton4bNej.Text = "Nej";
        this.radioButton4bNej.UseVisualStyleBackColor = true;
        this.radioButton4bNej.CheckedChanged += new System.EventHandler(this.radioButton4bNej_CheckedChanged);
        // 
        // radioButton4bJa
        // 
        this.radioButton4bJa.AutoSize = true;
        this.radioButton4bJa.Location = new System.Drawing.Point(406, 20);
        this.radioButton4bJa.Name = "radioButton4bJa";
        this.radioButton4bJa.Size = new System.Drawing.Size(38, 17);
        this.radioButton4bJa.TabIndex = 22;
        this.radioButton4bJa.TabStop = true;
        this.radioButton4bJa.Text = "Ja";
        this.radioButton4bJa.UseVisualStyleBackColor = true;
        this.radioButton4bJa.CheckedChanged += new System.EventHandler(this.radioButton4bJa_CheckedChanged);
        // 
        // label18
        // 
        this.label18.AutoSize = true;
        this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label18.Location = new System.Drawing.Point(19, 22);
        this.label18.Name = "label18";
        this.label18.Size = new System.Drawing.Size(263, 13);
        this.label18.TabIndex = 0;
        this.label18.Text = "Finns körskador på bilväg som bör åtgärdas ?";
        // 
        // groupBox12
        // 
        this.groupBox12.Controls.Add(this.radioButton4aNej);
        this.groupBox12.Controls.Add(this.radioButton4aJa);
        this.groupBox12.Controls.Add(this.label17);
        this.groupBox12.Location = new System.Drawing.Point(8, 62);
        this.groupBox12.Name = "groupBox12";
        this.groupBox12.Size = new System.Drawing.Size(507, 55);
        this.groupBox12.TabIndex = 9;
        this.groupBox12.TabStop = false;
        this.groupBox12.Text = "4a.";
        // 
        // radioButton4aNej
        // 
        this.radioButton4aNej.AutoSize = true;
        this.radioButton4aNej.Location = new System.Drawing.Point(450, 22);
        this.radioButton4aNej.Name = "radioButton4aNej";
        this.radioButton4aNej.Size = new System.Drawing.Size(43, 17);
        this.radioButton4aNej.TabIndex = 21;
        this.radioButton4aNej.TabStop = true;
        this.radioButton4aNej.Text = "Nej";
        this.radioButton4aNej.UseVisualStyleBackColor = true;
        this.radioButton4aNej.CheckedChanged += new System.EventHandler(this.radioButton4aNej_CheckedChanged);
        // 
        // radioButton4aJa
        // 
        this.radioButton4aJa.AutoSize = true;
        this.radioButton4aJa.Location = new System.Drawing.Point(406, 22);
        this.radioButton4aJa.Name = "radioButton4aJa";
        this.radioButton4aJa.Size = new System.Drawing.Size(38, 17);
        this.radioButton4aJa.TabIndex = 20;
        this.radioButton4aJa.TabStop = true;
        this.radioButton4aJa.Text = "Ja";
        this.radioButton4aJa.UseVisualStyleBackColor = true;
        this.radioButton4aJa.CheckedChanged += new System.EventHandler(this.radioButton4aJa_CheckedChanged);
        // 
        // label17
        // 
        this.label17.AutoSize = true;
        this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label17.Location = new System.Drawing.Point(19, 24);
        this.label17.Name = "label17";
        this.label17.Size = new System.Drawing.Size(261, 13);
        this.label17.TabIndex = 0;
        this.label17.Text = "Finns körskador i terräng som bör åtgärdas ?";
        // 
        // groupBox10
        // 
        this.groupBox10.Controls.Add(this.radioButton3Ejaktuellt);
        this.groupBox10.Controls.Add(this.radioButton3Nej);
        this.groupBox10.Controls.Add(this.radioButton3Ja);
        this.groupBox10.Controls.Add(this.label16);
        this.groupBox10.Location = new System.Drawing.Point(8, 13);
        this.groupBox10.Name = "groupBox10";
        this.groupBox10.Size = new System.Drawing.Size(507, 55);
        this.groupBox10.TabIndex = 8;
        this.groupBox10.TabStop = false;
        this.groupBox10.Text = "3.";
        // 
        // radioButton3Ejaktuellt
        // 
        this.radioButton3Ejaktuellt.AutoSize = true;
        this.radioButton3Ejaktuellt.Location = new System.Drawing.Point(413, 24);
        this.radioButton3Ejaktuellt.Name = "radioButton3Ejaktuellt";
        this.radioButton3Ejaktuellt.Size = new System.Drawing.Size(82, 17);
        this.radioButton3Ejaktuellt.TabIndex = 20;
        this.radioButton3Ejaktuellt.TabStop = true;
        this.radioButton3Ejaktuellt.Text = "Ej aktuellt";
        this.radioButton3Ejaktuellt.UseVisualStyleBackColor = true;
        this.radioButton3Ejaktuellt.CheckedChanged += new System.EventHandler(this.radioButton3Ejaktuellt_CheckedChanged);
        // 
        // radioButton3Nej
        // 
        this.radioButton3Nej.AutoSize = true;
        this.radioButton3Nej.Location = new System.Drawing.Point(364, 24);
        this.radioButton3Nej.Name = "radioButton3Nej";
        this.radioButton3Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton3Nej.TabIndex = 19;
        this.radioButton3Nej.TabStop = true;
        this.radioButton3Nej.Text = "Nej";
        this.radioButton3Nej.UseVisualStyleBackColor = true;
        this.radioButton3Nej.CheckedChanged += new System.EventHandler(this.radioButton3Nej_CheckedChanged);
        // 
        // radioButton3Ja
        // 
        this.radioButton3Ja.AutoSize = true;
        this.radioButton3Ja.Location = new System.Drawing.Point(320, 24);
        this.radioButton3Ja.Name = "radioButton3Ja";
        this.radioButton3Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton3Ja.TabIndex = 18;
        this.radioButton3Ja.TabStop = true;
        this.radioButton3Ja.Text = "Ja";
        this.radioButton3Ja.UseVisualStyleBackColor = true;
        this.radioButton3Ja.CheckedChanged += new System.EventHandler(this.radioButton3Ja_CheckedChanged);
        // 
        // label16
        // 
        this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label16.Location = new System.Drawing.Point(19, 17);
        this.label16.Name = "label16";
        this.label16.Size = new System.Drawing.Size(285, 26);
        this.label16.TabIndex = 0;
        this.label16.Text = " Har kulturmiljöer framhävts/ej skadats genom avverkningen ?";
        // 
        // tabPageFrågor3
        // 
        this.tabPageFrågor3.Controls.Add(this.groupBox19);
        this.tabPageFrågor3.Controls.Add(this.groupBox21);
        this.tabPageFrågor3.Controls.Add(this.groupBox20);
        this.tabPageFrågor3.Controls.Add(this.groupBox11);
        this.tabPageFrågor3.Location = new System.Drawing.Point(4, 22);
        this.tabPageFrågor3.Name = "tabPageFrågor3";
        this.tabPageFrågor3.Size = new System.Drawing.Size(551, 468);
        this.tabPageFrågor3.TabIndex = 4;
        this.tabPageFrågor3.Text = "Frågor Sida 3";
        this.tabPageFrågor3.UseVisualStyleBackColor = true;
        // 
        // groupBox19
        // 
        this.groupBox19.Controls.Add(this.radioButton12Nej);
        this.groupBox19.Controls.Add(this.radioButton12Ja);
        this.groupBox19.Controls.Add(this.label24);
        this.groupBox19.Location = new System.Drawing.Point(8, 116);
        this.groupBox19.Name = "groupBox19";
        this.groupBox19.Size = new System.Drawing.Size(507, 55);
        this.groupBox19.TabIndex = 2;
        this.groupBox19.TabStop = false;
        this.groupBox19.Text = "12.";
        // 
        // radioButton12Nej
        // 
        this.radioButton12Nej.AutoSize = true;
        this.radioButton12Nej.Location = new System.Drawing.Point(448, 20);
        this.radioButton12Nej.Name = "radioButton12Nej";
        this.radioButton12Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton12Nej.TabIndex = 2;
        this.radioButton12Nej.TabStop = true;
        this.radioButton12Nej.Text = "Nej";
        this.radioButton12Nej.UseVisualStyleBackColor = true;
        this.radioButton12Nej.CheckedChanged += new System.EventHandler(this.radioButton12Nej_CheckedChanged);
        // 
        // radioButton12Ja
        // 
        this.radioButton12Ja.AutoSize = true;
        this.radioButton12Ja.Location = new System.Drawing.Point(404, 20);
        this.radioButton12Ja.Name = "radioButton12Ja";
        this.radioButton12Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton12Ja.TabIndex = 1;
        this.radioButton12Ja.TabStop = true;
        this.radioButton12Ja.Text = "Ja";
        this.radioButton12Ja.UseVisualStyleBackColor = true;
        this.radioButton12Ja.CheckedChanged += new System.EventHandler(this.radioButton12Ja_CheckedChanged);
        // 
        // label24
        // 
        this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label24.Location = new System.Drawing.Point(19, 22);
        this.label24.Name = "label24";
        this.label24.Size = new System.Drawing.Size(375, 26);
        this.label24.TabIndex = 0;
        this.label24.Text = "Är  körspår inrapporterade samt trakten rapporterad avslutad ?";
        // 
        // groupBox21
        // 
        this.groupBox21.Controls.Add(this.radioButton11Nej);
        this.groupBox21.Controls.Add(this.radioButton11Ja);
        this.groupBox21.Controls.Add(this.label26);
        this.groupBox21.Location = new System.Drawing.Point(8, 65);
        this.groupBox21.Name = "groupBox21";
        this.groupBox21.Size = new System.Drawing.Size(507, 55);
        this.groupBox21.TabIndex = 1;
        this.groupBox21.TabStop = false;
        this.groupBox21.Text = "11.";
        // 
        // radioButton11Nej
        // 
        this.radioButton11Nej.AutoSize = true;
        this.radioButton11Nej.Location = new System.Drawing.Point(448, 22);
        this.radioButton11Nej.Name = "radioButton11Nej";
        this.radioButton11Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton11Nej.TabIndex = 2;
        this.radioButton11Nej.TabStop = true;
        this.radioButton11Nej.Text = "Nej";
        this.radioButton11Nej.UseVisualStyleBackColor = true;
        this.radioButton11Nej.CheckedChanged += new System.EventHandler(this.radioButton11Nej_CheckedChanged);
        // 
        // radioButton11Ja
        // 
        this.radioButton11Ja.AutoSize = true;
        this.radioButton11Ja.Location = new System.Drawing.Point(404, 22);
        this.radioButton11Ja.Name = "radioButton11Ja";
        this.radioButton11Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton11Ja.TabIndex = 1;
        this.radioButton11Ja.TabStop = true;
        this.radioButton11Ja.Text = "Ja";
        this.radioButton11Ja.UseVisualStyleBackColor = true;
        this.radioButton11Ja.CheckedChanged += new System.EventHandler(this.radioButton11Ja_CheckedChanged);
        // 
        // label26
        // 
        this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label26.Location = new System.Drawing.Point(19, 22);
        this.label26.Name = "label26";
        this.label26.Size = new System.Drawing.Size(375, 26);
        this.label26.TabIndex = 0;
        this.label26.Text = "Volymer och kontrollstamsfiler inrapporterade ?";
        // 
        // groupBox20
        // 
        this.groupBox20.Controls.Add(this.radioButton10Nej);
        this.groupBox20.Controls.Add(this.radioButton10Ja);
        this.groupBox20.Controls.Add(this.label25);
        this.groupBox20.Location = new System.Drawing.Point(8, 13);
        this.groupBox20.Name = "groupBox20";
        this.groupBox20.Size = new System.Drawing.Size(507, 55);
        this.groupBox20.TabIndex = 0;
        this.groupBox20.TabStop = false;
        this.groupBox20.Text = "10.";
        // 
        // radioButton10Nej
        // 
        this.radioButton10Nej.AutoSize = true;
        this.radioButton10Nej.Location = new System.Drawing.Point(448, 22);
        this.radioButton10Nej.Name = "radioButton10Nej";
        this.radioButton10Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton10Nej.TabIndex = 2;
        this.radioButton10Nej.TabStop = true;
        this.radioButton10Nej.Text = "Nej";
        this.radioButton10Nej.UseVisualStyleBackColor = true;
        this.radioButton10Nej.CheckedChanged += new System.EventHandler(this.radioButton10Nej_CheckedChanged);
        // 
        // radioButton10Ja
        // 
        this.radioButton10Ja.AutoSize = true;
        this.radioButton10Ja.Location = new System.Drawing.Point(404, 22);
        this.radioButton10Ja.Name = "radioButton10Ja";
        this.radioButton10Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton10Ja.TabIndex = 1;
        this.radioButton10Ja.TabStop = true;
        this.radioButton10Ja.Text = "Ja";
        this.radioButton10Ja.UseVisualStyleBackColor = true;
        this.radioButton10Ja.CheckedChanged += new System.EventHandler(this.radioButton10Ja_CheckedChanged);
        // 
        // label25
        // 
        this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label25.Location = new System.Drawing.Point(19, 22);
        this.label25.Name = "label25";
        this.label25.Size = new System.Drawing.Size(375, 26);
        this.label25.TabIndex = 0;
        this.label25.Text = "Är allt virke utkört till avlägg och uppmärkt  ?";
        // 
        // groupBox11
        // 
        this.groupBox11.Controls.Add(this.richTextBoxOvrigt);
        this.groupBox11.Location = new System.Drawing.Point(8, 358);
        this.groupBox11.Name = "groupBox11";
        this.groupBox11.Size = new System.Drawing.Size(507, 100);
        this.groupBox11.TabIndex = 3;
        this.groupBox11.TabStop = false;
        this.groupBox11.Text = "KOMMENTAR";
        // 
        // richTextBoxOvrigt
        // 
        this.richTextBoxOvrigt.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetSlutavverkning, "Fragor.Ovrigt", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.richTextBoxOvrigt.Location = new System.Drawing.Point(13, 20);
        this.richTextBoxOvrigt.MaxLength = 4095;
        this.richTextBoxOvrigt.Name = "richTextBoxOvrigt";
        this.richTextBoxOvrigt.Size = new System.Drawing.Size(480, 66);
        this.richTextBoxOvrigt.TabIndex = 0;
        this.richTextBoxOvrigt.Text = "";
        this.richTextBoxOvrigt.TextChanged += new System.EventHandler(this.richTextBoxOvrigt_TextChanged);
        // 
        // environmentSlutavverkning
        // 
        designerSettings1.ApplicationConnection = null;
        designerSettings1.DefaultFont = new System.Drawing.Font("Arial", 10F);
        designerSettings1.Icon = ((System.Drawing.Icon)(resources.GetObject("designerSettings1.Icon")));
        designerSettings1.Restrictions = designerRestrictions1;
        designerSettings1.Text = "";
        this.environmentSlutavverkning.DesignerSettings = designerSettings1;
        emailSettings1.Address = "";
        emailSettings1.Host = "";
        emailSettings1.MessageTemplate = "";
        emailSettings1.Name = "";
        emailSettings1.Password = "";
        emailSettings1.Port = 49;
        emailSettings1.UserName = "";
        this.environmentSlutavverkning.EmailSettings = emailSettings1;
        previewSettings1.Buttons = ((FastReport.PreviewButtons)((((((FastReport.PreviewButtons.Print | FastReport.PreviewButtons.Save)
                    | FastReport.PreviewButtons.Find)
                    | FastReport.PreviewButtons.Zoom)
                    | FastReport.PreviewButtons.Navigator)
                    | FastReport.PreviewButtons.Close)));
        previewSettings1.Icon = ((System.Drawing.Icon)(resources.GetObject("previewSettings1.Icon")));
        previewSettings1.Text = "";
        this.environmentSlutavverkning.PreviewSettings = previewSettings1;
        this.environmentSlutavverkning.ReportSettings = reportSettings1;
        this.environmentSlutavverkning.UIStyle = FastReport.Utils.UIStyle.Office2007Black;
        // 
        // progressBarData
        // 
        this.progressBarData.ForeColor = System.Drawing.SystemColors.Desktop;
        this.progressBarData.Location = new System.Drawing.Point(6, 541);
        this.progressBarData.Maximum = 28;
        this.progressBarData.Name = "progressBarData";
        this.progressBarData.Size = new System.Drawing.Size(541, 20);
        this.progressBarData.Step = 1;
        this.progressBarData.TabIndex = 2;
        // 
        // labelProgress
        // 
        this.labelProgress.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelProgress.Location = new System.Drawing.Point(477, 524);
        this.labelProgress.Name = "labelProgress";
        this.labelProgress.Size = new System.Drawing.Size(68, 13);
        this.labelProgress.TabIndex = 3;
        this.labelProgress.Text = "0% Klar";
        this.labelProgress.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
        // 
        // labelFragaObs
        // 
        this.labelFragaObs.AutoSize = true;
        this.labelFragaObs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelFragaObs.Location = new System.Drawing.Point(24, 38);
        this.labelFragaObs.Name = "labelFragaObs";
        this.labelFragaObs.Size = new System.Drawing.Size(458, 13);
        this.labelFragaObs.TabIndex = 31;
        this.labelFragaObs.Text = "Ifall något ”Nej” har valts på någon av frågorna så skall en miljörapport skrivas" +
            ".";
        // 
        // labelOBS
        // 
        this.labelOBS.AutoSize = true;
        this.labelOBS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelOBS.Location = new System.Drawing.Point(19, 17);
        this.labelOBS.Name = "labelOBS";
        this.labelOBS.Size = new System.Drawing.Size(36, 13);
        this.labelOBS.TabIndex = 32;
        this.labelOBS.Text = "OBS!";
        // 
        // reportSlutavverkning
        // 
        this.reportSlutavverkning.ReportResourceString = resources.GetString("reportSlutavverkning.ReportResourceString");
        this.reportSlutavverkning.RegisterData(this.dataSetSlutavverkning, "dataSetSlutavverkning");
        // 
        // textBoxRegion
        // 
        this.textBoxRegion.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetSlutavverkning, "UserData.Region", true));
        this.textBoxRegion.Location = new System.Drawing.Point(15, 30);
        this.textBoxRegion.MaxLength = 6;
        this.textBoxRegion.Name = "textBoxRegion";
        this.textBoxRegion.ReadOnly = true;
        this.textBoxRegion.Size = new System.Drawing.Size(116, 21);
        this.textBoxRegion.TabIndex = 8;
        this.textBoxRegion.Visible = false;
        // 
        // textBoxDistrikt
        // 
        this.textBoxDistrikt.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetSlutavverkning, "UserData.Distrikt", true));
        this.textBoxDistrikt.Location = new System.Drawing.Point(140, 30);
        this.textBoxDistrikt.MaxLength = 6;
        this.textBoxDistrikt.Name = "textBoxDistrikt";
        this.textBoxDistrikt.ReadOnly = true;
        this.textBoxDistrikt.Size = new System.Drawing.Size(116, 21);
        this.textBoxDistrikt.TabIndex = 9;
        this.textBoxDistrikt.Visible = false;
        // 
        // SlutavverkningForm
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.AutoScroll = true;
        this.AutoSize = true;
        this.ClientSize = new System.Drawing.Size(563, 573);
        this.Controls.Add(this.labelProgress);
        this.Controls.Add(this.progressBarData);
        this.Controls.Add(this.tabControl);
        this.Controls.Add(this.menuStripSlutavverkning);
        this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
        this.MainMenuStrip = this.menuStripSlutavverkning;
        this.MaximizeBox = false;
        this.MaximumSize = new System.Drawing.Size(620, 640);
        this.MinimizeBox = false;
        this.Name = "SlutavverkningForm";
        this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        this.Text = "KORUS Slutavverkning";
        this.Load += new System.EventHandler(this.Slutavverkning_Load);
        this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Slutavverkning_FormClosing);
        ((System.ComponentModel.ISupportInitialize)(this.dataSetSlutavverkning)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableUserData)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFragor)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetSettings)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableRegion)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableDistrikt)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableStandort)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableMarkberedningsmetod)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableUrsprung)).EndInit();
        this.menuStripSlutavverkning.ResumeLayout(false);
        this.menuStripSlutavverkning.PerformLayout();
        this.tabControl.ResumeLayout(false);
        this.tabPageSlutavverkning.ResumeLayout(false);
        this.groupBoxSlutavverkning.ResumeLayout(false);
        this.groupBoxSlutavverkning.PerformLayout();
        this.groupBoxMinstEnlSVL.ResumeLayout(false);
        this.groupBoxMinstEnlSVL.PerformLayout();
        this.groupBoxOBS.ResumeLayout(false);
        this.groupBoxOBS.PerformLayout();
        this.groupBox1.ResumeLayout(false);
        this.groupBox1.PerformLayout();
        this.groupBox22.ResumeLayout(false);
        this.groupBox22.PerformLayout();
        this.groupBoxTrakt.ResumeLayout(false);
        this.groupBoxTrakt.PerformLayout();
        this.groupBoxRegion.ResumeLayout(false);
        this.groupBoxRegion.PerformLayout();
        this.tabPageFrågor1.ResumeLayout(false);
        this.tabPageFrågor1.PerformLayout();
        this.groupBox9.ResumeLayout(false);
        this.groupBox9.PerformLayout();
        this.groupBox8.ResumeLayout(false);
        this.groupBox8.PerformLayout();
        this.groupBox7.ResumeLayout(false);
        this.groupBox7.PerformLayout();
        this.groupBox6.ResumeLayout(false);
        this.groupBox6.PerformLayout();
        this.groupBox5.ResumeLayout(false);
        this.groupBox5.PerformLayout();
        this.groupBox2.ResumeLayout(false);
        this.groupBox2.PerformLayout();
        this.groupBox4.ResumeLayout(false);
        this.groupBox4.PerformLayout();
        this.groupBox3.ResumeLayout(false);
        this.groupBox3.PerformLayout();
        this.tabPageFrågor2.ResumeLayout(false);
        this.groupBox18.ResumeLayout(false);
        this.groupBox18.PerformLayout();
        this.groupBox17.ResumeLayout(false);
        this.groupBox17.PerformLayout();
        this.groupBox16.ResumeLayout(false);
        this.groupBox16.PerformLayout();
        this.groupBox15.ResumeLayout(false);
        this.groupBox15.PerformLayout();
        this.groupBox14.ResumeLayout(false);
        this.groupBox14.PerformLayout();
        this.groupBox13.ResumeLayout(false);
        this.groupBox13.PerformLayout();
        this.groupBox12.ResumeLayout(false);
        this.groupBox12.PerformLayout();
        this.groupBox10.ResumeLayout(false);
        this.groupBox10.PerformLayout();
        this.tabPageFrågor3.ResumeLayout(false);
        this.groupBox19.ResumeLayout(false);
        this.groupBox19.PerformLayout();
        this.groupBox21.ResumeLayout(false);
        this.groupBox21.PerformLayout();
        this.groupBox20.ResumeLayout(false);
        this.groupBox20.PerformLayout();
        this.groupBox11.ResumeLayout(false);
        ((System.ComponentModel.ISupportInitialize)(this.reportSlutavverkning)).EndInit();
        this.ResumeLayout(false);
        this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.MenuStrip menuStripSlutavverkning;
    private System.Windows.Forms.ToolStripMenuItem arkivToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem sparaToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem sparaSomToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    private System.Windows.Forms.ToolStripMenuItem avslutaToolStripMenuItem;
    private System.Data.DataSet dataSetSlutavverkning;
    private System.Data.DataTable dataTableUserData;
    private System.Windows.Forms.OpenFileDialog openReportFileDialog;
    private System.Windows.Forms.TabControl tabControl;
    private System.Windows.Forms.TabPage tabPageFrågor1;
    private System.Data.DataTable dataTableFragor;
    private System.Data.DataColumn dataColumnFraga1;
    private System.Data.DataColumn dataColumnFraga2;
    private System.Data.DataSet dataSetSettings;
    private System.Data.DataTable dataTableRegion;
    private System.Data.DataTable dataTableDistrikt;
    private System.Data.DataTable dataTableStandort;
    private System.Data.DataColumn dataColumnSettingsStandortNamn;
    private FastReport.EnvironmentSettings environmentSlutavverkning;
    private System.Windows.Forms.ProgressBar progressBarData;
    private System.Windows.Forms.Label labelProgress;
    private System.Data.DataColumn dataColumnSettingsRegionId;
    private System.Data.DataColumn dataColumnSettingsDistriktRegionId;
    private System.Data.DataColumn dataColumnSettingsRegionNamn;
    private System.Data.DataColumn dataColumnSettingsDistriktId;
    private System.Data.DataColumn dataColumnSettingsDistriktNamn;
    private System.Data.DataTable dataTableMarkberedningsmetod;
    private System.Data.DataColumn dataColumnSettingsMarkberedningsmetodNamn;
    private System.Data.DataTable dataTableUrsprung;
    private System.Data.DataColumn dataColumnSettingsUrsprungNamn;
    private System.Data.DataColumn dataColumnRegionId;
    private System.Data.DataColumn dataColumnRegion;
    private System.Data.DataColumn dataColumnDistriktId;
    private System.Data.DataColumn dataColumnDistrikt;
    private System.Data.DataColumn dataColumnUrsprung;
    private System.Data.DataColumn dataColumnDatum;
    private System.Data.DataColumn dataColumnTraktnr;
    private System.Data.DataColumn dataColumnTraktnamn;
    private System.Data.DataColumn dataColumnStandort;
    private System.Data.DataColumn dataColumnEntreprenor;
    private System.Data.DataColumn dataColumnLanguage;
    private System.Data.DataColumn dataColumnMailFrom;
    private System.Data.DataColumn dataColumnStatus;
    private System.Data.DataColumn dataColumnStatus_Datum;
    private System.Data.DataColumn dataColumnÅrtal;
    private System.Windows.Forms.FolderBrowserDialog saveReportFolderBrowserDialog;
    private System.Windows.Forms.TabPage tabPageSlutavverkning;
    private System.Windows.Forms.GroupBox groupBoxRegion;
    private System.Windows.Forms.ComboBox comboBoxUrsprung;
    private System.Windows.Forms.DateTimePicker dateTimePicker;
    private System.Windows.Forms.ComboBox comboBoxDistrikt;
    private System.Windows.Forms.ComboBox comboBoxRegion;
    private System.Windows.Forms.Label labelDatum;
    private System.Windows.Forms.Label labelUrsprung;
    private System.Windows.Forms.Label labelDistrikt;
    private System.Windows.Forms.Label labelRegion;
    private System.Windows.Forms.GroupBox groupBoxTrakt;
    private System.Windows.Forms.TextBox textBoxTraktnr;
    private System.Windows.Forms.TextBox textBoxTraktnamn;
    private System.Windows.Forms.TextBox textBoxEntreprenor;
    private System.Windows.Forms.ComboBox comboBoxStandort;
    private System.Windows.Forms.Label labeEntreprenor;
    private System.Windows.Forms.Label labelStandort;
    private System.Windows.Forms.Label labelTraktnamn;
    private System.Windows.Forms.Label labelTraktnr;
    private System.Windows.Forms.GroupBox groupBoxSlutavverkning;
    private System.Windows.Forms.Label labelFragaObs;
    private System.Windows.Forms.Label labelOBS;
    private FastReport.Report reportSlutavverkning;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.TabPage tabPageFrågor2;
    private System.Windows.Forms.TabPage tabPageFrågor3;
    private System.Windows.Forms.GroupBox groupBox11;
    private System.Windows.Forms.RichTextBox richTextBoxOvrigt;
    private System.Data.DataColumn dataColumnFraga3;
    private System.Data.DataColumn dataColumnFraga4;
    private System.Data.DataColumn dataColumnFraga5;
    private System.Data.DataColumn dataColumnFraga6;
    private System.Data.DataColumn dataColumnFraga7;
    private System.Data.DataColumn dataColumnFraga8;
    private System.Data.DataColumn dataColumnFraga9;
    private System.Data.DataColumn dataColumnFraga10;
    private System.Data.DataColumn dataColumnFraga11;
    private System.Data.DataColumn dataColumnFraga12;
    private System.Data.DataColumn dataColumnFraga13;
    private System.Data.DataColumn dataColumnFraga14;
    private System.Data.DataColumn dataColumnFraga15;
    private System.Data.DataColumn dataColumnFraga16;
    private System.Data.DataColumn dataColumnFraga17;
    private System.Data.DataColumn dataColumnFraga18;
    private System.Data.DataColumn dataColumnFraga19;
    private System.Data.DataColumn dataColumnFraga20;
    private System.Data.DataColumn dataColumnFraga21;
    private System.Data.DataColumn dataColumnOvrigt;
    private System.Windows.Forms.GroupBox groupBoxMinstEnlSVL;
    private System.Windows.Forms.RadioButton radioButton1bNej;
    private System.Windows.Forms.RadioButton radioButton1bJa;
    private System.Windows.Forms.GroupBox groupBoxOBS;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.GroupBox groupBox22;
    private System.Windows.Forms.RadioButton radioButton1aNej;
    private System.Windows.Forms.RadioButton radioButton1aJa;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.GroupBox groupBox9;
    private System.Windows.Forms.RadioButton radioButton2hNej;
    private System.Windows.Forms.RadioButton radioButton2hJa;
    private System.Windows.Forms.Label label15;
    private System.Windows.Forms.GroupBox groupBox8;
    private System.Windows.Forms.RadioButton radioButton2gNej;
    private System.Windows.Forms.Label label14;
    private System.Windows.Forms.RadioButton radioButton2gJa;
    private System.Windows.Forms.GroupBox groupBox7;
    private System.Windows.Forms.RadioButton radioButton2fNej;
    private System.Windows.Forms.RadioButton radioButton2fJa;
    private System.Windows.Forms.Label label13;
    private System.Windows.Forms.GroupBox groupBox6;
    private System.Windows.Forms.RadioButton radioButton2eNej;
    private System.Windows.Forms.RadioButton radioButton2eJa;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.GroupBox groupBox5;
    private System.Windows.Forms.RadioButton radioButton2dEjaktuellt;
    private System.Windows.Forms.RadioButton radioButton2dNej;
    private System.Windows.Forms.RadioButton radioButton2dJa;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.RadioButton radioButton2cNej;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.RadioButton radioButton2cJa;
    private System.Windows.Forms.GroupBox groupBox4;
    private System.Windows.Forms.RadioButton radioButton2bNej;
    private System.Windows.Forms.RadioButton radioButton2bJa;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.GroupBox groupBox3;
    private System.Windows.Forms.RadioButton radioButton2aEjaktuellt;
    private System.Windows.Forms.RadioButton radioButton2aNej;
    private System.Windows.Forms.RadioButton radioButton2aJa;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.GroupBox groupBox18;
    private System.Windows.Forms.RadioButton radioButton9Nej;
    private System.Windows.Forms.RadioButton radioButton9Ja;
    private System.Windows.Forms.Label label23;
    private System.Windows.Forms.GroupBox groupBox17;
    private System.Windows.Forms.RadioButton radioButton8Ejaktuellt;
    private System.Windows.Forms.RadioButton radioButton8Nej;
    private System.Windows.Forms.RadioButton radioButton8Ja;
    private System.Windows.Forms.Label label22;
    private System.Windows.Forms.GroupBox groupBox16;
    private System.Windows.Forms.RadioButton radioButton7Nej;
    private System.Windows.Forms.RadioButton radioButton7Ja;
    private System.Windows.Forms.Label label21;
    private System.Windows.Forms.GroupBox groupBox15;
    private System.Windows.Forms.RadioButton radioButton6Nej;
    private System.Windows.Forms.RadioButton radioButton6Ja;
    private System.Windows.Forms.Label label20;
    private System.Windows.Forms.GroupBox groupBox14;
    private System.Windows.Forms.RadioButton radioButton5Nej;
    private System.Windows.Forms.RadioButton radioButton5Ja;
    private System.Windows.Forms.Label label19;
    private System.Windows.Forms.GroupBox groupBox13;
    private System.Windows.Forms.RadioButton radioButton4bNej;
    private System.Windows.Forms.RadioButton radioButton4bJa;
    private System.Windows.Forms.Label label18;
    private System.Windows.Forms.GroupBox groupBox12;
    private System.Windows.Forms.RadioButton radioButton4aNej;
    private System.Windows.Forms.RadioButton radioButton4aJa;
    private System.Windows.Forms.Label label17;
    private System.Windows.Forms.GroupBox groupBox10;
    private System.Windows.Forms.RadioButton radioButton3Ejaktuellt;
    private System.Windows.Forms.RadioButton radioButton3Nej;
    private System.Windows.Forms.RadioButton radioButton3Ja;
    private System.Windows.Forms.Label label16;
    private System.Windows.Forms.GroupBox groupBox19;
    private System.Windows.Forms.RadioButton radioButton12Nej;
    private System.Windows.Forms.RadioButton radioButton12Ja;
    private System.Windows.Forms.Label label24;
    private System.Windows.Forms.GroupBox groupBox21;
    private System.Windows.Forms.RadioButton radioButton11Nej;
    private System.Windows.Forms.RadioButton radioButton11Ja;
    private System.Windows.Forms.Label label26;
    private System.Windows.Forms.GroupBox groupBox20;
    private System.Windows.Forms.RadioButton radioButton10Nej;
    private System.Windows.Forms.RadioButton radioButton10Ja;
    private System.Windows.Forms.Label label25;
    private System.Windows.Forms.RadioButton radioButton2cEjaktuellt;
    private System.Windows.Forms.RadioButton radioButton5Ejaktuellt;
    private System.Windows.Forms.RadioButton radioButton7Ejaktuellt;
    private System.Windows.Forms.RadioButton radioButton6Ejaktuellt;
    private System.Windows.Forms.TextBox textBoxDistrikt;
    private System.Windows.Forms.TextBox textBoxRegion;
  }
}

