﻿#region

using System;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using Egenuppfoljning.Interfaces;
using Egenuppfoljning.Properties;
using FastReport.Export.OoXML;
using FastReport.Export.Pdf;
using Microsoft.Office.Interop.Outlook;
using Application = System.Windows.Forms.Application;
using Attachment = System.Net.Mail.Attachment;
using Exception = System.Exception;

#endregion

namespace Egenuppfoljning.Applications
{
  /// <summary>
  ///   PC Application for egenuppföljning - Biobränsle.
  /// </summary>
  public partial class BiobränsleForm : Form, IKorusRapport
  {
    //Test debug

    //Epost
    private const int CP_NOCLOSE_BUTTON = 0x200;
    private string mEpostAdress;
    private string mMeddelande;

    private string mSDefaultExcel2007Path;
    private string mSDefaultPdfPath;
    private string mSDefaultReportsPath;
    private string mSSettingsFilePath;
    private string mSSettingsPath;
    private string mTitel;


    /// <summary>
    ///   Constructor, initiate data.
    /// </summary>
    public BiobränsleForm()
    {
      //Select the language that should be used.
      Thread.CurrentThread.CurrentCulture = new CultureInfo(Settings.Default.SettingsLanguage);
      Thread.CurrentThread.CurrentUICulture = new CultureInfo(Settings.Default.SettingsLanguage);
      InitializeComponent();
    }

    public BiobränsleForm(bool aRedigeraLagratData)
    {
      //Select the language that should be used.
      Thread.CurrentThread.CurrentCulture = new CultureInfo(Settings.Default.SettingsLanguage);
      Thread.CurrentThread.CurrentUICulture = new CultureInfo(Settings.Default.SettingsLanguage);
      InitializeComponent();
      sparaSomToolStripMenuItem.Visible = false;

      //nyckelvärden kan ej ändras.
      comboBoxRegion.Enabled = false;
      comboBoxDistrikt.Enabled = false;
      textBoxTraktnr.Enabled = false;
      comboBoxStandort.Enabled = false;
      textBoxEntreprenor.Enabled = false;
      dateTimePicker.Enabled = false;
      sparaToolStripMenuItem.Text = Resources.Lagra_andrat_data;

      RedigeraLagratData = aRedigeraLagratData;

      if (RedigeraLagratData)
      {
          comboBoxRegion.Visible = false;
          comboBoxDistrikt.Visible = false;
          textBoxRegion.Visible = true;
          textBoxDistrikt.Visible = true;
      }
      else
      {
          comboBoxRegion.Visible = true;
          comboBoxDistrikt.Visible = true;
          textBoxRegion.Visible = false;
          textBoxDistrikt.Visible = false;
      }
    }

    // Avvaktivera stäng knappen, för att få kontrollen att kunna välja att inte lagra data ifall Avsluta valet i menyn väljs

    protected override CreateParams CreateParams
    {
      get
      {
        var myCp = base.CreateParams;
        myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
        return myCp;
      }
    }

    #region IKorusRapport Members

    public bool RedigeraLagratData { get; private set; }

    public void ShowEditDialog(bool aRedigeraLagratData)
    {
      try
      {
        RedigeraLagratData = aRedigeraLagratData;
        ShowDialog();
      }
      catch (Exception ex)
      {
#if !DEBUG
       MessageBox.Show("Starta editeringsdialog misslyckades.", "Editera misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    /// <summary>
    ///   Writes the data that has been selected in the GUI.
    /// </summary>
    public bool WriteDataToReportXmlFile(string aFilePath, bool aShowDialog, bool aWriteSchema, string aReportStatus,
                                         string aReportStatusDatum)
    {
      XmlTextWriter objXmlReportWriter = null;
      XmlTextWriter objXmlSchemReportWriter = null;

      if (!aWriteSchema && aFilePath != null)
      {
        //check so the correct data exsist for the report filename
        var reportName = GetReportFileName();
        var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(aFilePath);
        if (fileNameWithoutExtension != null && (reportName != null && !fileNameWithoutExtension.Equals(reportName)))
        {
          aFilePath = ShowSaveAsDialog(Settings.Default.ReportFilePath.Equals(string.Empty)
                                         ? mSDefaultReportsPath
                                         : Path.GetDirectoryName(Settings.Default.ReportFilePath), Resources.keg);
        }
      }

      if (aFilePath == null || aFilePath.Trim().Length <= 0)
      {
        return false;
      }

      InitEmptyTables();

      try
      {
        if (aWriteSchema)
        {
          objXmlSchemReportWriter = new XmlTextWriter(aFilePath, null) {Formatting = Formatting.Indented};
          objXmlSchemReportWriter.WriteStartDocument();
          dataSetBiobränsle.WriteXmlSchema(objXmlSchemReportWriter);
          objXmlSchemReportWriter.WriteEndDocument();
          objXmlSchemReportWriter.Flush();
        }
        else
        {
          objXmlReportWriter = new XmlTextWriter(aFilePath, null) {Formatting = Formatting.Indented};
          objXmlReportWriter.WriteStartDocument();
          SetDsStr("UserData", "Language", Settings.Default.SettingsLanguage);
          SetDsStr("UserData", "Status", aReportStatus);
          SetDsStr("UserData", "Status_Datum", aReportStatusDatum);
          dataSetBiobränsle.WriteXml(objXmlReportWriter);
          objXmlReportWriter.WriteEndDocument();
          objXmlReportWriter.Flush();
          Settings.Default.ReportFilePath = aFilePath;

          if (aShowDialog)
          {
            MessageBox.Show(String.Format(Resources.RapportenSparadMessage, Path.GetFileName(aFilePath))
                            , Resources.RapportSparadTitel, MessageBoxButtons.OK, MessageBoxIcon.Information);
          }
        }
      }
      catch (Exception ex)
      {
#if !DEBUG
     MessageBox.Show("Skriva data till rapportfil misslyckades", "Spara misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
        return false;
      }
      finally
      {
        if (objXmlReportWriter != null)
        {
          objXmlReportWriter.Close();
        }

        if (objXmlSchemReportWriter != null)
        {
          objXmlSchemReportWriter.Close();
        }
      }
      return true;
    }

    public void ShowReport()
    {
      try
      {
        reportBiobränsle.Show();
      }
      catch (Exception ex)
      {
#if !DEBUG
        MessageBox.Show(Resources.ForhandgranskningFailed);
#else
        MessageBox.Show(Resources.ForhandgranskningFailed + ex);
#endif
      }
    }


    public void ExporteraExcel2007()
    {
      try
      {
        InitExportDirs();
        //kolla först så användaren har skrivit in det datat som krävs för ett giltigt filnamn.        
        reportBiobränsle.Prepare();
        var export = new Excel2007Export();
        // Visa export alternativ
        if (!export.ShowDialog()) return;

        var filePath = ShowSaveAsDialog(mSDefaultExcel2007Path, Resources.Excel_suffix);
        if (filePath == null) return;
        reportBiobränsle.Export(export, filePath);
        MessageBox.Show(Resources.ExportRapport + filePath
                        , Resources.Exportering_lyckades, MessageBoxButtons.OK, MessageBoxIcon.Information);
      }
      catch (Exception ex)
      {
#if !DEBUG
          MessageBox.Show("Misslyckades med att exportera ut data till formatet: Excel 2007");
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    public void ExporteraAdobePDF()
    {
      try
      {
        InitExportDirs();
        //kolla först så användaren har skrivit in det datat som krävs för ett giltigt filnamn.        
        reportBiobränsle.Prepare();
        var export = new PDFExport();
        // Visa export alternativ
        if (!export.ShowDialog()) return;

        var filePath = ShowSaveAsDialog(mSDefaultPdfPath, Resources.Pdf_suffix);
        if (filePath == null) return;
        reportBiobränsle.Export(export, filePath);
        MessageBox.Show(Resources.ExportRapport + filePath
                        , Resources.Exportering_lyckades, MessageBoxButtons.OK, MessageBoxIcon.Information);
      }
      catch (Exception ex)
      {
#if !DEBUG
          MessageBox.Show("Misslyckades med att exportera ut data till formatet: Pdf");
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    public void UppdateraData(KorusDataEventArgs aData)
    {
      try
      {
        dataSetBiobränsle.Clear();
        //userdata
        DateTime datum;
        DateTime.TryParse(aData.data.datum, out datum);

        dataSetBiobränsle.Tables["UserData"].Rows.Add(aData.data.regionid, aData.data.regionnamn, aData.data.distriktid,
                                                      aData.data.distriktnamn, aData.data.ursprung, datum,
                                                      aData.data.traktnr, aData.data.traktnamn, aData.data.standort,
                                                      aData.data.entreprenor, string.Empty, string.Empty,
                                                      Resources.Lagrad, DateTime.MinValue, aData.data.årtal,
                                                      aData.data.antalvaltor, aData.data.avstand, aData.data.bedomdvolym,
                                                      aData.data.hygge, aData.data.vagkant, aData.data.aker,
                                                      aData.data.latbilshugg, aData.data.grotbil,
                                                      aData.data.traktordragenhugg, aData.data.skotarburenhugg,
                                                      aData.data.avlagg1norr, aData.data.avlagg1ost,
                                                      aData.data.avlagg2norr, aData.data.avlagg2ost,
                                                      aData.data.avlagg3norr, aData.data.avlagg3ost,
                                                      aData.data.avlagg4norr, aData.data.avlagg4ost,
                                                      aData.data.avlagg5norr, aData.data.avlagg5ost,
                                                      aData.data.avlagg6norr, aData.data.avlagg6ost);
        dataSetBiobränsle.Tables["Fragor"].Rows.Add(aData.frågeformulär.fraga1, aData.frågeformulär.fraga2,
                                                    aData.frågeformulär.fraga3, aData.frågeformulär.fraga4,
                                                    aData.frågeformulär.fraga5, aData.frågeformulär.fraga6,
                                                    aData.frågeformulär.fraga7, aData.frågeformulär.fraga8,
                                                    aData.frågeformulär.fraga9, aData.frågeformulär.fraga10,
                                                    aData.frågeformulär.fraga11, aData.frågeformulär.fraga12,
                                                    aData.frågeformulär.fraga13, aData.frågeformulär.fraga14,
                                                    aData.frågeformulär.fraga15, aData.frågeformulär.fraga16,
                                                    aData.frågeformulär.fraga17, aData.frågeformulär.fraga18,
                                                    aData.frågeformulär.fraga19, aData.frågeformulär.ovrigt);

        UpdateAntalVältor();
        StatusFrågor();
        ProgressNumberOfDataDone();
      }
      catch (Exception ex)
      {
#if !DEBUG
     MessageBox.Show("Misslyckades med att läsa in data", "Läsa data misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    public KorusDataEventArgs GetKORUSData()
    {
      var obj = new KorusDataEventArgs();

      //Nyckelvariabler
      int.TryParse(GetDsStr("UserData", "RegionId"), out obj.data.regionid);
      int.TryParse(GetDsStr("UserData", "DistriktId"), out obj.data.distriktid);
      int.TryParse(GetDsStr("UserData", "Traktnr"), out obj.data.traktnr);
      int.TryParse(GetDsStr("UserData", "Standort"), out obj.data.standort);
      int.TryParse(GetDsStr("UserData", "Entreprenor"), out obj.data.entreprenor);

      obj.data.rapport_typ = (int) RapportTyp.Biobränsle;
      obj.data.årtal = GetDatumÅr(GetDsStr("UserData", "Datum"));
      obj.data.traktnamn = GetDsStr("UserData", "Traktnamn");
      obj.data.regionnamn = GetDsStr("UserData", "Region");
      obj.data.distriktnamn = GetDsStr("UserData", "Distrikt");
      obj.data.ursprung = GetDsStr("UserData", "Ursprung");
      obj.data.datum = GetDsStr("UserData", "Datum");
      obj.data.kommentar = GetDsStr("Fragor", "Ovrigt"); //Undantag biobränsla, får lagra övrigt som kommentar

      int.TryParse(GetDsStr("UserData", "AntalValtor"), out obj.data.antalvaltor);
      int.TryParse(GetDsStr("UserData", "Avstand"), out obj.data.avstand);
      int.TryParse(GetDsStr("UserData", "BedomdVolym"), out obj.data.bedomdvolym);
      int.TryParse(GetDsStr("UserData", "Hygge"), out obj.data.hygge);
      int.TryParse(GetDsStr("UserData", "Vagkant"), out obj.data.vagkant);
      int.TryParse(GetDsStr("UserData", "Aker"), out obj.data.aker);
      bool.TryParse(GetDsStr("UserData", "Latbilshugg"), out obj.data.latbilshugg);
      bool.TryParse(GetDsStr("UserData", "Grotbil"), out obj.data.grotbil);
      bool.TryParse(GetDsStr("UserData", "TraktordragenHugg"), out obj.data.traktordragenhugg);
      bool.TryParse(GetDsStr("UserData", "SkotarburenHugg"), out obj.data.skotarburenhugg);
      int.TryParse(GetDsStr("UserData", "Avlagg1Norr"), out obj.data.avlagg1norr);
      int.TryParse(GetDsStr("UserData", "Avlagg1Ost"), out obj.data.avlagg1ost);
      int.TryParse(GetDsStr("UserData", "Avlagg2Norr"), out obj.data.avlagg2norr);
      int.TryParse(GetDsStr("UserData", "Avlagg2Ost"), out obj.data.avlagg2ost);
      int.TryParse(GetDsStr("UserData", "Avlagg3Norr"), out obj.data.avlagg3norr);
      int.TryParse(GetDsStr("UserData", "Avlagg3Ost"), out obj.data.avlagg3ost);
      int.TryParse(GetDsStr("UserData", "Avlagg4Norr"), out obj.data.avlagg4norr);
      int.TryParse(GetDsStr("UserData", "Avlagg4Ost"), out obj.data.avlagg4ost);
      int.TryParse(GetDsStr("UserData", "Avlagg5Norr"), out obj.data.avlagg5norr);
      int.TryParse(GetDsStr("UserData", "Avlagg5Ost"), out obj.data.avlagg5ost);
      int.TryParse(GetDsStr("UserData", "Avlagg6Norr"), out obj.data.avlagg6norr);
      int.TryParse(GetDsStr("UserData", "Avlagg6Ost"), out obj.data.avlagg6ost);

      obj.frågeformulär.fraga1 = GetDsStr("Fragor", "Fraga1");
      obj.frågeformulär.fraga2 = GetDsStr("Fragor", "Fraga2");
      obj.frågeformulär.fraga3 = GetDsStr("Fragor", "Fraga3");
      obj.frågeformulär.fraga4 = GetDsStr("Fragor", "Fraga4");
      obj.frågeformulär.fraga5 = GetDsStr("Fragor", "Fraga5");
      obj.frågeformulär.fraga6 = GetDsStr("Fragor", "Fraga6");
      obj.frågeformulär.fraga7 = GetDsStr("Fragor", "Fraga7");
      obj.frågeformulär.fraga8 = GetDsStr("Fragor", "Fraga8");
      obj.frågeformulär.fraga9 = GetDsStr("Fragor", "Fraga9");
      obj.frågeformulär.fraga10 = GetDsStr("Fragor", "Fraga10");
      obj.frågeformulär.fraga11 = GetDsStr("Fragor", "Fraga11");
      obj.frågeformulär.fraga12 = GetDsStr("Fragor", "Fraga12");
      obj.frågeformulär.fraga13 = GetDsStr("Fragor", "Fraga13");
      obj.frågeformulär.fraga14 = GetDsStr("Fragor", "Fraga14");
      obj.frågeformulär.fraga15 = GetDsStr("Fragor", "Fraga15");
      obj.frågeformulär.fraga16 = GetDsStr("Fragor", "Fraga16");
      obj.frågeformulär.fraga17 = GetDsStr("Fragor", "Fraga17");
      obj.frågeformulär.fraga18 = GetDsStr("Fragor", "Fraga18");
      obj.frågeformulär.fraga19 = GetDsStr("Fragor", "Fraga19");
      obj.frågeformulär.ovrigt = GetDsStr("Fragor", "Ovrigt");

      return obj;
    }

    public void SkrivUt()
    {
      try
      {
        reportBiobränsle.Print();
      }
      catch (Exception ex)
      {
#if !DEBUG
       MessageBox.Show("Skriva ut rapport misslyckades", "Skriva ut misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    public string GetEgenuppföljningsTyp()
    {
      return Resources.Biobransle;
    }

    public int GetTotalaAntaletYtor()
    {
      return 0;
    }

    public int GetProcentStatus()
    {
      return (int) ((progressBarData.Value/(double) progressBarData.Maximum)*100);
    }

    public void LaddaForm(string aRapportSökväg)
    {
      //mSSettingsPath = Application.StartupPath + Settings.Default.SettingsPath;
      //mSSettingsFilePath = Application.StartupPath + Settings.Default.SettingsPath + Settings.Default.SettingsFilename +
      //                     "." + Settings.Default.SettingsLanguage + Resources.Xml_suffix;
        mSSettingsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + Settings.Default.SettingsPath;
        mSSettingsFilePath = mSSettingsPath + Settings.Default.SettingsFilename +
                             "." + Settings.Default.SettingsLanguage + Resources.Xml_suffix;

        mSDefaultExcel2007Path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                               Settings.Default.ReportsPath + Settings.Default.Excel2007Path;
      mSDefaultPdfPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                         Settings.Default.ReportsPath + Settings.Default.PdfPath;

      InitFastReportMail();

      if (aRapportSökväg != null)
      {
        Settings.Default.ReportFilePath = aRapportSökväg;
        mSDefaultReportsPath = Application.StartupPath + Settings.Default.ReportsPath;
        LoadBiobränsle();
      }
      else
      {
        ReadSettingsXmlFile();
        InitData();
        InitEmptyTables();
      }
    }

    /// <summary>
    ///   Checks the values in the GUI and puts together a name from the data as follows: [Biobränslesmetod]_[Traktnr]_[Traktnamn]_[Entreprenor]
    /// </summary>
    /// <returns> The filename. </returns>
    public string GetReportFileName()
    {
      var missingData = "";
      var filename = Resources.BiobranslePrefix;

      //Region id
      var regionid = GetDsStr("UserData", "RegionId");

      if (regionid.Equals(string.Empty))
      {
        missingData += " " + Resources.RegionId;
      }
      else
      {
        filename += Resources.FileSep + regionid;
      }

      //Distrikt id
      var distriktid = GetDsStr("UserData", "DistriktId");

      if (distriktid.Equals(string.Empty))
      {
        missingData += " " + Resources.DistriktId;
      }
      else
      {
        filename += Resources.FileSep + distriktid;
      }

      //Årtal
      DateTime date;
      DateTime.TryParse(GetDsStr("UserData", "Datum"), out date);

      if (date.Equals(DateTime.MinValue))
      {
        missingData += " " + Resources.Artal;
      }
      else
      {
        filename += Resources.FileSep + date.Year.ToString(CultureInfo.InvariantCulture);
      }

      //Traktnr
      var traktnr = GetDsStr("UserData", "Traktnr");

      if (traktnr.Equals(string.Empty) || traktnr.Equals("0"))
      {
        missingData += " " + Resources.Traktnr;
      }
      else
      {
        filename += Resources.FileSep + traktnr;
      }

      //Ståndort
      var standort = GetDsStr("UserData", "Standort");

      if (standort.Equals(string.Empty))
      {
        missingData += " " + Resources.Standort;
      }
      else
      {
        filename += Resources.FileSep + standort;
      }

      var entreprenörer = GetDsStr("UserData", "Entreprenor");

      //Entreprenörnr
      if (entreprenörer.Equals(string.Empty) || entreprenörer.Equals("0"))
      {
        missingData += " " + Resources.Entreprenor;
      }
      else
      {
        filename += Resources.FileSep + entreprenörer;
      }


      if (!missingData.Equals(string.Empty))
      {
        //some data is missing to create the file name
        var errMessage = "Följande data saknas för att kunna spara rapporten: ";
        errMessage += missingData;
        errMessage += "\nFyll i dessa data och välj spara igen.";
        MessageBox.Show(errMessage, Resources.Data_saknas, MessageBoxButtons.OK, MessageBoxIcon.Stop);

        return null;
      }
      return filename;
    }

    public void SändEpost(string aEpostAdress, string aTitel, string aMeddelande, bool aAnvändInternEpost,
                          bool aBifogRapport)
    {
      mEpostAdress = aEpostAdress;
      mTitel = aTitel;
      mMeddelande = aMeddelande;

      if (aAnvändInternEpost)
      {
        SendInternMail(aBifogRapport);
      }
      else
      {
        SendViaOutlook(aBifogRapport);
      }
    }

    public string GetDsStr(string aTable, string aColumn)
    {
      return GetDsStr(aTable, aColumn, 0, dataSetBiobränsle);
    }

    #endregion

    private void avslutaToolStripMenuItem_Click(object sender, EventArgs e)
    {
      RedigeraLagratData = false; //uppdatera inte databasen
      Hide();
    }

    private void WriteSettingsXmlFile(bool aWriteSchema)
    {
      XmlTextWriter objXmlSettingsWriter = null;
      XmlTextWriter objXmlSettingsSchemaWriter = null;
      InitSettingsData();

      try
      {
        if (aWriteSchema)
        {
          objXmlSettingsSchemaWriter = new XmlTextWriter(mSSettingsPath + Settings.Default.SettingsSchemaFilename, null)
            {Formatting = Formatting.Indented};
          objXmlSettingsSchemaWriter.WriteStartDocument();
          dataSetSettings.WriteXmlSchema(objXmlSettingsSchemaWriter);
          objXmlSettingsSchemaWriter.WriteEndDocument();
          objXmlSettingsSchemaWriter.Flush();
        }
        else
        {
          objXmlSettingsWriter = new XmlTextWriter(mSSettingsFilePath, null) {Formatting = Formatting.Indented};
          objXmlSettingsWriter.WriteStartDocument();
          dataSetSettings.WriteXml(objXmlSettingsWriter);
          objXmlSettingsWriter.WriteEndDocument();
          objXmlSettingsWriter.Flush();
        }
      }
      catch (Exception ex)
      {
#if !DEBUG
       MessageBox.Show("Spara Settings data misslyckades", "Spara misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
      finally
      {
        if (objXmlSettingsWriter != null)
        {
          objXmlSettingsWriter.Close();
        }
        if (objXmlSettingsSchemaWriter != null)
        {
          objXmlSettingsSchemaWriter.Close();
        }
      }
    }

    /// <summary>
    ///   Read the XML file containing the settings for the Application.
    /// </summary>
    private void ReadSettingsXmlFile()
    {
      InitSettingsData();

      var objValidator = new XmlValidator(mSSettingsFilePath,
                                          mSSettingsPath + Settings.Default.SettingsSchemaFilename);

      if (objValidator.ValidateXmlFileFailed())
      {
        //something is wrong with the Settings file then write a new Settings and Schema file.
        WriteSettingsXmlFile(false);
        WriteSettingsXmlFile(true);
      }

      try
      {
        dataSetSettings.Clear();
        dataSetSettings.ReadXml(mSSettingsFilePath);
      }
      catch (Exception ex)
      {
#if !DEBUG
       MessageBox.Show("Läsa Settings data misslyckades", "Läsa misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    /// <summary>
    ///   Initializes the report data tables.
    /// </summary>
    private void InitEmptyTables()
    {
      if (dataSetBiobränsle.Tables["UserData"].Rows.Count == 0)
      {
        dataSetBiobränsle.Tables["UserData"].Rows.Add(0, string.Empty, 0, string.Empty, string.Empty, DateTime.Today, 0,
                                                      string.Empty, 0, 0, string.Empty, string.Empty, string.Empty,
                                                      DateTime.Today, 0, 0, 0, 0, 0, 0, 0, false, false, false, false, 0,
                                                      0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
      }

      if (dataSetBiobränsle.Tables["Fragor"].Rows.Count == 0)
      {
        dataSetBiobränsle.Tables["Fragor"].Rows.Add(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty,
                                                    string.Empty, string.Empty, string.Empty, string.Empty, string.Empty,
                                                    string.Empty, string.Empty, string.Empty, string.Empty, string.Empty,
                                                    string.Empty, string.Empty, string.Empty, string.Empty, string.Empty);
      }
      StatusFrågor();
      UpdateAntalVältor();
    }

    /// <summary>
    ///   Sets a hardcoded default settingsdata.
    /// </summary>
    private void DefaultSettingsData()
    {
      dataSetSettings.Tables["Region"].Rows.Add("11", "11 - Nord");
      dataSetSettings.Tables["Region"].Rows.Add("16", "16 - Mitt");
      dataSetSettings.Tables["Region"].Rows.Add("18", "18 - Syd");

      dataSetSettings.Tables["Distrikt"].Rows.Add("11", "14", "14 - Örnsköldsvik");
      dataSetSettings.Tables["Distrikt"].Rows.Add("11", "19", "19 - Umeå");
      dataSetSettings.Tables["Distrikt"].Rows.Add("16", "12", "12 - Sveg");
      dataSetSettings.Tables["Distrikt"].Rows.Add("16", "14", "14 - Ljusdal");
      dataSetSettings.Tables["Distrikt"].Rows.Add("16", "15", "15 - Delsbo");
      dataSetSettings.Tables["Distrikt"].Rows.Add("16", "16", "16 - Hudiksvall");
      dataSetSettings.Tables["Distrikt"].Rows.Add("16", "18", "18 - Bollnäs");
      dataSetSettings.Tables["Distrikt"].Rows.Add("16", "21", "21 - Uppland");
      dataSetSettings.Tables["Distrikt"].Rows.Add("18", "10", "10 - Västerås");
      dataSetSettings.Tables["Distrikt"].Rows.Add("18", "11", "11 - Örebro");
      dataSetSettings.Tables["Distrikt"].Rows.Add("18", "12", "12 - Nyköping");
      dataSetSettings.Tables["Distrikt"].Rows.Add("18", "13", "13 - Götaland");
      dataSetSettings.Tables["Distrikt"].Rows.Add("18", "21", "21 - Egen Skog");

      dataSetSettings.Tables["Standort"].Rows.Add("1");
      dataSetSettings.Tables["Standort"].Rows.Add("2");
      dataSetSettings.Tables["Standort"].Rows.Add("3");
      dataSetSettings.Tables["Standort"].Rows.Add("4");
      dataSetSettings.Tables["Standort"].Rows.Add("5");
      dataSetSettings.Tables["Standort"].Rows.Add("6");

      dataSetSettings.Tables["Markberedningsmetod"].Rows.Add("Kontinuerligt harv");
      dataSetSettings.Tables["Markberedningsmetod"].Rows.Add("Högläggning");
      dataSetSettings.Tables["Markberedningsmetod"].Rows.Add("För sådd");

      dataSetSettings.Tables["Ursprung"].Rows.Add("Eget");
      dataSetSettings.Tables["Ursprung"].Rows.Add("Köp");
    }

    /// <summary>
    ///   Initializes the settings data tables.
    /// </summary>
    private void InitSettingsData()
    {
      if (dataSetSettings.Tables["Region"].Rows.Count == 0)
      {
        dataSetSettings.Tables["Region"].Rows.Add(0, string.Empty);
      }

      if (dataSetSettings.Tables["Distrikt"].Rows.Count == 0)
      {
        dataSetSettings.Tables["Distrikt"].Rows.Add(0, 0, string.Empty);
      }

      if (dataSetSettings.Tables["Standort"].Rows.Count == 0)
      {
        dataSetSettings.Tables["Standort"].Rows.Add(0);
      }

      if (dataSetSettings.Tables["Markberedningsmetod"].Rows.Count == 0)
      {
        dataSetSettings.Tables["Markberedningsmetod"].Rows.Add(string.Empty);
      }

      if (dataSetSettings.Tables["Ursprung"].Rows.Count == 0)
      {
        dataSetSettings.Tables["Ursprung"].Rows.Add(string.Empty);
      }
    }

    /// <summary>
    ///   first check the language in the file, if it is a new language, then give a question to restart with that file
    /// </summary>
    /// <param name="aFilePath"> File path to the report. </param>
    /// <param name="aStartup"> Flag that indicates if it is load in startup or not </param>
    /// <returns> true if Language check is ignored (same language) false if user selected cancel on the dialog. </returns>
    private static bool CheckLanguage(string aFilePath, bool aStartup)
    {
      if (!aStartup)
      {
        using (var tmpSet = new DataSet())
        {
          tmpSet.ReadXml(aFilePath);
          if (tmpSet.Tables["UserData"].Rows[0]["Language"] != null)
          {
            var language = tmpSet.Tables["UserData"].Rows[0]["Language"].ToString();
            if (!language.Equals(Settings.Default.SettingsLanguage))
            {
              if (MessageBox.Show(
                String.Format(
                  "Rapporten {0} är inställd på språket {1} ({2}). Vill du ändra till det språket och ladda rapporten?",
                  Path.GetFileName(aFilePath), language, GetLanguage(language))
                , Resources.Andra_sprak, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
              {
                Settings.Default.ReportFilePath = aFilePath;
                ChangeLanguage(language);
              }
              else
              {
                //User cancel, then cancel to load of the report, 
                return false;
              }
            }
          }
        }
      }
      return true;
    }

    /// <summary>
    ///   Reads the active Report XML document.
    /// </summary>
    private void ReadDataFromReportXmlFile(string aFilePath, bool aStartup)
    {
      InitEmptyTables();

      if (!File.Exists(aFilePath))
      {
        //Return if the file does not exists.
        return;
      }

      if (!File.Exists(mSSettingsPath + Settings.Default.ReportSchemaBiobransle))
      {
        //Write a new Report Schema file if it does not exist.
        WriteDataToReportXmlFile(mSSettingsPath + Settings.Default.ReportSchemaBiobransle, false, true,
                                 Settings.Default.ReportStatus, Settings.Default.ReportStatusDatum);
      }

      var objValidator = new XmlValidator(aFilePath, mSSettingsPath + Settings.Default.ReportSchemaBiobransle);

      if (objValidator.ValidateXmlFileFailed())
      {
        //something is wrong with the XML file.
        if (
          MessageBox.Show(Resources.EjGiltigFil, Resources.FelaktigKegFil, MessageBoxButtons.YesNo,
                          MessageBoxIcon.Question) == DialogResult.No)
        {
          return;
        }
      }

      try
      {
        //first check the language.
        if (!CheckLanguage(aFilePath, aStartup)) return;

        dataSetBiobränsle.Clear();
        dataSetBiobränsle.ReadXml(aFilePath);

        UpdateAntalVältor();
        StatusFrågor();
        ProgressNumberOfDataDone();
        Settings.Default.ReportFilePath = aFilePath;
      }
      catch (Exception ex)
      {
#if !DEBUG
     MessageBox.Show("Läsa data från rapport fil misslyckades", "Läsa data misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    private void InitExportDirs()
    {
      if (!Directory.Exists(mSDefaultExcel2007Path))
      {
        //Report dir does not exists, create it.
        Directory.CreateDirectory(mSDefaultExcel2007Path);
      }
      if (!Directory.Exists(mSDefaultPdfPath))
      {
        //Report dir does not exists, create it.
        Directory.CreateDirectory(mSDefaultPdfPath);
      }
    }

    /// <summary>
    ///   Checks so the application got the required files and directories otherwise create them.
    /// </summary>
    private void InitApplicationDirFilesStructure()
    {
      if (!Directory.Exists(mSSettingsPath))
      {
        //Settings dir does not exists, create it.
        Directory.CreateDirectory(mSSettingsPath);
      }

      if (!Directory.Exists(mSDefaultReportsPath))
      {
        //Report dir does not exists, create it.
        Directory.CreateDirectory(mSDefaultReportsPath);
      }

      if (!File.Exists(mSSettingsFilePath))
      {
        //Settings xml file does not exist, create a default file.
        DefaultSettingsData();
        WriteSettingsXmlFile(false);
      }
      if (!File.Exists(mSSettingsPath + Settings.Default.SettingsSchemaFilename))
      {
        //The schema settings file is missing, create it.
        WriteSettingsXmlFile(true);
      }

      if (!File.Exists(mSSettingsPath + Settings.Default.ReportSchemaBiobransle))
      {
        //The schema report file is missing, create it.
        WriteDataToReportXmlFile(mSSettingsPath + Settings.Default.ReportSchemaBiobransle, false, true,
                                 Settings.Default.ReportStatus, Settings.Default.ReportStatusDatum);
      }
    }

    /// <summary>
    ///   Called when the applications starts.
    /// </summary>
    /// <param name="sender"> Sender object </param>
    /// <param name="e"> Event arguments </param>
    private void Biobränsle_Load(object sender, EventArgs e)
    {
    }


    private void sparaToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (RedigeraLagratData)
      {
        Hide();
      }
      else
      {
        WriteDataToReportXmlFile(Settings.Default.ReportFilePath, true, false, Resources.Obehandlad,
                                 DateTime.MinValue.ToString(CultureInfo.InvariantCulture));
      }
    }

    private void LoadBiobränsle()
    {
      InitApplicationDirFilesStructure();
      ReadSettingsXmlFile();
      InitData();
      ReadDataFromReportXmlFile(Settings.Default.ReportFilePath, true);
    }


    /// <summary>
    ///   Shows the save as dialog to let the user be able to save the report with a custom name.
    /// </summary>
    /// <returns> The file path name. </returns>
    private string ShowSaveAsDialog(string aSelectedPath, string aFileSuffix)
    {
      // folderBrowserSaveReportDialog.RootFolder = Environment.SpecialFolder.MyComputer;

      saveReportFolderBrowserDialog.SelectedPath = aSelectedPath;

      var fileName = GetReportFileName();

      if (fileName != null)
      {
        saveReportFolderBrowserDialog.Description = Resources.Spara_rapport + fileName + aFileSuffix;

        if (saveReportFolderBrowserDialog.ShowDialog() == DialogResult.OK)
        {
          return saveReportFolderBrowserDialog.SelectedPath + Path.DirectorySeparatorChar + fileName + aFileSuffix;
        }
      }
      return null;
    }

    private void sparaSomToolStripMenuItem_Click(object sender, EventArgs e)
    {
      var filePath = ShowSaveAsDialog(Settings.Default.ReportFilePath.Equals(string.Empty)
                                        ? mSDefaultReportsPath
                                        : Path.GetDirectoryName(Settings.Default.ReportFilePath), Resources.keg);
      if (File.Exists(filePath))
      {
        if (
          MessageBox.Show(
            Resources.Skriva_over_redan_befintlig_rapport + Path.GetFileNameWithoutExtension(filePath) +
            Resources.Fragetecken,
            Resources.Skriva_over_rapport, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
        {
          return;
        }
      }
      WriteDataToReportXmlFile(filePath, true, false, Settings.Default.ReportStatus, Settings.Default.ReportStatusDatum);
    }

    private void Biobränsle_FormClosing(object sender, FormClosingEventArgs e)
    {
      Settings.Default.Save();
      BindingContext[dataSetBiobränsle].EndCurrentEdit();

      e.Cancel = true;
      Hide();
    }

    private void InitData()
    {
      dataSetBiobränsle.Clear();
      progressBarData.Value = 0;
      labelProgress.Text = Resources.Klar;
    }

    private void SendViaOutlook(bool aBifogaRapport)
    {
      if (!CheckMailSettings(false)) return;
      //First save the report
      WriteDataToReportXmlFile(Settings.Default.ReportFilePath, false, false, Settings.Default.ReportStatus,
                               Settings.Default.ReportStatusDatum);
      try
      {
        var oApp = new Microsoft.Office.Interop.Outlook.Application();
        var oMsg = (MailItem) oApp.CreateItem(OlItemType.olMailItem);
        var oRecip = oMsg.Recipients.Add(mEpostAdress);

        oRecip.Resolve();

        oMsg.Subject = mTitel;
        oMsg.Body = mMeddelande;

        if (aBifogaRapport)
        {
          //Add rapport.
          var sDisplayName = Settings.Default.MailAttachmentName;
          var iPosition = oMsg.Body.Length + 1;
          const int ATTACH_TYPE = (int) OlAttachmentType.olByValue;
          oMsg.Attachments.Add(Settings.Default.ReportFilePath, ATTACH_TYPE, iPosition, sDisplayName);
        }
        // Visa meddelandet.
        // oMsg.Display(true);  //modal

        oMsg.Save();
        // ReSharper disable RedundantCast
        ((_MailItem) oMsg).Send();
        // ReSharper restore RedundantCast
      }
      catch (Exception ex)
      {
#if !DEBUG
       MessageBox.Show("Sända rapporten via Outlook misslyckades. Kolla så epost inställningarna är korrekta.", "Sända misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    private bool CheckMailSettings(bool aInternMail)
    {
      var errMessage = string.Empty;

      if (Settings.Default.MailSendFrom.Equals(string.Empty))
      {
        errMessage += Resources.DinEPostadress + " ";
      }
      if (Settings.Default.MailFullName.Equals(string.Empty))
      {
        errMessage += Resources.DittNamn + " ";
      }
      if (mEpostAdress.Equals(string.Empty))
      {
        errMessage += Resources.MottagarensEPostadress + " ";
      }
      if (mTitel.Equals(string.Empty))
      {
        errMessage += Resources.Titel + " ";
      }
      if (aInternMail)
      {
        if (Settings.Default.MailSMTP.Equals(string.Empty))
        {
          errMessage += Resources.UtgaendeEPostSMTP + " ";
        }
      }

      if (!errMessage.Equals(string.Empty))
      {
        MessageBox.Show(
          String.Format(
            Resources.Foljande_epost_installningar_saknas,
            errMessage), Resources.Epost_installningar_saknas, MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }

      return (errMessage.Equals(string.Empty));
    }

    private void SendInternMail(bool aBifogaRapport)
    {
      if (!CheckMailSettings(true)) return;
      //First save the report
      WriteDataToReportXmlFile(Settings.Default.ReportFilePath, false, false, Settings.Default.ReportStatus,
                               Settings.Default.ReportStatusDatum);
      try
      {
        var smtpClient = new SmtpClient(Settings.Default.MailSMTP);
        var fromMail = new MailAddress(Settings.Default.MailSendFrom, Settings.Default.MailFullName,
                                       Encoding.UTF8);
        var toMail = new MailAddress(mEpostAdress);

        using (var message = new MailMessage(fromMail, toMail))
        {
          message.Body = mMeddelande;
          message.BodyEncoding = Encoding.UTF8;
          message.Subject = mTitel;
          message.SubjectEncoding = Encoding.UTF8;

          var smtpUserInfo = new NetworkCredential(Settings.Default.MailUsername,
                                                   Settings.Default.MailPassword);
          smtpClient.UseDefaultCredentials = false;
          smtpClient.Credentials = smtpUserInfo;
          smtpClient.Port = Settings.Default.MailSMTPPort;
          if (aBifogaRapport)
          {
            var attachment = new Attachment(Settings.Default.ReportFilePath);
            message.Attachments.Add(attachment);
          }
          smtpClient.Send(message);
          MessageBox.Show(Resources.RapportenArSand, Resources.RapportSandTitle, MessageBoxButtons.OK,
                          MessageBoxIcon.Information);
        }
      }
      catch (Exception ex)
      {
#if !DEBUG
       MessageBox.Show("Sända rapport via intern mail misslyckades. Kolla så epost inställningarna är korrekta.", "Sända misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    private void InitFastReportMail()
    {
      reportBiobränsle.EmailSettings.Recipients = new[] {mEpostAdress};
      reportBiobränsle.EmailSettings.Subject = mTitel;
      reportBiobränsle.EmailSettings.Message = mMeddelande;
      environmentBiobränsle.EmailSettings.Address = Settings.Default.MailSendFrom;
      environmentBiobränsle.EmailSettings.Name = Settings.Default.MailFullName;
      environmentBiobränsle.EmailSettings.Host = Settings.Default.MailSMTP;
      environmentBiobränsle.EmailSettings.Port = Settings.Default.MailSMTPPort;
      environmentBiobränsle.EmailSettings.UserName = Settings.Default.MailUsername;
      environmentBiobränsle.EmailSettings.Password = Settings.Default.MailPassword;
    }

    /// <summary>
    ///   Changes the language and restarts the application.
    /// </summary>
    /// <param name="aLanguage"> The language </param>
    private static void ChangeLanguage(string aLanguage)
    {
      if (Settings.Default.SettingsLanguage.Equals(aLanguage)) return;

      try
      {
        Settings.Default.SettingsLanguage = aLanguage;
        Application.Restart();
      }
      catch (NotSupportedException nse)
      {
#if !DEBUG
          MessageBox.Show("Applikationen gick inte att starta om, gör detta manuellt istället.");
#else
        MessageBox.Show(nse.ToString());
#endif
      }
    }

    /// <summary>
    ///   Gets the correct language from the language id string.
    /// </summary>
    /// <param name="aLangId"> Language id string </param>
    /// <returns> The language string </returns>
    private static string GetLanguage(string aLangId)
    {
      if (aLangId.Equals(Settings.Default.SprakSv))
      {
        return Resources.Svenska;
      }
      return aLangId.Equals(Settings.Default.SprakEn) ? Resources.Engelska : "Odefinerat språk";
    }


    private static string GetDsStr(string aTable, string aColumn, int aRowIndex, DataSet aDataSet)
    {
      try
      {
        if (aDataSet != null && aDataSet.Tables[aTable] != null)
        {
          if (aDataSet.Tables[aTable].Rows.Count > aRowIndex && aDataSet.Tables[aTable].Rows[aRowIndex][aColumn] != null)
          {
            return aDataSet.Tables[aTable].Rows[aRowIndex][aColumn].ToString();
          }
        }
      }
      catch (Exception)
      {
//ignore 
      }
      return string.Empty;
    }

    private void ProgressNumberOfDataDone()
    {
      var nrOfDataDone = 0;
      if (!GetDsStr("UserData", "Region").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("UserData", "Distrikt").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("UserData", "Ursprung").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("UserData", "Traktnr").Equals(string.Empty) &&
          !GetDsStr("UserData", "Traktnr").Equals(Resources.Noll))
        nrOfDataDone++;
      if (!GetDsStr("UserData", "Traktnamn").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("UserData", "Standort").Equals(string.Empty) &&
          !GetDsStr("UserData", "Standort").Equals(Resources.Noll))
        nrOfDataDone++;
      if (!GetDsStr("UserData", "Entreprenor").Equals(string.Empty) &&
          !GetDsStr("UserData", "Entreprenor").Equals(Resources.Noll))
        nrOfDataDone++;
      if (!GetDsStr("UserData", "Avstand").Equals(string.Empty) &&
          !GetDsStr("UserData", "Avstand").Equals(Resources.Noll))
        nrOfDataDone++;
      if (!GetDsStr("UserData", "BedomdVolym").Equals(string.Empty) &&
          !GetDsStr("UserData", "BedomdVolym").Equals(Resources.Noll))
        nrOfDataDone++;
      if (!GetDsStr("UserData", "Hygge").Equals(string.Empty) && !GetDsStr("UserData", "Hygge").Equals(Resources.Noll)
          ||
          (!GetDsStr("UserData", "Vagkant").Equals(string.Empty) &&
           !GetDsStr("UserData", "Vagkant").Equals(Resources.Noll))
          ||
          (!GetDsStr("UserData", "Aker").Equals(string.Empty) && !GetDsStr("UserData", "Aker").Equals(Resources.Noll)))
        nrOfDataDone++;

      if (!GetDsStr("UserData", "Avlagg1Norr").Equals(string.Empty) &&
          !GetDsStr("UserData", "Avlagg1Norr").Equals(Resources.Noll)
          ||
          !GetDsStr("UserData", "Avlagg1Ost").Equals(string.Empty) &&
          !GetDsStr("UserData", "Avlagg1Ost").Equals(Resources.Noll)
          ||
          !GetDsStr("UserData", "Avlagg2Norr").Equals(string.Empty) &&
          !GetDsStr("UserData", "Avlagg2Norr").Equals(Resources.Noll)
          ||
          !GetDsStr("UserData", "Avlagg2Ost").Equals(string.Empty) &&
          !GetDsStr("UserData", "Avlagg2Ost").Equals(Resources.Noll)
          ||
          !GetDsStr("UserData", "Avlagg3Norr").Equals(string.Empty) &&
          !GetDsStr("UserData", "Avlagg3Norr").Equals(Resources.Noll)
          ||
          !GetDsStr("UserData", "Avlagg3Ost").Equals(string.Empty) &&
          !GetDsStr("UserData", "Avlagg3Ost").Equals(Resources.Noll)
          ||
          !GetDsStr("UserData", "Avlagg4Norr").Equals(string.Empty) &&
          !GetDsStr("UserData", "Avlagg4Norr").Equals(Resources.Noll)
          ||
          !GetDsStr("UserData", "Avlagg4Ost").Equals(string.Empty) &&
          !GetDsStr("UserData", "Avlagg4Ost").Equals(Resources.Noll)
          ||
          !GetDsStr("UserData", "Avlagg5Norr").Equals(string.Empty) &&
          !GetDsStr("UserData", "Avlagg5Norr").Equals(Resources.Noll)
          ||
          !GetDsStr("UserData", "Avlagg5Ost").Equals(string.Empty) &&
          !GetDsStr("UserData", "Avlagg5Ost").Equals(Resources.Noll)
          ||
          !GetDsStr("UserData", "Avlagg6Norr").Equals(string.Empty) &&
          !GetDsStr("UserData", "Avlagg6Norr").Equals(Resources.Noll)
          ||
          !GetDsStr("UserData", "Avlagg6Ost").Equals(string.Empty) &&
          !GetDsStr("UserData", "Avlagg6Ost").Equals(Resources.Noll))
      {
        nrOfDataDone++;
      }

      //Natu-kulturmiljöhänsyn
      if (!GetDsStr("Fragor", "Fraga1").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("Fragor", "Fraga2").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("Fragor", "Fraga3").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("Fragor", "Fraga4").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("Fragor", "Fraga5").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("Fragor", "Fraga6").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("Fragor", "Fraga7").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("Fragor", "Fraga8").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("Fragor", "Fraga9").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("Fragor", "Fraga10").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("Fragor", "Fraga11").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("Fragor", "Fraga12").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("Fragor", "Fraga13").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("Fragor", "Fraga14").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("Fragor", "Fraga15").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("Fragor", "Fraga16").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("Fragor", "Fraga17").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("Fragor", "Fraga18").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("Fragor", "Fraga19").Equals(string.Empty))
        nrOfDataDone++;

      progressBarData.Value = nrOfDataDone;

      if (progressBarData.Value > 0 && progressBarData.Maximum > 0)
      {
        labelProgress.Text = String.Format("{0}% Klar",
                                           (int) ((progressBarData.Value/(float) progressBarData.Maximum)*100));
      }

      if (progressBarData.Value > 0 && progressBarData.Value == progressBarData.Maximum)
      {
        progressBarData.ForeColor = Color.Green;
      }
      else
      {
        progressBarData.ForeColor = SystemColors.Desktop;
      }
    }


    /// <summary>
    ///   Checks the combobox or textbox if it is focused in that case update the progressbar.
    /// </summary>
    /// <param name="aSender"> The sending object </param>
    private void ProgressFocusedObject(ref object aSender)
    {
      try
      {
        var control = aSender as Control;

        if (control != null && control.Focused)
        {
          ProgressNumberOfDataDone();
        }
      }
      catch (Exception ex)
      {
#if DEBUG
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    private void UpdateDistriktList(ref object sender)
    {
      var comboBox = sender as ComboBox;
      if (comboBox == null) return;
      comboBoxDistrikt.Items.Clear();
      if (comboBox.SelectedIndex < 0) return;
      var activeRegionId = dataSetSettings.Tables["Region"].Rows[comboBox.SelectedIndex]["Id"].ToString();
      foreach (
        var row in
          dataSetSettings.Tables["Distrikt"].Rows.Cast<DataRow>().Where(
            row => row["RegionId"].ToString().Equals(activeRegionId)))
      {
        comboBoxDistrikt.Items.Add(row["Namn"].ToString());
      }
    }

    private static bool CheckaPositivtHeltal(Control aTextBox)
    {
      var rValue = false;
      if (aTextBox != null && !aTextBox.Text.Equals(string.Empty))
      {
        if (aTextBox.Focused && DataSetParser.GetObjIntValue(aTextBox.Text) < 0)
        {
          MessageBox.Show(Resources.Ogiltigt_nummer, Resources.Ogiltigt_nummer, MessageBoxButtons.OK,
                          MessageBoxIcon.Information);
          aTextBox.Text = string.Empty;
        }
        else
        {
          rValue = true;
        }
      }
      return rValue;
    }

    private void comboBoxRegion_SelectedIndexChanged(object sender, EventArgs e)
    {
      UpdateDistriktList(ref sender);
      ProgressFocusedObject(ref sender);
    }

    private void comboBoxDistrikt_SelectedIndexChanged(object sender, EventArgs e)
    {
      ProgressFocusedObject(ref sender);
    }

    private void textBoxTraktnr_TextChanged(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box == null) return;
      if (CheckaPositivtHeltal(box))
      {
        ProgressFocusedObject(ref sender);
      }
    }

    private void textBoxTraktnamn_TextChanged(object sender, EventArgs e)
    {
      ProgressFocusedObject(ref sender);
    }

    private void comboBoxStandort_SelectedIndexChanged(object sender, EventArgs e)
    {
      ProgressFocusedObject(ref sender);
    }

    private void textBoxEntreprenor_TextChanged(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box == null) return;
      if (!CheckaPositivtHeltal(box)) return;
      ProgressFocusedObject(ref sender);
    }


    private void comboBoxUrsprung_SelectedIndexChanged(object sender, EventArgs e)
    {
      ProgressFocusedObject(ref sender);
    }

    private void SetDsStr(string aTable, string aColumn, string aText)
    {
      if (dataSetBiobränsle == null || dataSetBiobränsle.Tables[aTable] == null) return;
      if (dataSetBiobränsle.Tables[aTable].Rows.Count <= 0 || dataSetBiobränsle.Tables[aTable].Rows[0][aColumn] == null ||
          dataSetBiobränsle.Tables[aTable].Rows[0][aColumn].ToString().Equals(aText)) return;
      try
      {
        dataSetBiobränsle.Tables[aTable].Rows[0][aColumn] = aText;
      }
      catch (Exception)
      {
//ignore            
      }
    }

    private void SetSelectedDistriktId()
    {
      int index = 0, settingsIndex = 0;
      foreach (DataRow row in dataSetSettings.Tables["Distrikt"].Rows)
      {
        if (row["RegionId"].ToString().Equals(GetDsStr("Region", "Id", comboBoxRegion.SelectedIndex, dataSetSettings)))
        {
          if (comboBoxDistrikt.SelectedIndex == index)
          {
            SetDsStr("UserData", "DistriktId", GetDsStr("Distrikt", "Id", settingsIndex, dataSetSettings));
            break;
          }
          index++;
        }
        settingsIndex++;
      }
    }

    private static int GetDatumÅr(object aDatum)
    {
      if (aDatum != null && !aDatum.ToString().Equals(string.Empty))
      {
        DateTime datum;
        DateTime.TryParse(aDatum.ToString(), out datum);

        if (!datum.Equals(DateTime.MinValue))
        {
          return datum.Year;
        }
      }
      return 0;
    }

    private void dateTimePicker_ValueChanged(object sender, EventArgs e)
    {
      SetDsStr("UserData", "Datum", dateTimePicker.Text);
      var årtal = GetDatumÅr(dateTimePicker.Text);
      SetDsStr("UserData", "Artal", årtal.ToString(CultureInfo.InvariantCulture));
    }

    private void comboBoxDistrikt_SelectionChangeCommitted(object sender, EventArgs e)
    {
      SetDsStr("UserData", "Distrikt", comboBoxDistrikt.Text);
      SetSelectedDistriktId();
    }

    private void comboBoxRegion_SelectionChangeCommitted(object sender, EventArgs e)
    {
      SetDsStr("UserData", "Region", comboBoxRegion.Text);
      SetDsStr("UserData", "RegionId", GetDsStr("Region", "Id", comboBoxRegion.SelectedIndex, dataSetSettings));
    }

    private void comboBoxUrsprung_SelectionChangeCommitted(object sender, EventArgs e)
    {
      SetDsStr("UserData", "Ursprung", comboBoxUrsprung.Text);
    }

    private void comboBoxStandort_SelectionChangeCommitted(object sender, EventArgs e)
    {
      SetDsStr("UserData", "Standort", comboBoxStandort.Text);
    }

    private void textBoxAvstånd_TextChanged(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box == null) return;
      if (!CheckaPositivtHeltal(box)) return;
      SetDsStr("UserData", "Avstand", textBoxAvstånd.Text);
      ProgressFocusedObject(ref sender);
    }

    private void textBoxBedömdVolym_TextChanged(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box == null) return;
      if (!CheckaPositivtHeltal(box)) return;
      SetDsStr("UserData", "BedomdVolym", textBoxBedömdVolym.Text);
      ProgressFocusedObject(ref sender);
    }

    private void textBoxHygge_TextChanged(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box == null) return;
      if (!CheckaPositivtHeltal(box)) return;
      SetDsStr("UserData", "Hygge", textBoxHygge.Text);
      UpdateAntalVältor();
      ProgressFocusedObject(ref sender);
    }

    private void textBoxVägkant_TextChanged(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box == null) return;
      if (!CheckaPositivtHeltal(box)) return;
      SetDsStr("UserData", "Vagkant", textBoxVägkant.Text);
      UpdateAntalVältor();
      ProgressFocusedObject(ref sender);
    }

    private void textBoxÅker_TextChanged(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box == null) return;
      if (!CheckaPositivtHeltal(box)) return;
      SetDsStr("UserData", "Aker", textBoxÅker.Text);
      UpdateAntalVältor();
      ProgressFocusedObject(ref sender);
    }

    private void checkBoxLatbilshugg_CheckedChanged(object sender, EventArgs e)
    {
      SetDsStr("UserData", "Latbilshugg", checkBoxLatbilshugg.Checked.ToString(CultureInfo.InvariantCulture));
    }

    private void checkBoxGrotbil_CheckedChanged(object sender, EventArgs e)
    {
      SetDsStr("UserData", "Grotbil", checkBoxGrotbil.Checked.ToString(CultureInfo.InvariantCulture));
    }

    private void checkBoxTraktordragenHugg_CheckedChanged(object sender, EventArgs e)
    {
      SetDsStr("UserData", "TraktordragenHugg", checkBoxTraktordragenHugg.Checked.ToString(CultureInfo.InvariantCulture));
    }

    private void checkBoxSkotarburenHugg_CheckedChanged(object sender, EventArgs e)
    {
      SetDsStr("UserData", "SkotarburenHugg", checkBoxSkotarburenHugg.Checked.ToString(CultureInfo.InvariantCulture));
    }

    private void textBoxAvlagg1Norr_TextChanged(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box == null) return;
      ProgressFocusedObject(ref sender);
    }

    private void textBoxAvlagg1Ost_TextChanged(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box == null) return;
      ProgressFocusedObject(ref sender);
    }

    private void textBoxAvlagg2Norr_TextChanged(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box == null) return;
      ProgressFocusedObject(ref sender);
    }

    private void textBoxAvlagg2Ost_TextChanged(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box == null) return;
      ProgressFocusedObject(ref sender);
    }

    private void textBoxAvlagg3Norr_TextChanged(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box == null) return;
      ProgressFocusedObject(ref sender);
    }

    private void textBoxAvlagg3Ost_TextChanged(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box == null) return;
      ProgressFocusedObject(ref sender);
    }

    private void textBoxAvlagg4Norr_TextChanged(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box == null) return;
      ProgressFocusedObject(ref sender);
    }

    private void textBoxAvlagg4Ost_TextChanged(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box == null) return;
      ProgressFocusedObject(ref sender);
    }

    private void textBoxAvlagg5Norr_TextChanged(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box == null) return;
      ProgressFocusedObject(ref sender);
    }

    private void textBoxAvlagg5Ost_TextChanged(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box == null) return;
      ProgressFocusedObject(ref sender);
    }

    private void textBoxAvlagg6Norr_TextChanged(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box == null) return;
      ProgressFocusedObject(ref sender);
    }

    private void textBoxAvlagg6Ost_TextChanged(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box == null) return;
      ProgressFocusedObject(ref sender);
    }

    private void textBoxTraktnr_KeyDown(object sender, KeyEventArgs e)
    {
      DataHelper.AccepteraEndastHeltalSomKeyDown(e,false);
    }

    private void textBoxTraktnr_Leave(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box != null && box.Text.Trim().Equals(string.Empty))
      {
        box.Text = Resources.Noll;
      }
      SetDsStr("UserData", "Traktnr", textBoxTraktnr.Text);
    }

    private void textBoxEntreprenor_KeyDown(object sender, KeyEventArgs e)
    {
        DataHelper.AccepteraEndastHeltalSomKeyDown(e, false);
    }

    private void textBoxAntalVältor_KeyDown(object sender, KeyEventArgs e)
    {
        DataHelper.AccepteraEndastHeltalSomKeyDown(e, false);
    }

    private void textBoxAvstånd_KeyDown(object sender, KeyEventArgs e)
    {
        DataHelper.AccepteraEndastHeltalSomKeyDown(e, false);
    }

    private void textBoxBedömdVolym_KeyDown(object sender, KeyEventArgs e)
    {
        DataHelper.AccepteraEndastHeltalSomKeyDown(e, false);
    }

    private void textBoxHygge_KeyDown(object sender, KeyEventArgs e)
    {
        DataHelper.AccepteraEndastHeltalSomKeyDown(e, false);
    }

    private void textBoxVägkant_KeyDown(object sender, KeyEventArgs e)
    {
        DataHelper.AccepteraEndastHeltalSomKeyDown(e, false);
    }

    private void textBoxÅker_KeyDown(object sender, KeyEventArgs e)
    {
        DataHelper.AccepteraEndastHeltalSomKeyDown(e, false);
    }

    private void textBoxAvlagg1Norr_KeyDown(object sender, KeyEventArgs e)
    {
        DataHelper.AccepteraEndastHeltalSomKeyDown(e, false);
    }

    private void textBoxAvlagg1Ost_KeyDown(object sender, KeyEventArgs e)
    {
        DataHelper.AccepteraEndastHeltalSomKeyDown(e, false);
    }

    private void textBoxAvlagg2Norr_KeyDown(object sender, KeyEventArgs e)
    {
        DataHelper.AccepteraEndastHeltalSomKeyDown(e, false);
    }

    private void textBoxAvlagg2Ost_KeyDown(object sender, KeyEventArgs e)
    {
        DataHelper.AccepteraEndastHeltalSomKeyDown(e, false);
    }

    private void textBoxAvlagg3Norr_KeyDown(object sender, KeyEventArgs e)
    {
        DataHelper.AccepteraEndastHeltalSomKeyDown(e, false);
    }

    private void textBoxAvlagg3Ost_KeyDown(object sender, KeyEventArgs e)
    {
        DataHelper.AccepteraEndastHeltalSomKeyDown(e, false);
    }

    private void textBoxAvlagg4Norr_KeyDown(object sender, KeyEventArgs e)
    {
        DataHelper.AccepteraEndastHeltalSomKeyDown(e, false);
    }

    private void textBoxAvlagg4Ost_KeyDown(object sender, KeyEventArgs e)
    {
        DataHelper.AccepteraEndastHeltalSomKeyDown(e, false);
    }

    private void textBoxAvlagg5Norr_KeyDown(object sender, KeyEventArgs e)
    {
        DataHelper.AccepteraEndastHeltalSomKeyDown(e, false);
    }

    private void textBoxAvlagg5Ost_KeyDown(object sender, KeyEventArgs e)
    {
        DataHelper.AccepteraEndastHeltalSomKeyDown(e, false);
    }

    private void textBoxAvlagg6Norr_KeyDown(object sender, KeyEventArgs e)
    {
        DataHelper.AccepteraEndastHeltalSomKeyDown(e, false);
    }

    private void textBoxAvlagg6Ost_KeyDown(object sender, KeyEventArgs e)
    {
        DataHelper.AccepteraEndastHeltalSomKeyDown(e, false);
    }

    private void textBoxEntreprenor_Leave(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box != null && box.Text.Trim().Equals(string.Empty))
      {
        box.Text = Resources.Noll;
      }
    }


    private void UpdateAntalVältor()
    {
      var hygge = 0;
      if (DataSetParser.GetObjIntValue(GetDsStr("UserData", "Hygge")) > 0)
      {
        hygge = DataSetParser.GetObjIntValue(GetDsStr("UserData", "Hygge"));
      }
      var vagkant = 0;
      if (DataSetParser.GetObjIntValue(GetDsStr("UserData", "Vagkant")) > 0)
      {
        vagkant = DataSetParser.GetObjIntValue(GetDsStr("UserData", "Vagkant"));
      }
      var aker = 0;
      if (DataSetParser.GetObjIntValue(GetDsStr("UserData", "Aker")) > 0)
      {
        aker = DataSetParser.GetObjIntValue(GetDsStr("UserData", "Aker"));
      }
      var antalVältor = (hygge + vagkant + aker);
      SetDsStr("UserData", "AntalValtor", antalVältor.ToString(CultureInfo.InvariantCulture));
      textBoxAntalVältor.Text = antalVältor.ToString(CultureInfo.InvariantCulture);
    }


    private void StatusFrågor()
    {
      try
      {
        radioButton1Ja.Checked = GetDsStr("Fragor", "Fraga1").Equals("Ja");
        radioButton1Nej.Checked = GetDsStr("Fragor", "Fraga1").Equals("Nej");
        radioButton2Ja.Checked = GetDsStr("Fragor", "Fraga2").Equals("Ja");
        radioButton2Nej.Checked = GetDsStr("Fragor", "Fraga2").Equals("Nej");
        radioButton3Bra.Checked = GetDsStr("Fragor", "Fraga3").Equals("Bra");
        radioButton3Dålig.Checked = GetDsStr("Fragor", "Fraga3").Equals("Dålig");
        radioButton4Bra.Checked = GetDsStr("Fragor", "Fraga4").Equals("Bra");
        radioButton4Dålig.Checked = GetDsStr("Fragor", "Fraga4").Equals("Dålig");
        radioButton5Bra.Checked = GetDsStr("Fragor", "Fraga5").Equals("Bra");
        radioButton5Dålig.Checked = GetDsStr("Fragor", "Fraga5").Equals("Dålig");
        radioButton6Bra.Checked = GetDsStr("Fragor", "Fraga6").Equals("Bra");
        radioButton6Dålig.Checked = GetDsStr("Fragor", "Fraga6").Equals("Dålig");
        radioButton6Ejaktuellt.Checked = GetDsStr("Fragor", "Fraga6").Equals("Ej aktuellt");
        radioButton7Bra.Checked = GetDsStr("Fragor", "Fraga7").Equals("Bra");
        radioButton7Dålig.Checked = GetDsStr("Fragor", "Fraga7").Equals("Dålig");
        radioButton8Bra.Checked = GetDsStr("Fragor", "Fraga8").Equals("Bra");
        radioButton8Dålig.Checked = GetDsStr("Fragor", "Fraga8").Equals("Dålig");
        radioButton9Ja.Checked = GetDsStr("Fragor", "Fraga9").Equals("Ja");
        radioButton9Nej.Checked = GetDsStr("Fragor", "Fraga9").Equals("Nej");
        radioButton10Ja.Checked = GetDsStr("Fragor", "Fraga10").Equals("Ja");
        radioButton10Nej.Checked = GetDsStr("Fragor", "Fraga10").Equals("Nej");
        radioButton11Ja.Checked = GetDsStr("Fragor", "Fraga11").Equals("Ja");
        radioButton11Nej.Checked = GetDsStr("Fragor", "Fraga11").Equals("Nej");
        radioButton12Ja.Checked = GetDsStr("Fragor", "Fraga12").Equals("Ja");
        radioButton12Nej.Checked = GetDsStr("Fragor", "Fraga12").Equals("Nej");
        radioButton12Ejaktuellt.Checked = GetDsStr("Fragor", "Fraga12").Equals("Ej aktuellt");
        radioButton13Ja.Checked = GetDsStr("Fragor", "Fraga13").Equals("Ja");
        radioButton13Nej.Checked = GetDsStr("Fragor", "Fraga13").Equals("Nej");
        radioButton13Ejaktuellt.Checked = GetDsStr("Fragor", "Fraga13").Equals("Ej aktuellt");
        radioButton14Ja.Checked = GetDsStr("Fragor", "Fraga14").Equals("Ja");
        radioButton14Nej.Checked = GetDsStr("Fragor", "Fraga14").Equals("Nej");
        radioButton14Ejaktuellt.Checked = GetDsStr("Fragor", "Fraga14").Equals("Ej aktuellt");
        radioButton15Ja.Checked = GetDsStr("Fragor", "Fraga15").Equals("Ja");
        radioButton15Nej.Checked = GetDsStr("Fragor", "Fraga15").Equals("Nej");
        radioButton15Ejaktuellt.Checked = GetDsStr("Fragor", "Fraga15").Equals("Ej aktuellt");
        radioButton16Ja.Checked = GetDsStr("Fragor", "Fraga16").Equals("Ja");
        radioButton16Nej.Checked = GetDsStr("Fragor", "Fraga16").Equals("Nej");
        radioButton16Ejaktuellt.Checked = GetDsStr("Fragor", "Fraga16").Equals("Ej aktuellt");
        radioButton17Ja.Checked = GetDsStr("Fragor", "Fraga17").Equals("Ja");
        radioButton17Nej.Checked = GetDsStr("Fragor", "Fraga17").Equals("Nej");
        radioButton18Ja.Checked = GetDsStr("Fragor", "Fraga18").Equals("Ja");
        radioButton18Nej.Checked = GetDsStr("Fragor", "Fraga18").Equals("Nej");
        radioButton19Ja.Checked = GetDsStr("Fragor", "Fraga19").Equals("Ja");
        radioButton19Nej.Checked = GetDsStr("Fragor", "Fraga19").Equals("Nej");
      }
      catch (Exception ex)
      {
#if !DEBUG
        MessageBox.Show("Misslyckades med att uppdatera frågornas status.", "Status error", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    private void textBoxAvstånd_Leave(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box != null && box.Text.Trim().Equals(string.Empty))
      {
        box.Text = Resources.Noll;
      }
    }

    private void textBoxBedömdVolym_Leave(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box != null && box.Text.Trim().Equals(string.Empty))
      {
        box.Text = Resources.Noll;
      }
    }

    private void textBoxHygge_Leave(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box != null && box.Text.Trim().Equals(string.Empty))
      {
        box.Text = Resources.Noll;
      }
    }

    private void textBoxVägkant_Leave(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box != null && box.Text.Trim().Equals(string.Empty))
      {
        box.Text = Resources.Noll;
      }
    }

    private void textBoxÅker_Leave(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box != null && box.Text.Trim().Equals(string.Empty))
      {
        box.Text = Resources.Noll;
      }
    }

    private void SättRButtonSvar(object aSender, string aFraga)
    {
      try
      {
        var rbutton = aSender as RadioButton;
        if (rbutton == null || !rbutton.Checked) return;

        SetDsStr("Fragor", aFraga, rbutton.Text);
        ProgressFocusedObject(ref aSender);
      }
      catch (Exception ex)
      {
#if !DEBUG
        MessageBox.Show("Misslyckades med att hämta svaret från radiobutton.");
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    private void radioButton1Ja_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga1");
    }

    private void radioButton1Nej_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga1");
    }

    private void radioButton2Ja_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga2");
    }

    private void radioButton2Nej_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga2");
    }

    private void radioButton3Bra_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga3");
    }

    private void radioButton3Dålig_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga3");
    }

    private void radioButton4Bra_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga4");
    }

    private void radioButton4Dålig_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga4");
    }

    private void radioButton5Bra_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga5");
    }

    private void radioButton5Dålig_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga5");
    }

    private void radioButton6Bra_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga6");
    }

    private void radioButton6Dålig_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga6");
    }

    private void radioButton6Ejaktuellt_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga6");
    }

    private void radioButton7Bra_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga7");
    }

    private void radioButton7Dålig_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga7");
    }

    private void radioButton8Bra_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga8");
    }

    private void radioButton8Dålig_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga8");
    }

    private void radioButton9Ja_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga9");
    }

    private void radioButton9Nej_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga9");
    }

    private void radioButton10Ja_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga10");
    }

    private void radioButton10Nej_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga10");
    }

    private void radioButton11Ja_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga11");
    }

    private void radioButton11Nej_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga11");
    }

    private void radioButton12Ja_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga12");
    }

    private void radioButton12Nej_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga12");
    }

    private void radioButton13Ja_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga13");
    }

    private void radioButton13Nej_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga13");
    }

    private void radioButton14Ja_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga14");
    }

    private void radioButton14Nej_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga14");
    }

    private void radioButton15Ja_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga15");
    }

    private void radioButton15Nej_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga15");
    }

    private void radioButton16Ja_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga16");
    }

    private void radioButton16Nej_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga16");
    }

    private void radioButton17Ja_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga17");
    }

    private void radioButton17Nej_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga17");
    }

    private void radioButton18Ja_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga18");
    }

    private void radioButton18Nej_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga18");
    }

    private void radioButton19Ja_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga19");
    }

    private void radioButton19Nej_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga19");
    }

    private void radioButton12Ejaktuellt_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga12");
    }

    private void radioButton13Ejaktuellt_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga13");
    }

    private void radioButton14Ejaktuellt_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga14");
    }

    private void radioButton15Ejaktuellt_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga15");
    }

    private void radioButton16Ejaktuellt_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga16");
    }

    private void richTextBoxOvrigt_TextChanged(object sender, EventArgs e)
    {
      var box = sender as RichTextBox;
      if (box == null) return;
      ProgressNumberOfDataDone();
    }
  }
}