﻿namespace Egenuppfoljning.Applications
{
  partial class BiobränsleForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BiobränsleForm));
        FastReport.Design.DesignerSettings designerSettings1 = new FastReport.Design.DesignerSettings();
        FastReport.Design.DesignerRestrictions designerRestrictions1 = new FastReport.Design.DesignerRestrictions();
        FastReport.Export.Email.EmailSettings emailSettings1 = new FastReport.Export.Email.EmailSettings();
        FastReport.PreviewSettings previewSettings1 = new FastReport.PreviewSettings();
        FastReport.ReportSettings reportSettings1 = new FastReport.ReportSettings();
        this.dataSetBiobränsle = new System.Data.DataSet();
        this.dataTableUserData = new System.Data.DataTable();
        this.dataColumnRegionId = new System.Data.DataColumn();
        this.dataColumnRegion = new System.Data.DataColumn();
        this.dataColumnDistriktId = new System.Data.DataColumn();
        this.dataColumnDistrikt = new System.Data.DataColumn();
        this.dataColumnUrsprung = new System.Data.DataColumn();
        this.dataColumnDatum = new System.Data.DataColumn();
        this.dataColumnTraktnr = new System.Data.DataColumn();
        this.dataColumnTraktnamn = new System.Data.DataColumn();
        this.dataColumnStandort = new System.Data.DataColumn();
        this.dataColumnEntreprenor = new System.Data.DataColumn();
        this.dataColumnLanguage = new System.Data.DataColumn();
        this.dataColumnMailFrom = new System.Data.DataColumn();
        this.dataColumnStatus = new System.Data.DataColumn();
        this.dataColumnStatus_Datum = new System.Data.DataColumn();
        this.dataColumnÅrtal = new System.Data.DataColumn();
        this.dataColumnAntalValtor = new System.Data.DataColumn();
        this.dataColumnAvstand = new System.Data.DataColumn();
        this.dataColumnBedomdVolym = new System.Data.DataColumn();
        this.dataColumnHygge = new System.Data.DataColumn();
        this.dataColumnVagkant = new System.Data.DataColumn();
        this.dataColumnAker = new System.Data.DataColumn();
        this.dataColumnLatbilshugg = new System.Data.DataColumn();
        this.dataColumnGrotbil = new System.Data.DataColumn();
        this.dataColumnTraktordragenHugg = new System.Data.DataColumn();
        this.dataColumnSkotarburenHugg = new System.Data.DataColumn();
        this.dataColumnAvlagg1Norr = new System.Data.DataColumn();
        this.dataColumnAvlagg1Ost = new System.Data.DataColumn();
        this.dataColumnAvlagg2Norr = new System.Data.DataColumn();
        this.dataColumnAvlagg2Ost = new System.Data.DataColumn();
        this.dataColumnAvlagg3Norr = new System.Data.DataColumn();
        this.dataColumnAvlagg3Ost = new System.Data.DataColumn();
        this.dataColumnAvlagg4Norr = new System.Data.DataColumn();
        this.dataColumnAvlagg4Ost = new System.Data.DataColumn();
        this.dataColumnAvlagg5Norr = new System.Data.DataColumn();
        this.dataColumnAvlagg5Ost = new System.Data.DataColumn();
        this.dataColumnAvlagg6Norr = new System.Data.DataColumn();
        this.dataColumnAvlagg6Ost = new System.Data.DataColumn();
        this.dataTableFragor = new System.Data.DataTable();
        this.dataColumnFraga1 = new System.Data.DataColumn();
        this.dataColumnFraga2 = new System.Data.DataColumn();
        this.dataColumnFraga3 = new System.Data.DataColumn();
        this.dataColumnFraga4 = new System.Data.DataColumn();
        this.dataColumnFraga5 = new System.Data.DataColumn();
        this.dataColumnFraga6 = new System.Data.DataColumn();
        this.dataColumnFraga7 = new System.Data.DataColumn();
        this.dataColumnFraga8 = new System.Data.DataColumn();
        this.dataColumnFraga9 = new System.Data.DataColumn();
        this.dataColumnFraga10 = new System.Data.DataColumn();
        this.dataColumnFraga11 = new System.Data.DataColumn();
        this.dataColumnFraga12 = new System.Data.DataColumn();
        this.dataColumnFraga13 = new System.Data.DataColumn();
        this.dataColumnFraga14 = new System.Data.DataColumn();
        this.dataColumnFraga15 = new System.Data.DataColumn();
        this.dataColumnFraga16 = new System.Data.DataColumn();
        this.dataColumnFraga17 = new System.Data.DataColumn();
        this.dataColumnFraga18 = new System.Data.DataColumn();
        this.dataColumnFraga19 = new System.Data.DataColumn();
        this.dataColumnOvrigt = new System.Data.DataColumn();
        this.dataSetSettings = new System.Data.DataSet();
        this.dataTableRegion = new System.Data.DataTable();
        this.dataColumnSettingsRegionId = new System.Data.DataColumn();
        this.dataColumnSettingsRegionNamn = new System.Data.DataColumn();
        this.dataTableDistrikt = new System.Data.DataTable();
        this.dataColumnSettingsDistriktRegionId = new System.Data.DataColumn();
        this.dataColumnSettingsDistriktId = new System.Data.DataColumn();
        this.dataColumnSettingsDistriktNamn = new System.Data.DataColumn();
        this.dataTableStandort = new System.Data.DataTable();
        this.dataColumnSettingsStandortNamn = new System.Data.DataColumn();
        this.dataTableMarkberedningsmetod = new System.Data.DataTable();
        this.dataColumnSettingsMarkberedningsmetodNamn = new System.Data.DataColumn();
        this.dataTableUrsprung = new System.Data.DataTable();
        this.dataColumnSettingsUrsprungNamn = new System.Data.DataColumn();
        this.menuStripBiobränsle = new System.Windows.Forms.MenuStrip();
        this.arkivToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.sparaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.sparaSomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
        this.avslutaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.openReportFileDialog = new System.Windows.Forms.OpenFileDialog();
        this.tabControl1 = new System.Windows.Forms.TabControl();
        this.tabPageBiobränsle = new System.Windows.Forms.TabPage();
        this.groupBoxBiobränsle = new System.Windows.Forms.GroupBox();
        this.groupBox6 = new System.Windows.Forms.GroupBox();
        this.label18 = new System.Windows.Forms.Label();
        this.label17 = new System.Windows.Forms.Label();
        this.label16 = new System.Windows.Forms.Label();
        this.label15 = new System.Windows.Forms.Label();
        this.label13 = new System.Windows.Forms.Label();
        this.groupBox9 = new System.Windows.Forms.GroupBox();
        this.textBoxAvlagg6Ost = new System.Windows.Forms.TextBox();
        this.textBoxAvlagg5Ost = new System.Windows.Forms.TextBox();
        this.textBoxAvlagg4Ost = new System.Windows.Forms.TextBox();
        this.groupBox8 = new System.Windows.Forms.GroupBox();
        this.textBoxAvlagg3Ost = new System.Windows.Forms.TextBox();
        this.textBoxAvlagg2Ost = new System.Windows.Forms.TextBox();
        this.textBoxAvlagg1Ost = new System.Windows.Forms.TextBox();
        this.groupBox7 = new System.Windows.Forms.GroupBox();
        this.textBoxAvlagg3Norr = new System.Windows.Forms.TextBox();
        this.textBoxAvlagg2Norr = new System.Windows.Forms.TextBox();
        this.textBoxAvlagg1Norr = new System.Windows.Forms.TextBox();
        this.label14 = new System.Windows.Forms.Label();
        this.groupBox10 = new System.Windows.Forms.GroupBox();
        this.textBoxAvlagg6Norr = new System.Windows.Forms.TextBox();
        this.textBoxAvlagg5Norr = new System.Windows.Forms.TextBox();
        this.textBoxAvlagg4Norr = new System.Windows.Forms.TextBox();
        this.groupBox5 = new System.Windows.Forms.GroupBox();
        this.checkBoxTraktordragenHugg = new System.Windows.Forms.CheckBox();
        this.checkBoxLatbilshugg = new System.Windows.Forms.CheckBox();
        this.checkBoxGrotbil = new System.Windows.Forms.CheckBox();
        this.checkBoxSkotarburenHugg = new System.Windows.Forms.CheckBox();
        this.groupBox2 = new System.Windows.Forms.GroupBox();
        this.label9 = new System.Windows.Forms.Label();
        this.textBoxBedömdVolym = new System.Windows.Forms.TextBox();
        this.label4 = new System.Windows.Forms.Label();
        this.label5 = new System.Windows.Forms.Label();
        this.label6 = new System.Windows.Forms.Label();
        this.textBoxAntalVältor = new System.Windows.Forms.TextBox();
        this.textBoxAvstånd = new System.Windows.Forms.TextBox();
        this.groupBox1 = new System.Windows.Forms.GroupBox();
        this.textBoxHygge = new System.Windows.Forms.TextBox();
        this.label10 = new System.Windows.Forms.Label();
        this.label11 = new System.Windows.Forms.Label();
        this.label12 = new System.Windows.Forms.Label();
        this.textBoxVägkant = new System.Windows.Forms.TextBox();
        this.textBoxÅker = new System.Windows.Forms.TextBox();
        this.groupBoxTrakt = new System.Windows.Forms.GroupBox();
        this.textBoxTraktnr = new System.Windows.Forms.TextBox();
        this.textBoxTraktnamn = new System.Windows.Forms.TextBox();
        this.textBoxEntreprenor = new System.Windows.Forms.TextBox();
        this.comboBoxStandort = new System.Windows.Forms.ComboBox();
        this.labeEntreprenor = new System.Windows.Forms.Label();
        this.labelStandort = new System.Windows.Forms.Label();
        this.labelTraktnamn = new System.Windows.Forms.Label();
        this.labelTraktnr = new System.Windows.Forms.Label();
        this.groupBoxRegion = new System.Windows.Forms.GroupBox();
        this.comboBoxUrsprung = new System.Windows.Forms.ComboBox();
        this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
        this.comboBoxDistrikt = new System.Windows.Forms.ComboBox();
        this.comboBoxRegion = new System.Windows.Forms.ComboBox();
        this.labelDatum = new System.Windows.Forms.Label();
        this.labelUrsprung = new System.Windows.Forms.Label();
        this.labelDistrikt = new System.Windows.Forms.Label();
        this.labelRegion = new System.Windows.Forms.Label();
        this.tabPageFrågor1 = new System.Windows.Forms.TabPage();
        this.groupBox17 = new System.Windows.Forms.GroupBox();
        this.radioButton8Dålig = new System.Windows.Forms.RadioButton();
        this.radioButton8Bra = new System.Windows.Forms.RadioButton();
        this.label25 = new System.Windows.Forms.Label();
        this.groupBox16 = new System.Windows.Forms.GroupBox();
        this.radioButton7Dålig = new System.Windows.Forms.RadioButton();
        this.radioButton7Bra = new System.Windows.Forms.RadioButton();
        this.label24 = new System.Windows.Forms.Label();
        this.groupBox15 = new System.Windows.Forms.GroupBox();
        this.radioButton6Ejaktuellt = new System.Windows.Forms.RadioButton();
        this.radioButton6Dålig = new System.Windows.Forms.RadioButton();
        this.radioButton6Bra = new System.Windows.Forms.RadioButton();
        this.label23 = new System.Windows.Forms.Label();
        this.groupBox14 = new System.Windows.Forms.GroupBox();
        this.radioButton5Dålig = new System.Windows.Forms.RadioButton();
        this.radioButton5Bra = new System.Windows.Forms.RadioButton();
        this.label22 = new System.Windows.Forms.Label();
        this.groupBox13 = new System.Windows.Forms.GroupBox();
        this.radioButton4Dålig = new System.Windows.Forms.RadioButton();
        this.radioButton4Bra = new System.Windows.Forms.RadioButton();
        this.label21 = new System.Windows.Forms.Label();
        this.groupBox12 = new System.Windows.Forms.GroupBox();
        this.radioButton3Dålig = new System.Windows.Forms.RadioButton();
        this.radioButton3Bra = new System.Windows.Forms.RadioButton();
        this.label20 = new System.Windows.Forms.Label();
        this.label19 = new System.Windows.Forms.Label();
        this.label3 = new System.Windows.Forms.Label();
        this.groupBox4 = new System.Windows.Forms.GroupBox();
        this.radioButton2Nej = new System.Windows.Forms.RadioButton();
        this.radioButton2Ja = new System.Windows.Forms.RadioButton();
        this.label2 = new System.Windows.Forms.Label();
        this.groupBox3 = new System.Windows.Forms.GroupBox();
        this.radioButton1Nej = new System.Windows.Forms.RadioButton();
        this.radioButton1Ja = new System.Windows.Forms.RadioButton();
        this.label1 = new System.Windows.Forms.Label();
        this.tabPageFrågor2 = new System.Windows.Forms.TabPage();
        this.groupBox25 = new System.Windows.Forms.GroupBox();
        this.radioButton16Ejaktuellt = new System.Windows.Forms.RadioButton();
        this.radioButton16Nej = new System.Windows.Forms.RadioButton();
        this.radioButton16Ja = new System.Windows.Forms.RadioButton();
        this.label34 = new System.Windows.Forms.Label();
        this.groupBox24 = new System.Windows.Forms.GroupBox();
        this.radioButton15Ejaktuellt = new System.Windows.Forms.RadioButton();
        this.radioButton15Nej = new System.Windows.Forms.RadioButton();
        this.radioButton15Ja = new System.Windows.Forms.RadioButton();
        this.label33 = new System.Windows.Forms.Label();
        this.groupBox23 = new System.Windows.Forms.GroupBox();
        this.radioButton14Ejaktuellt = new System.Windows.Forms.RadioButton();
        this.radioButton14Nej = new System.Windows.Forms.RadioButton();
        this.radioButton14Ja = new System.Windows.Forms.RadioButton();
        this.label32 = new System.Windows.Forms.Label();
        this.groupBox22 = new System.Windows.Forms.GroupBox();
        this.radioButton13Ejaktuellt = new System.Windows.Forms.RadioButton();
        this.radioButton13Nej = new System.Windows.Forms.RadioButton();
        this.radioButton13Ja = new System.Windows.Forms.RadioButton();
        this.label31 = new System.Windows.Forms.Label();
        this.groupBox21 = new System.Windows.Forms.GroupBox();
        this.radioButton12Ejaktuellt = new System.Windows.Forms.RadioButton();
        this.radioButton12Nej = new System.Windows.Forms.RadioButton();
        this.radioButton12Ja = new System.Windows.Forms.RadioButton();
        this.label30 = new System.Windows.Forms.Label();
        this.groupBox20 = new System.Windows.Forms.GroupBox();
        this.radioButton11Nej = new System.Windows.Forms.RadioButton();
        this.radioButton11Ja = new System.Windows.Forms.RadioButton();
        this.label29 = new System.Windows.Forms.Label();
        this.groupBox19 = new System.Windows.Forms.GroupBox();
        this.radioButton10Nej = new System.Windows.Forms.RadioButton();
        this.radioButton10Ja = new System.Windows.Forms.RadioButton();
        this.label28 = new System.Windows.Forms.Label();
        this.groupBox18 = new System.Windows.Forms.GroupBox();
        this.radioButton9Nej = new System.Windows.Forms.RadioButton();
        this.radioButton9Ja = new System.Windows.Forms.RadioButton();
        this.label27 = new System.Windows.Forms.Label();
        this.label26 = new System.Windows.Forms.Label();
        this.tabPageFrågor3 = new System.Windows.Forms.TabPage();
        this.groupBox28 = new System.Windows.Forms.GroupBox();
        this.radioButton19Nej = new System.Windows.Forms.RadioButton();
        this.radioButton19Ja = new System.Windows.Forms.RadioButton();
        this.label37 = new System.Windows.Forms.Label();
        this.groupBox27 = new System.Windows.Forms.GroupBox();
        this.radioButton18Nej = new System.Windows.Forms.RadioButton();
        this.radioButton18Ja = new System.Windows.Forms.RadioButton();
        this.label36 = new System.Windows.Forms.Label();
        this.groupBox26 = new System.Windows.Forms.GroupBox();
        this.radioButton17Nej = new System.Windows.Forms.RadioButton();
        this.radioButton17Ja = new System.Windows.Forms.RadioButton();
        this.label35 = new System.Windows.Forms.Label();
        this.groupBoxOBS = new System.Windows.Forms.GroupBox();
        this.label7 = new System.Windows.Forms.Label();
        this.label8 = new System.Windows.Forms.Label();
        this.groupBox11 = new System.Windows.Forms.GroupBox();
        this.richTextBoxOvrigt = new System.Windows.Forms.RichTextBox();
        this.environmentBiobränsle = new FastReport.EnvironmentSettings();
        this.progressBarData = new System.Windows.Forms.ProgressBar();
        this.labelProgress = new System.Windows.Forms.Label();
        this.saveReportFolderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
        this.labelFragaObs = new System.Windows.Forms.Label();
        this.labelOBS = new System.Windows.Forms.Label();
        this.reportBiobränsle = new FastReport.Report();
        this.textBoxRegion = new System.Windows.Forms.TextBox();
        this.textBoxDistrikt = new System.Windows.Forms.TextBox();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetBiobränsle)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableUserData)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFragor)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetSettings)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableRegion)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableDistrikt)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableStandort)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableMarkberedningsmetod)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableUrsprung)).BeginInit();
        this.menuStripBiobränsle.SuspendLayout();
        this.tabControl1.SuspendLayout();
        this.tabPageBiobränsle.SuspendLayout();
        this.groupBoxBiobränsle.SuspendLayout();
        this.groupBox6.SuspendLayout();
        this.groupBox9.SuspendLayout();
        this.groupBox8.SuspendLayout();
        this.groupBox7.SuspendLayout();
        this.groupBox10.SuspendLayout();
        this.groupBox5.SuspendLayout();
        this.groupBox2.SuspendLayout();
        this.groupBox1.SuspendLayout();
        this.groupBoxTrakt.SuspendLayout();
        this.groupBoxRegion.SuspendLayout();
        this.tabPageFrågor1.SuspendLayout();
        this.groupBox17.SuspendLayout();
        this.groupBox16.SuspendLayout();
        this.groupBox15.SuspendLayout();
        this.groupBox14.SuspendLayout();
        this.groupBox13.SuspendLayout();
        this.groupBox12.SuspendLayout();
        this.groupBox4.SuspendLayout();
        this.groupBox3.SuspendLayout();
        this.tabPageFrågor2.SuspendLayout();
        this.groupBox25.SuspendLayout();
        this.groupBox24.SuspendLayout();
        this.groupBox23.SuspendLayout();
        this.groupBox22.SuspendLayout();
        this.groupBox21.SuspendLayout();
        this.groupBox20.SuspendLayout();
        this.groupBox19.SuspendLayout();
        this.groupBox18.SuspendLayout();
        this.tabPageFrågor3.SuspendLayout();
        this.groupBox28.SuspendLayout();
        this.groupBox27.SuspendLayout();
        this.groupBox26.SuspendLayout();
        this.groupBoxOBS.SuspendLayout();
        this.groupBox11.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.reportBiobränsle)).BeginInit();
        this.SuspendLayout();
        // 
        // dataSetBiobränsle
        // 
        this.dataSetBiobränsle.DataSetName = "DataSetBiobränsle";
        this.dataSetBiobränsle.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableUserData,
            this.dataTableFragor});
        // 
        // dataTableUserData
        // 
        this.dataTableUserData.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnRegionId,
            this.dataColumnRegion,
            this.dataColumnDistriktId,
            this.dataColumnDistrikt,
            this.dataColumnUrsprung,
            this.dataColumnDatum,
            this.dataColumnTraktnr,
            this.dataColumnTraktnamn,
            this.dataColumnStandort,
            this.dataColumnEntreprenor,
            this.dataColumnLanguage,
            this.dataColumnMailFrom,
            this.dataColumnStatus,
            this.dataColumnStatus_Datum,
            this.dataColumnÅrtal,
            this.dataColumnAntalValtor,
            this.dataColumnAvstand,
            this.dataColumnBedomdVolym,
            this.dataColumnHygge,
            this.dataColumnVagkant,
            this.dataColumnAker,
            this.dataColumnLatbilshugg,
            this.dataColumnGrotbil,
            this.dataColumnTraktordragenHugg,
            this.dataColumnSkotarburenHugg,
            this.dataColumnAvlagg1Norr,
            this.dataColumnAvlagg1Ost,
            this.dataColumnAvlagg2Norr,
            this.dataColumnAvlagg2Ost,
            this.dataColumnAvlagg3Norr,
            this.dataColumnAvlagg3Ost,
            this.dataColumnAvlagg4Norr,
            this.dataColumnAvlagg4Ost,
            this.dataColumnAvlagg5Norr,
            this.dataColumnAvlagg5Ost,
            this.dataColumnAvlagg6Norr,
            this.dataColumnAvlagg6Ost});
        this.dataTableUserData.TableName = "UserData";
        // 
        // dataColumnRegionId
        // 
        this.dataColumnRegionId.ColumnName = "RegionId";
        this.dataColumnRegionId.DataType = typeof(int);
        // 
        // dataColumnRegion
        // 
        this.dataColumnRegion.ColumnName = "Region";
        // 
        // dataColumnDistriktId
        // 
        this.dataColumnDistriktId.ColumnName = "DistriktId";
        this.dataColumnDistriktId.DataType = typeof(int);
        // 
        // dataColumnDistrikt
        // 
        this.dataColumnDistrikt.ColumnName = "Distrikt";
        // 
        // dataColumnUrsprung
        // 
        this.dataColumnUrsprung.ColumnName = "Ursprung";
        // 
        // dataColumnDatum
        // 
        this.dataColumnDatum.ColumnName = "Datum";
        this.dataColumnDatum.DataType = typeof(System.DateTime);
        // 
        // dataColumnTraktnr
        // 
        this.dataColumnTraktnr.ColumnName = "Traktnr";
        this.dataColumnTraktnr.DataType = typeof(int);
        // 
        // dataColumnTraktnamn
        // 
        this.dataColumnTraktnamn.ColumnName = "Traktnamn";
        // 
        // dataColumnStandort
        // 
        this.dataColumnStandort.ColumnName = "Standort";
        this.dataColumnStandort.DataType = typeof(int);
        // 
        // dataColumnEntreprenor
        // 
        this.dataColumnEntreprenor.ColumnName = "Entreprenor";
        this.dataColumnEntreprenor.DataType = typeof(int);
        // 
        // dataColumnLanguage
        // 
        this.dataColumnLanguage.ColumnName = "Language";
        // 
        // dataColumnMailFrom
        // 
        this.dataColumnMailFrom.ColumnName = "MailFrom";
        // 
        // dataColumnStatus
        // 
        this.dataColumnStatus.ColumnName = "Status";
        // 
        // dataColumnStatus_Datum
        // 
        this.dataColumnStatus_Datum.ColumnName = "Status_Datum";
        this.dataColumnStatus_Datum.DataType = typeof(System.DateTime);
        // 
        // dataColumnÅrtal
        // 
        this.dataColumnÅrtal.ColumnName = "Artal";
        this.dataColumnÅrtal.DataType = typeof(int);
        // 
        // dataColumnAntalValtor
        // 
        this.dataColumnAntalValtor.ColumnName = "AntalValtor";
        this.dataColumnAntalValtor.DataType = typeof(int);
        // 
        // dataColumnAvstand
        // 
        this.dataColumnAvstand.ColumnName = "Avstand";
        this.dataColumnAvstand.DataType = typeof(int);
        // 
        // dataColumnBedomdVolym
        // 
        this.dataColumnBedomdVolym.ColumnName = "BedomdVolym";
        this.dataColumnBedomdVolym.DataType = typeof(int);
        // 
        // dataColumnHygge
        // 
        this.dataColumnHygge.ColumnName = "Hygge";
        this.dataColumnHygge.DataType = typeof(int);
        // 
        // dataColumnVagkant
        // 
        this.dataColumnVagkant.ColumnName = "Vagkant";
        this.dataColumnVagkant.DataType = typeof(int);
        // 
        // dataColumnAker
        // 
        this.dataColumnAker.ColumnName = "Aker";
        this.dataColumnAker.DataType = typeof(int);
        // 
        // dataColumnLatbilshugg
        // 
        this.dataColumnLatbilshugg.ColumnName = "Latbilshugg";
        this.dataColumnLatbilshugg.DataType = typeof(bool);
        // 
        // dataColumnGrotbil
        // 
        this.dataColumnGrotbil.ColumnName = "Grotbil";
        this.dataColumnGrotbil.DataType = typeof(bool);
        // 
        // dataColumnTraktordragenHugg
        // 
        this.dataColumnTraktordragenHugg.ColumnName = "TraktordragenHugg";
        this.dataColumnTraktordragenHugg.DataType = typeof(bool);
        // 
        // dataColumnSkotarburenHugg
        // 
        this.dataColumnSkotarburenHugg.ColumnName = "SkotarburenHugg";
        this.dataColumnSkotarburenHugg.DataType = typeof(bool);
        // 
        // dataColumnAvlagg1Norr
        // 
        this.dataColumnAvlagg1Norr.ColumnName = "Avlagg1Norr";
        this.dataColumnAvlagg1Norr.DataType = typeof(int);
        // 
        // dataColumnAvlagg1Ost
        // 
        this.dataColumnAvlagg1Ost.ColumnName = "Avlagg1Ost";
        this.dataColumnAvlagg1Ost.DataType = typeof(int);
        // 
        // dataColumnAvlagg2Norr
        // 
        this.dataColumnAvlagg2Norr.ColumnName = "Avlagg2Norr";
        this.dataColumnAvlagg2Norr.DataType = typeof(int);
        // 
        // dataColumnAvlagg2Ost
        // 
        this.dataColumnAvlagg2Ost.ColumnName = "Avlagg2Ost";
        this.dataColumnAvlagg2Ost.DataType = typeof(int);
        // 
        // dataColumnAvlagg3Norr
        // 
        this.dataColumnAvlagg3Norr.ColumnName = "Avlagg3Norr";
        this.dataColumnAvlagg3Norr.DataType = typeof(int);
        // 
        // dataColumnAvlagg3Ost
        // 
        this.dataColumnAvlagg3Ost.ColumnName = "Avlagg3Ost";
        this.dataColumnAvlagg3Ost.DataType = typeof(int);
        // 
        // dataColumnAvlagg4Norr
        // 
        this.dataColumnAvlagg4Norr.ColumnName = "Avlagg4Norr";
        this.dataColumnAvlagg4Norr.DataType = typeof(int);
        // 
        // dataColumnAvlagg4Ost
        // 
        this.dataColumnAvlagg4Ost.ColumnName = "Avlagg4Ost";
        this.dataColumnAvlagg4Ost.DataType = typeof(int);
        // 
        // dataColumnAvlagg5Norr
        // 
        this.dataColumnAvlagg5Norr.ColumnName = "Avlagg5Norr";
        this.dataColumnAvlagg5Norr.DataType = typeof(int);
        // 
        // dataColumnAvlagg5Ost
        // 
        this.dataColumnAvlagg5Ost.ColumnName = "Avlagg5Ost";
        this.dataColumnAvlagg5Ost.DataType = typeof(int);
        // 
        // dataColumnAvlagg6Norr
        // 
        this.dataColumnAvlagg6Norr.ColumnName = "Avlagg6Norr";
        this.dataColumnAvlagg6Norr.DataType = typeof(int);
        // 
        // dataColumnAvlagg6Ost
        // 
        this.dataColumnAvlagg6Ost.ColumnName = "Avlagg6Ost";
        this.dataColumnAvlagg6Ost.DataType = typeof(int);
        // 
        // dataTableFragor
        // 
        this.dataTableFragor.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFraga1,
            this.dataColumnFraga2,
            this.dataColumnFraga3,
            this.dataColumnFraga4,
            this.dataColumnFraga5,
            this.dataColumnFraga6,
            this.dataColumnFraga7,
            this.dataColumnFraga8,
            this.dataColumnFraga9,
            this.dataColumnFraga10,
            this.dataColumnFraga11,
            this.dataColumnFraga12,
            this.dataColumnFraga13,
            this.dataColumnFraga14,
            this.dataColumnFraga15,
            this.dataColumnFraga16,
            this.dataColumnFraga17,
            this.dataColumnFraga18,
            this.dataColumnFraga19,
            this.dataColumnOvrigt});
        this.dataTableFragor.TableName = "Fragor";
        // 
        // dataColumnFraga1
        // 
        this.dataColumnFraga1.Caption = "Fraga1";
        this.dataColumnFraga1.ColumnName = "Fraga1";
        // 
        // dataColumnFraga2
        // 
        this.dataColumnFraga2.ColumnName = "Fraga2";
        // 
        // dataColumnFraga3
        // 
        this.dataColumnFraga3.ColumnName = "Fraga3";
        // 
        // dataColumnFraga4
        // 
        this.dataColumnFraga4.ColumnName = "Fraga4";
        // 
        // dataColumnFraga5
        // 
        this.dataColumnFraga5.ColumnName = "Fraga5";
        // 
        // dataColumnFraga6
        // 
        this.dataColumnFraga6.ColumnName = "Fraga6";
        // 
        // dataColumnFraga7
        // 
        this.dataColumnFraga7.ColumnName = "Fraga7";
        // 
        // dataColumnFraga8
        // 
        this.dataColumnFraga8.ColumnName = "Fraga8";
        // 
        // dataColumnFraga9
        // 
        this.dataColumnFraga9.ColumnName = "Fraga9";
        // 
        // dataColumnFraga10
        // 
        this.dataColumnFraga10.ColumnName = "Fraga10";
        // 
        // dataColumnFraga11
        // 
        this.dataColumnFraga11.ColumnName = "Fraga11";
        // 
        // dataColumnFraga12
        // 
        this.dataColumnFraga12.ColumnName = "Fraga12";
        // 
        // dataColumnFraga13
        // 
        this.dataColumnFraga13.ColumnName = "Fraga13";
        // 
        // dataColumnFraga14
        // 
        this.dataColumnFraga14.ColumnName = "Fraga14";
        // 
        // dataColumnFraga15
        // 
        this.dataColumnFraga15.ColumnName = "Fraga15";
        // 
        // dataColumnFraga16
        // 
        this.dataColumnFraga16.ColumnName = "Fraga16";
        // 
        // dataColumnFraga17
        // 
        this.dataColumnFraga17.ColumnName = "Fraga17";
        // 
        // dataColumnFraga18
        // 
        this.dataColumnFraga18.ColumnName = "Fraga18";
        // 
        // dataColumnFraga19
        // 
        this.dataColumnFraga19.ColumnName = "Fraga19";
        // 
        // dataColumnOvrigt
        // 
        this.dataColumnOvrigt.ColumnName = "Ovrigt";
        // 
        // dataSetSettings
        // 
        this.dataSetSettings.DataSetName = "DataSetSettings";
        this.dataSetSettings.Relations.AddRange(new System.Data.DataRelation[] {
            new System.Data.DataRelation("RelationRegionDistrikt", "Region", "Distrikt", new string[] {
                        "Id"}, new string[] {
                        "RegionId"}, false)});
        this.dataSetSettings.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableRegion,
            this.dataTableDistrikt,
            this.dataTableStandort,
            this.dataTableMarkberedningsmetod,
            this.dataTableUrsprung});
        // 
        // dataTableRegion
        // 
        this.dataTableRegion.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSettingsRegionId,
            this.dataColumnSettingsRegionNamn});
        this.dataTableRegion.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, false)});
        this.dataTableRegion.TableName = "Region";
        // 
        // dataColumnSettingsRegionId
        // 
        this.dataColumnSettingsRegionId.ColumnName = "Id";
        this.dataColumnSettingsRegionId.DataType = typeof(int);
        // 
        // dataColumnSettingsRegionNamn
        // 
        this.dataColumnSettingsRegionNamn.ColumnName = "Namn";
        // 
        // dataTableDistrikt
        // 
        this.dataTableDistrikt.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSettingsDistriktRegionId,
            this.dataColumnSettingsDistriktId,
            this.dataColumnSettingsDistriktNamn});
        this.dataTableDistrikt.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.ForeignKeyConstraint("RelationRegionDistrikt", "Region", new string[] {
                        "Id"}, new string[] {
                        "RegionId"}, System.Data.AcceptRejectRule.None, System.Data.Rule.Cascade, System.Data.Rule.Cascade)});
        this.dataTableDistrikt.TableName = "Distrikt";
        // 
        // dataColumnSettingsDistriktRegionId
        // 
        this.dataColumnSettingsDistriktRegionId.ColumnName = "RegionId";
        this.dataColumnSettingsDistriktRegionId.DataType = typeof(int);
        // 
        // dataColumnSettingsDistriktId
        // 
        this.dataColumnSettingsDistriktId.ColumnName = "Id";
        this.dataColumnSettingsDistriktId.DataType = typeof(int);
        // 
        // dataColumnSettingsDistriktNamn
        // 
        this.dataColumnSettingsDistriktNamn.ColumnName = "Namn";
        // 
        // dataTableStandort
        // 
        this.dataTableStandort.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSettingsStandortNamn});
        this.dataTableStandort.TableName = "Standort";
        // 
        // dataColumnSettingsStandortNamn
        // 
        this.dataColumnSettingsStandortNamn.ColumnName = "Id";
        this.dataColumnSettingsStandortNamn.DataType = typeof(int);
        // 
        // dataTableMarkberedningsmetod
        // 
        this.dataTableMarkberedningsmetod.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSettingsMarkberedningsmetodNamn});
        this.dataTableMarkberedningsmetod.TableName = "Markberedningsmetod";
        // 
        // dataColumnSettingsMarkberedningsmetodNamn
        // 
        this.dataColumnSettingsMarkberedningsmetodNamn.ColumnName = "Namn";
        // 
        // dataTableUrsprung
        // 
        this.dataTableUrsprung.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSettingsUrsprungNamn});
        this.dataTableUrsprung.TableName = "Ursprung";
        // 
        // dataColumnSettingsUrsprungNamn
        // 
        this.dataColumnSettingsUrsprungNamn.ColumnName = "Namn";
        // 
        // menuStripBiobränsle
        // 
        this.menuStripBiobränsle.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.arkivToolStripMenuItem});
        this.menuStripBiobränsle.Location = new System.Drawing.Point(0, 0);
        this.menuStripBiobränsle.Name = "menuStripBiobränsle";
        this.menuStripBiobränsle.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
        this.menuStripBiobränsle.Size = new System.Drawing.Size(571, 24);
        this.menuStripBiobränsle.TabIndex = 0;
        this.menuStripBiobränsle.Text = "menuStrip1";
        // 
        // arkivToolStripMenuItem
        // 
        this.arkivToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sparaToolStripMenuItem,
            this.sparaSomToolStripMenuItem,
            this.toolStripSeparator2,
            this.avslutaToolStripMenuItem});
        this.arkivToolStripMenuItem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.arkivToolStripMenuItem.Name = "arkivToolStripMenuItem";
        this.arkivToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
        this.arkivToolStripMenuItem.Text = global::Egenuppfoljning.Properties.Resources.Arkiv;
        // 
        // sparaToolStripMenuItem
        // 
        this.sparaToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("sparaToolStripMenuItem.Image")));
        this.sparaToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
        this.sparaToolStripMenuItem.Name = "sparaToolStripMenuItem";
        this.sparaToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
        this.sparaToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
        this.sparaToolStripMenuItem.Text = global::Egenuppfoljning.Properties.Resources.Spara;
        this.sparaToolStripMenuItem.Click += new System.EventHandler(this.sparaToolStripMenuItem_Click);
        // 
        // sparaSomToolStripMenuItem
        // 
        this.sparaSomToolStripMenuItem.Name = "sparaSomToolStripMenuItem";
        this.sparaSomToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
        this.sparaSomToolStripMenuItem.Text = global::Egenuppfoljning.Properties.Resources.SparaSom;
        this.sparaSomToolStripMenuItem.Click += new System.EventHandler(this.sparaSomToolStripMenuItem_Click);
        // 
        // toolStripSeparator2
        // 
        this.toolStripSeparator2.Name = "toolStripSeparator2";
        this.toolStripSeparator2.Size = new System.Drawing.Size(147, 6);
        // 
        // avslutaToolStripMenuItem
        // 
        this.avslutaToolStripMenuItem.Name = "avslutaToolStripMenuItem";
        this.avslutaToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
        this.avslutaToolStripMenuItem.Text = "Stäng";
        this.avslutaToolStripMenuItem.Click += new System.EventHandler(this.avslutaToolStripMenuItem_Click);
        // 
        // tabControl1
        // 
        this.tabControl1.Controls.Add(this.tabPageBiobränsle);
        this.tabControl1.Controls.Add(this.tabPageFrågor1);
        this.tabControl1.Controls.Add(this.tabPageFrågor2);
        this.tabControl1.Controls.Add(this.tabPageFrågor3);
        this.tabControl1.Location = new System.Drawing.Point(0, 27);
        this.tabControl1.Name = "tabControl1";
        this.tabControl1.SelectedIndex = 0;
        this.tabControl1.Size = new System.Drawing.Size(570, 493);
        this.tabControl1.TabIndex = 1;
        // 
        // tabPageBiobränsle
        // 
        this.tabPageBiobränsle.Controls.Add(this.groupBoxBiobränsle);
        this.tabPageBiobränsle.Controls.Add(this.groupBoxTrakt);
        this.tabPageBiobränsle.Controls.Add(this.groupBoxRegion);
        this.tabPageBiobränsle.Location = new System.Drawing.Point(4, 22);
        this.tabPageBiobränsle.Name = "tabPageBiobränsle";
        this.tabPageBiobränsle.Padding = new System.Windows.Forms.Padding(3);
        this.tabPageBiobränsle.Size = new System.Drawing.Size(562, 467);
        this.tabPageBiobränsle.TabIndex = 2;
        this.tabPageBiobränsle.Text = "Biobränsle";
        this.tabPageBiobränsle.UseVisualStyleBackColor = true;
        // 
        // groupBoxBiobränsle
        // 
        this.groupBoxBiobränsle.Controls.Add(this.groupBox6);
        this.groupBoxBiobränsle.Controls.Add(this.groupBox5);
        this.groupBoxBiobränsle.Controls.Add(this.groupBox2);
        this.groupBoxBiobränsle.Controls.Add(this.groupBox1);
        this.groupBoxBiobränsle.Location = new System.Drawing.Point(11, 132);
        this.groupBoxBiobränsle.Name = "groupBoxBiobränsle";
        this.groupBoxBiobränsle.Size = new System.Drawing.Size(541, 320);
        this.groupBoxBiobränsle.TabIndex = 2;
        this.groupBoxBiobränsle.TabStop = false;
        // 
        // groupBox6
        // 
        this.groupBox6.Controls.Add(this.label18);
        this.groupBox6.Controls.Add(this.label17);
        this.groupBox6.Controls.Add(this.label16);
        this.groupBox6.Controls.Add(this.label15);
        this.groupBox6.Controls.Add(this.label13);
        this.groupBox6.Controls.Add(this.groupBox9);
        this.groupBox6.Controls.Add(this.groupBox8);
        this.groupBox6.Controls.Add(this.groupBox7);
        this.groupBox6.Controls.Add(this.label14);
        this.groupBox6.Controls.Add(this.groupBox10);
        this.groupBox6.Location = new System.Drawing.Point(281, 76);
        this.groupBox6.Name = "groupBox6";
        this.groupBox6.Size = new System.Drawing.Size(260, 244);
        this.groupBox6.TabIndex = 3;
        this.groupBox6.TabStop = false;
        this.groupBox6.Text = "KOORDINATER";
        // 
        // label18
        // 
        this.label18.AutoSize = true;
        this.label18.Location = new System.Drawing.Point(18, 207);
        this.label18.Name = "label18";
        this.label18.Size = new System.Drawing.Size(56, 13);
        this.label18.TabIndex = 7;
        this.label18.Text = "Avlägg6:";
        // 
        // label17
        // 
        this.label17.AutoSize = true;
        this.label17.Location = new System.Drawing.Point(18, 180);
        this.label17.Name = "label17";
        this.label17.Size = new System.Drawing.Size(56, 13);
        this.label17.TabIndex = 6;
        this.label17.Text = "Avlägg5:";
        // 
        // label16
        // 
        this.label16.AutoSize = true;
        this.label16.Location = new System.Drawing.Point(18, 152);
        this.label16.Name = "label16";
        this.label16.Size = new System.Drawing.Size(56, 13);
        this.label16.TabIndex = 5;
        this.label16.Text = "Avlägg4:";
        // 
        // label15
        // 
        this.label15.AutoSize = true;
        this.label15.Location = new System.Drawing.Point(16, 95);
        this.label15.Name = "label15";
        this.label15.Size = new System.Drawing.Size(56, 13);
        this.label15.TabIndex = 2;
        this.label15.Text = "Avlägg3:";
        // 
        // label13
        // 
        this.label13.AutoSize = true;
        this.label13.Location = new System.Drawing.Point(16, 68);
        this.label13.Name = "label13";
        this.label13.Size = new System.Drawing.Size(56, 13);
        this.label13.TabIndex = 1;
        this.label13.Text = "Avlägg2:";
        // 
        // groupBox9
        // 
        this.groupBox9.Controls.Add(this.textBoxAvlagg6Ost);
        this.groupBox9.Controls.Add(this.textBoxAvlagg5Ost);
        this.groupBox9.Controls.Add(this.textBoxAvlagg4Ost);
        this.groupBox9.Location = new System.Drawing.Point(161, 129);
        this.groupBox9.Name = "groupBox9";
        this.groupBox9.Size = new System.Drawing.Size(79, 104);
        this.groupBox9.TabIndex = 9;
        this.groupBox9.TabStop = false;
        this.groupBox9.Text = "Ost";
        // 
        // textBoxAvlagg6Ost
        // 
        this.textBoxAvlagg6Ost.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetBiobränsle, "UserData.Avlagg6Ost", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "000 000"));
        this.textBoxAvlagg6Ost.Location = new System.Drawing.Point(8, 75);
        this.textBoxAvlagg6Ost.MaxLength = 6;
        this.textBoxAvlagg6Ost.Name = "textBoxAvlagg6Ost";
        this.textBoxAvlagg6Ost.Size = new System.Drawing.Size(63, 21);
        this.textBoxAvlagg6Ost.TabIndex = 2;
        this.textBoxAvlagg6Ost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        this.textBoxAvlagg6Ost.TextChanged += new System.EventHandler(this.textBoxAvlagg6Ost_TextChanged);
        this.textBoxAvlagg6Ost.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxAvlagg6Ost_KeyDown);
        // 
        // textBoxAvlagg5Ost
        // 
        this.textBoxAvlagg5Ost.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetBiobränsle, "UserData.Avlagg5Ost", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "000 000"));
        this.textBoxAvlagg5Ost.Location = new System.Drawing.Point(8, 48);
        this.textBoxAvlagg5Ost.MaxLength = 6;
        this.textBoxAvlagg5Ost.Name = "textBoxAvlagg5Ost";
        this.textBoxAvlagg5Ost.Size = new System.Drawing.Size(63, 21);
        this.textBoxAvlagg5Ost.TabIndex = 1;
        this.textBoxAvlagg5Ost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        this.textBoxAvlagg5Ost.TextChanged += new System.EventHandler(this.textBoxAvlagg5Ost_TextChanged);
        this.textBoxAvlagg5Ost.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxAvlagg5Ost_KeyDown);
        // 
        // textBoxAvlagg4Ost
        // 
        this.textBoxAvlagg4Ost.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetBiobränsle, "UserData.Avlagg4Ost", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "000 000"));
        this.textBoxAvlagg4Ost.Location = new System.Drawing.Point(8, 20);
        this.textBoxAvlagg4Ost.MaxLength = 6;
        this.textBoxAvlagg4Ost.Name = "textBoxAvlagg4Ost";
        this.textBoxAvlagg4Ost.Size = new System.Drawing.Size(63, 21);
        this.textBoxAvlagg4Ost.TabIndex = 0;
        this.textBoxAvlagg4Ost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        this.textBoxAvlagg4Ost.TextChanged += new System.EventHandler(this.textBoxAvlagg4Ost_TextChanged);
        this.textBoxAvlagg4Ost.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxAvlagg4Ost_KeyDown);
        // 
        // groupBox8
        // 
        this.groupBox8.Controls.Add(this.textBoxAvlagg3Ost);
        this.groupBox8.Controls.Add(this.textBoxAvlagg2Ost);
        this.groupBox8.Controls.Add(this.textBoxAvlagg1Ost);
        this.groupBox8.Location = new System.Drawing.Point(161, 17);
        this.groupBox8.Name = "groupBox8";
        this.groupBox8.Size = new System.Drawing.Size(79, 104);
        this.groupBox8.TabIndex = 4;
        this.groupBox8.TabStop = false;
        this.groupBox8.Text = "Ost";
        // 
        // textBoxAvlagg3Ost
        // 
        this.textBoxAvlagg3Ost.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetBiobränsle, "UserData.Avlagg3Ost", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "000 000"));
        this.textBoxAvlagg3Ost.Location = new System.Drawing.Point(8, 75);
        this.textBoxAvlagg3Ost.MaxLength = 6;
        this.textBoxAvlagg3Ost.Name = "textBoxAvlagg3Ost";
        this.textBoxAvlagg3Ost.Size = new System.Drawing.Size(63, 21);
        this.textBoxAvlagg3Ost.TabIndex = 2;
        this.textBoxAvlagg3Ost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        this.textBoxAvlagg3Ost.TextChanged += new System.EventHandler(this.textBoxAvlagg3Ost_TextChanged);
        this.textBoxAvlagg3Ost.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxAvlagg3Ost_KeyDown);
        // 
        // textBoxAvlagg2Ost
        // 
        this.textBoxAvlagg2Ost.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetBiobränsle, "UserData.Avlagg2Ost", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "000 000"));
        this.textBoxAvlagg2Ost.Location = new System.Drawing.Point(8, 48);
        this.textBoxAvlagg2Ost.MaxLength = 6;
        this.textBoxAvlagg2Ost.Name = "textBoxAvlagg2Ost";
        this.textBoxAvlagg2Ost.Size = new System.Drawing.Size(63, 21);
        this.textBoxAvlagg2Ost.TabIndex = 1;
        this.textBoxAvlagg2Ost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        this.textBoxAvlagg2Ost.TextChanged += new System.EventHandler(this.textBoxAvlagg2Ost_TextChanged);
        this.textBoxAvlagg2Ost.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxAvlagg2Ost_KeyDown);
        // 
        // textBoxAvlagg1Ost
        // 
        this.textBoxAvlagg1Ost.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetBiobränsle, "UserData.Avlagg1Ost", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "000 000"));
        this.textBoxAvlagg1Ost.Location = new System.Drawing.Point(8, 20);
        this.textBoxAvlagg1Ost.MaxLength = 6;
        this.textBoxAvlagg1Ost.Name = "textBoxAvlagg1Ost";
        this.textBoxAvlagg1Ost.Size = new System.Drawing.Size(63, 21);
        this.textBoxAvlagg1Ost.TabIndex = 0;
        this.textBoxAvlagg1Ost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        this.textBoxAvlagg1Ost.TextChanged += new System.EventHandler(this.textBoxAvlagg1Ost_TextChanged);
        this.textBoxAvlagg1Ost.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxAvlagg1Ost_KeyDown);
        // 
        // groupBox7
        // 
        this.groupBox7.Controls.Add(this.textBoxAvlagg3Norr);
        this.groupBox7.Controls.Add(this.textBoxAvlagg2Norr);
        this.groupBox7.Controls.Add(this.textBoxAvlagg1Norr);
        this.groupBox7.Location = new System.Drawing.Point(84, 17);
        this.groupBox7.Name = "groupBox7";
        this.groupBox7.Size = new System.Drawing.Size(79, 104);
        this.groupBox7.TabIndex = 3;
        this.groupBox7.TabStop = false;
        this.groupBox7.Text = "Norr";
        // 
        // textBoxAvlagg3Norr
        // 
        this.textBoxAvlagg3Norr.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetBiobränsle, "UserData.Avlagg3Norr", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "000 000"));
        this.textBoxAvlagg3Norr.Location = new System.Drawing.Point(8, 75);
        this.textBoxAvlagg3Norr.MaxLength = 6;
        this.textBoxAvlagg3Norr.Name = "textBoxAvlagg3Norr";
        this.textBoxAvlagg3Norr.Size = new System.Drawing.Size(63, 21);
        this.textBoxAvlagg3Norr.TabIndex = 2;
        this.textBoxAvlagg3Norr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        this.textBoxAvlagg3Norr.TextChanged += new System.EventHandler(this.textBoxAvlagg3Norr_TextChanged);
        this.textBoxAvlagg3Norr.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxAvlagg3Norr_KeyDown);
        // 
        // textBoxAvlagg2Norr
        // 
        this.textBoxAvlagg2Norr.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetBiobränsle, "UserData.Avlagg2Norr", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "000 000"));
        this.textBoxAvlagg2Norr.Location = new System.Drawing.Point(8, 48);
        this.textBoxAvlagg2Norr.MaxLength = 6;
        this.textBoxAvlagg2Norr.Name = "textBoxAvlagg2Norr";
        this.textBoxAvlagg2Norr.Size = new System.Drawing.Size(63, 21);
        this.textBoxAvlagg2Norr.TabIndex = 1;
        this.textBoxAvlagg2Norr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        this.textBoxAvlagg2Norr.TextChanged += new System.EventHandler(this.textBoxAvlagg2Norr_TextChanged);
        this.textBoxAvlagg2Norr.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxAvlagg2Norr_KeyDown);
        // 
        // textBoxAvlagg1Norr
        // 
        this.textBoxAvlagg1Norr.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetBiobränsle, "UserData.Avlagg1Norr", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "000 000"));
        this.textBoxAvlagg1Norr.Location = new System.Drawing.Point(8, 20);
        this.textBoxAvlagg1Norr.MaxLength = 6;
        this.textBoxAvlagg1Norr.Name = "textBoxAvlagg1Norr";
        this.textBoxAvlagg1Norr.Size = new System.Drawing.Size(63, 21);
        this.textBoxAvlagg1Norr.TabIndex = 0;
        this.textBoxAvlagg1Norr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        this.textBoxAvlagg1Norr.TextChanged += new System.EventHandler(this.textBoxAvlagg1Norr_TextChanged);
        this.textBoxAvlagg1Norr.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxAvlagg1Norr_KeyDown);
        // 
        // label14
        // 
        this.label14.AutoSize = true;
        this.label14.Location = new System.Drawing.Point(16, 43);
        this.label14.Name = "label14";
        this.label14.Size = new System.Drawing.Size(56, 13);
        this.label14.TabIndex = 0;
        this.label14.Text = "Avlägg1:";
        // 
        // groupBox10
        // 
        this.groupBox10.Controls.Add(this.textBoxAvlagg6Norr);
        this.groupBox10.Controls.Add(this.textBoxAvlagg5Norr);
        this.groupBox10.Controls.Add(this.textBoxAvlagg4Norr);
        this.groupBox10.Location = new System.Drawing.Point(84, 129);
        this.groupBox10.Name = "groupBox10";
        this.groupBox10.Size = new System.Drawing.Size(79, 104);
        this.groupBox10.TabIndex = 8;
        this.groupBox10.TabStop = false;
        this.groupBox10.Text = "Norr";
        // 
        // textBoxAvlagg6Norr
        // 
        this.textBoxAvlagg6Norr.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetBiobränsle, "UserData.Avlagg6Norr", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "000 000"));
        this.textBoxAvlagg6Norr.Location = new System.Drawing.Point(8, 75);
        this.textBoxAvlagg6Norr.MaxLength = 6;
        this.textBoxAvlagg6Norr.Name = "textBoxAvlagg6Norr";
        this.textBoxAvlagg6Norr.Size = new System.Drawing.Size(63, 21);
        this.textBoxAvlagg6Norr.TabIndex = 2;
        this.textBoxAvlagg6Norr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        this.textBoxAvlagg6Norr.TextChanged += new System.EventHandler(this.textBoxAvlagg6Norr_TextChanged);
        this.textBoxAvlagg6Norr.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxAvlagg6Norr_KeyDown);
        // 
        // textBoxAvlagg5Norr
        // 
        this.textBoxAvlagg5Norr.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetBiobränsle, "UserData.Avlagg5Norr", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "000 000"));
        this.textBoxAvlagg5Norr.Location = new System.Drawing.Point(8, 48);
        this.textBoxAvlagg5Norr.MaxLength = 6;
        this.textBoxAvlagg5Norr.Name = "textBoxAvlagg5Norr";
        this.textBoxAvlagg5Norr.Size = new System.Drawing.Size(63, 21);
        this.textBoxAvlagg5Norr.TabIndex = 1;
        this.textBoxAvlagg5Norr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        this.textBoxAvlagg5Norr.TextChanged += new System.EventHandler(this.textBoxAvlagg5Norr_TextChanged);
        this.textBoxAvlagg5Norr.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxAvlagg5Norr_KeyDown);
        // 
        // textBoxAvlagg4Norr
        // 
        this.textBoxAvlagg4Norr.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetBiobränsle, "UserData.Avlagg4Norr", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "000 000"));
        this.textBoxAvlagg4Norr.Location = new System.Drawing.Point(8, 20);
        this.textBoxAvlagg4Norr.MaxLength = 6;
        this.textBoxAvlagg4Norr.Name = "textBoxAvlagg4Norr";
        this.textBoxAvlagg4Norr.Size = new System.Drawing.Size(63, 21);
        this.textBoxAvlagg4Norr.TabIndex = 0;
        this.textBoxAvlagg4Norr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        this.textBoxAvlagg4Norr.TextChanged += new System.EventHandler(this.textBoxAvlagg4Norr_TextChanged);
        this.textBoxAvlagg4Norr.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxAvlagg4Norr_KeyDown);
        // 
        // groupBox5
        // 
        this.groupBox5.Controls.Add(this.checkBoxTraktordragenHugg);
        this.groupBox5.Controls.Add(this.checkBoxLatbilshugg);
        this.groupBox5.Controls.Add(this.checkBoxGrotbil);
        this.groupBox5.Controls.Add(this.checkBoxSkotarburenHugg);
        this.groupBox5.Location = new System.Drawing.Point(0, 76);
        this.groupBox5.Name = "groupBox5";
        this.groupBox5.Size = new System.Drawing.Size(283, 124);
        this.groupBox5.TabIndex = 2;
        this.groupBox5.TabStop = false;
        this.groupBox5.Text = "FLISNING/AVHÄMTNING";
        // 
        // checkBoxTraktordragenHugg
        // 
        this.checkBoxTraktordragenHugg.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
        this.checkBoxTraktordragenHugg.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.dataSetBiobränsle, "UserData.TraktordragenHugg", true));
        this.checkBoxTraktordragenHugg.Location = new System.Drawing.Point(30, 70);
        this.checkBoxTraktordragenHugg.Name = "checkBoxTraktordragenHugg";
        this.checkBoxTraktordragenHugg.Size = new System.Drawing.Size(210, 17);
        this.checkBoxTraktordragenHugg.TabIndex = 2;
        this.checkBoxTraktordragenHugg.Text = "Traktordragen hugg";
        this.checkBoxTraktordragenHugg.UseVisualStyleBackColor = true;
        this.checkBoxTraktordragenHugg.CheckedChanged += new System.EventHandler(this.checkBoxTraktordragenHugg_CheckedChanged);
        // 
        // checkBoxLatbilshugg
        // 
        this.checkBoxLatbilshugg.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
        this.checkBoxLatbilshugg.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.dataSetBiobränsle, "UserData.Latbilshugg", true));
        this.checkBoxLatbilshugg.Location = new System.Drawing.Point(30, 23);
        this.checkBoxLatbilshugg.Name = "checkBoxLatbilshugg";
        this.checkBoxLatbilshugg.Size = new System.Drawing.Size(210, 17);
        this.checkBoxLatbilshugg.TabIndex = 0;
        this.checkBoxLatbilshugg.Text = "Latbilshugg";
        this.checkBoxLatbilshugg.UseVisualStyleBackColor = true;
        this.checkBoxLatbilshugg.CheckedChanged += new System.EventHandler(this.checkBoxLatbilshugg_CheckedChanged);
        // 
        // checkBoxGrotbil
        // 
        this.checkBoxGrotbil.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
        this.checkBoxGrotbil.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.dataSetBiobränsle, "UserData.Grotbil", true));
        this.checkBoxGrotbil.Location = new System.Drawing.Point(30, 47);
        this.checkBoxGrotbil.Name = "checkBoxGrotbil";
        this.checkBoxGrotbil.Size = new System.Drawing.Size(210, 17);
        this.checkBoxGrotbil.TabIndex = 1;
        this.checkBoxGrotbil.Text = "Grotbil";
        this.checkBoxGrotbil.UseVisualStyleBackColor = true;
        this.checkBoxGrotbil.CheckedChanged += new System.EventHandler(this.checkBoxGrotbil_CheckedChanged);
        // 
        // checkBoxSkotarburenHugg
        // 
        this.checkBoxSkotarburenHugg.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
        this.checkBoxSkotarburenHugg.DataBindings.Add(new System.Windows.Forms.Binding("Checked", this.dataSetBiobränsle, "UserData.SkotarburenHugg", true));
        this.checkBoxSkotarburenHugg.Location = new System.Drawing.Point(30, 93);
        this.checkBoxSkotarburenHugg.Name = "checkBoxSkotarburenHugg";
        this.checkBoxSkotarburenHugg.Size = new System.Drawing.Size(210, 17);
        this.checkBoxSkotarburenHugg.TabIndex = 3;
        this.checkBoxSkotarburenHugg.Text = "Skotarburen hugg";
        this.checkBoxSkotarburenHugg.UseVisualStyleBackColor = true;
        this.checkBoxSkotarburenHugg.CheckedChanged += new System.EventHandler(this.checkBoxSkotarburenHugg_CheckedChanged);
        // 
        // groupBox2
        // 
        this.groupBox2.Controls.Add(this.label9);
        this.groupBox2.Controls.Add(this.textBoxBedömdVolym);
        this.groupBox2.Controls.Add(this.label4);
        this.groupBox2.Controls.Add(this.label5);
        this.groupBox2.Controls.Add(this.label6);
        this.groupBox2.Controls.Add(this.textBoxAntalVältor);
        this.groupBox2.Controls.Add(this.textBoxAvstånd);
        this.groupBox2.Location = new System.Drawing.Point(0, 5);
        this.groupBox2.Name = "groupBox2";
        this.groupBox2.Size = new System.Drawing.Size(283, 80);
        this.groupBox2.TabIndex = 0;
        this.groupBox2.TabStop = false;
        this.groupBox2.Text = "PRODUKTIONSRAPPORTERING";
        // 
        // label9
        // 
        this.label9.AutoSize = true;
        this.label9.Location = new System.Drawing.Point(235, 45);
        this.label9.Name = "label9";
        this.label9.Size = new System.Drawing.Size(35, 13);
        this.label9.TabIndex = 6;
        this.label9.Text = "MWh";
        // 
        // textBoxBedömdVolym
        // 
        this.textBoxBedömdVolym.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetBiobränsle, "UserData.BedomdVolym", true));
        this.textBoxBedömdVolym.Location = new System.Drawing.Point(166, 42);
        this.textBoxBedömdVolym.MaxLength = 4;
        this.textBoxBedömdVolym.Name = "textBoxBedömdVolym";
        this.textBoxBedömdVolym.Size = new System.Drawing.Size(63, 21);
        this.textBoxBedömdVolym.TabIndex = 5;
        this.textBoxBedömdVolym.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        this.textBoxBedömdVolym.TextChanged += new System.EventHandler(this.textBoxBedömdVolym_TextChanged);
        this.textBoxBedömdVolym.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxBedömdVolym_KeyDown);
        this.textBoxBedömdVolym.Leave += new System.EventHandler(this.textBoxBedömdVolym_Leave);
        // 
        // label4
        // 
        this.label4.AutoSize = true;
        this.label4.Location = new System.Drawing.Point(7, 20);
        this.label4.Name = "label4";
        this.label4.Size = new System.Drawing.Size(77, 13);
        this.label4.TabIndex = 0;
        this.label4.Text = "Antal vältor:";
        // 
        // label5
        // 
        this.label5.AutoSize = true;
        this.label5.Location = new System.Drawing.Point(85, 20);
        this.label5.Name = "label5";
        this.label5.Size = new System.Drawing.Size(57, 13);
        this.label5.TabIndex = 2;
        this.label5.Text = "Avstånd:";
        // 
        // label6
        // 
        this.label6.AutoSize = true;
        this.label6.Location = new System.Drawing.Point(163, 20);
        this.label6.Name = "label6";
        this.label6.Size = new System.Drawing.Size(112, 13);
        this.label6.TabIndex = 4;
        this.label6.Text = "Bed. energimängd:";
        // 
        // textBoxAntalVältor
        // 
        this.textBoxAntalVältor.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetBiobränsle, "UserData.AntalValtor", true));
        this.textBoxAntalVältor.Enabled = false;
        this.textBoxAntalVältor.Location = new System.Drawing.Point(9, 42);
        this.textBoxAntalVältor.MaxLength = 4;
        this.textBoxAntalVältor.Name = "textBoxAntalVältor";
        this.textBoxAntalVältor.Size = new System.Drawing.Size(63, 21);
        this.textBoxAntalVältor.TabIndex = 1;
        this.textBoxAntalVältor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        this.textBoxAntalVältor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxAntalVältor_KeyDown);
        // 
        // textBoxAvstånd
        // 
        this.textBoxAvstånd.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetBiobränsle, "UserData.Avstand", true));
        this.textBoxAvstånd.Location = new System.Drawing.Point(88, 42);
        this.textBoxAvstånd.MaxLength = 4;
        this.textBoxAvstånd.Name = "textBoxAvstånd";
        this.textBoxAvstånd.Size = new System.Drawing.Size(63, 21);
        this.textBoxAvstånd.TabIndex = 3;
        this.textBoxAvstånd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        this.textBoxAvstånd.TextChanged += new System.EventHandler(this.textBoxAvstånd_TextChanged);
        this.textBoxAvstånd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxAvstånd_KeyDown);
        this.textBoxAvstånd.Leave += new System.EventHandler(this.textBoxAvstånd_Leave);
        // 
        // groupBox1
        // 
        this.groupBox1.Controls.Add(this.textBoxHygge);
        this.groupBox1.Controls.Add(this.label10);
        this.groupBox1.Controls.Add(this.label11);
        this.groupBox1.Controls.Add(this.label12);
        this.groupBox1.Controls.Add(this.textBoxVägkant);
        this.groupBox1.Controls.Add(this.textBoxÅker);
        this.groupBox1.Location = new System.Drawing.Point(281, 5);
        this.groupBox1.Name = "groupBox1";
        this.groupBox1.Size = new System.Drawing.Size(260, 80);
        this.groupBox1.TabIndex = 1;
        this.groupBox1.TabStop = false;
        this.groupBox1.Text = "VÄLTORS PLACERING";
        // 
        // textBoxHygge
        // 
        this.textBoxHygge.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetBiobränsle, "UserData.Hygge", true));
        this.textBoxHygge.Location = new System.Drawing.Point(11, 42);
        this.textBoxHygge.MaxLength = 2;
        this.textBoxHygge.Name = "textBoxHygge";
        this.textBoxHygge.Size = new System.Drawing.Size(63, 21);
        this.textBoxHygge.TabIndex = 1;
        this.textBoxHygge.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        this.textBoxHygge.TextChanged += new System.EventHandler(this.textBoxHygge_TextChanged);
        this.textBoxHygge.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxHygge_KeyDown);
        this.textBoxHygge.Leave += new System.EventHandler(this.textBoxHygge_Leave);
        // 
        // label10
        // 
        this.label10.AutoSize = true;
        this.label10.Location = new System.Drawing.Point(9, 20);
        this.label10.Name = "label10";
        this.label10.Size = new System.Drawing.Size(46, 13);
        this.label10.TabIndex = 0;
        this.label10.Text = "Hygge:";
        // 
        // label11
        // 
        this.label11.AutoSize = true;
        this.label11.Location = new System.Drawing.Point(93, 20);
        this.label11.Name = "label11";
        this.label11.Size = new System.Drawing.Size(57, 13);
        this.label11.TabIndex = 2;
        this.label11.Text = "Vägkant:";
        // 
        // label12
        // 
        this.label12.AutoSize = true;
        this.label12.Location = new System.Drawing.Point(179, 20);
        this.label12.Name = "label12";
        this.label12.Size = new System.Drawing.Size(37, 13);
        this.label12.TabIndex = 4;
        this.label12.Text = "Åker:";
        // 
        // textBoxVägkant
        // 
        this.textBoxVägkant.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetBiobränsle, "UserData.Vagkant", true));
        this.textBoxVägkant.Location = new System.Drawing.Point(96, 42);
        this.textBoxVägkant.MaxLength = 2;
        this.textBoxVägkant.Name = "textBoxVägkant";
        this.textBoxVägkant.Size = new System.Drawing.Size(63, 21);
        this.textBoxVägkant.TabIndex = 3;
        this.textBoxVägkant.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        this.textBoxVägkant.TextChanged += new System.EventHandler(this.textBoxVägkant_TextChanged);
        this.textBoxVägkant.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxVägkant_KeyDown);
        this.textBoxVägkant.Leave += new System.EventHandler(this.textBoxVägkant_Leave);
        // 
        // textBoxÅker
        // 
        this.textBoxÅker.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetBiobränsle, "UserData.Aker", true));
        this.textBoxÅker.Location = new System.Drawing.Point(182, 42);
        this.textBoxÅker.MaxLength = 2;
        this.textBoxÅker.Name = "textBoxÅker";
        this.textBoxÅker.Size = new System.Drawing.Size(63, 21);
        this.textBoxÅker.TabIndex = 5;
        this.textBoxÅker.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        this.textBoxÅker.TextChanged += new System.EventHandler(this.textBoxÅker_TextChanged);
        this.textBoxÅker.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxÅker_KeyDown);
        this.textBoxÅker.Leave += new System.EventHandler(this.textBoxÅker_Leave);
        // 
        // groupBoxTrakt
        // 
        this.groupBoxTrakt.Controls.Add(this.textBoxTraktnr);
        this.groupBoxTrakt.Controls.Add(this.textBoxTraktnamn);
        this.groupBoxTrakt.Controls.Add(this.textBoxEntreprenor);
        this.groupBoxTrakt.Controls.Add(this.comboBoxStandort);
        this.groupBoxTrakt.Controls.Add(this.labeEntreprenor);
        this.groupBoxTrakt.Controls.Add(this.labelStandort);
        this.groupBoxTrakt.Controls.Add(this.labelTraktnamn);
        this.groupBoxTrakt.Controls.Add(this.labelTraktnr);
        this.groupBoxTrakt.Location = new System.Drawing.Point(12, 66);
        this.groupBoxTrakt.Name = "groupBoxTrakt";
        this.groupBoxTrakt.Size = new System.Drawing.Size(540, 72);
        this.groupBoxTrakt.TabIndex = 1;
        this.groupBoxTrakt.TabStop = false;
        // 
        // textBoxTraktnr
        // 
        this.textBoxTraktnr.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetBiobränsle, "UserData.Traktnr", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, null, "000000"));
        this.textBoxTraktnr.Location = new System.Drawing.Point(15, 29);
        this.textBoxTraktnr.MaxLength = 6;
        this.textBoxTraktnr.Name = "textBoxTraktnr";
        this.textBoxTraktnr.Size = new System.Drawing.Size(116, 21);
        this.textBoxTraktnr.TabIndex = 1;
        this.textBoxTraktnr.TextChanged += new System.EventHandler(this.textBoxTraktnr_TextChanged);
        this.textBoxTraktnr.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxTraktnr_KeyDown);
        this.textBoxTraktnr.Leave += new System.EventHandler(this.textBoxTraktnr_Leave);
        // 
        // textBoxTraktnamn
        // 
        this.textBoxTraktnamn.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetBiobränsle, "UserData.Traktnamn", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.textBoxTraktnamn.Location = new System.Drawing.Point(140, 30);
        this.textBoxTraktnamn.MaxLength = 31;
        this.textBoxTraktnamn.Name = "textBoxTraktnamn";
        this.textBoxTraktnamn.Size = new System.Drawing.Size(116, 21);
        this.textBoxTraktnamn.TabIndex = 3;
        this.textBoxTraktnamn.TextChanged += new System.EventHandler(this.textBoxTraktnamn_TextChanged);
        // 
        // textBoxEntreprenor
        // 
        this.textBoxEntreprenor.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetBiobränsle, "UserData.Entreprenor", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, null, "0000"));
        this.textBoxEntreprenor.Location = new System.Drawing.Point(390, 29);
        this.textBoxEntreprenor.MaxLength = 4;
        this.textBoxEntreprenor.Name = "textBoxEntreprenor";
        this.textBoxEntreprenor.Size = new System.Drawing.Size(116, 21);
        this.textBoxEntreprenor.TabIndex = 7;
        this.textBoxEntreprenor.TextChanged += new System.EventHandler(this.textBoxEntreprenor_TextChanged);
        this.textBoxEntreprenor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxEntreprenor_KeyDown);
        this.textBoxEntreprenor.Leave += new System.EventHandler(this.textBoxEntreprenor_Leave);
        // 
        // comboBoxStandort
        // 
        this.comboBoxStandort.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetBiobränsle, "UserData.Standort", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.comboBoxStandort.DataSource = this.dataSetSettings;
        this.comboBoxStandort.DisplayMember = "Standort.Id";
        this.comboBoxStandort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxStandort.FormattingEnabled = true;
        this.comboBoxStandort.Location = new System.Drawing.Point(265, 30);
        this.comboBoxStandort.Name = "comboBoxStandort";
        this.comboBoxStandort.Size = new System.Drawing.Size(116, 21);
        this.comboBoxStandort.TabIndex = 5;
        this.comboBoxStandort.SelectionChangeCommitted += new System.EventHandler(this.comboBoxStandort_SelectionChangeCommitted);
        this.comboBoxStandort.SelectedIndexChanged += new System.EventHandler(this.comboBoxStandort_SelectedIndexChanged);
        // 
        // labeEntreprenor
        // 
        this.labeEntreprenor.AutoSize = true;
        this.labeEntreprenor.Location = new System.Drawing.Point(387, 14);
        this.labeEntreprenor.Name = "labeEntreprenor";
        this.labeEntreprenor.Size = new System.Drawing.Size(145, 13);
        this.labeEntreprenor.TabIndex = 6;
        this.labeEntreprenor.Text = "Maskinnr/Entreprenörnr";
        // 
        // labelStandort
        // 
        this.labelStandort.AutoSize = true;
        this.labelStandort.Location = new System.Drawing.Point(262, 14);
        this.labelStandort.Name = "labelStandort";
        this.labelStandort.Size = new System.Drawing.Size(57, 13);
        this.labelStandort.TabIndex = 4;
        this.labelStandort.Text = "Ståndort";
        // 
        // labelTraktnamn
        // 
        this.labelTraktnamn.AutoSize = true;
        this.labelTraktnamn.Location = new System.Drawing.Point(140, 14);
        this.labelTraktnamn.Name = "labelTraktnamn";
        this.labelTraktnamn.Size = new System.Drawing.Size(70, 13);
        this.labelTraktnamn.TabIndex = 2;
        this.labelTraktnamn.Text = "Traktnamn";
        // 
        // labelTraktnr
        // 
        this.labelTraktnr.AutoSize = true;
        this.labelTraktnr.Location = new System.Drawing.Point(13, 14);
        this.labelTraktnr.Name = "labelTraktnr";
        this.labelTraktnr.Size = new System.Drawing.Size(50, 13);
        this.labelTraktnr.TabIndex = 0;
        this.labelTraktnr.Text = "Traktnr";
        // 
        // groupBoxRegion
        // 
        this.groupBoxRegion.Controls.Add(this.textBoxDistrikt);
        this.groupBoxRegion.Controls.Add(this.textBoxRegion);
        this.groupBoxRegion.Controls.Add(this.comboBoxUrsprung);
        this.groupBoxRegion.Controls.Add(this.dateTimePicker);
        this.groupBoxRegion.Controls.Add(this.comboBoxDistrikt);
        this.groupBoxRegion.Controls.Add(this.comboBoxRegion);
        this.groupBoxRegion.Controls.Add(this.labelDatum);
        this.groupBoxRegion.Controls.Add(this.labelUrsprung);
        this.groupBoxRegion.Controls.Add(this.labelDistrikt);
        this.groupBoxRegion.Controls.Add(this.labelRegion);
        this.groupBoxRegion.Location = new System.Drawing.Point(12, 6);
        this.groupBoxRegion.Name = "groupBoxRegion";
        this.groupBoxRegion.Size = new System.Drawing.Size(540, 72);
        this.groupBoxRegion.TabIndex = 0;
        this.groupBoxRegion.TabStop = false;
        // 
        // comboBoxUrsprung
        // 
        this.comboBoxUrsprung.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetBiobränsle, "UserData.Ursprung", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.comboBoxUrsprung.DataSource = this.dataSetSettings;
        this.comboBoxUrsprung.DisplayMember = "Ursprung.Namn";
        this.comboBoxUrsprung.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxUrsprung.FormattingEnabled = true;
        this.comboBoxUrsprung.Location = new System.Drawing.Point(267, 30);
        this.comboBoxUrsprung.Name = "comboBoxUrsprung";
        this.comboBoxUrsprung.Size = new System.Drawing.Size(116, 21);
        this.comboBoxUrsprung.TabIndex = 5;
        this.comboBoxUrsprung.SelectionChangeCommitted += new System.EventHandler(this.comboBoxUrsprung_SelectionChangeCommitted);
        this.comboBoxUrsprung.SelectedIndexChanged += new System.EventHandler(this.comboBoxUrsprung_SelectedIndexChanged);
        // 
        // dateTimePicker
        // 
        this.dateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.dataSetBiobränsle, "UserData.Datum", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.dateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
        this.dateTimePicker.Location = new System.Drawing.Point(391, 31);
        this.dateTimePicker.Name = "dateTimePicker";
        this.dateTimePicker.Size = new System.Drawing.Size(116, 21);
        this.dateTimePicker.TabIndex = 7;
        this.dateTimePicker.ValueChanged += new System.EventHandler(this.dateTimePicker_ValueChanged);
        // 
        // comboBoxDistrikt
        // 
        this.comboBoxDistrikt.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetBiobränsle, "UserData.Distrikt", true));
        this.comboBoxDistrikt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxDistrikt.FormattingEnabled = true;
        this.comboBoxDistrikt.Location = new System.Drawing.Point(140, 30);
        this.comboBoxDistrikt.Name = "comboBoxDistrikt";
        this.comboBoxDistrikt.Size = new System.Drawing.Size(116, 21);
        this.comboBoxDistrikt.TabIndex = 3;
        this.comboBoxDistrikt.SelectionChangeCommitted += new System.EventHandler(this.comboBoxDistrikt_SelectionChangeCommitted);
        this.comboBoxDistrikt.SelectedIndexChanged += new System.EventHandler(this.comboBoxDistrikt_SelectedIndexChanged);
        // 
        // comboBoxRegion
        // 
        this.comboBoxRegion.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetBiobränsle, "UserData.Region", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.comboBoxRegion.DataSource = this.dataSetSettings;
        this.comboBoxRegion.DisplayMember = "Region.Namn";
        this.comboBoxRegion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxRegion.FormattingEnabled = true;
        this.comboBoxRegion.Location = new System.Drawing.Point(16, 30);
        this.comboBoxRegion.Name = "comboBoxRegion";
        this.comboBoxRegion.Size = new System.Drawing.Size(116, 21);
        this.comboBoxRegion.TabIndex = 1;
        this.comboBoxRegion.ValueMember = "Region.Id";
        this.comboBoxRegion.SelectionChangeCommitted += new System.EventHandler(this.comboBoxRegion_SelectionChangeCommitted);
        this.comboBoxRegion.SelectedIndexChanged += new System.EventHandler(this.comboBoxRegion_SelectedIndexChanged);
        // 
        // labelDatum
        // 
        this.labelDatum.AutoSize = true;
        this.labelDatum.Location = new System.Drawing.Point(387, 14);
        this.labelDatum.Name = "labelDatum";
        this.labelDatum.Size = new System.Drawing.Size(45, 13);
        this.labelDatum.TabIndex = 6;
        this.labelDatum.Text = "Datum";
        // 
        // labelUrsprung
        // 
        this.labelUrsprung.AutoSize = true;
        this.labelUrsprung.Location = new System.Drawing.Point(264, 14);
        this.labelUrsprung.Name = "labelUrsprung";
        this.labelUrsprung.Size = new System.Drawing.Size(59, 13);
        this.labelUrsprung.TabIndex = 4;
        this.labelUrsprung.Text = "Ursprung";
        // 
        // labelDistrikt
        // 
        this.labelDistrikt.AutoSize = true;
        this.labelDistrikt.Location = new System.Drawing.Point(140, 14);
        this.labelDistrikt.Name = "labelDistrikt";
        this.labelDistrikt.Size = new System.Drawing.Size(49, 13);
        this.labelDistrikt.TabIndex = 2;
        this.labelDistrikt.Text = "Distrikt";
        // 
        // labelRegion
        // 
        this.labelRegion.AutoSize = true;
        this.labelRegion.Location = new System.Drawing.Point(13, 14);
        this.labelRegion.Name = "labelRegion";
        this.labelRegion.Size = new System.Drawing.Size(46, 13);
        this.labelRegion.TabIndex = 0;
        this.labelRegion.Text = "Region";
        // 
        // tabPageFrågor1
        // 
        this.tabPageFrågor1.Controls.Add(this.groupBox17);
        this.tabPageFrågor1.Controls.Add(this.groupBox16);
        this.tabPageFrågor1.Controls.Add(this.groupBox15);
        this.tabPageFrågor1.Controls.Add(this.groupBox14);
        this.tabPageFrågor1.Controls.Add(this.groupBox13);
        this.tabPageFrågor1.Controls.Add(this.groupBox12);
        this.tabPageFrågor1.Controls.Add(this.label19);
        this.tabPageFrågor1.Controls.Add(this.label3);
        this.tabPageFrågor1.Controls.Add(this.groupBox4);
        this.tabPageFrågor1.Controls.Add(this.groupBox3);
        this.tabPageFrågor1.Location = new System.Drawing.Point(4, 22);
        this.tabPageFrågor1.Name = "tabPageFrågor1";
        this.tabPageFrågor1.Padding = new System.Windows.Forms.Padding(3);
        this.tabPageFrågor1.Size = new System.Drawing.Size(562, 467);
        this.tabPageFrågor1.TabIndex = 1;
        this.tabPageFrågor1.Text = "Frågor Sida 1";
        this.tabPageFrågor1.UseVisualStyleBackColor = true;
        // 
        // groupBox17
        // 
        this.groupBox17.Controls.Add(this.radioButton8Dålig);
        this.groupBox17.Controls.Add(this.radioButton8Bra);
        this.groupBox17.Controls.Add(this.label25);
        this.groupBox17.Location = new System.Drawing.Point(10, 402);
        this.groupBox17.Name = "groupBox17";
        this.groupBox17.Size = new System.Drawing.Size(542, 55);
        this.groupBox17.TabIndex = 19;
        this.groupBox17.TabStop = false;
        this.groupBox17.Text = "8.";
        // 
        // radioButton8Dålig
        // 
        this.radioButton8Dålig.AutoSize = true;
        this.radioButton8Dålig.Location = new System.Drawing.Point(485, 20);
        this.radioButton8Dålig.Name = "radioButton8Dålig";
        this.radioButton8Dålig.Size = new System.Drawing.Size(53, 17);
        this.radioButton8Dålig.TabIndex = 15;
        this.radioButton8Dålig.TabStop = true;
        this.radioButton8Dålig.Text = "Dålig";
        this.radioButton8Dålig.UseVisualStyleBackColor = true;
        this.radioButton8Dålig.CheckedChanged += new System.EventHandler(this.radioButton8Dålig_CheckedChanged);
        // 
        // radioButton8Bra
        // 
        this.radioButton8Bra.AutoSize = true;
        this.radioButton8Bra.Location = new System.Drawing.Point(435, 20);
        this.radioButton8Bra.Name = "radioButton8Bra";
        this.radioButton8Bra.Size = new System.Drawing.Size(44, 17);
        this.radioButton8Bra.TabIndex = 14;
        this.radioButton8Bra.TabStop = true;
        this.radioButton8Bra.Text = "Bra";
        this.radioButton8Bra.UseVisualStyleBackColor = true;
        this.radioButton8Bra.CheckedChanged += new System.EventHandler(this.radioButton8Bra_CheckedChanged);
        // 
        // label25
        // 
        this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label25.Location = new System.Drawing.Point(20, 22);
        this.label25.Name = "label25";
        this.label25.Size = new System.Drawing.Size(361, 26);
        this.label25.TabIndex = 0;
        this.label25.Text = "Nedkört ris";
        // 
        // groupBox16
        // 
        this.groupBox16.Controls.Add(this.radioButton7Dålig);
        this.groupBox16.Controls.Add(this.radioButton7Bra);
        this.groupBox16.Controls.Add(this.label24);
        this.groupBox16.Location = new System.Drawing.Point(11, 351);
        this.groupBox16.Name = "groupBox16";
        this.groupBox16.Size = new System.Drawing.Size(542, 55);
        this.groupBox16.TabIndex = 18;
        this.groupBox16.TabStop = false;
        this.groupBox16.Text = "7.";
        // 
        // radioButton7Dålig
        // 
        this.radioButton7Dålig.AutoSize = true;
        this.radioButton7Dålig.Location = new System.Drawing.Point(484, 20);
        this.radioButton7Dålig.Name = "radioButton7Dålig";
        this.radioButton7Dålig.Size = new System.Drawing.Size(53, 17);
        this.radioButton7Dålig.TabIndex = 13;
        this.radioButton7Dålig.TabStop = true;
        this.radioButton7Dålig.Text = "Dålig";
        this.radioButton7Dålig.UseVisualStyleBackColor = true;
        this.radioButton7Dålig.CheckedChanged += new System.EventHandler(this.radioButton7Dålig_CheckedChanged);
        // 
        // radioButton7Bra
        // 
        this.radioButton7Bra.AutoSize = true;
        this.radioButton7Bra.Location = new System.Drawing.Point(433, 20);
        this.radioButton7Bra.Name = "radioButton7Bra";
        this.radioButton7Bra.Size = new System.Drawing.Size(44, 17);
        this.radioButton7Bra.TabIndex = 12;
        this.radioButton7Bra.TabStop = true;
        this.radioButton7Bra.Text = "Bra";
        this.radioButton7Bra.UseVisualStyleBackColor = true;
        this.radioButton7Bra.CheckedChanged += new System.EventHandler(this.radioButton7Bra_CheckedChanged);
        // 
        // label24
        // 
        this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label24.Location = new System.Drawing.Point(20, 22);
        this.label24.Name = "label24";
        this.label24.Size = new System.Drawing.Size(361, 26);
        this.label24.TabIndex = 0;
        this.label24.Text = "Hyggesrensning";
        // 
        // groupBox15
        // 
        this.groupBox15.Controls.Add(this.radioButton6Ejaktuellt);
        this.groupBox15.Controls.Add(this.radioButton6Dålig);
        this.groupBox15.Controls.Add(this.radioButton6Bra);
        this.groupBox15.Controls.Add(this.label23);
        this.groupBox15.Location = new System.Drawing.Point(12, 304);
        this.groupBox15.Name = "groupBox15";
        this.groupBox15.Size = new System.Drawing.Size(541, 55);
        this.groupBox15.TabIndex = 17;
        this.groupBox15.TabStop = false;
        this.groupBox15.Text = "6.";
        // 
        // radioButton6Ejaktuellt
        // 
        this.radioButton6Ejaktuellt.AutoSize = true;
        this.radioButton6Ejaktuellt.Location = new System.Drawing.Point(454, 24);
        this.radioButton6Ejaktuellt.Name = "radioButton6Ejaktuellt";
        this.radioButton6Ejaktuellt.Size = new System.Drawing.Size(82, 17);
        this.radioButton6Ejaktuellt.TabIndex = 14;
        this.radioButton6Ejaktuellt.TabStop = true;
        this.radioButton6Ejaktuellt.Text = "Ej aktuellt";
        this.radioButton6Ejaktuellt.UseVisualStyleBackColor = true;
        this.radioButton6Ejaktuellt.CheckedChanged += new System.EventHandler(this.radioButton6Ejaktuellt_CheckedChanged);
        // 
        // radioButton6Dålig
        // 
        this.radioButton6Dålig.AutoSize = true;
        this.radioButton6Dålig.Location = new System.Drawing.Point(394, 24);
        this.radioButton6Dålig.Name = "radioButton6Dålig";
        this.radioButton6Dålig.Size = new System.Drawing.Size(53, 17);
        this.radioButton6Dålig.TabIndex = 13;
        this.radioButton6Dålig.TabStop = true;
        this.radioButton6Dålig.Text = "Dålig";
        this.radioButton6Dålig.UseVisualStyleBackColor = true;
        this.radioButton6Dålig.CheckedChanged += new System.EventHandler(this.radioButton6Dålig_CheckedChanged);
        // 
        // radioButton6Bra
        // 
        this.radioButton6Bra.AutoSize = true;
        this.radioButton6Bra.Location = new System.Drawing.Point(339, 24);
        this.radioButton6Bra.Name = "radioButton6Bra";
        this.radioButton6Bra.Size = new System.Drawing.Size(44, 17);
        this.radioButton6Bra.TabIndex = 12;
        this.radioButton6Bra.TabStop = true;
        this.radioButton6Bra.Text = "Bra";
        this.radioButton6Bra.UseVisualStyleBackColor = true;
        this.radioButton6Bra.CheckedChanged += new System.EventHandler(this.radioButton6Bra_CheckedChanged);
        // 
        // label23
        // 
        this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label23.Location = new System.Drawing.Point(20, 22);
        this.label23.Name = "label23";
        this.label23.Size = new System.Drawing.Size(361, 26);
        this.label23.TabIndex = 0;
        this.label23.Text = "Bredd på basväg";
        // 
        // groupBox14
        // 
        this.groupBox14.Controls.Add(this.radioButton5Dålig);
        this.groupBox14.Controls.Add(this.radioButton5Bra);
        this.groupBox14.Controls.Add(this.label22);
        this.groupBox14.Location = new System.Drawing.Point(13, 259);
        this.groupBox14.Name = "groupBox14";
        this.groupBox14.Size = new System.Drawing.Size(540, 55);
        this.groupBox14.TabIndex = 16;
        this.groupBox14.TabStop = false;
        this.groupBox14.Text = "5.";
        // 
        // radioButton5Dålig
        // 
        this.radioButton5Dålig.AutoSize = true;
        this.radioButton5Dålig.Location = new System.Drawing.Point(482, 20);
        this.radioButton5Dålig.Name = "radioButton5Dålig";
        this.radioButton5Dålig.Size = new System.Drawing.Size(53, 17);
        this.radioButton5Dålig.TabIndex = 11;
        this.radioButton5Dålig.TabStop = true;
        this.radioButton5Dålig.Text = "Dålig";
        this.radioButton5Dålig.UseVisualStyleBackColor = true;
        this.radioButton5Dålig.CheckedChanged += new System.EventHandler(this.radioButton5Dålig_CheckedChanged);
        // 
        // radioButton5Bra
        // 
        this.radioButton5Bra.AutoSize = true;
        this.radioButton5Bra.Location = new System.Drawing.Point(430, 20);
        this.radioButton5Bra.Name = "radioButton5Bra";
        this.radioButton5Bra.Size = new System.Drawing.Size(44, 17);
        this.radioButton5Bra.TabIndex = 10;
        this.radioButton5Bra.TabStop = true;
        this.radioButton5Bra.Text = "Bra";
        this.radioButton5Bra.UseVisualStyleBackColor = true;
        this.radioButton5Bra.CheckedChanged += new System.EventHandler(this.radioButton5Bra_CheckedChanged);
        // 
        // label22
        // 
        this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label22.Location = new System.Drawing.Point(19, 22);
        this.label22.Name = "label22";
        this.label22.Size = new System.Drawing.Size(361, 26);
        this.label22.TabIndex = 0;
        this.label22.Text = "Körväg mellan högar";
        // 
        // groupBox13
        // 
        this.groupBox13.Controls.Add(this.radioButton4Dålig);
        this.groupBox13.Controls.Add(this.radioButton4Bra);
        this.groupBox13.Controls.Add(this.label21);
        this.groupBox13.Location = new System.Drawing.Point(13, 212);
        this.groupBox13.Name = "groupBox13";
        this.groupBox13.Size = new System.Drawing.Size(540, 55);
        this.groupBox13.TabIndex = 15;
        this.groupBox13.TabStop = false;
        this.groupBox13.Text = "4.";
        // 
        // radioButton4Dålig
        // 
        this.radioButton4Dålig.AutoSize = true;
        this.radioButton4Dålig.Location = new System.Drawing.Point(482, 20);
        this.radioButton4Dålig.Name = "radioButton4Dålig";
        this.radioButton4Dålig.Size = new System.Drawing.Size(53, 17);
        this.radioButton4Dålig.TabIndex = 9;
        this.radioButton4Dålig.TabStop = true;
        this.radioButton4Dålig.Text = "Dålig";
        this.radioButton4Dålig.UseVisualStyleBackColor = true;
        this.radioButton4Dålig.CheckedChanged += new System.EventHandler(this.radioButton4Dålig_CheckedChanged);
        // 
        // radioButton4Bra
        // 
        this.radioButton4Bra.AutoSize = true;
        this.radioButton4Bra.Location = new System.Drawing.Point(430, 20);
        this.radioButton4Bra.Name = "radioButton4Bra";
        this.radioButton4Bra.Size = new System.Drawing.Size(44, 17);
        this.radioButton4Bra.TabIndex = 8;
        this.radioButton4Bra.TabStop = true;
        this.radioButton4Bra.Text = "Bra";
        this.radioButton4Bra.UseVisualStyleBackColor = true;
        this.radioButton4Bra.CheckedChanged += new System.EventHandler(this.radioButton4Bra_CheckedChanged);
        // 
        // label21
        // 
        this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label21.Location = new System.Drawing.Point(19, 22);
        this.label21.Name = "label21";
        this.label21.Size = new System.Drawing.Size(361, 26);
        this.label21.TabIndex = 0;
        this.label21.Text = "Avstånd mellan högar";
        // 
        // groupBox12
        // 
        this.groupBox12.Controls.Add(this.radioButton3Dålig);
        this.groupBox12.Controls.Add(this.radioButton3Bra);
        this.groupBox12.Controls.Add(this.label20);
        this.groupBox12.Location = new System.Drawing.Point(13, 166);
        this.groupBox12.Name = "groupBox12";
        this.groupBox12.Size = new System.Drawing.Size(540, 55);
        this.groupBox12.TabIndex = 14;
        this.groupBox12.TabStop = false;
        this.groupBox12.Text = "3.";
        // 
        // radioButton3Dålig
        // 
        this.radioButton3Dålig.AutoSize = true;
        this.radioButton3Dålig.Location = new System.Drawing.Point(482, 20);
        this.radioButton3Dålig.Name = "radioButton3Dålig";
        this.radioButton3Dålig.Size = new System.Drawing.Size(53, 17);
        this.radioButton3Dålig.TabIndex = 7;
        this.radioButton3Dålig.TabStop = true;
        this.radioButton3Dålig.Text = "Dålig";
        this.radioButton3Dålig.UseVisualStyleBackColor = true;
        this.radioButton3Dålig.CheckedChanged += new System.EventHandler(this.radioButton3Dålig_CheckedChanged);
        // 
        // radioButton3Bra
        // 
        this.radioButton3Bra.AutoSize = true;
        this.radioButton3Bra.Location = new System.Drawing.Point(430, 20);
        this.radioButton3Bra.Name = "radioButton3Bra";
        this.radioButton3Bra.Size = new System.Drawing.Size(44, 17);
        this.radioButton3Bra.TabIndex = 6;
        this.radioButton3Bra.TabStop = true;
        this.radioButton3Bra.Text = "Bra";
        this.radioButton3Bra.UseVisualStyleBackColor = true;
        this.radioButton3Bra.CheckedChanged += new System.EventHandler(this.radioButton3Bra_CheckedChanged);
        // 
        // label20
        // 
        this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label20.Location = new System.Drawing.Point(19, 22);
        this.label20.Name = "label20";
        this.label20.Size = new System.Drawing.Size(361, 26);
        this.label20.TabIndex = 0;
        this.label20.Text = "Storlek på grothögar";
        // 
        // label19
        // 
        this.label19.AutoSize = true;
        this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label19.Location = new System.Drawing.Point(10, 149);
        this.label19.Name = "label19";
        this.label19.Size = new System.Drawing.Size(172, 13);
        this.label19.TabIndex = 13;
        this.label19.Text = "KVALITET PÅ RISANPASSNING";
        // 
        // label3
        // 
        this.label3.AutoSize = true;
        this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label3.Location = new System.Drawing.Point(9, 10);
        this.label3.Name = "label3";
        this.label3.Size = new System.Drawing.Size(137, 13);
        this.label3.TabIndex = 10;
        this.label3.Text = "TRAKTEN VID ANKOMST";
        // 
        // groupBox4
        // 
        this.groupBox4.Controls.Add(this.radioButton2Nej);
        this.groupBox4.Controls.Add(this.radioButton2Ja);
        this.groupBox4.Controls.Add(this.label2);
        this.groupBox4.Location = new System.Drawing.Point(11, 77);
        this.groupBox4.Name = "groupBox4";
        this.groupBox4.Size = new System.Drawing.Size(540, 55);
        this.groupBox4.TabIndex = 12;
        this.groupBox4.TabStop = false;
        this.groupBox4.Text = "2.";
        // 
        // radioButton2Nej
        // 
        this.radioButton2Nej.AutoSize = true;
        this.radioButton2Nej.Location = new System.Drawing.Point(484, 21);
        this.radioButton2Nej.Name = "radioButton2Nej";
        this.radioButton2Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton2Nej.TabIndex = 5;
        this.radioButton2Nej.TabStop = true;
        this.radioButton2Nej.Text = "Nej";
        this.radioButton2Nej.UseVisualStyleBackColor = true;
        this.radioButton2Nej.CheckedChanged += new System.EventHandler(this.radioButton2Nej_CheckedChanged);
        // 
        // radioButton2Ja
        // 
        this.radioButton2Ja.AutoSize = true;
        this.radioButton2Ja.Location = new System.Drawing.Point(434, 21);
        this.radioButton2Ja.Name = "radioButton2Ja";
        this.radioButton2Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton2Ja.TabIndex = 4;
        this.radioButton2Ja.TabStop = true;
        this.radioButton2Ja.Text = "Ja";
        this.radioButton2Ja.UseVisualStyleBackColor = true;
        this.radioButton2Ja.CheckedChanged += new System.EventHandler(this.radioButton2Ja_CheckedChanged);
        // 
        // label2
        // 
        this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label2.Location = new System.Drawing.Point(19, 21);
        this.label2.Name = "label2";
        this.label2.Size = new System.Drawing.Size(361, 26);
        this.label2.TabIndex = 0;
        this.label2.Text = "Var vägen i bra skick innan påbörjat arbete?";
        // 
        // groupBox3
        // 
        this.groupBox3.Controls.Add(this.radioButton1Nej);
        this.groupBox3.Controls.Add(this.radioButton1Ja);
        this.groupBox3.Controls.Add(this.label1);
        this.groupBox3.Location = new System.Drawing.Point(12, 27);
        this.groupBox3.Name = "groupBox3";
        this.groupBox3.Size = new System.Drawing.Size(540, 55);
        this.groupBox3.TabIndex = 11;
        this.groupBox3.TabStop = false;
        this.groupBox3.Text = "1.";
        // 
        // radioButton1Nej
        // 
        this.radioButton1Nej.AutoSize = true;
        this.radioButton1Nej.Location = new System.Drawing.Point(483, 22);
        this.radioButton1Nej.Name = "radioButton1Nej";
        this.radioButton1Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton1Nej.TabIndex = 3;
        this.radioButton1Nej.TabStop = true;
        this.radioButton1Nej.Text = "Nej";
        this.radioButton1Nej.UseVisualStyleBackColor = true;
        this.radioButton1Nej.CheckedChanged += new System.EventHandler(this.radioButton1Nej_CheckedChanged);
        // 
        // radioButton1Ja
        // 
        this.radioButton1Ja.AutoSize = true;
        this.radioButton1Ja.Location = new System.Drawing.Point(433, 22);
        this.radioButton1Ja.Name = "radioButton1Ja";
        this.radioButton1Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton1Ja.TabIndex = 2;
        this.radioButton1Ja.TabStop = true;
        this.radioButton1Ja.Text = "Ja";
        this.radioButton1Ja.UseVisualStyleBackColor = true;
        this.radioButton1Ja.CheckedChanged += new System.EventHandler(this.radioButton1Ja_CheckedChanged);
        // 
        // label1
        // 
        this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label1.Location = new System.Drawing.Point(19, 22);
        this.label1.Name = "label1";
        this.label1.Size = new System.Drawing.Size(361, 26);
        this.label1.TabIndex = 0;
        this.label1.Text = "Var trakten i bra skick innan påbörjat arbete?";
        // 
        // tabPageFrågor2
        // 
        this.tabPageFrågor2.Controls.Add(this.groupBox25);
        this.tabPageFrågor2.Controls.Add(this.groupBox24);
        this.tabPageFrågor2.Controls.Add(this.groupBox23);
        this.tabPageFrågor2.Controls.Add(this.groupBox22);
        this.tabPageFrågor2.Controls.Add(this.groupBox21);
        this.tabPageFrågor2.Controls.Add(this.groupBox20);
        this.tabPageFrågor2.Controls.Add(this.groupBox19);
        this.tabPageFrågor2.Controls.Add(this.groupBox18);
        this.tabPageFrågor2.Controls.Add(this.label26);
        this.tabPageFrågor2.Location = new System.Drawing.Point(4, 22);
        this.tabPageFrågor2.Name = "tabPageFrågor2";
        this.tabPageFrågor2.Size = new System.Drawing.Size(562, 467);
        this.tabPageFrågor2.TabIndex = 3;
        this.tabPageFrågor2.Text = "Frågor Sida 2";
        this.tabPageFrågor2.UseVisualStyleBackColor = true;
        // 
        // groupBox25
        // 
        this.groupBox25.Controls.Add(this.radioButton16Ejaktuellt);
        this.groupBox25.Controls.Add(this.radioButton16Nej);
        this.groupBox25.Controls.Add(this.radioButton16Ja);
        this.groupBox25.Controls.Add(this.label34);
        this.groupBox25.Location = new System.Drawing.Point(12, 360);
        this.groupBox25.Name = "groupBox25";
        this.groupBox25.Size = new System.Drawing.Size(540, 55);
        this.groupBox25.TabIndex = 17;
        this.groupBox25.TabStop = false;
        this.groupBox25.Text = "16.";
        // 
        // radioButton16Ejaktuellt
        // 
        this.radioButton16Ejaktuellt.AutoSize = true;
        this.radioButton16Ejaktuellt.Location = new System.Drawing.Point(449, 22);
        this.radioButton16Ejaktuellt.Name = "radioButton16Ejaktuellt";
        this.radioButton16Ejaktuellt.Size = new System.Drawing.Size(82, 17);
        this.radioButton16Ejaktuellt.TabIndex = 22;
        this.radioButton16Ejaktuellt.TabStop = true;
        this.radioButton16Ejaktuellt.Text = "Ej aktuellt";
        this.radioButton16Ejaktuellt.UseVisualStyleBackColor = true;
        this.radioButton16Ejaktuellt.CheckedChanged += new System.EventHandler(this.radioButton16Ejaktuellt_CheckedChanged);
        // 
        // radioButton16Nej
        // 
        this.radioButton16Nej.AutoSize = true;
        this.radioButton16Nej.Location = new System.Drawing.Point(400, 22);
        this.radioButton16Nej.Name = "radioButton16Nej";
        this.radioButton16Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton16Nej.TabIndex = 21;
        this.radioButton16Nej.TabStop = true;
        this.radioButton16Nej.Text = "Nej";
        this.radioButton16Nej.UseVisualStyleBackColor = true;
        this.radioButton16Nej.CheckedChanged += new System.EventHandler(this.radioButton16Nej_CheckedChanged);
        // 
        // radioButton16Ja
        // 
        this.radioButton16Ja.AutoSize = true;
        this.radioButton16Ja.Location = new System.Drawing.Point(350, 22);
        this.radioButton16Ja.Name = "radioButton16Ja";
        this.radioButton16Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton16Ja.TabIndex = 20;
        this.radioButton16Ja.TabStop = true;
        this.radioButton16Ja.Text = "Ja";
        this.radioButton16Ja.UseVisualStyleBackColor = true;
        this.radioButton16Ja.CheckedChanged += new System.EventHandler(this.radioButton16Ja_CheckedChanged);
        // 
        // label34
        // 
        this.label34.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label34.Location = new System.Drawing.Point(20, 22);
        this.label34.Name = "label34";
        this.label34.Size = new System.Drawing.Size(361, 26);
        this.label34.TabIndex = 0;
        this.label34.Text = "Är diken rensade från virke/material?";
        // 
        // groupBox24
        // 
        this.groupBox24.Controls.Add(this.radioButton15Ejaktuellt);
        this.groupBox24.Controls.Add(this.radioButton15Nej);
        this.groupBox24.Controls.Add(this.radioButton15Ja);
        this.groupBox24.Controls.Add(this.label33);
        this.groupBox24.Location = new System.Drawing.Point(12, 309);
        this.groupBox24.Name = "groupBox24";
        this.groupBox24.Size = new System.Drawing.Size(540, 55);
        this.groupBox24.TabIndex = 16;
        this.groupBox24.TabStop = false;
        this.groupBox24.Text = "15.";
        // 
        // radioButton15Ejaktuellt
        // 
        this.radioButton15Ejaktuellt.AutoSize = true;
        this.radioButton15Ejaktuellt.Location = new System.Drawing.Point(449, 31);
        this.radioButton15Ejaktuellt.Name = "radioButton15Ejaktuellt";
        this.radioButton15Ejaktuellt.Size = new System.Drawing.Size(82, 17);
        this.radioButton15Ejaktuellt.TabIndex = 20;
        this.radioButton15Ejaktuellt.TabStop = true;
        this.radioButton15Ejaktuellt.Text = "Ej aktuellt";
        this.radioButton15Ejaktuellt.UseVisualStyleBackColor = true;
        this.radioButton15Ejaktuellt.CheckedChanged += new System.EventHandler(this.radioButton15Ejaktuellt_CheckedChanged);
        // 
        // radioButton15Nej
        // 
        this.radioButton15Nej.AutoSize = true;
        this.radioButton15Nej.Location = new System.Drawing.Point(400, 31);
        this.radioButton15Nej.Name = "radioButton15Nej";
        this.radioButton15Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton15Nej.TabIndex = 19;
        this.radioButton15Nej.TabStop = true;
        this.radioButton15Nej.Text = "Nej";
        this.radioButton15Nej.UseVisualStyleBackColor = true;
        this.radioButton15Nej.CheckedChanged += new System.EventHandler(this.radioButton15Nej_CheckedChanged);
        // 
        // radioButton15Ja
        // 
        this.radioButton15Ja.AutoSize = true;
        this.radioButton15Ja.Location = new System.Drawing.Point(350, 31);
        this.radioButton15Ja.Name = "radioButton15Ja";
        this.radioButton15Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton15Ja.TabIndex = 18;
        this.radioButton15Ja.TabStop = true;
        this.radioButton15Ja.Text = "Ja";
        this.radioButton15Ja.UseVisualStyleBackColor = true;
        this.radioButton15Ja.CheckedChanged += new System.EventHandler(this.radioButton15Ja_CheckedChanged);
        // 
        // label33
        // 
        this.label33.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label33.Location = new System.Drawing.Point(20, 22);
        this.label33.Name = "label33";
        this.label33.Size = new System.Drawing.Size(321, 26);
        this.label33.TabIndex = 0;
        this.label33.Text = "Finns tillfällig bro/virke kvar för passage över vattendrag?";
        // 
        // groupBox23
        // 
        this.groupBox23.Controls.Add(this.radioButton14Ejaktuellt);
        this.groupBox23.Controls.Add(this.radioButton14Nej);
        this.groupBox23.Controls.Add(this.radioButton14Ja);
        this.groupBox23.Controls.Add(this.label32);
        this.groupBox23.Location = new System.Drawing.Point(12, 263);
        this.groupBox23.Name = "groupBox23";
        this.groupBox23.Size = new System.Drawing.Size(540, 55);
        this.groupBox23.TabIndex = 15;
        this.groupBox23.TabStop = false;
        this.groupBox23.Text = "14.";
        // 
        // radioButton14Ejaktuellt
        // 
        this.radioButton14Ejaktuellt.AutoSize = true;
        this.radioButton14Ejaktuellt.Location = new System.Drawing.Point(452, 20);
        this.radioButton14Ejaktuellt.Name = "radioButton14Ejaktuellt";
        this.radioButton14Ejaktuellt.Size = new System.Drawing.Size(82, 17);
        this.radioButton14Ejaktuellt.TabIndex = 18;
        this.radioButton14Ejaktuellt.TabStop = true;
        this.radioButton14Ejaktuellt.Text = "Ej aktuellt";
        this.radioButton14Ejaktuellt.UseVisualStyleBackColor = true;
        this.radioButton14Ejaktuellt.CheckedChanged += new System.EventHandler(this.radioButton14Ejaktuellt_CheckedChanged);
        // 
        // radioButton14Nej
        // 
        this.radioButton14Nej.AutoSize = true;
        this.radioButton14Nej.Location = new System.Drawing.Point(400, 20);
        this.radioButton14Nej.Name = "radioButton14Nej";
        this.radioButton14Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton14Nej.TabIndex = 17;
        this.radioButton14Nej.TabStop = true;
        this.radioButton14Nej.Text = "Nej";
        this.radioButton14Nej.UseVisualStyleBackColor = true;
        this.radioButton14Nej.CheckedChanged += new System.EventHandler(this.radioButton14Nej_CheckedChanged);
        // 
        // radioButton14Ja
        // 
        this.radioButton14Ja.AutoSize = true;
        this.radioButton14Ja.Location = new System.Drawing.Point(350, 20);
        this.radioButton14Ja.Name = "radioButton14Ja";
        this.radioButton14Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton14Ja.TabIndex = 16;
        this.radioButton14Ja.TabStop = true;
        this.radioButton14Ja.Text = "Ja";
        this.radioButton14Ja.UseVisualStyleBackColor = true;
        this.radioButton14Ja.CheckedChanged += new System.EventHandler(this.radioButton14Ja_CheckedChanged);
        // 
        // label32
        // 
        this.label32.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label32.Location = new System.Drawing.Point(20, 22);
        this.label32.Name = "label32";
        this.label32.Size = new System.Drawing.Size(321, 26);
        this.label32.TabIndex = 0;
        this.label32.Text = "Är stigar och leder oskadade och framkomliga?";
        // 
        // groupBox22
        // 
        this.groupBox22.Controls.Add(this.radioButton13Ejaktuellt);
        this.groupBox22.Controls.Add(this.radioButton13Nej);
        this.groupBox22.Controls.Add(this.radioButton13Ja);
        this.groupBox22.Controls.Add(this.label31);
        this.groupBox22.Location = new System.Drawing.Point(11, 216);
        this.groupBox22.Name = "groupBox22";
        this.groupBox22.Size = new System.Drawing.Size(540, 55);
        this.groupBox22.TabIndex = 14;
        this.groupBox22.TabStop = false;
        this.groupBox22.Text = "13.";
        // 
        // radioButton13Ejaktuellt
        // 
        this.radioButton13Ejaktuellt.AutoSize = true;
        this.radioButton13Ejaktuellt.Location = new System.Drawing.Point(452, 24);
        this.radioButton13Ejaktuellt.Name = "radioButton13Ejaktuellt";
        this.radioButton13Ejaktuellt.Size = new System.Drawing.Size(82, 17);
        this.radioButton13Ejaktuellt.TabIndex = 16;
        this.radioButton13Ejaktuellt.TabStop = true;
        this.radioButton13Ejaktuellt.Text = "Ej aktuellt";
        this.radioButton13Ejaktuellt.UseVisualStyleBackColor = true;
        this.radioButton13Ejaktuellt.CheckedChanged += new System.EventHandler(this.radioButton13Ejaktuellt_CheckedChanged);
        // 
        // radioButton13Nej
        // 
        this.radioButton13Nej.AutoSize = true;
        this.radioButton13Nej.Location = new System.Drawing.Point(401, 24);
        this.radioButton13Nej.Name = "radioButton13Nej";
        this.radioButton13Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton13Nej.TabIndex = 15;
        this.radioButton13Nej.TabStop = true;
        this.radioButton13Nej.Text = "Nej";
        this.radioButton13Nej.UseVisualStyleBackColor = true;
        this.radioButton13Nej.CheckedChanged += new System.EventHandler(this.radioButton13Nej_CheckedChanged);
        // 
        // radioButton13Ja
        // 
        this.radioButton13Ja.AutoSize = true;
        this.radioButton13Ja.Location = new System.Drawing.Point(351, 24);
        this.radioButton13Ja.Name = "radioButton13Ja";
        this.radioButton13Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton13Ja.TabIndex = 14;
        this.radioButton13Ja.TabStop = true;
        this.radioButton13Ja.Text = "Ja";
        this.radioButton13Ja.UseVisualStyleBackColor = true;
        this.radioButton13Ja.CheckedChanged += new System.EventHandler(this.radioButton13Ja_CheckedChanged);
        // 
        // label31
        // 
        this.label31.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label31.Location = new System.Drawing.Point(20, 18);
        this.label31.Name = "label31";
        this.label31.Size = new System.Drawing.Size(322, 26);
        this.label31.TabIndex = 0;
        this.label31.Text = "Har skador på forminnen och kulturlämningar undvikits och lämnats risfria?";
        // 
        // groupBox21
        // 
        this.groupBox21.Controls.Add(this.radioButton12Ejaktuellt);
        this.groupBox21.Controls.Add(this.radioButton12Nej);
        this.groupBox21.Controls.Add(this.radioButton12Ja);
        this.groupBox21.Controls.Add(this.label30);
        this.groupBox21.Location = new System.Drawing.Point(11, 170);
        this.groupBox21.Name = "groupBox21";
        this.groupBox21.Size = new System.Drawing.Size(540, 55);
        this.groupBox21.TabIndex = 13;
        this.groupBox21.TabStop = false;
        this.groupBox21.Text = "12.";
        // 
        // radioButton12Ejaktuellt
        // 
        this.radioButton12Ejaktuellt.AutoSize = true;
        this.radioButton12Ejaktuellt.Location = new System.Drawing.Point(452, 20);
        this.radioButton12Ejaktuellt.Name = "radioButton12Ejaktuellt";
        this.radioButton12Ejaktuellt.Size = new System.Drawing.Size(82, 17);
        this.radioButton12Ejaktuellt.TabIndex = 14;
        this.radioButton12Ejaktuellt.TabStop = true;
        this.radioButton12Ejaktuellt.Text = "Ej aktuellt";
        this.radioButton12Ejaktuellt.UseVisualStyleBackColor = true;
        this.radioButton12Ejaktuellt.CheckedChanged += new System.EventHandler(this.radioButton12Ejaktuellt_CheckedChanged);
        // 
        // radioButton12Nej
        // 
        this.radioButton12Nej.AutoSize = true;
        this.radioButton12Nej.Location = new System.Drawing.Point(401, 20);
        this.radioButton12Nej.Name = "radioButton12Nej";
        this.radioButton12Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton12Nej.TabIndex = 13;
        this.radioButton12Nej.TabStop = true;
        this.radioButton12Nej.Text = "Nej";
        this.radioButton12Nej.UseVisualStyleBackColor = true;
        this.radioButton12Nej.CheckedChanged += new System.EventHandler(this.radioButton12Nej_CheckedChanged);
        // 
        // radioButton12Ja
        // 
        this.radioButton12Ja.AutoSize = true;
        this.radioButton12Ja.Location = new System.Drawing.Point(351, 20);
        this.radioButton12Ja.Name = "radioButton12Ja";
        this.radioButton12Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton12Ja.TabIndex = 12;
        this.radioButton12Ja.TabStop = true;
        this.radioButton12Ja.Text = "Ja";
        this.radioButton12Ja.UseVisualStyleBackColor = true;
        this.radioButton12Ja.CheckedChanged += new System.EventHandler(this.radioButton12Ja_CheckedChanged);
        // 
        // label30
        // 
        this.label30.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label30.Location = new System.Drawing.Point(21, 22);
        this.label30.Name = "label30";
        this.label30.Size = new System.Drawing.Size(233, 26);
        this.label30.TabIndex = 0;
        this.label30.Text = "Har gammal död ved lämnats orörd?";
        // 
        // groupBox20
        // 
        this.groupBox20.Controls.Add(this.radioButton11Nej);
        this.groupBox20.Controls.Add(this.radioButton11Ja);
        this.groupBox20.Controls.Add(this.label29);
        this.groupBox20.Location = new System.Drawing.Point(12, 124);
        this.groupBox20.Name = "groupBox20";
        this.groupBox20.Size = new System.Drawing.Size(540, 55);
        this.groupBox20.TabIndex = 12;
        this.groupBox20.TabStop = false;
        this.groupBox20.Text = "11.";
        // 
        // radioButton11Nej
        // 
        this.radioButton11Nej.AutoSize = true;
        this.radioButton11Nej.Location = new System.Drawing.Point(482, 20);
        this.radioButton11Nej.Name = "radioButton11Nej";
        this.radioButton11Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton11Nej.TabIndex = 11;
        this.radioButton11Nej.TabStop = true;
        this.radioButton11Nej.Text = "Nej";
        this.radioButton11Nej.UseVisualStyleBackColor = true;
        this.radioButton11Nej.CheckedChanged += new System.EventHandler(this.radioButton11Nej_CheckedChanged);
        // 
        // radioButton11Ja
        // 
        this.radioButton11Ja.AutoSize = true;
        this.radioButton11Ja.Location = new System.Drawing.Point(432, 20);
        this.radioButton11Ja.Name = "radioButton11Ja";
        this.radioButton11Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton11Ja.TabIndex = 10;
        this.radioButton11Ja.TabStop = true;
        this.radioButton11Ja.Text = "Ja";
        this.radioButton11Ja.UseVisualStyleBackColor = true;
        this.radioButton11Ja.CheckedChanged += new System.EventHandler(this.radioButton11Ja_CheckedChanged);
        // 
        // label29
        // 
        this.label29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label29.Location = new System.Drawing.Point(20, 22);
        this.label29.Name = "label29";
        this.label29.Size = new System.Drawing.Size(361, 26);
        this.label29.TabIndex = 0;
        this.label29.Text = "Finns körskador i terräng som bör åtgärdas?";
        // 
        // groupBox19
        // 
        this.groupBox19.Controls.Add(this.radioButton10Nej);
        this.groupBox19.Controls.Add(this.radioButton10Ja);
        this.groupBox19.Controls.Add(this.label28);
        this.groupBox19.Location = new System.Drawing.Point(12, 77);
        this.groupBox19.Name = "groupBox19";
        this.groupBox19.Size = new System.Drawing.Size(540, 55);
        this.groupBox19.TabIndex = 11;
        this.groupBox19.TabStop = false;
        this.groupBox19.Text = "10.";
        // 
        // radioButton10Nej
        // 
        this.radioButton10Nej.AutoSize = true;
        this.radioButton10Nej.Location = new System.Drawing.Point(482, 20);
        this.radioButton10Nej.Name = "radioButton10Nej";
        this.radioButton10Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton10Nej.TabIndex = 9;
        this.radioButton10Nej.TabStop = true;
        this.radioButton10Nej.Text = "Nej";
        this.radioButton10Nej.UseVisualStyleBackColor = true;
        this.radioButton10Nej.CheckedChanged += new System.EventHandler(this.radioButton10Nej_CheckedChanged);
        // 
        // radioButton10Ja
        // 
        this.radioButton10Ja.AutoSize = true;
        this.radioButton10Ja.Location = new System.Drawing.Point(432, 20);
        this.radioButton10Ja.Name = "radioButton10Ja";
        this.radioButton10Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton10Ja.TabIndex = 8;
        this.radioButton10Ja.TabStop = true;
        this.radioButton10Ja.Text = "Ja";
        this.radioButton10Ja.UseVisualStyleBackColor = true;
        this.radioButton10Ja.CheckedChanged += new System.EventHandler(this.radioButton10Ja_CheckedChanged);
        // 
        // label28
        // 
        this.label28.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label28.Location = new System.Drawing.Point(20, 22);
        this.label28.Name = "label28";
        this.label28.Size = new System.Drawing.Size(361, 26);
        this.label28.TabIndex = 0;
        this.label28.Text = "Finns körskador på väg som bör åtgärdas?";
        // 
        // groupBox18
        // 
        this.groupBox18.Controls.Add(this.radioButton9Nej);
        this.groupBox18.Controls.Add(this.radioButton9Ja);
        this.groupBox18.Controls.Add(this.label27);
        this.groupBox18.Location = new System.Drawing.Point(12, 29);
        this.groupBox18.Name = "groupBox18";
        this.groupBox18.Size = new System.Drawing.Size(540, 55);
        this.groupBox18.TabIndex = 10;
        this.groupBox18.TabStop = false;
        this.groupBox18.Text = "9.";
        // 
        // radioButton9Nej
        // 
        this.radioButton9Nej.AutoSize = true;
        this.radioButton9Nej.Location = new System.Drawing.Point(482, 20);
        this.radioButton9Nej.Name = "radioButton9Nej";
        this.radioButton9Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton9Nej.TabIndex = 7;
        this.radioButton9Nej.TabStop = true;
        this.radioButton9Nej.Text = "Nej";
        this.radioButton9Nej.UseVisualStyleBackColor = true;
        this.radioButton9Nej.CheckedChanged += new System.EventHandler(this.radioButton9Nej_CheckedChanged);
        // 
        // radioButton9Ja
        // 
        this.radioButton9Ja.AutoSize = true;
        this.radioButton9Ja.Location = new System.Drawing.Point(432, 20);
        this.radioButton9Ja.Name = "radioButton9Ja";
        this.radioButton9Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton9Ja.TabIndex = 6;
        this.radioButton9Ja.TabStop = true;
        this.radioButton9Ja.Text = "Ja";
        this.radioButton9Ja.UseVisualStyleBackColor = true;
        this.radioButton9Ja.CheckedChanged += new System.EventHandler(this.radioButton9Ja_CheckedChanged);
        // 
        // label27
        // 
        this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label27.Location = new System.Drawing.Point(20, 22);
        this.label27.Name = "label27";
        this.label27.Size = new System.Drawing.Size(361, 26);
        this.label27.TabIndex = 0;
        this.label27.Text = "Är risskotning utförd enl traktdirektiv?";
        // 
        // label26
        // 
        this.label26.AutoSize = true;
        this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label26.Location = new System.Drawing.Point(8, 11);
        this.label26.Name = "label26";
        this.label26.Size = new System.Drawing.Size(109, 13);
        this.label26.TabIndex = 9;
        this.label26.Text = "EGENUPPFÖLJNING";
        // 
        // tabPageFrågor3
        // 
        this.tabPageFrågor3.Controls.Add(this.groupBox28);
        this.tabPageFrågor3.Controls.Add(this.groupBox27);
        this.tabPageFrågor3.Controls.Add(this.groupBox26);
        this.tabPageFrågor3.Controls.Add(this.groupBoxOBS);
        this.tabPageFrågor3.Controls.Add(this.groupBox11);
        this.tabPageFrågor3.Location = new System.Drawing.Point(4, 22);
        this.tabPageFrågor3.Name = "tabPageFrågor3";
        this.tabPageFrågor3.Size = new System.Drawing.Size(562, 467);
        this.tabPageFrågor3.TabIndex = 4;
        this.tabPageFrågor3.Text = "Frågor Sida 3";
        this.tabPageFrågor3.UseVisualStyleBackColor = true;
        // 
        // groupBox28
        // 
        this.groupBox28.Controls.Add(this.radioButton19Nej);
        this.groupBox28.Controls.Add(this.radioButton19Ja);
        this.groupBox28.Controls.Add(this.label37);
        this.groupBox28.Location = new System.Drawing.Point(8, 116);
        this.groupBox28.Name = "groupBox28";
        this.groupBox28.Size = new System.Drawing.Size(540, 55);
        this.groupBox28.TabIndex = 7;
        this.groupBox28.TabStop = false;
        this.groupBox28.Text = "19.";
        // 
        // radioButton19Nej
        // 
        this.radioButton19Nej.AutoSize = true;
        this.radioButton19Nej.Location = new System.Drawing.Point(483, 23);
        this.radioButton19Nej.Name = "radioButton19Nej";
        this.radioButton19Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton19Nej.TabIndex = 27;
        this.radioButton19Nej.TabStop = true;
        this.radioButton19Nej.Text = "Nej";
        this.radioButton19Nej.UseVisualStyleBackColor = true;
        this.radioButton19Nej.CheckedChanged += new System.EventHandler(this.radioButton19Nej_CheckedChanged);
        // 
        // radioButton19Ja
        // 
        this.radioButton19Ja.AutoSize = true;
        this.radioButton19Ja.Location = new System.Drawing.Point(433, 23);
        this.radioButton19Ja.Name = "radioButton19Ja";
        this.radioButton19Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton19Ja.TabIndex = 26;
        this.radioButton19Ja.TabStop = true;
        this.radioButton19Ja.Text = "Ja";
        this.radioButton19Ja.UseVisualStyleBackColor = true;
        this.radioButton19Ja.CheckedChanged += new System.EventHandler(this.radioButton19Ja_CheckedChanged);
        // 
        // label37
        // 
        this.label37.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label37.Location = new System.Drawing.Point(20, 22);
        this.label37.Name = "label37";
        this.label37.Size = new System.Drawing.Size(361, 26);
        this.label37.TabIndex = 0;
        this.label37.Text = "Är trakten avstädad och eventuellt oljespill sanerat?";
        // 
        // groupBox27
        // 
        this.groupBox27.Controls.Add(this.radioButton18Nej);
        this.groupBox27.Controls.Add(this.radioButton18Ja);
        this.groupBox27.Controls.Add(this.label36);
        this.groupBox27.Location = new System.Drawing.Point(8, 69);
        this.groupBox27.Name = "groupBox27";
        this.groupBox27.Size = new System.Drawing.Size(540, 55);
        this.groupBox27.TabIndex = 6;
        this.groupBox27.TabStop = false;
        this.groupBox27.Text = "18.";
        // 
        // radioButton18Nej
        // 
        this.radioButton18Nej.AutoSize = true;
        this.radioButton18Nej.Location = new System.Drawing.Point(483, 20);
        this.radioButton18Nej.Name = "radioButton18Nej";
        this.radioButton18Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton18Nej.TabIndex = 25;
        this.radioButton18Nej.TabStop = true;
        this.radioButton18Nej.Text = "Nej";
        this.radioButton18Nej.UseVisualStyleBackColor = true;
        this.radioButton18Nej.CheckedChanged += new System.EventHandler(this.radioButton18Nej_CheckedChanged);
        // 
        // radioButton18Ja
        // 
        this.radioButton18Ja.AutoSize = true;
        this.radioButton18Ja.Location = new System.Drawing.Point(433, 20);
        this.radioButton18Ja.Name = "radioButton18Ja";
        this.radioButton18Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton18Ja.TabIndex = 24;
        this.radioButton18Ja.TabStop = true;
        this.radioButton18Ja.Text = "Ja";
        this.radioButton18Ja.UseVisualStyleBackColor = true;
        this.radioButton18Ja.CheckedChanged += new System.EventHandler(this.radioButton18Ja_CheckedChanged);
        // 
        // label36
        // 
        this.label36.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label36.Location = new System.Drawing.Point(20, 22);
        this.label36.Name = "label36";
        this.label36.Size = new System.Drawing.Size(361, 26);
        this.label36.TabIndex = 0;
        this.label36.Text = "Är materialet täckt med vältpapp?";
        // 
        // groupBox26
        // 
        this.groupBox26.Controls.Add(this.radioButton17Nej);
        this.groupBox26.Controls.Add(this.radioButton17Ja);
        this.groupBox26.Controls.Add(this.label35);
        this.groupBox26.Location = new System.Drawing.Point(8, 20);
        this.groupBox26.Name = "groupBox26";
        this.groupBox26.Size = new System.Drawing.Size(540, 55);
        this.groupBox26.TabIndex = 5;
        this.groupBox26.TabStop = false;
        this.groupBox26.Text = "17.";
        // 
        // radioButton17Nej
        // 
        this.radioButton17Nej.AutoSize = true;
        this.radioButton17Nej.Location = new System.Drawing.Point(483, 20);
        this.radioButton17Nej.Name = "radioButton17Nej";
        this.radioButton17Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton17Nej.TabIndex = 23;
        this.radioButton17Nej.TabStop = true;
        this.radioButton17Nej.Text = "Nej";
        this.radioButton17Nej.UseVisualStyleBackColor = true;
        this.radioButton17Nej.CheckedChanged += new System.EventHandler(this.radioButton17Nej_CheckedChanged);
        // 
        // radioButton17Ja
        // 
        this.radioButton17Ja.AutoSize = true;
        this.radioButton17Ja.Location = new System.Drawing.Point(433, 20);
        this.radioButton17Ja.Name = "radioButton17Ja";
        this.radioButton17Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton17Ja.TabIndex = 22;
        this.radioButton17Ja.TabStop = true;
        this.radioButton17Ja.Text = "Ja";
        this.radioButton17Ja.UseVisualStyleBackColor = true;
        this.radioButton17Ja.CheckedChanged += new System.EventHandler(this.radioButton17Ja_CheckedChanged);
        // 
        // label35
        // 
        this.label35.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label35.Location = new System.Drawing.Point(20, 22);
        this.label35.Name = "label35";
        this.label35.Size = new System.Drawing.Size(361, 26);
        this.label35.TabIndex = 0;
        this.label35.Text = "Är allt ris utkört till avlägg och uppmärkt med vältlappar?";
        // 
        // groupBoxOBS
        // 
        this.groupBoxOBS.Controls.Add(this.label7);
        this.groupBoxOBS.Controls.Add(this.label8);
        this.groupBoxOBS.Location = new System.Drawing.Point(8, 162);
        this.groupBoxOBS.Name = "groupBoxOBS";
        this.groupBoxOBS.Size = new System.Drawing.Size(540, 66);
        this.groupBoxOBS.TabIndex = 8;
        this.groupBoxOBS.TabStop = false;
        // 
        // label7
        // 
        this.label7.AutoSize = true;
        this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label7.Location = new System.Drawing.Point(24, 38);
        this.label7.Name = "label7";
        this.label7.Size = new System.Drawing.Size(456, 13);
        this.label7.TabIndex = 1;
        this.label7.Text = "Ifall något ”Nej” har valts på någon av frågorna så skall en miljörapport skrivas" +
            "!";
        // 
        // label8
        // 
        this.label8.AutoSize = true;
        this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label8.Location = new System.Drawing.Point(19, 17);
        this.label8.Name = "label8";
        this.label8.Size = new System.Drawing.Size(32, 13);
        this.label8.TabIndex = 0;
        this.label8.Text = "OBS!";
        // 
        // groupBox11
        // 
        this.groupBox11.Controls.Add(this.richTextBoxOvrigt);
        this.groupBox11.Location = new System.Drawing.Point(11, 352);
        this.groupBox11.Name = "groupBox11";
        this.groupBox11.Size = new System.Drawing.Size(540, 100);
        this.groupBox11.TabIndex = 4;
        this.groupBox11.TabStop = false;
        this.groupBox11.Text = "KOMMENTAR";
        // 
        // richTextBoxOvrigt
        // 
        this.richTextBoxOvrigt.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetBiobränsle, "Fragor.Ovrigt", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.richTextBoxOvrigt.Location = new System.Drawing.Point(15, 20);
        this.richTextBoxOvrigt.MaxLength = 4095;
        this.richTextBoxOvrigt.Name = "richTextBoxOvrigt";
        this.richTextBoxOvrigt.Size = new System.Drawing.Size(511, 66);
        this.richTextBoxOvrigt.TabIndex = 0;
        this.richTextBoxOvrigt.Text = "";
        this.richTextBoxOvrigt.TextChanged += new System.EventHandler(this.richTextBoxOvrigt_TextChanged);
        // 
        // environmentBiobränsle
        // 
        designerSettings1.ApplicationConnection = null;
        designerSettings1.DefaultFont = new System.Drawing.Font("Arial", 10F);
        designerSettings1.Icon = ((System.Drawing.Icon)(resources.GetObject("designerSettings1.Icon")));
        designerSettings1.Restrictions = designerRestrictions1;
        designerSettings1.Text = "";
        this.environmentBiobränsle.DesignerSettings = designerSettings1;
        emailSettings1.Address = "";
        emailSettings1.Host = "";
        emailSettings1.MessageTemplate = "";
        emailSettings1.Name = "";
        emailSettings1.Password = "";
        emailSettings1.Port = 49;
        emailSettings1.UserName = "";
        this.environmentBiobränsle.EmailSettings = emailSettings1;
        previewSettings1.Buttons = ((FastReport.PreviewButtons)((((((FastReport.PreviewButtons.Print | FastReport.PreviewButtons.Save)
                    | FastReport.PreviewButtons.Find)
                    | FastReport.PreviewButtons.Zoom)
                    | FastReport.PreviewButtons.Navigator)
                    | FastReport.PreviewButtons.Close)));
        previewSettings1.Icon = ((System.Drawing.Icon)(resources.GetObject("previewSettings1.Icon")));
        previewSettings1.Text = "";
        this.environmentBiobränsle.PreviewSettings = previewSettings1;
        this.environmentBiobränsle.ReportSettings = reportSettings1;
        this.environmentBiobränsle.UIStyle = FastReport.Utils.UIStyle.Office2007Black;
        // 
        // progressBarData
        // 
        this.progressBarData.ForeColor = System.Drawing.SystemColors.Desktop;
        this.progressBarData.Location = new System.Drawing.Point(6, 541);
        this.progressBarData.Maximum = 30;
        this.progressBarData.Name = "progressBarData";
        this.progressBarData.Size = new System.Drawing.Size(560, 20);
        this.progressBarData.Step = 1;
        this.progressBarData.TabIndex = 2;
        // 
        // labelProgress
        // 
        this.labelProgress.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelProgress.Location = new System.Drawing.Point(498, 525);
        this.labelProgress.Name = "labelProgress";
        this.labelProgress.Size = new System.Drawing.Size(68, 13);
        this.labelProgress.TabIndex = 3;
        this.labelProgress.Text = "0% Klar";
        this.labelProgress.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
        // 
        // labelFragaObs
        // 
        this.labelFragaObs.AutoSize = true;
        this.labelFragaObs.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelFragaObs.Location = new System.Drawing.Point(24, 38);
        this.labelFragaObs.Name = "labelFragaObs";
        this.labelFragaObs.Size = new System.Drawing.Size(458, 13);
        this.labelFragaObs.TabIndex = 31;
        this.labelFragaObs.Text = "Ifall något ”Nej” har valts på någon av frågorna så skall en miljörapport skrivas" +
            ".";
        // 
        // labelOBS
        // 
        this.labelOBS.AutoSize = true;
        this.labelOBS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelOBS.Location = new System.Drawing.Point(19, 17);
        this.labelOBS.Name = "labelOBS";
        this.labelOBS.Size = new System.Drawing.Size(36, 13);
        this.labelOBS.TabIndex = 32;
        this.labelOBS.Text = "OBS!";
        // 
        // reportBiobränsle
        // 
        this.reportBiobränsle.ReportResourceString = resources.GetString("reportBiobränsle.ReportResourceString");
        this.reportBiobränsle.RegisterData(this.dataSetBiobränsle, "dataSetBiobränsle");
        // 
        // textBoxRegion
        // 
        this.textBoxRegion.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetBiobränsle, "UserData.Region", true));
        this.textBoxRegion.Location = new System.Drawing.Point(15, 30);
        this.textBoxRegion.MaxLength = 6;
        this.textBoxRegion.Name = "textBoxRegion";
        this.textBoxRegion.ReadOnly = true;
        this.textBoxRegion.Size = new System.Drawing.Size(116, 21);
        this.textBoxRegion.TabIndex = 8;
        this.textBoxRegion.Visible = false;
        // 
        // textBoxDistrikt
        // 
        this.textBoxDistrikt.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetBiobränsle, "UserData.Distrikt", true));
        this.textBoxDistrikt.Location = new System.Drawing.Point(140, 30);
        this.textBoxDistrikt.MaxLength = 6;
        this.textBoxDistrikt.Name = "textBoxDistrikt";
        this.textBoxDistrikt.ReadOnly = true;
        this.textBoxDistrikt.Size = new System.Drawing.Size(116, 21);
        this.textBoxDistrikt.TabIndex = 9;
        this.textBoxDistrikt.Visible = false;
        // 
        // BiobränsleForm
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.AutoScroll = true;
        this.AutoSize = true;
        this.ClientSize = new System.Drawing.Size(571, 567);
        this.Controls.Add(this.labelProgress);
        this.Controls.Add(this.progressBarData);
        this.Controls.Add(this.tabControl1);
        this.Controls.Add(this.menuStripBiobränsle);
        this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
        this.MainMenuStrip = this.menuStripBiobränsle;
        this.MaximizeBox = false;
        this.MaximumSize = new System.Drawing.Size(630, 644);
        this.MinimizeBox = false;
        this.Name = "BiobränsleForm";
        this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        this.Text = "KORUS Biobränsle";
        this.Load += new System.EventHandler(this.Biobränsle_Load);
        this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Biobränsle_FormClosing);
        ((System.ComponentModel.ISupportInitialize)(this.dataSetBiobränsle)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableUserData)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFragor)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetSettings)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableRegion)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableDistrikt)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableStandort)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableMarkberedningsmetod)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableUrsprung)).EndInit();
        this.menuStripBiobränsle.ResumeLayout(false);
        this.menuStripBiobränsle.PerformLayout();
        this.tabControl1.ResumeLayout(false);
        this.tabPageBiobränsle.ResumeLayout(false);
        this.groupBoxBiobränsle.ResumeLayout(false);
        this.groupBox6.ResumeLayout(false);
        this.groupBox6.PerformLayout();
        this.groupBox9.ResumeLayout(false);
        this.groupBox9.PerformLayout();
        this.groupBox8.ResumeLayout(false);
        this.groupBox8.PerformLayout();
        this.groupBox7.ResumeLayout(false);
        this.groupBox7.PerformLayout();
        this.groupBox10.ResumeLayout(false);
        this.groupBox10.PerformLayout();
        this.groupBox5.ResumeLayout(false);
        this.groupBox2.ResumeLayout(false);
        this.groupBox2.PerformLayout();
        this.groupBox1.ResumeLayout(false);
        this.groupBox1.PerformLayout();
        this.groupBoxTrakt.ResumeLayout(false);
        this.groupBoxTrakt.PerformLayout();
        this.groupBoxRegion.ResumeLayout(false);
        this.groupBoxRegion.PerformLayout();
        this.tabPageFrågor1.ResumeLayout(false);
        this.tabPageFrågor1.PerformLayout();
        this.groupBox17.ResumeLayout(false);
        this.groupBox17.PerformLayout();
        this.groupBox16.ResumeLayout(false);
        this.groupBox16.PerformLayout();
        this.groupBox15.ResumeLayout(false);
        this.groupBox15.PerformLayout();
        this.groupBox14.ResumeLayout(false);
        this.groupBox14.PerformLayout();
        this.groupBox13.ResumeLayout(false);
        this.groupBox13.PerformLayout();
        this.groupBox12.ResumeLayout(false);
        this.groupBox12.PerformLayout();
        this.groupBox4.ResumeLayout(false);
        this.groupBox4.PerformLayout();
        this.groupBox3.ResumeLayout(false);
        this.groupBox3.PerformLayout();
        this.tabPageFrågor2.ResumeLayout(false);
        this.tabPageFrågor2.PerformLayout();
        this.groupBox25.ResumeLayout(false);
        this.groupBox25.PerformLayout();
        this.groupBox24.ResumeLayout(false);
        this.groupBox24.PerformLayout();
        this.groupBox23.ResumeLayout(false);
        this.groupBox23.PerformLayout();
        this.groupBox22.ResumeLayout(false);
        this.groupBox22.PerformLayout();
        this.groupBox21.ResumeLayout(false);
        this.groupBox21.PerformLayout();
        this.groupBox20.ResumeLayout(false);
        this.groupBox20.PerformLayout();
        this.groupBox19.ResumeLayout(false);
        this.groupBox19.PerformLayout();
        this.groupBox18.ResumeLayout(false);
        this.groupBox18.PerformLayout();
        this.tabPageFrågor3.ResumeLayout(false);
        this.groupBox28.ResumeLayout(false);
        this.groupBox28.PerformLayout();
        this.groupBox27.ResumeLayout(false);
        this.groupBox27.PerformLayout();
        this.groupBox26.ResumeLayout(false);
        this.groupBox26.PerformLayout();
        this.groupBoxOBS.ResumeLayout(false);
        this.groupBoxOBS.PerformLayout();
        this.groupBox11.ResumeLayout(false);
        ((System.ComponentModel.ISupportInitialize)(this.reportBiobränsle)).EndInit();
        this.ResumeLayout(false);
        this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.MenuStrip menuStripBiobränsle;
    private System.Data.DataSet dataSetBiobränsle;
    private System.Data.DataTable dataTableUserData;
    private System.Windows.Forms.OpenFileDialog openReportFileDialog;
    private System.Windows.Forms.TabControl tabControl1;
    private System.Windows.Forms.TabPage tabPageFrågor1;
    private System.Data.DataTable dataTableFragor;
    private System.Data.DataColumn dataColumnFraga1;
    private System.Data.DataColumn dataColumnFraga2;
    private System.Data.DataSet dataSetSettings;
    private System.Data.DataTable dataTableRegion;
    private System.Data.DataTable dataTableDistrikt;
    private System.Data.DataTable dataTableStandort;
    private System.Data.DataColumn dataColumnSettingsStandortNamn;
    private FastReport.EnvironmentSettings environmentBiobränsle;
    private System.Windows.Forms.ProgressBar progressBarData;
    private System.Windows.Forms.Label labelProgress;
    private System.Data.DataColumn dataColumnSettingsRegionId;
    private System.Data.DataColumn dataColumnSettingsDistriktRegionId;
    private System.Data.DataColumn dataColumnSettingsRegionNamn;
    private System.Data.DataColumn dataColumnSettingsDistriktId;
    private System.Data.DataColumn dataColumnSettingsDistriktNamn;
    private System.Data.DataTable dataTableMarkberedningsmetod;
    private System.Data.DataColumn dataColumnSettingsMarkberedningsmetodNamn;
    private System.Data.DataTable dataTableUrsprung;
    private System.Data.DataColumn dataColumnSettingsUrsprungNamn;
    private System.Data.DataColumn dataColumnRegionId;
    private System.Data.DataColumn dataColumnRegion;
    private System.Data.DataColumn dataColumnDistriktId;
    private System.Data.DataColumn dataColumnDistrikt;
    private System.Data.DataColumn dataColumnUrsprung;
    private System.Data.DataColumn dataColumnDatum;
    private System.Data.DataColumn dataColumnTraktnr;
    private System.Data.DataColumn dataColumnTraktnamn;
    private System.Data.DataColumn dataColumnStandort;
    private System.Data.DataColumn dataColumnEntreprenor;
    private System.Data.DataColumn dataColumnLanguage;
    private System.Data.DataColumn dataColumnMailFrom;
    private System.Data.DataColumn dataColumnStatus;
    private System.Data.DataColumn dataColumnStatus_Datum;
    private System.Data.DataColumn dataColumnÅrtal;
    private System.Windows.Forms.FolderBrowserDialog saveReportFolderBrowserDialog;
    private System.Windows.Forms.TabPage tabPageBiobränsle;
    private System.Windows.Forms.GroupBox groupBoxRegion;
    private System.Windows.Forms.ComboBox comboBoxUrsprung;
    private System.Windows.Forms.DateTimePicker dateTimePicker;
    private System.Windows.Forms.ComboBox comboBoxDistrikt;
    private System.Windows.Forms.ComboBox comboBoxRegion;
    private System.Windows.Forms.Label labelDatum;
    private System.Windows.Forms.Label labelUrsprung;
    private System.Windows.Forms.Label labelDistrikt;
    private System.Windows.Forms.Label labelRegion;
    private System.Windows.Forms.GroupBox groupBoxTrakt;
    private System.Windows.Forms.TextBox textBoxTraktnr;
    private System.Windows.Forms.TextBox textBoxTraktnamn;
    private System.Windows.Forms.TextBox textBoxEntreprenor;
    private System.Windows.Forms.ComboBox comboBoxStandort;
    private System.Windows.Forms.Label labeEntreprenor;
    private System.Windows.Forms.Label labelStandort;
    private System.Windows.Forms.Label labelTraktnamn;
    private System.Windows.Forms.Label labelTraktnr;
    private System.Windows.Forms.GroupBox groupBoxBiobränsle;
    private System.Windows.Forms.Label labelFragaObs;
    private System.Windows.Forms.Label labelOBS;
    private FastReport.Report reportBiobränsle;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.TextBox textBoxBedömdVolym;
    private System.Windows.Forms.TextBox textBoxAvstånd;
    private System.Windows.Forms.TextBox textBoxAntalVältor;
    private System.Windows.Forms.TextBox textBoxÅker;
    private System.Windows.Forms.TextBox textBoxVägkant;
    private System.Windows.Forms.TextBox textBoxHygge;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.CheckBox checkBoxLatbilshugg;
    private System.Windows.Forms.CheckBox checkBoxGrotbil;
    private System.Windows.Forms.CheckBox checkBoxSkotarburenHugg;
    private System.Windows.Forms.CheckBox checkBoxTraktordragenHugg;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.GroupBox groupBox5;
    private System.Windows.Forms.GroupBox groupBox6;
    private System.Windows.Forms.GroupBox groupBox7;
    private System.Windows.Forms.TextBox textBoxAvlagg3Norr;
    private System.Windows.Forms.TextBox textBoxAvlagg2Norr;
    private System.Windows.Forms.TextBox textBoxAvlagg1Norr;
    private System.Windows.Forms.Label label14;
    private System.Windows.Forms.GroupBox groupBox9;
    private System.Windows.Forms.TextBox textBoxAvlagg6Ost;
    private System.Windows.Forms.TextBox textBoxAvlagg5Ost;
    private System.Windows.Forms.TextBox textBoxAvlagg4Ost;
    private System.Windows.Forms.GroupBox groupBox8;
    private System.Windows.Forms.TextBox textBoxAvlagg3Ost;
    private System.Windows.Forms.TextBox textBoxAvlagg2Ost;
    private System.Windows.Forms.TextBox textBoxAvlagg1Ost;
    private System.Windows.Forms.GroupBox groupBox10;
    private System.Windows.Forms.TextBox textBoxAvlagg6Norr;
    private System.Windows.Forms.TextBox textBoxAvlagg5Norr;
    private System.Windows.Forms.TextBox textBoxAvlagg4Norr;
    private System.Windows.Forms.Label label18;
    private System.Windows.Forms.Label label17;
    private System.Windows.Forms.Label label16;
    private System.Windows.Forms.Label label15;
    private System.Windows.Forms.Label label13;
    private System.Windows.Forms.TabPage tabPageFrågor2;
    private System.Windows.Forms.TabPage tabPageFrågor3;
    private System.Windows.Forms.GroupBox groupBox11;
    private System.Windows.Forms.RichTextBox richTextBoxOvrigt;
    private System.Data.DataColumn dataColumnAntalValtor;
    private System.Data.DataColumn dataColumnAvstand;
    private System.Data.DataColumn dataColumnBedomdVolym;
    private System.Data.DataColumn dataColumnHygge;
    private System.Data.DataColumn dataColumnVagkant;
    private System.Data.DataColumn dataColumnAker;
    private System.Data.DataColumn dataColumnLatbilshugg;
    private System.Data.DataColumn dataColumnGrotbil;
    private System.Data.DataColumn dataColumnTraktordragenHugg;
    private System.Data.DataColumn dataColumnSkotarburenHugg;
    private System.Data.DataColumn dataColumnAvlagg1Norr;
    private System.Data.DataColumn dataColumnAvlagg1Ost;
    private System.Data.DataColumn dataColumnAvlagg2Norr;
    private System.Data.DataColumn dataColumnAvlagg2Ost;
    private System.Data.DataColumn dataColumnAvlagg3Norr;
    private System.Data.DataColumn dataColumnAvlagg3Ost;
    private System.Data.DataColumn dataColumnAvlagg4Norr;
    private System.Data.DataColumn dataColumnAvlagg4Ost;
    private System.Data.DataColumn dataColumnAvlagg5Norr;
    private System.Data.DataColumn dataColumnAvlagg5Ost;
    private System.Data.DataColumn dataColumnAvlagg6Norr;
    private System.Data.DataColumn dataColumnAvlagg6Ost;
    private System.Data.DataColumn dataColumnFraga3;
    private System.Data.DataColumn dataColumnFraga4;
    private System.Data.DataColumn dataColumnFraga5;
    private System.Data.DataColumn dataColumnFraga6;
    private System.Data.DataColumn dataColumnFraga7;
    private System.Data.DataColumn dataColumnFraga8;
    private System.Data.DataColumn dataColumnFraga9;
    private System.Data.DataColumn dataColumnFraga10;
    private System.Data.DataColumn dataColumnFraga11;
    private System.Data.DataColumn dataColumnFraga12;
    private System.Data.DataColumn dataColumnFraga13;
    private System.Data.DataColumn dataColumnFraga14;
    private System.Data.DataColumn dataColumnFraga15;
    private System.Data.DataColumn dataColumnFraga16;
    private System.Data.DataColumn dataColumnFraga17;
    private System.Data.DataColumn dataColumnFraga18;
    private System.Data.DataColumn dataColumnFraga19;
    private System.Data.DataColumn dataColumnOvrigt;
    private System.Windows.Forms.ToolStripMenuItem arkivToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem sparaToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem sparaSomToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    private System.Windows.Forms.ToolStripMenuItem avslutaToolStripMenuItem;
    private System.Windows.Forms.GroupBox groupBox17;
    private System.Windows.Forms.RadioButton radioButton8Dålig;
    private System.Windows.Forms.RadioButton radioButton8Bra;
    private System.Windows.Forms.Label label25;
    private System.Windows.Forms.GroupBox groupBox16;
    private System.Windows.Forms.RadioButton radioButton7Dålig;
    private System.Windows.Forms.RadioButton radioButton7Bra;
    private System.Windows.Forms.Label label24;
    private System.Windows.Forms.GroupBox groupBox15;
    private System.Windows.Forms.RadioButton radioButton6Ejaktuellt;
    private System.Windows.Forms.RadioButton radioButton6Dålig;
    private System.Windows.Forms.RadioButton radioButton6Bra;
    private System.Windows.Forms.Label label23;
    private System.Windows.Forms.GroupBox groupBox14;
    private System.Windows.Forms.RadioButton radioButton5Dålig;
    private System.Windows.Forms.RadioButton radioButton5Bra;
    private System.Windows.Forms.Label label22;
    private System.Windows.Forms.GroupBox groupBox13;
    private System.Windows.Forms.RadioButton radioButton4Dålig;
    private System.Windows.Forms.RadioButton radioButton4Bra;
    private System.Windows.Forms.Label label21;
    private System.Windows.Forms.GroupBox groupBox12;
    private System.Windows.Forms.RadioButton radioButton3Dålig;
    private System.Windows.Forms.RadioButton radioButton3Bra;
    private System.Windows.Forms.Label label20;
    private System.Windows.Forms.Label label19;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.GroupBox groupBox4;
    private System.Windows.Forms.RadioButton radioButton2Nej;
    private System.Windows.Forms.RadioButton radioButton2Ja;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.GroupBox groupBox3;
    private System.Windows.Forms.RadioButton radioButton1Nej;
    private System.Windows.Forms.RadioButton radioButton1Ja;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.GroupBox groupBox25;
    private System.Windows.Forms.RadioButton radioButton16Nej;
    private System.Windows.Forms.RadioButton radioButton16Ja;
    private System.Windows.Forms.Label label34;
    private System.Windows.Forms.GroupBox groupBox24;
    private System.Windows.Forms.RadioButton radioButton15Nej;
    private System.Windows.Forms.RadioButton radioButton15Ja;
    private System.Windows.Forms.Label label33;
    private System.Windows.Forms.GroupBox groupBox23;
    private System.Windows.Forms.RadioButton radioButton14Nej;
    private System.Windows.Forms.RadioButton radioButton14Ja;
    private System.Windows.Forms.Label label32;
    private System.Windows.Forms.GroupBox groupBox22;
    private System.Windows.Forms.RadioButton radioButton13Nej;
    private System.Windows.Forms.RadioButton radioButton13Ja;
    private System.Windows.Forms.Label label31;
    private System.Windows.Forms.GroupBox groupBox21;
    private System.Windows.Forms.RadioButton radioButton12Nej;
    private System.Windows.Forms.RadioButton radioButton12Ja;
    private System.Windows.Forms.Label label30;
    private System.Windows.Forms.GroupBox groupBox20;
    private System.Windows.Forms.RadioButton radioButton11Nej;
    private System.Windows.Forms.RadioButton radioButton11Ja;
    private System.Windows.Forms.Label label29;
    private System.Windows.Forms.GroupBox groupBox19;
    private System.Windows.Forms.RadioButton radioButton10Nej;
    private System.Windows.Forms.RadioButton radioButton10Ja;
    private System.Windows.Forms.Label label28;
    private System.Windows.Forms.GroupBox groupBox18;
    private System.Windows.Forms.RadioButton radioButton9Nej;
    private System.Windows.Forms.RadioButton radioButton9Ja;
    private System.Windows.Forms.Label label27;
    private System.Windows.Forms.Label label26;
    private System.Windows.Forms.GroupBox groupBox28;
    private System.Windows.Forms.RadioButton radioButton19Nej;
    private System.Windows.Forms.RadioButton radioButton19Ja;
    private System.Windows.Forms.Label label37;
    private System.Windows.Forms.GroupBox groupBox27;
    private System.Windows.Forms.RadioButton radioButton18Nej;
    private System.Windows.Forms.RadioButton radioButton18Ja;
    private System.Windows.Forms.Label label36;
    private System.Windows.Forms.GroupBox groupBox26;
    private System.Windows.Forms.RadioButton radioButton17Nej;
    private System.Windows.Forms.RadioButton radioButton17Ja;
    private System.Windows.Forms.Label label35;
    private System.Windows.Forms.GroupBox groupBoxOBS;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.RadioButton radioButton15Ejaktuellt;
    private System.Windows.Forms.RadioButton radioButton14Ejaktuellt;
    private System.Windows.Forms.RadioButton radioButton13Ejaktuellt;
    private System.Windows.Forms.RadioButton radioButton12Ejaktuellt;
    private System.Windows.Forms.RadioButton radioButton16Ejaktuellt;
    private System.Windows.Forms.TextBox textBoxRegion;
    private System.Windows.Forms.TextBox textBoxDistrikt;
  }
}

