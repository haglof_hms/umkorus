﻿namespace Egenuppfoljning.Applications
{
  partial class GallringForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GallringForm));
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
        FastReport.Design.DesignerSettings designerSettings1 = new FastReport.Design.DesignerSettings();
        FastReport.Design.DesignerRestrictions designerRestrictions1 = new FastReport.Design.DesignerRestrictions();
        FastReport.Export.Email.EmailSettings emailSettings1 = new FastReport.Export.Email.EmailSettings();
        FastReport.PreviewSettings previewSettings1 = new FastReport.PreviewSettings();
        FastReport.ReportSettings reportSettings1 = new FastReport.ReportSettings();
        this.menuStripGallring = new System.Windows.Forms.MenuStrip();
        this.arkivToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.sparaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.sparaSomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
        this.avslutaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.openReportFileDialog = new System.Windows.Forms.OpenFileDialog();
        this.tabControl1 = new System.Windows.Forms.TabControl();
        this.tabPageGallring = new System.Windows.Forms.TabPage();
        this.groupBoxGallring = new System.Windows.Forms.GroupBox();
        this.label1 = new System.Windows.Forms.Label();
        this.comboBoxStickväg = new System.Windows.Forms.ComboBox();
        this.dataSetGallring = new System.Data.DataSet();
        this.dataTableYta = new System.Data.DataTable();
        this.dataColumnYta = new System.Data.DataColumn();
        this.dataColumnTall = new System.Data.DataColumn();
        this.dataColumnGran = new System.Data.DataColumn();
        this.dataColumnLov = new System.Data.DataColumn();
        this.dataColumnContorta = new System.Data.DataColumn();
        this.dataColumnSumma = new System.Data.DataColumn();
        this.dataColumnSkador = new System.Data.DataColumn();
        this.dataColumnStickvbredd = new System.Data.DataColumn();
        this.dataTableUserData = new System.Data.DataTable();
        this.dataColumnRegionId = new System.Data.DataColumn();
        this.dataColumnRegion2 = new System.Data.DataColumn();
        this.dataColumnDistriktId = new System.Data.DataColumn();
        this.dataColumnDistrikt = new System.Data.DataColumn();
        this.dataColumnUrsprung = new System.Data.DataColumn();
        this.dataColumnDatum = new System.Data.DataColumn();
        this.dataColumnTraktnr = new System.Data.DataColumn();
        this.dataColumnTraktnamn = new System.Data.DataColumn();
        this.dataColumnStandort = new System.Data.DataColumn();
        this.dataColumnEntreprenor = new System.Data.DataColumn();
        this.dataColumnKommentar = new System.Data.DataColumn();
        this.dataColumnLanguage = new System.Data.DataColumn();
        this.dataColumnMailFrom = new System.Data.DataColumn();
        this.dataColumnMalgrundyta = new System.Data.DataColumn();
        this.dataColumnStickvagsavstand = new System.Data.DataColumn();
        this.dataColumnStatus = new System.Data.DataColumn();
        this.dataColumnStatus_Datum = new System.Data.DataColumn();
        this.dataColumnArtal = new System.Data.DataColumn();
        this.dataColumnAreal = new System.Data.DataColumn();
        this.dataColumnStickvagssystem = new System.Data.DataColumn();
        this.dataTableFragor = new System.Data.DataTable();
        this.dataColumnFraga1 = new System.Data.DataColumn();
        this.dataColumnFraga2 = new System.Data.DataColumn();
        this.dataColumnFraga3 = new System.Data.DataColumn();
        this.dataColumnFraga4 = new System.Data.DataColumn();
        this.dataColumnFraga5 = new System.Data.DataColumn();
        this.dataColumnFraga6 = new System.Data.DataColumn();
        this.dataColumnFraga7 = new System.Data.DataColumn();
        this.dataColumnFraga8 = new System.Data.DataColumn();
        this.dataColumnFraga9 = new System.Data.DataColumn();
        this.dataColumnOvrigt = new System.Data.DataColumn();
        this.labelMaluppfyllelseProcent = new System.Windows.Forms.Label();
        this.labelMaluppfyllelse = new System.Windows.Forms.Label();
        this.textBoxMaluppfyllelse = new System.Windows.Forms.TextBox();
        this.labelArealHa = new System.Windows.Forms.Label();
        this.labelAreal = new System.Windows.Forms.Label();
        this.textBoxAreal = new System.Windows.Forms.TextBox();
        this.labelVagarealProcent = new System.Windows.Forms.Label();
        this.labelStickvagsavstandM = new System.Windows.Forms.Label();
        this.labelMalgrundytaM2Ha = new System.Windows.Forms.Label();
        this.textBoxStickvagsavstand = new System.Windows.Forms.TextBox();
        this.labelMalgrundyta = new System.Windows.Forms.Label();
        this.textBoxVagareal = new System.Windows.Forms.TextBox();
        this.labelVagareal = new System.Windows.Forms.Label();
        this.labelStickvagsavstand = new System.Windows.Forms.Label();
        this.textBoxMalgrundyta = new System.Windows.Forms.TextBox();
        this.groupBoxTrakt = new System.Windows.Forms.GroupBox();
        this.textBoxTraktnr = new System.Windows.Forms.TextBox();
        this.textBoxTraktnamn = new System.Windows.Forms.TextBox();
        this.textBoxEntreprenor = new System.Windows.Forms.TextBox();
        this.comboBoxStandort = new System.Windows.Forms.ComboBox();
        this.dataSetSettings = new System.Data.DataSet();
        this.dataTableRegion = new System.Data.DataTable();
        this.dataColumnSettingsRegionId = new System.Data.DataColumn();
        this.dataColumnSettingsRegionNamn = new System.Data.DataColumn();
        this.dataTableDistrikt = new System.Data.DataTable();
        this.dataColumnSettingsDistriktRegionId = new System.Data.DataColumn();
        this.dataColumnSettingsDistriktId = new System.Data.DataColumn();
        this.dataColumnSettingsDistriktNamn = new System.Data.DataColumn();
        this.dataTableStandort = new System.Data.DataTable();
        this.dataColumnSettingsStandortNamn = new System.Data.DataColumn();
        this.dataTableMarkberedningsmetod = new System.Data.DataTable();
        this.dataColumnSettingsMarkberedningsmetodNamn = new System.Data.DataColumn();
        this.dataTableUrsprung = new System.Data.DataTable();
        this.dataColumnSettingsUrsprungNamn = new System.Data.DataColumn();
        this.labeEntreprenor = new System.Windows.Forms.Label();
        this.labelStandort = new System.Windows.Forms.Label();
        this.labelTraktnamn = new System.Windows.Forms.Label();
        this.labelTraktnr = new System.Windows.Forms.Label();
        this.groupBoxRegion = new System.Windows.Forms.GroupBox();
        this.comboBoxRegion = new System.Windows.Forms.ComboBox();
        this.comboBoxUrsprung = new System.Windows.Forms.ComboBox();
        this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
        this.comboBoxDistrikt = new System.Windows.Forms.ComboBox();
        this.labelDatum = new System.Windows.Forms.Label();
        this.labelUrsprung = new System.Windows.Forms.Label();
        this.labelDistrikt = new System.Windows.Forms.Label();
        this.labelRegion = new System.Windows.Forms.Label();
        this.tabPageYtor = new System.Windows.Forms.TabPage();
        this.groupBoxGallringsstallen = new System.Windows.Forms.GroupBox();
        this.dataGridViewGallringsstallen = new System.Windows.Forms.DataGridView();
        this.ytaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.stickvbreddDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.tallDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.granDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.lovDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.contortaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.summaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.skadorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.groupBox10 = new System.Windows.Forms.GroupBox();
        this.textBoxMedelvardeSkador = new System.Windows.Forms.TextBox();
        this.groupBox9 = new System.Windows.Forms.GroupBox();
        this.label2 = new System.Windows.Forms.Label();
        this.groupBox11 = new System.Windows.Forms.GroupBox();
        this.textBoxMedelvardeSumma = new System.Windows.Forms.TextBox();
        this.groupBox8 = new System.Windows.Forms.GroupBox();
        this.textBoxSummaSkador = new System.Windows.Forms.TextBox();
        this.groupBox12 = new System.Windows.Forms.GroupBox();
        this.textBoxMedelvardeContorta = new System.Windows.Forms.TextBox();
        this.groupBox7 = new System.Windows.Forms.GroupBox();
        this.textBoxSumma = new System.Windows.Forms.TextBox();
        this.groupBox13 = new System.Windows.Forms.GroupBox();
        this.textBoxMedelvardeLov = new System.Windows.Forms.TextBox();
        this.groupBox6 = new System.Windows.Forms.GroupBox();
        this.textBoxSummaContorta = new System.Windows.Forms.TextBox();
        this.groupBox14 = new System.Windows.Forms.GroupBox();
        this.textBoxMedelvardeGran = new System.Windows.Forms.TextBox();
        this.groupBox5 = new System.Windows.Forms.GroupBox();
        this.textBoxSummaLov = new System.Windows.Forms.TextBox();
        this.groupBox15 = new System.Windows.Forms.GroupBox();
        this.textBoxMedelvardeTall = new System.Windows.Forms.TextBox();
        this.groupBox4 = new System.Windows.Forms.GroupBox();
        this.textBoxSummaGran = new System.Windows.Forms.TextBox();
        this.groupBox16 = new System.Windows.Forms.GroupBox();
        this.textBoxMedelvardeStickvbredd = new System.Windows.Forms.TextBox();
        this.groupBox3 = new System.Windows.Forms.GroupBox();
        this.textBoxSummaTall = new System.Windows.Forms.TextBox();
        this.groupBox2 = new System.Windows.Forms.GroupBox();
        this.label4 = new System.Windows.Forms.Label();
        this.groupBox1 = new System.Windows.Forms.GroupBox();
        this.textBoxSummaStickvbredd = new System.Windows.Forms.TextBox();
        this.tabPageFrågor1 = new System.Windows.Forms.TabPage();
        this.labelNaturSida1 = new System.Windows.Forms.Label();
        this.groupBoxFraga6 = new System.Windows.Forms.GroupBox();
        this.radioButton6Nej = new System.Windows.Forms.RadioButton();
        this.radioButton6Ja = new System.Windows.Forms.RadioButton();
        this.labelFraga6 = new System.Windows.Forms.Label();
        this.groupBoxFraga5 = new System.Windows.Forms.GroupBox();
        this.radioButton5Ejaktuellt = new System.Windows.Forms.RadioButton();
        this.radioButton5Nej = new System.Windows.Forms.RadioButton();
        this.radioButton5Ja = new System.Windows.Forms.RadioButton();
        this.labelFraga5 = new System.Windows.Forms.Label();
        this.groupBoxFraga4 = new System.Windows.Forms.GroupBox();
        this.radioButton4Nej = new System.Windows.Forms.RadioButton();
        this.radioButton4Ja = new System.Windows.Forms.RadioButton();
        this.labelFraga4b = new System.Windows.Forms.Label();
        this.labelFraga4a = new System.Windows.Forms.Label();
        this.groupBoxFraga3 = new System.Windows.Forms.GroupBox();
        this.radioButton3Ejaktuellt = new System.Windows.Forms.RadioButton();
        this.labelFraga3 = new System.Windows.Forms.Label();
        this.radioButton3Nej = new System.Windows.Forms.RadioButton();
        this.radioButton3Ja = new System.Windows.Forms.RadioButton();
        this.groupBoxFraga1 = new System.Windows.Forms.GroupBox();
        this.radioButton1Nej = new System.Windows.Forms.RadioButton();
        this.radioButton1Ja = new System.Windows.Forms.RadioButton();
        this.labelFraga1 = new System.Windows.Forms.Label();
        this.groupBoxFraga2 = new System.Windows.Forms.GroupBox();
        this.radioButton2Ejaktuellt = new System.Windows.Forms.RadioButton();
        this.radioButton2Nej = new System.Windows.Forms.RadioButton();
        this.radioButton2Ja = new System.Windows.Forms.RadioButton();
        this.labelFraga2 = new System.Windows.Forms.Label();
        this.tabPageFrågor2 = new System.Windows.Forms.TabPage();
        this.groupBoxKommentarer = new System.Windows.Forms.GroupBox();
        this.richTextBoxKommentarer = new System.Windows.Forms.RichTextBox();
        this.groupBoxOBS = new System.Windows.Forms.GroupBox();
        this.labelFragaObs = new System.Windows.Forms.Label();
        this.labelOBS = new System.Windows.Forms.Label();
        this.labelNaturSida2 = new System.Windows.Forms.Label();
        this.groupBoxFraga9 = new System.Windows.Forms.GroupBox();
        this.radioButton9Nej = new System.Windows.Forms.RadioButton();
        this.radioButton9Ja = new System.Windows.Forms.RadioButton();
        this.labelFraga9a = new System.Windows.Forms.Label();
        this.groupBoxFraga8 = new System.Windows.Forms.GroupBox();
        this.radioButton8Nej = new System.Windows.Forms.RadioButton();
        this.radioButton8Ja = new System.Windows.Forms.RadioButton();
        this.labelFraga8 = new System.Windows.Forms.Label();
        this.groupBoxFraga7 = new System.Windows.Forms.GroupBox();
        this.radioButton7Ejaktuellt = new System.Windows.Forms.RadioButton();
        this.radioButton7Nej = new System.Windows.Forms.RadioButton();
        this.radioButton7Ja = new System.Windows.Forms.RadioButton();
        this.labelFraga7 = new System.Windows.Forms.Label();
        this.environmentGallring = new FastReport.EnvironmentSettings();
        this.saveReportfolderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
        this.labelTotaltAntalYtor = new System.Windows.Forms.Label();
        this.labelProgress = new System.Windows.Forms.Label();
        this.progressBarData = new System.Windows.Forms.ProgressBar();
        this.reportGallring = new FastReport.Report();
        this.dataSetCalculations = new System.Data.DataSet();
        this.dataTableMedelvarde = new System.Data.DataTable();
        this.dataColumnMedelvardeStickvbredd = new System.Data.DataColumn();
        this.textBoxRegion = new System.Windows.Forms.TextBox();
        this.textBoxDistrikt = new System.Windows.Forms.TextBox();
        this.menuStripGallring.SuspendLayout();
        this.tabControl1.SuspendLayout();
        this.tabPageGallring.SuspendLayout();
        this.groupBoxGallring.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetGallring)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableYta)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableUserData)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFragor)).BeginInit();
        this.groupBoxTrakt.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetSettings)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableRegion)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableDistrikt)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableStandort)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableMarkberedningsmetod)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableUrsprung)).BeginInit();
        this.groupBoxRegion.SuspendLayout();
        this.tabPageYtor.SuspendLayout();
        this.groupBoxGallringsstallen.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGallringsstallen)).BeginInit();
        this.groupBox10.SuspendLayout();
        this.groupBox9.SuspendLayout();
        this.groupBox11.SuspendLayout();
        this.groupBox8.SuspendLayout();
        this.groupBox12.SuspendLayout();
        this.groupBox7.SuspendLayout();
        this.groupBox13.SuspendLayout();
        this.groupBox6.SuspendLayout();
        this.groupBox14.SuspendLayout();
        this.groupBox5.SuspendLayout();
        this.groupBox15.SuspendLayout();
        this.groupBox4.SuspendLayout();
        this.groupBox16.SuspendLayout();
        this.groupBox3.SuspendLayout();
        this.groupBox2.SuspendLayout();
        this.groupBox1.SuspendLayout();
        this.tabPageFrågor1.SuspendLayout();
        this.groupBoxFraga6.SuspendLayout();
        this.groupBoxFraga5.SuspendLayout();
        this.groupBoxFraga4.SuspendLayout();
        this.groupBoxFraga3.SuspendLayout();
        this.groupBoxFraga1.SuspendLayout();
        this.groupBoxFraga2.SuspendLayout();
        this.tabPageFrågor2.SuspendLayout();
        this.groupBoxKommentarer.SuspendLayout();
        this.groupBoxOBS.SuspendLayout();
        this.groupBoxFraga9.SuspendLayout();
        this.groupBoxFraga8.SuspendLayout();
        this.groupBoxFraga7.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.reportGallring)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetCalculations)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableMedelvarde)).BeginInit();
        this.SuspendLayout();
        // 
        // menuStripGallring
        // 
        this.menuStripGallring.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.arkivToolStripMenuItem});
        this.menuStripGallring.Location = new System.Drawing.Point(0, 0);
        this.menuStripGallring.Name = "menuStripGallring";
        this.menuStripGallring.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
        this.menuStripGallring.Size = new System.Drawing.Size(635, 24);
        this.menuStripGallring.TabIndex = 0;
        this.menuStripGallring.Text = "menuStrip1";
        // 
        // arkivToolStripMenuItem
        // 
        this.arkivToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sparaToolStripMenuItem,
            this.sparaSomToolStripMenuItem,
            this.toolStripSeparator2,
            this.avslutaToolStripMenuItem});
        this.arkivToolStripMenuItem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.arkivToolStripMenuItem.Name = "arkivToolStripMenuItem";
        this.arkivToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
        this.arkivToolStripMenuItem.Text = global::Egenuppfoljning.Properties.Resources.Arkiv;
        // 
        // sparaToolStripMenuItem
        // 
        this.sparaToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("sparaToolStripMenuItem.Image")));
        this.sparaToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
        this.sparaToolStripMenuItem.Name = "sparaToolStripMenuItem";
        this.sparaToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
        this.sparaToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
        this.sparaToolStripMenuItem.Text = global::Egenuppfoljning.Properties.Resources.Spara;
        this.sparaToolStripMenuItem.Click += new System.EventHandler(this.sparaToolStripMenuItem_Click);
        // 
        // sparaSomToolStripMenuItem
        // 
        this.sparaSomToolStripMenuItem.Name = "sparaSomToolStripMenuItem";
        this.sparaSomToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
        this.sparaSomToolStripMenuItem.Text = global::Egenuppfoljning.Properties.Resources.SparaSom;
        this.sparaSomToolStripMenuItem.Click += new System.EventHandler(this.sparaSomToolStripMenuItem_Click);
        // 
        // toolStripSeparator2
        // 
        this.toolStripSeparator2.Name = "toolStripSeparator2";
        this.toolStripSeparator2.Size = new System.Drawing.Size(147, 6);
        // 
        // avslutaToolStripMenuItem
        // 
        this.avslutaToolStripMenuItem.Name = "avslutaToolStripMenuItem";
        this.avslutaToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
        this.avslutaToolStripMenuItem.Text = "Stäng";
        this.avslutaToolStripMenuItem.Click += new System.EventHandler(this.avslutaToolStripMenuItem_Click);
        // 
        // tabControl1
        // 
        this.tabControl1.Controls.Add(this.tabPageGallring);
        this.tabControl1.Controls.Add(this.tabPageYtor);
        this.tabControl1.Controls.Add(this.tabPageFrågor1);
        this.tabControl1.Controls.Add(this.tabPageFrågor2);
        this.tabControl1.Location = new System.Drawing.Point(0, 27);
        this.tabControl1.Name = "tabControl1";
        this.tabControl1.SelectedIndex = 0;
        this.tabControl1.Size = new System.Drawing.Size(635, 432);
        this.tabControl1.TabIndex = 1;
        // 
        // tabPageGallring
        // 
        this.tabPageGallring.Controls.Add(this.groupBoxGallring);
        this.tabPageGallring.Controls.Add(this.groupBoxTrakt);
        this.tabPageGallring.Controls.Add(this.groupBoxRegion);
        this.tabPageGallring.Location = new System.Drawing.Point(4, 22);
        this.tabPageGallring.Name = "tabPageGallring";
        this.tabPageGallring.Size = new System.Drawing.Size(627, 406);
        this.tabPageGallring.TabIndex = 2;
        this.tabPageGallring.Text = "Gallring";
        this.tabPageGallring.UseVisualStyleBackColor = true;
        // 
        // groupBoxGallring
        // 
        this.groupBoxGallring.Controls.Add(this.label1);
        this.groupBoxGallring.Controls.Add(this.comboBoxStickväg);
        this.groupBoxGallring.Controls.Add(this.labelMaluppfyllelseProcent);
        this.groupBoxGallring.Controls.Add(this.labelMaluppfyllelse);
        this.groupBoxGallring.Controls.Add(this.textBoxMaluppfyllelse);
        this.groupBoxGallring.Controls.Add(this.labelArealHa);
        this.groupBoxGallring.Controls.Add(this.labelAreal);
        this.groupBoxGallring.Controls.Add(this.textBoxAreal);
        this.groupBoxGallring.Controls.Add(this.labelVagarealProcent);
        this.groupBoxGallring.Controls.Add(this.labelStickvagsavstandM);
        this.groupBoxGallring.Controls.Add(this.labelMalgrundytaM2Ha);
        this.groupBoxGallring.Controls.Add(this.textBoxStickvagsavstand);
        this.groupBoxGallring.Controls.Add(this.labelMalgrundyta);
        this.groupBoxGallring.Controls.Add(this.textBoxVagareal);
        this.groupBoxGallring.Controls.Add(this.labelVagareal);
        this.groupBoxGallring.Controls.Add(this.labelStickvagsavstand);
        this.groupBoxGallring.Controls.Add(this.textBoxMalgrundyta);
        this.groupBoxGallring.Location = new System.Drawing.Point(8, 121);
        this.groupBoxGallring.Name = "groupBoxGallring";
        this.groupBoxGallring.Size = new System.Drawing.Size(616, 139);
        this.groupBoxGallring.TabIndex = 2;
        this.groupBoxGallring.TabStop = false;
        // 
        // label1
        // 
        this.label1.AutoSize = true;
        this.label1.Location = new System.Drawing.Point(231, 18);
        this.label1.Name = "label1";
        this.label1.Size = new System.Drawing.Size(107, 13);
        this.label1.TabIndex = 3;
        this.label1.Text = "Stickvägssystem:";
        // 
        // comboBoxStickväg
        // 
        this.comboBoxStickväg.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetGallring, "UserData.Stickvagssystem", true));
        this.comboBoxStickväg.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxStickväg.FormattingEnabled = true;
        this.comboBoxStickväg.ItemHeight = 13;
        this.comboBoxStickväg.Items.AddRange(new object[] {
            "Stickväg",
            "Slingerväg"});
        this.comboBoxStickväg.Location = new System.Drawing.Point(234, 37);
        this.comboBoxStickväg.Name = "comboBoxStickväg";
        this.comboBoxStickväg.Size = new System.Drawing.Size(108, 21);
        this.comboBoxStickväg.TabIndex = 4;
        this.comboBoxStickväg.SelectionChangeCommitted += new System.EventHandler(this.comboBoxStickväg_SelectionChangeCommitted);
        this.comboBoxStickväg.SelectedIndexChanged += new System.EventHandler(this.comboBoxStickväg_SelectedIndexChanged);
        // 
        // dataSetGallring
        // 
        this.dataSetGallring.DataSetName = "DataSetGallring";
        this.dataSetGallring.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableYta,
            this.dataTableUserData,
            this.dataTableFragor});
        // 
        // dataTableYta
        // 
        this.dataTableYta.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnYta,
            this.dataColumnTall,
            this.dataColumnGran,
            this.dataColumnLov,
            this.dataColumnContorta,
            this.dataColumnSumma,
            this.dataColumnSkador,
            this.dataColumnStickvbredd});
        this.dataTableYta.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Yta"}, false)});
        this.dataTableYta.TableName = "Yta";
        // 
        // dataColumnYta
        // 
        this.dataColumnYta.AutoIncrementSeed = ((long)(2));
        this.dataColumnYta.ColumnName = "Yta";
        this.dataColumnYta.DataType = typeof(int);
        this.dataColumnYta.ReadOnly = true;
        // 
        // dataColumnTall
        // 
        this.dataColumnTall.ColumnName = "Tall";
        this.dataColumnTall.DataType = typeof(int);
        // 
        // dataColumnGran
        // 
        this.dataColumnGran.ColumnName = "Gran";
        this.dataColumnGran.DataType = typeof(int);
        // 
        // dataColumnLov
        // 
        this.dataColumnLov.ColumnName = "Lov";
        this.dataColumnLov.DataType = typeof(int);
        // 
        // dataColumnContorta
        // 
        this.dataColumnContorta.ColumnName = "Contorta";
        this.dataColumnContorta.DataType = typeof(int);
        // 
        // dataColumnSumma
        // 
        this.dataColumnSumma.ColumnName = "Summa";
        this.dataColumnSumma.DataType = typeof(int);
        // 
        // dataColumnSkador
        // 
        this.dataColumnSkador.ColumnName = "Skador";
        this.dataColumnSkador.DataType = typeof(int);
        // 
        // dataColumnStickvbredd
        // 
        this.dataColumnStickvbredd.ColumnName = "Stickvbredd";
        this.dataColumnStickvbredd.DataType = typeof(double);
        // 
        // dataTableUserData
        // 
        this.dataTableUserData.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnRegionId,
            this.dataColumnRegion2,
            this.dataColumnDistriktId,
            this.dataColumnDistrikt,
            this.dataColumnUrsprung,
            this.dataColumnDatum,
            this.dataColumnTraktnr,
            this.dataColumnTraktnamn,
            this.dataColumnStandort,
            this.dataColumnEntreprenor,
            this.dataColumnKommentar,
            this.dataColumnLanguage,
            this.dataColumnMailFrom,
            this.dataColumnMalgrundyta,
            this.dataColumnStickvagsavstand,
            this.dataColumnStatus,
            this.dataColumnStatus_Datum,
            this.dataColumnArtal,
            this.dataColumnAreal,
            this.dataColumnStickvagssystem});
        this.dataTableUserData.TableName = "UserData";
        // 
        // dataColumnRegionId
        // 
        this.dataColumnRegionId.ColumnName = "RegionId";
        this.dataColumnRegionId.DataType = typeof(int);
        // 
        // dataColumnRegion2
        // 
        this.dataColumnRegion2.ColumnName = "Region";
        // 
        // dataColumnDistriktId
        // 
        this.dataColumnDistriktId.ColumnName = "DistriktId";
        this.dataColumnDistriktId.DataType = typeof(int);
        // 
        // dataColumnDistrikt
        // 
        this.dataColumnDistrikt.ColumnName = "Distrikt";
        // 
        // dataColumnUrsprung
        // 
        this.dataColumnUrsprung.ColumnName = "Ursprung";
        // 
        // dataColumnDatum
        // 
        this.dataColumnDatum.ColumnName = "Datum";
        this.dataColumnDatum.DataType = typeof(System.DateTime);
        // 
        // dataColumnTraktnr
        // 
        this.dataColumnTraktnr.ColumnName = "Traktnr";
        this.dataColumnTraktnr.DataType = typeof(int);
        // 
        // dataColumnTraktnamn
        // 
        this.dataColumnTraktnamn.ColumnName = "Traktnamn";
        // 
        // dataColumnStandort
        // 
        this.dataColumnStandort.ColumnName = "Standort";
        this.dataColumnStandort.DataType = typeof(int);
        // 
        // dataColumnEntreprenor
        // 
        this.dataColumnEntreprenor.ColumnName = "Entreprenor";
        this.dataColumnEntreprenor.DataType = typeof(int);
        // 
        // dataColumnKommentar
        // 
        this.dataColumnKommentar.ColumnName = "Kommentar";
        // 
        // dataColumnLanguage
        // 
        this.dataColumnLanguage.ColumnName = "Language";
        // 
        // dataColumnMailFrom
        // 
        this.dataColumnMailFrom.ColumnName = "MailFrom";
        // 
        // dataColumnMalgrundyta
        // 
        this.dataColumnMalgrundyta.ColumnName = "Malgrundyta";
        this.dataColumnMalgrundyta.DataType = typeof(int);
        // 
        // dataColumnStickvagsavstand
        // 
        this.dataColumnStickvagsavstand.ColumnName = "Stickvagsavstand";
        this.dataColumnStickvagsavstand.DataType = typeof(int);
        // 
        // dataColumnStatus
        // 
        this.dataColumnStatus.ColumnName = "Status";
        // 
        // dataColumnStatus_Datum
        // 
        this.dataColumnStatus_Datum.ColumnName = "Status_Datum";
        this.dataColumnStatus_Datum.DataType = typeof(System.DateTime);
        // 
        // dataColumnArtal
        // 
        this.dataColumnArtal.ColumnName = "Artal";
        this.dataColumnArtal.DataType = typeof(int);
        // 
        // dataColumnAreal
        // 
        this.dataColumnAreal.ColumnName = "Areal";
        this.dataColumnAreal.DataType = typeof(double);
        // 
        // dataColumnStickvagssystem
        // 
        this.dataColumnStickvagssystem.ColumnName = "Stickvagssystem";
        // 
        // dataTableFragor
        // 
        this.dataTableFragor.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnFraga1,
            this.dataColumnFraga2,
            this.dataColumnFraga3,
            this.dataColumnFraga4,
            this.dataColumnFraga5,
            this.dataColumnFraga6,
            this.dataColumnFraga7,
            this.dataColumnFraga8,
            this.dataColumnFraga9,
            this.dataColumnOvrigt});
        this.dataTableFragor.TableName = "Fragor";
        // 
        // dataColumnFraga1
        // 
        this.dataColumnFraga1.Caption = "Fraga1";
        this.dataColumnFraga1.ColumnName = "Fraga1";
        // 
        // dataColumnFraga2
        // 
        this.dataColumnFraga2.ColumnName = "Fraga2";
        // 
        // dataColumnFraga3
        // 
        this.dataColumnFraga3.ColumnName = "Fraga3";
        // 
        // dataColumnFraga4
        // 
        this.dataColumnFraga4.ColumnName = "Fraga4";
        // 
        // dataColumnFraga5
        // 
        this.dataColumnFraga5.ColumnName = "Fraga5";
        // 
        // dataColumnFraga6
        // 
        this.dataColumnFraga6.ColumnName = "Fraga6";
        // 
        // dataColumnFraga7
        // 
        this.dataColumnFraga7.ColumnName = "Fraga7";
        // 
        // dataColumnFraga8
        // 
        this.dataColumnFraga8.ColumnName = "Fraga8";
        // 
        // dataColumnFraga9
        // 
        this.dataColumnFraga9.ColumnName = "Fraga9";
        // 
        // dataColumnOvrigt
        // 
        this.dataColumnOvrigt.ColumnName = "Ovrigt";
        // 
        // labelMaluppfyllelseProcent
        // 
        this.labelMaluppfyllelseProcent.AutoSize = true;
        this.labelMaluppfyllelseProcent.Location = new System.Drawing.Point(545, 93);
        this.labelMaluppfyllelseProcent.Name = "labelMaluppfyllelseProcent";
        this.labelMaluppfyllelseProcent.Size = new System.Drawing.Size(20, 13);
        this.labelMaluppfyllelseProcent.TabIndex = 16;
        this.labelMaluppfyllelseProcent.Text = "%";
        // 
        // labelMaluppfyllelse
        // 
        this.labelMaluppfyllelse.AutoSize = true;
        this.labelMaluppfyllelse.Location = new System.Drawing.Point(479, 78);
        this.labelMaluppfyllelse.Name = "labelMaluppfyllelse";
        this.labelMaluppfyllelse.Size = new System.Drawing.Size(91, 13);
        this.labelMaluppfyllelse.TabIndex = 14;
        this.labelMaluppfyllelse.Text = "Måluppfyllelse:";
        // 
        // textBoxMaluppfyllelse
        // 
        this.textBoxMaluppfyllelse.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxMaluppfyllelse.Location = new System.Drawing.Point(482, 96);
        this.textBoxMaluppfyllelse.Name = "textBoxMaluppfyllelse";
        this.textBoxMaluppfyllelse.ReadOnly = true;
        this.textBoxMaluppfyllelse.Size = new System.Drawing.Size(63, 21);
        this.textBoxMaluppfyllelse.TabIndex = 15;
        this.textBoxMaluppfyllelse.TabStop = false;
        this.textBoxMaluppfyllelse.Text = "0";
        this.textBoxMaluppfyllelse.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // labelArealHa
        // 
        this.labelArealHa.AutoSize = true;
        this.labelArealHa.Location = new System.Drawing.Point(81, 41);
        this.labelArealHa.Name = "labelArealHa";
        this.labelArealHa.Size = new System.Drawing.Size(21, 13);
        this.labelArealHa.TabIndex = 2;
        this.labelArealHa.Text = "ha";
        // 
        // labelAreal
        // 
        this.labelAreal.AutoSize = true;
        this.labelAreal.Location = new System.Drawing.Point(13, 17);
        this.labelAreal.Name = "labelAreal";
        this.labelAreal.Size = new System.Drawing.Size(40, 13);
        this.labelAreal.TabIndex = 0;
        this.labelAreal.Text = "Areal:";
        // 
        // textBoxAreal
        // 
        this.textBoxAreal.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetGallring, "UserData.Areal", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "N1"));
        this.textBoxAreal.Location = new System.Drawing.Point(16, 35);
        this.textBoxAreal.MaxLength = 16;
        this.textBoxAreal.Name = "textBoxAreal";
        this.textBoxAreal.Size = new System.Drawing.Size(63, 21);
        this.textBoxAreal.TabIndex = 1;
        this.textBoxAreal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        this.textBoxAreal.TextChanged += new System.EventHandler(this.textBoxAreal_TextChanged);
        this.textBoxAreal.Validated += new System.EventHandler(this.textBoxAreal_Validated);
        this.textBoxAreal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxAreal_KeyDown);
        this.textBoxAreal.Leave += new System.EventHandler(this.textBoxAreal_Leave);
        // 
        // labelVagarealProcent
        // 
        this.labelVagarealProcent.AutoSize = true;
        this.labelVagarealProcent.Location = new System.Drawing.Point(548, 40);
        this.labelVagarealProcent.Name = "labelVagarealProcent";
        this.labelVagarealProcent.Size = new System.Drawing.Size(20, 13);
        this.labelVagarealProcent.TabIndex = 7;
        this.labelVagarealProcent.Text = "%";
        // 
        // labelStickvagsavstandM
        // 
        this.labelStickvagsavstandM.AutoSize = true;
        this.labelStickvagsavstandM.Location = new System.Drawing.Point(303, 104);
        this.labelStickvagsavstandM.Name = "labelStickvagsavstandM";
        this.labelStickvagsavstandM.Size = new System.Drawing.Size(138, 13);
        this.labelStickvagsavstandM.TabIndex = 13;
        this.labelStickvagsavstandM.Text = "m (från skördarspåret)";
        // 
        // labelMalgrundytaM2Ha
        // 
        this.labelMalgrundytaM2Ha.AutoSize = true;
        this.labelMalgrundytaM2Ha.Location = new System.Drawing.Point(82, 101);
        this.labelMalgrundytaM2Ha.Name = "labelMalgrundytaM2Ha";
        this.labelMalgrundytaM2Ha.Size = new System.Drawing.Size(45, 13);
        this.labelMalgrundytaM2Ha.TabIndex = 10;
        this.labelMalgrundytaM2Ha.Text = "m2/ha";
        // 
        // textBoxStickvagsavstand
        // 
        this.textBoxStickvagsavstand.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetGallring, "UserData.Stickvagsavstand", true));
        this.textBoxStickvagsavstand.Location = new System.Drawing.Point(234, 98);
        this.textBoxStickvagsavstand.MaxLength = 16;
        this.textBoxStickvagsavstand.Name = "textBoxStickvagsavstand";
        this.textBoxStickvagsavstand.Size = new System.Drawing.Size(63, 21);
        this.textBoxStickvagsavstand.TabIndex = 12;
        this.textBoxStickvagsavstand.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        this.textBoxStickvagsavstand.TextChanged += new System.EventHandler(this.textBoxStickvagsavstand_TextChanged);
        this.textBoxStickvagsavstand.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxStickvagsavstand_KeyDown);
        this.textBoxStickvagsavstand.Leave += new System.EventHandler(this.textBoxStickvagsavstand_Leave);
        // 
        // labelMalgrundyta
        // 
        this.labelMalgrundyta.AutoSize = true;
        this.labelMalgrundyta.Location = new System.Drawing.Point(15, 78);
        this.labelMalgrundyta.Name = "labelMalgrundyta";
        this.labelMalgrundyta.Size = new System.Drawing.Size(158, 13);
        this.labelMalgrundyta.TabIndex = 8;
        this.labelMalgrundyta.Text = "Målgrundyta efter gallring:";
        // 
        // textBoxVagareal
        // 
        this.textBoxVagareal.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxVagareal.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxVagareal.Location = new System.Drawing.Point(482, 35);
        this.textBoxVagareal.Name = "textBoxVagareal";
        this.textBoxVagareal.ReadOnly = true;
        this.textBoxVagareal.Size = new System.Drawing.Size(63, 21);
        this.textBoxVagareal.TabIndex = 6;
        this.textBoxVagareal.TabStop = false;
        this.textBoxVagareal.Text = "0";
        this.textBoxVagareal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // labelVagareal
        // 
        this.labelVagareal.AutoSize = true;
        this.labelVagareal.Location = new System.Drawing.Point(479, 17);
        this.labelVagareal.Name = "labelVagareal";
        this.labelVagareal.Size = new System.Drawing.Size(60, 13);
        this.labelVagareal.TabIndex = 5;
        this.labelVagareal.Text = "Vägareal:";
        // 
        // labelStickvagsavstand
        // 
        this.labelStickvagsavstand.AutoSize = true;
        this.labelStickvagsavstand.Location = new System.Drawing.Point(231, 80);
        this.labelStickvagsavstand.Name = "labelStickvagsavstand";
        this.labelStickvagsavstand.Size = new System.Drawing.Size(111, 13);
        this.labelStickvagsavstand.TabIndex = 11;
        this.labelStickvagsavstand.Text = "Stickvägsavstånd:";
        // 
        // textBoxMalgrundyta
        // 
        this.textBoxMalgrundyta.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetGallring, "UserData.Malgrundyta", true));
        this.textBoxMalgrundyta.Location = new System.Drawing.Point(18, 96);
        this.textBoxMalgrundyta.MaxLength = 16;
        this.textBoxMalgrundyta.Name = "textBoxMalgrundyta";
        this.textBoxMalgrundyta.Size = new System.Drawing.Size(63, 21);
        this.textBoxMalgrundyta.TabIndex = 9;
        this.textBoxMalgrundyta.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        this.textBoxMalgrundyta.TextChanged += new System.EventHandler(this.textBoxMalgrundyta_TextChanged);
        this.textBoxMalgrundyta.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxMalgrundyta_KeyDown);
        this.textBoxMalgrundyta.Leave += new System.EventHandler(this.textBoxMalgrundyta_Leave);
        // 
        // groupBoxTrakt
        // 
        this.groupBoxTrakt.Controls.Add(this.textBoxTraktnr);
        this.groupBoxTrakt.Controls.Add(this.textBoxTraktnamn);
        this.groupBoxTrakt.Controls.Add(this.textBoxEntreprenor);
        this.groupBoxTrakt.Controls.Add(this.comboBoxStandort);
        this.groupBoxTrakt.Controls.Add(this.labeEntreprenor);
        this.groupBoxTrakt.Controls.Add(this.labelStandort);
        this.groupBoxTrakt.Controls.Add(this.labelTraktnamn);
        this.groupBoxTrakt.Controls.Add(this.labelTraktnr);
        this.groupBoxTrakt.Location = new System.Drawing.Point(8, 64);
        this.groupBoxTrakt.Name = "groupBoxTrakt";
        this.groupBoxTrakt.Size = new System.Drawing.Size(616, 72);
        this.groupBoxTrakt.TabIndex = 1;
        this.groupBoxTrakt.TabStop = false;
        // 
        // textBoxTraktnr
        // 
        this.textBoxTraktnr.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetGallring, "UserData.Traktnr", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, null, "000000"));
        this.textBoxTraktnr.Location = new System.Drawing.Point(15, 29);
        this.textBoxTraktnr.MaxLength = 6;
        this.textBoxTraktnr.Name = "textBoxTraktnr";
        this.textBoxTraktnr.Size = new System.Drawing.Size(116, 21);
        this.textBoxTraktnr.TabIndex = 1;
        this.textBoxTraktnr.TextChanged += new System.EventHandler(this.textBoxTraktnr_TextChanged);
        this.textBoxTraktnr.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxTraktnr_KeyDown);
        this.textBoxTraktnr.Leave += new System.EventHandler(this.textBoxTraktnr_Leave);
        // 
        // textBoxTraktnamn
        // 
        this.textBoxTraktnamn.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetGallring, "UserData.Traktnamn", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.textBoxTraktnamn.Location = new System.Drawing.Point(171, 29);
        this.textBoxTraktnamn.MaxLength = 31;
        this.textBoxTraktnamn.Name = "textBoxTraktnamn";
        this.textBoxTraktnamn.Size = new System.Drawing.Size(116, 21);
        this.textBoxTraktnamn.TabIndex = 3;
        this.textBoxTraktnamn.TextChanged += new System.EventHandler(this.textBoxTraktnamn_TextChanged);
        // 
        // textBoxEntreprenor
        // 
        this.textBoxEntreprenor.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetGallring, "UserData.Entreprenor", true, System.Windows.Forms.DataSourceUpdateMode.OnValidation, null, "0000"));
        this.textBoxEntreprenor.Location = new System.Drawing.Point(483, 29);
        this.textBoxEntreprenor.MaxLength = 4;
        this.textBoxEntreprenor.Name = "textBoxEntreprenor";
        this.textBoxEntreprenor.Size = new System.Drawing.Size(116, 21);
        this.textBoxEntreprenor.TabIndex = 7;
        this.textBoxEntreprenor.TextChanged += new System.EventHandler(this.textBoxEntreprenor_TextChanged);
        this.textBoxEntreprenor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxEntreprenor_KeyDown);
        this.textBoxEntreprenor.Leave += new System.EventHandler(this.textBoxEntreprenor_Leave);
        // 
        // comboBoxStandort
        // 
        this.comboBoxStandort.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetGallring, "UserData.Standort", true));
        this.comboBoxStandort.DataSource = this.dataSetSettings;
        this.comboBoxStandort.DisplayMember = "Standort.Id";
        this.comboBoxStandort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxStandort.FormattingEnabled = true;
        this.comboBoxStandort.Location = new System.Drawing.Point(328, 29);
        this.comboBoxStandort.Name = "comboBoxStandort";
        this.comboBoxStandort.Size = new System.Drawing.Size(116, 21);
        this.comboBoxStandort.TabIndex = 5;
        this.comboBoxStandort.SelectionChangeCommitted += new System.EventHandler(this.comboBoxStandort_SelectionChangeCommitted);
        this.comboBoxStandort.SelectedIndexChanged += new System.EventHandler(this.comboBoxStandort_SelectedIndexChanged);
        // 
        // dataSetSettings
        // 
        this.dataSetSettings.DataSetName = "DataSetSettings";
        this.dataSetSettings.Relations.AddRange(new System.Data.DataRelation[] {
            new System.Data.DataRelation("RelationRegionDistrikt", "Region", "Distrikt", new string[] {
                        "Id"}, new string[] {
                        "RegionId"}, false)});
        this.dataSetSettings.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableRegion,
            this.dataTableDistrikt,
            this.dataTableStandort,
            this.dataTableMarkberedningsmetod,
            this.dataTableUrsprung});
        // 
        // dataTableRegion
        // 
        this.dataTableRegion.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSettingsRegionId,
            this.dataColumnSettingsRegionNamn});
        this.dataTableRegion.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, false)});
        this.dataTableRegion.TableName = "Region";
        // 
        // dataColumnSettingsRegionId
        // 
        this.dataColumnSettingsRegionId.ColumnName = "Id";
        this.dataColumnSettingsRegionId.DataType = typeof(int);
        // 
        // dataColumnSettingsRegionNamn
        // 
        this.dataColumnSettingsRegionNamn.ColumnName = "Namn";
        // 
        // dataTableDistrikt
        // 
        this.dataTableDistrikt.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSettingsDistriktRegionId,
            this.dataColumnSettingsDistriktId,
            this.dataColumnSettingsDistriktNamn});
        this.dataTableDistrikt.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.ForeignKeyConstraint("RelationRegionDistrikt", "Region", new string[] {
                        "Id"}, new string[] {
                        "RegionId"}, System.Data.AcceptRejectRule.None, System.Data.Rule.Cascade, System.Data.Rule.Cascade)});
        this.dataTableDistrikt.TableName = "Distrikt";
        // 
        // dataColumnSettingsDistriktRegionId
        // 
        this.dataColumnSettingsDistriktRegionId.ColumnName = "RegionId";
        this.dataColumnSettingsDistriktRegionId.DataType = typeof(int);
        // 
        // dataColumnSettingsDistriktId
        // 
        this.dataColumnSettingsDistriktId.ColumnName = "Id";
        this.dataColumnSettingsDistriktId.DataType = typeof(int);
        // 
        // dataColumnSettingsDistriktNamn
        // 
        this.dataColumnSettingsDistriktNamn.ColumnName = "Namn";
        // 
        // dataTableStandort
        // 
        this.dataTableStandort.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSettingsStandortNamn});
        this.dataTableStandort.TableName = "Standort";
        // 
        // dataColumnSettingsStandortNamn
        // 
        this.dataColumnSettingsStandortNamn.ColumnName = "Id";
        this.dataColumnSettingsStandortNamn.DataType = typeof(int);
        // 
        // dataTableMarkberedningsmetod
        // 
        this.dataTableMarkberedningsmetod.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSettingsMarkberedningsmetodNamn});
        this.dataTableMarkberedningsmetod.TableName = "Markberedningsmetod";
        // 
        // dataColumnSettingsMarkberedningsmetodNamn
        // 
        this.dataColumnSettingsMarkberedningsmetodNamn.ColumnName = "Namn";
        // 
        // dataTableUrsprung
        // 
        this.dataTableUrsprung.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSettingsUrsprungNamn});
        this.dataTableUrsprung.TableName = "Ursprung";
        // 
        // dataColumnSettingsUrsprungNamn
        // 
        this.dataColumnSettingsUrsprungNamn.ColumnName = "Namn";
        // 
        // labeEntreprenor
        // 
        this.labeEntreprenor.AutoSize = true;
        this.labeEntreprenor.Location = new System.Drawing.Point(479, 12);
        this.labeEntreprenor.Name = "labeEntreprenor";
        this.labeEntreprenor.Size = new System.Drawing.Size(133, 13);
        this.labeEntreprenor.TabIndex = 6;
        this.labeEntreprenor.Text = "Maskinnr/Entreprenör";
        // 
        // labelStandort
        // 
        this.labelStandort.AutoSize = true;
        this.labelStandort.Location = new System.Drawing.Point(325, 13);
        this.labelStandort.Name = "labelStandort";
        this.labelStandort.Size = new System.Drawing.Size(57, 13);
        this.labelStandort.TabIndex = 4;
        this.labelStandort.Text = "Ståndort";
        // 
        // labelTraktnamn
        // 
        this.labelTraktnamn.AutoSize = true;
        this.labelTraktnamn.Location = new System.Drawing.Point(171, 13);
        this.labelTraktnamn.Name = "labelTraktnamn";
        this.labelTraktnamn.Size = new System.Drawing.Size(70, 13);
        this.labelTraktnamn.TabIndex = 2;
        this.labelTraktnamn.Text = "Traktnamn";
        // 
        // labelTraktnr
        // 
        this.labelTraktnr.AutoSize = true;
        this.labelTraktnr.Location = new System.Drawing.Point(13, 14);
        this.labelTraktnr.Name = "labelTraktnr";
        this.labelTraktnr.Size = new System.Drawing.Size(50, 13);
        this.labelTraktnr.TabIndex = 0;
        this.labelTraktnr.Text = "Traktnr";
        // 
        // groupBoxRegion
        // 
        this.groupBoxRegion.Controls.Add(this.textBoxDistrikt);
        this.groupBoxRegion.Controls.Add(this.textBoxRegion);
        this.groupBoxRegion.Controls.Add(this.comboBoxRegion);
        this.groupBoxRegion.Controls.Add(this.comboBoxUrsprung);
        this.groupBoxRegion.Controls.Add(this.dateTimePicker);
        this.groupBoxRegion.Controls.Add(this.comboBoxDistrikt);
        this.groupBoxRegion.Controls.Add(this.labelDatum);
        this.groupBoxRegion.Controls.Add(this.labelUrsprung);
        this.groupBoxRegion.Controls.Add(this.labelDistrikt);
        this.groupBoxRegion.Controls.Add(this.labelRegion);
        this.groupBoxRegion.Location = new System.Drawing.Point(8, 3);
        this.groupBoxRegion.Name = "groupBoxRegion";
        this.groupBoxRegion.Size = new System.Drawing.Size(616, 72);
        this.groupBoxRegion.TabIndex = 0;
        this.groupBoxRegion.TabStop = false;
        // 
        // comboBoxRegion
        // 
        this.comboBoxRegion.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetGallring, "UserData.Region", true));
        this.comboBoxRegion.DataSource = this.dataSetSettings;
        this.comboBoxRegion.DisplayMember = "Region.Namn";
        this.comboBoxRegion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxRegion.FormattingEnabled = true;
        this.comboBoxRegion.Location = new System.Drawing.Point(16, 30);
        this.comboBoxRegion.Name = "comboBoxRegion";
        this.comboBoxRegion.Size = new System.Drawing.Size(116, 21);
        this.comboBoxRegion.TabIndex = 1;
        this.comboBoxRegion.ValueMember = "Region.Id";
        this.comboBoxRegion.SelectionChangeCommitted += new System.EventHandler(this.comboBoxRegion_SelectionChangeCommitted);
        this.comboBoxRegion.SelectedIndexChanged += new System.EventHandler(this.comboBoxRegion_SelectedIndexChanged);
        // 
        // comboBoxUrsprung
        // 
        this.comboBoxUrsprung.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetGallring, "UserData.Ursprung", true));
        this.comboBoxUrsprung.DataSource = this.dataSetSettings;
        this.comboBoxUrsprung.DisplayMember = "Ursprung.Namn";
        this.comboBoxUrsprung.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxUrsprung.FormattingEnabled = true;
        this.comboBoxUrsprung.Location = new System.Drawing.Point(328, 30);
        this.comboBoxUrsprung.Name = "comboBoxUrsprung";
        this.comboBoxUrsprung.Size = new System.Drawing.Size(116, 21);
        this.comboBoxUrsprung.TabIndex = 5;
        this.comboBoxUrsprung.SelectionChangeCommitted += new System.EventHandler(this.comboBoxUrsprung_SelectionChangeCommitted);
        this.comboBoxUrsprung.SelectedIndexChanged += new System.EventHandler(this.comboBoxUrsprung_SelectedIndexChanged);
        // 
        // dateTimePicker
        // 
        this.dateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.dataSetGallring, "UserData.Datum", true));
        this.dateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
        this.dateTimePicker.Location = new System.Drawing.Point(484, 30);
        this.dateTimePicker.Name = "dateTimePicker";
        this.dateTimePicker.Size = new System.Drawing.Size(116, 21);
        this.dateTimePicker.TabIndex = 7;
        this.dateTimePicker.ValueChanged += new System.EventHandler(this.dateTimePicker_ValueChanged);
        // 
        // comboBoxDistrikt
        // 
        this.comboBoxDistrikt.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetGallring, "UserData.Distrikt", true));
        this.comboBoxDistrikt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxDistrikt.FormattingEnabled = true;
        this.comboBoxDistrikt.Location = new System.Drawing.Point(171, 30);
        this.comboBoxDistrikt.Name = "comboBoxDistrikt";
        this.comboBoxDistrikt.Size = new System.Drawing.Size(116, 21);
        this.comboBoxDistrikt.TabIndex = 3;
        this.comboBoxDistrikt.SelectionChangeCommitted += new System.EventHandler(this.comboBoxDistrikt_SelectionChangeCommitted);
        this.comboBoxDistrikt.SelectedIndexChanged += new System.EventHandler(this.comboBoxDistrikt_SelectedIndexChanged);
        // 
        // labelDatum
        // 
        this.labelDatum.AutoSize = true;
        this.labelDatum.Location = new System.Drawing.Point(480, 13);
        this.labelDatum.Name = "labelDatum";
        this.labelDatum.Size = new System.Drawing.Size(45, 13);
        this.labelDatum.TabIndex = 6;
        this.labelDatum.Text = "Datum";
        // 
        // labelUrsprung
        // 
        this.labelUrsprung.AutoSize = true;
        this.labelUrsprung.Location = new System.Drawing.Point(325, 14);
        this.labelUrsprung.Name = "labelUrsprung";
        this.labelUrsprung.Size = new System.Drawing.Size(59, 13);
        this.labelUrsprung.TabIndex = 4;
        this.labelUrsprung.Text = "Ursprung";
        // 
        // labelDistrikt
        // 
        this.labelDistrikt.AutoSize = true;
        this.labelDistrikt.Location = new System.Drawing.Point(171, 14);
        this.labelDistrikt.Name = "labelDistrikt";
        this.labelDistrikt.Size = new System.Drawing.Size(49, 13);
        this.labelDistrikt.TabIndex = 2;
        this.labelDistrikt.Text = "Distrikt";
        // 
        // labelRegion
        // 
        this.labelRegion.AutoSize = true;
        this.labelRegion.Location = new System.Drawing.Point(13, 14);
        this.labelRegion.Name = "labelRegion";
        this.labelRegion.Size = new System.Drawing.Size(46, 13);
        this.labelRegion.TabIndex = 0;
        this.labelRegion.Text = "Region";
        // 
        // tabPageYtor
        // 
        this.tabPageYtor.Controls.Add(this.groupBoxGallringsstallen);
        this.tabPageYtor.Location = new System.Drawing.Point(4, 22);
        this.tabPageYtor.Name = "tabPageYtor";
        this.tabPageYtor.Padding = new System.Windows.Forms.Padding(3);
        this.tabPageYtor.Size = new System.Drawing.Size(627, 406);
        this.tabPageYtor.TabIndex = 0;
        this.tabPageYtor.Text = "Ytor";
        this.tabPageYtor.UseVisualStyleBackColor = true;
        // 
        // groupBoxGallringsstallen
        // 
        this.groupBoxGallringsstallen.Controls.Add(this.dataGridViewGallringsstallen);
        this.groupBoxGallringsstallen.Controls.Add(this.groupBox10);
        this.groupBoxGallringsstallen.Controls.Add(this.groupBox9);
        this.groupBoxGallringsstallen.Controls.Add(this.groupBox11);
        this.groupBoxGallringsstallen.Controls.Add(this.groupBox8);
        this.groupBoxGallringsstallen.Controls.Add(this.groupBox12);
        this.groupBoxGallringsstallen.Controls.Add(this.groupBox7);
        this.groupBoxGallringsstallen.Controls.Add(this.groupBox13);
        this.groupBoxGallringsstallen.Controls.Add(this.groupBox6);
        this.groupBoxGallringsstallen.Controls.Add(this.groupBox14);
        this.groupBoxGallringsstallen.Controls.Add(this.groupBox5);
        this.groupBoxGallringsstallen.Controls.Add(this.groupBox15);
        this.groupBoxGallringsstallen.Controls.Add(this.groupBox4);
        this.groupBoxGallringsstallen.Controls.Add(this.groupBox16);
        this.groupBoxGallringsstallen.Controls.Add(this.groupBox3);
        this.groupBoxGallringsstallen.Controls.Add(this.groupBox2);
        this.groupBoxGallringsstallen.Controls.Add(this.groupBox1);
        this.groupBoxGallringsstallen.Location = new System.Drawing.Point(8, 6);
        this.groupBoxGallringsstallen.Name = "groupBoxGallringsstallen";
        this.groupBoxGallringsstallen.Size = new System.Drawing.Size(613, 390);
        this.groupBoxGallringsstallen.TabIndex = 0;
        this.groupBoxGallringsstallen.TabStop = false;
        this.groupBoxGallringsstallen.Text = "ANTALET GALLRINGSSTÄLLEN";
        // 
        // dataGridViewGallringsstallen
        // 
        this.dataGridViewGallringsstallen.AllowUserToResizeColumns = false;
        this.dataGridViewGallringsstallen.AllowUserToResizeRows = false;
        dataGridViewCellStyle1.NullValue = null;
        this.dataGridViewGallringsstallen.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
        this.dataGridViewGallringsstallen.AutoGenerateColumns = false;
        this.dataGridViewGallringsstallen.BackgroundColor = System.Drawing.SystemColors.ControlDark;
        this.dataGridViewGallringsstallen.BorderStyle = System.Windows.Forms.BorderStyle.None;
        dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.BottomCenter;
        dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
        dataGridViewCellStyle2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
        dataGridViewCellStyle2.NullValue = null;
        dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        this.dataGridViewGallringsstallen.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
        this.dataGridViewGallringsstallen.ColumnHeadersHeight = 55;
        this.dataGridViewGallringsstallen.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
        this.dataGridViewGallringsstallen.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ytaDataGridViewTextBoxColumn,
            this.stickvbreddDataGridViewTextBoxColumn,
            this.tallDataGridViewTextBoxColumn,
            this.granDataGridViewTextBoxColumn,
            this.lovDataGridViewTextBoxColumn,
            this.contortaDataGridViewTextBoxColumn,
            this.summaDataGridViewTextBoxColumn,
            this.skadorDataGridViewTextBoxColumn});
        this.dataGridViewGallringsstallen.DataMember = "Yta";
        this.dataGridViewGallringsstallen.DataSource = this.dataSetGallring;
        dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
        dataGridViewCellStyle11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
        dataGridViewCellStyle11.NullValue = null;
        dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
        this.dataGridViewGallringsstallen.DefaultCellStyle = dataGridViewCellStyle11;
        this.dataGridViewGallringsstallen.ImeMode = System.Windows.Forms.ImeMode.NoControl;
        this.dataGridViewGallringsstallen.Location = new System.Drawing.Point(19, 28);
        this.dataGridViewGallringsstallen.Name = "dataGridViewGallringsstallen";
        this.dataGridViewGallringsstallen.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
        dataGridViewCellStyle12.NullValue = null;
        this.dataGridViewGallringsstallen.RowsDefaultCellStyle = dataGridViewCellStyle12;
        this.dataGridViewGallringsstallen.RowTemplate.DefaultCellStyle.NullValue = null;
        this.dataGridViewGallringsstallen.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
        this.dataGridViewGallringsstallen.Size = new System.Drawing.Size(577, 261);
        this.dataGridViewGallringsstallen.TabIndex = 35;
        this.dataGridViewGallringsstallen.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dataGridViewGallringsstallen_UserDeletingRow);
        this.dataGridViewGallringsstallen.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dataGridViewGallringsstallen_UserDeletedRow);
        this.dataGridViewGallringsstallen.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dataGridViewGallringsstallen_CellValidating);
        this.dataGridViewGallringsstallen.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dataGridViewGallringsstallen_RowsAdded);
        this.dataGridViewGallringsstallen.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewGallringsstallen_CellEndEdit);
        this.dataGridViewGallringsstallen.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewGallringsstallen_DataError);
        // 
        // ytaDataGridViewTextBoxColumn
        // 
        this.ytaDataGridViewTextBoxColumn.DataPropertyName = "Yta";
        dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle3.BackColor = System.Drawing.Color.LemonChiffon;
        dataGridViewCellStyle3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.ytaDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle3;
        this.ytaDataGridViewTextBoxColumn.HeaderText = "Provyta";
        this.ytaDataGridViewTextBoxColumn.Name = "ytaDataGridViewTextBoxColumn";
        this.ytaDataGridViewTextBoxColumn.ReadOnly = true;
        this.ytaDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
        this.ytaDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        this.ytaDataGridViewTextBoxColumn.Width = 55;
        // 
        // stickvbreddDataGridViewTextBoxColumn
        // 
        this.stickvbreddDataGridViewTextBoxColumn.DataPropertyName = "Stickvbredd";
        dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle4.Format = "N1";
        dataGridViewCellStyle4.NullValue = null;
        this.stickvbreddDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle4;
        this.stickvbreddDataGridViewTextBoxColumn.HeaderText = "Stickvbredd";
        this.stickvbreddDataGridViewTextBoxColumn.Name = "stickvbreddDataGridViewTextBoxColumn";
        this.stickvbreddDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
        this.stickvbreddDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        this.stickvbreddDataGridViewTextBoxColumn.Width = 92;
        // 
        // tallDataGridViewTextBoxColumn
        // 
        this.tallDataGridViewTextBoxColumn.DataPropertyName = "Tall";
        dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
        dataGridViewCellStyle5.NullValue = null;
        this.tallDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle5;
        this.tallDataGridViewTextBoxColumn.HeaderText = "Tall";
        this.tallDataGridViewTextBoxColumn.Name = "tallDataGridViewTextBoxColumn";
        this.tallDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
        this.tallDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        this.tallDataGridViewTextBoxColumn.Width = 60;
        // 
        // granDataGridViewTextBoxColumn
        // 
        this.granDataGridViewTextBoxColumn.DataPropertyName = "Gran";
        dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle6.BackColor = System.Drawing.Color.White;
        dataGridViewCellStyle6.NullValue = null;
        this.granDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle6;
        this.granDataGridViewTextBoxColumn.HeaderText = "Gran";
        this.granDataGridViewTextBoxColumn.Name = "granDataGridViewTextBoxColumn";
        this.granDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
        this.granDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        this.granDataGridViewTextBoxColumn.Width = 60;
        // 
        // lovDataGridViewTextBoxColumn
        // 
        this.lovDataGridViewTextBoxColumn.DataPropertyName = "Lov";
        dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle7.BackColor = System.Drawing.Color.White;
        dataGridViewCellStyle7.NullValue = null;
        this.lovDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle7;
        this.lovDataGridViewTextBoxColumn.HeaderText = "Löv";
        this.lovDataGridViewTextBoxColumn.Name = "lovDataGridViewTextBoxColumn";
        this.lovDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
        this.lovDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        this.lovDataGridViewTextBoxColumn.Width = 60;
        // 
        // contortaDataGridViewTextBoxColumn
        // 
        this.contortaDataGridViewTextBoxColumn.DataPropertyName = "Contorta";
        dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
        dataGridViewCellStyle8.NullValue = null;
        this.contortaDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle8;
        this.contortaDataGridViewTextBoxColumn.HeaderText = "Contorta";
        this.contortaDataGridViewTextBoxColumn.Name = "contortaDataGridViewTextBoxColumn";
        this.contortaDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
        this.contortaDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        this.contortaDataGridViewTextBoxColumn.Width = 64;
        // 
        // summaDataGridViewTextBoxColumn
        // 
        this.summaDataGridViewTextBoxColumn.DataPropertyName = "Summa";
        dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle9.BackColor = System.Drawing.Color.LemonChiffon;
        this.summaDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle9;
        this.summaDataGridViewTextBoxColumn.HeaderText = "Summa";
        this.summaDataGridViewTextBoxColumn.Name = "summaDataGridViewTextBoxColumn";
        this.summaDataGridViewTextBoxColumn.ReadOnly = true;
        this.summaDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
        this.summaDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        this.summaDataGridViewTextBoxColumn.Width = 64;
        // 
        // skadorDataGridViewTextBoxColumn
        // 
        this.skadorDataGridViewTextBoxColumn.DataPropertyName = "Skador";
        dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
        this.skadorDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle10;
        this.skadorDataGridViewTextBoxColumn.HeaderText = "Skador %";
        this.skadorDataGridViewTextBoxColumn.Name = "skadorDataGridViewTextBoxColumn";
        this.skadorDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
        this.skadorDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        this.skadorDataGridViewTextBoxColumn.Width = 70;
        // 
        // groupBox10
        // 
        this.groupBox10.Controls.Add(this.textBoxMedelvardeSkador);
        this.groupBox10.Location = new System.Drawing.Point(516, 320);
        this.groupBox10.Name = "groupBox10";
        this.groupBox10.Size = new System.Drawing.Size(79, 50);
        this.groupBox10.TabIndex = 34;
        this.groupBox10.TabStop = false;
        // 
        // textBoxMedelvardeSkador
        // 
        this.textBoxMedelvardeSkador.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxMedelvardeSkador.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxMedelvardeSkador.Location = new System.Drawing.Point(1, 19);
        this.textBoxMedelvardeSkador.Name = "textBoxMedelvardeSkador";
        this.textBoxMedelvardeSkador.ReadOnly = true;
        this.textBoxMedelvardeSkador.Size = new System.Drawing.Size(78, 21);
        this.textBoxMedelvardeSkador.TabIndex = 11;
        this.textBoxMedelvardeSkador.TabStop = false;
        this.textBoxMedelvardeSkador.Text = "0";
        this.textBoxMedelvardeSkador.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        // 
        // groupBox9
        // 
        this.groupBox9.Controls.Add(this.label2);
        this.groupBox9.Location = new System.Drawing.Point(18, 320);
        this.groupBox9.Name = "groupBox9";
        this.groupBox9.Size = new System.Drawing.Size(93, 50);
        this.groupBox9.TabIndex = 30;
        this.groupBox9.TabStop = false;
        // 
        // label2
        // 
        this.label2.AutoSize = true;
        this.label2.Location = new System.Drawing.Point(5, 22);
        this.label2.Name = "label2";
        this.label2.Size = new System.Drawing.Size(83, 13);
        this.label2.TabIndex = 14;
        this.label2.Text = "MEDELVÄRDE:";
        // 
        // groupBox11
        // 
        this.groupBox11.Controls.Add(this.textBoxMedelvardeSumma);
        this.groupBox11.Location = new System.Drawing.Point(449, 320);
        this.groupBox11.Name = "groupBox11";
        this.groupBox11.Size = new System.Drawing.Size(66, 50);
        this.groupBox11.TabIndex = 33;
        this.groupBox11.TabStop = false;
        // 
        // textBoxMedelvardeSumma
        // 
        this.textBoxMedelvardeSumma.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxMedelvardeSumma.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxMedelvardeSumma.Location = new System.Drawing.Point(1, 20);
        this.textBoxMedelvardeSumma.Name = "textBoxMedelvardeSumma";
        this.textBoxMedelvardeSumma.ReadOnly = true;
        this.textBoxMedelvardeSumma.Size = new System.Drawing.Size(64, 21);
        this.textBoxMedelvardeSumma.TabIndex = 9;
        this.textBoxMedelvardeSumma.TabStop = false;
        this.textBoxMedelvardeSumma.Text = "0";
        this.textBoxMedelvardeSumma.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        // 
        // groupBox8
        // 
        this.groupBox8.Controls.Add(this.textBoxSummaSkador);
        this.groupBox8.Location = new System.Drawing.Point(517, 278);
        this.groupBox8.Name = "groupBox8";
        this.groupBox8.Size = new System.Drawing.Size(79, 50);
        this.groupBox8.TabIndex = 26;
        this.groupBox8.TabStop = false;
        this.groupBox8.Text = "Skador %";
        // 
        // textBoxSummaSkador
        // 
        this.textBoxSummaSkador.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxSummaSkador.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxSummaSkador.Location = new System.Drawing.Point(0, 20);
        this.textBoxSummaSkador.Name = "textBoxSummaSkador";
        this.textBoxSummaSkador.ReadOnly = true;
        this.textBoxSummaSkador.Size = new System.Drawing.Size(78, 21);
        this.textBoxSummaSkador.TabIndex = 11;
        this.textBoxSummaSkador.TabStop = false;
        this.textBoxSummaSkador.Text = "0";
        this.textBoxSummaSkador.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        // 
        // groupBox12
        // 
        this.groupBox12.Controls.Add(this.textBoxMedelvardeContorta);
        this.groupBox12.Location = new System.Drawing.Point(388, 320);
        this.groupBox12.Name = "groupBox12";
        this.groupBox12.Size = new System.Drawing.Size(60, 50);
        this.groupBox12.TabIndex = 32;
        this.groupBox12.TabStop = false;
        // 
        // textBoxMedelvardeContorta
        // 
        this.textBoxMedelvardeContorta.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxMedelvardeContorta.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxMedelvardeContorta.Location = new System.Drawing.Point(1, 19);
        this.textBoxMedelvardeContorta.Name = "textBoxMedelvardeContorta";
        this.textBoxMedelvardeContorta.ReadOnly = true;
        this.textBoxMedelvardeContorta.Size = new System.Drawing.Size(60, 21);
        this.textBoxMedelvardeContorta.TabIndex = 7;
        this.textBoxMedelvardeContorta.TabStop = false;
        this.textBoxMedelvardeContorta.Text = "0";
        this.textBoxMedelvardeContorta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        // 
        // groupBox7
        // 
        this.groupBox7.Controls.Add(this.textBoxSumma);
        this.groupBox7.Location = new System.Drawing.Point(450, 278);
        this.groupBox7.Name = "groupBox7";
        this.groupBox7.Size = new System.Drawing.Size(66, 50);
        this.groupBox7.TabIndex = 25;
        this.groupBox7.TabStop = false;
        this.groupBox7.Text = "Summa";
        // 
        // textBoxSumma
        // 
        this.textBoxSumma.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxSumma.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxSumma.Location = new System.Drawing.Point(0, 20);
        this.textBoxSumma.Name = "textBoxSumma";
        this.textBoxSumma.ReadOnly = true;
        this.textBoxSumma.Size = new System.Drawing.Size(66, 21);
        this.textBoxSumma.TabIndex = 9;
        this.textBoxSumma.TabStop = false;
        this.textBoxSumma.Text = "0";
        this.textBoxSumma.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        // 
        // groupBox13
        // 
        this.groupBox13.Controls.Add(this.textBoxMedelvardeLov);
        this.groupBox13.Location = new System.Drawing.Point(327, 320);
        this.groupBox13.Name = "groupBox13";
        this.groupBox13.Size = new System.Drawing.Size(60, 50);
        this.groupBox13.TabIndex = 31;
        this.groupBox13.TabStop = false;
        // 
        // textBoxMedelvardeLov
        // 
        this.textBoxMedelvardeLov.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxMedelvardeLov.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxMedelvardeLov.Location = new System.Drawing.Point(0, 19);
        this.textBoxMedelvardeLov.Name = "textBoxMedelvardeLov";
        this.textBoxMedelvardeLov.ReadOnly = true;
        this.textBoxMedelvardeLov.Size = new System.Drawing.Size(60, 21);
        this.textBoxMedelvardeLov.TabIndex = 5;
        this.textBoxMedelvardeLov.TabStop = false;
        this.textBoxMedelvardeLov.Text = "0";
        this.textBoxMedelvardeLov.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        // 
        // groupBox6
        // 
        this.groupBox6.Controls.Add(this.textBoxSummaContorta);
        this.groupBox6.Location = new System.Drawing.Point(389, 278);
        this.groupBox6.Name = "groupBox6";
        this.groupBox6.Size = new System.Drawing.Size(60, 50);
        this.groupBox6.TabIndex = 24;
        this.groupBox6.TabStop = false;
        this.groupBox6.Text = "Cont.";
        // 
        // textBoxSummaContorta
        // 
        this.textBoxSummaContorta.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxSummaContorta.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxSummaContorta.Location = new System.Drawing.Point(0, 19);
        this.textBoxSummaContorta.Name = "textBoxSummaContorta";
        this.textBoxSummaContorta.ReadOnly = true;
        this.textBoxSummaContorta.Size = new System.Drawing.Size(60, 21);
        this.textBoxSummaContorta.TabIndex = 7;
        this.textBoxSummaContorta.TabStop = false;
        this.textBoxSummaContorta.Text = "0";
        this.textBoxSummaContorta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        // 
        // groupBox14
        // 
        this.groupBox14.Controls.Add(this.textBoxMedelvardeGran);
        this.groupBox14.Location = new System.Drawing.Point(266, 320);
        this.groupBox14.Name = "groupBox14";
        this.groupBox14.Size = new System.Drawing.Size(60, 50);
        this.groupBox14.TabIndex = 29;
        this.groupBox14.TabStop = false;
        // 
        // textBoxMedelvardeGran
        // 
        this.textBoxMedelvardeGran.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxMedelvardeGran.Location = new System.Drawing.Point(1, 19);
        this.textBoxMedelvardeGran.Name = "textBoxMedelvardeGran";
        this.textBoxMedelvardeGran.ReadOnly = true;
        this.textBoxMedelvardeGran.Size = new System.Drawing.Size(60, 21);
        this.textBoxMedelvardeGran.TabIndex = 3;
        this.textBoxMedelvardeGran.TabStop = false;
        this.textBoxMedelvardeGran.Text = "0";
        this.textBoxMedelvardeGran.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        // 
        // groupBox5
        // 
        this.groupBox5.Controls.Add(this.textBoxSummaLov);
        this.groupBox5.Location = new System.Drawing.Point(328, 278);
        this.groupBox5.Name = "groupBox5";
        this.groupBox5.Size = new System.Drawing.Size(60, 50);
        this.groupBox5.TabIndex = 23;
        this.groupBox5.TabStop = false;
        this.groupBox5.Text = "Löv";
        // 
        // textBoxSummaLov
        // 
        this.textBoxSummaLov.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxSummaLov.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxSummaLov.Location = new System.Drawing.Point(0, 19);
        this.textBoxSummaLov.Name = "textBoxSummaLov";
        this.textBoxSummaLov.ReadOnly = true;
        this.textBoxSummaLov.Size = new System.Drawing.Size(60, 21);
        this.textBoxSummaLov.TabIndex = 5;
        this.textBoxSummaLov.TabStop = false;
        this.textBoxSummaLov.Text = "0";
        this.textBoxSummaLov.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        // 
        // groupBox15
        // 
        this.groupBox15.Controls.Add(this.textBoxMedelvardeTall);
        this.groupBox15.Location = new System.Drawing.Point(205, 320);
        this.groupBox15.Name = "groupBox15";
        this.groupBox15.Size = new System.Drawing.Size(60, 50);
        this.groupBox15.TabIndex = 28;
        this.groupBox15.TabStop = false;
        // 
        // textBoxMedelvardeTall
        // 
        this.textBoxMedelvardeTall.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxMedelvardeTall.Location = new System.Drawing.Point(1, 19);
        this.textBoxMedelvardeTall.Name = "textBoxMedelvardeTall";
        this.textBoxMedelvardeTall.ReadOnly = true;
        this.textBoxMedelvardeTall.Size = new System.Drawing.Size(60, 21);
        this.textBoxMedelvardeTall.TabIndex = 1;
        this.textBoxMedelvardeTall.TabStop = false;
        this.textBoxMedelvardeTall.Text = "0";
        this.textBoxMedelvardeTall.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        // 
        // groupBox4
        // 
        this.groupBox4.Controls.Add(this.textBoxSummaGran);
        this.groupBox4.Location = new System.Drawing.Point(267, 278);
        this.groupBox4.Name = "groupBox4";
        this.groupBox4.Size = new System.Drawing.Size(60, 50);
        this.groupBox4.TabIndex = 22;
        this.groupBox4.TabStop = false;
        this.groupBox4.Text = "Gran";
        // 
        // textBoxSummaGran
        // 
        this.textBoxSummaGran.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxSummaGran.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxSummaGran.Location = new System.Drawing.Point(0, 19);
        this.textBoxSummaGran.Name = "textBoxSummaGran";
        this.textBoxSummaGran.ReadOnly = true;
        this.textBoxSummaGran.Size = new System.Drawing.Size(60, 21);
        this.textBoxSummaGran.TabIndex = 3;
        this.textBoxSummaGran.TabStop = false;
        this.textBoxSummaGran.Text = "0";
        this.textBoxSummaGran.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        // 
        // groupBox16
        // 
        this.groupBox16.Controls.Add(this.textBoxMedelvardeStickvbredd);
        this.groupBox16.Location = new System.Drawing.Point(112, 320);
        this.groupBox16.Name = "groupBox16";
        this.groupBox16.Size = new System.Drawing.Size(92, 50);
        this.groupBox16.TabIndex = 27;
        this.groupBox16.TabStop = false;
        // 
        // textBoxMedelvardeStickvbredd
        // 
        this.textBoxMedelvardeStickvbredd.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxMedelvardeStickvbredd.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxMedelvardeStickvbredd.Location = new System.Drawing.Point(0, 19);
        this.textBoxMedelvardeStickvbredd.Name = "textBoxMedelvardeStickvbredd";
        this.textBoxMedelvardeStickvbredd.ReadOnly = true;
        this.textBoxMedelvardeStickvbredd.Size = new System.Drawing.Size(92, 21);
        this.textBoxMedelvardeStickvbredd.TabIndex = 15;
        this.textBoxMedelvardeStickvbredd.TabStop = false;
        this.textBoxMedelvardeStickvbredd.Text = "0";
        this.textBoxMedelvardeStickvbredd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        // 
        // groupBox3
        // 
        this.groupBox3.Controls.Add(this.textBoxSummaTall);
        this.groupBox3.Location = new System.Drawing.Point(206, 278);
        this.groupBox3.Name = "groupBox3";
        this.groupBox3.Size = new System.Drawing.Size(60, 50);
        this.groupBox3.TabIndex = 21;
        this.groupBox3.TabStop = false;
        this.groupBox3.Text = "Tall";
        // 
        // textBoxSummaTall
        // 
        this.textBoxSummaTall.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxSummaTall.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxSummaTall.Location = new System.Drawing.Point(0, 19);
        this.textBoxSummaTall.Name = "textBoxSummaTall";
        this.textBoxSummaTall.ReadOnly = true;
        this.textBoxSummaTall.Size = new System.Drawing.Size(60, 21);
        this.textBoxSummaTall.TabIndex = 1;
        this.textBoxSummaTall.TabStop = false;
        this.textBoxSummaTall.Text = "0";
        this.textBoxSummaTall.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        // 
        // groupBox2
        // 
        this.groupBox2.Controls.Add(this.label4);
        this.groupBox2.Location = new System.Drawing.Point(19, 278);
        this.groupBox2.Name = "groupBox2";
        this.groupBox2.Size = new System.Drawing.Size(93, 50);
        this.groupBox2.TabIndex = 20;
        this.groupBox2.TabStop = false;
        // 
        // label4
        // 
        this.label4.AutoSize = true;
        this.label4.Location = new System.Drawing.Point(18, 22);
        this.label4.Name = "label4";
        this.label4.Size = new System.Drawing.Size(53, 13);
        this.label4.TabIndex = 14;
        this.label4.Text = "SUMMA:";
        // 
        // groupBox1
        // 
        this.groupBox1.Controls.Add(this.textBoxSummaStickvbredd);
        this.groupBox1.Location = new System.Drawing.Point(113, 278);
        this.groupBox1.Name = "groupBox1";
        this.groupBox1.Size = new System.Drawing.Size(92, 50);
        this.groupBox1.TabIndex = 19;
        this.groupBox1.TabStop = false;
        this.groupBox1.Text = "Stickvbredd";
        // 
        // textBoxSummaStickvbredd
        // 
        this.textBoxSummaStickvbredd.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxSummaStickvbredd.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxSummaStickvbredd.Location = new System.Drawing.Point(1, 19);
        this.textBoxSummaStickvbredd.Name = "textBoxSummaStickvbredd";
        this.textBoxSummaStickvbredd.ReadOnly = true;
        this.textBoxSummaStickvbredd.Size = new System.Drawing.Size(90, 21);
        this.textBoxSummaStickvbredd.TabIndex = 13;
        this.textBoxSummaStickvbredd.TabStop = false;
        this.textBoxSummaStickvbredd.Text = "0";
        this.textBoxSummaStickvbredd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
        // 
        // tabPageFrågor1
        // 
        this.tabPageFrågor1.Controls.Add(this.labelNaturSida1);
        this.tabPageFrågor1.Controls.Add(this.groupBoxFraga6);
        this.tabPageFrågor1.Controls.Add(this.groupBoxFraga5);
        this.tabPageFrågor1.Controls.Add(this.groupBoxFraga4);
        this.tabPageFrågor1.Controls.Add(this.groupBoxFraga3);
        this.tabPageFrågor1.Controls.Add(this.groupBoxFraga1);
        this.tabPageFrågor1.Controls.Add(this.groupBoxFraga2);
        this.tabPageFrågor1.Location = new System.Drawing.Point(4, 22);
        this.tabPageFrågor1.Name = "tabPageFrågor1";
        this.tabPageFrågor1.Padding = new System.Windows.Forms.Padding(3);
        this.tabPageFrågor1.Size = new System.Drawing.Size(627, 406);
        this.tabPageFrågor1.TabIndex = 1;
        this.tabPageFrågor1.Text = "Frågor Sida 1";
        this.tabPageFrågor1.UseVisualStyleBackColor = true;
        // 
        // labelNaturSida1
        // 
        this.labelNaturSida1.AutoSize = true;
        this.labelNaturSida1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelNaturSida1.Location = new System.Drawing.Point(9, 10);
        this.labelNaturSida1.Name = "labelNaturSida1";
        this.labelNaturSida1.Size = new System.Drawing.Size(202, 13);
        this.labelNaturSida1.TabIndex = 7;
        this.labelNaturSida1.Text = "NATUR- OCH KULTURMILJÖHÄNSYN";
        // 
        // groupBoxFraga6
        // 
        this.groupBoxFraga6.Controls.Add(this.radioButton6Nej);
        this.groupBoxFraga6.Controls.Add(this.radioButton6Ja);
        this.groupBoxFraga6.Controls.Add(this.labelFraga6);
        this.groupBoxFraga6.Location = new System.Drawing.Point(12, 342);
        this.groupBoxFraga6.Name = "groupBoxFraga6";
        this.groupBoxFraga6.Size = new System.Drawing.Size(607, 55);
        this.groupBoxFraga6.TabIndex = 13;
        this.groupBoxFraga6.TabStop = false;
        this.groupBoxFraga6.Text = "6.";
        // 
        // radioButton6Nej
        // 
        this.radioButton6Nej.AutoSize = true;
        this.radioButton6Nej.Location = new System.Drawing.Point(462, 23);
        this.radioButton6Nej.Name = "radioButton6Nej";
        this.radioButton6Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton6Nej.TabIndex = 15;
        this.radioButton6Nej.TabStop = true;
        this.radioButton6Nej.Text = "Nej";
        this.radioButton6Nej.UseVisualStyleBackColor = true;
        this.radioButton6Nej.CheckedChanged += new System.EventHandler(this.radioButton6Nej_CheckedChanged);
        // 
        // radioButton6Ja
        // 
        this.radioButton6Ja.AutoSize = true;
        this.radioButton6Ja.Location = new System.Drawing.Point(412, 23);
        this.radioButton6Ja.Name = "radioButton6Ja";
        this.radioButton6Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton6Ja.TabIndex = 14;
        this.radioButton6Ja.TabStop = true;
        this.radioButton6Ja.Text = "Ja";
        this.radioButton6Ja.UseVisualStyleBackColor = true;
        this.radioButton6Ja.CheckedChanged += new System.EventHandler(this.radioButton6Ja_CheckedChanged);
        // 
        // labelFraga6
        // 
        this.labelFraga6.AutoSize = true;
        this.labelFraga6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelFraga6.Location = new System.Drawing.Point(19, 25);
        this.labelFraga6.Name = "labelFraga6";
        this.labelFraga6.Size = new System.Drawing.Size(138, 13);
        this.labelFraga6.TabIndex = 0;
        this.labelFraga6.Text = "Har döda träd värnats?";
        // 
        // groupBoxFraga5
        // 
        this.groupBoxFraga5.Controls.Add(this.radioButton5Ejaktuellt);
        this.groupBoxFraga5.Controls.Add(this.radioButton5Nej);
        this.groupBoxFraga5.Controls.Add(this.radioButton5Ja);
        this.groupBoxFraga5.Controls.Add(this.labelFraga5);
        this.groupBoxFraga5.Location = new System.Drawing.Point(12, 295);
        this.groupBoxFraga5.Name = "groupBoxFraga5";
        this.groupBoxFraga5.Size = new System.Drawing.Size(607, 55);
        this.groupBoxFraga5.TabIndex = 12;
        this.groupBoxFraga5.TabStop = false;
        this.groupBoxFraga5.Text = "5.";
        // 
        // radioButton5Ejaktuellt
        // 
        this.radioButton5Ejaktuellt.AutoSize = true;
        this.radioButton5Ejaktuellt.Location = new System.Drawing.Point(511, 24);
        this.radioButton5Ejaktuellt.Name = "radioButton5Ejaktuellt";
        this.radioButton5Ejaktuellt.Size = new System.Drawing.Size(82, 17);
        this.radioButton5Ejaktuellt.TabIndex = 14;
        this.radioButton5Ejaktuellt.TabStop = true;
        this.radioButton5Ejaktuellt.Text = "Ej aktuellt";
        this.radioButton5Ejaktuellt.UseVisualStyleBackColor = true;
        this.radioButton5Ejaktuellt.CheckedChanged += new System.EventHandler(this.radioButton5Ejaktuellt_CheckedChanged);
        // 
        // radioButton5Nej
        // 
        this.radioButton5Nej.AutoSize = true;
        this.radioButton5Nej.Location = new System.Drawing.Point(462, 24);
        this.radioButton5Nej.Name = "radioButton5Nej";
        this.radioButton5Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton5Nej.TabIndex = 13;
        this.radioButton5Nej.TabStop = true;
        this.radioButton5Nej.Text = "Nej";
        this.radioButton5Nej.UseVisualStyleBackColor = true;
        this.radioButton5Nej.CheckedChanged += new System.EventHandler(this.radioButton5Nej_CheckedChanged);
        // 
        // radioButton5Ja
        // 
        this.radioButton5Ja.AutoSize = true;
        this.radioButton5Ja.Location = new System.Drawing.Point(412, 24);
        this.radioButton5Ja.Name = "radioButton5Ja";
        this.radioButton5Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton5Ja.TabIndex = 12;
        this.radioButton5Ja.TabStop = true;
        this.radioButton5Ja.Text = "Ja";
        this.radioButton5Ja.UseVisualStyleBackColor = true;
        this.radioButton5Ja.CheckedChanged += new System.EventHandler(this.radioButton5Ja_CheckedChanged);
        // 
        // labelFraga5
        // 
        this.labelFraga5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelFraga5.Location = new System.Drawing.Point(19, 23);
        this.labelFraga5.Name = "labelFraga5";
        this.labelFraga5.Size = new System.Drawing.Size(348, 23);
        this.labelFraga5.TabIndex = 0;
        this.labelFraga5.Text = "Har skador undvikits på kulturminnen och fornlämningar?";
        // 
        // groupBoxFraga4
        // 
        this.groupBoxFraga4.Controls.Add(this.radioButton4Nej);
        this.groupBoxFraga4.Controls.Add(this.radioButton4Ja);
        this.groupBoxFraga4.Controls.Add(this.labelFraga4b);
        this.groupBoxFraga4.Controls.Add(this.labelFraga4a);
        this.groupBoxFraga4.Location = new System.Drawing.Point(12, 231);
        this.groupBoxFraga4.Name = "groupBoxFraga4";
        this.groupBoxFraga4.Size = new System.Drawing.Size(607, 70);
        this.groupBoxFraga4.TabIndex = 11;
        this.groupBoxFraga4.TabStop = false;
        this.groupBoxFraga4.Text = "4.";
        // 
        // radioButton4Nej
        // 
        this.radioButton4Nej.AutoSize = true;
        this.radioButton4Nej.Location = new System.Drawing.Point(462, 33);
        this.radioButton4Nej.Name = "radioButton4Nej";
        this.radioButton4Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton4Nej.TabIndex = 13;
        this.radioButton4Nej.TabStop = true;
        this.radioButton4Nej.Text = "Nej";
        this.radioButton4Nej.UseVisualStyleBackColor = true;
        this.radioButton4Nej.CheckedChanged += new System.EventHandler(this.radioButton4Nej_CheckedChanged);
        // 
        // radioButton4Ja
        // 
        this.radioButton4Ja.AutoSize = true;
        this.radioButton4Ja.Location = new System.Drawing.Point(412, 33);
        this.radioButton4Ja.Name = "radioButton4Ja";
        this.radioButton4Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton4Ja.TabIndex = 12;
        this.radioButton4Ja.TabStop = true;
        this.radioButton4Ja.Text = "Ja";
        this.radioButton4Ja.UseVisualStyleBackColor = true;
        this.radioButton4Ja.CheckedChanged += new System.EventHandler(this.radioButton4Ja_CheckedChanged);
        // 
        // labelFraga4b
        // 
        this.labelFraga4b.AutoSize = true;
        this.labelFraga4b.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelFraga4b.Location = new System.Drawing.Point(19, 37);
        this.labelFraga4b.Name = "labelFraga4b";
        this.labelFraga4b.Size = new System.Drawing.Size(379, 13);
        this.labelFraga4b.TabIndex = 1;
        this.labelFraga4b.Text = "(i skyddzonen, framtidsbiotopen ok, hänsynskrävande biotoper)? ";
        // 
        // labelFraga4a
        // 
        this.labelFraga4a.AutoSize = true;
        this.labelFraga4a.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelFraga4a.Location = new System.Drawing.Point(19, 22);
        this.labelFraga4a.Name = "labelFraga4a";
        this.labelFraga4a.Size = new System.Drawing.Size(266, 13);
        this.labelFraga4a.TabIndex = 0;
        this.labelFraga4a.Text = "Har gallringen gynnat naturvärden på trakten";
        // 
        // groupBoxFraga3
        // 
        this.groupBoxFraga3.Controls.Add(this.radioButton3Ejaktuellt);
        this.groupBoxFraga3.Controls.Add(this.labelFraga3);
        this.groupBoxFraga3.Controls.Add(this.radioButton3Nej);
        this.groupBoxFraga3.Controls.Add(this.radioButton3Ja);
        this.groupBoxFraga3.Location = new System.Drawing.Point(12, 185);
        this.groupBoxFraga3.Name = "groupBoxFraga3";
        this.groupBoxFraga3.Size = new System.Drawing.Size(607, 55);
        this.groupBoxFraga3.TabIndex = 10;
        this.groupBoxFraga3.TabStop = false;
        this.groupBoxFraga3.Text = "3.";
        // 
        // radioButton3Ejaktuellt
        // 
        this.radioButton3Ejaktuellt.AutoSize = true;
        this.radioButton3Ejaktuellt.Location = new System.Drawing.Point(511, 20);
        this.radioButton3Ejaktuellt.Name = "radioButton3Ejaktuellt";
        this.radioButton3Ejaktuellt.Size = new System.Drawing.Size(82, 17);
        this.radioButton3Ejaktuellt.TabIndex = 11;
        this.radioButton3Ejaktuellt.TabStop = true;
        this.radioButton3Ejaktuellt.Text = "Ej aktuellt";
        this.radioButton3Ejaktuellt.UseVisualStyleBackColor = true;
        this.radioButton3Ejaktuellt.CheckedChanged += new System.EventHandler(this.radioButton3Ejaktuellt_CheckedChanged);
        // 
        // labelFraga3
        // 
        this.labelFraga3.AutoSize = true;
        this.labelFraga3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelFraga3.Location = new System.Drawing.Point(19, 22);
        this.labelFraga3.Name = "labelFraga3";
        this.labelFraga3.Size = new System.Drawing.Size(217, 13);
        this.labelFraga3.TabIndex = 0;
        this.labelFraga3.Text = "Har skador undvikits i vattenmiljöer?";
        // 
        // radioButton3Nej
        // 
        this.radioButton3Nej.AutoSize = true;
        this.radioButton3Nej.Location = new System.Drawing.Point(462, 20);
        this.radioButton3Nej.Name = "radioButton3Nej";
        this.radioButton3Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton3Nej.TabIndex = 10;
        this.radioButton3Nej.TabStop = true;
        this.radioButton3Nej.Text = "Nej";
        this.radioButton3Nej.UseVisualStyleBackColor = true;
        this.radioButton3Nej.CheckedChanged += new System.EventHandler(this.radioButton3Nej_CheckedChanged);
        // 
        // radioButton3Ja
        // 
        this.radioButton3Ja.AutoSize = true;
        this.radioButton3Ja.Location = new System.Drawing.Point(412, 20);
        this.radioButton3Ja.Name = "radioButton3Ja";
        this.radioButton3Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton3Ja.TabIndex = 9;
        this.radioButton3Ja.TabStop = true;
        this.radioButton3Ja.Text = "Ja";
        this.radioButton3Ja.UseVisualStyleBackColor = true;
        this.radioButton3Ja.CheckedChanged += new System.EventHandler(this.radioButton3Ja_CheckedChanged);
        // 
        // groupBoxFraga1
        // 
        this.groupBoxFraga1.Controls.Add(this.radioButton1Nej);
        this.groupBoxFraga1.Controls.Add(this.radioButton1Ja);
        this.groupBoxFraga1.Controls.Add(this.labelFraga1);
        this.groupBoxFraga1.Location = new System.Drawing.Point(12, 32);
        this.groupBoxFraga1.Name = "groupBoxFraga1";
        this.groupBoxFraga1.Size = new System.Drawing.Size(609, 55);
        this.groupBoxFraga1.TabIndex = 8;
        this.groupBoxFraga1.TabStop = false;
        this.groupBoxFraga1.Text = "1.";
        // 
        // radioButton1Nej
        // 
        this.radioButton1Nej.AutoSize = true;
        this.radioButton1Nej.Location = new System.Drawing.Point(462, 22);
        this.radioButton1Nej.Name = "radioButton1Nej";
        this.radioButton1Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton1Nej.TabIndex = 5;
        this.radioButton1Nej.TabStop = true;
        this.radioButton1Nej.Text = "Nej";
        this.radioButton1Nej.UseVisualStyleBackColor = true;
        this.radioButton1Nej.CheckedChanged += new System.EventHandler(this.radioButton1Nej_CheckedChanged);
        // 
        // radioButton1Ja
        // 
        this.radioButton1Ja.AutoSize = true;
        this.radioButton1Ja.Location = new System.Drawing.Point(412, 22);
        this.radioButton1Ja.Name = "radioButton1Ja";
        this.radioButton1Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton1Ja.TabIndex = 4;
        this.radioButton1Ja.TabStop = true;
        this.radioButton1Ja.Text = "Ja";
        this.radioButton1Ja.UseVisualStyleBackColor = true;
        this.radioButton1Ja.CheckedChanged += new System.EventHandler(this.radioButton1Ja_CheckedChanged);
        // 
        // labelFraga1
        // 
        this.labelFraga1.AutoSize = true;
        this.labelFraga1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelFraga1.Location = new System.Drawing.Point(19, 24);
        this.labelFraga1.Name = "labelFraga1";
        this.labelFraga1.Size = new System.Drawing.Size(137, 13);
        this.labelFraga1.TabIndex = 0;
        this.labelFraga1.Text = "Är traktdirektiv följda?";
        // 
        // groupBoxFraga2
        // 
        this.groupBoxFraga2.Controls.Add(this.radioButton2Ejaktuellt);
        this.groupBoxFraga2.Controls.Add(this.radioButton2Nej);
        this.groupBoxFraga2.Controls.Add(this.radioButton2Ja);
        this.groupBoxFraga2.Controls.Add(this.labelFraga2);
        this.groupBoxFraga2.Location = new System.Drawing.Point(12, 138);
        this.groupBoxFraga2.Name = "groupBoxFraga2";
        this.groupBoxFraga2.Size = new System.Drawing.Size(607, 55);
        this.groupBoxFraga2.TabIndex = 9;
        this.groupBoxFraga2.TabStop = false;
        this.groupBoxFraga2.Text = "2.";
        // 
        // radioButton2Ejaktuellt
        // 
        this.radioButton2Ejaktuellt.AutoSize = true;
        this.radioButton2Ejaktuellt.Location = new System.Drawing.Point(511, 21);
        this.radioButton2Ejaktuellt.Name = "radioButton2Ejaktuellt";
        this.radioButton2Ejaktuellt.Size = new System.Drawing.Size(82, 17);
        this.radioButton2Ejaktuellt.TabIndex = 8;
        this.radioButton2Ejaktuellt.TabStop = true;
        this.radioButton2Ejaktuellt.Text = "Ej aktuellt";
        this.radioButton2Ejaktuellt.UseVisualStyleBackColor = true;
        this.radioButton2Ejaktuellt.CheckedChanged += new System.EventHandler(this.radioButton2Ejaktuellt_CheckedChanged);
        // 
        // radioButton2Nej
        // 
        this.radioButton2Nej.AutoSize = true;
        this.radioButton2Nej.Location = new System.Drawing.Point(462, 21);
        this.radioButton2Nej.Name = "radioButton2Nej";
        this.radioButton2Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton2Nej.TabIndex = 7;
        this.radioButton2Nej.TabStop = true;
        this.radioButton2Nej.Text = "Nej";
        this.radioButton2Nej.UseVisualStyleBackColor = true;
        this.radioButton2Nej.CheckedChanged += new System.EventHandler(this.radioButton2Nej_CheckedChanged);
        // 
        // radioButton2Ja
        // 
        this.radioButton2Ja.AutoSize = true;
        this.radioButton2Ja.Location = new System.Drawing.Point(412, 21);
        this.radioButton2Ja.Name = "radioButton2Ja";
        this.radioButton2Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton2Ja.TabIndex = 6;
        this.radioButton2Ja.TabStop = true;
        this.radioButton2Ja.Text = "Ja";
        this.radioButton2Ja.UseVisualStyleBackColor = true;
        this.radioButton2Ja.CheckedChanged += new System.EventHandler(this.radioButton2Ja_CheckedChanged);
        // 
        // labelFraga2
        // 
        this.labelFraga2.AutoSize = true;
        this.labelFraga2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelFraga2.Location = new System.Drawing.Point(19, 23);
        this.labelFraga2.Name = "labelFraga2";
        this.labelFraga2.Size = new System.Drawing.Size(291, 13);
        this.labelFraga2.TabIndex = 0;
        this.labelFraga2.Text = "Har skador undvikits i hänsynskrävande biotoper?";
        // 
        // tabPageFrågor2
        // 
        this.tabPageFrågor2.Controls.Add(this.groupBoxKommentarer);
        this.tabPageFrågor2.Controls.Add(this.groupBoxOBS);
        this.tabPageFrågor2.Controls.Add(this.labelNaturSida2);
        this.tabPageFrågor2.Controls.Add(this.groupBoxFraga9);
        this.tabPageFrågor2.Controls.Add(this.groupBoxFraga8);
        this.tabPageFrågor2.Controls.Add(this.groupBoxFraga7);
        this.tabPageFrågor2.Location = new System.Drawing.Point(4, 22);
        this.tabPageFrågor2.Name = "tabPageFrågor2";
        this.tabPageFrågor2.Size = new System.Drawing.Size(627, 406);
        this.tabPageFrågor2.TabIndex = 3;
        this.tabPageFrågor2.Text = "Frågor Sida 2";
        this.tabPageFrågor2.UseVisualStyleBackColor = true;
        // 
        // groupBoxKommentarer
        // 
        this.groupBoxKommentarer.Controls.Add(this.richTextBoxKommentarer);
        this.groupBoxKommentarer.Location = new System.Drawing.Point(8, 294);
        this.groupBoxKommentarer.Name = "groupBoxKommentarer";
        this.groupBoxKommentarer.Size = new System.Drawing.Size(616, 100);
        this.groupBoxKommentarer.TabIndex = 11;
        this.groupBoxKommentarer.TabStop = false;
        this.groupBoxKommentarer.Text = "KOMMENTARER";
        // 
        // richTextBoxKommentarer
        // 
        this.richTextBoxKommentarer.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetGallring, "UserData.Kommentar", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.richTextBoxKommentarer.Location = new System.Drawing.Point(9, 21);
        this.richTextBoxKommentarer.MaxLength = 4095;
        this.richTextBoxKommentarer.Name = "richTextBoxKommentarer";
        this.richTextBoxKommentarer.Size = new System.Drawing.Size(599, 68);
        this.richTextBoxKommentarer.TabIndex = 0;
        this.richTextBoxKommentarer.Text = "";
        this.richTextBoxKommentarer.TextChanged += new System.EventHandler(this.richTextBoxKommentarer_TextChanged);
        // 
        // groupBoxOBS
        // 
        this.groupBoxOBS.Controls.Add(this.labelFragaObs);
        this.groupBoxOBS.Controls.Add(this.labelOBS);
        this.groupBoxOBS.Location = new System.Drawing.Point(8, 173);
        this.groupBoxOBS.Name = "groupBoxOBS";
        this.groupBoxOBS.Size = new System.Drawing.Size(616, 66);
        this.groupBoxOBS.TabIndex = 10;
        this.groupBoxOBS.TabStop = false;
        // 
        // labelFragaObs
        // 
        this.labelFragaObs.AutoSize = true;
        this.labelFragaObs.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelFragaObs.Location = new System.Drawing.Point(24, 38);
        this.labelFragaObs.Name = "labelFragaObs";
        this.labelFragaObs.Size = new System.Drawing.Size(456, 13);
        this.labelFragaObs.TabIndex = 1;
        this.labelFragaObs.Text = "Ifall något ”Nej” har valts på någon av frågorna så skall en miljörapport skrivas" +
            "!";
        // 
        // labelOBS
        // 
        this.labelOBS.AutoSize = true;
        this.labelOBS.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelOBS.Location = new System.Drawing.Point(19, 17);
        this.labelOBS.Name = "labelOBS";
        this.labelOBS.Size = new System.Drawing.Size(32, 13);
        this.labelOBS.TabIndex = 0;
        this.labelOBS.Text = "OBS!";
        // 
        // labelNaturSida2
        // 
        this.labelNaturSida2.AutoSize = true;
        this.labelNaturSida2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelNaturSida2.Location = new System.Drawing.Point(5, 8);
        this.labelNaturSida2.Name = "labelNaturSida2";
        this.labelNaturSida2.Size = new System.Drawing.Size(202, 13);
        this.labelNaturSida2.TabIndex = 6;
        this.labelNaturSida2.Text = "NATUR- OCH KULTURMILJÖHÄNSYN";
        // 
        // groupBoxFraga9
        // 
        this.groupBoxFraga9.Controls.Add(this.radioButton9Nej);
        this.groupBoxFraga9.Controls.Add(this.radioButton9Ja);
        this.groupBoxFraga9.Controls.Add(this.labelFraga9a);
        this.groupBoxFraga9.Location = new System.Drawing.Point(8, 124);
        this.groupBoxFraga9.Name = "groupBoxFraga9";
        this.groupBoxFraga9.Size = new System.Drawing.Size(616, 56);
        this.groupBoxFraga9.TabIndex = 9;
        this.groupBoxFraga9.TabStop = false;
        this.groupBoxFraga9.Text = "9.";
        // 
        // radioButton9Nej
        // 
        this.radioButton9Nej.AutoSize = true;
        this.radioButton9Nej.Location = new System.Drawing.Point(475, 22);
        this.radioButton9Nej.Name = "radioButton9Nej";
        this.radioButton9Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton9Nej.TabIndex = 20;
        this.radioButton9Nej.TabStop = true;
        this.radioButton9Nej.Text = "Nej";
        this.radioButton9Nej.UseVisualStyleBackColor = true;
        this.radioButton9Nej.CheckedChanged += new System.EventHandler(this.radioButton9Nej_CheckedChanged);
        // 
        // radioButton9Ja
        // 
        this.radioButton9Ja.AutoSize = true;
        this.radioButton9Ja.Location = new System.Drawing.Point(425, 22);
        this.radioButton9Ja.Name = "radioButton9Ja";
        this.radioButton9Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton9Ja.TabIndex = 19;
        this.radioButton9Ja.TabStop = true;
        this.radioButton9Ja.Text = "Ja";
        this.radioButton9Ja.UseVisualStyleBackColor = true;
        this.radioButton9Ja.CheckedChanged += new System.EventHandler(this.radioButton9Ja_CheckedChanged);
        // 
        // labelFraga9a
        // 
        this.labelFraga9a.AutoSize = true;
        this.labelFraga9a.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelFraga9a.Location = new System.Drawing.Point(19, 24);
        this.labelFraga9a.Name = "labelFraga9a";
        this.labelFraga9a.Size = new System.Drawing.Size(192, 13);
        this.labelFraga9a.TabIndex = 0;
        this.labelFraga9a.Text = "Var avverkningstidpunkten rätt?";
        // 
        // groupBoxFraga8
        // 
        this.groupBoxFraga8.Controls.Add(this.radioButton8Nej);
        this.groupBoxFraga8.Controls.Add(this.radioButton8Ja);
        this.groupBoxFraga8.Controls.Add(this.labelFraga8);
        this.groupBoxFraga8.Location = new System.Drawing.Point(8, 77);
        this.groupBoxFraga8.Name = "groupBoxFraga8";
        this.groupBoxFraga8.Size = new System.Drawing.Size(616, 55);
        this.groupBoxFraga8.TabIndex = 8;
        this.groupBoxFraga8.TabStop = false;
        this.groupBoxFraga8.Text = "8.";
        // 
        // radioButton8Nej
        // 
        this.radioButton8Nej.AutoSize = true;
        this.radioButton8Nej.Location = new System.Drawing.Point(475, 20);
        this.radioButton8Nej.Name = "radioButton8Nej";
        this.radioButton8Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton8Nej.TabIndex = 18;
        this.radioButton8Nej.TabStop = true;
        this.radioButton8Nej.Text = "Nej";
        this.radioButton8Nej.UseVisualStyleBackColor = true;
        this.radioButton8Nej.CheckedChanged += new System.EventHandler(this.radioButton8Nej_CheckedChanged);
        // 
        // radioButton8Ja
        // 
        this.radioButton8Ja.AutoSize = true;
        this.radioButton8Ja.Location = new System.Drawing.Point(425, 20);
        this.radioButton8Ja.Name = "radioButton8Ja";
        this.radioButton8Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton8Ja.TabIndex = 17;
        this.radioButton8Ja.TabStop = true;
        this.radioButton8Ja.Text = "Ja";
        this.radioButton8Ja.UseVisualStyleBackColor = true;
        this.radioButton8Ja.CheckedChanged += new System.EventHandler(this.radioButton8Ja_CheckedChanged);
        // 
        // labelFraga8
        // 
        this.labelFraga8.AutoSize = true;
        this.labelFraga8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelFraga8.Location = new System.Drawing.Point(19, 22);
        this.labelFraga8.Name = "labelFraga8";
        this.labelFraga8.Size = new System.Drawing.Size(128, 13);
        this.labelFraga8.TabIndex = 0;
        this.labelFraga8.Text = "Är trakten avstädad?";
        // 
        // groupBoxFraga7
        // 
        this.groupBoxFraga7.Controls.Add(this.radioButton7Ejaktuellt);
        this.groupBoxFraga7.Controls.Add(this.radioButton7Nej);
        this.groupBoxFraga7.Controls.Add(this.radioButton7Ja);
        this.groupBoxFraga7.Controls.Add(this.labelFraga7);
        this.groupBoxFraga7.Location = new System.Drawing.Point(8, 30);
        this.groupBoxFraga7.Name = "groupBoxFraga7";
        this.groupBoxFraga7.Size = new System.Drawing.Size(616, 55);
        this.groupBoxFraga7.TabIndex = 7;
        this.groupBoxFraga7.TabStop = false;
        this.groupBoxFraga7.Text = "7.";
        // 
        // radioButton7Ejaktuellt
        // 
        this.radioButton7Ejaktuellt.AutoSize = true;
        this.radioButton7Ejaktuellt.Location = new System.Drawing.Point(528, 20);
        this.radioButton7Ejaktuellt.Name = "radioButton7Ejaktuellt";
        this.radioButton7Ejaktuellt.Size = new System.Drawing.Size(82, 17);
        this.radioButton7Ejaktuellt.TabIndex = 17;
        this.radioButton7Ejaktuellt.TabStop = true;
        this.radioButton7Ejaktuellt.Text = "Ej aktuellt";
        this.radioButton7Ejaktuellt.UseVisualStyleBackColor = true;
        this.radioButton7Ejaktuellt.CheckedChanged += new System.EventHandler(this.radioButton7Ejaktuellt_CheckedChanged);
        // 
        // radioButton7Nej
        // 
        this.radioButton7Nej.AutoSize = true;
        this.radioButton7Nej.Location = new System.Drawing.Point(475, 20);
        this.radioButton7Nej.Name = "radioButton7Nej";
        this.radioButton7Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton7Nej.TabIndex = 16;
        this.radioButton7Nej.TabStop = true;
        this.radioButton7Nej.Text = "Nej";
        this.radioButton7Nej.UseVisualStyleBackColor = true;
        this.radioButton7Nej.CheckedChanged += new System.EventHandler(this.radioButton7Nej_CheckedChanged);
        // 
        // radioButton7Ja
        // 
        this.radioButton7Ja.AutoSize = true;
        this.radioButton7Ja.Location = new System.Drawing.Point(425, 20);
        this.radioButton7Ja.Name = "radioButton7Ja";
        this.radioButton7Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton7Ja.TabIndex = 15;
        this.radioButton7Ja.TabStop = true;
        this.radioButton7Ja.Text = "Ja";
        this.radioButton7Ja.UseVisualStyleBackColor = true;
        this.radioButton7Ja.CheckedChanged += new System.EventHandler(this.radioButton7Ja_CheckedChanged);
        // 
        // labelFraga7
        // 
        this.labelFraga7.AutoSize = true;
        this.labelFraga7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelFraga7.Location = new System.Drawing.Point(19, 22);
        this.labelFraga7.Name = "labelFraga7";
        this.labelFraga7.Size = new System.Drawing.Size(271, 13);
        this.labelFraga7.TabIndex = 0;
        this.labelFraga7.Text = "Har 3 högstubbar/ha skapats i grövre gallring?";
        // 
        // environmentGallring
        // 
        designerSettings1.ApplicationConnection = null;
        designerSettings1.DefaultFont = new System.Drawing.Font("Arial", 10F);
        designerSettings1.Icon = ((System.Drawing.Icon)(resources.GetObject("designerSettings1.Icon")));
        designerSettings1.Restrictions = designerRestrictions1;
        designerSettings1.Text = "";
        this.environmentGallring.DesignerSettings = designerSettings1;
        emailSettings1.Address = "";
        emailSettings1.Host = "";
        emailSettings1.MessageTemplate = "";
        emailSettings1.Name = "";
        emailSettings1.Password = "";
        emailSettings1.Port = 49;
        emailSettings1.UserName = "";
        this.environmentGallring.EmailSettings = emailSettings1;
        previewSettings1.Buttons = ((FastReport.PreviewButtons)((((((FastReport.PreviewButtons.Print | FastReport.PreviewButtons.Save)
                    | FastReport.PreviewButtons.Find)
                    | FastReport.PreviewButtons.Zoom)
                    | FastReport.PreviewButtons.Navigator)
                    | FastReport.PreviewButtons.Close)));
        previewSettings1.Icon = ((System.Drawing.Icon)(resources.GetObject("previewSettings1.Icon")));
        previewSettings1.Text = "";
        this.environmentGallring.PreviewSettings = previewSettings1;
        this.environmentGallring.ReportSettings = reportSettings1;
        this.environmentGallring.UIStyle = FastReport.Utils.UIStyle.Office2007Black;
        // 
        // labelTotaltAntalYtor
        // 
        this.labelTotaltAntalYtor.AutoSize = true;
        this.labelTotaltAntalYtor.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelTotaltAntalYtor.Location = new System.Drawing.Point(1, 462);
        this.labelTotaltAntalYtor.Name = "labelTotaltAntalYtor";
        this.labelTotaltAntalYtor.Size = new System.Drawing.Size(113, 13);
        this.labelTotaltAntalYtor.TabIndex = 2;
        this.labelTotaltAntalYtor.Text = "Totalt antal ytor: 0";
        // 
        // labelProgress
        // 
        this.labelProgress.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelProgress.Location = new System.Drawing.Point(545, 462);
        this.labelProgress.Name = "labelProgress";
        this.labelProgress.Size = new System.Drawing.Size(86, 13);
        this.labelProgress.TabIndex = 4;
        this.labelProgress.Text = "0% klar";
        this.labelProgress.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
        // 
        // progressBarData
        // 
        this.progressBarData.ForeColor = System.Drawing.SystemColors.Desktop;
        this.progressBarData.Location = new System.Drawing.Point(4, 484);
        this.progressBarData.Maximum = 21;
        this.progressBarData.Name = "progressBarData";
        this.progressBarData.Size = new System.Drawing.Size(627, 20);
        this.progressBarData.Step = 1;
        this.progressBarData.TabIndex = 3;
        // 
        // reportGallring
        // 
        this.reportGallring.ReportResourceString = resources.GetString("reportGallring.ReportResourceString");
        this.reportGallring.RegisterData(this.dataSetCalculations, "dataSetCalculations");
        // 
        // dataSetCalculations
        // 
        this.dataSetCalculations.DataSetName = "DataSetCalculations";
        this.dataSetCalculations.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableMedelvarde});
        // 
        // dataTableMedelvarde
        // 
        this.dataTableMedelvarde.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnMedelvardeStickvbredd});
        this.dataTableMedelvarde.TableName = "Medelvarde";
        // 
        // dataColumnMedelvardeStickvbredd
        // 
        this.dataColumnMedelvardeStickvbredd.ColumnName = "Stickvbredd";
        this.dataColumnMedelvardeStickvbredd.DataType = typeof(double);
        // 
        // textBoxRegion
        // 
        this.textBoxRegion.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetGallring, "UserData.Region", true));
        this.textBoxRegion.Location = new System.Drawing.Point(15, 30);
        this.textBoxRegion.MaxLength = 6;
        this.textBoxRegion.Name = "textBoxRegion";
        this.textBoxRegion.ReadOnly = true;
        this.textBoxRegion.Size = new System.Drawing.Size(116, 21);
        this.textBoxRegion.TabIndex = 8;
        this.textBoxRegion.Visible = false;
        // 
        // textBoxDistrikt
        // 
        this.textBoxDistrikt.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetGallring, "UserData.Distrikt", true));
        this.textBoxDistrikt.Location = new System.Drawing.Point(171, 30);
        this.textBoxDistrikt.MaxLength = 6;
        this.textBoxDistrikt.Name = "textBoxDistrikt";
        this.textBoxDistrikt.ReadOnly = true;
        this.textBoxDistrikt.Size = new System.Drawing.Size(116, 21);
        this.textBoxDistrikt.TabIndex = 9;
        this.textBoxDistrikt.Visible = false;
        // 
        // GallringForm
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.AutoScroll = true;
        this.AutoSize = true;
        this.ClientSize = new System.Drawing.Size(635, 541);
        this.Controls.Add(this.labelTotaltAntalYtor);
        this.Controls.Add(this.labelProgress);
        this.Controls.Add(this.progressBarData);
        this.Controls.Add(this.tabControl1);
        this.Controls.Add(this.menuStripGallring);
        this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
        this.MainMenuStrip = this.menuStripGallring;
        this.MaximizeBox = false;
        this.MaximumSize = new System.Drawing.Size(651, 580);
        this.MinimizeBox = false;
        this.Name = "GallringForm";
        this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        this.Text = "KORUS Gallring";
        this.Load += new System.EventHandler(this.Gallring_Load);
        this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Gallring_FormClosing);
        this.menuStripGallring.ResumeLayout(false);
        this.menuStripGallring.PerformLayout();
        this.tabControl1.ResumeLayout(false);
        this.tabPageGallring.ResumeLayout(false);
        this.groupBoxGallring.ResumeLayout(false);
        this.groupBoxGallring.PerformLayout();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetGallring)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableYta)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableUserData)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFragor)).EndInit();
        this.groupBoxTrakt.ResumeLayout(false);
        this.groupBoxTrakt.PerformLayout();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetSettings)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableRegion)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableDistrikt)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableStandort)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableMarkberedningsmetod)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableUrsprung)).EndInit();
        this.groupBoxRegion.ResumeLayout(false);
        this.groupBoxRegion.PerformLayout();
        this.tabPageYtor.ResumeLayout(false);
        this.groupBoxGallringsstallen.ResumeLayout(false);
        ((System.ComponentModel.ISupportInitialize)(this.dataGridViewGallringsstallen)).EndInit();
        this.groupBox10.ResumeLayout(false);
        this.groupBox10.PerformLayout();
        this.groupBox9.ResumeLayout(false);
        this.groupBox9.PerformLayout();
        this.groupBox11.ResumeLayout(false);
        this.groupBox11.PerformLayout();
        this.groupBox8.ResumeLayout(false);
        this.groupBox8.PerformLayout();
        this.groupBox12.ResumeLayout(false);
        this.groupBox12.PerformLayout();
        this.groupBox7.ResumeLayout(false);
        this.groupBox7.PerformLayout();
        this.groupBox13.ResumeLayout(false);
        this.groupBox13.PerformLayout();
        this.groupBox6.ResumeLayout(false);
        this.groupBox6.PerformLayout();
        this.groupBox14.ResumeLayout(false);
        this.groupBox14.PerformLayout();
        this.groupBox5.ResumeLayout(false);
        this.groupBox5.PerformLayout();
        this.groupBox15.ResumeLayout(false);
        this.groupBox15.PerformLayout();
        this.groupBox4.ResumeLayout(false);
        this.groupBox4.PerformLayout();
        this.groupBox16.ResumeLayout(false);
        this.groupBox16.PerformLayout();
        this.groupBox3.ResumeLayout(false);
        this.groupBox3.PerformLayout();
        this.groupBox2.ResumeLayout(false);
        this.groupBox2.PerformLayout();
        this.groupBox1.ResumeLayout(false);
        this.groupBox1.PerformLayout();
        this.tabPageFrågor1.ResumeLayout(false);
        this.tabPageFrågor1.PerformLayout();
        this.groupBoxFraga6.ResumeLayout(false);
        this.groupBoxFraga6.PerformLayout();
        this.groupBoxFraga5.ResumeLayout(false);
        this.groupBoxFraga5.PerformLayout();
        this.groupBoxFraga4.ResumeLayout(false);
        this.groupBoxFraga4.PerformLayout();
        this.groupBoxFraga3.ResumeLayout(false);
        this.groupBoxFraga3.PerformLayout();
        this.groupBoxFraga1.ResumeLayout(false);
        this.groupBoxFraga1.PerformLayout();
        this.groupBoxFraga2.ResumeLayout(false);
        this.groupBoxFraga2.PerformLayout();
        this.tabPageFrågor2.ResumeLayout(false);
        this.tabPageFrågor2.PerformLayout();
        this.groupBoxKommentarer.ResumeLayout(false);
        this.groupBoxOBS.ResumeLayout(false);
        this.groupBoxOBS.PerformLayout();
        this.groupBoxFraga9.ResumeLayout(false);
        this.groupBoxFraga9.PerformLayout();
        this.groupBoxFraga8.ResumeLayout(false);
        this.groupBoxFraga8.PerformLayout();
        this.groupBoxFraga7.ResumeLayout(false);
        this.groupBoxFraga7.PerformLayout();
        ((System.ComponentModel.ISupportInitialize)(this.reportGallring)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetCalculations)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableMedelvarde)).EndInit();
        this.ResumeLayout(false);
        this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.MenuStrip menuStripGallring;
    private System.Windows.Forms.ToolStripMenuItem arkivToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem sparaToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem sparaSomToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    private System.Windows.Forms.ToolStripMenuItem avslutaToolStripMenuItem;
    private System.Windows.Forms.OpenFileDialog openReportFileDialog;
    private System.Windows.Forms.TabControl tabControl1;
    private System.Windows.Forms.TabPage tabPageYtor;
    private System.Windows.Forms.TabPage tabPageFrågor1;
    private FastReport.EnvironmentSettings environmentGallring;
    private System.Windows.Forms.FolderBrowserDialog saveReportfolderBrowserDialog;
    private System.Windows.Forms.TabPage tabPageGallring;
    private System.Windows.Forms.TabPage tabPageFrågor2;
    private System.Windows.Forms.GroupBox groupBoxRegion;
    private System.Windows.Forms.ComboBox comboBoxUrsprung;
    private System.Windows.Forms.DateTimePicker dateTimePicker;
    private System.Windows.Forms.ComboBox comboBoxDistrikt;
    private System.Windows.Forms.Label labelDatum;
    private System.Windows.Forms.Label labelUrsprung;
    private System.Windows.Forms.Label labelDistrikt;
    private System.Windows.Forms.Label labelRegion;
    private System.Windows.Forms.GroupBox groupBoxTrakt;
    private System.Windows.Forms.TextBox textBoxTraktnr;
    private System.Windows.Forms.TextBox textBoxTraktnamn;
    private System.Windows.Forms.TextBox textBoxEntreprenor;
    private System.Windows.Forms.ComboBox comboBoxStandort;
    private System.Windows.Forms.Label labeEntreprenor;
    private System.Windows.Forms.Label labelStandort;
    private System.Windows.Forms.Label labelTraktnamn;
    private System.Windows.Forms.Label labelTraktnr;
    private System.Windows.Forms.GroupBox groupBoxGallring;
    private System.Windows.Forms.Label labelMaluppfyllelseProcent;
    private System.Windows.Forms.Label labelMaluppfyllelse;
    private System.Windows.Forms.TextBox textBoxMaluppfyllelse;
    private System.Windows.Forms.Label labelArealHa;
    private System.Windows.Forms.Label labelAreal;
    private System.Windows.Forms.TextBox textBoxAreal;
    private System.Windows.Forms.Label labelVagarealProcent;
    private System.Windows.Forms.Label labelStickvagsavstandM;
    private System.Windows.Forms.Label labelMalgrundytaM2Ha;
    private System.Windows.Forms.TextBox textBoxStickvagsavstand;
    private System.Windows.Forms.Label labelMalgrundyta;
    private System.Windows.Forms.TextBox textBoxVagareal;
    private System.Windows.Forms.Label labelVagareal;
    private System.Windows.Forms.Label labelStickvagsavstand;
    private System.Windows.Forms.TextBox textBoxMalgrundyta;
    private System.Windows.Forms.Label labelTotaltAntalYtor;
    private System.Windows.Forms.Label labelProgress;
    private System.Windows.Forms.ProgressBar progressBarData;
    private System.Data.DataSet dataSetGallring;
    private System.Data.DataTable dataTableYta;
    private System.Data.DataColumn dataColumnYta;
    private System.Data.DataColumn dataColumnTall;
    private System.Data.DataColumn dataColumnGran;
    private System.Data.DataColumn dataColumnLov;
    private System.Data.DataColumn dataColumnContorta;
    private System.Data.DataColumn dataColumnSumma;
    private System.Data.DataColumn dataColumnSkador;
    private System.Data.DataTable dataTableUserData;
    private System.Data.DataColumn dataColumnRegionId;
    private System.Data.DataColumn dataColumnRegion2;
    private System.Data.DataColumn dataColumnDistriktId;
    private System.Data.DataColumn dataColumnDistrikt;
    private System.Data.DataColumn dataColumnUrsprung;
    private System.Data.DataColumn dataColumnDatum;
    private System.Data.DataColumn dataColumnTraktnr;
    private System.Data.DataColumn dataColumnTraktnamn;
    private System.Data.DataColumn dataColumnStandort;
    private System.Data.DataColumn dataColumnEntreprenor;
    private System.Data.DataColumn dataColumnKommentar;
    private System.Data.DataColumn dataColumnLanguage;
    private System.Data.DataColumn dataColumnMailFrom;
    private System.Data.DataColumn dataColumnMalgrundyta;
    private System.Data.DataColumn dataColumnStickvagsavstand;
    private System.Data.DataColumn dataColumnStatus;
    private System.Data.DataColumn dataColumnStatus_Datum;
    private System.Data.DataColumn dataColumnArtal;
    private System.Data.DataColumn dataColumnAreal;
    private System.Data.DataTable dataTableFragor;
    private System.Data.DataColumn dataColumnFraga1;
    private System.Data.DataColumn dataColumnFraga2;
    private System.Data.DataColumn dataColumnFraga3;
    private System.Data.DataColumn dataColumnFraga4;
    private System.Data.DataColumn dataColumnFraga5;
    private System.Data.DataColumn dataColumnFraga6;
    private System.Data.DataColumn dataColumnFraga7;
    private System.Data.DataColumn dataColumnFraga8;
    private System.Data.DataColumn dataColumnFraga9;
    private System.Data.DataColumn dataColumnOvrigt;
    private System.Windows.Forms.ComboBox comboBoxRegion;
    private System.Data.DataSet dataSetSettings;
    private System.Data.DataTable dataTableRegion;
    private System.Data.DataColumn dataColumnSettingsRegionId;
    private System.Data.DataColumn dataColumnSettingsRegionNamn;
    private System.Data.DataTable dataTableDistrikt;
    private System.Data.DataColumn dataColumnSettingsDistriktRegionId;
    private System.Data.DataColumn dataColumnSettingsDistriktId;
    private System.Data.DataColumn dataColumnSettingsDistriktNamn;
    private System.Data.DataTable dataTableStandort;
    private System.Data.DataColumn dataColumnSettingsStandortNamn;
    private System.Data.DataTable dataTableMarkberedningsmetod;
    private System.Data.DataColumn dataColumnSettingsMarkberedningsmetodNamn;
    private System.Data.DataTable dataTableUrsprung;
    private System.Data.DataColumn dataColumnSettingsUrsprungNamn;
    private FastReport.Report reportGallring;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.ComboBox comboBoxStickväg;
    private System.Data.DataColumn dataColumnStickvagssystem;
    private System.Windows.Forms.Label labelNaturSida1;
    private System.Windows.Forms.GroupBox groupBoxFraga6;
    private System.Windows.Forms.RadioButton radioButton6Nej;
    private System.Windows.Forms.RadioButton radioButton6Ja;
    private System.Windows.Forms.Label labelFraga6;
    private System.Windows.Forms.GroupBox groupBoxFraga5;
    private System.Windows.Forms.RadioButton radioButton5Ejaktuellt;
    private System.Windows.Forms.RadioButton radioButton5Nej;
    private System.Windows.Forms.RadioButton radioButton5Ja;
    private System.Windows.Forms.Label labelFraga5;
    private System.Windows.Forms.GroupBox groupBoxFraga4;
    private System.Windows.Forms.RadioButton radioButton4Nej;
    private System.Windows.Forms.RadioButton radioButton4Ja;
    private System.Windows.Forms.Label labelFraga4b;
    private System.Windows.Forms.Label labelFraga4a;
    private System.Windows.Forms.GroupBox groupBoxFraga3;
    private System.Windows.Forms.RadioButton radioButton3Ejaktuellt;
    private System.Windows.Forms.Label labelFraga3;
    private System.Windows.Forms.RadioButton radioButton3Nej;
    private System.Windows.Forms.RadioButton radioButton3Ja;
    private System.Windows.Forms.GroupBox groupBoxFraga1;
    private System.Windows.Forms.RadioButton radioButton1Nej;
    private System.Windows.Forms.RadioButton radioButton1Ja;
    private System.Windows.Forms.Label labelFraga1;
    private System.Windows.Forms.GroupBox groupBoxFraga2;
    private System.Windows.Forms.RadioButton radioButton2Ejaktuellt;
    private System.Windows.Forms.RadioButton radioButton2Nej;
    private System.Windows.Forms.RadioButton radioButton2Ja;
    private System.Windows.Forms.Label labelFraga2;
    private System.Windows.Forms.GroupBox groupBoxOBS;
    private System.Windows.Forms.Label labelFragaObs;
    private System.Windows.Forms.Label labelOBS;
    private System.Windows.Forms.Label labelNaturSida2;
    private System.Windows.Forms.GroupBox groupBoxFraga9;
    private System.Windows.Forms.RadioButton radioButton9Nej;
    private System.Windows.Forms.RadioButton radioButton9Ja;
    private System.Windows.Forms.Label labelFraga9a;
    private System.Windows.Forms.GroupBox groupBoxFraga8;
    private System.Windows.Forms.RadioButton radioButton8Nej;
    private System.Windows.Forms.RadioButton radioButton8Ja;
    private System.Windows.Forms.Label labelFraga8;
    private System.Windows.Forms.GroupBox groupBoxFraga7;
    private System.Windows.Forms.RadioButton radioButton7Ejaktuellt;
    private System.Windows.Forms.RadioButton radioButton7Nej;
    private System.Windows.Forms.RadioButton radioButton7Ja;
    private System.Windows.Forms.Label labelFraga7;
    private System.Windows.Forms.GroupBox groupBoxKommentarer;
    private System.Windows.Forms.RichTextBox richTextBoxKommentarer;
    private System.Windows.Forms.GroupBox groupBoxGallringsstallen;
    private System.Windows.Forms.GroupBox groupBox10;
    private System.Windows.Forms.TextBox textBoxMedelvardeSkador;
    private System.Windows.Forms.GroupBox groupBox9;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.GroupBox groupBox11;
    private System.Windows.Forms.TextBox textBoxMedelvardeSumma;
    private System.Windows.Forms.GroupBox groupBox8;
    private System.Windows.Forms.TextBox textBoxSummaSkador;
    private System.Windows.Forms.GroupBox groupBox12;
    private System.Windows.Forms.TextBox textBoxMedelvardeContorta;
    private System.Windows.Forms.GroupBox groupBox7;
    private System.Windows.Forms.TextBox textBoxSumma;
    private System.Windows.Forms.GroupBox groupBox13;
    private System.Windows.Forms.TextBox textBoxMedelvardeLov;
    private System.Windows.Forms.GroupBox groupBox6;
    private System.Windows.Forms.TextBox textBoxSummaContorta;
    private System.Windows.Forms.GroupBox groupBox14;
    private System.Windows.Forms.TextBox textBoxMedelvardeGran;
    private System.Windows.Forms.GroupBox groupBox5;
    private System.Windows.Forms.TextBox textBoxSummaLov;
    private System.Windows.Forms.GroupBox groupBox15;
    private System.Windows.Forms.TextBox textBoxMedelvardeTall;
    private System.Windows.Forms.GroupBox groupBox4;
    private System.Windows.Forms.TextBox textBoxSummaGran;
    private System.Windows.Forms.GroupBox groupBox16;
    private System.Windows.Forms.TextBox textBoxMedelvardeStickvbredd;
    private System.Windows.Forms.GroupBox groupBox3;
    private System.Windows.Forms.TextBox textBoxSummaTall;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.TextBox textBoxSummaStickvbredd;
    private System.Data.DataColumn dataColumnStickvbredd;
    private System.Windows.Forms.DataGridView dataGridViewGallringsstallen;
    private System.Windows.Forms.DataGridViewTextBoxColumn ytaDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn stickvbreddDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn tallDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn granDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn lovDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn contortaDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn summaDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn skadorDataGridViewTextBoxColumn;
    private System.Data.DataSet dataSetCalculations;
    private System.Data.DataTable dataTableMedelvarde;
    private System.Data.DataColumn dataColumnMedelvardeStickvbredd;
    private System.Windows.Forms.TextBox textBoxRegion;
    private System.Windows.Forms.TextBox textBoxDistrikt;
  }
}

