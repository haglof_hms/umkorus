﻿#region

using System;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using Egenuppfoljning.Interfaces;
using Egenuppfoljning.Properties;
using FastReport.Export.OoXML;
using FastReport.Export.Pdf;
using Microsoft.Office.Interop.Outlook;
using Application = System.Windows.Forms.Application;
using Attachment = System.Net.Mail.Attachment;
using Exception = System.Exception;

#endregion

namespace Egenuppfoljning.Applications
{
  /// <summary>
  ///   PC Application for egenuppföljning - Röjning.
  /// </summary>
  public partial class RöjningForm : Form, IKorusRapport
  {
    //Epost
    private const int CP_NOCLOSE_BUTTON = 0x200;
    private string mEpostAdress;
    private string mMeddelande;

    private string mSDefaultExcel2007Path;
    private string mSDefaultPdfPath;
    private string mSDefaultReportsPath;
    private string mSSettingsFilePath;
    private string mSSettingsPath;
    private string mTitel;


    /// <summary>
    ///   Constructor, initiate data.
    /// </summary>
    public RöjningForm()
    {
      //Select the language that should be used.
      Thread.CurrentThread.CurrentCulture = new CultureInfo(Settings.Default.SettingsLanguage);
      Thread.CurrentThread.CurrentUICulture = new CultureInfo(Settings.Default.SettingsLanguage);
      InitializeComponent();
    }


    public RöjningForm(bool aRedigeraLagratData)
    {
      //Select the language that should be used.
      Thread.CurrentThread.CurrentCulture = new CultureInfo(Settings.Default.SettingsLanguage);
      Thread.CurrentThread.CurrentUICulture = new CultureInfo(Settings.Default.SettingsLanguage);
      InitializeComponent();
      sparaSomToolStripMenuItem.Visible = false;

      //nyckelvärden kan ej ändras.
      comboBoxInvTyp.Enabled = false;
      comboBoxRegion.Enabled = false;
      comboBoxDistrikt.Enabled = false;
      textBoxTraktnr.Enabled = false;
      textBoxTraktDel.Enabled = false;
      textBoxEntreprenor.Enabled = false;
      dateTimePicker.Enabled = false;
      sparaToolStripMenuItem.Text = Resources.Lagra_andrat_data;

      RedigeraLagratData = aRedigeraLagratData;
    
      if (RedigeraLagratData)
      {
          comboBoxRegion.Visible = false;
          comboBoxDistrikt.Visible = false;
          textBoxRegion.Visible = true;
          textBoxDistrikt.Visible = true;
      }
      else
      {
          comboBoxRegion.Visible = true;
          comboBoxDistrikt.Visible = true;
          textBoxRegion.Visible = false;
          textBoxDistrikt.Visible = false;
      }
    }

    // Avvaktivera stäng knappen, för att få kontrollen att kunna välja att inte lagra data ifall Avsluta valet i menyn väljs

    protected override CreateParams CreateParams
    {
      get
      {
        var myCp = base.CreateParams;
        myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
        return myCp;
      }
    }

    #region IKorusRapport Members

    public bool RedigeraLagratData { get; private set; }

    public void LaddaForm(string aRapportSökväg)
    {
      //mSSettingsPath = Application.StartupPath + Settings.Default.SettingsPath;
      //mSSettingsFilePath = Application.StartupPath + Settings.Default.SettingsPath + Settings.Default.SettingsFilename +
      //                     "." + Settings.Default.SettingsLanguage + Resources.Xml_suffix;
        mSSettingsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + Settings.Default.SettingsPath;
        mSSettingsFilePath = mSSettingsPath + Settings.Default.SettingsFilename +
                             "." + Settings.Default.SettingsLanguage + Resources.Xml_suffix;

      mSDefaultExcel2007Path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                               Settings.Default.ReportsPath + Settings.Default.Excel2007Path;
      mSDefaultPdfPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                         Settings.Default.ReportsPath + Settings.Default.PdfPath;
      InitFastReportMail();

      if (aRapportSökväg != null)
      {
        Settings.Default.ReportFilePath = aRapportSökväg;
        mSDefaultReportsPath = Application.StartupPath + Settings.Default.ReportsPath;
        LoadRöjning();
      }
      else
      {
        ReadSettingsXmlFile();
        InitData();
        InitEmptyTables();
      }
    }

    public void UppdateraData(KorusDataEventArgs aData)
    {
      try
      {
        dataSetRöjning.Clear();

        DateTime datum;
        DateTime.TryParse(aData.data.datum, out datum);

        //userdata
        dataSetRöjning.Tables["UserData"].Rows.Add(aData.data.regionid, aData.data.regionnamn, aData.data.distriktid,
                                                   aData.data.distriktnamn, aData.data.ursprung, datum,
                                                   aData.data.traktnr, aData.data.traktnamn, aData.data.standort,
                                                   aData.data.entreprenor, aData.data.kommentar, string.Empty,
                                                   string.Empty, aData.data.provytestorlek, Resources.Lagrad,
                                                   DateTime.MinValue, aData.data.årtal, aData.data.malgrundyta,
                                                   aData.data.invtyp,DataSetParser.getInvNamn(aData.data.invtyp),aData.data.atgareal);

        //frågor
        dataSetRöjning.Tables["Fragor"].Rows.Add(aData.frågeformulär.fraga1, aData.frågeformulär.fraga2,
                                                 aData.frågeformulär.fraga3, aData.frågeformulär.fraga4,
                                                 aData.frågeformulär.fraga5, aData.frågeformulär.fraga6,
                                                 aData.frågeformulär.ovrigt);

        //ytor
        foreach (var yta in aData.ytor)
        {
          dataSetRöjning.Tables["Yta"].Rows.Add(yta.yta, yta.tall, yta.tallmedelhojd, yta.contorta,
                                                yta.contortamedelhojd, yta.gran, yta.granmedelhojd, yta.lov,
                                                yta.lovmedelhojd, yta.hst, yta.mhojd,yta.rojstam);
        }

        UpdateGUIData();
      }
      catch (Exception ex)
      {
#if !DEBUG
     MessageBox.Show("Misslyckades med att läsa in data", "Läsa data misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    public int GetTotalaAntaletYtor()
    {
      if (dataSetRöjning != null && dataSetRöjning.Tables["Yta"] != null)
      {
        return dataSetRöjning.Tables["Yta"].Rows.Count;
      }
      return 0;
    }

    public int GetProcentStatus()
    {
      return (int) ((progressBarData.Value/(double) progressBarData.Maximum)*100);
    }

    public string GetEgenuppföljningsTyp()
    {
      return Resources.Rojning;
    }

    public void ShowEditDialog(bool aRedigeraLagratData)
    {
      RedigeraLagratData = aRedigeraLagratData;
      ShowDialog();
    }

    /// <summary>
    ///   Writes the data that has been selected in the GUI.
    /// </summary>
    public bool WriteDataToReportXmlFile(string aFilePath, bool aShowDialog, bool aWriteSchema, string aReportStatus,
                                         string aReportStatusDatum)
    {
      XmlTextWriter objXmlReportWriter = null;
      XmlTextWriter objXmlSchemReportWriter = null;

      if (!aWriteSchema && aFilePath != null)
      {
        //check so the correct data exist for the report filename
        var reportName = GetReportFileName();

        var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(aFilePath);
        if (fileNameWithoutExtension != null && (reportName != null && !fileNameWithoutExtension.Equals(reportName)))
        {
          aFilePath = ShowSaveAsDialog(Settings.Default.ReportFilePath.Equals(string.Empty)
                                         ? mSDefaultReportsPath
                                         : Path.GetDirectoryName(Settings.Default.ReportFilePath), Resources.keg);
        }
      }

      if (aFilePath == null || aFilePath.Trim().Length <= 0)
      {
        return false;
      }

      InitEmptyTables();

      try
      {
        if (aWriteSchema)
        {
          objXmlSchemReportWriter = new XmlTextWriter(aFilePath, null) {Formatting = Formatting.Indented};
          objXmlSchemReportWriter.WriteStartDocument();
          dataSetRöjning.WriteXmlSchema(objXmlSchemReportWriter);
          objXmlSchemReportWriter.WriteEndDocument();
          objXmlSchemReportWriter.Flush();
        }
        else
        {
          objXmlReportWriter = new XmlTextWriter(aFilePath, null) {Formatting = Formatting.Indented};
          objXmlReportWriter.WriteStartDocument();
          SetDsStr("UserData", "Language", Settings.Default.SettingsLanguage);
          SetDsStr("UserData", "Status", aReportStatus);
          SetDsStr("UserData", "Status_Datum", aReportStatusDatum);
          dataSetRöjning.WriteXml(objXmlReportWriter);
          objXmlReportWriter.WriteEndDocument();
          objXmlReportWriter.Flush();
          Settings.Default.ReportFilePath = aFilePath;

          if (aShowDialog)
          {
            MessageBox.Show(String.Format(Resources.RapportenSparadMessage, Path.GetFileName(aFilePath))
                            , Resources.RapportSparadTitel, MessageBoxButtons.OK, MessageBoxIcon.Information);
          }
        }
      }
      catch (Exception ex)
      {
#if !DEBUG
     MessageBox.Show("Skriva data till rapportfil misslyckades", "Spara misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
        return false;
      }
      finally
      {
        if (objXmlReportWriter != null)
        {
          objXmlReportWriter.Close();
        }

        if (objXmlSchemReportWriter != null)
        {
          objXmlSchemReportWriter.Close();
        }
      }
      return true;
    }

    public void ShowReport()
    {
      try
      {
        reportRöjning.Show();
      }
      catch (Exception ex)
      {
#if !DEBUG
        MessageBox.Show(Resources.ForhandgranskningFailed);
#else
        MessageBox.Show(Resources.ForhandgranskningFailed + ex);
#endif
      }
    }


    public void ExporteraExcel2007()
    {
      try
      {
        InitExportDirs();
        //kolla först så användaren har skrivit in det datat som krävs för ett giltigt filnamn.        
        reportRöjning.Prepare();
        var export = new Excel2007Export();
        // Visa export alternativ
        if (!export.ShowDialog()) return;

        var filePath = ShowSaveAsDialog(mSDefaultExcel2007Path, Resources.Excel_suffix);
        if (filePath == null) return;
        reportRöjning.Export(export, filePath);
        MessageBox.Show(Resources.ExportRapport + filePath
                        , Resources.Exportering_lyckades, MessageBoxButtons.OK, MessageBoxIcon.Information);
      }
      catch (Exception ex)
      {
#if !DEBUG
          MessageBox.Show("Misslyckades med att exportera ut data till formatet: Excel 2007");
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    public void ExporteraAdobePDF()
    {
      try
      {
        InitExportDirs();
        //kolla först så användaren har skrivit in det datat som krävs för ett giltigt filnamn.        
        reportRöjning.Prepare();
        var export = new PDFExport();
        // Visa export alternativ
        if (!export.ShowDialog()) return;

        var filePath = ShowSaveAsDialog(mSDefaultPdfPath, Resources.Pdf_suffix);
        if (filePath == null) return;
        reportRöjning.Export(export, filePath);
        MessageBox.Show(Resources.ExportRapport + filePath
                        , Resources.Exportering_lyckades, MessageBoxButtons.OK, MessageBoxIcon.Information);
      }
      catch (Exception ex)
      {
#if !DEBUG
          MessageBox.Show("Misslyckades med att exportera ut data till formatet: Pdf");
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    /// <summary>
    ///   Checks the values in the GUI and puts together a name from the data as follows: [rapport_typ]_[regionid]_[distriktid]_[traktnr]_[ståndort]_[entreprenör]_[årtal].keg
    /// </summary>
    /// <returns> The filename. </returns>
    public string GetReportFileName()
    {
      var missingData = "";
      var filename = Resources.RojningPrefix; //Rapport typ

      //InvTyp
      var invtypid = GetDsStr("UserData", "InvTypId");

      if (invtypid.Equals(string.Empty) || invtypid.Equals("-1"))
      {
          missingData += " " + Resources.InvTypId;
      }
      else
      {
          filename += Resources.FileSep + invtypid;
      }

      //Region id
      var regionid = GetDsStr("UserData", "RegionId");
      if (regionid.Equals(string.Empty) || regionid.Equals("0") || regionid.Equals("-1"))
      {
        missingData += " " + Resources.RegionId;
      }
      else
      {
        filename += Resources.FileSep + regionid;
      }

      //Distrikt id
      var distriktid = GetDsStr("UserData", "DistriktId");
      if (distriktid.Equals(string.Empty) || distriktid.Equals("0") || distriktid.Equals("-1"))
      {
        missingData += " " + Resources.DistriktId;
      }
      else
      {
        filename += Resources.FileSep + distriktid;
      }

      //Artal
      DateTime date;
      DateTime.TryParse(GetDsStr("UserData", "Datum"), out date);

      if (date.Equals(DateTime.MinValue))
      {
        missingData += " " + Resources.Artal;
      }
      else
      {
        filename += Resources.FileSep + date.Year.ToString(CultureInfo.InvariantCulture);
      }

      //Traktnr
      var traktnr = GetDsStr("UserData", "Traktnr");
      if (traktnr.Equals(string.Empty) || traktnr.Equals("0") || traktnr.Equals("-1"))
      {
        missingData += " " + Resources.Traktnr;
      }
      else
      {
        filename += Resources.FileSep + traktnr;
      }

      //TraktDel
      var traktdel = GetDsStr("UserData", "Standort");
      if (traktdel.Equals(string.Empty) || traktdel.Equals("0") || traktdel.Equals("-1"))
      {
        missingData += " " + Resources.Standort;
      }
      else
      {
          filename += Resources.FileSep + traktdel;
      }

      //Entreprenörnr
      var entreprenör = GetDsStr("UserData", "Entreprenor");
      if (entreprenör.Equals(string.Empty) || entreprenör.Equals("0") || entreprenör.Equals("-1"))
      {
        missingData += " " + Resources.Entreprenor;
      }
      else
      {
        filename += Resources.FileSep + entreprenör;
      }

      if (!missingData.Equals(string.Empty))
      {
        //some data is missing to create the file name
        var errMessage = "Följande data saknas för att kunna spara rapporten: ";
        errMessage += missingData;
        errMessage += "\nFyll i dessa data och välj spara igen.";
        MessageBox.Show(errMessage, Resources.Data_saknas, MessageBoxButtons.OK, MessageBoxIcon.Stop);
        return null;
      }
      return filename;
    }

    public void SkrivUt()
    {
      try
      {
        reportRöjning.Print();
      }
      catch (Exception ex)
      {
#if !DEBUG
       MessageBox.Show("Skriva ut rapport misslyckades", "Skriva ut misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    public KorusDataEventArgs GetKORUSData()
    {
      var obj = new KorusDataEventArgs();

      //Nyckelvariabler
      int regionid, distriktid, traktnr, standort, entreprenor,invtypid;
      float atgareal;
      int.TryParse(GetDsStr("UserData", "InvTypId"), out invtypid);
      int.TryParse(GetDsStr("UserData", "RegionId"), out regionid);
      int.TryParse(GetDsStr("UserData", "DistriktId"), out distriktid);
      int.TryParse(GetDsStr("UserData", "Traktnr"), out traktnr);
      int.TryParse(GetDsStr("UserData", "Standort"), out standort);
      int.TryParse(GetDsStr("UserData", "Entreprenor"), out entreprenor);
      var årtal = GetDatumÅr(GetDsStr("UserData", "Datum"));
      float.TryParse(GetDsStr("UserData", "AtgAreal"), out atgareal);
      //UserData
      obj.data.invtyp = invtypid;
      obj.data.regionid = regionid; //int
      obj.data.distriktid = distriktid;
      obj.data.traktnr = traktnr;
      obj.data.standort = standort;
      obj.data.entreprenor = entreprenor;
      obj.data.rapport_typ = (int) RapportTyp.Röjning;
      obj.data.årtal = årtal;
      obj.data.atgareal = atgareal;

      obj.data.traktnamn = GetDsStr("UserData", "Traktnamn");
      obj.data.regionnamn = GetDsStr("UserData", "Region");
      obj.data.distriktnamn = GetDsStr("UserData", "Distrikt");
      obj.data.ursprung = GetDsStr("UserData", "Ursprung");
      obj.data.datum = GetDsStr("UserData", "Datum");
      obj.data.kommentar = GetDsStr("UserData", "Kommentar");
      int.TryParse(GetDsStr("UserData", "Provytestorlek"), out obj.data.provytestorlek);
      int.TryParse(GetDsStr("UserData", "Mal"), out obj.data.malgrundyta);

      obj.frågeformulär.fraga1 = GetDsStr("Fragor", "Fraga1");
      obj.frågeformulär.fraga2 = GetDsStr("Fragor", "Fraga2");
      obj.frågeformulär.fraga3 = GetDsStr("Fragor", "Fraga3");
      obj.frågeformulär.fraga4 = GetDsStr("Fragor", "Fraga4");
      obj.frågeformulär.fraga5 = GetDsStr("Fragor", "Fraga5");
      obj.frågeformulär.fraga6 = GetDsStr("Fragor", "Fraga6");
      obj.frågeformulär.ovrigt = GetDsStr("Fragor", "Ovrigt");

      //Ytor
      if (GetNrOfRows("Yta") > 0)
      {
        for (var i = 0; i < GetNrOfRows("Yta"); i++)
        {
          var yta = new KorusDataEventArgs.Yta();

          int.TryParse(GetDsStr("Yta", "Yta", i, dataSetRöjning), out yta.yta); //löpnummer
          int.TryParse(GetDsStr("Yta", "TallAntal", i, dataSetRöjning), out yta.tall);
          float.TryParse(GetDsStr("Yta", "TallMedelhojd", i, dataSetRöjning), out yta.tallmedelhojd);
          int.TryParse(GetDsStr("Yta", "ContAntal", i, dataSetRöjning), out yta.contorta);
          float.TryParse(GetDsStr("Yta", "ContMedelhojd", i, dataSetRöjning), out yta.contortamedelhojd);
          int.TryParse(GetDsStr("Yta", "GranAntal", i, dataSetRöjning), out yta.gran);
          float.TryParse(GetDsStr("Yta", "GranMedelhojd", i, dataSetRöjning), out yta.granmedelhojd);
          int.TryParse(GetDsStr("Yta", "BjorkAntal", i, dataSetRöjning), out yta.lov);
          float.TryParse(GetDsStr("Yta", "BjorkMedelhojd", i, dataSetRöjning), out yta.lovmedelhojd);
          int.TryParse(GetDsStr("Yta", "Hst", i, dataSetRöjning), out yta.hst);
          float.TryParse(GetDsStr("Yta", "MHojd", i, dataSetRöjning), out yta.mhojd);
          int.TryParse(GetDsStr("Yta", "RojstamAntal", i, dataSetRöjning), out yta.rojstam);

          obj.ytor.Add(yta);
        }
      }

      return obj;
    }

    public string GetDsStr(string aTable, string aColumn)
    {
      return GetDsStr(aTable, aColumn, 0, dataSetRöjning);
    }

    public void SändEpost(string aEpostAdress, string aTitel, string aMeddelande, bool aAnvändInternEpost,
                          bool aBifogRapport)
    {
      mEpostAdress = aEpostAdress;
      mTitel = aTitel;
      mMeddelande = aMeddelande;

      if (aAnvändInternEpost)
      {
        SendInternMail(aBifogRapport);
      }
      else
      {
        SendViaOutlook(aBifogRapport);
      }
    }

    #endregion

    private void avslutaToolStripMenuItem_Click(object sender, EventArgs e)
    {
      DataHelper.TvingaDataGridViewAttValidera(dataGridViewRöjningsstallen);
      DataSetParser.TabortFelaktigaYtor(dataSetRöjning);
      RedigeraLagratData = false; //uppdatera inte databasen
      Hide();
    }

    private void WriteSettingsXmlFile(bool aWriteSchema)
    {
      XmlTextWriter objXmlSettingsWriter = null;
      XmlTextWriter objXmlSettingsSchemaWriter = null;
      InitSettingsData();

      try
      {
        if (aWriteSchema)
        {
          objXmlSettingsSchemaWriter = new XmlTextWriter(mSSettingsPath + Settings.Default.SettingsSchemaFilename, null)
            {Formatting = Formatting.Indented};
          objXmlSettingsSchemaWriter.WriteStartDocument();
          dataSetSettings.WriteXmlSchema(objXmlSettingsSchemaWriter);
          objXmlSettingsSchemaWriter.WriteEndDocument();
          objXmlSettingsSchemaWriter.Flush();
        }
        else
        {
          objXmlSettingsWriter = new XmlTextWriter(mSSettingsFilePath, null) {Formatting = Formatting.Indented};
          objXmlSettingsWriter.WriteStartDocument();
          dataSetSettings.WriteXml(objXmlSettingsWriter);
          objXmlSettingsWriter.WriteEndDocument();
          objXmlSettingsWriter.Flush();
        }
      }
      catch (Exception ex)
      {
#if !DEBUG
       MessageBox.Show("Spara Settings data misslyckades", "Spara misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
      finally
      {
        if (objXmlSettingsWriter != null)
        {
          objXmlSettingsWriter.Close();
        }
        if (objXmlSettingsSchemaWriter != null)
        {
          objXmlSettingsSchemaWriter.Close();
        }
      }
    }

    /// <summary>
    ///   Read the XML file containing the settings for the Application.
    /// </summary>
    private void ReadSettingsXmlFile()
    {
      InitSettingsData();

      var objValidator = new XmlValidator(mSSettingsFilePath,
                                          mSSettingsPath + Settings.Default.SettingsSchemaFilename);

      if (objValidator.ValidateXmlFileFailed())
      {
        //something is wrong with the Settings file then write a new Settings and Schema file.
        WriteSettingsXmlFile(false);
        WriteSettingsXmlFile(true);
      }

      try
      {
        dataSetSettings.Clear();
        dataSetSettings.ReadXml(mSSettingsFilePath);
      }
      catch (Exception ex)
      {
#if !DEBUG
       MessageBox.Show("Läsa Settings data misslyckades", "Läsa misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    private void LäggTillHögstaVärdetFörYta(int aRowIndex)
    {
      try
      {
        var slNr =
          DataSetParser.GetObjIntValue(
            dataGridViewRöjningsstallen.Rows[aRowIndex].Cells[ytaDataGridViewTextBoxColumn.Name].Value);

        if (slNr >= 1 || dataGridViewRöjningsstallen.Rows.Count <= 0) return;

//Yta får inte vara mindre än 1, leta upp det högsta Yta och fortsätt på det
        var maxValue = 0;
        for (var i = 0; i < dataGridViewRöjningsstallen.Rows.Count; i++)
        {
          var value =
            DataSetParser.GetObjIntValue(
              dataGridViewRöjningsstallen.Rows[i].Cells[ytaDataGridViewTextBoxColumn.Name].Value);

          if (value > maxValue)
          {
            maxValue = value;
          }
        }
        //lägg på +1 och sätt in det i sista raden för slNr
        dataGridViewRöjningsstallen.Rows[aRowIndex].Cells[ytaDataGridViewTextBoxColumn.Name].Value = maxValue + 1;

        if (
          !dataGridViewRöjningsstallen.Rows[aRowIndex].Cells[ytaDataGridViewTextBoxColumn.Name].Value.ToString().Equals
             (string.Empty)) return;

//inget nytt yt nummer kunde anges, en bugg som kan hända i tabellkomponenten, i så fall ta bort raden.
        if (aRowIndex >= 0 && aRowIndex < (dataGridViewRöjningsstallen.Rows.Count - 2))
        {
          dataGridViewRöjningsstallen.Rows.RemoveAt(aRowIndex);
        }
      }
      catch (Exception ex)
      {
#if !DEBUG
       MessageBox.Show("Misslyckades med att öka nummret för yta.", "Kunde inte sätta yt-numret.", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    /// <summary>
    ///   Initializes the report data tables.
    /// </summary>
    private void InitEmptyTables()
    {
      if (dataSetRöjning.Tables["UserData"].Rows.Count == 0)
      {
        dataSetRöjning.Tables["UserData"].Rows.Add(0, string.Empty, 0, string.Empty, string.Empty, DateTime.Today, 0,
                                                   string.Empty, 0, 0, string.Empty, string.Empty, string.Empty, 0,
                                                   Resources.Obehandlad, DateTime.MinValue, DateTime.Today.Year, 0);
      }

      if (dataSetRöjning.Tables["Fragor"].Rows.Count == 0)
      {
        dataSetRöjning.Tables["Fragor"].Rows.Add(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty,
                                                 string.Empty, string.Empty);
      }

      if (dataSetRöjning.Tables["Yta"].Rows.Count == 0)
      {
//If no rows exist then add a empty one before save.
          dataSetRöjning.Tables["Yta"].Rows.Add(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
      }

      if (dataSetCalculations.Tables["Medelvarde"].Rows.Count == 0)
      {
          dataSetCalculations.Tables["Medelvarde"].Rows.Add(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
      }

      if (dataSetCalculations.Tables["Summa"].Rows.Count == 0)
      {
          dataSetCalculations.Tables["Summa"].Rows.Add(0, 0.0, 0, 0.0, 0, 0.0, 0, 0.0, 0, 0.0, 0.0);
      }
      StatusFrågor();
    }

    /// <summary>
    ///   Sets a hardcoded default settingsdata.
    /// </summary>
    private void DefaultSettingsData()
    {
        dataSetSettings.Tables["InvTyp"].Rows.Add("0", "Egen");
        dataSetSettings.Tables["InvTyp"].Rows.Add("1", "Distrikt");

        dataSetSettings.Tables["Region"].Rows.Add("11", "11 - Nord");
        dataSetSettings.Tables["Region"].Rows.Add("16", "16 - Mitt");
        dataSetSettings.Tables["Region"].Rows.Add("18", "18 - Syd");

        dataSetSettings.Tables["Distrikt"].Rows.Add("11", "14", "14 - Örnsköldsvik");
        dataSetSettings.Tables["Distrikt"].Rows.Add("11", "19", "19 - Umeå");
        dataSetSettings.Tables["Distrikt"].Rows.Add("16", "12", "12 - Sveg");
        dataSetSettings.Tables["Distrikt"].Rows.Add("16", "14", "14 - Ljusdal");
        dataSetSettings.Tables["Distrikt"].Rows.Add("16", "15", "15 - Delsbo");
        dataSetSettings.Tables["Distrikt"].Rows.Add("16", "16", "16 - Hudiksvall");
        dataSetSettings.Tables["Distrikt"].Rows.Add("16", "18", "18 - Bollnäs");
        dataSetSettings.Tables["Distrikt"].Rows.Add("16", "21", "21 - Uppland");
        dataSetSettings.Tables["Distrikt"].Rows.Add("18", "10", "10 - Västerås");
        dataSetSettings.Tables["Distrikt"].Rows.Add("18", "11", "11 - Örebro");
        dataSetSettings.Tables["Distrikt"].Rows.Add("18", "12", "12 - Nyköping");
        dataSetSettings.Tables["Distrikt"].Rows.Add("18", "13", "13 - Götaland");
        dataSetSettings.Tables["Distrikt"].Rows.Add("18", "21", "21 - Egen Skog");

      /*dataSetSettings.Tables["Standort"].Rows.Add("1");
      dataSetSettings.Tables["Standort"].Rows.Add("2");
      dataSetSettings.Tables["Standort"].Rows.Add("3");
      dataSetSettings.Tables["Standort"].Rows.Add("4");
      dataSetSettings.Tables["Standort"].Rows.Add("5");
      dataSetSettings.Tables["Standort"].Rows.Add("6");*/

      dataSetSettings.Tables["Markberedningsmetod"].Rows.Add("Kontinuerligt harv");
      dataSetSettings.Tables["Markberedningsmetod"].Rows.Add("Högläggning");
      dataSetSettings.Tables["Markberedningsmetod"].Rows.Add("För sådd");

      dataSetSettings.Tables["Ursprung"].Rows.Add("Eget");
      dataSetSettings.Tables["Ursprung"].Rows.Add("Köp");
    }

    /// <summary>
    ///   Initializes the settings data tables.
    /// </summary>
    private void InitSettingsData()
    {
      if (dataSetSettings.Tables["Region"].Rows.Count == 0)
      {
        dataSetSettings.Tables["Region"].Rows.Add(0, string.Empty);
      }

      if (dataSetSettings.Tables["Distrikt"].Rows.Count == 0)
      {
        dataSetSettings.Tables["Distrikt"].Rows.Add(0, 0, string.Empty);
      }

      if (dataSetSettings.Tables["InvTyp"].Rows.Count == 0)
      {
        dataSetSettings.Tables["InvTyp"].Rows.Add(-1,string.Empty);
      }

      if (dataSetSettings.Tables["Markberedningsmetod"].Rows.Count == 0)
      {
        dataSetSettings.Tables["Markberedningsmetod"].Rows.Add(string.Empty);
      }

      if (dataSetSettings.Tables["Ursprung"].Rows.Count == 0)
      {
        dataSetSettings.Tables["Ursprung"].Rows.Add(string.Empty);
      }
    }

    /// <summary>
    ///   first check the language in the file, if it is a new language, then give a question to restart with that file
    /// </summary>
    /// <param name="aFilePath"> File path to the report. </param>
    /// <param name="aStartup"> Flag that indicates if it is load in startup or not </param>
    /// <returns> true if Language check is ignored (same language) false if user selected cancel on the dialog. </returns>
    private static bool CheckLanguage(string aFilePath, bool aStartup)
    {
      if (!aStartup)
      {
        using (var tmpSet = new DataSet())
        {
          tmpSet.ReadXml(aFilePath);
          if (tmpSet.Tables["UserData"].Rows[0]["Language"] != null)
          {
            var language = tmpSet.Tables["UserData"].Rows[0]["Language"].ToString();
            if (!language.Equals(Settings.Default.SettingsLanguage))
            {
              if (MessageBox.Show(
                String.Format(
                  "Rapporten {0} är inställd på språket {1} ({2}). Vill du ändra till det språket och ladda rapporten?",
                  Path.GetFileName(aFilePath), language, GetLanguage(language))
                , Resources.Andra_sprak, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
              {
                Settings.Default.ReportFilePath = aFilePath;
                ChangeLanguage(language);
              }
              else
              {
                //User cancel, then cancel to load of the report, 
                return false;
              }
            }
          }
        }
      }
      return true;
    }

    private void UpdateGUIData()
    {
      CalculateValues();
      StatusFrågor();
      ProgressNumberOfDataDone();
    }

    /// <summary>
    ///   Reads the active Report XML document.
    /// </summary>
    private void ReadDataFromReportXmlFile(string aFilePath, bool aStartup)
    {
      InitEmptyTables();

      if (!File.Exists(aFilePath))
      {
        //Return if the file does not exists.
        return;
      }

      if (!File.Exists(mSSettingsPath + Settings.Default.ReportSchemaRojning))
      {
        //Write a new Report Schema file if it does not exist.
        WriteDataToReportXmlFile(mSSettingsPath + Settings.Default.ReportSchemaRojning, false, true,
                                 Settings.Default.ReportStatus, Settings.Default.ReportStatusDatum);
      }

      var objValidator = new XmlValidator(aFilePath, mSSettingsPath + Settings.Default.ReportSchemaRojning);

      if (objValidator.ValidateXmlFileFailed())
      {
        //something is wrong with the XML file.
        if (
          MessageBox.Show(Resources.EjGiltigFil, Resources.FelaktigKegFil, MessageBoxButtons.YesNo,
                          MessageBoxIcon.Question) == DialogResult.No)
        {
          return;
        }
      }

      try
      {
        //first check the language.
        if (!CheckLanguage(aFilePath, aStartup)) return;

        dataSetRöjning.Clear();
        dataSetRöjning.ReadXml(aFilePath);
        UpdateGUIData();
        Settings.Default.ReportFilePath = aFilePath;
      }
      catch (Exception ex)
      {
#if !DEBUG
     MessageBox.Show("Läsa data från rapport fil misslyckades", "Läsa data misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    private void InitExportDirs()
    {
      if (!Directory.Exists(mSDefaultExcel2007Path))
      {
        //Report dir does not exists, create it.
        Directory.CreateDirectory(mSDefaultExcel2007Path);
      }
      if (!Directory.Exists(mSDefaultPdfPath))
      {
        //Report dir does not exists, create it.
        Directory.CreateDirectory(mSDefaultPdfPath);
      }
    }

    /// <summary>
    ///   Checks so the application got the required files and directories otherwise create them.
    /// </summary>
    private void InitApplicationDirFilesStructure()
    {
      if (!Directory.Exists(mSSettingsPath))
      {
        //Settings dir does not exists, create it.
        Directory.CreateDirectory(mSSettingsPath);
      }

      if (!Directory.Exists(mSDefaultReportsPath))
      {
        //Report dir does not exists, create it.
        Directory.CreateDirectory(mSDefaultReportsPath);
      }

      if (!File.Exists(mSSettingsFilePath))
      {
        //Settings xml file does not exist, create a default file.
        DefaultSettingsData();
        WriteSettingsXmlFile(false);
      }
      if (!File.Exists(mSSettingsPath + Settings.Default.SettingsSchemaFilename))
      {
        //The schema settings file is missing, create it.
        WriteSettingsXmlFile(true);
      }

      if (!File.Exists(mSSettingsPath + Settings.Default.ReportSchemaRojning))
      {
        //The schema report file is missing, create it.
        WriteDataToReportXmlFile(mSSettingsPath + Settings.Default.ReportSchemaRojning, false, true,
                                 Settings.Default.ReportStatus, Settings.Default.ReportStatusDatum);
      }
    }

    private void LoadRöjning()
    {
      InitApplicationDirFilesStructure();
      ReadSettingsXmlFile();
      InitData();
      ReadDataFromReportXmlFile(Settings.Default.ReportFilePath, true);
      CalculateValues();
    }

    /// <summary>
    ///   Called when the applications starts.
    /// </summary>
    /// <param name="sender"> Sender object </param>
    /// <param name="e"> Event arguments </param>
    private void Röjning_Load(object sender, EventArgs e)
    {
    }


    /// <summary>
    ///   Adds the rows together and calculates the different sums in the table and shows it to the user.
    /// </summary>
    private void UpdateSumValues()
    {
        int sumTallAntal = 0, sumContAntal = 0, sumGranAntal = 0, sumBjorkAntal = 0, sumHst = 0, sumRojstamAntal = 0; 
      double sumTallMedelvarde = 0.0,
             sumContMedelvarde = 0.0,
             sumGranMedelvarde = 0.0,
             sumBjorkMedelvarde = 0.0,
             sumMHojd = 0.0;


      try
      {
        for (var i = 0; i < AntalYtor(); i++)
        {
          sumTallAntal +=
            DataSetParser.GetObjIntValue(
              dataGridViewRöjningsstallen.Rows[i].Cells[tallAntalDataGridViewTextBoxColumn.Name].Value, true);
          sumTallMedelvarde +=
            DataSetParser.GetObjDoubleValue(
              dataGridViewRöjningsstallen.Rows[i].Cells[tallMedelhojdDataGridViewTextBoxColumn.Name].Value, true);
          sumContAntal +=
            DataSetParser.GetObjIntValue(
              dataGridViewRöjningsstallen.Rows[i].Cells[contAntalDataGridViewTextBoxColumn.Name].Value, true);
          sumContMedelvarde +=
            DataSetParser.GetObjDoubleValue(
              dataGridViewRöjningsstallen.Rows[i].Cells[contMedelhojdDataGridViewTextBoxColumn.Name].Value, true);
          sumGranAntal +=
            DataSetParser.GetObjIntValue(
              dataGridViewRöjningsstallen.Rows[i].Cells[granAntalDataGridViewTextBoxColumn.Name].Value, true);
          sumGranMedelvarde +=
            DataSetParser.GetObjDoubleValue(
              dataGridViewRöjningsstallen.Rows[i].Cells[granMedelhojdDataGridViewTextBoxColumn.Name].Value, true);
          sumBjorkAntal +=
            DataSetParser.GetObjIntValue(
              dataGridViewRöjningsstallen.Rows[i].Cells[bjorkAntalDataGridViewTextBoxColumn.Name].Value, true);
          sumBjorkMedelvarde +=
            DataSetParser.GetObjDoubleValue(
              dataGridViewRöjningsstallen.Rows[i].Cells[bjorkMedelhojdDataGridViewTextBoxColumn.Name].Value, true);
          sumHst +=
            DataSetParser.GetObjIntValue(
              dataGridViewRöjningsstallen.Rows[i].Cells[hstAntalDataGridViewTextBoxColumn.Name].Value, true);
          sumMHojd +=
            DataSetParser.GetObjDoubleValue(
              dataGridViewRöjningsstallen.Rows[i].Cells[barrMedelhojdDataGridViewTextBoxColumn.Name].Value, true);
          sumRojstamAntal +=
                        DataSetParser.GetObjIntValue(
                          dataGridViewRöjningsstallen.Rows[i].Cells[RojstamAntalDataGridViewTextBoxColumn.Name].Value, true);
        }

        //Update the total sum textboxes

        textBoxSummaRojstamAntal.Text = sumRojstamAntal.ToString(CultureInfo.InvariantCulture);
        DataSetParser.SetDsInt("Summa", "RojstamSummaAntal", sumRojstamAntal, dataSetCalculations);

        textBoxSummaTallAntal.Text = sumTallAntal.ToString(CultureInfo.InvariantCulture);
        DataSetParser.SetDsInt("Summa", "TallSummaAntal", sumTallAntal, dataSetCalculations);
        textBoxSummaTallMedelhojd.Text = sumTallMedelvarde.ToString(CultureInfo.InvariantCulture);
        DataSetParser.SetDsDouble("Summa", "TallSummaMHojd", sumTallMedelvarde, dataSetCalculations);

        textBoxSummaContAntal.Text = sumContAntal.ToString(CultureInfo.InvariantCulture);
        DataSetParser.SetDsInt("Summa", "ContSummaAntal", sumContAntal, dataSetCalculations);
        textBoxSummaContMedelhojd.Text = sumContMedelvarde.ToString(CultureInfo.InvariantCulture);
        DataSetParser.SetDsDouble("Summa", "ContSummaMHojd", sumContMedelvarde, dataSetCalculations);

        textBoxSummaGranAntal.Text = sumGranAntal.ToString(CultureInfo.InvariantCulture);
        DataSetParser.SetDsInt("Summa", "GranSummaAntal", sumGranAntal, dataSetCalculations);
        textBoxSummaGranMedelhojd.Text = sumGranMedelvarde.ToString(CultureInfo.InvariantCulture);
        DataSetParser.SetDsDouble("Summa", "GranSummaMHojd", sumGranMedelvarde, dataSetCalculations);

        textBoxSummaBjorkAntal.Text = sumBjorkAntal.ToString(CultureInfo.InvariantCulture);
        DataSetParser.SetDsInt("Summa", "BjorkSummaAntal", sumBjorkAntal, dataSetCalculations);
        textBoxSummaBjorkMedelhojd.Text = sumBjorkMedelvarde.ToString(CultureInfo.InvariantCulture);
        DataSetParser.SetDsDouble("Summa", "BjorkSummaMHojd", sumBjorkMedelvarde, dataSetCalculations);

        textBoxSummaHst.Text = sumHst.ToString(CultureInfo.InvariantCulture);
        DataSetParser.SetDsInt("Summa", "HstSummaAntal", sumHst, dataSetCalculations);
        textBoxSummaBarrMHojd.Text = Math.Round(sumMHojd, 1).ToString(CultureInfo.InvariantCulture);
        DataSetParser.SetDsDouble("Summa", "BarrHstMHojd", sumMHojd, dataSetCalculations);

        ProgressNumberOfDataDone();
      }
      catch (Exception ex)
      {
#if !DEBUG
       MessageBox.Show("Beräkna summa värden misslyckades.", "Beräkning misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    /// <summary>
    ///   Updates the "Optimalt+Bra" value in the tabel.
    /// </summary>
    /// <param name="aRow"> The DataGridView row </param>
    private void UpdateHstAndMHojd(DataGridViewRow aRow)
    {
      try
      {
        var valueTallAntal = DataSetParser.GetObjIntValue(aRow.Cells[tallAntalDataGridViewTextBoxColumn.Name].Value,
                                                          true);
        var valueTallMedelHojd =
          DataSetParser.GetObjDoubleValue(aRow.Cells[tallMedelhojdDataGridViewTextBoxColumn.Name].Value, true);
        var valueContAntal = DataSetParser.GetObjIntValue(aRow.Cells[contAntalDataGridViewTextBoxColumn.Name].Value,
                                                          true);
        var valueContMedelhojd =
          DataSetParser.GetObjDoubleValue(aRow.Cells[contMedelhojdDataGridViewTextBoxColumn.Name].Value, true);
        var valueGranAntal = DataSetParser.GetObjIntValue(aRow.Cells[granAntalDataGridViewTextBoxColumn.Name].Value,
                                                          true);
        var valueGranMedelhojd =
          DataSetParser.GetObjDoubleValue(aRow.Cells[granMedelhojdDataGridViewTextBoxColumn.Name].Value, true);
        var valueBjorkAntal = DataSetParser.GetObjIntValue(aRow.Cells[bjorkAntalDataGridViewTextBoxColumn.Name].Value,
                                                           true);
        DataSetParser.GetObjDoubleValue(aRow.Cells[bjorkMedelhojdDataGridViewTextBoxColumn.Name].Value, true);
        var sumBarr = (valueTallAntal + valueContAntal + valueGranAntal + valueBjorkAntal);

        aRow.Cells[hstAntalDataGridViewTextBoxColumn.Name].Value = sumBarr > 0 ? sumBarr : 0;

        var taljare = (valueTallAntal*valueTallMedelHojd + valueContAntal*valueContMedelhojd +
                       valueGranAntal*valueGranMedelhojd);
        double namnare = (valueTallAntal + valueContAntal + valueGranAntal);

        if (taljare > 0 && namnare > 0)
        {
          aRow.Cells[barrMedelhojdDataGridViewTextBoxColumn.Name].Value = (Math.Round(taljare/namnare, 1));
        }
        else
        {
          aRow.Cells[barrMedelhojdDataGridViewTextBoxColumn.Name].Value = 0.0;
        }
      }
      catch (Exception ex)
      {
#if !DEBUG
       MessageBox.Show("Beräkna Optimalt+Bra misslyckades", "Beräkna misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }


    private double GetMeanValue(int sumValue, bool aRound)
    {
      return GetMeanValue(sumValue, string.Empty, aRound);
    }

    private double GetMeanValue(double sumValue, string columnName, bool aRound)
    {
      var meanValue = 0.0;
      try
      {
        if (!columnName.Equals(string.Empty))
        {
          var totalTäljare = 0.0;
          var totaltNämnare = 0;
          foreach (DataGridViewRow row in dataGridViewRöjningsstallen.Rows)
          {
            if (row.Cells[columnName] == null) continue;
            var columnIndex = row.Cells[columnName].ColumnIndex;
            if (columnIndex <= 0) continue;
            var antal = DataSetParser.GetObjIntValue(row.Cells[columnIndex - 1].Value, true);
            var höjd = DataSetParser.GetObjDoubleValue(row.Cells[columnName].Value, true);
            totalTäljare += (antal*höjd);
            totaltNämnare += antal;
          }
          if (totalTäljare > 0 && totaltNämnare > 0)
          {
            meanValue = totalTäljare/totaltNämnare;
          }
        }
        else
        {
          var totalNrOfRows = AntalYtor();

          if (sumValue > 0 && totalNrOfRows > 0)
          {
            meanValue = sumValue/totalNrOfRows;
          }
        }

        if (aRound)
        {
          //Round the value 
          meanValue = Math.Round(meanValue, 1);
        }
      }
      catch (Exception ex)
      {
        //Math Round failed.
#if !DEBUG
        meanValue = 0.0;
#else
        MessageBox.Show(ex.ToString());
#endif
      }

      return meanValue;
    }

    /// <summary>
    ///   Updates the meanvalues in the GUI.
    /// </summary>
    private void UpdateMeanValues()
    {
        var medelvardeRojstamAntal = GetMeanValue(DataSetParser.GetObjIntValue(textBoxSummaRojstamAntal.Text, true), false);
        textBoxMedelvardeRojstamAntal.Text = Math.Round(medelvardeRojstamAntal, 1).ToString(CultureInfo.InvariantCulture);
        DataSetParser.SetDsDouble("Medelvarde", "RojstamMedelAntal", medelvardeRojstamAntal, dataSetCalculations);

      var medelvardeTallAntal = GetMeanValue(DataSetParser.GetObjIntValue(textBoxSummaTallAntal.Text, true), false);
      textBoxMedelvardeTallAntal.Text = Math.Round(medelvardeTallAntal, 1).ToString(CultureInfo.InvariantCulture);
      DataSetParser.SetDsDouble("Medelvarde", "TallMedelAntal", medelvardeTallAntal, dataSetCalculations);

      var medelvardeTallMedelhojd = GetMeanValue(DataSetParser.GetObjDoubleValue(textBoxSummaTallMedelhojd.Text, true),
                                                 tallMedelhojdDataGridViewTextBoxColumn.Name, false);
      textBoxMedelvardeTallMedelhojd.Text = Math.Round(medelvardeTallMedelhojd, 1)
                                                .ToString(CultureInfo.InvariantCulture);
      DataSetParser.SetDsDouble("Medelvarde", "TallMedelMHojd", medelvardeTallMedelhojd, dataSetCalculations);

      var medelvardeContAntal = GetMeanValue(DataSetParser.GetObjIntValue(textBoxSummaContAntal.Text, true), false);
      textBoxMedelvardeContAntal.Text = Math.Round(medelvardeContAntal, 1).ToString(CultureInfo.InvariantCulture);
      DataSetParser.SetDsDouble("Medelvarde", "ContMedelAntal", medelvardeContAntal, dataSetCalculations);

      var medelvardeContMedelhojd = GetMeanValue(DataSetParser.GetObjDoubleValue(textBoxSummaContMedelhojd.Text, true),
                                                 contMedelhojdDataGridViewTextBoxColumn.Name, false);
      textBoxMedelvardeContMedelhojd.Text = Math.Round(medelvardeContMedelhojd, 1)
                                                .ToString(CultureInfo.InvariantCulture);
      DataSetParser.SetDsDouble("Medelvarde", "ContMedelMHojd", medelvardeContMedelhojd, dataSetCalculations);

      var medelvardeGranAntal = GetMeanValue(DataSetParser.GetObjIntValue(textBoxSummaGranAntal.Text, true), false);
      textBoxMedelvardeGranAntal.Text = Math.Round(medelvardeGranAntal, 1).ToString(CultureInfo.InvariantCulture);
      DataSetParser.SetDsDouble("Medelvarde", "GranMedelAntal", medelvardeGranAntal, dataSetCalculations);

      var medelvardeGranMedelhojd = GetMeanValue(DataSetParser.GetObjDoubleValue(textBoxSummaGranMedelhojd.Text, true),
                                                 granMedelhojdDataGridViewTextBoxColumn.Name, false);

      textBoxMedelvardeGranMedelhojd.Text = Math.Round(medelvardeGranMedelhojd, 1)
                                                .ToString(CultureInfo.InvariantCulture);
      DataSetParser.SetDsDouble("Medelvarde", "GranMedelMHojd", medelvardeGranMedelhojd, dataSetCalculations);

      var medelvardeBjorkAntal = GetMeanValue(DataSetParser.GetObjIntValue(textBoxSummaBjorkAntal.Text, true), false);
      textBoxMedelvardeBjorkAntal.Text = Math.Round(medelvardeBjorkAntal, 1).ToString(CultureInfo.InvariantCulture);
      DataSetParser.SetDsDouble("Medelvarde", "BjorkMedelAntal", medelvardeBjorkAntal, dataSetCalculations);

      var medelvardeBjorkMedelhojd = GetMeanValue(
        DataSetParser.GetObjDoubleValue(textBoxSummaBjorkMedelhojd.Text, true),
        bjorkMedelhojdDataGridViewTextBoxColumn.Name, false);
      textBoxMedelvardeBjorkMedelhojd.Text =
        Math.Round(medelvardeBjorkMedelhojd, 1).ToString(CultureInfo.InvariantCulture);
      DataSetParser.SetDsDouble("Medelvarde", "BjorkMedelMHojd", medelvardeBjorkMedelhojd, dataSetCalculations);

      var medelvardeHst = GetMeanValue(DataSetParser.GetObjIntValue(textBoxSummaHst.Text, true), false);
      textBoxMedelvardeHst.Text = Math.Round(medelvardeHst, 1).ToString(CultureInfo.InvariantCulture);
      DataSetParser.SetDsDouble("Medelvarde", "HstMedelAntal", medelvardeHst, dataSetCalculations);

      var täljare = (DataSetParser.GetObjIntValue(textBoxSummaTallAntal.Text, true)*medelvardeTallMedelhojd)
                    + (DataSetParser.GetObjIntValue(textBoxSummaGranAntal.Text, true)*medelvardeGranMedelhojd)
                    + (DataSetParser.GetObjIntValue(textBoxSummaContAntal.Text, true)*medelvardeContMedelhojd);

      var nämnare = DataSetParser.GetObjIntValue(textBoxSummaTallAntal.Text, true)
                    + DataSetParser.GetObjIntValue(textBoxSummaGranAntal.Text, true)
                    + DataSetParser.GetObjIntValue(textBoxSummaContAntal.Text, true);


      if (täljare <= 0 || nämnare <= 0)
      {
        textBoxMedelvardeBarrMHojd.Text = Resources.Noll;
      }
      else
      {
        var medelvardeBarrMedelHojd = (täljare/nämnare);
        textBoxMedelvardeBarrMHojd.Text = Math.Round(medelvardeBarrMedelHojd, 1).ToString(CultureInfo.InvariantCulture);
        DataSetParser.SetDsDouble("Medelvarde", "BarrMedelMHojd", medelvardeBarrMedelHojd, dataSetCalculations);
      }
    }

    private void UpdateRöjningValues()
    {
      try
      {
        var provYteStorlek = DataSetParser.GetObjDoubleValue(textBoxProvytestorlek.Text, true);

        textBoxProvytestorlekRadie.Text = provYteStorlek > 0
                                            ? Math.Round(Math.Sqrt(provYteStorlek/Math.PI), 1).ToString(
                                              CultureInfo.InvariantCulture)
                                            : Resources.Noll;

        var sumMedelHojd = GetMeanValue(DataSetParser.GetObjIntValue(textBoxSummaTallAntal.Text), false) +
                           GetMeanValue(DataSetParser.GetObjIntValue(textBoxSummaContAntal.Text), false)
                           + GetMeanValue(DataSetParser.GetObjIntValue(textBoxSummaGranAntal.Text), false) +
                           GetMeanValue(DataSetParser.GetObjIntValue(textBoxSummaBjorkAntal.Text), false);


        textBoxTotaltHuvudstammar.Text =
          Math.Round(provYteStorlek*sumMedelHojd, 0).ToString(CultureInfo.InvariantCulture);

        textBoxBarrhuvudstammarsHojd.Text = textBoxMedelvardeBarrMHojd.Text;
        textBoxLovhuvudstammarsHojd.Text = textBoxMedelvardeBjorkMedelhojd.Text;

        //Måluppfyllelse
        var totaltHuvudstammar = DataSetParser.GetObjIntValue(textBoxTotaltHuvudstammar.Text);
        var mål = DataSetParser.GetObjIntValue(textBoxMål.Text);

        if (totaltHuvudstammar > 0 && mål > 0)
        {
          textBoxMåluppfyllelse.Text =
            Math.Round((totaltHuvudstammar/(double) mål)*100, 1).ToString(CultureInfo.InvariantCulture);
        }
        else
        {
          textBoxMåluppfyllelse.Text = Resources.Noll;
        }

        //Träslagsblandning

        var tallSum = DataSetParser.GetObjDoubleValue(textBoxSummaTallAntal.Text, true);
        var hStSum = DataSetParser.GetObjDoubleValue(textBoxSummaHst.Text, true);

        if (tallSum > 0 && hStSum > 0)
        {
          textBoxT.Text = Math.Round((100*tallSum)/hStSum, 0).ToString(CultureInfo.InvariantCulture);
        }
        else
        {
          textBoxT.Text = Resources.Noll;
        }

        var contSum = DataSetParser.GetObjDoubleValue(textBoxSummaContAntal.Text, true);

        if (contSum > 0 && hStSum > 0)
        {
          textBoxC.Text = Math.Round((100*contSum)/hStSum, 0).ToString(CultureInfo.InvariantCulture);
        }
        else
        {
          textBoxC.Text = Resources.Noll;
        }

        var granSum = DataSetParser.GetObjDoubleValue(textBoxSummaGranAntal.Text, true);

        if (granSum > 0 && hStSum > 0)
        {
          textBoxG.Text = Math.Round((100*granSum)/hStSum, 0).ToString(CultureInfo.InvariantCulture);
        }
        else
        {
          textBoxG.Text = Resources.Noll;
        }

        var bjorkSum = DataSetParser.GetObjDoubleValue(textBoxSummaBjorkAntal.Text, true);

        if (bjorkSum > 0 && hStSum > 0)
        {
          textBoxL.Text = Math.Round((100*bjorkSum)/hStSum, 0).ToString(CultureInfo.InvariantCulture);
        }
        else
        {
          textBoxL.Text = Resources.Noll;
        }
      }
      catch (Exception ex)
      {
#if !DEBUG
        MessageBox.Show("Misslyckades med att beräkna röjnings värden.", "Beräkna röjningsvärden misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    private void CalculateValues()
    {
      UpdateSumValues();
      UpdateMeanValues();
      UpdateRöjningValues();
    }

    private void sparaToolStripMenuItem_Click(object sender, EventArgs e)
    {
      DataHelper.TvingaDataGridViewAttValidera(dataGridViewRöjningsstallen);
      DataSetParser.TabortFelaktigaYtor(dataSetRöjning);
      if (RedigeraLagratData)
      {
        Hide();
      }
      else
      {
        WriteDataToReportXmlFile(Settings.Default.ReportFilePath, true, false, Resources.Obehandlad,
                                 DateTime.MinValue.ToString(CultureInfo.InvariantCulture));
      }
    }

    /// <summary>
    ///   Shows the save as dialog to let the user be able to save the report with a custom name.
    /// </summary>
    /// <returns> The file path name. </returns>
    private string ShowSaveAsDialog(string aSelectedPath, string aFileSuffix)
    {
      // folderBrowserSaveReportDialog.RootFolder = Environment.SpecialFolder.MyComputer;

      saveReportFolderBrowserDialog.SelectedPath = aSelectedPath;

      var fileName = GetReportFileName();

      if (fileName != null)
      {
        saveReportFolderBrowserDialog.Description = Resources.Spara_rapport + fileName + aFileSuffix;

        if (saveReportFolderBrowserDialog.ShowDialog() == DialogResult.OK)
        {
          return saveReportFolderBrowserDialog.SelectedPath + Path.DirectorySeparatorChar + fileName + aFileSuffix;
        }
      }
      return null;
    }


    private void sparaSomToolStripMenuItem_Click(object sender, EventArgs e)
    {
      var filePath = ShowSaveAsDialog(Settings.Default.ReportFilePath.Equals(string.Empty)
                                        ? mSDefaultReportsPath
                                        : Path.GetDirectoryName(Settings.Default.ReportFilePath), Resources.keg);
      if (File.Exists(filePath))
      {
        if (
          MessageBox.Show(
            Resources.Skriva_over_redan_befintlig_rapport + Path.GetFileNameWithoutExtension(filePath) +
            Resources.Fragetecken,
            Resources.Skriva_over_rapport, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
        {
          return;
        }
      }
      DataHelper.TvingaDataGridViewAttValidera(dataGridViewRöjningsstallen);
      DataSetParser.TabortFelaktigaYtor(dataSetRöjning);
      WriteDataToReportXmlFile(filePath, true, false, Settings.Default.ReportStatus, Settings.Default.ReportStatusDatum);
    }

    private void Röjning_FormClosing(object sender, FormClosingEventArgs e)
    {
      Settings.Default.Save();
      BindingContext[dataSetRöjning].EndCurrentEdit();

      e.Cancel = true;
      Hide();
    }

    private void InitData()
    {
      dataSetRöjning.Clear();
      dataSetCalculations.Clear();
      dataSetRöjning.Tables["Yta"].Columns["Yta"].AutoIncrementStep = -1;
      dataSetRöjning.Tables["Yta"].Columns["Yta"].AutoIncrementSeed = -1;
      dataSetRöjning.Tables["Yta"].Columns["Yta"].AutoIncrementStep = 1;
      dataSetRöjning.Tables["Yta"].Columns["Yta"].AutoIncrementSeed = 2;
      progressBarData.Value = 0;
      labelProgress.Text = Resources.Klar;
      labelTotaltAntalYtor.Text = Resources.Totalt_antal_ytor__0;
      reportRöjning.RegisterData(dataSetRöjning, "dataSetRöjning");
      reportRöjning.RegisterData(dataSetCalculations, "dataSetCalculations");
    }

    private void dataGridViewRöjningsstallen_DataError(object sender, DataGridViewDataErrorEventArgs e)
    {
      if (!((e.Exception) is FormatException)) return;
      var view = (DataGridView) sender;
      MessageBox.Show(Resources.EndastPositiva, Resources.FelaktigtVarde, MessageBoxButtons.OK,
                      MessageBoxIcon.Stop);
      view.CurrentCell.Value = 0;
      e.ThrowException = false;
      e.Cancel = false;
    }

    private int GetNrOfRows(string aTable)
    {
      try
      {
        if (dataSetRöjning != null && dataSetRöjning.Tables[aTable] != null)
        {
          return dataSetRöjning.Tables[aTable].Rows.Count;
        }
      }
      catch (Exception)
      {
//ignore 
      }
      return 0;
    }

    private static int GetDatumÅr(object aDatum)
    {
      if (aDatum != null && !aDatum.ToString().Equals(string.Empty))
      {
        DateTime datum;
        DateTime.TryParse(aDatum.ToString(), out datum);

        if (!datum.Equals(DateTime.MinValue))
        {
          return datum.Year;
        }
      }
      return 0;
    }


    private void SendViaOutlook(bool aBifogaRapport)
    {
      if (!CheckMailSettings(false)) return;
      //First save the report
      WriteDataToReportXmlFile(Settings.Default.ReportFilePath, false, false, Settings.Default.ReportStatus,
                               Settings.Default.ReportStatusDatum);
      try
      {
        var oApp = new Microsoft.Office.Interop.Outlook.Application();
        var oMsg = (MailItem) oApp.CreateItem(OlItemType.olMailItem);
        var oRecip = oMsg.Recipients.Add(mEpostAdress);
        oRecip.Resolve();

        oMsg.Subject = mTitel;
        oMsg.Body = mMeddelande;

        if (aBifogaRapport)
        {
          //Add rapport.
          var sDisplayName = Settings.Default.MailAttachmentName;
          var iPosition = oMsg.Body.Length + 1;
          const int ATTACH_TYPE = (int) OlAttachmentType.olByValue;
          oMsg.Attachments.Add(Settings.Default.ReportFilePath, ATTACH_TYPE, iPosition, sDisplayName);
        }

        // Visa meddelandet.
        // oMsg.Display(true);  //modal

        oMsg.Save();
        // ReSharper disable RedundantCast
        ((_MailItem) oMsg).Send();
        // ReSharper restore RedundantCast
      }
      catch (Exception ex)
      {
#if !DEBUG
       MessageBox.Show("Sända rapporten via Outlook misslyckades. Kolla så epost inställningarna är korrekta.", "Sända misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    private bool CheckMailSettings(bool aInternMail)
    {
      var errMessage = string.Empty;

      if (Settings.Default.MailSendFrom.Equals(string.Empty))
      {
        errMessage += Resources.DinEPostadress + " ";
      }
      if (Settings.Default.MailFullName.Equals(string.Empty))
      {
        errMessage += Resources.DittNamn + " ";
      }
      if (mEpostAdress.Equals(string.Empty))
      {
        errMessage += Resources.MottagarensEPostadress + " ";
      }
      if (mTitel.Equals(string.Empty))
      {
        errMessage += Resources.Titel + " ";
      }
      if (aInternMail)
      {
        if (Settings.Default.MailSMTP.Equals(string.Empty))
        {
          errMessage += Resources.UtgaendeEPostSMTP + " ";
        }
      }

      if (!errMessage.Equals(string.Empty))
      {
        MessageBox.Show(
          String.Format(
            Resources.Foljande_epost_installningar_saknas,
            errMessage), Resources.Epost_installningar_saknas, MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }

      return (errMessage.Equals(string.Empty));
    }

    private void SendInternMail(bool aBifogaRapport)
    {
      if (!CheckMailSettings(true)) return;
      //First save the report
      WriteDataToReportXmlFile(Settings.Default.ReportFilePath, false, false, Settings.Default.ReportStatus,
                               Settings.Default.ReportStatusDatum);
      try
      {
        var smtpClient = new SmtpClient(Settings.Default.MailSMTP);
        var fromMail = new MailAddress(Settings.Default.MailSendFrom, Settings.Default.MailFullName,
                                       Encoding.UTF8);
        var toMail = new MailAddress(mEpostAdress);

        using (var message = new MailMessage(fromMail, toMail))
        {
          message.Body = mMeddelande;
          message.BodyEncoding = Encoding.UTF8;
          message.Subject = mTitel;
          message.SubjectEncoding = Encoding.UTF8;

          var smtpUserInfo = new NetworkCredential(Settings.Default.MailUsername,
                                                   Settings.Default.MailPassword);
          smtpClient.UseDefaultCredentials = false;
          smtpClient.Credentials = smtpUserInfo;
          smtpClient.Port = Settings.Default.MailSMTPPort;
          if (aBifogaRapport)
          {
            var attachment = new Attachment(Settings.Default.ReportFilePath);
            message.Attachments.Add(attachment);
          }
          smtpClient.Send(message);
          MessageBox.Show(Resources.RapportenArSand, Resources.RapportSandTitle, MessageBoxButtons.OK,
                          MessageBoxIcon.Information);
        }
      }
      catch (Exception ex)
      {
#if !DEBUG
       MessageBox.Show("Sända rapport via intern mail misslyckades. Kolla så epost inställningarna är korrekta.", "Sända misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    private void InitFastReportMail()
    {
      reportRöjning.EmailSettings.Recipients = new[] {mEpostAdress};
      reportRöjning.EmailSettings.Subject = mTitel;
      reportRöjning.EmailSettings.Message = mMeddelande;
      environmentRöjning.EmailSettings.Address = Settings.Default.MailSendFrom;
      environmentRöjning.EmailSettings.Name = Settings.Default.MailFullName;
      environmentRöjning.EmailSettings.Host = Settings.Default.MailSMTP;
      environmentRöjning.EmailSettings.Port = Settings.Default.MailSMTPPort;
      environmentRöjning.EmailSettings.UserName = Settings.Default.MailUsername;
      environmentRöjning.EmailSettings.Password = Settings.Default.MailPassword;
    }

    /// <summary>
    ///   Changes the language and restarts the application.
    /// </summary>
    /// <param name="aLanguage"> The language </param>
    private static void ChangeLanguage(string aLanguage)
    {
      if (Settings.Default.SettingsLanguage.Equals(aLanguage)) return;

      try
      {
        Settings.Default.SettingsLanguage = aLanguage;
        Application.Restart();
      }
      catch (NotSupportedException nse)
      {
#if !DEBUG
          MessageBox.Show("Applikationen gick inte att starta om, gör detta manuellt istället.");
#else
        MessageBox.Show(nse.ToString());
#endif
      }
    }

    /// <summary>
    ///   Gets the correct language from the language id string.
    /// </summary>
    /// <param name="aLangId"> Language id string </param>
    /// <returns> The language string </returns>
    private static string GetLanguage(string aLangId)
    {
      if (aLangId.Equals(Settings.Default.SprakSv))
      {
        return Resources.Svenska;
      }
      return aLangId.Equals(Settings.Default.SprakEn) ? Resources.Engelska : "Odefinerat språk";
    }


    private static string GetDsStr(string aTable, string aColumn, int aRowIndex, DataSet aDataSet)
    {
      try
      {
        if (aDataSet != null && aDataSet.Tables[aTable] != null)
        {
          if (aDataSet.Tables[aTable].Rows.Count > aRowIndex && aDataSet.Tables[aTable].Rows[aRowIndex][aColumn] != null)
          {
            return aDataSet.Tables[aTable].Rows[aRowIndex][aColumn].ToString();
          }
        }
      }
      catch (Exception)
      {
//ignore 
      }
      return string.Empty;
    }


    private void ProgressNumberOfDataDone()
    {
      var nrOfDataDone = 0;

      if (!GetDsStr("UserData", "Region").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("UserData", "Distrikt").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("UserData", "Ursprung").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("UserData", "Traktnr").Equals(string.Empty) &&
          !GetDsStr("UserData", "Traktnr").Equals(Resources.Noll))
        nrOfDataDone++;
      if (!GetDsStr("UserData", "Traktnamn").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("UserData", "Standort").Equals(string.Empty) &&
          !GetDsStr("UserData", "Standort").Equals(Resources.Noll))
        nrOfDataDone++;
      if (!GetDsStr("UserData", "Entreprenor").Equals(string.Empty) &&
          !GetDsStr("UserData", "Entreprenor").Equals(Resources.Noll))
        nrOfDataDone++;
      if (!GetDsStr("UserData", "Mal").Equals(string.Empty) && !GetDsStr("UserData", "Mal").Equals(Resources.Noll))
        nrOfDataDone++;

      //Natu-kulturmiljöhänsyn
      if (GetDsStr("Fragor", "Fraga1").Equals("Ja"))
      {
        progressBarData.Maximum = Settings.Default.MaxProgressRojning;
        nrOfDataDone++;
      }
      else if (GetDsStr("Fragor", "Fraga1").Equals("Nej"))
      {
        //Ifall traktdirekt är nej, ökas progress med 1, och en check mot kommentar görs.
        progressBarData.Maximum = Settings.Default.MaxProgressRojning + 1;
        nrOfDataDone++;

        if (!GetDsStr("UserData", "Kommentar").Equals(string.Empty))
          nrOfDataDone++;
      }

      if (!GetDsStr("Fragor", "Fraga2").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("Fragor", "Fraga3").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("Fragor", "Fraga4").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("Fragor", "Fraga5").Equals(string.Empty))
        nrOfDataDone++;
      if (!GetDsStr("Fragor", "Fraga6").Equals(string.Empty))
        nrOfDataDone++;

      //Check sums if anything has been entered in the table
      if (!textBoxSummaTallAntal.Text.Equals(Resources.Noll) || !textBoxSummaTallMedelhojd.Text.Equals(Resources.Noll)
          || !textBoxSummaContAntal.Text.Equals(Resources.Noll) ||
          !textBoxSummaContMedelhojd.Text.Equals(Resources.Noll)
          || !textBoxSummaGranAntal.Text.Equals(Resources.Noll) ||
          !textBoxSummaGranMedelhojd.Text.Equals(Resources.Noll)
          || !textBoxSummaBjorkAntal.Text.Equals(Resources.Noll) ||
          !textBoxSummaBjorkMedelhojd.Text.Equals(Resources.Noll)||
          !textBoxSummaRojstamAntal.Text.Equals(Resources.Noll))
        nrOfDataDone++;


      progressBarData.Value = nrOfDataDone;

      if (progressBarData.Value > 0 && progressBarData.Maximum > 0)
      {
        labelProgress.Text = String.Format("{0}% Klar",
                                           (int) ((progressBarData.Value/(float) progressBarData.Maximum)*100));
      }

      if (progressBarData.Value > 0 && progressBarData.Value == progressBarData.Maximum)
      {
        progressBarData.ForeColor = Color.Green;
      }
      else
      {
        progressBarData.ForeColor = SystemColors.Desktop;
      }

      labelTotaltAntalYtor.Text = String.Format("Totalt antal ytor: " + AntalYtor());

      if (AntalYtor() < 2 && DataSetParser.GetObjIntValue(textBoxSummaTallAntal.Text) == 0
          && DataSetParser.GetObjIntValue(textBoxSummaTallMedelhojd.Text) == 0 &&
          DataSetParser.GetObjIntValue(textBoxSummaContAntal.Text) == 0
          && DataSetParser.GetObjIntValue(textBoxSummaContMedelhojd.Text) == 0 &&
          DataSetParser.GetObjIntValue(textBoxSummaGranAntal.Text) == 0
          && DataSetParser.GetObjIntValue(textBoxSummaGranMedelhojd.Text) == 0 &&
          DataSetParser.GetObjIntValue(textBoxSummaBjorkAntal.Text) == 0
          && DataSetParser.GetObjIntValue(textBoxSummaBjorkMedelhojd.Text) == 0 &&
          DataSetParser.GetObjIntValue(textBoxSummaRojstamAntal.Text) == 0)
      {
        labelTotaltAntalYtor.Text = Resources.Totalt_antal_ytor__0;
      }
      else
      {
        labelTotaltAntalYtor.Text = String.Format("Totalt antal ytor: " + AntalYtor());
      }
    }

    /// <summary>
    ///   Checks the combobox or textbox if it is focused in that case update the progressbar.
    /// </summary>
    /// <param name="aSender"> The sending object </param>
    private void ProgressFocusedObject(ref object aSender)
    {
      try
      {
        var control = aSender as Control;

        if (control != null && control.Focused)
        {
          ProgressNumberOfDataDone();
        }
      }
      catch (Exception ex)
      {
#if DEBUG
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    private void UpdateDistriktList(ref object sender)
    {
      var comboBox = sender as ComboBox;
      if (comboBox == null) return;
      comboBoxDistrikt.Items.Clear();
      if (comboBox.SelectedIndex < 0) return;
      var activeRegionId = dataSetSettings.Tables["Region"].Rows[comboBox.SelectedIndex]["Id"].ToString();
      foreach (
        var row in
          dataSetSettings.Tables["Distrikt"].Rows.Cast<DataRow>().Where(
            row => row["RegionId"].ToString().Equals(activeRegionId)))
      {
        comboBoxDistrikt.Items.Add(row["Namn"].ToString());
      }
    }


    private static bool CheckaPositivtHeltal(Control aTextBox)
    {
      var rValue = false;
      if (aTextBox != null && !aTextBox.Text.Equals(string.Empty))
      {
        if (aTextBox.Focused && DataSetParser.GetObjIntValue(aTextBox.Text) < 0)
        {
          MessageBox.Show(Resources.Ogiltigt_nummer, Resources.Ogiltigt_nummer, MessageBoxButtons.OK,
                          MessageBoxIcon.Information);
          aTextBox.Text = string.Empty;
        }
        else
        {
          rValue = true;
        }
      }
      return rValue;
    }

    private void comboBoxRegion_SelectedIndexChanged(object sender, EventArgs e)
    {
      UpdateDistriktList(ref sender);
      ProgressFocusedObject(ref sender);
    }

    private void comboBoxDistrikt_SelectedIndexChanged(object sender, EventArgs e)
    {
      ProgressFocusedObject(ref sender);
    }

    private void textBoxTraktnr_TextChanged(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box == null) return;
      if (CheckaPositivtHeltal(box))
      {
        ProgressFocusedObject(ref sender);
      }
    }

    private void textBoxTraktnamn_TextChanged(object sender, EventArgs e)
    {
      ProgressFocusedObject(ref sender);
    }

    /*private void comboBoxStandort_SelectedIndexChanged(object sender, EventArgs e)
    {
      ProgressFocusedObject(ref sender);
    }*/

    private void textBoxEntreprenor_TextChanged(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box == null) return;
      if (!CheckaPositivtHeltal(box)) return;
      ProgressFocusedObject(ref sender);
    }

    private void comboBoxUrsprung_SelectedIndexChanged(object sender, EventArgs e)
    {
      ProgressFocusedObject(ref sender);
    }


    private void dataGridViewRöjningsstallen_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
    {
      try
      {
        if (dataGridViewRöjningsstallen == null || e.FormattedValue == null ||
            dataGridViewRöjningsstallen.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly) return;

        // Don't try to validate the 'new row' until finished 
        // editing since there
        // is not any point in validating its initial value.
        if (dataGridViewRöjningsstallen.Rows[e.RowIndex].IsNewRow)
        {
          return;
        }

        dataGridViewRöjningsstallen.Rows[e.RowIndex].ErrorText = "";
        dataGridViewRöjningsstallen.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "";

        var cname = dataGridViewRöjningsstallen.CurrentCell.OwningColumn.Name;
        var cellVärde = e.FormattedValue.ToString().Trim();

        if (cname.Equals(tallMedelhojdDataGridViewTextBoxColumn.Name) ||
            cname.Equals(contMedelhojdDataGridViewTextBoxColumn.Name)
            || cname.Equals(granMedelhojdDataGridViewTextBoxColumn.Name) ||
            cname.Equals(bjorkMedelhojdDataGridViewTextBoxColumn.Name))
        {
          if (!string.IsNullOrEmpty(cellVärde))
          {
            double cellDoubleValue;
            double.TryParse(cellVärde, out cellDoubleValue);
            if (cellDoubleValue < 0.0)
            {
              MessageBox.Show(Resources.EndastPositiva, Resources.FelaktigtVarde, MessageBoxButtons.OK,
                              MessageBoxIcon.Stop);
            }
          }
        }
        if (!cname.Equals(tallAntalDataGridViewTextBoxColumn.Name) &&
            !cname.Equals(contAntalDataGridViewTextBoxColumn.Name) &&
            !cname.Equals(granAntalDataGridViewTextBoxColumn.Name) &&
            !cname.Equals(bjorkAntalDataGridViewTextBoxColumn.Name) &&
            !cname.Equals(RojstamAntalDataGridViewTextBoxColumn.Name)) return;


        if (string.IsNullOrEmpty(cellVärde)) return;

        int cellIntValue;

        int.TryParse(cellVärde, out cellIntValue);
        if (cellIntValue < 0)
        {
          MessageBox.Show(Resources.EndastPositiva, Resources.FelaktigtVarde, MessageBoxButtons.OK,
                          MessageBoxIcon.Stop);
        }
      }
      catch (Exception ex)
      {
#if !DEBUG
       MessageBox.Show("Det gick inte att validera värdet på cellen.", "Fel vid cell validering", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    private void textBoxProvytestorlek_TextChanged(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box == null) return;
      if (!CheckaPositivtHeltal(box)) return;
      SetDsStr("UserData", "Provytestorlek", "100");
      UpdateRöjningValues();
      ProgressFocusedObject(ref sender);
    }

    private void dataGridViewRöjningsstallen_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
    {
      CalculateValues();
      ProgressNumberOfDataDone();
    }

    private int AntalYtor()
    {
      var antalYtor = 0;
      try
      {
        antalYtor +=
          dataGridViewRöjningsstallen.Rows.Cast<DataGridViewRow>().Count(
            row => DataSetParser.GetObjIntValue(row.Cells[ytaDataGridViewTextBoxColumn.Name].Value) >= 0);
      }
      catch (Exception ex)
      {
#if !DEBUG
       MessageBox.Show("Antal ytor misslyckades.", "Antal ytor misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
      return antalYtor;
    }

    private void ClearYtor()
    {
      try
      {
        dataSetRöjning.Tables["Yta"].Clear();
        if (dataSetRöjning.Tables["Yta"].Rows.Count == 0)
        {
          //If no rows exist then add a empty one before save.
            dataSetRöjning.Tables["Yta"].Rows.Add(1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
        }

        dataSetCalculations.Clear();
        if (dataSetCalculations.Tables["Medelvarde"].Rows.Count == 0)
        {
            dataSetCalculations.Tables["Medelvarde"].Rows.Add(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
        }

        if (dataSetCalculations.Tables["Summa"].Rows.Count == 0)
        {
            dataSetCalculations.Tables["Summa"].Rows.Add(0, 0.0, 0, 0.0, 0, 0.0, 0, 0.0, 0, 0.0, 0.0);
        }
      }
      catch (Exception ex)
      {
#if !DEBUG
       MessageBox.Show("Rensa ytor misslyckades.", "Fel vid nollställning", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    private void dataGridViewRöjningsstallen_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
    {
      if (AntalYtor() > 1) return;
      //Update the total sum textboxes, reset to zero
      ClearYtor();
      CalculateValues();
      ProgressNumberOfDataDone();
    }

    private void SetDsStr(string aTable, string aColumn, string aText)
    {
      if (dataSetRöjning == null || dataSetRöjning.Tables[aTable] == null) return;
      if (dataSetRöjning.Tables[aTable].Rows.Count <= 0 || dataSetRöjning.Tables[aTable].Rows[0][aColumn] == null ||
          dataSetRöjning.Tables[aTable].Rows[0][aColumn].ToString().Equals(aText)) return;
      try
      {
        dataSetRöjning.Tables[aTable].Rows[0][aColumn] = aText;
      }
      catch (Exception)
      {
//ignore            
      }
    }


    private void SetSelectedDistriktId()
    {
      int index = 0, settingsIndex = 0;
      foreach (DataRow row in dataSetSettings.Tables["Distrikt"].Rows)
      {
        if (row["RegionId"].ToString().Equals(GetDsStr("Region", "Id", comboBoxRegion.SelectedIndex, dataSetSettings)))
        {
          if (comboBoxDistrikt.SelectedIndex == index)
          {
            SetDsStr("UserData", "DistriktId", GetDsStr("Distrikt", "Id", settingsIndex, dataSetSettings));
            break;
          }
          index++;
        }
        settingsIndex++;
      }
    }


    private void dateTimePicker_ValueChanged(object sender, EventArgs e)
    {
      SetDsStr("UserData", "Datum", dateTimePicker.Text);
      var årtal = GetDatumÅr(dateTimePicker.Text);
      SetDsStr("UserData", "Artal", årtal.ToString(CultureInfo.InvariantCulture));
    }

    private void comboBoxDistrikt_SelectionChangeCommitted(object sender, EventArgs e)
    {
      SetDsStr("UserData", "Distrikt", comboBoxDistrikt.Text);
      SetSelectedDistriktId();
    }

    private void comboBoxRegion_SelectionChangeCommitted(object sender, EventArgs e)
    {
      SetDsStr("UserData", "Region", comboBoxRegion.Text);
      var regionid = GetDsStr("Region", "Id", comboBoxRegion.SelectedIndex, dataSetSettings);
      SetDsStr("UserData", "RegionId", regionid);
    }

    private void comboBoxUrsprung_SelectionChangeCommitted(object sender, EventArgs e)
    {
      SetDsStr("UserData", "Ursprung", comboBoxUrsprung.Text);
    }

    /*private void comboBoxStandort_SelectionChangeCommitted(object sender, EventArgs e)
    {
      SetDsStr("UserData", "Standort", comboBoxStandort.Text);
    }*/

    private void textBoxMål_TextChanged(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box == null) return;
      if (!CheckaPositivtHeltal(box)) return;
      SetDsStr("UserData", "Mal", textBoxMål.Text);
      UpdateRöjningValues();
      ProgressFocusedObject(ref sender);
    }


    private void textBoxTraktnr_KeyDown(object sender, KeyEventArgs e)
    {
        DataHelper.AccepteraEndastHeltalSomKeyDown(e, false);
    }

    private void textBoxEntreprenor_KeyDown(object sender, KeyEventArgs e)
    {
        DataHelper.AccepteraEndastHeltalSomKeyDown(e, false);
    }

    private void textBoxProvytestorlek_KeyDown(object sender, KeyEventArgs e)
    {
        DataHelper.AccepteraEndastHeltalSomKeyDown(e, false);
    }

    private void textBoxMål_KeyDown(object sender, KeyEventArgs e)
    {
        DataHelper.AccepteraEndastHeltalSomKeyDown(e, false);
    }

    private void textBoxTraktnr_Leave(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box != null && box.Text.Trim().Equals(string.Empty))
      {
        box.Text = Resources.Noll;
      }
      SetDsStr("UserData", "Traktnr", textBoxTraktnr.Text);
    }

    private void textBoxEntreprenor_Leave(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box != null && box.Text.Trim().Equals(string.Empty))
      {
        box.Text = Resources.Noll;
      }
    }

    private void textBoxProvytestorlek_Leave(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box != null && box.Text.Trim().Equals(string.Empty))
      {
        box.Text = Resources.Noll;
      }
    }

    private void textBoxMål_Leave(object sender, EventArgs e)
    {
      var box = sender as TextBox;
      if (box != null && box.Text.Trim().Equals(string.Empty))
      {
        box.Text = Resources.Noll;
      }
    }

    private void SättRButtonSvar(object aSender, string aFraga)
    {
      try
      {
        var rbutton = aSender as RadioButton;
        if (rbutton == null || !rbutton.Checked) return;

        SetDsStr("Fragor", aFraga, rbutton.Text);
        ProgressFocusedObject(ref aSender);
      }
      catch (Exception ex)
      {
#if !DEBUG
        MessageBox.Show("Misslyckades med att hämta svaret från radiobutton.");
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    private void StatusFrågor()
    {
      try
      {
        radioButton1Ja.Checked = GetDsStr("Fragor", "Fraga1").Equals("Ja");
        radioButton1Nej.Checked = GetDsStr("Fragor", "Fraga1").Equals("Nej");
        radioButton2Ja.Checked = GetDsStr("Fragor", "Fraga2").Equals("Ja");
        radioButton2Nej.Checked = GetDsStr("Fragor", "Fraga2").Equals("Nej");
        radioButton2Ejaktuellt.Checked = GetDsStr("Fragor", "Fraga2").Equals("Ej aktuellt");
        radioButton3Ja.Checked = GetDsStr("Fragor", "Fraga3").Equals("Ja");
        radioButton3Nej.Checked = GetDsStr("Fragor", "Fraga3").Equals("Nej");
        radioButton3Ejaktuellt.Checked = GetDsStr("Fragor", "Fraga3").Equals("Ej aktuellt");
        radioButton4Ja.Checked = GetDsStr("Fragor", "Fraga4").Equals("Ja");
        radioButton4Nej.Checked = GetDsStr("Fragor", "Fraga4").Equals("Nej");
        radioButton4Ejaktuellt.Checked = GetDsStr("Fragor", "Fraga4").Equals("Ej aktuellt");
        radioButton5Ja.Checked = GetDsStr("Fragor", "Fraga5").Equals("Ja");
        radioButton5Nej.Checked = GetDsStr("Fragor", "Fraga5").Equals("Nej");
        radioButton5Ejaktuellt.Checked = GetDsStr("Fragor", "Fraga5").Equals("Ej aktuellt");
        radioButton6Ja.Checked = GetDsStr("Fragor", "Fraga6").Equals("Ja");
        radioButton6Nej.Checked = GetDsStr("Fragor", "Fraga6").Equals("Nej");
        radioButton6Ejaktuellt.Checked = GetDsStr("Fragor", "Fraga6").Equals("Ej aktuellt");
      }
      catch (Exception ex)
      {
#if !DEBUG
        MessageBox.Show("Misslyckades med att uppdatera frågornas status.", "Status error", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }


    private void radioButton1Ja_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga1");
    }

    private void radioButton1Nej_CheckedChanged(object sender, EventArgs e)
    {
      var button = sender as RadioButton;
      if (button == null) return;
      SättRButtonSvar(sender, "Fraga1");
      if (!button.Checked || !button.Focused) return;
      MessageBox.Show(
        Resources.Nej_har_valts_for_traktdirektiv,
        Resources.En_kommentar_kravs, MessageBoxButtons.OK, MessageBoxIcon.Information);
      ProgressNumberOfDataDone();
    }

    private void radioButton2Ja_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga2");
    }

    private void radioButton2Nej_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga2");
    }

    private void radioButton2Ejaktuellt_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga2");
    }

    private void radioButton3Ja_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga3");
    }

    private void radioButton3Nej_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga3");
    }

    private void radioButton3Ejaktuellt_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga3");
    }

    private void radioButton4Ja_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga4");
    }

    private void radioButton4Nej_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga4");
    }

    private void radioButton4Ejaktuellt_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga4");
    }

    private void radioButton5Ja_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga5");
    }

    private void radioButton5Nej_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga5");
    }

    private void radioButton5Ejaktuellt_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga5");
    }

    private void radioButton6Ja_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga6");
    }

    private void radioButton6Nej_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga6");
    }

    private void radioButton6Ejaktuellt_CheckedChanged(object sender, EventArgs e)
    {
      SättRButtonSvar(sender, "Fraga6");
    }

    private void dataGridViewRöjningsstallen_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
    {
      if (e.RowIndex > 0)
      {
        LäggTillHögstaVärdetFörYta(e.RowIndex - 1);
      }
    }

    private void richTextBoxKommentar_TextChanged(object sender, EventArgs e)
    {
      var box = sender as RichTextBox;
      if (box == null) return;
      ProgressNumberOfDataDone();
    }

    private void dataGridViewRöjningsstallen_CellEndEdit(object sender, DataGridViewCellEventArgs e)
    {
      try
      {
        if (dataGridViewRöjningsstallen.CurrentCell.Value == null)
        {
          dataGridViewRöjningsstallen.Refresh();
          dataGridViewRöjningsstallen.RefreshEdit();
          dataGridViewRöjningsstallen.CommitEdit(DataGridViewDataErrorContexts.Commit);
          dataGridViewRöjningsstallen.Update();

          return;
        }

        var cname = dataGridViewRöjningsstallen.CurrentCell.OwningColumn.Name;
        var cellVärde = dataGridViewRöjningsstallen.CurrentCell.Value.ToString().Trim();
        if (string.IsNullOrEmpty(cellVärde)) return;

        if (cname.Equals(tallMedelhojdDataGridViewTextBoxColumn.Name) ||
            cname.Equals(contMedelhojdDataGridViewTextBoxColumn.Name)
            || cname.Equals(granMedelhojdDataGridViewTextBoxColumn.Name) ||
            cname.Equals(bjorkMedelhojdDataGridViewTextBoxColumn.Name))
        {
          double cellDoubleValue;
          if ((!double.TryParse(cellVärde, out cellDoubleValue) ||
               cellDoubleValue < 0.0))
          {
            dataGridViewRöjningsstallen.CurrentCell.Value = 0;
          }
        }
        else if (cname.Equals(tallAntalDataGridViewTextBoxColumn.Name) ||
                 cname.Equals(contAntalDataGridViewTextBoxColumn.Name)
                 || cname.Equals(granAntalDataGridViewTextBoxColumn.Name) ||
                 cname.Equals(bjorkAntalDataGridViewTextBoxColumn.Name) ||
                cname.Equals(RojstamAntalDataGridViewTextBoxColumn.Name))
        {
          int cellIntValue;
          if ((!int.TryParse(cellVärde, out cellIntValue) ||
               cellIntValue < 0))
          {
            dataGridViewRöjningsstallen.CurrentCell.Value = 0;
          }
        }
        //    UpdateHstAndMHojd(dataGridViewRöjningsstallen.CurrentRow);
        CalculateValues();
      }
      catch (Exception ex)
      {
#if !DEBUG
       MessageBox.Show("Det gick inte att kontrollera värdet på cellen.", "Fel vid cell editering", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
        MessageBox.Show(ex.ToString());
#endif
      }
    }

    private void dataGridViewRöjningsstallen_CellValidated(object sender, DataGridViewCellEventArgs e)
    {
      UpdateHstAndMHojd(dataGridViewRöjningsstallen.CurrentRow);
    }

    private void textBoxTraktDel_Leave(object sender, EventArgs e)
    {
        var box = sender as TextBox;
        if (box != null && box.Text.Trim().Equals(string.Empty))
        {
            box.Text = Resources.Noll;
        }
        DataSetParser.SetDsStr("UserData", "Standort", textBoxTraktnr.Text, dataSetRöjning);      
    }

    private void textBoxTraktDel_KeyDown(object sender, KeyEventArgs e)
    {
        DataHelper.AccepteraEndastHeltalSomKeyDown(e, false);
    }

    private void textBoxTraktDel_TextChanged(object sender, EventArgs e)
    {
        var box = sender as TextBox;
        if (box == null) return;
        if (!CheckaPositivtHeltal(box)) return;
        ProgressFocusedObject(ref sender);
    }

    private void comboBoxInvTyp_SelectedIndexChanged(object sender, EventArgs e)
    {
        ProgressFocusedObject(ref sender);
    }

    private void comboBoxInvTyp_SelectionChangeCommitted(object sender, EventArgs e)
    {
        DataSetParser.SetDsStr("UserData", "InvTyp", comboBoxInvTyp.Text, dataSetRöjning);
        DataSetParser.SetDsStr("UserData", "InvTypId", GetDsStr("InvTyp", "Id", comboBoxInvTyp.SelectedIndex, dataSetSettings), dataSetRöjning);
    }

 
    private void UpdateDecimal(object sender)
    {
        var box = sender as TextBox;

        if (box == null) return;
        var d = (double)Math.Round(DataSetParser.GetObjDecimalValue(box.Text), 1);
        if (d < 0) return;
        DataSetParser.SetDsDouble("UserData", "AtgAreal", d, dataSetRöjning);
        box.Text = d.ToString(CultureInfo.InvariantCulture);
    }


    private static bool CheckaPositivtDecimaltal(Control aTextBox)
    {
        var rValue = false;
        if (aTextBox != null && !aTextBox.Text.Equals(string.Empty))
        {
            if (aTextBox.Focused && DataSetParser.GetObjFloatValue(aTextBox.Text) < 0)
            {
                MessageBox.Show(Resources.Ogiltigt_nummer, Resources.Ogiltigt_nummer, MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
                aTextBox.Text = string.Empty;
            }
            else
            {
                rValue = true;
            }
        }
        return rValue;
    }

    private void UpdateAtgAreal()
    {
        var tmp = 0;
        try
        {
            if (!string.IsNullOrEmpty(textBoxAtgAreal.Text))
            {
                if (textBoxAtgAreal.Text.Contains('.'))
                {
                    tmp = textBoxAtgAreal.SelectionStart;
                    textBoxAtgAreal.Text = textBoxAtgAreal.Text.Replace('.', ',');
                    textBoxAtgAreal.SelectionStart = tmp;                    
                }
            }
        }
        catch (FormatException ex)
        {
            //No numerical float value  
#if DEBUG
            MessageBox.Show(ex.ToString());
#endif
            textBoxAtgAreal.Text = Resources.Noll;
        }
    }
    private void textBoxAtgAreal_TextChanged(object sender, EventArgs e)
    {
        var box = sender as TextBox;
        if (box == null) return;
        if (!CheckaPositivtDecimaltal(box)) return;
        //DataSetParser.SetDsStr("UserData", "AtgAreal", box.Text, dataSetRöjning);
        SetDsStr("UserData", "AtgAreal", box.Text);
        UpdateAtgAreal();
        ProgressFocusedObject(ref sender);
    }
    private void textBoxAtgAreal_Validated(object sender, EventArgs e)
    {
        UpdateDecimal(sender);
    }
    private void textBoxAtgAreal_KeyDown(object sender, KeyEventArgs e)
    {
        DataHelper.AccepteraEndastHeltalSomKeyDown(e,true);
    }
    private void textBoxAtgAreal_Leave(object sender, EventArgs e)
    {
        var box = sender as TextBox;
        if (box != null && box.Text.Trim().Equals(string.Empty))
        {
            box.Text = Resources.Noll;
        }
    }

  }
}