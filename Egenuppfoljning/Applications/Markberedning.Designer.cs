﻿using System;
using System.Data;

namespace Egenuppfoljning.Applications
{
  partial class MarkberedningForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MarkberedningForm));
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
        FastReport.Design.DesignerSettings designerSettings3 = new FastReport.Design.DesignerSettings();
        FastReport.Design.DesignerRestrictions designerRestrictions3 = new FastReport.Design.DesignerRestrictions();
        FastReport.Export.Email.EmailSettings emailSettings3 = new FastReport.Export.Email.EmailSettings();
        FastReport.PreviewSettings previewSettings3 = new FastReport.PreviewSettings();
        FastReport.ReportSettings reportSettings3 = new FastReport.ReportSettings();
        this.dataSetMarkberedning = new System.Data.DataSet();
        this.dataTableYta = new System.Data.DataTable();
        this.dataColumnYta = new System.Data.DataColumn();
        this.dataColumnOptimalt = new System.Data.DataColumn();
        this.dataColumnBra = new System.Data.DataColumn();
        this.dataColumnBlekjordsflack = new System.Data.DataColumn();
        this.dataColumnOptimaltBra = new System.Data.DataColumn();
        this.dataTableUserData = new System.Data.DataTable();
        this.dataColumnRegionId = new System.Data.DataColumn();
        this.dataColumnRegion = new System.Data.DataColumn();
        this.dataColumnDistriktId = new System.Data.DataColumn();
        this.dataColumnDistrikt = new System.Data.DataColumn();
        this.dataColumnUrsprung = new System.Data.DataColumn();
        this.dataColumnDatum = new System.Data.DataColumn();
        this.dataColumnTraktnr = new System.Data.DataColumn();
        this.dataColumnTraktnamn = new System.Data.DataColumn();
        this.dataColumnStandort = new System.Data.DataColumn();
        this.dataColumnEntreprenor = new System.Data.DataColumn();
        this.dataColumnMarkberedningsmetod = new System.Data.DataColumn();
        this.dataColumnGISStracka = new System.Data.DataColumn();
        this.dataColumnGISAreal = new System.Data.DataColumn();
        this.dataColumnKommentar = new System.Data.DataColumn();
        this.dataColumnLanguage = new System.Data.DataColumn();
        this.dataColumnMailFrom = new System.Data.DataColumn();
        this.dataColumnStatus = new System.Data.DataColumn();
        this.dataColumnStatus_Datum = new System.Data.DataColumn();
        this.dataColumnArtal = new System.Data.DataColumn();
        this.dataColumnMal = new System.Data.DataColumn();
        this.dataColumnDatainsamlingsmetod = new System.Data.DataColumn();
        this.dataColumnInvTypId = new System.Data.DataColumn();
        this.dataColumnInvTyp = new System.Data.DataColumn();
        this.dataTableFragor = new System.Data.DataTable();
        this.dataColumn1 = new System.Data.DataColumn();
        this.dataColumn2 = new System.Data.DataColumn();
        this.dataColumn3 = new System.Data.DataColumn();
        this.dataColumn4 = new System.Data.DataColumn();
        this.dataColumn5 = new System.Data.DataColumn();
        this.dataColumn6 = new System.Data.DataColumn();
        this.dataColumn7 = new System.Data.DataColumn();
        this.dataColumn9 = new System.Data.DataColumn();
        this.dataColumn8 = new System.Data.DataColumn();
        this.dataColumn10 = new System.Data.DataColumn();
        this.dataColumn11 = new System.Data.DataColumn();
        this.dataColumn12 = new System.Data.DataColumn();
        this.dataSetSettings = new System.Data.DataSet();
        this.dataTableRegion = new System.Data.DataTable();
        this.dataColumnSettingsRegionId = new System.Data.DataColumn();
        this.dataColumnSettingsRegionNamn = new System.Data.DataColumn();
        this.dataTableDistrikt = new System.Data.DataTable();
        this.dataColumnSettingsDistriktRegionId = new System.Data.DataColumn();
        this.dataColumnSettingsDistriktId = new System.Data.DataColumn();
        this.dataColumnSettingsDistriktNamn = new System.Data.DataColumn();
        this.dataTableMarkberedningsmetod = new System.Data.DataTable();
        this.dataColumnSettingsMarkberedningsmetodNamn = new System.Data.DataColumn();
        this.dataTableUrsprung = new System.Data.DataTable();
        this.dataColumnSettingsUrsprungNamn = new System.Data.DataColumn();
        this.dataTableInvTyp = new System.Data.DataTable();
        this.dataColumnSettingsInvTypId = new System.Data.DataColumn();
        this.dataColumnSettingsInvTypNamn = new System.Data.DataColumn();
        this.menuStripMarkberedning = new System.Windows.Forms.MenuStrip();
        this.arkivToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.sparaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.sparaSomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
        this.avslutaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.reportMarkberedning = new FastReport.Report();
        this.openReportFileDialog = new System.Windows.Forms.OpenFileDialog();
        this.tabControl1 = new System.Windows.Forms.TabControl();
        this.tabPageMarkberedning = new System.Windows.Forms.TabPage();
        this.groupBoxFörSådd = new System.Windows.Forms.GroupBox();
        this.groupBox12 = new System.Windows.Forms.GroupBox();
        this.textBoxContortaStambrev = new System.Windows.Forms.TextBox();
        this.textBoxTallStambrev = new System.Windows.Forms.TextBox();
        this.label16 = new System.Windows.Forms.Label();
        this.groupBox11 = new System.Windows.Forms.GroupBox();
        this.textBoxContortaTotGiva = new System.Windows.Forms.TextBox();
        this.textBoxTallTotGiva = new System.Windows.Forms.TextBox();
        this.label15 = new System.Windows.Forms.Label();
        this.groupBoxGIS = new System.Windows.Forms.GroupBox();
        this.textBoxGISData = new System.Windows.Forms.TextBox();
        this.labelGISData = new System.Windows.Forms.Label();
        this.textBoxGISAreal = new System.Windows.Forms.TextBox();
        this.labelGISAreal = new System.Windows.Forms.Label();
        this.textBoxGISStracka = new System.Windows.Forms.TextBox();
        this.labelGISStracka = new System.Windows.Forms.Label();
        this.groupBoxPlantering = new System.Windows.Forms.GroupBox();
        this.label11 = new System.Windows.Forms.Label();
        this.textBoxMåluppfyllelse = new System.Windows.Forms.TextBox();
        this.textBoxPlanteringAntalOptimalaBra = new System.Windows.Forms.TextBox();
        this.label10 = new System.Windows.Forms.Label();
        this.labelPlanteringAntalOptimalaBra = new System.Windows.Forms.Label();
        this.textBoxMål = new System.Windows.Forms.TextBox();
        this.label12 = new System.Windows.Forms.Label();
        this.textBoxPlanteringAntalOptimala = new System.Windows.Forms.TextBox();
        this.labelPlanteringAntalOptimala = new System.Windows.Forms.Label();
        this.groupBox2 = new System.Windows.Forms.GroupBox();
        this.label13 = new System.Windows.Forms.Label();
        this.comboBoxDatainsamlingsmetod = new System.Windows.Forms.ComboBox();
        this.label9 = new System.Windows.Forms.Label();
        this.comboBoxMarkberedningsmetod = new System.Windows.Forms.ComboBox();
        this.groupBoxTrakt = new System.Windows.Forms.GroupBox();
        this.textBoxTraktDel = new System.Windows.Forms.TextBox();
        this.textBoxTraktnr = new System.Windows.Forms.TextBox();
        this.textBoxTraktnamn = new System.Windows.Forms.TextBox();
        this.textBoxEntreprenor = new System.Windows.Forms.TextBox();
        this.labeEntreprenor = new System.Windows.Forms.Label();
        this.labelStandort = new System.Windows.Forms.Label();
        this.labelTraktnamn = new System.Windows.Forms.Label();
        this.labelTraktnr = new System.Windows.Forms.Label();
        this.groupBoxRegion = new System.Windows.Forms.GroupBox();
        this.comboBoxInvtyp = new System.Windows.Forms.ComboBox();
        this.labelInvtyp = new System.Windows.Forms.Label();
        this.comboBoxUrsprung = new System.Windows.Forms.ComboBox();
        this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
        this.comboBoxDistrikt = new System.Windows.Forms.ComboBox();
        this.comboBoxRegion = new System.Windows.Forms.ComboBox();
        this.labelDatum = new System.Windows.Forms.Label();
        this.labelUrsprung = new System.Windows.Forms.Label();
        this.labelDistrikt = new System.Windows.Forms.Label();
        this.labelRegion = new System.Windows.Forms.Label();
        this.tabPageYtor = new System.Windows.Forms.TabPage();
        this.groupBoxMarkberedningsstallen = new System.Windows.Forms.GroupBox();
        this.groupBoxBra = new System.Windows.Forms.GroupBox();
        this.groupBoxBlekjordsflack = new System.Windows.Forms.GroupBox();
        this.labelBlekjordsflack = new System.Windows.Forms.Label();
        this.labelBra = new System.Windows.Forms.Label();
        this.dataGridViewMarkberedningsstallen = new System.Windows.Forms.DataGridView();
        this.ytaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.optimaltDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.braDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.blekjordsflackDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.optimaltBraDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.groupBoxMedelvarde = new System.Windows.Forms.GroupBox();
        this.textBoxMedelvardeOptimaltBra = new System.Windows.Forms.TextBox();
        this.labelMedelvardeOptimaltBra = new System.Windows.Forms.Label();
        this.textBoxMedelvardeBlekjordsflack = new System.Windows.Forms.TextBox();
        this.labelMedelvardeBlekjordsflack = new System.Windows.Forms.Label();
        this.textBoxMedelvardeBra = new System.Windows.Forms.TextBox();
        this.labelMedelvardeBra = new System.Windows.Forms.Label();
        this.textBoxMedelvardeOptimalt = new System.Windows.Forms.TextBox();
        this.labelMedelvardeOptimalt = new System.Windows.Forms.Label();
        this.groupBoxSumma = new System.Windows.Forms.GroupBox();
        this.textBoxSummaOptimalt = new System.Windows.Forms.TextBox();
        this.textBoxSummaOptimaltBra = new System.Windows.Forms.TextBox();
        this.labelSummaOptimaltBra = new System.Windows.Forms.Label();
        this.textBoxSummaBlekjordsflack = new System.Windows.Forms.TextBox();
        this.labelSummaBlekjordsflack = new System.Windows.Forms.Label();
        this.textBoxSummaBra = new System.Windows.Forms.TextBox();
        this.labelSummaBra = new System.Windows.Forms.Label();
        this.labelSummaOptimalt = new System.Windows.Forms.Label();
        this.tabPageFrågor = new System.Windows.Forms.TabPage();
        this.groupBox1 = new System.Windows.Forms.GroupBox();
        this.richTextBoxKommentar = new System.Windows.Forms.RichTextBox();
        this.label8 = new System.Windows.Forms.Label();
        this.groupBox9 = new System.Windows.Forms.GroupBox();
        this.radioButton7Nej = new System.Windows.Forms.RadioButton();
        this.radioButton7Ja = new System.Windows.Forms.RadioButton();
        this.label7 = new System.Windows.Forms.Label();
        this.groupBox8 = new System.Windows.Forms.GroupBox();
        this.radioButton6Nej = new System.Windows.Forms.RadioButton();
        this.radioButton6Ja = new System.Windows.Forms.RadioButton();
        this.label6 = new System.Windows.Forms.Label();
        this.groupBox7 = new System.Windows.Forms.GroupBox();
        this.radioButton5Nej = new System.Windows.Forms.RadioButton();
        this.radioButton5Ja = new System.Windows.Forms.RadioButton();
        this.label5 = new System.Windows.Forms.Label();
        this.groupBox6 = new System.Windows.Forms.GroupBox();
        this.radioButton4Nej = new System.Windows.Forms.RadioButton();
        this.radioButton4Ja = new System.Windows.Forms.RadioButton();
        this.label4 = new System.Windows.Forms.Label();
        this.groupBox5 = new System.Windows.Forms.GroupBox();
        this.radioButton3Nej = new System.Windows.Forms.RadioButton();
        this.radioButton3Ja = new System.Windows.Forms.RadioButton();
        this.label3 = new System.Windows.Forms.Label();
        this.groupBox3 = new System.Windows.Forms.GroupBox();
        this.radioButton1Nej = new System.Windows.Forms.RadioButton();
        this.radioButton1Ja = new System.Windows.Forms.RadioButton();
        this.label1 = new System.Windows.Forms.Label();
        this.groupBox4 = new System.Windows.Forms.GroupBox();
        this.radioButton2Nej = new System.Windows.Forms.RadioButton();
        this.radioButton2Ja = new System.Windows.Forms.RadioButton();
        this.label2 = new System.Windows.Forms.Label();
        this.environmentMarkberedning = new FastReport.EnvironmentSettings();
        this.saveReportFolderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
        this.labelTotaltAntalYtor = new System.Windows.Forms.Label();
        this.labelProgress = new System.Windows.Forms.Label();
        this.progressBarData = new System.Windows.Forms.ProgressBar();
        this.textBoxRegion = new System.Windows.Forms.TextBox();
        this.textBoxDistrikt = new System.Windows.Forms.TextBox();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetMarkberedning)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableYta)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableUserData)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFragor)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetSettings)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableRegion)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableDistrikt)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableMarkberedningsmetod)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableUrsprung)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableInvTyp)).BeginInit();
        this.menuStripMarkberedning.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.reportMarkberedning)).BeginInit();
        this.tabControl1.SuspendLayout();
        this.tabPageMarkberedning.SuspendLayout();
        this.groupBoxFörSådd.SuspendLayout();
        this.groupBox12.SuspendLayout();
        this.groupBox11.SuspendLayout();
        this.groupBoxGIS.SuspendLayout();
        this.groupBoxPlantering.SuspendLayout();
        this.groupBox2.SuspendLayout();
        this.groupBoxTrakt.SuspendLayout();
        this.groupBoxRegion.SuspendLayout();
        this.tabPageYtor.SuspendLayout();
        this.groupBoxMarkberedningsstallen.SuspendLayout();
        this.groupBoxBra.SuspendLayout();
        this.groupBoxBlekjordsflack.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMarkberedningsstallen)).BeginInit();
        this.groupBoxMedelvarde.SuspendLayout();
        this.groupBoxSumma.SuspendLayout();
        this.tabPageFrågor.SuspendLayout();
        this.groupBox1.SuspendLayout();
        this.groupBox9.SuspendLayout();
        this.groupBox8.SuspendLayout();
        this.groupBox7.SuspendLayout();
        this.groupBox6.SuspendLayout();
        this.groupBox5.SuspendLayout();
        this.groupBox3.SuspendLayout();
        this.groupBox4.SuspendLayout();
        this.SuspendLayout();
        // 
        // dataSetMarkberedning
        // 
        this.dataSetMarkberedning.DataSetName = "DataSetMarkberedning";
        this.dataSetMarkberedning.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableYta,
            this.dataTableUserData,
            this.dataTableFragor});
        // 
        // dataTableYta
        // 
        this.dataTableYta.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnYta,
            this.dataColumnOptimalt,
            this.dataColumnBra,
            this.dataColumnBlekjordsflack,
            this.dataColumnOptimaltBra});
        this.dataTableYta.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Yta"}, false)});
        this.dataTableYta.TableName = "Yta";
        // 
        // dataColumnYta
        // 
        this.dataColumnYta.AutoIncrementSeed = ((long)(2));
        this.dataColumnYta.ColumnName = "Yta";
        this.dataColumnYta.DataType = typeof(int);
        this.dataColumnYta.ReadOnly = true;
        // 
        // dataColumnOptimalt
        // 
        this.dataColumnOptimalt.ColumnName = "Optimalt";
        this.dataColumnOptimalt.DataType = typeof(int);
        // 
        // dataColumnBra
        // 
        this.dataColumnBra.ColumnName = "Bra";
        this.dataColumnBra.DataType = typeof(int);
        // 
        // dataColumnBlekjordsflack
        // 
        this.dataColumnBlekjordsflack.ColumnName = "Blekjordsflack";
        this.dataColumnBlekjordsflack.DataType = typeof(int);
        // 
        // dataColumnOptimaltBra
        // 
        this.dataColumnOptimaltBra.ColumnName = "OptimaltBra";
        this.dataColumnOptimaltBra.DataType = typeof(int);
        // 
        // dataTableUserData
        // 
        this.dataTableUserData.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnRegionId,
            this.dataColumnRegion,
            this.dataColumnDistriktId,
            this.dataColumnDistrikt,
            this.dataColumnUrsprung,
            this.dataColumnDatum,
            this.dataColumnTraktnr,
            this.dataColumnTraktnamn,
            this.dataColumnStandort,
            this.dataColumnEntreprenor,
            this.dataColumnMarkberedningsmetod,
            this.dataColumnGISStracka,
            this.dataColumnGISAreal,
            this.dataColumnKommentar,
            this.dataColumnLanguage,
            this.dataColumnMailFrom,
            this.dataColumnStatus,
            this.dataColumnStatus_Datum,
            this.dataColumnArtal,
            this.dataColumnMal,
            this.dataColumnDatainsamlingsmetod,
            this.dataColumnInvTypId,
            this.dataColumnInvTyp});
        this.dataTableUserData.TableName = "UserData";
        // 
        // dataColumnRegionId
        // 
        this.dataColumnRegionId.ColumnName = "RegionId";
        this.dataColumnRegionId.DataType = typeof(int);
        // 
        // dataColumnRegion
        // 
        this.dataColumnRegion.ColumnName = "Region";
        // 
        // dataColumnDistriktId
        // 
        this.dataColumnDistriktId.ColumnName = "DistriktId";
        this.dataColumnDistriktId.DataType = typeof(int);
        // 
        // dataColumnDistrikt
        // 
        this.dataColumnDistrikt.ColumnName = "Distrikt";
        // 
        // dataColumnUrsprung
        // 
        this.dataColumnUrsprung.ColumnName = "Ursprung";
        // 
        // dataColumnDatum
        // 
        this.dataColumnDatum.ColumnName = "Datum";
        this.dataColumnDatum.DataType = typeof(System.DateTime);
        // 
        // dataColumnTraktnr
        // 
        this.dataColumnTraktnr.ColumnName = "Traktnr";
        this.dataColumnTraktnr.DataType = typeof(int);
        // 
        // dataColumnTraktnamn
        // 
        this.dataColumnTraktnamn.ColumnName = "Traktnamn";
        // 
        // dataColumnStandort
        // 
        this.dataColumnStandort.ColumnName = "Standort";
        this.dataColumnStandort.DataType = typeof(int);
        // 
        // dataColumnEntreprenor
        // 
        this.dataColumnEntreprenor.ColumnName = "Entreprenor";
        this.dataColumnEntreprenor.DataType = typeof(int);
        // 
        // dataColumnMarkberedningsmetod
        // 
        this.dataColumnMarkberedningsmetod.ColumnName = "Markberedningsmetod";
        // 
        // dataColumnGISStracka
        // 
        this.dataColumnGISStracka.ColumnName = "GISStracka";
        this.dataColumnGISStracka.DataType = typeof(int);
        // 
        // dataColumnGISAreal
        // 
        this.dataColumnGISAreal.ColumnName = "GISAreal";
        this.dataColumnGISAreal.DataType = typeof(double);
        // 
        // dataColumnKommentar
        // 
        this.dataColumnKommentar.ColumnName = "Kommentar";
        // 
        // dataColumnLanguage
        // 
        this.dataColumnLanguage.ColumnName = "Language";
        // 
        // dataColumnMailFrom
        // 
        this.dataColumnMailFrom.ColumnName = "MailFrom";
        // 
        // dataColumnStatus
        // 
        this.dataColumnStatus.ColumnName = "Status";
        // 
        // dataColumnStatus_Datum
        // 
        this.dataColumnStatus_Datum.ColumnName = "Status_Datum";
        this.dataColumnStatus_Datum.DataType = typeof(System.DateTime);
        // 
        // dataColumnArtal
        // 
        this.dataColumnArtal.ColumnName = "Artal";
        this.dataColumnArtal.DataType = typeof(int);
        // 
        // dataColumnMal
        // 
        this.dataColumnMal.ColumnName = "Mal";
        this.dataColumnMal.DataType = typeof(int);
        // 
        // dataColumnDatainsamlingsmetod
        // 
        this.dataColumnDatainsamlingsmetod.ColumnName = "Datainsamlingsmetod";
        // 
        // dataColumnInvTypId
        // 
        this.dataColumnInvTypId.ColumnName = "InvTypId";
        this.dataColumnInvTypId.DataType = typeof(int);
        // 
        // dataColumnInvTyp
        // 
        this.dataColumnInvTyp.ColumnName = "InvTyp";
        // 
        // dataTableFragor
        // 
        this.dataTableFragor.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn7,
            this.dataColumn9,
            this.dataColumn8,
            this.dataColumn10,
            this.dataColumn11,
            this.dataColumn12});
        this.dataTableFragor.TableName = "Fragor";
        // 
        // dataColumn1
        // 
        this.dataColumn1.Caption = "Fraga1";
        this.dataColumn1.ColumnName = "Fraga1";
        // 
        // dataColumn2
        // 
        this.dataColumn2.ColumnName = "Fraga2";
        // 
        // dataColumn3
        // 
        this.dataColumn3.ColumnName = "Fraga3";
        // 
        // dataColumn4
        // 
        this.dataColumn4.ColumnName = "Fraga4";
        // 
        // dataColumn5
        // 
        this.dataColumn5.ColumnName = "Fraga5";
        // 
        // dataColumn6
        // 
        this.dataColumn6.ColumnName = "Fraga6";
        // 
        // dataColumn7
        // 
        this.dataColumn7.ColumnName = "Fraga7";
        // 
        // dataColumn9
        // 
        this.dataColumn9.ColumnName = "Ovrigt";
        // 
        // dataColumn8
        // 
        this.dataColumn8.ColumnName = "Fraga8";
        // 
        // dataColumn10
        // 
        this.dataColumn10.ColumnName = "Fraga9";
        // 
        // dataColumn11
        // 
        this.dataColumn11.ColumnName = "Fraga10";
        // 
        // dataColumn12
        // 
        this.dataColumn12.ColumnName = "Fraga11";
        // 
        // dataSetSettings
        // 
        this.dataSetSettings.DataSetName = "DataSetSettings";
        this.dataSetSettings.Relations.AddRange(new System.Data.DataRelation[] {
            new System.Data.DataRelation("RelationRegionDistrikt", "Region", "Distrikt", new string[] {
                        "Id"}, new string[] {
                        "RegionId"}, false)});
        this.dataSetSettings.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableRegion,
            this.dataTableDistrikt,
            this.dataTableMarkberedningsmetod,
            this.dataTableUrsprung,
            this.dataTableInvTyp});
        // 
        // dataTableRegion
        // 
        this.dataTableRegion.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSettingsRegionId,
            this.dataColumnSettingsRegionNamn});
        this.dataTableRegion.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint2", new string[] {
                        "Id"}, false)});
        this.dataTableRegion.TableName = "Region";
        // 
        // dataColumnSettingsRegionId
        // 
        this.dataColumnSettingsRegionId.ColumnName = "Id";
        this.dataColumnSettingsRegionId.DataType = typeof(int);
        // 
        // dataColumnSettingsRegionNamn
        // 
        this.dataColumnSettingsRegionNamn.ColumnName = "Namn";
        // 
        // dataTableDistrikt
        // 
        this.dataTableDistrikt.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSettingsDistriktRegionId,
            this.dataColumnSettingsDistriktId,
            this.dataColumnSettingsDistriktNamn});
        this.dataTableDistrikt.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.ForeignKeyConstraint("RelationRegionDistrikt", "Region", new string[] {
                        "Id"}, new string[] {
                        "RegionId"}, System.Data.AcceptRejectRule.None, System.Data.Rule.Cascade, System.Data.Rule.Cascade)});
        this.dataTableDistrikt.TableName = "Distrikt";
        // 
        // dataColumnSettingsDistriktRegionId
        // 
        this.dataColumnSettingsDistriktRegionId.ColumnName = "RegionId";
        this.dataColumnSettingsDistriktRegionId.DataType = typeof(int);
        // 
        // dataColumnSettingsDistriktId
        // 
        this.dataColumnSettingsDistriktId.ColumnName = "Id";
        this.dataColumnSettingsDistriktId.DataType = typeof(int);
        // 
        // dataColumnSettingsDistriktNamn
        // 
        this.dataColumnSettingsDistriktNamn.ColumnName = "Namn";
        // 
        // dataTableMarkberedningsmetod
        // 
        this.dataTableMarkberedningsmetod.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSettingsMarkberedningsmetodNamn});
        this.dataTableMarkberedningsmetod.TableName = "Markberedningsmetod";
        // 
        // dataColumnSettingsMarkberedningsmetodNamn
        // 
        this.dataColumnSettingsMarkberedningsmetodNamn.ColumnName = "Namn";
        // 
        // dataTableUrsprung
        // 
        this.dataTableUrsprung.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSettingsUrsprungNamn});
        this.dataTableUrsprung.TableName = "Ursprung";
        // 
        // dataColumnSettingsUrsprungNamn
        // 
        this.dataColumnSettingsUrsprungNamn.ColumnName = "Namn";
        // 
        // dataTableInvTyp
        // 
        this.dataTableInvTyp.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSettingsInvTypId,
            this.dataColumnSettingsInvTypNamn});
        this.dataTableInvTyp.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, false)});
        this.dataTableInvTyp.TableName = "InvTyp";
        // 
        // dataColumnSettingsInvTypId
        // 
        this.dataColumnSettingsInvTypId.ColumnName = "Id";
        this.dataColumnSettingsInvTypId.DataType = typeof(int);
        // 
        // dataColumnSettingsInvTypNamn
        // 
        this.dataColumnSettingsInvTypNamn.ColumnName = "Namn";
        // 
        // menuStripMarkberedning
        // 
        this.menuStripMarkberedning.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.arkivToolStripMenuItem});
        this.menuStripMarkberedning.Location = new System.Drawing.Point(0, 0);
        this.menuStripMarkberedning.Name = "menuStripMarkberedning";
        this.menuStripMarkberedning.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
        this.menuStripMarkberedning.Size = new System.Drawing.Size(564, 24);
        this.menuStripMarkberedning.TabIndex = 0;
        this.menuStripMarkberedning.Text = "menuStrip1";
        // 
        // arkivToolStripMenuItem
        // 
        this.arkivToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sparaToolStripMenuItem,
            this.sparaSomToolStripMenuItem,
            this.toolStripSeparator2,
            this.avslutaToolStripMenuItem});
        this.arkivToolStripMenuItem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.arkivToolStripMenuItem.Name = "arkivToolStripMenuItem";
        this.arkivToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
        this.arkivToolStripMenuItem.Text = global::Egenuppfoljning.Properties.Resources.Arkiv;
        // 
        // sparaToolStripMenuItem
        // 
        this.sparaToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("sparaToolStripMenuItem.Image")));
        this.sparaToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
        this.sparaToolStripMenuItem.Name = "sparaToolStripMenuItem";
        this.sparaToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
        this.sparaToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
        this.sparaToolStripMenuItem.Text = global::Egenuppfoljning.Properties.Resources.Spara;
        this.sparaToolStripMenuItem.Click += new System.EventHandler(this.sparaToolStripMenuItem_Click);
        // 
        // sparaSomToolStripMenuItem
        // 
        this.sparaSomToolStripMenuItem.Name = "sparaSomToolStripMenuItem";
        this.sparaSomToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
        this.sparaSomToolStripMenuItem.Text = global::Egenuppfoljning.Properties.Resources.SparaSom;
        this.sparaSomToolStripMenuItem.Click += new System.EventHandler(this.sparaSomToolStripMenuItem_Click);
        // 
        // toolStripSeparator2
        // 
        this.toolStripSeparator2.Name = "toolStripSeparator2";
        this.toolStripSeparator2.Size = new System.Drawing.Size(147, 6);
        // 
        // avslutaToolStripMenuItem
        // 
        this.avslutaToolStripMenuItem.Name = "avslutaToolStripMenuItem";
        this.avslutaToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
        this.avslutaToolStripMenuItem.Text = "Stäng";
        this.avslutaToolStripMenuItem.Click += new System.EventHandler(this.avslutaToolStripMenuItem_Click);
        // 
        // reportMarkberedning
        // 
        this.reportMarkberedning.ReportResourceString = resources.GetString("reportMarkberedning.ReportResourceString");
        this.reportMarkberedning.RegisterData(this.dataSetMarkberedning, "dataSetMarkberedning");
        // 
        // tabControl1
        // 
        this.tabControl1.Controls.Add(this.tabPageMarkberedning);
        this.tabControl1.Controls.Add(this.tabPageYtor);
        this.tabControl1.Controls.Add(this.tabPageFrågor);
        this.tabControl1.Location = new System.Drawing.Point(0, 27);
        this.tabControl1.Name = "tabControl1";
        this.tabControl1.SelectedIndex = 0;
        this.tabControl1.Size = new System.Drawing.Size(561, 580);
        this.tabControl1.TabIndex = 1;
        // 
        // tabPageMarkberedning
        // 
        this.tabPageMarkberedning.Controls.Add(this.groupBoxFörSådd);
        this.tabPageMarkberedning.Controls.Add(this.groupBoxGIS);
        this.tabPageMarkberedning.Controls.Add(this.groupBoxPlantering);
        this.tabPageMarkberedning.Controls.Add(this.groupBox2);
        this.tabPageMarkberedning.Controls.Add(this.groupBoxTrakt);
        this.tabPageMarkberedning.Controls.Add(this.groupBoxRegion);
        this.tabPageMarkberedning.Location = new System.Drawing.Point(4, 22);
        this.tabPageMarkberedning.Name = "tabPageMarkberedning";
        this.tabPageMarkberedning.Padding = new System.Windows.Forms.Padding(3);
        this.tabPageMarkberedning.Size = new System.Drawing.Size(553, 554);
        this.tabPageMarkberedning.TabIndex = 2;
        this.tabPageMarkberedning.Text = "Markberedning";
        this.tabPageMarkberedning.UseVisualStyleBackColor = true;
        // 
        // groupBoxFörSådd
        // 
        this.groupBoxFörSådd.Controls.Add(this.groupBox12);
        this.groupBoxFörSådd.Controls.Add(this.label16);
        this.groupBoxFörSådd.Controls.Add(this.groupBox11);
        this.groupBoxFörSådd.Controls.Add(this.label15);
        this.groupBoxFörSådd.Location = new System.Drawing.Point(8, 402);
        this.groupBoxFörSådd.Name = "groupBoxFörSådd";
        this.groupBoxFörSådd.Size = new System.Drawing.Size(538, 136);
        this.groupBoxFörSådd.TabIndex = 5;
        this.groupBoxFörSådd.TabStop = false;
        this.groupBoxFörSådd.Text = "För sådd";
        // 
        // groupBox12
        // 
        this.groupBox12.Controls.Add(this.textBoxContortaStambrev);
        this.groupBox12.Controls.Add(this.textBoxTallStambrev);
        this.groupBox12.Location = new System.Drawing.Point(183, 31);
        this.groupBox12.Name = "groupBox12";
        this.groupBox12.Size = new System.Drawing.Size(336, 83);
        this.groupBox12.TabIndex = 3;
        this.groupBox12.TabStop = false;
        this.groupBox12.Text = "Stambrev/proveniens";
        // 
        // textBoxContortaStambrev
        // 
        this.textBoxContortaStambrev.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetMarkberedning, "Fragor.Fraga11", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.textBoxContortaStambrev.Location = new System.Drawing.Point(6, 52);
        this.textBoxContortaStambrev.MaxLength = 64;
        this.textBoxContortaStambrev.Name = "textBoxContortaStambrev";
        this.textBoxContortaStambrev.Size = new System.Drawing.Size(315, 21);
        this.textBoxContortaStambrev.TabIndex = 18;
        this.textBoxContortaStambrev.TextChanged += new System.EventHandler(this.textBoxContortaStambrev_TextChanged);
        // 
        // textBoxTallStambrev
        // 
        this.textBoxTallStambrev.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetMarkberedning, "Fragor.Fraga9", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.textBoxTallStambrev.Location = new System.Drawing.Point(7, 20);
        this.textBoxTallStambrev.MaxLength = 64;
        this.textBoxTallStambrev.Name = "textBoxTallStambrev";
        this.textBoxTallStambrev.Size = new System.Drawing.Size(314, 21);
        this.textBoxTallStambrev.TabIndex = 16;
        this.textBoxTallStambrev.TextChanged += new System.EventHandler(this.textBoxTallStambrev_TextChanged);
        // 
        // label16
        // 
        this.label16.AutoSize = true;
        this.label16.Location = new System.Drawing.Point(15, 86);
        this.label16.Name = "label16";
        this.label16.Size = new System.Drawing.Size(67, 13);
        this.label16.TabIndex = 1;
        this.label16.Text = "CONTORTA";
        // 
        // groupBox11
        // 
        this.groupBox11.Controls.Add(this.textBoxContortaTotGiva);
        this.groupBox11.Controls.Add(this.textBoxTallTotGiva);
        this.groupBox11.Location = new System.Drawing.Point(88, 31);
        this.groupBox11.Name = "groupBox11";
        this.groupBox11.Size = new System.Drawing.Size(97, 83);
        this.groupBox11.TabIndex = 2;
        this.groupBox11.TabStop = false;
        this.groupBox11.Text = "Tot giva (kg)";
        // 
        // textBoxContortaTotGiva
        // 
        this.textBoxContortaTotGiva.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetMarkberedning, "Fragor.Fraga10", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, null, "N1"));
        this.textBoxContortaTotGiva.Location = new System.Drawing.Point(7, 52);
        this.textBoxContortaTotGiva.MaxLength = 5;
        this.textBoxContortaTotGiva.Name = "textBoxContortaTotGiva";
        this.textBoxContortaTotGiva.Size = new System.Drawing.Size(81, 21);
        this.textBoxContortaTotGiva.TabIndex = 17;
        this.textBoxContortaTotGiva.TextChanged += new System.EventHandler(this.textBoxContortaTotGiva_TextChanged);
        this.textBoxContortaTotGiva.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxContortaTotGiva_KeyDown);
        this.textBoxContortaTotGiva.Leave += new System.EventHandler(this.textBoxContortaTotGiva_Leave);
        // 
        // textBoxTallTotGiva
        // 
        this.textBoxTallTotGiva.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetMarkberedning, "Fragor.Fraga8", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, null, "N1"));
        this.textBoxTallTotGiva.Location = new System.Drawing.Point(7, 20);
        this.textBoxTallTotGiva.MaxLength = 5;
        this.textBoxTallTotGiva.Name = "textBoxTallTotGiva";
        this.textBoxTallTotGiva.Size = new System.Drawing.Size(81, 21);
        this.textBoxTallTotGiva.TabIndex = 15;
        this.textBoxTallTotGiva.TextChanged += new System.EventHandler(this.textBoxTallTotGiva_TextChanged);
        this.textBoxTallTotGiva.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxTallTotGiva_KeyDown);
        this.textBoxTallTotGiva.Leave += new System.EventHandler(this.textBoxTallTotGiva_Leave);
        // 
        // label15
        // 
        this.label15.AutoSize = true;
        this.label15.Location = new System.Drawing.Point(14, 54);
        this.label15.Name = "label15";
        this.label15.Size = new System.Drawing.Size(34, 13);
        this.label15.TabIndex = 0;
        this.label15.Text = "TALL";
        // 
        // groupBoxGIS
        // 
        this.groupBoxGIS.Controls.Add(this.textBoxGISData);
        this.groupBoxGIS.Controls.Add(this.labelGISData);
        this.groupBoxGIS.Controls.Add(this.textBoxGISAreal);
        this.groupBoxGIS.Controls.Add(this.labelGISAreal);
        this.groupBoxGIS.Controls.Add(this.textBoxGISStracka);
        this.groupBoxGIS.Controls.Add(this.labelGISStracka);
        this.groupBoxGIS.Location = new System.Drawing.Point(8, 341);
        this.groupBoxGIS.Name = "groupBoxGIS";
        this.groupBoxGIS.Size = new System.Drawing.Size(538, 65);
        this.groupBoxGIS.TabIndex = 4;
        this.groupBoxGIS.TabStop = false;
        this.groupBoxGIS.Text = "GIS";
        // 
        // textBoxGISData
        // 
        this.textBoxGISData.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxGISData.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxGISData.Location = new System.Drawing.Point(457, 27);
        this.textBoxGISData.Name = "textBoxGISData";
        this.textBoxGISData.ReadOnly = true;
        this.textBoxGISData.Size = new System.Drawing.Size(63, 21);
        this.textBoxGISData.TabIndex = 5;
        this.textBoxGISData.TabStop = false;
        this.textBoxGISData.Text = "0";
        // 
        // labelGISData
        // 
        this.labelGISData.AutoSize = true;
        this.labelGISData.Location = new System.Drawing.Point(321, 30);
        this.labelGISData.Name = "labelGISData";
        this.labelGISData.Size = new System.Drawing.Size(134, 13);
        this.labelGISData.TabIndex = 4;
        this.labelGISData.Text = "MB Sträcka per hektar";
        // 
        // textBoxGISAreal
        // 
        this.textBoxGISAreal.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetMarkberedning, "UserData.GISAreal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, null, "N1"));
        this.textBoxGISAreal.Location = new System.Drawing.Point(248, 27);
        this.textBoxGISAreal.MaxLength = 16;
        this.textBoxGISAreal.Name = "textBoxGISAreal";
        this.textBoxGISAreal.Size = new System.Drawing.Size(63, 21);
        this.textBoxGISAreal.TabIndex = 14;
        this.textBoxGISAreal.TextChanged += new System.EventHandler(this.textBoxGISAreal_TextChanged);
        this.textBoxGISAreal.Validated += new System.EventHandler(this.textBoxGISAreal_Validated);
        this.textBoxGISAreal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxGISAreal_KeyDown);
        this.textBoxGISAreal.Leave += new System.EventHandler(this.textBoxGISAreal_Leave);
        // 
        // labelGISAreal
        // 
        this.labelGISAreal.AutoSize = true;
        this.labelGISAreal.Location = new System.Drawing.Point(184, 30);
        this.labelGISAreal.Name = "labelGISAreal";
        this.labelGISAreal.Size = new System.Drawing.Size(64, 13);
        this.labelGISAreal.TabIndex = 2;
        this.labelGISAreal.Text = "Areal (ha)";
        // 
        // textBoxGISStracka
        // 
        this.textBoxGISStracka.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetMarkberedning, "UserData.GISStracka", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.textBoxGISStracka.Location = new System.Drawing.Point(112, 27);
        this.textBoxGISStracka.MaxLength = 16;
        this.textBoxGISStracka.Name = "textBoxGISStracka";
        this.textBoxGISStracka.Size = new System.Drawing.Size(63, 21);
        this.textBoxGISStracka.TabIndex = 13;
        this.textBoxGISStracka.TextChanged += new System.EventHandler(this.textBoxGISStracka_TextChanged);
        this.textBoxGISStracka.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxGISStracka_KeyDown);
        this.textBoxGISStracka.Leave += new System.EventHandler(this.textBoxGISStracka_Leave);
        // 
        // labelGISStracka
        // 
        this.labelGISStracka.AutoSize = true;
        this.labelGISStracka.Location = new System.Drawing.Point(15, 30);
        this.labelGISStracka.Name = "labelGISStracka";
        this.labelGISStracka.Size = new System.Drawing.Size(95, 13);
        this.labelGISStracka.TabIndex = 0;
        this.labelGISStracka.Text = "MB Sträcka (m)";
        // 
        // groupBoxPlantering
        // 
        this.groupBoxPlantering.Controls.Add(this.label11);
        this.groupBoxPlantering.Controls.Add(this.textBoxMåluppfyllelse);
        this.groupBoxPlantering.Controls.Add(this.textBoxPlanteringAntalOptimalaBra);
        this.groupBoxPlantering.Controls.Add(this.label10);
        this.groupBoxPlantering.Controls.Add(this.labelPlanteringAntalOptimalaBra);
        this.groupBoxPlantering.Controls.Add(this.textBoxMål);
        this.groupBoxPlantering.Controls.Add(this.label12);
        this.groupBoxPlantering.Controls.Add(this.textBoxPlanteringAntalOptimala);
        this.groupBoxPlantering.Controls.Add(this.labelPlanteringAntalOptimala);
        this.groupBoxPlantering.Location = new System.Drawing.Point(9, 252);
        this.groupBoxPlantering.Name = "groupBoxPlantering";
        this.groupBoxPlantering.Size = new System.Drawing.Size(538, 98);
        this.groupBoxPlantering.TabIndex = 3;
        this.groupBoxPlantering.TabStop = false;
        this.groupBoxPlantering.Text = "INFÖR PLANTERING";
        // 
        // label11
        // 
        this.label11.AutoSize = true;
        this.label11.Location = new System.Drawing.Point(476, 65);
        this.label11.Name = "label11";
        this.label11.Size = new System.Drawing.Size(20, 13);
        this.label11.TabIndex = 8;
        this.label11.Text = "%";
        // 
        // textBoxMåluppfyllelse
        // 
        this.textBoxMåluppfyllelse.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxMåluppfyllelse.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxMåluppfyllelse.Location = new System.Drawing.Point(412, 62);
        this.textBoxMåluppfyllelse.Name = "textBoxMåluppfyllelse";
        this.textBoxMåluppfyllelse.ReadOnly = true;
        this.textBoxMåluppfyllelse.Size = new System.Drawing.Size(63, 21);
        this.textBoxMåluppfyllelse.TabIndex = 7;
        this.textBoxMåluppfyllelse.TabStop = false;
        this.textBoxMåluppfyllelse.Text = "0";
        // 
        // textBoxPlanteringAntalOptimalaBra
        // 
        this.textBoxPlanteringAntalOptimalaBra.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxPlanteringAntalOptimalaBra.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxPlanteringAntalOptimalaBra.Location = new System.Drawing.Point(412, 27);
        this.textBoxPlanteringAntalOptimalaBra.Name = "textBoxPlanteringAntalOptimalaBra";
        this.textBoxPlanteringAntalOptimalaBra.ReadOnly = true;
        this.textBoxPlanteringAntalOptimalaBra.Size = new System.Drawing.Size(63, 21);
        this.textBoxPlanteringAntalOptimalaBra.TabIndex = 3;
        this.textBoxPlanteringAntalOptimalaBra.TabStop = false;
        this.textBoxPlanteringAntalOptimalaBra.Text = "0";
        // 
        // label10
        // 
        this.label10.AutoSize = true;
        this.label10.Location = new System.Drawing.Point(321, 65);
        this.label10.Name = "label10";
        this.label10.Size = new System.Drawing.Size(88, 13);
        this.label10.TabIndex = 6;
        this.label10.Text = "Måluppfyllelse";
        // 
        // labelPlanteringAntalOptimalaBra
        // 
        this.labelPlanteringAntalOptimalaBra.AutoSize = true;
        this.labelPlanteringAntalOptimalaBra.Location = new System.Drawing.Point(274, 30);
        this.labelPlanteringAntalOptimalaBra.Name = "labelPlanteringAntalOptimalaBra";
        this.labelPlanteringAntalOptimalaBra.Size = new System.Drawing.Size(135, 13);
        this.labelPlanteringAntalOptimalaBra.TabIndex = 2;
        this.labelPlanteringAntalOptimalaBra.Text = "Antal optimala och bra";
        // 
        // textBoxMål
        // 
        this.textBoxMål.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetMarkberedning, "UserData.Mal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.textBoxMål.Location = new System.Drawing.Point(126, 62);
        this.textBoxMål.MaxLength = 16;
        this.textBoxMål.Name = "textBoxMål";
        this.textBoxMål.Size = new System.Drawing.Size(63, 21);
        this.textBoxMål.TabIndex = 12;
        this.textBoxMål.TextChanged += new System.EventHandler(this.textBoxMål_TextChanged);
        this.textBoxMål.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxMål_KeyDown);
        this.textBoxMål.Leave += new System.EventHandler(this.textBoxMål_Leave);
        // 
        // label12
        // 
        this.label12.AutoSize = true;
        this.label12.Location = new System.Drawing.Point(97, 65);
        this.label12.Name = "label12";
        this.label12.Size = new System.Drawing.Size(27, 13);
        this.label12.TabIndex = 4;
        this.label12.Text = "Mål";
        // 
        // textBoxPlanteringAntalOptimala
        // 
        this.textBoxPlanteringAntalOptimala.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxPlanteringAntalOptimala.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxPlanteringAntalOptimala.Location = new System.Drawing.Point(126, 27);
        this.textBoxPlanteringAntalOptimala.Name = "textBoxPlanteringAntalOptimala";
        this.textBoxPlanteringAntalOptimala.ReadOnly = true;
        this.textBoxPlanteringAntalOptimala.Size = new System.Drawing.Size(63, 21);
        this.textBoxPlanteringAntalOptimala.TabIndex = 1;
        this.textBoxPlanteringAntalOptimala.TabStop = false;
        this.textBoxPlanteringAntalOptimala.Text = "0";
        // 
        // labelPlanteringAntalOptimala
        // 
        this.labelPlanteringAntalOptimala.AutoSize = true;
        this.labelPlanteringAntalOptimala.Location = new System.Drawing.Point(34, 30);
        this.labelPlanteringAntalOptimala.Name = "labelPlanteringAntalOptimala";
        this.labelPlanteringAntalOptimala.Size = new System.Drawing.Size(90, 13);
        this.labelPlanteringAntalOptimala.TabIndex = 0;
        this.labelPlanteringAntalOptimala.Text = "Antal optimala";
        // 
        // groupBox2
        // 
        this.groupBox2.Controls.Add(this.label13);
        this.groupBox2.Controls.Add(this.comboBoxDatainsamlingsmetod);
        this.groupBox2.Controls.Add(this.label9);
        this.groupBox2.Controls.Add(this.comboBoxMarkberedningsmetod);
        this.groupBox2.Location = new System.Drawing.Point(8, 181);
        this.groupBox2.Name = "groupBox2";
        this.groupBox2.Size = new System.Drawing.Size(539, 78);
        this.groupBox2.TabIndex = 2;
        this.groupBox2.TabStop = false;
        // 
        // label13
        // 
        this.label13.AutoSize = true;
        this.label13.Location = new System.Drawing.Point(54, 19);
        this.label13.Name = "label13";
        this.label13.Size = new System.Drawing.Size(136, 13);
        this.label13.TabIndex = 0;
        this.label13.Text = "Markberedningsmetod";
        // 
        // comboBoxDatainsamlingsmetod
        // 
        this.comboBoxDatainsamlingsmetod.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetMarkberedning, "UserData.Datainsamlingsmetod", true));
        this.comboBoxDatainsamlingsmetod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxDatainsamlingsmetod.FormattingEnabled = true;
        this.comboBoxDatainsamlingsmetod.Items.AddRange(new object[] {
            "Cirkelyta (100 m2)",
            "Sträckmätning (40 m)"});
        this.comboBoxDatainsamlingsmetod.Location = new System.Drawing.Point(311, 38);
        this.comboBoxDatainsamlingsmetod.Name = "comboBoxDatainsamlingsmetod";
        this.comboBoxDatainsamlingsmetod.Size = new System.Drawing.Size(165, 21);
        this.comboBoxDatainsamlingsmetod.TabIndex = 11;
        this.comboBoxDatainsamlingsmetod.SelectionChangeCommitted += new System.EventHandler(this.comboBoxDatainsamlingsmetod_SelectionChangeCommitted);
        this.comboBoxDatainsamlingsmetod.SelectedIndexChanged += new System.EventHandler(this.comboBoxDatainsamlingsmetod_SelectedIndexChanged);
        // 
        // label9
        // 
        this.label9.AutoSize = true;
        this.label9.Location = new System.Drawing.Point(308, 19);
        this.label9.Name = "label9";
        this.label9.Size = new System.Drawing.Size(131, 13);
        this.label9.TabIndex = 2;
        this.label9.Text = "Datainsamlingsmetod";
        // 
        // comboBoxMarkberedningsmetod
        // 
        this.comboBoxMarkberedningsmetod.Cursor = System.Windows.Forms.Cursors.Default;
        this.comboBoxMarkberedningsmetod.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetMarkberedning, "UserData.Markberedningsmetod", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.comboBoxMarkberedningsmetod.DataSource = this.dataSetSettings;
        this.comboBoxMarkberedningsmetod.DisplayMember = "Markberedningsmetod.Namn";
        this.comboBoxMarkberedningsmetod.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxMarkberedningsmetod.FormattingEnabled = true;
        this.comboBoxMarkberedningsmetod.Location = new System.Drawing.Point(57, 38);
        this.comboBoxMarkberedningsmetod.Name = "comboBoxMarkberedningsmetod";
        this.comboBoxMarkberedningsmetod.Size = new System.Drawing.Size(165, 21);
        this.comboBoxMarkberedningsmetod.TabIndex = 10;
        this.comboBoxMarkberedningsmetod.SelectionChangeCommitted += new System.EventHandler(this.comboBoxMarkberedningsmetod_SelectionChangeCommitted);
        this.comboBoxMarkberedningsmetod.SelectedIndexChanged += new System.EventHandler(this.comboBoxMarkberedningsmetod_SelectedIndexChanged);
        this.comboBoxMarkberedningsmetod.SelectedValueChanged += new System.EventHandler(this.comboBoxMarkberedningsmetod_SelectedValueChanged);
        // 
        // groupBoxTrakt
        // 
        this.groupBoxTrakt.Controls.Add(this.textBoxTraktDel);
        this.groupBoxTrakt.Controls.Add(this.textBoxTraktnr);
        this.groupBoxTrakt.Controls.Add(this.textBoxTraktnamn);
        this.groupBoxTrakt.Controls.Add(this.textBoxEntreprenor);
        this.groupBoxTrakt.Controls.Add(this.labeEntreprenor);
        this.groupBoxTrakt.Controls.Add(this.labelStandort);
        this.groupBoxTrakt.Controls.Add(this.labelTraktnamn);
        this.groupBoxTrakt.Controls.Add(this.labelTraktnr);
        this.groupBoxTrakt.Location = new System.Drawing.Point(8, 120);
        this.groupBoxTrakt.Name = "groupBoxTrakt";
        this.groupBoxTrakt.Size = new System.Drawing.Size(538, 72);
        this.groupBoxTrakt.TabIndex = 1;
        this.groupBoxTrakt.TabStop = false;
        // 
        // textBoxTraktDel
        // 
        this.textBoxTraktDel.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetMarkberedning, "UserData.Standort", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, null, "0000"));
        this.textBoxTraktDel.Location = new System.Drawing.Point(267, 31);
        this.textBoxTraktDel.MaxLength = 4;
        this.textBoxTraktDel.Name = "textBoxTraktDel";
        this.textBoxTraktDel.Size = new System.Drawing.Size(116, 21);
        this.textBoxTraktDel.TabIndex = 8;
        this.textBoxTraktDel.TextChanged += new System.EventHandler(this.textBoxTraktdel_TextChanged);
        this.textBoxTraktDel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxTraktdel_KeyDown);
        this.textBoxTraktDel.Leave += new System.EventHandler(this.textBoxTraktdel_Leave);
        // 
        // textBoxTraktnr
        // 
        this.textBoxTraktnr.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetMarkberedning, "UserData.Traktnr", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, null, "000000000"));
        this.textBoxTraktnr.Location = new System.Drawing.Point(15, 31);
        this.textBoxTraktnr.MaxLength = 9;
        this.textBoxTraktnr.Name = "textBoxTraktnr";
        this.textBoxTraktnr.Size = new System.Drawing.Size(116, 21);
        this.textBoxTraktnr.TabIndex = 6;
        this.textBoxTraktnr.TextChanged += new System.EventHandler(this.textBoxTraktnr_TextChanged);
        this.textBoxTraktnr.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxTraktnr_KeyDown);
        this.textBoxTraktnr.Leave += new System.EventHandler(this.textBoxTraktnr_Leave);
        // 
        // textBoxTraktnamn
        // 
        this.textBoxTraktnamn.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetMarkberedning, "UserData.Traktnamn", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.textBoxTraktnamn.Location = new System.Drawing.Point(140, 31);
        this.textBoxTraktnamn.MaxLength = 31;
        this.textBoxTraktnamn.Name = "textBoxTraktnamn";
        this.textBoxTraktnamn.Size = new System.Drawing.Size(116, 21);
        this.textBoxTraktnamn.TabIndex = 7;
        this.textBoxTraktnamn.TextChanged += new System.EventHandler(this.textBoxTraktnamn_TextChanged);
        // 
        // textBoxEntreprenor
        // 
        this.textBoxEntreprenor.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetMarkberedning, "UserData.Entreprenor", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, null, "0000"));
        this.textBoxEntreprenor.Location = new System.Drawing.Point(388, 31);
        this.textBoxEntreprenor.MaxLength = 4;
        this.textBoxEntreprenor.Name = "textBoxEntreprenor";
        this.textBoxEntreprenor.Size = new System.Drawing.Size(116, 21);
        this.textBoxEntreprenor.TabIndex = 9;
        this.textBoxEntreprenor.TextChanged += new System.EventHandler(this.textBoxEntreprenor_TextChanged);
        this.textBoxEntreprenor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxEntreprenor_KeyDown);
        this.textBoxEntreprenor.Leave += new System.EventHandler(this.textBoxEntreprenor_Leave);
        // 
        // labeEntreprenor
        // 
        this.labeEntreprenor.AutoSize = true;
        this.labeEntreprenor.Location = new System.Drawing.Point(384, 14);
        this.labeEntreprenor.Name = "labeEntreprenor";
        this.labeEntreprenor.Size = new System.Drawing.Size(145, 13);
        this.labeEntreprenor.TabIndex = 6;
        this.labeEntreprenor.Text = "Maskinnr/Entreprenörnr";
        // 
        // labelStandort
        // 
        this.labelStandort.AutoSize = true;
        this.labelStandort.Location = new System.Drawing.Point(262, 14);
        this.labelStandort.Name = "labelStandort";
        this.labelStandort.Size = new System.Drawing.Size(55, 13);
        this.labelStandort.TabIndex = 4;
        this.labelStandort.Text = "Traktdel";
        // 
        // labelTraktnamn
        // 
        this.labelTraktnamn.AutoSize = true;
        this.labelTraktnamn.Location = new System.Drawing.Point(140, 14);
        this.labelTraktnamn.Name = "labelTraktnamn";
        this.labelTraktnamn.Size = new System.Drawing.Size(70, 13);
        this.labelTraktnamn.TabIndex = 2;
        this.labelTraktnamn.Text = "Traktnamn";
        // 
        // labelTraktnr
        // 
        this.labelTraktnr.AutoSize = true;
        this.labelTraktnr.Location = new System.Drawing.Point(13, 14);
        this.labelTraktnr.Name = "labelTraktnr";
        this.labelTraktnr.Size = new System.Drawing.Size(50, 13);
        this.labelTraktnr.TabIndex = 0;
        this.labelTraktnr.Text = "Traktnr";
        // 
        // groupBoxRegion
        // 
        this.groupBoxRegion.Controls.Add(this.textBoxDistrikt);
        this.groupBoxRegion.Controls.Add(this.textBoxRegion);
        this.groupBoxRegion.Controls.Add(this.comboBoxInvtyp);
        this.groupBoxRegion.Controls.Add(this.labelInvtyp);
        this.groupBoxRegion.Controls.Add(this.comboBoxUrsprung);
        this.groupBoxRegion.Controls.Add(this.dateTimePicker);
        this.groupBoxRegion.Controls.Add(this.comboBoxDistrikt);
        this.groupBoxRegion.Controls.Add(this.comboBoxRegion);
        this.groupBoxRegion.Controls.Add(this.labelDatum);
        this.groupBoxRegion.Controls.Add(this.labelUrsprung);
        this.groupBoxRegion.Controls.Add(this.labelDistrikt);
        this.groupBoxRegion.Controls.Add(this.labelRegion);
        this.groupBoxRegion.Location = new System.Drawing.Point(8, 6);
        this.groupBoxRegion.Name = "groupBoxRegion";
        this.groupBoxRegion.Size = new System.Drawing.Size(538, 108);
        this.groupBoxRegion.TabIndex = 0;
        this.groupBoxRegion.TabStop = false;
        // 
        // comboBoxInvtyp
        // 
        this.comboBoxInvtyp.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetMarkberedning, "UserData.InvTyp", true));
        this.comboBoxInvtyp.DataSource = this.dataSetSettings;
        this.comboBoxInvtyp.DisplayMember = "InvTyp.Namn";
        this.comboBoxInvtyp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxInvtyp.FormattingEnabled = true;
        this.comboBoxInvtyp.Location = new System.Drawing.Point(15, 28);
        this.comboBoxInvtyp.Name = "comboBoxInvtyp";
        this.comboBoxInvtyp.Size = new System.Drawing.Size(116, 21);
        this.comboBoxInvtyp.TabIndex = 1;
        this.comboBoxInvtyp.ValueMember = "InvTyp.Id";
        this.comboBoxInvtyp.SelectionChangeCommitted += new System.EventHandler(this.comboBoxInvtyp_SelectionChangeCommitted);
        this.comboBoxInvtyp.SelectedIndexChanged += new System.EventHandler(this.comboBoxInvtyp_SelectedIndexChanged);
        // 
        // labelInvtyp
        // 
        this.labelInvtyp.AutoSize = true;
        this.labelInvtyp.Location = new System.Drawing.Point(15, 12);
        this.labelInvtyp.Name = "labelInvtyp";
        this.labelInvtyp.Size = new System.Drawing.Size(45, 13);
        this.labelInvtyp.TabIndex = 8;
        this.labelInvtyp.Text = "Invtyp";
        // 
        // comboBoxUrsprung
        // 
        this.comboBoxUrsprung.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetMarkberedning, "UserData.Ursprung", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.comboBoxUrsprung.DataSource = this.dataSetSettings;
        this.comboBoxUrsprung.DisplayMember = "Ursprung.Namn";
        this.comboBoxUrsprung.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxUrsprung.FormattingEnabled = true;
        this.comboBoxUrsprung.Location = new System.Drawing.Point(388, 29);
        this.comboBoxUrsprung.Name = "comboBoxUrsprung";
        this.comboBoxUrsprung.Size = new System.Drawing.Size(116, 21);
        this.comboBoxUrsprung.TabIndex = 4;
        this.comboBoxUrsprung.SelectionChangeCommitted += new System.EventHandler(this.comboBoxUrsprung_SelectionChangeCommitted);
        this.comboBoxUrsprung.SelectedIndexChanged += new System.EventHandler(this.comboBoxUrsprung_SelectedIndexChanged);
        // 
        // dateTimePicker
        // 
        this.dateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.dataSetMarkberedning, "UserData.Datum", true));
        this.dateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
        this.dateTimePicker.Location = new System.Drawing.Point(15, 77);
        this.dateTimePicker.Name = "dateTimePicker";
        this.dateTimePicker.Size = new System.Drawing.Size(116, 21);
        this.dateTimePicker.TabIndex = 5;
        this.dateTimePicker.ValueChanged += new System.EventHandler(this.dateTimePicker_ValueChanged);
        // 
        // comboBoxDistrikt
        // 
        this.comboBoxDistrikt.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetMarkberedning, "UserData.Distrikt", true));
        this.comboBoxDistrikt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxDistrikt.FormattingEnabled = true;
        this.comboBoxDistrikt.Location = new System.Drawing.Point(267, 29);
        this.comboBoxDistrikt.Name = "comboBoxDistrikt";
        this.comboBoxDistrikt.Size = new System.Drawing.Size(116, 21);
        this.comboBoxDistrikt.TabIndex = 3;
        this.comboBoxDistrikt.SelectionChangeCommitted += new System.EventHandler(this.comboBoxDistrikt_SelectionChangeCommitted);
        this.comboBoxDistrikt.SelectedIndexChanged += new System.EventHandler(this.comboBoxDistrikt_SelectedIndexChanged);
        // 
        // comboBoxRegion
        // 
        this.comboBoxRegion.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetMarkberedning, "UserData.Region", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.comboBoxRegion.DataSource = this.dataSetSettings;
        this.comboBoxRegion.DisplayMember = "Region.Namn";
        this.comboBoxRegion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxRegion.FormattingEnabled = true;
        this.comboBoxRegion.Location = new System.Drawing.Point(140, 29);
        this.comboBoxRegion.Name = "comboBoxRegion";
        this.comboBoxRegion.Size = new System.Drawing.Size(116, 21);
        this.comboBoxRegion.TabIndex = 2;
        this.comboBoxRegion.ValueMember = "Region.Id";
        this.comboBoxRegion.SelectionChangeCommitted += new System.EventHandler(this.comboBoxRegion_SelectionChangeCommitted);
        this.comboBoxRegion.SelectedIndexChanged += new System.EventHandler(this.comboBoxRegion_SelectedIndexChanged);
        // 
        // labelDatum
        // 
        this.labelDatum.AutoSize = true;
        this.labelDatum.Location = new System.Drawing.Point(15, 60);
        this.labelDatum.Name = "labelDatum";
        this.labelDatum.Size = new System.Drawing.Size(45, 13);
        this.labelDatum.TabIndex = 6;
        this.labelDatum.Text = "Datum";
        // 
        // labelUrsprung
        // 
        this.labelUrsprung.AutoSize = true;
        this.labelUrsprung.Location = new System.Drawing.Point(388, 13);
        this.labelUrsprung.Name = "labelUrsprung";
        this.labelUrsprung.Size = new System.Drawing.Size(59, 13);
        this.labelUrsprung.TabIndex = 4;
        this.labelUrsprung.Text = "Ursprung";
        // 
        // labelDistrikt
        // 
        this.labelDistrikt.AutoSize = true;
        this.labelDistrikt.Location = new System.Drawing.Point(267, 13);
        this.labelDistrikt.Name = "labelDistrikt";
        this.labelDistrikt.Size = new System.Drawing.Size(49, 13);
        this.labelDistrikt.TabIndex = 2;
        this.labelDistrikt.Text = "Distrikt";
        // 
        // labelRegion
        // 
        this.labelRegion.AutoSize = true;
        this.labelRegion.Location = new System.Drawing.Point(140, 13);
        this.labelRegion.Name = "labelRegion";
        this.labelRegion.Size = new System.Drawing.Size(46, 13);
        this.labelRegion.TabIndex = 0;
        this.labelRegion.Text = "Region";
        // 
        // tabPageYtor
        // 
        this.tabPageYtor.Controls.Add(this.groupBoxMarkberedningsstallen);
        this.tabPageYtor.Location = new System.Drawing.Point(4, 22);
        this.tabPageYtor.Name = "tabPageYtor";
        this.tabPageYtor.Padding = new System.Windows.Forms.Padding(3);
        this.tabPageYtor.Size = new System.Drawing.Size(553, 554);
        this.tabPageYtor.TabIndex = 0;
        this.tabPageYtor.Text = "Ytor";
        this.tabPageYtor.UseVisualStyleBackColor = true;
        // 
        // groupBoxMarkberedningsstallen
        // 
        this.groupBoxMarkberedningsstallen.Controls.Add(this.groupBoxBra);
        this.groupBoxMarkberedningsstallen.Controls.Add(this.dataGridViewMarkberedningsstallen);
        this.groupBoxMarkberedningsstallen.Controls.Add(this.groupBoxMedelvarde);
        this.groupBoxMarkberedningsstallen.Controls.Add(this.groupBoxSumma);
        this.groupBoxMarkberedningsstallen.Location = new System.Drawing.Point(8, 6);
        this.groupBoxMarkberedningsstallen.Name = "groupBoxMarkberedningsstallen";
        this.groupBoxMarkberedningsstallen.Size = new System.Drawing.Size(550, 439);
        this.groupBoxMarkberedningsstallen.TabIndex = 0;
        this.groupBoxMarkberedningsstallen.TabStop = false;
        this.groupBoxMarkberedningsstallen.Text = "ANTALET MARKBEREDNINGSSTÄLLEN";
        // 
        // groupBoxBra
        // 
        this.groupBoxBra.BackColor = System.Drawing.SystemColors.Control;
        this.groupBoxBra.Controls.Add(this.groupBoxBlekjordsflack);
        this.groupBoxBra.Controls.Add(this.labelBra);
        this.groupBoxBra.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.groupBoxBra.Location = new System.Drawing.Point(222, 20);
        this.groupBoxBra.Name = "groupBoxBra";
        this.groupBoxBra.Size = new System.Drawing.Size(182, 47);
        this.groupBoxBra.TabIndex = 1;
        this.groupBoxBra.TabStop = false;
        // 
        // groupBoxBlekjordsflack
        // 
        this.groupBoxBlekjordsflack.Controls.Add(this.labelBlekjordsflack);
        this.groupBoxBlekjordsflack.Location = new System.Drawing.Point(89, 20);
        this.groupBoxBlekjordsflack.Name = "groupBoxBlekjordsflack";
        this.groupBoxBlekjordsflack.Size = new System.Drawing.Size(93, 27);
        this.groupBoxBlekjordsflack.TabIndex = 1;
        this.groupBoxBlekjordsflack.TabStop = false;
        // 
        // labelBlekjordsflack
        // 
        this.labelBlekjordsflack.AutoSize = true;
        this.labelBlekjordsflack.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelBlekjordsflack.Location = new System.Drawing.Point(2, 9);
        this.labelBlekjordsflack.Name = "labelBlekjordsflack";
        this.labelBlekjordsflack.Size = new System.Drawing.Size(87, 13);
        this.labelBlekjordsflack.TabIndex = 0;
        this.labelBlekjordsflack.Text = "Blekjordsfläck";
        // 
        // labelBra
        // 
        this.labelBra.AutoSize = true;
        this.labelBra.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelBra.Location = new System.Drawing.Point(6, 8);
        this.labelBra.Name = "labelBra";
        this.labelBra.Size = new System.Drawing.Size(26, 13);
        this.labelBra.TabIndex = 0;
        this.labelBra.Text = "Bra";
        // 
        // dataGridViewMarkberedningsstallen
        // 
        this.dataGridViewMarkberedningsstallen.AllowUserToResizeColumns = false;
        this.dataGridViewMarkberedningsstallen.AllowUserToResizeRows = false;
        dataGridViewCellStyle19.NullValue = null;
        this.dataGridViewMarkberedningsstallen.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle19;
        this.dataGridViewMarkberedningsstallen.AutoGenerateColumns = false;
        this.dataGridViewMarkberedningsstallen.BackgroundColor = System.Drawing.SystemColors.ControlDark;
        this.dataGridViewMarkberedningsstallen.BorderStyle = System.Windows.Forms.BorderStyle.None;
        dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
        dataGridViewCellStyle20.BackColor = System.Drawing.SystemColors.Control;
        dataGridViewCellStyle20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        dataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.WindowText;
        dataGridViewCellStyle20.NullValue = null;
        dataGridViewCellStyle20.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle20.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        this.dataGridViewMarkberedningsstallen.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle20;
        this.dataGridViewMarkberedningsstallen.ColumnHeadersHeight = 40;
        this.dataGridViewMarkberedningsstallen.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
        this.dataGridViewMarkberedningsstallen.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ytaDataGridViewTextBoxColumn,
            this.optimaltDataGridViewTextBoxColumn,
            this.braDataGridViewTextBoxColumn,
            this.blekjordsflackDataGridViewTextBoxColumn,
            this.optimaltBraDataGridViewTextBoxColumn});
        this.dataGridViewMarkberedningsstallen.DataMember = "Yta";
        this.dataGridViewMarkberedningsstallen.DataSource = this.dataSetMarkberedning;
        dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        dataGridViewCellStyle26.BackColor = System.Drawing.SystemColors.Window;
        dataGridViewCellStyle26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        dataGridViewCellStyle26.ForeColor = System.Drawing.SystemColors.ControlText;
        dataGridViewCellStyle26.Format = "N0";
        dataGridViewCellStyle26.NullValue = null;
        dataGridViewCellStyle26.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle26.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
        this.dataGridViewMarkberedningsstallen.DefaultCellStyle = dataGridViewCellStyle26;
        this.dataGridViewMarkberedningsstallen.Location = new System.Drawing.Point(38, 26);
        this.dataGridViewMarkberedningsstallen.Name = "dataGridViewMarkberedningsstallen";
        this.dataGridViewMarkberedningsstallen.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
        dataGridViewCellStyle27.Format = "N0";
        dataGridViewCellStyle27.NullValue = null;
        this.dataGridViewMarkberedningsstallen.RowsDefaultCellStyle = dataGridViewCellStyle27;
        this.dataGridViewMarkberedningsstallen.RowTemplate.DefaultCellStyle.Format = "N0";
        this.dataGridViewMarkberedningsstallen.RowTemplate.DefaultCellStyle.NullValue = null;
        this.dataGridViewMarkberedningsstallen.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
        this.dataGridViewMarkberedningsstallen.Size = new System.Drawing.Size(482, 283);
        this.dataGridViewMarkberedningsstallen.TabIndex = 0;
        this.dataGridViewMarkberedningsstallen.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dataGridViewMarkberedningsstallen_UserDeletingRow);
        this.dataGridViewMarkberedningsstallen.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dataGridViewMarkberedningsstallen_UserDeletedRow);
        this.dataGridViewMarkberedningsstallen.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dataGridViewMarkberedningsstallen_CellValidating);
        this.dataGridViewMarkberedningsstallen.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dataGridViewMarkberedningsstallen_RowsAdded);
        this.dataGridViewMarkberedningsstallen.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewMarkberedningsstallen_CellEndEdit);
        this.dataGridViewMarkberedningsstallen.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewMarkberedningsstallen_DataError);
        // 
        // ytaDataGridViewTextBoxColumn
        // 
        this.ytaDataGridViewTextBoxColumn.DataPropertyName = "Yta";
        dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle21.BackColor = System.Drawing.Color.LemonChiffon;
        dataGridViewCellStyle21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.ytaDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle21;
        this.ytaDataGridViewTextBoxColumn.HeaderText = "Yta";
        this.ytaDataGridViewTextBoxColumn.Name = "ytaDataGridViewTextBoxColumn";
        this.ytaDataGridViewTextBoxColumn.ReadOnly = true;
        this.ytaDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
        this.ytaDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        this.ytaDataGridViewTextBoxColumn.Width = 54;
        // 
        // optimaltDataGridViewTextBoxColumn
        // 
        this.optimaltDataGridViewTextBoxColumn.DataPropertyName = "Optimalt";
        dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle22.BackColor = System.Drawing.Color.White;
        dataGridViewCellStyle22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        dataGridViewCellStyle22.NullValue = null;
        this.optimaltDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle22;
        this.optimaltDataGridViewTextBoxColumn.HeaderText = "Optimalt";
        this.optimaltDataGridViewTextBoxColumn.Name = "optimaltDataGridViewTextBoxColumn";
        this.optimaltDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
        this.optimaltDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        this.optimaltDataGridViewTextBoxColumn.Width = 90;
        // 
        // braDataGridViewTextBoxColumn
        // 
        this.braDataGridViewTextBoxColumn.DataPropertyName = "Bra";
        dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle23.BackColor = System.Drawing.Color.White;
        dataGridViewCellStyle23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        dataGridViewCellStyle23.NullValue = null;
        this.braDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle23;
        this.braDataGridViewTextBoxColumn.HeaderText = "Bra";
        this.braDataGridViewTextBoxColumn.Name = "braDataGridViewTextBoxColumn";
        this.braDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
        this.braDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        this.braDataGridViewTextBoxColumn.Width = 90;
        // 
        // blekjordsflackDataGridViewTextBoxColumn
        // 
        this.blekjordsflackDataGridViewTextBoxColumn.DataPropertyName = "Blekjordsflack";
        dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle24.BackColor = System.Drawing.Color.White;
        dataGridViewCellStyle24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        dataGridViewCellStyle24.NullValue = null;
        this.blekjordsflackDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle24;
        this.blekjordsflackDataGridViewTextBoxColumn.HeaderText = "Blekjordsfläck";
        this.blekjordsflackDataGridViewTextBoxColumn.Name = "blekjordsflackDataGridViewTextBoxColumn";
        this.blekjordsflackDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
        this.blekjordsflackDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        this.blekjordsflackDataGridViewTextBoxColumn.Width = 90;
        // 
        // optimaltBraDataGridViewTextBoxColumn
        // 
        this.optimaltBraDataGridViewTextBoxColumn.DataPropertyName = "OptimaltBra";
        dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle25.BackColor = System.Drawing.Color.LemonChiffon;
        dataGridViewCellStyle25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        dataGridViewCellStyle25.NullValue = null;
        this.optimaltBraDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle25;
        this.optimaltBraDataGridViewTextBoxColumn.HeaderText = "Optimalt+Bra";
        this.optimaltBraDataGridViewTextBoxColumn.Name = "optimaltBraDataGridViewTextBoxColumn";
        this.optimaltBraDataGridViewTextBoxColumn.ReadOnly = true;
        this.optimaltBraDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
        this.optimaltBraDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        this.optimaltBraDataGridViewTextBoxColumn.Width = 90;
        // 
        // groupBoxMedelvarde
        // 
        this.groupBoxMedelvarde.Controls.Add(this.textBoxMedelvardeOptimaltBra);
        this.groupBoxMedelvarde.Controls.Add(this.labelMedelvardeOptimaltBra);
        this.groupBoxMedelvarde.Controls.Add(this.textBoxMedelvardeBlekjordsflack);
        this.groupBoxMedelvarde.Controls.Add(this.labelMedelvardeBlekjordsflack);
        this.groupBoxMedelvarde.Controls.Add(this.textBoxMedelvardeBra);
        this.groupBoxMedelvarde.Controls.Add(this.labelMedelvardeBra);
        this.groupBoxMedelvarde.Controls.Add(this.textBoxMedelvardeOptimalt);
        this.groupBoxMedelvarde.Controls.Add(this.labelMedelvardeOptimalt);
        this.groupBoxMedelvarde.Location = new System.Drawing.Point(0, 374);
        this.groupBoxMedelvarde.Name = "groupBoxMedelvarde";
        this.groupBoxMedelvarde.Size = new System.Drawing.Size(550, 65);
        this.groupBoxMedelvarde.TabIndex = 3;
        this.groupBoxMedelvarde.TabStop = false;
        this.groupBoxMedelvarde.Text = "MEDELVÄRDE";
        // 
        // textBoxMedelvardeOptimaltBra
        // 
        this.textBoxMedelvardeOptimaltBra.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxMedelvardeOptimaltBra.Location = new System.Drawing.Point(463, 27);
        this.textBoxMedelvardeOptimaltBra.Name = "textBoxMedelvardeOptimaltBra";
        this.textBoxMedelvardeOptimaltBra.ReadOnly = true;
        this.textBoxMedelvardeOptimaltBra.Size = new System.Drawing.Size(63, 21);
        this.textBoxMedelvardeOptimaltBra.TabIndex = 7;
        this.textBoxMedelvardeOptimaltBra.TabStop = false;
        this.textBoxMedelvardeOptimaltBra.Text = "0";
        // 
        // labelMedelvardeOptimaltBra
        // 
        this.labelMedelvardeOptimaltBra.AutoSize = true;
        this.labelMedelvardeOptimaltBra.Location = new System.Drawing.Point(378, 30);
        this.labelMedelvardeOptimaltBra.Name = "labelMedelvardeOptimaltBra";
        this.labelMedelvardeOptimaltBra.Size = new System.Drawing.Size(87, 13);
        this.labelMedelvardeOptimaltBra.TabIndex = 6;
        this.labelMedelvardeOptimaltBra.Text = "Optimalt+Bra:";
        // 
        // textBoxMedelvardeBlekjordsflack
        // 
        this.textBoxMedelvardeBlekjordsflack.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxMedelvardeBlekjordsflack.Location = new System.Drawing.Point(314, 27);
        this.textBoxMedelvardeBlekjordsflack.Name = "textBoxMedelvardeBlekjordsflack";
        this.textBoxMedelvardeBlekjordsflack.ReadOnly = true;
        this.textBoxMedelvardeBlekjordsflack.Size = new System.Drawing.Size(63, 21);
        this.textBoxMedelvardeBlekjordsflack.TabIndex = 5;
        this.textBoxMedelvardeBlekjordsflack.TabStop = false;
        this.textBoxMedelvardeBlekjordsflack.Text = "0";
        // 
        // labelMedelvardeBlekjordsflack
        // 
        this.labelMedelvardeBlekjordsflack.AutoSize = true;
        this.labelMedelvardeBlekjordsflack.Location = new System.Drawing.Point(226, 30);
        this.labelMedelvardeBlekjordsflack.Name = "labelMedelvardeBlekjordsflack";
        this.labelMedelvardeBlekjordsflack.Size = new System.Drawing.Size(90, 13);
        this.labelMedelvardeBlekjordsflack.TabIndex = 4;
        this.labelMedelvardeBlekjordsflack.Text = "Blekjordsfläck:";
        // 
        // textBoxMedelvardeBra
        // 
        this.textBoxMedelvardeBra.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxMedelvardeBra.Location = new System.Drawing.Point(161, 27);
        this.textBoxMedelvardeBra.Name = "textBoxMedelvardeBra";
        this.textBoxMedelvardeBra.ReadOnly = true;
        this.textBoxMedelvardeBra.Size = new System.Drawing.Size(63, 21);
        this.textBoxMedelvardeBra.TabIndex = 3;
        this.textBoxMedelvardeBra.TabStop = false;
        this.textBoxMedelvardeBra.Text = "0";
        // 
        // labelMedelvardeBra
        // 
        this.labelMedelvardeBra.AutoSize = true;
        this.labelMedelvardeBra.Location = new System.Drawing.Point(133, 30);
        this.labelMedelvardeBra.Name = "labelMedelvardeBra";
        this.labelMedelvardeBra.Size = new System.Drawing.Size(29, 13);
        this.labelMedelvardeBra.TabIndex = 2;
        this.labelMedelvardeBra.Text = "Bra:";
        // 
        // textBoxMedelvardeOptimalt
        // 
        this.textBoxMedelvardeOptimalt.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxMedelvardeOptimalt.Location = new System.Drawing.Point(66, 27);
        this.textBoxMedelvardeOptimalt.Name = "textBoxMedelvardeOptimalt";
        this.textBoxMedelvardeOptimalt.ReadOnly = true;
        this.textBoxMedelvardeOptimalt.Size = new System.Drawing.Size(63, 21);
        this.textBoxMedelvardeOptimalt.TabIndex = 1;
        this.textBoxMedelvardeOptimalt.TabStop = false;
        this.textBoxMedelvardeOptimalt.Text = "0";
        // 
        // labelMedelvardeOptimalt
        // 
        this.labelMedelvardeOptimalt.AutoSize = true;
        this.labelMedelvardeOptimalt.Location = new System.Drawing.Point(9, 30);
        this.labelMedelvardeOptimalt.Name = "labelMedelvardeOptimalt";
        this.labelMedelvardeOptimalt.Size = new System.Drawing.Size(59, 13);
        this.labelMedelvardeOptimalt.TabIndex = 0;
        this.labelMedelvardeOptimalt.Text = "Optimalt:";
        // 
        // groupBoxSumma
        // 
        this.groupBoxSumma.Controls.Add(this.textBoxSummaOptimalt);
        this.groupBoxSumma.Controls.Add(this.textBoxSummaOptimaltBra);
        this.groupBoxSumma.Controls.Add(this.labelSummaOptimaltBra);
        this.groupBoxSumma.Controls.Add(this.textBoxSummaBlekjordsflack);
        this.groupBoxSumma.Controls.Add(this.labelSummaBlekjordsflack);
        this.groupBoxSumma.Controls.Add(this.textBoxSummaBra);
        this.groupBoxSumma.Controls.Add(this.labelSummaBra);
        this.groupBoxSumma.Controls.Add(this.labelSummaOptimalt);
        this.groupBoxSumma.Location = new System.Drawing.Point(0, 315);
        this.groupBoxSumma.Name = "groupBoxSumma";
        this.groupBoxSumma.Size = new System.Drawing.Size(550, 65);
        this.groupBoxSumma.TabIndex = 2;
        this.groupBoxSumma.TabStop = false;
        this.groupBoxSumma.Text = "SUMMA";
        // 
        // textBoxSummaOptimalt
        // 
        this.textBoxSummaOptimalt.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxSummaOptimalt.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxSummaOptimalt.Location = new System.Drawing.Point(67, 27);
        this.textBoxSummaOptimalt.Name = "textBoxSummaOptimalt";
        this.textBoxSummaOptimalt.ReadOnly = true;
        this.textBoxSummaOptimalt.Size = new System.Drawing.Size(63, 21);
        this.textBoxSummaOptimalt.TabIndex = 1;
        this.textBoxSummaOptimalt.TabStop = false;
        this.textBoxSummaOptimalt.Text = "0";
        // 
        // textBoxSummaOptimaltBra
        // 
        this.textBoxSummaOptimaltBra.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxSummaOptimaltBra.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxSummaOptimaltBra.Location = new System.Drawing.Point(463, 27);
        this.textBoxSummaOptimaltBra.Name = "textBoxSummaOptimaltBra";
        this.textBoxSummaOptimaltBra.ReadOnly = true;
        this.textBoxSummaOptimaltBra.Size = new System.Drawing.Size(63, 21);
        this.textBoxSummaOptimaltBra.TabIndex = 7;
        this.textBoxSummaOptimaltBra.TabStop = false;
        this.textBoxSummaOptimaltBra.Text = "0";
        // 
        // labelSummaOptimaltBra
        // 
        this.labelSummaOptimaltBra.AutoSize = true;
        this.labelSummaOptimaltBra.Location = new System.Drawing.Point(379, 30);
        this.labelSummaOptimaltBra.Name = "labelSummaOptimaltBra";
        this.labelSummaOptimaltBra.Size = new System.Drawing.Size(87, 13);
        this.labelSummaOptimaltBra.TabIndex = 6;
        this.labelSummaOptimaltBra.Text = "Optimalt+Bra:";
        // 
        // textBoxSummaBlekjordsflack
        // 
        this.textBoxSummaBlekjordsflack.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxSummaBlekjordsflack.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxSummaBlekjordsflack.Location = new System.Drawing.Point(314, 27);
        this.textBoxSummaBlekjordsflack.Name = "textBoxSummaBlekjordsflack";
        this.textBoxSummaBlekjordsflack.ReadOnly = true;
        this.textBoxSummaBlekjordsflack.Size = new System.Drawing.Size(63, 21);
        this.textBoxSummaBlekjordsflack.TabIndex = 5;
        this.textBoxSummaBlekjordsflack.TabStop = false;
        this.textBoxSummaBlekjordsflack.Text = "0";
        // 
        // labelSummaBlekjordsflack
        // 
        this.labelSummaBlekjordsflack.AutoSize = true;
        this.labelSummaBlekjordsflack.Location = new System.Drawing.Point(226, 30);
        this.labelSummaBlekjordsflack.Name = "labelSummaBlekjordsflack";
        this.labelSummaBlekjordsflack.Size = new System.Drawing.Size(90, 13);
        this.labelSummaBlekjordsflack.TabIndex = 4;
        this.labelSummaBlekjordsflack.Text = "Blekjordsfläck:";
        // 
        // textBoxSummaBra
        // 
        this.textBoxSummaBra.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxSummaBra.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxSummaBra.Location = new System.Drawing.Point(162, 27);
        this.textBoxSummaBra.Name = "textBoxSummaBra";
        this.textBoxSummaBra.ReadOnly = true;
        this.textBoxSummaBra.Size = new System.Drawing.Size(63, 21);
        this.textBoxSummaBra.TabIndex = 3;
        this.textBoxSummaBra.TabStop = false;
        this.textBoxSummaBra.Text = "0";
        // 
        // labelSummaBra
        // 
        this.labelSummaBra.AutoSize = true;
        this.labelSummaBra.Location = new System.Drawing.Point(133, 30);
        this.labelSummaBra.Name = "labelSummaBra";
        this.labelSummaBra.Size = new System.Drawing.Size(29, 13);
        this.labelSummaBra.TabIndex = 2;
        this.labelSummaBra.Text = "Bra:";
        // 
        // labelSummaOptimalt
        // 
        this.labelSummaOptimalt.AutoSize = true;
        this.labelSummaOptimalt.Location = new System.Drawing.Point(9, 30);
        this.labelSummaOptimalt.Name = "labelSummaOptimalt";
        this.labelSummaOptimalt.Size = new System.Drawing.Size(59, 13);
        this.labelSummaOptimalt.TabIndex = 0;
        this.labelSummaOptimalt.Text = "Optimalt:";
        // 
        // tabPageFrågor
        // 
        this.tabPageFrågor.Controls.Add(this.groupBox1);
        this.tabPageFrågor.Controls.Add(this.label8);
        this.tabPageFrågor.Controls.Add(this.groupBox9);
        this.tabPageFrågor.Controls.Add(this.groupBox8);
        this.tabPageFrågor.Controls.Add(this.groupBox7);
        this.tabPageFrågor.Controls.Add(this.groupBox6);
        this.tabPageFrågor.Controls.Add(this.groupBox5);
        this.tabPageFrågor.Controls.Add(this.groupBox3);
        this.tabPageFrågor.Controls.Add(this.groupBox4);
        this.tabPageFrågor.Location = new System.Drawing.Point(4, 22);
        this.tabPageFrågor.Name = "tabPageFrågor";
        this.tabPageFrågor.Padding = new System.Windows.Forms.Padding(3);
        this.tabPageFrågor.Size = new System.Drawing.Size(553, 554);
        this.tabPageFrågor.TabIndex = 1;
        this.tabPageFrågor.Text = "Frågor";
        this.tabPageFrågor.UseVisualStyleBackColor = true;
        // 
        // groupBox1
        // 
        this.groupBox1.Controls.Add(this.richTextBoxKommentar);
        this.groupBox1.Location = new System.Drawing.Point(8, 407);
        this.groupBox1.Name = "groupBox1";
        this.groupBox1.Size = new System.Drawing.Size(538, 100);
        this.groupBox1.TabIndex = 18;
        this.groupBox1.TabStop = false;
        this.groupBox1.Text = "KOMMENTARER";
        // 
        // richTextBoxKommentar
        // 
        this.richTextBoxKommentar.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetMarkberedning, "UserData.Kommentar", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.richTextBoxKommentar.Location = new System.Drawing.Point(15, 20);
        this.richTextBoxKommentar.MaxLength = 4095;
        this.richTextBoxKommentar.Name = "richTextBoxKommentar";
        this.richTextBoxKommentar.Size = new System.Drawing.Size(510, 68);
        this.richTextBoxKommentar.TabIndex = 0;
        this.richTextBoxKommentar.Text = "";
        this.richTextBoxKommentar.TextChanged += new System.EventHandler(this.richTextBoxKommentar_TextChanged);
        // 
        // label8
        // 
        this.label8.AutoSize = true;
        this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label8.Location = new System.Drawing.Point(7, 9);
        this.label8.Name = "label8";
        this.label8.Size = new System.Drawing.Size(202, 13);
        this.label8.TabIndex = 10;
        this.label8.Text = "NATUR- OCH KULTURMILJÖHÄNSYN";
        // 
        // groupBox9
        // 
        this.groupBox9.Controls.Add(this.radioButton7Nej);
        this.groupBox9.Controls.Add(this.radioButton7Ja);
        this.groupBox9.Controls.Add(this.label7);
        this.groupBox9.Location = new System.Drawing.Point(8, 295);
        this.groupBox9.Name = "groupBox9";
        this.groupBox9.Size = new System.Drawing.Size(535, 53);
        this.groupBox9.TabIndex = 17;
        this.groupBox9.TabStop = false;
        this.groupBox9.Text = "7.";
        // 
        // radioButton7Nej
        // 
        this.radioButton7Nej.AutoSize = true;
        this.radioButton7Nej.Location = new System.Drawing.Point(470, 19);
        this.radioButton7Nej.Name = "radioButton7Nej";
        this.radioButton7Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton7Nej.TabIndex = 19;
        this.radioButton7Nej.TabStop = true;
        this.radioButton7Nej.Text = "Nej";
        this.radioButton7Nej.UseVisualStyleBackColor = true;
        this.radioButton7Nej.CheckedChanged += new System.EventHandler(this.radioButton7Nej_CheckedChanged);
        // 
        // radioButton7Ja
        // 
        this.radioButton7Ja.AutoSize = true;
        this.radioButton7Ja.Location = new System.Drawing.Point(420, 19);
        this.radioButton7Ja.Name = "radioButton7Ja";
        this.radioButton7Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton7Ja.TabIndex = 18;
        this.radioButton7Ja.TabStop = true;
        this.radioButton7Ja.Text = "Ja";
        this.radioButton7Ja.UseVisualStyleBackColor = true;
        this.radioButton7Ja.CheckedChanged += new System.EventHandler(this.radioButton7Ja_CheckedChanged);
        // 
        // label7
        // 
        this.label7.AutoSize = true;
        this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label7.Location = new System.Drawing.Point(19, 21);
        this.label7.Name = "label7";
        this.label7.Size = new System.Drawing.Size(273, 13);
        this.label7.TabIndex = 0;
        this.label7.Text = "Är stigar och leder oskadade och framkomliga?";
        // 
        // groupBox8
        // 
        this.groupBox8.Controls.Add(this.radioButton6Nej);
        this.groupBox8.Controls.Add(this.radioButton6Ja);
        this.groupBox8.Controls.Add(this.label6);
        this.groupBox8.Location = new System.Drawing.Point(8, 254);
        this.groupBox8.Name = "groupBox8";
        this.groupBox8.Size = new System.Drawing.Size(535, 45);
        this.groupBox8.TabIndex = 16;
        this.groupBox8.TabStop = false;
        this.groupBox8.Text = "6.";
        // 
        // radioButton6Nej
        // 
        this.radioButton6Nej.AutoSize = true;
        this.radioButton6Nej.Location = new System.Drawing.Point(470, 19);
        this.radioButton6Nej.Name = "radioButton6Nej";
        this.radioButton6Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton6Nej.TabIndex = 17;
        this.radioButton6Nej.TabStop = true;
        this.radioButton6Nej.Text = "Nej";
        this.radioButton6Nej.UseVisualStyleBackColor = true;
        this.radioButton6Nej.CheckedChanged += new System.EventHandler(this.radioButton6Nej_CheckedChanged);
        // 
        // radioButton6Ja
        // 
        this.radioButton6Ja.AutoSize = true;
        this.radioButton6Ja.Location = new System.Drawing.Point(420, 19);
        this.radioButton6Ja.Name = "radioButton6Ja";
        this.radioButton6Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton6Ja.TabIndex = 16;
        this.radioButton6Ja.TabStop = true;
        this.radioButton6Ja.Text = "Ja";
        this.radioButton6Ja.UseVisualStyleBackColor = true;
        this.radioButton6Ja.CheckedChanged += new System.EventHandler(this.radioButton6Ja_CheckedChanged);
        // 
        // label6
        // 
        this.label6.AutoSize = true;
        this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label6.Location = new System.Drawing.Point(19, 21);
        this.label6.Name = "label6";
        this.label6.Size = new System.Drawing.Size(279, 13);
        this.label6.TabIndex = 0;
        this.label6.Text = "Är trakten avstädad och är ev. oljespill sanerat?";
        // 
        // groupBox7
        // 
        this.groupBox7.Controls.Add(this.radioButton5Nej);
        this.groupBox7.Controls.Add(this.radioButton5Ja);
        this.groupBox7.Controls.Add(this.label5);
        this.groupBox7.Location = new System.Drawing.Point(8, 212);
        this.groupBox7.Name = "groupBox7";
        this.groupBox7.Size = new System.Drawing.Size(535, 45);
        this.groupBox7.TabIndex = 15;
        this.groupBox7.TabStop = false;
        this.groupBox7.Text = "5.";
        // 
        // radioButton5Nej
        // 
        this.radioButton5Nej.AutoSize = true;
        this.radioButton5Nej.Location = new System.Drawing.Point(470, 19);
        this.radioButton5Nej.Name = "radioButton5Nej";
        this.radioButton5Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton5Nej.TabIndex = 15;
        this.radioButton5Nej.TabStop = true;
        this.radioButton5Nej.Text = "Nej";
        this.radioButton5Nej.UseVisualStyleBackColor = true;
        this.radioButton5Nej.CheckedChanged += new System.EventHandler(this.radioButton5Nej_CheckedChanged);
        // 
        // radioButton5Ja
        // 
        this.radioButton5Ja.AutoSize = true;
        this.radioButton5Ja.Location = new System.Drawing.Point(420, 19);
        this.radioButton5Ja.Name = "radioButton5Ja";
        this.radioButton5Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton5Ja.TabIndex = 14;
        this.radioButton5Ja.TabStop = true;
        this.radioButton5Ja.Text = "Ja";
        this.radioButton5Ja.UseVisualStyleBackColor = true;
        this.radioButton5Ja.CheckedChanged += new System.EventHandler(this.radioButton5Ja_CheckedChanged);
        // 
        // label5
        // 
        this.label5.AutoSize = true;
        this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label5.Location = new System.Drawing.Point(19, 21);
        this.label5.Name = "label5";
        this.label5.Size = new System.Drawing.Size(270, 13);
        this.label5.TabIndex = 0;
        this.label5.Text = "Har skador undvikits på högstubbar och lågor?";
        // 
        // groupBox6
        // 
        this.groupBox6.Controls.Add(this.radioButton4Nej);
        this.groupBox6.Controls.Add(this.radioButton4Ja);
        this.groupBox6.Controls.Add(this.label4);
        this.groupBox6.Location = new System.Drawing.Point(8, 171);
        this.groupBox6.Name = "groupBox6";
        this.groupBox6.Size = new System.Drawing.Size(535, 45);
        this.groupBox6.TabIndex = 14;
        this.groupBox6.TabStop = false;
        this.groupBox6.Text = "4.";
        // 
        // radioButton4Nej
        // 
        this.radioButton4Nej.AutoSize = true;
        this.radioButton4Nej.Location = new System.Drawing.Point(470, 19);
        this.radioButton4Nej.Name = "radioButton4Nej";
        this.radioButton4Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton4Nej.TabIndex = 13;
        this.radioButton4Nej.TabStop = true;
        this.radioButton4Nej.Text = "Nej";
        this.radioButton4Nej.UseVisualStyleBackColor = true;
        this.radioButton4Nej.CheckedChanged += new System.EventHandler(this.radioButton4Nej_CheckedChanged);
        // 
        // radioButton4Ja
        // 
        this.radioButton4Ja.AutoSize = true;
        this.radioButton4Ja.Location = new System.Drawing.Point(420, 19);
        this.radioButton4Ja.Name = "radioButton4Ja";
        this.radioButton4Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton4Ja.TabIndex = 12;
        this.radioButton4Ja.TabStop = true;
        this.radioButton4Ja.Text = "Ja";
        this.radioButton4Ja.UseVisualStyleBackColor = true;
        this.radioButton4Ja.CheckedChanged += new System.EventHandler(this.radioButton4Ja_CheckedChanged);
        // 
        // label4
        // 
        this.label4.AutoSize = true;
        this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label4.Location = new System.Drawing.Point(19, 21);
        this.label4.Name = "label4";
        this.label4.Size = new System.Drawing.Size(332, 13);
        this.label4.TabIndex = 0;
        this.label4.Text = "Har skador undvikits på kulturminnen och fornlämningar?";
        // 
        // groupBox5
        // 
        this.groupBox5.Controls.Add(this.radioButton3Nej);
        this.groupBox5.Controls.Add(this.radioButton3Ja);
        this.groupBox5.Controls.Add(this.label3);
        this.groupBox5.Location = new System.Drawing.Point(8, 129);
        this.groupBox5.Name = "groupBox5";
        this.groupBox5.Size = new System.Drawing.Size(535, 45);
        this.groupBox5.TabIndex = 13;
        this.groupBox5.TabStop = false;
        this.groupBox5.Text = "3.";
        // 
        // radioButton3Nej
        // 
        this.radioButton3Nej.AutoSize = true;
        this.radioButton3Nej.Location = new System.Drawing.Point(470, 19);
        this.radioButton3Nej.Name = "radioButton3Nej";
        this.radioButton3Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton3Nej.TabIndex = 11;
        this.radioButton3Nej.TabStop = true;
        this.radioButton3Nej.Text = "Nej";
        this.radioButton3Nej.UseVisualStyleBackColor = true;
        this.radioButton3Nej.CheckedChanged += new System.EventHandler(this.radioButton3Nej_CheckedChanged);
        // 
        // radioButton3Ja
        // 
        this.radioButton3Ja.AutoSize = true;
        this.radioButton3Ja.Location = new System.Drawing.Point(420, 19);
        this.radioButton3Ja.Name = "radioButton3Ja";
        this.radioButton3Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton3Ja.TabIndex = 10;
        this.radioButton3Ja.TabStop = true;
        this.radioButton3Ja.Text = "Ja";
        this.radioButton3Ja.UseVisualStyleBackColor = true;
        this.radioButton3Ja.CheckedChanged += new System.EventHandler(this.radioButton3Ja_CheckedChanged);
        // 
        // label3
        // 
        this.label3.AutoSize = true;
        this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label3.Location = new System.Drawing.Point(20, 21);
        this.label3.Name = "label3";
        this.label3.Size = new System.Drawing.Size(217, 13);
        this.label3.TabIndex = 0;
        this.label3.Text = "Har skador undvikits i vattenmiljöer?";
        // 
        // groupBox3
        // 
        this.groupBox3.Controls.Add(this.radioButton1Nej);
        this.groupBox3.Controls.Add(this.radioButton1Ja);
        this.groupBox3.Controls.Add(this.label1);
        this.groupBox3.Location = new System.Drawing.Point(8, 29);
        this.groupBox3.Name = "groupBox3";
        this.groupBox3.Size = new System.Drawing.Size(535, 48);
        this.groupBox3.TabIndex = 11;
        this.groupBox3.TabStop = false;
        this.groupBox3.Text = "1.";
        // 
        // radioButton1Nej
        // 
        this.radioButton1Nej.AutoSize = true;
        this.radioButton1Nej.Location = new System.Drawing.Point(470, 17);
        this.radioButton1Nej.Name = "radioButton1Nej";
        this.radioButton1Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton1Nej.TabIndex = 7;
        this.radioButton1Nej.TabStop = true;
        this.radioButton1Nej.Text = "Nej";
        this.radioButton1Nej.UseVisualStyleBackColor = true;
        this.radioButton1Nej.CheckedChanged += new System.EventHandler(this.radioButton1Nej_CheckedChanged);
        // 
        // radioButton1Ja
        // 
        this.radioButton1Ja.AutoSize = true;
        this.radioButton1Ja.Location = new System.Drawing.Point(420, 17);
        this.radioButton1Ja.Name = "radioButton1Ja";
        this.radioButton1Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton1Ja.TabIndex = 6;
        this.radioButton1Ja.TabStop = true;
        this.radioButton1Ja.Text = "Ja";
        this.radioButton1Ja.UseVisualStyleBackColor = true;
        this.radioButton1Ja.CheckedChanged += new System.EventHandler(this.radioButton1Ja_CheckedChanged);
        // 
        // label1
        // 
        this.label1.AutoSize = true;
        this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label1.Location = new System.Drawing.Point(19, 19);
        this.label1.Name = "label1";
        this.label1.Size = new System.Drawing.Size(137, 13);
        this.label1.TabIndex = 0;
        this.label1.Text = "Är traktdirektiv följda?";
        // 
        // groupBox4
        // 
        this.groupBox4.Controls.Add(this.radioButton2Nej);
        this.groupBox4.Controls.Add(this.radioButton2Ja);
        this.groupBox4.Controls.Add(this.label2);
        this.groupBox4.Location = new System.Drawing.Point(8, 89);
        this.groupBox4.Name = "groupBox4";
        this.groupBox4.Size = new System.Drawing.Size(535, 45);
        this.groupBox4.TabIndex = 12;
        this.groupBox4.TabStop = false;
        this.groupBox4.Text = "2.";
        // 
        // radioButton2Nej
        // 
        this.radioButton2Nej.AutoSize = true;
        this.radioButton2Nej.Location = new System.Drawing.Point(470, 17);
        this.radioButton2Nej.Name = "radioButton2Nej";
        this.radioButton2Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton2Nej.TabIndex = 9;
        this.radioButton2Nej.TabStop = true;
        this.radioButton2Nej.Text = "Nej";
        this.radioButton2Nej.UseVisualStyleBackColor = true;
        this.radioButton2Nej.CheckedChanged += new System.EventHandler(this.radioButton2Nej_CheckedChanged);
        // 
        // radioButton2Ja
        // 
        this.radioButton2Ja.AutoSize = true;
        this.radioButton2Ja.Location = new System.Drawing.Point(420, 17);
        this.radioButton2Ja.Name = "radioButton2Ja";
        this.radioButton2Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton2Ja.TabIndex = 8;
        this.radioButton2Ja.TabStop = true;
        this.radioButton2Ja.Text = "Ja";
        this.radioButton2Ja.UseVisualStyleBackColor = true;
        this.radioButton2Ja.CheckedChanged += new System.EventHandler(this.radioButton2Ja_CheckedChanged);
        // 
        // label2
        // 
        this.label2.AutoSize = true;
        this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label2.Location = new System.Drawing.Point(20, 19);
        this.label2.Name = "label2";
        this.label2.Size = new System.Drawing.Size(291, 13);
        this.label2.TabIndex = 0;
        this.label2.Text = "Har skador undvikits i hänsynskrävande biotoper?";
        // 
        // environmentMarkberedning
        // 
        designerSettings3.ApplicationConnection = null;
        designerSettings3.DefaultFont = new System.Drawing.Font("Arial", 10F);
        designerSettings3.Icon = ((System.Drawing.Icon)(resources.GetObject("designerSettings3.Icon")));
        designerSettings3.Restrictions = designerRestrictions3;
        designerSettings3.Text = "";
        this.environmentMarkberedning.DesignerSettings = designerSettings3;
        emailSettings3.Address = "";
        emailSettings3.Host = "";
        emailSettings3.MessageTemplate = "";
        emailSettings3.Name = "";
        emailSettings3.Password = "";
        emailSettings3.Port = 49;
        emailSettings3.UserName = "";
        this.environmentMarkberedning.EmailSettings = emailSettings3;
        previewSettings3.Buttons = ((FastReport.PreviewButtons)((((((FastReport.PreviewButtons.Print | FastReport.PreviewButtons.Save)
                    | FastReport.PreviewButtons.Find)
                    | FastReport.PreviewButtons.Zoom)
                    | FastReport.PreviewButtons.Navigator)
                    | FastReport.PreviewButtons.Close)));
        previewSettings3.Icon = ((System.Drawing.Icon)(resources.GetObject("previewSettings3.Icon")));
        previewSettings3.Text = "";
        this.environmentMarkberedning.PreviewSettings = previewSettings3;
        this.environmentMarkberedning.ReportSettings = reportSettings3;
        this.environmentMarkberedning.UIStyle = FastReport.Utils.UIStyle.Office2007Black;
        // 
        // labelTotaltAntalYtor
        // 
        this.labelTotaltAntalYtor.AutoSize = true;
        this.labelTotaltAntalYtor.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelTotaltAntalYtor.Location = new System.Drawing.Point(1, 610);
        this.labelTotaltAntalYtor.Name = "labelTotaltAntalYtor";
        this.labelTotaltAntalYtor.Size = new System.Drawing.Size(113, 13);
        this.labelTotaltAntalYtor.TabIndex = 2;
        this.labelTotaltAntalYtor.Text = "Totalt antal ytor: 0";
        // 
        // labelProgress
        // 
        this.labelProgress.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelProgress.Location = new System.Drawing.Point(489, 610);
        this.labelProgress.Name = "labelProgress";
        this.labelProgress.Size = new System.Drawing.Size(69, 13);
        this.labelProgress.TabIndex = 4;
        this.labelProgress.Text = "0% klar";
        this.labelProgress.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
        // 
        // progressBarData
        // 
        this.progressBarData.ForeColor = System.Drawing.SystemColors.Desktop;
        this.progressBarData.Location = new System.Drawing.Point(4, 628);
        this.progressBarData.Maximum = 22;
        this.progressBarData.Name = "progressBarData";
        this.progressBarData.Size = new System.Drawing.Size(553, 20);
        this.progressBarData.Step = 1;
        this.progressBarData.TabIndex = 3;
        // 
        // textBoxRegion
        // 
        this.textBoxRegion.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetMarkberedning, "UserData.Region", true));
        this.textBoxRegion.Location = new System.Drawing.Point(140, 29);
        this.textBoxRegion.MaxLength = 31;
        this.textBoxRegion.Name = "textBoxRegion";
        this.textBoxRegion.ReadOnly = true;
        this.textBoxRegion.Size = new System.Drawing.Size(116, 21);
        this.textBoxRegion.TabIndex = 10;
        this.textBoxRegion.Visible = false;
        // 
        // textBoxDistrikt
        // 
        this.textBoxDistrikt.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetMarkberedning, "UserData.Distrikt", true));
        this.textBoxDistrikt.Location = new System.Drawing.Point(267, 29);
        this.textBoxDistrikt.MaxLength = 31;
        this.textBoxDistrikt.Name = "textBoxDistrikt";
        this.textBoxDistrikt.ReadOnly = true;
        this.textBoxDistrikt.Size = new System.Drawing.Size(116, 21);
        this.textBoxDistrikt.TabIndex = 11;
        this.textBoxDistrikt.Visible = false;
        // 
        // MarkberedningForm
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.AutoScroll = true;
        this.AutoSize = true;
        this.ClientSize = new System.Drawing.Size(564, 648);
        this.Controls.Add(this.labelTotaltAntalYtor);
        this.Controls.Add(this.labelProgress);
        this.Controls.Add(this.progressBarData);
        this.Controls.Add(this.tabControl1);
        this.Controls.Add(this.menuStripMarkberedning);
        this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
        this.MainMenuStrip = this.menuStripMarkberedning;
        this.MaximizeBox = false;
        this.MaximumSize = new System.Drawing.Size(622, 690);
        this.MinimizeBox = false;
        this.Name = "MarkberedningForm";
        this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        this.Text = "KORUS Markberedning";
        this.Load += new System.EventHandler(this.Markberedning_Load);
        this.Shown += new System.EventHandler(this.MarkberedningForm_Shown);
        this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Markberedning_FormClosing);
        ((System.ComponentModel.ISupportInitialize)(this.dataSetMarkberedning)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableYta)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableUserData)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFragor)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetSettings)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableRegion)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableDistrikt)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableMarkberedningsmetod)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableUrsprung)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableInvTyp)).EndInit();
        this.menuStripMarkberedning.ResumeLayout(false);
        this.menuStripMarkberedning.PerformLayout();
        ((System.ComponentModel.ISupportInitialize)(this.reportMarkberedning)).EndInit();
        this.tabControl1.ResumeLayout(false);
        this.tabPageMarkberedning.ResumeLayout(false);
        this.groupBoxFörSådd.ResumeLayout(false);
        this.groupBoxFörSådd.PerformLayout();
        this.groupBox12.ResumeLayout(false);
        this.groupBox12.PerformLayout();
        this.groupBox11.ResumeLayout(false);
        this.groupBox11.PerformLayout();
        this.groupBoxGIS.ResumeLayout(false);
        this.groupBoxGIS.PerformLayout();
        this.groupBoxPlantering.ResumeLayout(false);
        this.groupBoxPlantering.PerformLayout();
        this.groupBox2.ResumeLayout(false);
        this.groupBox2.PerformLayout();
        this.groupBoxTrakt.ResumeLayout(false);
        this.groupBoxTrakt.PerformLayout();
        this.groupBoxRegion.ResumeLayout(false);
        this.groupBoxRegion.PerformLayout();
        this.tabPageYtor.ResumeLayout(false);
        this.groupBoxMarkberedningsstallen.ResumeLayout(false);
        this.groupBoxBra.ResumeLayout(false);
        this.groupBoxBra.PerformLayout();
        this.groupBoxBlekjordsflack.ResumeLayout(false);
        this.groupBoxBlekjordsflack.PerformLayout();
        ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMarkberedningsstallen)).EndInit();
        this.groupBoxMedelvarde.ResumeLayout(false);
        this.groupBoxMedelvarde.PerformLayout();
        this.groupBoxSumma.ResumeLayout(false);
        this.groupBoxSumma.PerformLayout();
        this.tabPageFrågor.ResumeLayout(false);
        this.tabPageFrågor.PerformLayout();
        this.groupBox1.ResumeLayout(false);
        this.groupBox9.ResumeLayout(false);
        this.groupBox9.PerformLayout();
        this.groupBox8.ResumeLayout(false);
        this.groupBox8.PerformLayout();
        this.groupBox7.ResumeLayout(false);
        this.groupBox7.PerformLayout();
        this.groupBox6.ResumeLayout(false);
        this.groupBox6.PerformLayout();
        this.groupBox5.ResumeLayout(false);
        this.groupBox5.PerformLayout();
        this.groupBox3.ResumeLayout(false);
        this.groupBox3.PerformLayout();
        this.groupBox4.ResumeLayout(false);
        this.groupBox4.PerformLayout();
        this.ResumeLayout(false);
        this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.MenuStrip menuStripMarkberedning;
    private System.Windows.Forms.ToolStripMenuItem arkivToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem sparaToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem sparaSomToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    private System.Windows.Forms.ToolStripMenuItem avslutaToolStripMenuItem;
    private System.Data.DataSet dataSetMarkberedning;
    private FastReport.Report reportMarkberedning;
    private System.Data.DataTable dataTableYta;
    private System.Data.DataTable dataTableUserData;
    private System.Data.DataColumn dataColumnYta;
    private System.Data.DataColumn dataColumnOptimalt;
    private System.Data.DataColumn dataColumnBra;
    private System.Data.DataColumn dataColumnBlekjordsflack;
    private System.Data.DataColumn dataColumnOptimaltBra;
    private System.Windows.Forms.OpenFileDialog openReportFileDialog;
    private System.Windows.Forms.TabControl tabControl1;
    private System.Windows.Forms.TabPage tabPageYtor;
    private System.Windows.Forms.TabPage tabPageFrågor;
    private System.Data.DataTable dataTableFragor;
    private System.Data.DataColumn dataColumn1;
    private System.Data.DataColumn dataColumn2;
    private System.Data.DataColumn dataColumn3;
    private System.Data.DataColumn dataColumn4;
    private System.Data.DataColumn dataColumn5;
    private System.Data.DataColumn dataColumn6;
    private System.Data.DataColumn dataColumn7;
    private System.Data.DataColumn dataColumn9;
    private System.Data.DataSet dataSetSettings;
    private System.Data.DataTable dataTableRegion;
    private System.Data.DataColumn dataColumnSettingsRegionId;
    private System.Data.DataTable dataTableDistrikt;
    private System.Data.DataTable dataTableMarkberedningsmetod;
    private System.Data.DataColumn dataColumnSettingsMarkberedningsmetodNamn;
    private FastReport.EnvironmentSettings environmentMarkberedning;
    private System.Data.DataColumn dataColumnSettingsRegionNamn;
    private System.Data.DataColumn dataColumnSettingsDistriktRegionId;
    private System.Data.DataTable dataTableUrsprung;
    private System.Data.DataColumn dataColumnSettingsUrsprungNamn;
    private System.Data.DataColumn dataColumnSettingsDistriktId;
    private System.Data.DataColumn dataColumnSettingsDistriktNamn;
    private System.Data.DataColumn dataColumnRegionId;
    private System.Data.DataColumn dataColumnRegion;
    private System.Data.DataColumn dataColumnDistriktId;
    private System.Data.DataColumn dataColumnDistrikt;
    private System.Data.DataColumn dataColumnUrsprung;
    private System.Data.DataColumn dataColumnDatum;
    private System.Data.DataColumn dataColumnTraktnr;
    private System.Data.DataColumn dataColumnTraktnamn;
    private System.Data.DataColumn dataColumnStandort;
    private System.Data.DataColumn dataColumnEntreprenor;
    private System.Data.DataColumn dataColumnMarkberedningsmetod;
    private System.Data.DataColumn dataColumnGISStracka;
    private System.Data.DataColumn dataColumnGISAreal;
    private System.Data.DataColumn dataColumnKommentar;
    private System.Data.DataColumn dataColumnLanguage;
    private System.Data.DataColumn dataColumnMailFrom;
    private System.Data.DataColumn dataColumnStatus;
    private System.Data.DataColumn dataColumnStatus_Datum;
    private System.Data.DataColumn dataColumnArtal;
    private System.Windows.Forms.FolderBrowserDialog saveReportFolderBrowserDialog;
    private System.Windows.Forms.TabPage tabPageMarkberedning;
    private System.Data.DataColumn dataColumnMal;
    private System.Windows.Forms.GroupBox groupBoxRegion;
    private System.Windows.Forms.ComboBox comboBoxUrsprung;
    private System.Windows.Forms.DateTimePicker dateTimePicker;
    private System.Windows.Forms.ComboBox comboBoxDistrikt;
    private System.Windows.Forms.ComboBox comboBoxRegion;
    private System.Windows.Forms.Label labelDatum;
    private System.Windows.Forms.Label labelUrsprung;
    private System.Windows.Forms.Label labelDistrikt;
    private System.Windows.Forms.Label labelRegion;
    private System.Windows.Forms.GroupBox groupBoxTrakt;
    private System.Windows.Forms.TextBox textBoxTraktnr;
    private System.Windows.Forms.TextBox textBoxTraktnamn;
    private System.Windows.Forms.TextBox textBoxEntreprenor;
    private System.Windows.Forms.Label labeEntreprenor;
    private System.Windows.Forms.Label labelStandort;
    private System.Windows.Forms.Label labelTraktnamn;
    private System.Windows.Forms.Label labelTraktnr;
    private System.Windows.Forms.GroupBox groupBox2;
    private System.Windows.Forms.ComboBox comboBoxMarkberedningsmetod;
    private System.Windows.Forms.GroupBox groupBoxPlantering;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.TextBox textBoxMåluppfyllelse;
    private System.Windows.Forms.TextBox textBoxPlanteringAntalOptimalaBra;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.Label labelPlanteringAntalOptimalaBra;
    private System.Windows.Forms.TextBox textBoxMål;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.TextBox textBoxPlanteringAntalOptimala;
    private System.Windows.Forms.Label labelPlanteringAntalOptimala;
    private System.Windows.Forms.GroupBox groupBoxGIS;
    private System.Windows.Forms.TextBox textBoxGISData;
    private System.Windows.Forms.Label labelGISData;
    private System.Windows.Forms.TextBox textBoxGISAreal;
    private System.Windows.Forms.Label labelGISAreal;
    private System.Windows.Forms.TextBox textBoxGISStracka;
    private System.Windows.Forms.Label labelGISStracka;
    private System.Windows.Forms.GroupBox groupBoxMarkberedningsstallen;
    private System.Windows.Forms.GroupBox groupBoxBra;
    private System.Windows.Forms.GroupBox groupBoxBlekjordsflack;
    private System.Windows.Forms.Label labelBlekjordsflack;
    private System.Windows.Forms.Label labelBra;
    private System.Windows.Forms.DataGridView dataGridViewMarkberedningsstallen;
    private System.Windows.Forms.GroupBox groupBoxMedelvarde;
    private System.Windows.Forms.TextBox textBoxMedelvardeOptimaltBra;
    private System.Windows.Forms.Label labelMedelvardeOptimaltBra;
    private System.Windows.Forms.TextBox textBoxMedelvardeBlekjordsflack;
    private System.Windows.Forms.Label labelMedelvardeBlekjordsflack;
    private System.Windows.Forms.TextBox textBoxMedelvardeBra;
    private System.Windows.Forms.Label labelMedelvardeBra;
    private System.Windows.Forms.TextBox textBoxMedelvardeOptimalt;
    private System.Windows.Forms.Label labelMedelvardeOptimalt;
    private System.Windows.Forms.GroupBox groupBoxSumma;
    private System.Windows.Forms.TextBox textBoxSummaOptimalt;
    private System.Windows.Forms.TextBox textBoxSummaOptimaltBra;
    private System.Windows.Forms.Label labelSummaOptimaltBra;
    private System.Windows.Forms.TextBox textBoxSummaBlekjordsflack;
    private System.Windows.Forms.Label labelSummaBlekjordsflack;
    private System.Windows.Forms.TextBox textBoxSummaBra;
    private System.Windows.Forms.Label labelSummaBra;
    private System.Windows.Forms.Label labelSummaOptimalt;
    private System.Windows.Forms.Label labelTotaltAntalYtor;
    private System.Windows.Forms.Label labelProgress;
    private System.Windows.Forms.ProgressBar progressBarData;
    private System.Data.DataColumn dataColumnDatainsamlingsmetod;
    private System.Windows.Forms.Label label13;
    private System.Windows.Forms.ComboBox comboBoxDatainsamlingsmetod;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.GroupBox groupBox9;
    private System.Windows.Forms.RadioButton radioButton7Nej;
    private System.Windows.Forms.RadioButton radioButton7Ja;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.GroupBox groupBox8;
    private System.Windows.Forms.RadioButton radioButton6Nej;
    private System.Windows.Forms.RadioButton radioButton6Ja;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.GroupBox groupBox7;
    private System.Windows.Forms.RadioButton radioButton5Nej;
    private System.Windows.Forms.RadioButton radioButton5Ja;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.GroupBox groupBox6;
    private System.Windows.Forms.RadioButton radioButton4Nej;
    private System.Windows.Forms.RadioButton radioButton4Ja;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.GroupBox groupBox5;
    private System.Windows.Forms.RadioButton radioButton3Nej;
    private System.Windows.Forms.RadioButton radioButton3Ja;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.GroupBox groupBox3;
    private System.Windows.Forms.RadioButton radioButton1Nej;
    private System.Windows.Forms.RadioButton radioButton1Ja;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.GroupBox groupBox4;
    private System.Windows.Forms.RadioButton radioButton2Nej;
    private System.Windows.Forms.RadioButton radioButton2Ja;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.RichTextBox richTextBoxKommentar;
    private System.Windows.Forms.GroupBox groupBoxFörSådd;
    private System.Windows.Forms.GroupBox groupBox12;
    private System.Windows.Forms.TextBox textBoxContortaStambrev;
    private System.Windows.Forms.TextBox textBoxTallStambrev;
    private System.Windows.Forms.Label label16;
    private System.Windows.Forms.GroupBox groupBox11;
    private System.Windows.Forms.TextBox textBoxContortaTotGiva;
    private System.Windows.Forms.TextBox textBoxTallTotGiva;
    private System.Windows.Forms.Label label15;
    private System.Data.DataColumn dataColumn8;
    private System.Data.DataColumn dataColumn10;
    private System.Data.DataColumn dataColumn11;
    private System.Data.DataColumn dataColumn12;
    private System.Windows.Forms.DataGridViewTextBoxColumn ytaDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn optimaltDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn braDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn blekjordsflackDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn optimaltBraDataGridViewTextBoxColumn;

    private string GetDsStr(string aTable, string aColumn, int aRowIndex, DataSet aDataSet)
    {
      try
      {
        if (aDataSet != null && aDataSet.Tables[aTable] != null)
        {
          if (aDataSet.Tables[aTable].Rows.Count > aRowIndex && aDataSet.Tables[aTable].Rows[aRowIndex][aColumn] != null)
          {
            return aDataSet.Tables[aTable].Rows[aRowIndex][aColumn].ToString();
          }
        }
      }
      catch (Exception)
      {
//ignore 
      }
      return string.Empty;
    }

    private System.Windows.Forms.TextBox textBoxTraktDel;
    private System.Windows.Forms.ComboBox comboBoxInvtyp;
    private System.Windows.Forms.Label labelInvtyp;
    private DataColumn dataColumnInvTypId;
    private DataColumn dataColumnInvTyp;
    private DataTable dataTableInvTyp;
    private DataColumn dataColumnSettingsInvTypId;
    private DataColumn dataColumnSettingsInvTypNamn;
    private System.Windows.Forms.TextBox textBoxRegion;
    private System.Windows.Forms.TextBox textBoxDistrikt;
  }
}

