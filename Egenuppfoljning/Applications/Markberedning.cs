﻿#region

using System;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using Egenuppfoljning.Interfaces;
using Egenuppfoljning.Properties;
using FastReport.Export.OoXML;
using FastReport.Export.Pdf;
using Microsoft.Office.Interop.Outlook;
using Application = System.Windows.Forms.Application;
using Attachment = System.Net.Mail.Attachment;
using Exception = System.Exception;

#endregion

namespace Egenuppfoljning.Applications
{
    /// <summary>
    ///   PC Application for egenuppföljning - Markberedning.
    /// </summary>
    public partial class MarkberedningForm : Form, IKorusRapport
    {
        //Epost
        private const int CP_NOCLOSE_BUTTON = 0x200;
        private string mEpostAdress;
        private string mMeddelande;

        private string mSDefaultExcel2007Path;
        private string mSDefaultPdfPath;
        private string mSDefaultReportsPath;
        private string mSSettingsFilePath;
        private string mSSettingsPath;
        private string mTitel;

        /// <summary>
        ///   Constructor, initiate data.
        /// </summary>
        public MarkberedningForm()
        {
            //Select the language that should be used.
            Thread.CurrentThread.CurrentCulture = new CultureInfo(Settings.Default.SettingsLanguage);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(Settings.Default.SettingsLanguage);
            InitializeComponent();
        }

        public MarkberedningForm(bool aRedigeraLagratData)
        {
            //Select the language that should be used.
            Thread.CurrentThread.CurrentCulture = new CultureInfo(Settings.Default.SettingsLanguage);
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(Settings.Default.SettingsLanguage);

            InitializeComponent();

            sparaSomToolStripMenuItem.Visible = false;

            //nyckelvärden kan ej ändras.
            comboBoxInvtyp.Enabled = false;
            comboBoxRegion.Enabled = false;
            comboBoxDistrikt.Enabled = false;
            textBoxTraktnr.Enabled = false;
            //comboBoxStandort.Enabled = false;
            textBoxTraktDel.Enabled = false;
            textBoxEntreprenor.Enabled = false;
            dateTimePicker.Enabled = false;
            sparaToolStripMenuItem.Text = Resources.Lagra_andrat_data;

            RedigeraLagratData = aRedigeraLagratData;

            if (RedigeraLagratData)
            {
                comboBoxRegion.Visible = false;
                comboBoxDistrikt.Visible = false;
                textBoxRegion.Visible = true;
                textBoxDistrikt.Visible = true;
            }
            else
            {
                comboBoxRegion.Visible = true;
                comboBoxDistrikt.Visible = true;
                textBoxRegion.Visible = false;
                textBoxDistrikt.Visible = false;
            }
        }

        // Avvaktivera stäng knappen, för att få kontrollen att kunna välja att inte lagra data ifall Avsluta valet i menyn väljs

        protected override CreateParams CreateParams
        {
            get
            {
                var myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }

        #region IKorusRapport Members

        public bool RedigeraLagratData { get; private set; }

        public void LaddaForm(string aRapportSökväg)
        {
            //mSSettingsPath = Application.StartupPath + Settings.Default.SettingsPath;
            mSSettingsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + Settings.Default.SettingsPath;
            mSSettingsFilePath = mSSettingsPath + Settings.Default.SettingsFilename +
                                 "." + Settings.Default.SettingsLanguage + Resources.Xml_suffix;
            mSDefaultExcel2007Path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                                     Settings.Default.ReportsPath + Settings.Default.Excel2007Path;
            mSDefaultPdfPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) +
                               Settings.Default.ReportsPath + Settings.Default.PdfPath;
            InitFastReportMail();

            if (aRapportSökväg != null)
            {
                Settings.Default.ReportFilePath = aRapportSökväg;
                mSDefaultReportsPath = Application.StartupPath + Settings.Default.ReportsPath;
                LoadMarkberedning();
            }
            else
            {
                ReadSettingsXmlFile();
                InitData();
                InitEmptyTables();
            }
        }

        /*public string getInvNamn(int nIndex)
        {
            if (nIndex == 0)
                return "Egen";
            else
                if (nIndex == 1)
                    return "Distrikt";
                else
                    return "";
        }*/

        public void UppdateraData(KorusDataEventArgs aData)
        {
            try
            {
                dataSetMarkberedning.Clear();

                DateTime datum;
                DateTime.TryParse(aData.data.datum, out datum);

                //userdata
                dataSetMarkberedning.Tables["UserData"].Rows.Add(aData.data.regionid, aData.data.regionnamn,
                                                                 aData.data.distriktid, aData.data.distriktnamn,
                                                                 aData.data.ursprung, datum, aData.data.traktnr,
                                                                 aData.data.traktnamn, aData.data.standort,
                                                                 aData.data.entreprenor, aData.data.markberedningsmetod,
                                                                 aData.data.gisstracka, aData.data.gisareal,
                                                                 aData.data.kommentar, string.Empty, string.Empty,
                                                                 Resources.Lagrad, DateTime.MinValue, aData.data.årtal,
                                                                 aData.data.malgrundyta, aData.data.datainsamlingsmetod,
                                                                 aData.data.invtyp, DataSetParser.getInvNamn(aData.data.invtyp));

                //frågor
                dataSetMarkberedning.Tables["Fragor"].Rows.Add(aData.frågeformulär.fraga1, aData.frågeformulär.fraga2,
                                                               aData.frågeformulär.fraga3, aData.frågeformulär.fraga4,
                                                               aData.frågeformulär.fraga5, aData.frågeformulär.fraga6,
                                                               aData.frågeformulär.fraga7, aData.frågeformulär.ovrigt,
                                                               aData.frågeformulär.fraga8, aData.frågeformulär.fraga9,
                                                               aData.frågeformulär.fraga10, aData.frågeformulär.fraga11);

                //ytor
                foreach (var yta in aData.ytor)
                {
                    dataSetMarkberedning.Tables["Yta"].Rows.Add(yta.yta, yta.optimalt, yta.bra, yta.blekjordsflack,
                                                                yta.optimaltbra);
                }

                UpdateGUIData();
            }
            catch (Exception ex)
            {
#if !DEBUG
     MessageBox.Show("Misslyckades med att läsa in data", "Läsa data misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        public string GetDsStr(string aTable, string aColumn)
        {
            return GetDsStr(aTable, aColumn, 0, dataSetMarkberedning);
        }


        public int GetTotalaAntaletYtor()
        {
            if (dataSetMarkberedning != null && dataSetMarkberedning.Tables["Yta"] != null)
            {
                return dataSetMarkberedning.Tables["Yta"].Rows.Count;
            }
            return 0;
        }


        public int GetProcentStatus()
        {
            return (int)((progressBarData.Value / (double)progressBarData.Maximum) * 100);
        }

        public string GetEgenuppföljningsTyp()
        {
            return Resources.Markberedning;
        }

        public void ShowEditDialog(bool aRedigeraLagratData)
        {
            RedigeraLagratData = aRedigeraLagratData;
            ShowDialog();
        }

        /// <summary>
        ///   Writes the data that has been selected in the GUI.
        /// </summary>
        public bool WriteDataToReportXmlFile(string aFilePath, bool aShowDialog, bool aWriteSchema, string aReportStatus,
                                             string aReportStatusDatum)
        {
            XmlTextWriter objXmlReportWriter = null;
            XmlTextWriter objXmlSchemReportWriter = null;

            UpdateDecimal(textBoxGISAreal);

            if (!aWriteSchema && aFilePath != null)
            {
                //check so the correct data exsist for the report filename
                var reportName = GetReportFileName();

                var fileNameWithoutExtension = Path.GetFileNameWithoutExtension(aFilePath);
                if (fileNameWithoutExtension != null && (reportName != null && !fileNameWithoutExtension.Equals(reportName)))
                {
                    aFilePath = ShowSaveAsDialog(Settings.Default.ReportFilePath.Equals(string.Empty)
                                                   ? mSDefaultReportsPath
                                                   : Path.GetDirectoryName(Settings.Default.ReportFilePath), Resources.keg);
                }
            }

            if (aFilePath == null || aFilePath.Trim().Length <= 0)
            {
                return false;
            }

            InitEmptyTables();

            try
            {
                if (aWriteSchema)
                {
                    objXmlSchemReportWriter = new XmlTextWriter(aFilePath, null) { Formatting = Formatting.Indented };
                    objXmlSchemReportWriter.WriteStartDocument();
                    dataSetMarkberedning.WriteXmlSchema(objXmlSchemReportWriter);
                    objXmlSchemReportWriter.WriteEndDocument();
                    objXmlSchemReportWriter.Flush();
                }
                else
                {
                    UpdateDataSetStatus();

                    objXmlReportWriter = new XmlTextWriter(aFilePath, null) { Formatting = Formatting.Indented };
                    objXmlReportWriter.WriteStartDocument();
                    SetDsStr("UserData", "Language", Settings.Default.SettingsLanguage);
                    SetDsStr("UserData", "Status", aReportStatus);
                    SetDsStr("UserData", "Status_Datum", aReportStatusDatum);
                    dataSetMarkberedning.WriteXml(objXmlReportWriter);
                    objXmlReportWriter.WriteEndDocument();
                    objXmlReportWriter.Flush();
                    Settings.Default.ReportFilePath = aFilePath;

                    if (aShowDialog)
                    {
                        MessageBox.Show(String.Format(Resources.RapportenSparadMessage, Path.GetFileName(aFilePath))
                                        , Resources.RapportSparadTitel, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
#if !DEBUG
     MessageBox.Show("Skriva data till rapportfil misslyckades", "Spara misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
                return false;
            }
            finally
            {
                if (objXmlReportWriter != null)
                {
                    objXmlReportWriter.Close();
                }

                if (objXmlSchemReportWriter != null)
                {
                    objXmlSchemReportWriter.Close();
                }
            }
            return true;
        }

        public void ShowReport()
        {
            try
            {
                reportMarkberedning.Show();
            }
            catch (Exception ex)
            {
#if !DEBUG
        MessageBox.Show(Resources.ForhandgranskningFailed);
#else
                MessageBox.Show(Resources.ForhandgranskningFailed + ex);
#endif
            }
        }


        public void ExporteraExcel2007()
        {
            try
            {
                InitExportDirs();
                //kolla först så användaren har skrivit in det datat som krävs för ett giltigt filnamn.        
                reportMarkberedning.Prepare();
                var export = new Excel2007Export();
                // Visa export alternativ
                if (!export.ShowDialog()) return;

                var filePath = ShowSaveAsDialog(mSDefaultExcel2007Path, Resources.Excel_suffix);
                if (filePath == null) return;
                reportMarkberedning.Export(export, filePath);
                MessageBox.Show(Resources.ExportRapport + filePath
                                , Resources.Exportering_lyckades, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
#if !DEBUG
          MessageBox.Show("Misslyckades med att exportera ut data till formatet: Excel 2007");
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        public void ExporteraAdobePDF()
        {
            try
            {
                InitExportDirs();
                //kolla först så användaren har skrivit in det datat som krävs för ett giltigt filnamn.        
                reportMarkberedning.Prepare();
                var export = new PDFExport();
                // Visa export alternativ
                if (!export.ShowDialog()) return;

                var filePath = ShowSaveAsDialog(mSDefaultPdfPath, Resources.Pdf_suffix);
                if (filePath == null) return;
                reportMarkberedning.Export(export, filePath);
                MessageBox.Show(Resources.ExportRapport + filePath
                                , Resources.Exportering_lyckades, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
#if !DEBUG
          MessageBox.Show("Misslyckades med att exportera ut data till formatet: Pdf");
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        /// <summary>
        ///   Checks the values in the GUI and puts together a name from the data as follows: [rapport_typ]_[regionid]_[distriktid]_[traktnr]_[ståndort]_[entreprenör]_[årtal].keg
        /// </summary>
        /// <returns> The filename. </returns>
        public string GetReportFileName()
        {
            var missingData = "";

            //Rapport typ
            var filename = Resources.MBPrefix;
            var markberedningsmetod = GetDsStr("UserData", "Markberedningsmetod");
            if (markberedningsmetod.Equals(Resources.KontinuerligtHarv))
            {
                filename += Resources.FileHarv;
            }
            else if (markberedningsmetod.Equals(Resources.Hoglaggning))
            {
                filename += Resources.FileHog;
            }
            else if (markberedningsmetod.Equals(Resources.ForSadd))
            {
                filename += Resources.FileSadd;
            }
            else
            {
                missingData += " " + Resources.Markberedningsmetod;
            }
            //InvTyp
            var invtypid = GetDsStr("UserData", "InvTypId");

            if (invtypid.Equals(string.Empty) || invtypid.Equals("-1"))
            {
                missingData += " " + Resources.InvTypId;
            }
            else
            {
                filename += Resources.FileSep + invtypid;
            }

            //Region id
            var regionid = GetDsStr("UserData", "RegionId");

            if (regionid.Equals(string.Empty) || regionid.Equals("0") || regionid.Equals("-1"))
            {
                missingData += " " + Resources.RegionId;
            }
            else
            {
                filename += Resources.FileSep + regionid;
            }

            //Distrikt id
            var distriktid = GetDsStr("UserData", "DistriktId");

            if (distriktid.Equals(string.Empty) || distriktid.Equals("0") || distriktid.Equals("-1"))
            {
                missingData += " " + Resources.DistriktId;
            }
            else
            {
                filename += Resources.FileSep + distriktid;
            }

            //Artal
            DateTime date;
            DateTime.TryParse(GetDsStr("UserData", "Datum"), out date);

            if (date.Equals(DateTime.MinValue))
            {
                missingData += " " + Resources.Artal;
            }
            else
            {
                filename += Resources.FileSep + date.Year.ToString(CultureInfo.InvariantCulture);
            }

            //Traktnr
            var traktnr = GetDsStr("UserData", "Traktnr");
            if (traktnr.Equals(string.Empty) || traktnr.Equals("0") || traktnr.Equals("-1"))
            {
                missingData += " " + Resources.Traktnr;
            }
            else
            {
                filename += Resources.FileSep + traktnr;
            }

            //Ståndort
            var ståndort = GetDsStr("UserData", "Standort");
            if (ståndort.Equals(string.Empty) || ståndort.Equals("0") || ståndort.Equals("-1"))
            {
                missingData += " " + Resources.Standort;
            }
            else
            {
                filename += Resources.FileSep + ståndort;
            }


            //Entreprenörnr
            var entreprenör = GetDsStr("UserData", "Entreprenor");
            if (entreprenör.Equals(string.Empty) || entreprenör.Equals("0") || entreprenör.Equals("-1"))
            {
                missingData += " " + Resources.Entreprenor;
            }
            else
            {
                filename += Resources.FileSep + entreprenör;
            }

            if (!missingData.Equals(string.Empty))
            {
                //some data is missing to create the file name
                var errMessage = "Följande data saknas för att kunna spara rapporten: ";
                errMessage += missingData;
                errMessage += "\nFyll i dessa data och välj spara igen.";
                MessageBox.Show(errMessage, Resources.Data_saknas, MessageBoxButtons.OK, MessageBoxIcon.Stop);

                return null;
            }
            return filename;
        }

        public void SkrivUt()
        {
            try
            {
                reportMarkberedning.Print();
            }
            catch (Exception ex)
            {
#if !DEBUG
       MessageBox.Show("Skriva ut rapport misslyckades", "Skriva ut misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        public KorusDataEventArgs GetKORUSData()
        {
            UpdateDataSetStatus();
            var obj = new KorusDataEventArgs();

            //Nyckelvariabler
            int regionid, distriktid, traktnr, standort, entreprenor,invtypid;
            int.TryParse(GetDsStr("UserData", "InvTypId"), out invtypid);
            int.TryParse(GetDsStr("UserData", "RegionId"), out regionid);
            int.TryParse(GetDsStr("UserData", "DistriktId"), out distriktid);
            int.TryParse(GetDsStr("UserData", "Traktnr"), out traktnr);
            int.TryParse(GetDsStr("UserData", "Standort"), out standort);
            int.TryParse(GetDsStr("UserData", "Entreprenor"), out entreprenor);
            var årtal = GetDatumÅr(GetDsStr("UserData", "Datum"));

            //UserData
            obj.data.invtyp = invtypid;
            obj.data.regionid = regionid; //int
            obj.data.distriktid = distriktid;
            obj.data.traktnr = traktnr;
            obj.data.standort = standort;
            obj.data.entreprenor = entreprenor;
            obj.data.rapport_typ = (int)RapportTyp.Markberedning;
            obj.data.årtal = årtal;

            obj.data.traktnamn = GetDsStr("UserData", "Traktnamn");
            obj.data.regionnamn = GetDsStr("UserData", "Region");
            obj.data.distriktnamn = GetDsStr("UserData", "Distrikt");
            obj.data.ursprung = GetDsStr("UserData", "Ursprung");
            obj.data.datum = GetDsStr("UserData", "Datum");
            obj.data.markberedningsmetod = GetDsStr("UserData", "Markberedningsmetod");
            obj.data.datainsamlingsmetod = GetDsStr("UserData", "Datainsamlingsmetod");
            int.TryParse(GetDsStr("UserData", "GISStracka"), out obj.data.gisstracka);
            float.TryParse(GetDsStr("UserData", "GISAreal"), out obj.data.gisareal); //float
            obj.data.kommentar = GetDsStr("UserData", "Kommentar"); //string
            int.TryParse(GetDsStr("UserData", "Mal"), out obj.data.malgrundyta);

            obj.frågeformulär.fraga1 = GetDsStr("Fragor", "Fraga1");
            obj.frågeformulär.fraga2 = GetDsStr("Fragor", "Fraga2");
            obj.frågeformulär.fraga3 = GetDsStr("Fragor", "Fraga3");
            obj.frågeformulär.fraga4 = GetDsStr("Fragor", "Fraga4");
            obj.frågeformulär.fraga5 = GetDsStr("Fragor", "Fraga5");
            obj.frågeformulär.fraga6 = GetDsStr("Fragor", "Fraga6");
            obj.frågeformulär.fraga7 = GetDsStr("Fragor", "Fraga7");
            obj.frågeformulär.ovrigt = GetDsStr("Fragor", "Ovrigt");
            obj.frågeformulär.fraga8 = GetDsStr("Fragor", "Fraga8");
            obj.frågeformulär.fraga9 = GetDsStr("Fragor", "Fraga9");
            obj.frågeformulär.fraga10 = GetDsStr("Fragor", "Fraga10");
            obj.frågeformulär.fraga11 = GetDsStr("Fragor", "Fraga11");

            //Ytor
            if (GetNrOfRows("Yta") > 0)
            {
                for (var i = 0; i < GetNrOfRows("Yta"); i++)
                {
                    var yta = new KorusDataEventArgs.Yta();

                    int.TryParse(GetDsStr("Yta", "Yta", i, dataSetMarkberedning), out yta.yta); //löpnummer
                    int.TryParse(GetDsStr("Yta", "Optimalt", i, dataSetMarkberedning), out yta.optimalt);
                    int.TryParse(GetDsStr("Yta", "Bra", i, dataSetMarkberedning), out yta.bra);
                    int.TryParse(GetDsStr("Yta", "Blekjordsflack", i, dataSetMarkberedning), out yta.blekjordsflack);
                    int.TryParse(GetDsStr("Yta", "OptimaltBra", i, dataSetMarkberedning), out yta.optimaltbra);

                    obj.ytor.Add(yta);
                }
            }

            return obj;
        }

        public void SändEpost(string aEpostAdress, string aTitel, string aMeddelande, bool aAnvändInternEpost,
                              bool aBifogRapport)
        {
            mEpostAdress = aEpostAdress;
            mTitel = aTitel;
            mMeddelande = aMeddelande;

            if (aAnvändInternEpost)
            {
                SendInternMail(aBifogRapport);
            }
            else
            {
                SendViaOutlook(aBifogRapport);
            }
        }

        #endregion

        private int GetNrOfRows(string aTable)
        {
            try
            {
                if (dataSetMarkberedning != null && dataSetMarkberedning.Tables[aTable] != null)
                {
                    return dataSetMarkberedning.Tables[aTable].Rows.Count;
                }
            }
            catch (Exception)
            {
                //ignore 
            }
            return 0;
        }


        private void LäggTillHögstaVärdetFörYta(int aRowIndex)
        {
            try
            {
                var slNr =
                  DataSetParser.GetObjIntValue(
                    dataGridViewMarkberedningsstallen.Rows[aRowIndex].Cells[ytaDataGridViewTextBoxColumn.Name].Value);

                if (slNr >= 1 || dataGridViewMarkberedningsstallen.Rows.Count <= 0) return;

                //Yta får inte vara mindre än 1, leta upp det högsta Yta och fortsätt på det
                var maxValue = 0;
                for (var i = 0; i < dataGridViewMarkberedningsstallen.Rows.Count; i++)
                {
                    var value =
                      DataSetParser.GetObjIntValue(
                        dataGridViewMarkberedningsstallen.Rows[i].Cells[ytaDataGridViewTextBoxColumn.Name].Value);

                    if (value > maxValue)
                    {
                        maxValue = value;
                    }
                }
                //lägg på +1 och sätt in det i sista raden för slNr
                dataGridViewMarkberedningsstallen.Rows[aRowIndex].Cells[ytaDataGridViewTextBoxColumn.Name].Value = maxValue +
                                                                                                                   1;

                if (
                  !dataGridViewMarkberedningsstallen.Rows[aRowIndex].Cells[ytaDataGridViewTextBoxColumn.Name].Value.ToString().
                                                                                                              Equals(
                                                                                                                string.Empty))
                    return;

                //inget nytt yt nummer kunde anges, en bugg som kan hända i tabellkomponenten, i så fall ta bort raden.
                if (aRowIndex >= 0 && aRowIndex < (dataGridViewMarkberedningsstallen.Rows.Count - 2))
                {
                    dataGridViewMarkberedningsstallen.Rows.RemoveAt(aRowIndex);
                }
            }
            catch (Exception ex)
            {
#if !DEBUG
       MessageBox.Show("Misslyckades med att öka nummret för yta.", "Kunde inte sätta yt-numret.", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void avslutaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DataHelper.TvingaDataGridViewAttValidera(dataGridViewMarkberedningsstallen);
            DataSetParser.TabortFelaktigaYtor(dataSetMarkberedning);

            RedigeraLagratData = false; //uppdatera inte databasen
            Hide();
        }

        private void WriteSettingsXmlFile(bool aWriteSchema)
        {
            XmlTextWriter objXmlSettingsWriter = null;
            XmlTextWriter objXmlSettingsSchemaWriter = null;
            InitSettingsData();

            try
            {
                if (aWriteSchema)
                {
                    objXmlSettingsSchemaWriter = new XmlTextWriter(mSSettingsPath + Settings.Default.SettingsSchemaFilename, null) { Formatting = Formatting.Indented };
                    objXmlSettingsSchemaWriter.WriteStartDocument();
                    dataSetSettings.WriteXmlSchema(objXmlSettingsSchemaWriter);
                    objXmlSettingsSchemaWriter.WriteEndDocument();
                    objXmlSettingsSchemaWriter.Flush();
                }
                else
                {
                    objXmlSettingsWriter = new XmlTextWriter(mSSettingsFilePath, null) { Formatting = Formatting.Indented };
                    objXmlSettingsWriter.WriteStartDocument();
                    dataSetSettings.WriteXml(objXmlSettingsWriter);
                    objXmlSettingsWriter.WriteEndDocument();
                    objXmlSettingsWriter.Flush();
                }
            }
            catch (Exception ex)
            {
#if !DEBUG
       MessageBox.Show("Spara Settings data misslyckades", "Spara misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
            finally
            {
                if (objXmlSettingsWriter != null)
                {
                    objXmlSettingsWriter.Close();
                }
                if (objXmlSettingsSchemaWriter != null)
                {
                    objXmlSettingsSchemaWriter.Close();
                }
            }
        }

        /// <summary>
        ///   Read the XML file containing the settings for the Application.
        /// </summary>
        private void ReadSettingsXmlFile()
        {
            InitSettingsData();

            var objValidator = new XmlValidator(mSSettingsFilePath,
                                                mSSettingsPath + Settings.Default.SettingsSchemaFilename);

            if (objValidator.ValidateXmlFileFailed())
            {
                //something is wrong with the Settings file then write a new Settings and Schema file.
                WriteSettingsXmlFile(false);
                WriteSettingsXmlFile(true);
            }

            try
            {
                dataSetSettings.Clear();
                dataSetSettings.ReadXml(mSSettingsFilePath);
            }
            catch (Exception ex)
            {
#if !DEBUG
       MessageBox.Show("Läsa Settings data misslyckades", "Läsa misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        /// <summary>
        ///   Initializes the report data tables.
        /// </summary>
        private void InitEmptyTables()
        {
            if (dataSetMarkberedning.Tables["UserData"].Rows.Count == 0)
            {
                dataSetMarkberedning.Tables["UserData"].Rows.Add(0, string.Empty, 0, string.Empty, string.Empty, DateTime.Today,
                                                                 0, string.Empty, 0, 0, string.Empty, 0, 0.0, string.Empty,
                                                                 string.Empty, string.Empty, Resources.Obehandlad,
                                                                 DateTime.MinValue, DateTime.Today.Year, 0, string.Empty);
            }

            if (dataSetMarkberedning.Tables["Fragor"].Rows.Count == 0)
            {
                dataSetMarkberedning.Tables["Fragor"].Rows.Add(string.Empty, string.Empty, string.Empty, string.Empty,
                                                               string.Empty, string.Empty, string.Empty, string.Empty,
                                                               string.Empty, string.Empty, string.Empty, string.Empty);
            }

            if (dataSetMarkberedning.Tables["Yta"].Rows.Count == 0)
            {
                //If no rows exist then add a empty one before save.
                dataSetMarkberedning.Tables["Yta"].Rows.Add(1, 0, 0, 0, 0);
            }
            StatusFrågor();
        }

        /// <summary>
        ///   Sets a hardcoded default settingsdata.
        /// </summary>
        private void DefaultSettingsData()
        {
            dataSetSettings.Tables["InvTyp"].Rows.Add("0", "Egen");
            dataSetSettings.Tables["InvTyp"].Rows.Add("1", "Distrikt");

            dataSetSettings.Tables["Region"].Rows.Add("11", "11 - Nord");
            dataSetSettings.Tables["Region"].Rows.Add("16", "16 - Mitt");
            dataSetSettings.Tables["Region"].Rows.Add("18", "18 - Syd");

            dataSetSettings.Tables["Distrikt"].Rows.Add("11", "14", "14 - Örnsköldsvik");
            dataSetSettings.Tables["Distrikt"].Rows.Add("11", "19", "19 - Umeå");
            dataSetSettings.Tables["Distrikt"].Rows.Add("16", "12", "12 - Sveg");
            dataSetSettings.Tables["Distrikt"].Rows.Add("16", "14", "14 - Ljusdal");
            dataSetSettings.Tables["Distrikt"].Rows.Add("16", "15", "15 - Delsbo");
            dataSetSettings.Tables["Distrikt"].Rows.Add("16", "16", "16 - Hudiksvall");
            dataSetSettings.Tables["Distrikt"].Rows.Add("16", "18", "18 - Bollnäs");
            dataSetSettings.Tables["Distrikt"].Rows.Add("16", "21", "21 - Uppland");
            dataSetSettings.Tables["Distrikt"].Rows.Add("18", "10", "10 - Västerås");
            dataSetSettings.Tables["Distrikt"].Rows.Add("18", "11", "11 - Örebro");
            dataSetSettings.Tables["Distrikt"].Rows.Add("18", "12", "12 - Nyköping");
            dataSetSettings.Tables["Distrikt"].Rows.Add("18", "13", "13 - Götaland");
            dataSetSettings.Tables["Distrikt"].Rows.Add("18", "21", "21 - Egen Skog");

            /*dataSetSettings.Tables["Standort"].Rows.Add("1");
            dataSetSettings.Tables["Standort"].Rows.Add("2");
            dataSetSettings.Tables["Standort"].Rows.Add("3");
            dataSetSettings.Tables["Standort"].Rows.Add("4");
            dataSetSettings.Tables["Standort"].Rows.Add("5");
            dataSetSettings.Tables["Standort"].Rows.Add("6");*/

            dataSetSettings.Tables["Markberedningsmetod"].Rows.Add("Kontinuerligt harv");
            dataSetSettings.Tables["Markberedningsmetod"].Rows.Add("Högläggning");
            dataSetSettings.Tables["Markberedningsmetod"].Rows.Add("För sådd");

            dataSetSettings.Tables["Ursprung"].Rows.Add("Eget");
            dataSetSettings.Tables["Ursprung"].Rows.Add("Köp");
        }

        /// <summary>
        ///   Initializes the settings data tables.
        /// </summary>
        private void InitSettingsData()
        {
            if (dataSetSettings.Tables["InvTyp"].Rows.Count == 0)
            {
                dataSetSettings.Tables["InvTyp"].Rows.Add(-1, string.Empty);
            }
            if (dataSetSettings.Tables["Region"].Rows.Count == 0)
            {
                dataSetSettings.Tables["Region"].Rows.Add(0, string.Empty);
            }

            if (dataSetSettings.Tables["Distrikt"].Rows.Count == 0)
            {
                dataSetSettings.Tables["Distrikt"].Rows.Add(0, 0, string.Empty);
            }

            /*if (dataSetSettings.Tables["Standort"].Rows.Count == 0)
            {
              dataSetSettings.Tables["Standort"].Rows.Add(0);
            }*/

            if (dataSetSettings.Tables["Markberedningsmetod"].Rows.Count == 0)
            {
                dataSetSettings.Tables["Markberedningsmetod"].Rows.Add(string.Empty);
            }

            if (dataSetSettings.Tables["Ursprung"].Rows.Count == 0)
            {
                dataSetSettings.Tables["Ursprung"].Rows.Add(string.Empty);
            }
        }

        /// <summary>
        ///   first check the language in the file, if it is a new language, then give a question to restart with that file
        /// </summary>
        /// <param name="aFilePath"> File path to the report. </param>
        /// <param name="aStartup"> Flag that indicates if it is load in startup or not </param>
        /// <returns> true if Language check is ignored (same language) false if user selected cancel on the dialog. </returns>
        private static bool CheckLanguage(string aFilePath, bool aStartup)
        {
            if (!aStartup)
            {
                using (var tmpSet = new DataSet())
                {
                    tmpSet.ReadXml(aFilePath);
                    if (tmpSet.Tables["UserData"].Rows[0]["Language"] != null)
                    {
                        var language = tmpSet.Tables["UserData"].Rows[0]["Language"].ToString();
                        if (!language.Equals(Settings.Default.SettingsLanguage))
                        {
                            if (MessageBox.Show(
                              String.Format(
                                "Rapporten {0} är inställd på språket {1} ({2}). Vill du ändra till det språket och ladda rapporten?",
                                Path.GetFileName(aFilePath), language, GetLanguage(language))
                              , Resources.Andra_sprak, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                Settings.Default.ReportFilePath = aFilePath;
                                ChangeLanguage(language);
                            }
                            else
                            {
                                //User cancel, then cancel to load of the report, 
                                return false;
                            }
                        }
                    }
                }
            }
            return true;
        }

        private void UpdateGUIData()
        {
            foreach (DataGridViewRow row in dataGridViewMarkberedningsstallen.Rows)
            {
                UpdateOptimaltBra(row);
            }

            UpdateSumValues();
            UpdateMeanValues();
            StatusFrågor();

            ProgressNumberOfDataDone();
        }

        /// <summary>
        ///   Reads the active Report XML document.
        /// </summary>
        private void ReadDataFromReportXmlFile(string aFilePath, bool aStartup)
        {
            InitEmptyTables();

            if (!File.Exists(aFilePath))
            {
                //Return if the file does not exists.
                return;
            }

            if (!File.Exists(mSSettingsPath + Settings.Default.ReportSchemaMB))
            {
                //Write a new Report Schema file if it does not exist.
                WriteDataToReportXmlFile(mSSettingsPath + Settings.Default.ReportSchemaMB, false, true,
                                         Settings.Default.ReportStatus, Settings.Default.ReportStatusDatum);
            }

            var objValidator = new XmlValidator(aFilePath, mSSettingsPath + Settings.Default.ReportSchemaMB);

            if (objValidator.ValidateXmlFileFailed())
            {
                //something is wrong with the XML file.
                if (
                  MessageBox.Show(Resources.EjGiltigFil, Resources.FelaktigKegFil, MessageBoxButtons.YesNo,
                                  MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }
            }

            try
            {
                //first check the language.
                if (!CheckLanguage(aFilePath, aStartup)) return;

                dataSetMarkberedning.Clear();
                dataSetMarkberedning.ReadXml(aFilePath);
                UpdateGUIData();
                Settings.Default.ReportFilePath = aFilePath;
                Settings.Default.ReportStatus = GetDsStr("UserData", "Status");
                Settings.Default.ReportStatusDatum = GetDsStr("UserData", "Status_Datum");
            }
            catch (Exception ex)
            {
#if !DEBUG
     MessageBox.Show("Läsa data från rapport fil misslyckades", "Läsa data misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void InitExportDirs()
        {
            if (!Directory.Exists(mSDefaultExcel2007Path))
            {
                //Report dir does not exists, create it.
                Directory.CreateDirectory(mSDefaultExcel2007Path);
            }
            if (!Directory.Exists(mSDefaultPdfPath))
            {
                //Report dir does not exists, create it.
                Directory.CreateDirectory(mSDefaultPdfPath);
            }
        }

        /// <summary>
        ///   Checks so the application got the required files and directories otherwise create them.
        /// </summary>
        private void InitApplicationDirFilesStructure()
        {
            if (!Directory.Exists(mSSettingsPath))
            {
                //Settings dir does not exists, create it.
                Directory.CreateDirectory(mSSettingsPath);
            }

            if (!Directory.Exists(mSDefaultReportsPath))
            {
                //Report dir does not exists, create it.
                Directory.CreateDirectory(mSDefaultReportsPath);
            }

            if (!File.Exists(mSSettingsFilePath))
            {
                //Settings xml file does not exist, create a default file.
                DefaultSettingsData();
                WriteSettingsXmlFile(false);
            }
            if (!File.Exists(mSSettingsPath + Settings.Default.SettingsSchemaFilename))
            {
                //The schema settings file is missing, create it.
                WriteSettingsXmlFile(true);
            }

            if (!File.Exists(mSSettingsPath + Settings.Default.ReportSchemaMB))
            {
                //The schema report file is missing, create it.
                WriteDataToReportXmlFile(mSSettingsPath + Settings.Default.ReportSchemaMB, false, true,
                                         Settings.Default.ReportStatus, Settings.Default.ReportStatusDatum);
            }
        }

        private void LoadMarkberedning()
        {
            InitApplicationDirFilesStructure();
            ReadSettingsXmlFile();
            InitData();
            ReadDataFromReportXmlFile(Settings.Default.ReportFilePath, true);
        }

        /// <summary>
        ///   Called when the applications starts.
        /// </summary>
        /// <param name="sender"> Sender object </param>
        /// <param name="e"> Event arguments </param>
        private void Markberedning_Load(object sender, EventArgs e)
        {
        }


        private void UpdateDataSetStatus()
        {
            try
            {
                if (!GetDsStr("UserData", "Markberedningsmetod").Equals(Resources.ForSadd))
                {
                    SetDsStr("Fragor", "Fraga8", Resources.Noll);
                    SetDsStr("Fragor", "Fraga9", string.Empty);
                    SetDsStr("Fragor", "Fraga10", Resources.Noll);
                    SetDsStr("Fragor", "Fraga11", string.Empty);
                }
                else
                {
                    //För sådd använder inte blekjordsfläck, nollställ
                    foreach (var row in from DataRow row in dataSetMarkberedning.Tables["Yta"].Rows
                                        let state = row.RowState.ToString()
                                        where !state.Equals("Deleted")
                                        select row)
                    {
                        row["Blekjordsflack"] = 0;
                    }
                }
            }
            catch (Exception ex)
            {
#if !DEBUG
     MessageBox.Show("Misslyckades att uppdatera dataset", "Fel vid uppdatera dataset", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        /// <summary>
        ///   Adds the rows together and calculates the different sums in the table and shows it to the user.
        /// </summary>
        private void UpdateSumValues()
        {
            int sumOptimalt = 0, sumBra = 0, sumBlekjordsflack = 0, sumOptimaltBra = 0;

            try
            {
                for (var i = 0; i < AntalYtor(); i++)
                {
                    var värde = DataSetParser.GetObjIntValue(
                      dataGridViewMarkberedningsstallen.Rows[i].Cells[optimaltDataGridViewTextBoxColumn.Name].Value);
                    if (värde >= 0)
                    {
                        sumOptimalt += värde;
                    }

                    värde =
                      DataSetParser.GetObjIntValue(
                        dataGridViewMarkberedningsstallen.Rows[i].Cells[braDataGridViewTextBoxColumn.Name].Value);
                    if (värde >= 0)
                    {
                        sumBra += värde;
                    }

                    värde =
                      DataSetParser.GetObjIntValue(
                        dataGridViewMarkberedningsstallen.Rows[i].Cells[blekjordsflackDataGridViewTextBoxColumn.Name].Value);
                    if (värde >= 0)
                    {
                        sumBlekjordsflack += värde;
                    }

                    värde =
                      DataSetParser.GetObjIntValue(
                        dataGridViewMarkberedningsstallen.Rows[i].Cells[optimaltBraDataGridViewTextBoxColumn.Name].Value);
                    if (värde >= 0)
                    {
                        sumOptimaltBra += värde;
                    }
                }

                //Update the total sum textboxes
                textBoxSummaOptimalt.Text = sumOptimalt.ToString(CultureInfo.InvariantCulture);
                textBoxSummaBra.Text = sumBra.ToString(CultureInfo.InvariantCulture);
                textBoxSummaBlekjordsflack.Text = sumBlekjordsflack.ToString(CultureInfo.InvariantCulture);
                textBoxSummaOptimaltBra.Text = sumOptimaltBra.ToString(CultureInfo.InvariantCulture);

                ProgressNumberOfDataDone();
            }
            catch (Exception ex)
            {
#if !DEBUG
       MessageBox.Show("Beräkna summa värden misslyckades.", "Beräkning misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }


        /// <summary>
        ///   Updates the "Optimalt+Bra" value in the tabel.
        /// </summary>
        /// <param name="aRow"> The DataGridView row </param>
        private void UpdateOptimaltBra(DataGridViewRow aRow)
        {
            try
            {
                var valueOptimalt = DataSetParser.GetObjIntValue(aRow.Cells[optimaltDataGridViewTextBoxColumn.Name].Value, true);
                var valueBra = DataSetParser.GetObjIntValue(aRow.Cells[braDataGridViewTextBoxColumn.Name].Value, true);
                var sumOptimaltBra = valueBra + valueOptimalt;
                aRow.Cells[optimaltBraDataGridViewTextBoxColumn.Name].Value =
                  sumOptimaltBra.ToString(CultureInfo.InvariantCulture);
                if (aRow.Cells[optimaltBraDataGridViewTextBoxColumn.Name].Value != null &&
                    !aRow.Cells[optimaltBraDataGridViewTextBoxColumn.Name].Value.ToString().Equals(string.Empty) &&
                    aRow.Cells[ytaDataGridViewTextBoxColumn.Name].Value != null &&
                    aRow.Cells[ytaDataGridViewTextBoxColumn.Name].Value.ToString().Equals(string.Empty))
                {
                    dataGridViewMarkberedningsstallen.Rows.Remove(aRow);
                }
            }
            catch (Exception ex)
            {
#if !DEBUG
       MessageBox.Show("Beräkna Optimalt+Bra misslyckades", "Beräkna misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }


        /// <summary>
        ///   Gets the mean value from a sum.
        /// </summary>
        /// <param name="sumValue"> The sum value </param>
        /// <param name="aAvrunda"> </param>
        /// <returns> The mean value (Sum/total nr of rows) </returns>
        private double GetMeanValue(int sumValue, bool aAvrunda)
        {
            var meanValue = 0.0;
            try
            {
                var totalNrOfRows = AntalYtor();

                if (sumValue > 0 && totalNrOfRows > 0)
                {
                    meanValue = sumValue / (double)totalNrOfRows;
                }

                if (aAvrunda)
                {
                    //Round the value 
                    meanValue = Math.Round(meanValue, 1);
                }
            }
            catch (Exception ex)
            {
                //Math Round failed.
#if !DEBUG
        meanValue = 0.0;
#else
                MessageBox.Show(ex.ToString());
#endif
            }

            return meanValue;
        }


        /// <summary>
        ///   Updates the meanvalues in the GUI.
        /// </summary>
        private void UpdateMeanValues()
        {
            var medelVardeOptimalt = GetMeanValue(DataSetParser.GetObjIntValue(textBoxSummaOptimalt.Text), false);
            var medelVardeOptimaltBra = GetMeanValue(DataSetParser.GetObjIntValue(textBoxSummaOptimaltBra.Text), false);
            textBoxMedelvardeOptimalt.Text = Math.Round(medelVardeOptimalt, 1).ToString(CultureInfo.InvariantCulture);
            textBoxMedelvardeBlekjordsflack.Text =
              GetMeanValue(DataSetParser.GetObjIntValue(textBoxSummaBlekjordsflack.Text), true).ToString(
                CultureInfo.InvariantCulture);
            textBoxMedelvardeBra.Text =
              GetMeanValue(DataSetParser.GetObjIntValue(textBoxSummaBra.Text), true).ToString(CultureInfo.InvariantCulture);
            textBoxMedelvardeOptimaltBra.Text = Math.Round(medelVardeOptimaltBra, 1).ToString(CultureInfo.InvariantCulture);
            double antalOptimala = 0;
            double antalOptimalaBra = 0;

            if (comboBoxDatainsamlingsmetod.Text.Equals("Sträckmätning (40 m)"))
            {
                var sträcka = DataSetParser.GetObjIntValue(textBoxGISStracka.Text);
                double areal = DataSetParser.GetObjFloatValue(textBoxGISAreal.Text);
                if (sträcka > 0 && areal > 0)
                {
                    antalOptimala = ((sträcka / areal) / 40) * medelVardeOptimalt;
                    antalOptimalaBra = ((sträcka / areal) / 40) * medelVardeOptimaltBra;
                }
            }
            else
            {
                //Cirkelyta (100 m2)
                antalOptimala = (medelVardeOptimalt * 100);
                antalOptimalaBra = (medelVardeOptimaltBra * 100);
            }

            textBoxPlanteringAntalOptimala.Text = Math.Round(antalOptimala, 1).ToString(CultureInfo.InvariantCulture);
            textBoxPlanteringAntalOptimalaBra.Text = Math.Round(antalOptimalaBra, 1).ToString(CultureInfo.InvariantCulture);

            var mål = DataSetParser.GetObjIntValue(textBoxMål.Text);
            if ((antalOptimalaBra) > 0 && mål > 0)
            {
                textBoxMåluppfyllelse.Text = Math.Round((((antalOptimalaBra) / mål) * 100), 1)
                                                 .ToString(CultureInfo.InvariantCulture);
            }
            else
            {
                textBoxMåluppfyllelse.Text = Resources.Noll;
            }
        }


        /// <summary>
        ///   Updates the GUI Gis data.
        /// </summary>
        private void UpdateGisData()
        {
            var gisAreal = -1.0f;

            var gisStracka = DataSetParser.GetObjIntValue(textBoxGISStracka.Text);
            var tmp = 0;

            try
            {
                if (!string.IsNullOrEmpty(textBoxGISAreal.Text))
                {
                    if (textBoxGISAreal.Text.Contains('.'))
                    {
                        tmp = textBoxGISAreal.SelectionStart;
                        textBoxGISAreal.Text = textBoxGISAreal.Text.Replace('.', ',');
                        textBoxGISAreal.SelectionStart = tmp;
                    }

                    gisAreal = DataSetParser.GetObjFloatValue(textBoxGISAreal.Text);
                }
            }
            catch (FormatException ex)
            {
                //No numerical float value  
#if DEBUG
                MessageBox.Show(ex.ToString());
#endif
                textBoxGISAreal.Text = Resources.Noll;
            }

            if (gisStracka > 0 && gisAreal > 0)
            {
                var gisData = (float)Math.Round((gisStracka / gisAreal), 1);
                textBoxGISData.Text = gisData.ToString(CultureInfo.InvariantCulture);
            }
            else
            {
                textBoxGISData.Text = Resources.Noll;
            }
        }

        private static bool CheckaPositivtDecimaltal(Control aTextBox)
        {
            var rValue = false;
            if (aTextBox != null && !aTextBox.Text.Equals(string.Empty))
            {
                if (aTextBox.Focused && DataSetParser.GetObjFloatValue(aTextBox.Text) < 0)
                {
                    MessageBox.Show(Resources.Ogiltigt_nummer, Resources.Ogiltigt_nummer, MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                    aTextBox.Text = string.Empty;
                }
                else
                {
                    rValue = true;
                }
            }
            return rValue;
        }

        private void textBoxGISStracka_TextChanged(object sender, EventArgs e)
        {
            var box = sender as TextBox;
            if (box == null) return;
            if (!CheckaPositivtHeltal(box)) return;
            SetDsStr("UserData", "GISStracka", textBoxGISStracka.Text);
            UpdateGisData();
            UpdateMeanValues();
            ProgressFocusedObject(ref sender);
        }

        private void textBoxGISAreal_TextChanged(object sender, EventArgs e)
        {
            var box = sender as TextBox;
            if (box == null) return;
            if (!CheckaPositivtDecimaltal(box)) return;
            SetDsStr("UserData", "GISAreal", box.Text);
            UpdateGisData();
            UpdateMeanValues();
            ProgressFocusedObject(ref sender);
        }
        private void textBoxGISAreal_Validated(object sender, EventArgs e)
        {
            UpdateDecimal(sender);
        }
        private void textBoxGISAreal_KeyDown(object sender, KeyEventArgs e)
        {
            DataHelper.AccepteraEndastHeltalSomKeyDown(e,true);
        }
        private void textBoxGISAreal_Leave(object sender, EventArgs e)
        {
            var box = sender as TextBox;
            if (box != null && box.Text.Trim().Equals(string.Empty))
            {
                box.Text = Resources.Noll;
            }
        }



        private void sparaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (RedigeraLagratData)
            {
                UpdateDecimal(textBoxGISAreal);
            }
            DataHelper.TvingaDataGridViewAttValidera(dataGridViewMarkberedningsstallen);
            DataSetParser.TabortFelaktigaYtor(dataSetMarkberedning);

            if (RedigeraLagratData)
            {
                Hide();
            }
            else
            {
                WriteDataToReportXmlFile(Settings.Default.ReportFilePath, true, false, Resources.Obehandlad,
                                         DateTime.MinValue.ToString(CultureInfo.InvariantCulture));
            }
        }

        /// <summary>
        ///   Shows the save as dialog to let the user be able to save the report with a custom name.
        /// </summary>
        /// <returns> The file path name. </returns>
        private string ShowSaveAsDialog(string aSelectedPath, string aFileSuffix)
        {
            // folderBrowserSaveReportDialog.RootFolder = Environment.SpecialFolder.MyComputer;

            saveReportFolderBrowserDialog.SelectedPath = aSelectedPath;

            var fileName = GetReportFileName();

            if (fileName != null)
            {
                saveReportFolderBrowserDialog.Description = Resources.Spara_rapport + fileName + aFileSuffix;

                if (saveReportFolderBrowserDialog.ShowDialog() == DialogResult.OK)
                {
                    return saveReportFolderBrowserDialog.SelectedPath + Path.DirectorySeparatorChar + fileName + aFileSuffix;
                }
            }
            return null;
        }

        private void sparaSomToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var filePath = ShowSaveAsDialog(Settings.Default.ReportFilePath.Equals(string.Empty)
                                              ? mSDefaultReportsPath
                                              : Path.GetDirectoryName(Settings.Default.ReportFilePath), Resources.keg);
            if (File.Exists(filePath))
            {
                if (
                  MessageBox.Show(
                    Resources.Skriva_over_redan_befintlig_rapport + Path.GetFileNameWithoutExtension(filePath) +
                    Resources.Fragetecken,
                    Resources.Skriva_over_rapport, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }
            }
            DataHelper.TvingaDataGridViewAttValidera(dataGridViewMarkberedningsstallen);
            DataSetParser.TabortFelaktigaYtor(dataSetMarkberedning);

            WriteDataToReportXmlFile(filePath, true, false, Settings.Default.ReportStatus, Settings.Default.ReportStatusDatum);
        }

        private void Markberedning_FormClosing(object sender, FormClosingEventArgs e)
        {
            Settings.Default.Save();
            BindingContext[dataSetMarkberedning].EndCurrentEdit();

            e.Cancel = true;
            Hide();
        }

        private void InitData()
        {
            dataSetMarkberedning.Clear();
            dataSetMarkberedning.Tables["Yta"].Columns["Yta"].AutoIncrementStep = -1;
            dataSetMarkberedning.Tables["Yta"].Columns["Yta"].AutoIncrementSeed = -1;
            dataSetMarkberedning.Tables["Yta"].Columns["Yta"].AutoIncrementStep = 1;
            dataSetMarkberedning.Tables["Yta"].Columns["Yta"].AutoIncrementSeed = 2;
            progressBarData.Value = 0;
            labelProgress.Text = Resources.Klar;
            labelTotaltAntalYtor.Text = Resources.Totalt_antal_ytor__0;
        }

        private void dataGridViewMarkberedningsstallen_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            if (!((e.Exception) is FormatException)) return;
            var view = (DataGridView)sender;
            MessageBox.Show(Resources.EndastPositiva, Resources.FelaktigtVarde, MessageBoxButtons.OK,
                            MessageBoxIcon.Stop);
            view.CurrentCell.Value = 0;
            e.ThrowException = false;
            e.Cancel = false;
        }

        private static int GetDatumÅr(object aDatum)
        {
            if (aDatum != null && !aDatum.ToString().Equals(string.Empty))
            {
                DateTime datum;
                DateTime.TryParse(aDatum.ToString(), out datum);

                if (!datum.Equals(DateTime.MinValue))
                {
                    return datum.Year;
                }
            }
            return 0;
        }


        private void comboBoxMarkberedningsmetod_SelectedValueChanged(object sender, EventArgs e)
        {
            var selectedText = ((ComboBox)sender).Text;
            var showBlekjordsflack = !selectedText.Equals(Resources.ForSadd);
            groupBoxGIS.Visible = !selectedText.Equals(Resources.Hoglaggning);
            comboBoxDatainsamlingsmetod.Enabled = !selectedText.Equals(Resources.Hoglaggning);
            blekjordsflackDataGridViewTextBoxColumn.Visible = showBlekjordsflack;
            labelBlekjordsflack.Visible = showBlekjordsflack;
            labelBra.Visible = showBlekjordsflack;
            groupBoxBra.Visible = showBlekjordsflack;
            groupBoxBlekjordsflack.Visible = showBlekjordsflack;
            groupBoxFörSådd.Visible = !showBlekjordsflack;
            labelSummaBlekjordsflack.Visible = showBlekjordsflack;
            textBoxSummaBlekjordsflack.Visible = showBlekjordsflack;
            labelMedelvardeBlekjordsflack.Visible = showBlekjordsflack;
            textBoxMedelvardeBlekjordsflack.Visible = showBlekjordsflack;

            if (showBlekjordsflack)
            {
                labelMedelvardeOptimaltBra.Location = labelSummaOptimaltBra.Location = new Point(379, 30);
                textBoxMedelvardeOptimaltBra.Location = textBoxSummaOptimaltBra.Location = new Point(463, 27);
            }
            else
            {
                labelMedelvardeOptimaltBra.Location = labelSummaOptimaltBra.Location = new Point(226, 30);
                textBoxMedelvardeOptimaltBra.Location = textBoxSummaOptimaltBra.Location = new Point(314, 27);
            }

            if (showBlekjordsflack && dataGridViewMarkberedningsstallen.Width < 420)
            {
                dataGridViewMarkberedningsstallen.Width += blekjordsflackDataGridViewTextBoxColumn.Width;
            }
            else if (!showBlekjordsflack && dataGridViewMarkberedningsstallen.Width >= 420)
            {
                dataGridViewMarkberedningsstallen.Width -= blekjordsflackDataGridViewTextBoxColumn.Width;
            }

            if (!comboBoxMarkberedningsmetod.Text.Equals(Resources.Hoglaggning)) return;
            //Vid högläggning så kan endast cirkelyta användas
            comboBoxDatainsamlingsmetod.Text = Resources.Cirkelyta__100_m2;
            SetDsStr("UserData", "Datainsamlingsmetod", comboBoxDatainsamlingsmetod.Text);
            UpdateMeanValues();
        }

        private void SendViaOutlook(bool aBifogaRapport)
        {
            if (!CheckMailSettings(false)) return;
            //First save the report
            WriteDataToReportXmlFile(Settings.Default.ReportFilePath, false, false, Settings.Default.ReportStatus,
                                     Settings.Default.ReportStatusDatum);
            try
            {
                var oApp = new Microsoft.Office.Interop.Outlook.Application();
                var oMsg = (MailItem)oApp.CreateItem(OlItemType.olMailItem);
                var oRecip = oMsg.Recipients.Add(mEpostAdress);
                oRecip.Resolve();

                oMsg.Subject = mTitel;
                oMsg.Body = mMeddelande;

                if (aBifogaRapport)
                {
                    //Add rapport.
                    var sDisplayName = Settings.Default.MailAttachmentName;
                    var iPosition = oMsg.Body.Length + 1;
                    const int ATTACH_TYPE = (int)OlAttachmentType.olByValue;
                    oMsg.Attachments.Add(Settings.Default.ReportFilePath, ATTACH_TYPE, iPosition, sDisplayName);
                }
                // Visa meddelandet.
                // oMsg.Display(true);  //modal

                oMsg.Save();
                // ReSharper disable RedundantCast
                ((_MailItem)oMsg).Send();
                // ReSharper restore RedundantCast       
            }
            catch (Exception ex)
            {
#if !DEBUG
       MessageBox.Show("Sända rapporten via Outlook misslyckades. Kolla så epost inställningarna är korrekta.", "Sända misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private bool CheckMailSettings(bool aInternMail)
        {
            var errMessage = string.Empty;

            if (Settings.Default.MailSendFrom.Equals(string.Empty))
            {
                errMessage += Resources.DinEPostadress + " ";
            }
            if (Settings.Default.MailFullName.Equals(string.Empty))
            {
                errMessage += Resources.DittNamn + " ";
            }
            if (mEpostAdress.Equals(string.Empty))
            {
                errMessage += Resources.MottagarensEPostadress + " ";
            }
            if (mTitel.Equals(string.Empty))
            {
                errMessage += Resources.Titel + " ";
            }
            if (aInternMail)
            {
                if (Settings.Default.MailSMTP.Equals(string.Empty))
                {
                    errMessage += Resources.UtgaendeEPostSMTP + " ";
                }
            }

            if (!errMessage.Equals(string.Empty))
            {
                MessageBox.Show(
                  String.Format(
                    Resources.Foljande_epost_installningar_saknas,
                    errMessage), Resources.Epost_installningar_saknas, MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }

            return (errMessage.Equals(string.Empty));
        }

        private void SendInternMail(bool aBifogaRapport)
        {
            if (!CheckMailSettings(true)) return;
            //First save the report
            WriteDataToReportXmlFile(Settings.Default.ReportFilePath, false, false, Settings.Default.ReportStatus,
                                     Settings.Default.ReportStatusDatum);
            try
            {
                var smtpClient = new SmtpClient(Settings.Default.MailSMTP);
                var fromMail = new MailAddress(Settings.Default.MailSendFrom, Settings.Default.MailFullName,
                                               Encoding.UTF8);
                var toMail = new MailAddress(mEpostAdress);

                using (var message = new MailMessage(fromMail, toMail))
                {
                    message.Body = mMeddelande;
                    message.BodyEncoding = Encoding.UTF8;
                    message.Subject = mTitel;
                    message.SubjectEncoding = Encoding.UTF8;

                    var smtpUserInfo = new NetworkCredential(Settings.Default.MailUsername,
                                                             Settings.Default.MailPassword);
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = smtpUserInfo;
                    smtpClient.Port = Settings.Default.MailSMTPPort;
                    if (aBifogaRapport)
                    {
                        var attachment = new Attachment(Settings.Default.ReportFilePath);
                        message.Attachments.Add(attachment);
                    }
                    smtpClient.Send(message);
                    MessageBox.Show(Resources.RapportenArSand, Resources.RapportSandTitle, MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
#if !DEBUG
       MessageBox.Show("Sända rapport via intern mail misslyckades. Kolla så epost inställningarna är korrekta.", "Sända misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void InitFastReportMail()
        {
            reportMarkberedning.EmailSettings.Recipients = new[] { mEpostAdress };
            reportMarkberedning.EmailSettings.Subject = mTitel;
            reportMarkberedning.EmailSettings.Message = mMeddelande;
            environmentMarkberedning.EmailSettings.Address = Settings.Default.MailSendFrom;
            environmentMarkberedning.EmailSettings.Name = Settings.Default.MailFullName;
            environmentMarkberedning.EmailSettings.Host = Settings.Default.MailSMTP;
            environmentMarkberedning.EmailSettings.Port = Settings.Default.MailSMTPPort;
            environmentMarkberedning.EmailSettings.UserName = Settings.Default.MailUsername;
            environmentMarkberedning.EmailSettings.Password = Settings.Default.MailPassword;
        }

        /// <summary>
        ///   Changes the language and restarts the application.
        /// </summary>
        /// <param name="aLanguage"> The language </param>
        private static void ChangeLanguage(string aLanguage)
        {
            if (Settings.Default.SettingsLanguage.Equals(aLanguage)) return;

            try
            {
                Settings.Default.SettingsLanguage = aLanguage;
                Application.Restart();
            }
            catch (NotSupportedException nse)
            {
#if !DEBUG
          MessageBox.Show("Applikationen gick inte att starta om, gör detta manuellt istället.");
#else
                MessageBox.Show(nse.ToString());
#endif
            }
        }

        /// <summary>
        ///   Gets the correct language from the language id string.
        /// </summary>
        /// <param name="aLangId"> Language id string </param>
        /// <returns> The language string </returns>
        private static string GetLanguage(string aLangId)
        {
            if (aLangId.Equals(Settings.Default.SprakSv))
            {
                return Resources.Svenska;
            }
            return aLangId.Equals(Settings.Default.SprakEn) ? Resources.Engelska : "Odefinerat språk";
        }


        private void ProgressNumberOfDataDone()
        {
            var nrOfDataDone = 0;
            if (!GetDsStr("UserData", "InvTyp").Equals(string.Empty))
                nrOfDataDone++;
            if (!GetDsStr("UserData", "Region").Equals(string.Empty))
                nrOfDataDone++;
            if (!GetDsStr("UserData", "Distrikt").Equals(string.Empty))
                nrOfDataDone++;
            if (!GetDsStr("UserData", "Ursprung").Equals(string.Empty))
                nrOfDataDone++;
            if (!GetDsStr("UserData", "Traktnr").Equals(string.Empty) &&
                !GetDsStr("UserData", "Traktnr").Equals(Resources.Noll))
                nrOfDataDone++;
            if (!GetDsStr("UserData", "Traktnamn").Equals(string.Empty))
                nrOfDataDone++;
            if (!GetDsStr("UserData", "Standort").Equals(string.Empty) &&
                !GetDsStr("UserData", "Standort").Equals(Resources.Noll))
                nrOfDataDone++;
            if (!GetDsStr("UserData", "Entreprenor").Equals(string.Empty) &&
                !GetDsStr("UserData", "Entreprenor").Equals(Resources.Noll))
                nrOfDataDone++;
            if (!GetDsStr("UserData", "Markberedningsmetod").Equals(string.Empty))
                nrOfDataDone++;
            if (!GetDsStr("UserData", "Datainsamlingsmetod").Equals(string.Empty))
                nrOfDataDone++;
            if ((!GetDsStr("UserData", "GISStracka").Equals(string.Empty) &&
                 !GetDsStr("UserData", "GISStracka").Equals(Resources.Noll))
                || (GetDsStr("UserData", "Markberedningsmetod").Equals("Högläggning")))
                nrOfDataDone++;
            if ((!GetDsStr("UserData", "GISAreal").Equals(string.Empty) &&
                 !GetDsStr("UserData", "GISAreal").Equals(Resources.Noll))
                || (GetDsStr("UserData", "Markberedningsmetod").Equals("Högläggning")))
                nrOfDataDone++;
            if (!GetDsStr("UserData", "Mal").Equals(string.Empty) && !GetDsStr("UserData", "Mal").Equals(Resources.Noll))
                nrOfDataDone++;
            if (!GetDsStr("UserData", "Datainsamlingsmetod").Equals(string.Empty))
                nrOfDataDone++;

            //Natu-kulturmiljöhänsyn
            if (GetDsStr("Fragor", "Fraga1").Equals("Ja"))
            {
                progressBarData.Maximum = Settings.Default.MaxProgressMarkberedning;
                nrOfDataDone++;
            }
            else if (GetDsStr("Fragor", "Fraga1").Equals("Nej"))
            {
                //Ifall traktdirekt är nej, ökas progress med 1, och en check mot kommentar görs.
                progressBarData.Maximum = Settings.Default.MaxProgressMarkberedning + 1;
                nrOfDataDone++;

                if (!GetDsStr("UserData", "Kommentar").Equals(string.Empty))
                    nrOfDataDone++;
            }

            if (!GetDsStr("Fragor", "Fraga2").Equals(string.Empty))
                nrOfDataDone++;
            if (!GetDsStr("Fragor", "Fraga3").Equals(string.Empty))
                nrOfDataDone++;
            if (!GetDsStr("Fragor", "Fraga4").Equals(string.Empty))
                nrOfDataDone++;
            if (!GetDsStr("Fragor", "Fraga5").Equals(string.Empty))
                nrOfDataDone++;
            if (!GetDsStr("Fragor", "Fraga6").Equals(string.Empty))
                nrOfDataDone++;
            if (!GetDsStr("Fragor", "Fraga7").Equals(string.Empty))
                nrOfDataDone++;

            //Check sums if anything has been entered in the table
            if (!textBoxSummaOptimalt.Text.Equals(Resources.Noll) || !textBoxSummaBra.Text.Equals(Resources.Noll)
                || !textBoxSummaBlekjordsflack.Text.Equals(Resources.Noll) ||
                !textBoxSummaOptimaltBra.Text.Equals(Resources.Noll))
                nrOfDataDone++;

            progressBarData.Value = nrOfDataDone;

            if (progressBarData.Value > 0 && progressBarData.Maximum > 0)
            {
                labelProgress.Text = String.Format("{0}% Klar", GetProcentStatus());
            }

            if (progressBarData.Value > 0 && progressBarData.Value == progressBarData.Maximum)
            {
                progressBarData.ForeColor = Color.Green;
            }
            else
            {
                progressBarData.ForeColor = SystemColors.Desktop;
            }

            if (AntalYtor() < 2 && DataSetParser.GetObjIntValue(textBoxSummaOptimalt.Text, true) == 0
                && DataSetParser.GetObjIntValue(textBoxSummaBra.Text, true) == 0 &&
                DataSetParser.GetObjIntValue(textBoxSummaBlekjordsflack.Text, true) == 0
                && DataSetParser.GetObjIntValue(textBoxSummaOptimaltBra.Text, true) == 0)
            {
                labelTotaltAntalYtor.Text = Resources.Totalt_antal_ytor__0;
            }
            else
            {
                labelTotaltAntalYtor.Text = String.Format("Totalt antal ytor: " + AntalYtor());
            }
        }

        /// <summary>
        ///   Checks the combobox or textbox if it is focused in that case update the progressbar.
        /// </summary>
        /// <param name="aSender"> The sending object </param>
        private void ProgressFocusedObject(ref object aSender)
        {
            try
            {
                var control = aSender as Control;

                if (control != null && control.Focused)
                {
                    ProgressNumberOfDataDone();
                }
            }
            catch (Exception ex)
            {
#if DEBUG
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void UpdateDistriktList(ref object sender)
        {
            var comboBox = sender as ComboBox;
            if (comboBox == null) return;
            comboBoxDistrikt.Items.Clear();
            if (comboBox.SelectedIndex < 0) return;
            var activeRegionId = dataSetSettings.Tables["Region"].Rows[comboBox.SelectedIndex]["Id"].ToString();
            foreach (
              var row in
                dataSetSettings.Tables["Distrikt"].Rows.Cast<DataRow>().Where(
                  row => row["RegionId"].ToString().Equals(activeRegionId)))
            {
                comboBoxDistrikt.Items.Add(row["Namn"].ToString());
            }
        }


        private static bool CheckaPositivtHeltal(Control aTextBox)
        {
            var rValue = false;
            if (aTextBox != null && !aTextBox.Text.Equals(string.Empty))
            {
                if (aTextBox.Focused && DataSetParser.GetObjIntValue(aTextBox.Text) < 0)
                {
                    MessageBox.Show(Resources.Ogiltigt_nummer, Resources.Ogiltigt_nummer, MessageBoxButtons.OK,
                                    MessageBoxIcon.Information);
                    aTextBox.Text = string.Empty;
                }
                else
                {
                    rValue = true;
                }
            }
            return rValue;
        }

        private void comboBoxRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateDistriktList(ref sender);
            ProgressFocusedObject(ref sender);
        }

        private void comboBoxDistrikt_SelectedIndexChanged(object sender, EventArgs e)
        {
            ProgressFocusedObject(ref sender);
        }

        private void textBoxTraktnr_TextChanged(object sender, EventArgs e)
        {
            var box = sender as TextBox;
            if (box == null) return;
            if (CheckaPositivtHeltal(box))
            {
                ProgressFocusedObject(ref sender);
            }
        }

        private void textBoxTraktnamn_TextChanged(object sender, EventArgs e)
        {
            ProgressFocusedObject(ref sender);
        }

        /*private void comboBoxStandort_SelectedIndexChanged(object sender, EventArgs e)
        {
          ProgressFocusedObject(ref sender);
        }*/

        private void textBoxEntreprenor_TextChanged(object sender, EventArgs e)
        {
            var box = sender as TextBox;
            if (box == null) return;
            if (!CheckaPositivtHeltal(box)) return;
            ProgressFocusedObject(ref sender);
        }

        private void comboBoxMarkberedningsmetod_SelectedIndexChanged(object sender, EventArgs e)
        {
            ProgressFocusedObject(ref sender);
        }

        private void comboBoxUrsprung_SelectedIndexChanged(object sender, EventArgs e)
        {
            ProgressFocusedObject(ref sender);
        }


        /// <summary>
        ///   Check the values in Bra and Blekjordsflack so the user writes the correct data. Blekjordsfläck is a part of Bra.
        /// </summary>
        /// <param name="aCurrentCell"> </param>
        /// <param name="aCellValue"> </param>
        /// <returns> </returns>
        private bool CheckBraBlekjordsflackValues(DataGridViewCell aCurrentCell, int aCellValue)
        {
            if (aCurrentCell != null)
            {
                if (aCurrentCell.OwningColumn.Name.Equals(blekjordsflackDataGridViewTextBoxColumn.Name))
                {
                    //edit Blekjordsflack, then check the value in Bra
                    return
                      !(aCellValue <=
                        DataSetParser.GetObjIntValue(aCurrentCell.OwningRow.Cells[braDataGridViewTextBoxColumn.Name].Value, true));
                }

                if (aCurrentCell.OwningColumn.Name.Equals(braDataGridViewTextBoxColumn.Name))
                {
                    //edit Bra, then check the value in Blekjordsfläck
                    return
                      !(aCellValue >=
                        DataSetParser.GetObjIntValue(
                          aCurrentCell.OwningRow.Cells[blekjordsflackDataGridViewTextBoxColumn.Name].Value, true));
                }
            }
            return false;
        }

        private void dataGridViewMarkberedningsstallen_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            try
            {
                if (dataGridViewMarkberedningsstallen == null || e.FormattedValue == null ||
                    dataGridViewMarkberedningsstallen.Rows[e.RowIndex].Cells[e.ColumnIndex].ReadOnly) return;

                // Don't try to validate the 'new row' until finished 
                // editing since there
                // is not any point in validating its initial value.
                if (dataGridViewMarkberedningsstallen.Rows[e.RowIndex].IsNewRow)
                {
                    return;
                }
                dataGridViewMarkberedningsstallen.Rows[e.RowIndex].ErrorText = "";
                dataGridViewMarkberedningsstallen.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "";

                var cellVärde = e.FormattedValue.ToString().Trim();
                if (string.IsNullOrEmpty(cellVärde)) return;

                int cellIntValue;

                int.TryParse(cellVärde, out cellIntValue);
                if (cellIntValue < 0)
                {
                    MessageBox.Show(Resources.EndastPositiva, Resources.FelaktigtVarde, MessageBoxButtons.OK,
                                    MessageBoxIcon.Stop);
                }
                else
                {
                    if (CheckBraBlekjordsflackValues(dataGridViewMarkberedningsstallen.CurrentCell, cellIntValue))
                    {
                        MessageBox.Show(Resources.Vardet_for_blekjordsflack, Resources.FelaktigtVarde,
                                        MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    }
                }
            }
            catch (Exception ex)
            {
#if !DEBUG
       MessageBox.Show("Det gick inte att validera värdet på cellen.", "Fel vid cell validering", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void dataGridViewMarkberedningsstallen_UserDeletedRow(object sender, DataGridViewRowEventArgs e)
        {
            UpdateSumValues();
            UpdateMeanValues();
            ProgressNumberOfDataDone();
        }

        private int AntalYtor()
        {
            var antalYtor = 0;
            try
            {
                antalYtor +=
                  dataGridViewMarkberedningsstallen.Rows.Cast<DataGridViewRow>().Count(
                    row => DataSetParser.GetObjIntValue(row.Cells[ytaDataGridViewTextBoxColumn.Name].Value) >= 0);
            }
            catch (Exception ex)
            {
#if !DEBUG
       MessageBox.Show("Antal ytor misslyckades.", "Antal ytor misslyckades", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
            return antalYtor;
        }

        private void dataGridViewMarkberedningsstallen_UserDeletingRow(object sender, DataGridViewRowCancelEventArgs e)
        {
            if (AntalYtor() > 1) return;
            //Update the total sum textboxes, reset to zero
            textBoxSummaOptimalt.Text = Resources.Noll;
            textBoxSummaBra.Text = Resources.Noll;
            textBoxSummaBlekjordsflack.Text = Resources.Noll;
            textBoxSummaOptimaltBra.Text = Resources.Noll;
            UpdateMeanValues();
            ProgressNumberOfDataDone();
        }

        private void SetDsStr(string aTable, string aColumn, string aText)
        {
            if (dataSetMarkberedning == null || dataSetMarkberedning.Tables[aTable] == null) return;
            if (dataSetMarkberedning.Tables[aTable].Rows.Count <= 0 ||
                dataSetMarkberedning.Tables[aTable].Rows[0][aColumn] == null ||
                dataSetMarkberedning.Tables[aTable].Rows[0][aColumn].ToString().Equals(aText)) return;
            try
            {
                dataSetMarkberedning.Tables[aTable].Rows[0][aColumn] = aText;
            }
            catch (Exception)
            {
                //ignore            
            }
        }

        private void SetSelectedDistriktId()
        {
            int index = 0, settingsIndex = 0;
            foreach (DataRow row in dataSetSettings.Tables["Distrikt"].Rows)
            {
                if (row["RegionId"].ToString().Equals(GetDsStr("Region", "Id", comboBoxRegion.SelectedIndex, dataSetSettings)))
                {
                    if (comboBoxDistrikt.SelectedIndex == index)
                    {
                        SetDsStr("UserData", "DistriktId", GetDsStr("Distrikt", "Id", settingsIndex, dataSetSettings));
                        break;
                    }
                    index++;
                }
                settingsIndex++;
            }
        }

        private void comboBoxMarkberedningsmetod_SelectionChangeCommitted(object sender, EventArgs e)
        {
            SetDsStr("UserData", "Markberedningsmetod", comboBoxMarkberedningsmetod.Text);
        }

        private void comboBoxRegion_SelectionChangeCommitted(object sender, EventArgs e)
        {
            SetDsStr("UserData", "Region", comboBoxRegion.Text);
            var regionid = GetDsStr("Region", "Id", comboBoxRegion.SelectedIndex, dataSetSettings);
            SetDsStr("UserData", "RegionId", regionid);
        }

        private void comboBoxDistrikt_SelectionChangeCommitted(object sender, EventArgs e)
        {
            SetDsStr("UserData", "Distrikt", comboBoxDistrikt.Text);
            SetSelectedDistriktId();
        }

        private void comboBoxUrsprung_SelectionChangeCommitted(object sender, EventArgs e)
        {
            SetDsStr("UserData", "Ursprung", comboBoxUrsprung.Text);
        }

        private void dateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            SetDsStr("UserData", "Datum", dateTimePicker.Text);
            var årtal = GetDatumÅr(dateTimePicker.Text);
            SetDsStr("UserData", "Artal", årtal.ToString(CultureInfo.InvariantCulture));
        }

        /*private void comboBoxStandort_SelectionChangeCommitted(object sender, EventArgs e)
        {
          SetDsStr("UserData", "Standort", comboBoxStandort.Text);
        }*/

        private void MarkberedningForm_Shown(object sender, EventArgs e)
        {
            UpdateSumValues();
        }

        private void textBoxMål_TextChanged(object sender, EventArgs e)
        {
            var box = sender as TextBox;
            if (box == null) return;
            if (!CheckaPositivtHeltal(box)) return;
            SetDsStr("UserData", "Mal", textBoxMål.Text);
            UpdateMeanValues();
            ProgressFocusedObject(ref sender);
        }

        private void dataGridViewMarkberedningsstallen_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (dataGridViewMarkberedningsstallen == null || dataGridViewMarkberedningsstallen.CurrentCell.Value == null)
                    return;

                var cellVärde = dataGridViewMarkberedningsstallen.CurrentCell.Value.ToString().Trim();
                if (string.IsNullOrEmpty(cellVärde)) return;


                var cellValue = DataSetParser.GetObjIntValue(cellVärde);
                if ((cellValue < 0)
                    || CheckBraBlekjordsflackValues(dataGridViewMarkberedningsstallen.CurrentCell, cellValue))
                {
                    //Cell data is wrong, set it to 0          
                    dataGridViewMarkberedningsstallen.CurrentCell.Value = 0;
                }
                else
                {
                    UpdateOptimaltBra(dataGridViewMarkberedningsstallen.CurrentCell.OwningRow);
                    UpdateSumValues();
                    UpdateMeanValues();
                }
            }
            catch (Exception ex)
            {
#if !DEBUG
       MessageBox.Show("Det gick inte att kontrollera värdet på cellen.", "Fel vid cell editering", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void textBoxTraktnr_KeyDown(object sender, KeyEventArgs e)
        {
            DataHelper.AccepteraEndastHeltalSomKeyDown(e, false);
        }

        private void textBoxEntreprenor_KeyDown(object sender, KeyEventArgs e)
        {
            DataHelper.AccepteraEndastHeltalSomKeyDown(e, false);
        }

        private void textBoxMål_KeyDown(object sender, KeyEventArgs e)
        {
            DataHelper.AccepteraEndastHeltalSomKeyDown(e, false);
        }

        private void textBoxGISStracka_KeyDown(object sender, KeyEventArgs e)
        {
            DataHelper.AccepteraEndastHeltalSomKeyDown(e, false);
        }

        private void textBoxTraktnr_Leave(object sender, EventArgs e)
        {
            var box = sender as TextBox;
            if (box != null && box.Text.Trim().Equals(string.Empty))
            {
                box.Text = Resources.Noll;
            }
            SetDsStr("UserData", "Traktnr", textBoxTraktnr.Text);
        }

        private void textBoxEntreprenor_Leave(object sender, EventArgs e)
        {
            var box = sender as TextBox;
            if (box != null && box.Text.Trim().Equals(string.Empty))
            {
                box.Text = Resources.Noll;
            }
        }

        private void textBoxMål_Leave(object sender, EventArgs e)
        {
            var box = sender as TextBox;
            if (box != null && box.Text.Trim().Equals(string.Empty))
            {
                box.Text = Resources.Noll;
            }
        }

        private void textBoxGISStracka_Leave(object sender, EventArgs e)
        {
            var box = sender as TextBox;
            if (box != null && box.Text.Trim().Equals(string.Empty))
            {
                box.Text = Resources.Noll;
            }
        }

        private void comboBoxDatainsamlingsmetod_SelectedIndexChanged(object sender, EventArgs e)
        {
            ProgressFocusedObject(ref sender);
        }

        private void comboBoxDatainsamlingsmetod_SelectionChangeCommitted(object sender, EventArgs e)
        {
            SetDsStr("UserData", "Datainsamlingsmetod", comboBoxDatainsamlingsmetod.Text);
            UpdateMeanValues();
        }


        private void SättRButtonSvar(object aSender, string aFraga)
        {
            try
            {
                var rbutton = aSender as RadioButton;
                if (rbutton == null || !rbutton.Checked) return;
                SetDsStr("Fragor", aFraga, rbutton.Text);
                ProgressFocusedObject(ref aSender);
            }
            catch (Exception ex)
            {
#if !DEBUG
        MessageBox.Show("Misslyckades med att hämta svaret från radiobutton.");
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void StatusFrågor()
        {
            try
            {
                radioButton1Ja.Checked = GetDsStr("Fragor", "Fraga1").Equals("Ja");
                radioButton1Nej.Checked = GetDsStr("Fragor", "Fraga1").Equals("Nej");
                radioButton2Ja.Checked = GetDsStr("Fragor", "Fraga2").Equals("Ja");
                radioButton2Nej.Checked = GetDsStr("Fragor", "Fraga2").Equals("Nej");
                radioButton3Ja.Checked = GetDsStr("Fragor", "Fraga3").Equals("Ja");
                radioButton3Nej.Checked = GetDsStr("Fragor", "Fraga3").Equals("Nej");
                radioButton4Ja.Checked = GetDsStr("Fragor", "Fraga4").Equals("Ja");
                radioButton4Nej.Checked = GetDsStr("Fragor", "Fraga4").Equals("Nej");
                radioButton5Ja.Checked = GetDsStr("Fragor", "Fraga5").Equals("Ja");
                radioButton5Nej.Checked = GetDsStr("Fragor", "Fraga5").Equals("Nej");
                radioButton6Ja.Checked = GetDsStr("Fragor", "Fraga6").Equals("Ja");
                radioButton6Nej.Checked = GetDsStr("Fragor", "Fraga6").Equals("Nej");
                radioButton7Ja.Checked = GetDsStr("Fragor", "Fraga7").Equals("Ja");
                radioButton7Nej.Checked = GetDsStr("Fragor", "Fraga7").Equals("Nej");
            }
            catch (Exception ex)
            {
#if !DEBUG
        MessageBox.Show("Misslyckades med att uppdatera frågornas status.", "Status error", MessageBoxButtons.OK, MessageBoxIcon.Error);
#else
                MessageBox.Show(ex.ToString());
#endif
            }
        }

        private void radioButton1Ja_CheckedChanged(object sender, EventArgs e)
        {
            SättRButtonSvar(sender, "Fraga1");
        }

        private void radioButton1Nej_CheckedChanged(object sender, EventArgs e)
        {
            var button = sender as RadioButton;
            if (button == null) return;
            SättRButtonSvar(sender, "Fraga1");
            if (!button.Checked || !button.Focused) return;
            MessageBox.Show(
              Resources.Nej_har_valts_for_traktdirektiv,
              Resources.En_kommentar_kravs, MessageBoxButtons.OK, MessageBoxIcon.Information);
            ProgressNumberOfDataDone();
        }

        private void radioButton2Ja_CheckedChanged(object sender, EventArgs e)
        {
            SättRButtonSvar(sender, "Fraga2");
        }

        private void radioButton2Nej_CheckedChanged(object sender, EventArgs e)
        {
            SättRButtonSvar(sender, "Fraga2");
        }

        private void radioButton3Ja_CheckedChanged(object sender, EventArgs e)
        {
            SättRButtonSvar(sender, "Fraga3");
        }

        private void radioButton3Nej_CheckedChanged(object sender, EventArgs e)
        {
            SättRButtonSvar(sender, "Fraga3");
        }

        private void radioButton4Ja_CheckedChanged(object sender, EventArgs e)
        {
            SättRButtonSvar(sender, "Fraga4");
        }

        private void radioButton4Nej_CheckedChanged(object sender, EventArgs e)
        {
            SättRButtonSvar(sender, "Fraga4");
        }

        private void radioButton5Ja_CheckedChanged(object sender, EventArgs e)
        {
            SättRButtonSvar(sender, "Fraga5");
        }

        private void radioButton5Nej_CheckedChanged(object sender, EventArgs e)
        {
            SättRButtonSvar(sender, "Fraga5");
        }

        private void radioButton6Ja_CheckedChanged(object sender, EventArgs e)
        {
            SättRButtonSvar(sender, "Fraga6");
        }

        private void radioButton6Nej_CheckedChanged(object sender, EventArgs e)
        {
            SättRButtonSvar(sender, "Fraga6");
        }

        private void radioButton7Ja_CheckedChanged(object sender, EventArgs e)
        {
            SättRButtonSvar(sender, "Fraga7");
        }

        private void radioButton7Nej_CheckedChanged(object sender, EventArgs e)
        {
            SättRButtonSvar(sender, "Fraga7");
        }

        private void textBoxTallTotGiva_TextChanged(object sender, EventArgs e)
        {
            var box = sender as TextBox;
            if (box == null) return;
            if (CheckaPositivtDecimaltal(box))
            {
                SetDsStr("Fragor", "Fraga8", box.Text);
            }
        }

        private void textBoxTallStambrev_TextChanged(object sender, EventArgs e)
        {
            var box = sender as TextBox;
            if (box != null)
            {
                SetDsStr("Fragor", "Fraga9", box.Text);
            }
        }

        private void textBoxContortaTotGiva_TextChanged(object sender, EventArgs e)
        {
            var box = sender as TextBox;
            if (box == null) return;
            if (CheckaPositivtDecimaltal(box))
            {
                SetDsStr("Fragor", "Fraga10", box.Text);
            }
        }

        private void textBoxContortaStambrev_TextChanged(object sender, EventArgs e)
        {
            var box = sender as TextBox;
            if (box != null)
            {
                SetDsStr("Fragor", "Fraga11", box.Text);
            }
        }

        private void textBoxTallTotGiva_KeyDown(object sender, KeyEventArgs e)
        {
            DataHelper.AccepteraEndastHeltalSomKeyDown(e, false);
        }

        private void textBoxContortaTotGiva_KeyDown(object sender, KeyEventArgs e)
        {
            DataHelper.AccepteraEndastHeltalSomKeyDown(e, false);
        }

        private void textBoxTallTotGiva_Leave(object sender, EventArgs e)
        {
            var box = sender as TextBox;
            if (box != null && box.Text.Trim().Equals(string.Empty))
            {
                box.Text = Resources.Noll;
            }
            else
            {
                if (box != null)
                {
                    var d = Math.Round(DataSetParser.GetObjDecimalValue(box.Text), 1);
                    SetDsStr("Fragor", "Fraga8", d.ToString(CultureInfo.InvariantCulture));
                    box.Text = d.ToString(CultureInfo.InvariantCulture);
                }
            }
        }

        private void textBoxContortaTotGiva_Leave(object sender, EventArgs e)
        {
            var box = sender as TextBox;
            if (box != null && box.Text.Trim().Equals(string.Empty))
            {
                box.Text = Resources.Noll;
            }
            else
            {
                if (box != null)
                {
                    var d = Math.Round(DataSetParser.GetObjDecimalValue(box.Text), 1);
                    SetDsStr("Fragor", "Fraga10", d.ToString(CultureInfo.InvariantCulture));
                    box.Text = d.ToString(CultureInfo.InvariantCulture);
                }
            }
        }

        private void dataGridViewMarkberedningsstallen_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            if (e.RowIndex > 0)
            {
                LäggTillHögstaVärdetFörYta(e.RowIndex - 1);
            }
        }

        private void richTextBoxKommentar_TextChanged(object sender, EventArgs e)
        {
            var box = sender as RichTextBox;
            if (box == null) return;
            ProgressNumberOfDataDone();
        }

        private void UpdateDecimal(object sender)
        {
            var box = sender as TextBox;

            if (box == null) return;
            var d = (double)Math.Round(DataSetParser.GetObjDecimalValue(box.Text), 1);
            if (d < 0) return;
            DataSetParser.SetDsDouble("UserData", "GISAreal", d, dataSetMarkberedning);
            box.Text = d.ToString(CultureInfo.InvariantCulture);
        }

        //#4297 20150326 JÖ Traktdel
        private void textBoxTraktdel_Leave(object sender, EventArgs e)
        {
            var box = sender as TextBox;
            if (box != null && box.Text.Trim().Equals(string.Empty))
            {
                box.Text = Resources.Noll;
            }
            DataSetParser.SetDsStr("UserData", "Standort", textBoxTraktDel.Text, dataSetMarkberedning);
        }
        //#4297 20150326 JÖ Traktdel
        private void textBoxTraktdel_KeyDown(object sender, KeyEventArgs e)
        {
            DataHelper.AccepteraEndastHeltalSomKeyDown(e, false);
        }
        //#4297 20150326 JÖ Traktdel
        private void textBoxTraktdel_TextChanged(object sender, EventArgs e)
        {
            var box = sender as TextBox;
            if (box == null) return;
            if (!CheckaPositivtHeltal(box)) return;
            ProgressFocusedObject(ref sender);
        }

        private void comboBoxInvtyp_SelectedIndexChanged(object sender, EventArgs e)
        {
            ProgressFocusedObject(ref sender);
        }

        private void comboBoxInvtyp_SelectionChangeCommitted(object sender, EventArgs e)
        {
            DataSetParser.SetDsStr("UserData", "InvTyp", comboBoxInvtyp.Text, dataSetMarkberedning);
            DataSetParser.SetDsStr("UserData", "InvTypId", GetDsStr("InvTyp", "Id", comboBoxInvtyp.SelectedIndex, dataSetSettings), dataSetMarkberedning);
        }
    }
}