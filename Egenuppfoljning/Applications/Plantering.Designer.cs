﻿namespace Egenuppfoljning.Applications
{
  partial class PlanteringForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PlanteringForm));
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
        System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
        FastReport.Design.DesignerSettings designerSettings3 = new FastReport.Design.DesignerSettings();
        FastReport.Design.DesignerRestrictions designerRestrictions3 = new FastReport.Design.DesignerRestrictions();
        FastReport.Export.Email.EmailSettings emailSettings3 = new FastReport.Export.Email.EmailSettings();
        FastReport.PreviewSettings previewSettings3 = new FastReport.PreviewSettings();
        FastReport.ReportSettings reportSettings3 = new FastReport.ReportSettings();
        this.dataSetPlantering = new System.Data.DataSet();
        this.dataTableYta = new System.Data.DataTable();
        this.dataColumnYta = new System.Data.DataColumn();
        this.dataColumnOptimalt = new System.Data.DataColumn();
        this.dataColumnBra = new System.Data.DataColumn();
        this.dataColumnVaravBattre = new System.Data.DataColumn();
        this.dataColumnOvrigt = new System.Data.DataColumn();
        this.dataColumnPlanterade = new System.Data.DataColumn();
        this.dataColumnPlanteringsdjupFel = new System.Data.DataColumn();
        this.dataColumnTilltryckning = new System.Data.DataColumn();
        this.dataTableUserData = new System.Data.DataTable();
        this.dataColumnRegionId = new System.Data.DataColumn();
        this.dataColumnRegion = new System.Data.DataColumn();
        this.dataColumnDistriktId = new System.Data.DataColumn();
        this.dataColumnDistrikt = new System.Data.DataColumn();
        this.dataColumnUrsprung = new System.Data.DataColumn();
        this.dataColumnDatum = new System.Data.DataColumn();
        this.dataColumnTraktnr = new System.Data.DataColumn();
        this.dataColumnTraktnamn = new System.Data.DataColumn();
        this.dataColumnStandort = new System.Data.DataColumn();
        this.dataColumnEntreprenor = new System.Data.DataColumn();
        this.dataColumnKommentar = new System.Data.DataColumn();
        this.dataColumnLanguage = new System.Data.DataColumn();
        this.dataColumnMailFrom = new System.Data.DataColumn();
        this.dataColumnProvytestorlek = new System.Data.DataColumn();
        this.dataColumnStatus = new System.Data.DataColumn();
        this.dataColumnStatus_Datum = new System.Data.DataColumn();
        this.dataColumnArtal = new System.Data.DataColumn();
        this.dataColumnMal = new System.Data.DataColumn();
        this.dataColumnInvTypId = new System.Data.DataColumn();
        this.dataColumnInvTyp = new System.Data.DataColumn();
        this.dataColumnAtgAreal = new System.Data.DataColumn();
        this.dataTableFragor = new System.Data.DataTable();
        this.dataColumn1 = new System.Data.DataColumn();
        this.dataColumn2 = new System.Data.DataColumn();
        this.dataColumn9 = new System.Data.DataColumn();
        this.dataColumn3 = new System.Data.DataColumn();
        this.dataColumn4 = new System.Data.DataColumn();
        this.dataColumn5 = new System.Data.DataColumn();
        this.dataColumn6 = new System.Data.DataColumn();
        this.dataColumn7 = new System.Data.DataColumn();
        this.dataColumn8 = new System.Data.DataColumn();
        this.dataColumn10 = new System.Data.DataColumn();
        this.dataColumn11 = new System.Data.DataColumn();
        this.dataColumn12 = new System.Data.DataColumn();
        this.dataSetSettings = new System.Data.DataSet();
        this.dataTableRegion = new System.Data.DataTable();
        this.dataColumnSettingsRegionId = new System.Data.DataColumn();
        this.dataColumnSettingsRegionNamn = new System.Data.DataColumn();
        this.dataTableDistrikt = new System.Data.DataTable();
        this.dataColumnSettingsDistriktRegionId = new System.Data.DataColumn();
        this.dataColumnSettingsDistriktId = new System.Data.DataColumn();
        this.dataColumnSettingsDistriktNamn = new System.Data.DataColumn();
        this.dataTableMarkberedningsmetod = new System.Data.DataTable();
        this.dataColumnSettingsMarkberedningsmetodNamn = new System.Data.DataColumn();
        this.dataTableUrsprung = new System.Data.DataTable();
        this.dataColumnSettingsUrsprungNamn = new System.Data.DataColumn();
        this.dataTableInvTyp = new System.Data.DataTable();
        this.dataColumnSettingsInvTypId = new System.Data.DataColumn();
        this.dataColumnSettingsInvTypNamn = new System.Data.DataColumn();
        this.menuStripPlantering = new System.Windows.Forms.MenuStrip();
        this.arkivToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.sparaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.sparaSomToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
        this.avslutaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
        this.reportPlantering = new FastReport.Report();
        this.openReportFileDialog = new System.Windows.Forms.OpenFileDialog();
        this.tabControl1 = new System.Windows.Forms.TabControl();
        this.tabPagePlantering = new System.Windows.Forms.TabPage();
        this.groupBoxPlantering = new System.Windows.Forms.GroupBox();
        this.label3 = new System.Windows.Forms.Label();
        this.label4 = new System.Windows.Forms.Label();
        this.textBoxMåluppfyllelse = new System.Windows.Forms.TextBox();
        this.label5 = new System.Windows.Forms.Label();
        this.label6 = new System.Windows.Forms.Label();
        this.textBoxMål = new System.Windows.Forms.TextBox();
        this.label7 = new System.Windows.Forms.Label();
        this.label8 = new System.Windows.Forms.Label();
        this.label21 = new System.Windows.Forms.Label();
        this.label22 = new System.Windows.Forms.Label();
        this.textBoxUttnyttjandegrad = new System.Windows.Forms.TextBox();
        this.label24 = new System.Windows.Forms.Label();
        this.textBoxAndelMedBraDjup = new System.Windows.Forms.TextBox();
        this.textBoxUtmärktPlanteringsIndex = new System.Windows.Forms.TextBox();
        this.label23 = new System.Windows.Forms.Label();
        this.textBoxAndelMedBraTillryckning = new System.Windows.Forms.TextBox();
        this.label25 = new System.Windows.Forms.Label();
        this.label26 = new System.Windows.Forms.Label();
        this.labelGISData = new System.Windows.Forms.Label();
        this.textBoxAntalSattaPlantor = new System.Windows.Forms.TextBox();
        this.textBoxPlanteringAntalOptimalaBra = new System.Windows.Forms.TextBox();
        this.labelPlanteringAntalOptimalaBra = new System.Windows.Forms.Label();
        this.textBoxAntalOptimalaPlantor = new System.Windows.Forms.TextBox();
        this.labelPlanteringAntalOptimala = new System.Windows.Forms.Label();
        this.labelGISStracka = new System.Windows.Forms.Label();
        this.textBoxProvytestorlek = new System.Windows.Forms.TextBox();
        this.groupBoxTrakt = new System.Windows.Forms.GroupBox();
        this.textBoxTraktDel = new System.Windows.Forms.TextBox();
        this.textBoxTraktnr = new System.Windows.Forms.TextBox();
        this.textBoxTraktnamn = new System.Windows.Forms.TextBox();
        this.textBoxEntreprenor = new System.Windows.Forms.TextBox();
        this.labeEntreprenor = new System.Windows.Forms.Label();
        this.labelStandort = new System.Windows.Forms.Label();
        this.labelTraktnamn = new System.Windows.Forms.Label();
        this.labelTraktnr = new System.Windows.Forms.Label();
        this.groupBoxRegion = new System.Windows.Forms.GroupBox();
        this.textBoxDistrikt = new System.Windows.Forms.TextBox();
        this.textBoxRegion = new System.Windows.Forms.TextBox();
        this.comboBoxInvTyp = new System.Windows.Forms.ComboBox();
        this.labelInvTyp = new System.Windows.Forms.Label();
        this.textBoxAtgAreal = new System.Windows.Forms.TextBox();
        this.labelAtgAreal = new System.Windows.Forms.Label();
        this.comboBoxUrsprung = new System.Windows.Forms.ComboBox();
        this.dateTimePicker = new System.Windows.Forms.DateTimePicker();
        this.comboBoxDistrikt = new System.Windows.Forms.ComboBox();
        this.comboBoxRegion = new System.Windows.Forms.ComboBox();
        this.labelDatum = new System.Windows.Forms.Label();
        this.labelUrsprung = new System.Windows.Forms.Label();
        this.labelDistrikt = new System.Windows.Forms.Label();
        this.labelRegion = new System.Windows.Forms.Label();
        this.tabPageYtor = new System.Windows.Forms.TabPage();
        this.groupBoxPlanteringsstallen = new System.Windows.Forms.GroupBox();
        this.groupBoxMedelvarde = new System.Windows.Forms.GroupBox();
        this.textBoxMedelvardeTilltryckning = new System.Windows.Forms.TextBox();
        this.textBoxMedelvardeBra = new System.Windows.Forms.TextBox();
        this.label16 = new System.Windows.Forms.Label();
        this.labelMedelvardeBra = new System.Windows.Forms.Label();
        this.textBoxMedelvardePlanteringsdjupfel = new System.Windows.Forms.TextBox();
        this.textBoxMedelvardeOptimalt = new System.Windows.Forms.TextBox();
        this.label17 = new System.Windows.Forms.Label();
        this.labelMedelvardeOptimalt = new System.Windows.Forms.Label();
        this.label18 = new System.Windows.Forms.Label();
        this.textBoxMedelvardeVaravBattre = new System.Windows.Forms.TextBox();
        this.textBoxMedelvardePlanterade = new System.Windows.Forms.TextBox();
        this.label20 = new System.Windows.Forms.Label();
        this.textBoxMedelvardeOvrigt = new System.Windows.Forms.TextBox();
        this.label19 = new System.Windows.Forms.Label();
        this.groupBoxSumma = new System.Windows.Forms.GroupBox();
        this.textBoxSummaTilltryckning = new System.Windows.Forms.TextBox();
        this.label15 = new System.Windows.Forms.Label();
        this.textBoxSummaPlanteringsdjupfel = new System.Windows.Forms.TextBox();
        this.label14 = new System.Windows.Forms.Label();
        this.label13 = new System.Windows.Forms.Label();
        this.textBoxSummaPlanterade = new System.Windows.Forms.TextBox();
        this.textBoxSummaOptimalt = new System.Windows.Forms.TextBox();
        this.textBoxSummaOvrigt = new System.Windows.Forms.TextBox();
        this.labelSummaOptimaltBra = new System.Windows.Forms.Label();
        this.textBoxSummaVaravBattre = new System.Windows.Forms.TextBox();
        this.labelSummaBlekjordsflack = new System.Windows.Forms.Label();
        this.textBoxSummaBra = new System.Windows.Forms.TextBox();
        this.labelSummaBra = new System.Windows.Forms.Label();
        this.labelSummaOptimalt = new System.Windows.Forms.Label();
        this.dataGridViewPlanteringsstallen = new System.Windows.Forms.DataGridView();
        this.ytaDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.optimaltDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.braDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.varavBattreDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.ovrigtDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.planteradeDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.planteringsDjupFelDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.tilltryckningDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
        this.tabPageFrågor = new System.Windows.Forms.TabPage();
        this.groupBoxTrädslag = new System.Windows.Forms.GroupBox();
        this.labelContorta = new System.Windows.Forms.Label();
        this.labelTall = new System.Windows.Forms.Label();
        this.labelGran = new System.Windows.Forms.Label();
        this.groupBoxStambrev = new System.Windows.Forms.GroupBox();
        this.textBoxContortaStambrev = new System.Windows.Forms.TextBox();
        this.textBoxGranStambrev = new System.Windows.Forms.TextBox();
        this.textBoxTallStambrev = new System.Windows.Forms.TextBox();
        this.groupBoxStorlek = new System.Windows.Forms.GroupBox();
        this.comboBoxStorlekContorta = new System.Windows.Forms.ComboBox();
        this.comboBoxStorlekGran = new System.Windows.Forms.ComboBox();
        this.comboBoxStorlekTall = new System.Windows.Forms.ComboBox();
        this.groupBoxAntal = new System.Windows.Forms.GroupBox();
        this.textBoxAntalContorta = new System.Windows.Forms.TextBox();
        this.textBoxAntalGran = new System.Windows.Forms.TextBox();
        this.textBoxAntalTall = new System.Windows.Forms.TextBox();
        this.groupBox5 = new System.Windows.Forms.GroupBox();
        this.richTextBoxKommentar = new System.Windows.Forms.RichTextBox();
        this.groupBoxOBS = new System.Windows.Forms.GroupBox();
        this.label1 = new System.Windows.Forms.Label();
        this.label2 = new System.Windows.Forms.Label();
        this.groupBox4 = new System.Windows.Forms.GroupBox();
        this.radioButton2Nej = new System.Windows.Forms.RadioButton();
        this.radioButton2Ja = new System.Windows.Forms.RadioButton();
        this.label9 = new System.Windows.Forms.Label();
        this.groupBox3 = new System.Windows.Forms.GroupBox();
        this.radioButton1Nej = new System.Windows.Forms.RadioButton();
        this.radioButton1Ja = new System.Windows.Forms.RadioButton();
        this.label10 = new System.Windows.Forms.Label();
        this.environmentPlantering = new FastReport.EnvironmentSettings();
        this.saveReportFolderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
        this.labelTotaltAntalYtor = new System.Windows.Forms.Label();
        this.labelProgress = new System.Windows.Forms.Label();
        this.progressBarData = new System.Windows.Forms.ProgressBar();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetPlantering)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableYta)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableUserData)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFragor)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetSettings)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableRegion)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableDistrikt)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableMarkberedningsmetod)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableUrsprung)).BeginInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableInvTyp)).BeginInit();
        this.menuStripPlantering.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.reportPlantering)).BeginInit();
        this.tabControl1.SuspendLayout();
        this.tabPagePlantering.SuspendLayout();
        this.groupBoxPlantering.SuspendLayout();
        this.groupBoxTrakt.SuspendLayout();
        this.groupBoxRegion.SuspendLayout();
        this.tabPageYtor.SuspendLayout();
        this.groupBoxPlanteringsstallen.SuspendLayout();
        this.groupBoxMedelvarde.SuspendLayout();
        this.groupBoxSumma.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPlanteringsstallen)).BeginInit();
        this.tabPageFrågor.SuspendLayout();
        this.groupBoxTrädslag.SuspendLayout();
        this.groupBoxStambrev.SuspendLayout();
        this.groupBoxStorlek.SuspendLayout();
        this.groupBoxAntal.SuspendLayout();
        this.groupBox5.SuspendLayout();
        this.groupBoxOBS.SuspendLayout();
        this.groupBox4.SuspendLayout();
        this.groupBox3.SuspendLayout();
        this.SuspendLayout();
        // 
        // dataSetPlantering
        // 
        this.dataSetPlantering.DataSetName = "DataSetPlantering";
        this.dataSetPlantering.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableYta,
            this.dataTableUserData,
            this.dataTableFragor});
        // 
        // dataTableYta
        // 
        this.dataTableYta.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnYta,
            this.dataColumnOptimalt,
            this.dataColumnBra,
            this.dataColumnVaravBattre,
            this.dataColumnOvrigt,
            this.dataColumnPlanterade,
            this.dataColumnPlanteringsdjupFel,
            this.dataColumnTilltryckning});
        this.dataTableYta.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Yta"}, false)});
        this.dataTableYta.TableName = "Yta";
        // 
        // dataColumnYta
        // 
        this.dataColumnYta.AutoIncrementSeed = ((long)(2));
        this.dataColumnYta.ColumnName = "Yta";
        this.dataColumnYta.DataType = typeof(int);
        this.dataColumnYta.ReadOnly = true;
        // 
        // dataColumnOptimalt
        // 
        this.dataColumnOptimalt.ColumnName = "Optimalt";
        this.dataColumnOptimalt.DataType = typeof(int);
        // 
        // dataColumnBra
        // 
        this.dataColumnBra.ColumnName = "Bra";
        this.dataColumnBra.DataType = typeof(int);
        // 
        // dataColumnVaravBattre
        // 
        this.dataColumnVaravBattre.ColumnName = "VaravBattre";
        this.dataColumnVaravBattre.DataType = typeof(int);
        // 
        // dataColumnOvrigt
        // 
        this.dataColumnOvrigt.ColumnName = "Ovrigt";
        this.dataColumnOvrigt.DataType = typeof(int);
        // 
        // dataColumnPlanterade
        // 
        this.dataColumnPlanterade.ColumnName = "Planterade";
        this.dataColumnPlanterade.DataType = typeof(int);
        // 
        // dataColumnPlanteringsdjupFel
        // 
        this.dataColumnPlanteringsdjupFel.ColumnName = "PlanteringsdjupFel";
        this.dataColumnPlanteringsdjupFel.DataType = typeof(int);
        // 
        // dataColumnTilltryckning
        // 
        this.dataColumnTilltryckning.ColumnName = "Tilltryckning";
        this.dataColumnTilltryckning.DataType = typeof(int);
        // 
        // dataTableUserData
        // 
        this.dataTableUserData.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnRegionId,
            this.dataColumnRegion,
            this.dataColumnDistriktId,
            this.dataColumnDistrikt,
            this.dataColumnUrsprung,
            this.dataColumnDatum,
            this.dataColumnTraktnr,
            this.dataColumnTraktnamn,
            this.dataColumnStandort,
            this.dataColumnEntreprenor,
            this.dataColumnKommentar,
            this.dataColumnLanguage,
            this.dataColumnMailFrom,
            this.dataColumnProvytestorlek,
            this.dataColumnStatus,
            this.dataColumnStatus_Datum,
            this.dataColumnArtal,
            this.dataColumnMal,
            this.dataColumnInvTypId,
            this.dataColumnInvTyp,
            this.dataColumnAtgAreal});
        this.dataTableUserData.TableName = "UserData";
        // 
        // dataColumnRegionId
        // 
        this.dataColumnRegionId.ColumnName = "RegionId";
        this.dataColumnRegionId.DataType = typeof(int);
        // 
        // dataColumnRegion
        // 
        this.dataColumnRegion.ColumnName = "Region";
        // 
        // dataColumnDistriktId
        // 
        this.dataColumnDistriktId.ColumnName = "DistriktId";
        this.dataColumnDistriktId.DataType = typeof(int);
        // 
        // dataColumnDistrikt
        // 
        this.dataColumnDistrikt.ColumnName = "Distrikt";
        // 
        // dataColumnUrsprung
        // 
        this.dataColumnUrsprung.ColumnName = "Ursprung";
        // 
        // dataColumnDatum
        // 
        this.dataColumnDatum.ColumnName = "Datum";
        this.dataColumnDatum.DataType = typeof(System.DateTime);
        // 
        // dataColumnTraktnr
        // 
        this.dataColumnTraktnr.ColumnName = "Traktnr";
        this.dataColumnTraktnr.DataType = typeof(int);
        // 
        // dataColumnTraktnamn
        // 
        this.dataColumnTraktnamn.ColumnName = "Traktnamn";
        // 
        // dataColumnStandort
        // 
        this.dataColumnStandort.ColumnName = "Standort";
        this.dataColumnStandort.DataType = typeof(int);
        // 
        // dataColumnEntreprenor
        // 
        this.dataColumnEntreprenor.ColumnName = "Entreprenor";
        this.dataColumnEntreprenor.DataType = typeof(int);
        // 
        // dataColumnKommentar
        // 
        this.dataColumnKommentar.ColumnName = "Kommentar";
        // 
        // dataColumnLanguage
        // 
        this.dataColumnLanguage.ColumnName = "Language";
        // 
        // dataColumnMailFrom
        // 
        this.dataColumnMailFrom.ColumnName = "MailFrom";
        // 
        // dataColumnProvytestorlek
        // 
        this.dataColumnProvytestorlek.ColumnName = "Provytestorlek";
        this.dataColumnProvytestorlek.DataType = typeof(int);
        // 
        // dataColumnStatus
        // 
        this.dataColumnStatus.ColumnName = "Status";
        // 
        // dataColumnStatus_Datum
        // 
        this.dataColumnStatus_Datum.ColumnName = "Status_Datum";
        this.dataColumnStatus_Datum.DataType = typeof(System.DateTime);
        // 
        // dataColumnArtal
        // 
        this.dataColumnArtal.ColumnName = "Artal";
        this.dataColumnArtal.DataType = typeof(int);
        // 
        // dataColumnMal
        // 
        this.dataColumnMal.ColumnName = "Mal";
        this.dataColumnMal.DataType = typeof(int);
        // 
        // dataColumnInvTypId
        // 
        this.dataColumnInvTypId.ColumnName = "InvTypId";
        this.dataColumnInvTypId.DataType = typeof(int);
        // 
        // dataColumnInvTyp
        // 
        this.dataColumnInvTyp.ColumnName = "InvTyp";
        // 
        // dataColumnAtgAreal
        // 
        this.dataColumnAtgAreal.ColumnName = "AtgAreal";
        this.dataColumnAtgAreal.DataType = typeof(double);
        // 
        // dataTableFragor
        // 
        this.dataTableFragor.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn9,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn7,
            this.dataColumn8,
            this.dataColumn10,
            this.dataColumn11,
            this.dataColumn12});
        this.dataTableFragor.TableName = "Fragor";
        // 
        // dataColumn1
        // 
        this.dataColumn1.Caption = "Fraga1";
        this.dataColumn1.ColumnName = "Fraga1";
        // 
        // dataColumn2
        // 
        this.dataColumn2.ColumnName = "Fraga2";
        // 
        // dataColumn9
        // 
        this.dataColumn9.ColumnName = "Ovrigt";
        // 
        // dataColumn3
        // 
        this.dataColumn3.ColumnName = "Fraga3";
        // 
        // dataColumn4
        // 
        this.dataColumn4.ColumnName = "Fraga4";
        // 
        // dataColumn5
        // 
        this.dataColumn5.ColumnName = "Fraga5";
        // 
        // dataColumn6
        // 
        this.dataColumn6.ColumnName = "Fraga6";
        // 
        // dataColumn7
        // 
        this.dataColumn7.ColumnName = "Fraga7";
        // 
        // dataColumn8
        // 
        this.dataColumn8.ColumnName = "Fraga8";
        // 
        // dataColumn10
        // 
        this.dataColumn10.ColumnName = "Fraga9";
        // 
        // dataColumn11
        // 
        this.dataColumn11.ColumnName = "Fraga10";
        // 
        // dataColumn12
        // 
        this.dataColumn12.ColumnName = "Fraga11";
        // 
        // dataSetSettings
        // 
        this.dataSetSettings.DataSetName = "DataSetSettings";
        this.dataSetSettings.Relations.AddRange(new System.Data.DataRelation[] {
            new System.Data.DataRelation("RelationRegionDistrikt", "Region", "Distrikt", new string[] {
                        "Id"}, new string[] {
                        "RegionId"}, false)});
        this.dataSetSettings.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTableRegion,
            this.dataTableDistrikt,
            this.dataTableMarkberedningsmetod,
            this.dataTableUrsprung,
            this.dataTableInvTyp});
        // 
        // dataTableRegion
        // 
        this.dataTableRegion.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSettingsRegionId,
            this.dataColumnSettingsRegionNamn});
        this.dataTableRegion.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, false)});
        this.dataTableRegion.TableName = "Region";
        // 
        // dataColumnSettingsRegionId
        // 
        this.dataColumnSettingsRegionId.ColumnName = "Id";
        this.dataColumnSettingsRegionId.DataType = typeof(int);
        // 
        // dataColumnSettingsRegionNamn
        // 
        this.dataColumnSettingsRegionNamn.ColumnName = "Namn";
        // 
        // dataTableDistrikt
        // 
        this.dataTableDistrikt.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSettingsDistriktRegionId,
            this.dataColumnSettingsDistriktId,
            this.dataColumnSettingsDistriktNamn});
        this.dataTableDistrikt.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.ForeignKeyConstraint("RelationRegionDistrikt", "Region", new string[] {
                        "Id"}, new string[] {
                        "RegionId"}, System.Data.AcceptRejectRule.None, System.Data.Rule.Cascade, System.Data.Rule.Cascade)});
        this.dataTableDistrikt.TableName = "Distrikt";
        // 
        // dataColumnSettingsDistriktRegionId
        // 
        this.dataColumnSettingsDistriktRegionId.ColumnName = "RegionId";
        this.dataColumnSettingsDistriktRegionId.DataType = typeof(int);
        // 
        // dataColumnSettingsDistriktId
        // 
        this.dataColumnSettingsDistriktId.ColumnName = "Id";
        this.dataColumnSettingsDistriktId.DataType = typeof(int);
        // 
        // dataColumnSettingsDistriktNamn
        // 
        this.dataColumnSettingsDistriktNamn.ColumnName = "Namn";
        // 
        // dataTableMarkberedningsmetod
        // 
        this.dataTableMarkberedningsmetod.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSettingsMarkberedningsmetodNamn});
        this.dataTableMarkberedningsmetod.TableName = "Markberedningsmetod";
        // 
        // dataColumnSettingsMarkberedningsmetodNamn
        // 
        this.dataColumnSettingsMarkberedningsmetodNamn.ColumnName = "Namn";
        // 
        // dataTableUrsprung
        // 
        this.dataTableUrsprung.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSettingsUrsprungNamn});
        this.dataTableUrsprung.TableName = "Ursprung";
        // 
        // dataColumnSettingsUrsprungNamn
        // 
        this.dataColumnSettingsUrsprungNamn.ColumnName = "Namn";
        // 
        // dataTableInvTyp
        // 
        this.dataTableInvTyp.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumnSettingsInvTypId,
            this.dataColumnSettingsInvTypNamn});
        this.dataTableInvTyp.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, false)});
        this.dataTableInvTyp.TableName = "InvTyp";
        // 
        // dataColumnSettingsInvTypId
        // 
        this.dataColumnSettingsInvTypId.ColumnName = "Id";
        this.dataColumnSettingsInvTypId.DataType = typeof(int);
        // 
        // dataColumnSettingsInvTypNamn
        // 
        this.dataColumnSettingsInvTypNamn.ColumnName = "Namn";
        // 
        // menuStripPlantering
        // 
        this.menuStripPlantering.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.arkivToolStripMenuItem});
        this.menuStripPlantering.Location = new System.Drawing.Point(0, 0);
        this.menuStripPlantering.Name = "menuStripPlantering";
        this.menuStripPlantering.Padding = new System.Windows.Forms.Padding(7, 2, 0, 2);
        this.menuStripPlantering.Size = new System.Drawing.Size(657, 24);
        this.menuStripPlantering.TabIndex = 0;
        this.menuStripPlantering.Text = "menuStrip1";
        // 
        // arkivToolStripMenuItem
        // 
        this.arkivToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.sparaToolStripMenuItem,
            this.sparaSomToolStripMenuItem,
            this.toolStripSeparator2,
            this.avslutaToolStripMenuItem});
        this.arkivToolStripMenuItem.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.arkivToolStripMenuItem.Name = "arkivToolStripMenuItem";
        this.arkivToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
        this.arkivToolStripMenuItem.Text = global::Egenuppfoljning.Properties.Resources.Arkiv;
        // 
        // sparaToolStripMenuItem
        // 
        this.sparaToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("sparaToolStripMenuItem.Image")));
        this.sparaToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
        this.sparaToolStripMenuItem.Name = "sparaToolStripMenuItem";
        this.sparaToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
        this.sparaToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
        this.sparaToolStripMenuItem.Text = global::Egenuppfoljning.Properties.Resources.Spara;
        this.sparaToolStripMenuItem.Click += new System.EventHandler(this.sparaToolStripMenuItem_Click);
        // 
        // sparaSomToolStripMenuItem
        // 
        this.sparaSomToolStripMenuItem.Name = "sparaSomToolStripMenuItem";
        this.sparaSomToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
        this.sparaSomToolStripMenuItem.Text = global::Egenuppfoljning.Properties.Resources.SparaSom;
        this.sparaSomToolStripMenuItem.Click += new System.EventHandler(this.sparaSomToolStripMenuItem_Click);
        // 
        // toolStripSeparator2
        // 
        this.toolStripSeparator2.Name = "toolStripSeparator2";
        this.toolStripSeparator2.Size = new System.Drawing.Size(147, 6);
        // 
        // avslutaToolStripMenuItem
        // 
        this.avslutaToolStripMenuItem.Name = "avslutaToolStripMenuItem";
        this.avslutaToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
        this.avslutaToolStripMenuItem.Text = "Stäng";
        this.avslutaToolStripMenuItem.Click += new System.EventHandler(this.avslutaToolStripMenuItem_Click);
        // 
        // reportPlantering
        // 
        this.reportPlantering.ReportResourceString = resources.GetString("reportPlantering.ReportResourceString");
        this.reportPlantering.RegisterData(this.dataSetPlantering, "dataSetPlantering");
        // 
        // tabControl1
        // 
        this.tabControl1.Controls.Add(this.tabPagePlantering);
        this.tabControl1.Controls.Add(this.tabPageYtor);
        this.tabControl1.Controls.Add(this.tabPageFrågor);
        this.tabControl1.Location = new System.Drawing.Point(0, 27);
        this.tabControl1.Name = "tabControl1";
        this.tabControl1.SelectedIndex = 0;
        this.tabControl1.Size = new System.Drawing.Size(652, 494);
        this.tabControl1.TabIndex = 1;
        // 
        // tabPagePlantering
        // 
        this.tabPagePlantering.Controls.Add(this.groupBoxPlantering);
        this.tabPagePlantering.Controls.Add(this.groupBoxTrakt);
        this.tabPagePlantering.Controls.Add(this.groupBoxRegion);
        this.tabPagePlantering.Location = new System.Drawing.Point(4, 22);
        this.tabPagePlantering.Name = "tabPagePlantering";
        this.tabPagePlantering.Size = new System.Drawing.Size(644, 468);
        this.tabPagePlantering.TabIndex = 2;
        this.tabPagePlantering.Text = "Plantering";
        this.tabPagePlantering.UseVisualStyleBackColor = true;
        // 
        // groupBoxPlantering
        // 
        this.groupBoxPlantering.Controls.Add(this.label3);
        this.groupBoxPlantering.Controls.Add(this.label4);
        this.groupBoxPlantering.Controls.Add(this.textBoxMåluppfyllelse);
        this.groupBoxPlantering.Controls.Add(this.label5);
        this.groupBoxPlantering.Controls.Add(this.label6);
        this.groupBoxPlantering.Controls.Add(this.textBoxMål);
        this.groupBoxPlantering.Controls.Add(this.label7);
        this.groupBoxPlantering.Controls.Add(this.label8);
        this.groupBoxPlantering.Controls.Add(this.label21);
        this.groupBoxPlantering.Controls.Add(this.label22);
        this.groupBoxPlantering.Controls.Add(this.textBoxUttnyttjandegrad);
        this.groupBoxPlantering.Controls.Add(this.label24);
        this.groupBoxPlantering.Controls.Add(this.textBoxAndelMedBraDjup);
        this.groupBoxPlantering.Controls.Add(this.textBoxUtmärktPlanteringsIndex);
        this.groupBoxPlantering.Controls.Add(this.label23);
        this.groupBoxPlantering.Controls.Add(this.textBoxAndelMedBraTillryckning);
        this.groupBoxPlantering.Controls.Add(this.label25);
        this.groupBoxPlantering.Controls.Add(this.label26);
        this.groupBoxPlantering.Controls.Add(this.labelGISData);
        this.groupBoxPlantering.Controls.Add(this.textBoxAntalSattaPlantor);
        this.groupBoxPlantering.Controls.Add(this.textBoxPlanteringAntalOptimalaBra);
        this.groupBoxPlantering.Controls.Add(this.labelPlanteringAntalOptimalaBra);
        this.groupBoxPlantering.Controls.Add(this.textBoxAntalOptimalaPlantor);
        this.groupBoxPlantering.Controls.Add(this.labelPlanteringAntalOptimala);
        this.groupBoxPlantering.Controls.Add(this.labelGISStracka);
        this.groupBoxPlantering.Controls.Add(this.textBoxProvytestorlek);
        this.groupBoxPlantering.Location = new System.Drawing.Point(8, 166);
        this.groupBoxPlantering.Name = "groupBoxPlantering";
        this.groupBoxPlantering.Size = new System.Drawing.Size(540, 247);
        this.groupBoxPlantering.TabIndex = 2;
        this.groupBoxPlantering.TabStop = false;
        // 
        // label3
        // 
        this.label3.AutoSize = true;
        this.label3.Location = new System.Drawing.Point(498, 204);
        this.label3.Name = "label3";
        this.label3.Size = new System.Drawing.Size(20, 13);
        this.label3.TabIndex = 25;
        this.label3.Text = "%";
        // 
        // label4
        // 
        this.label4.AutoSize = true;
        this.label4.Location = new System.Drawing.Point(496, 169);
        this.label4.Name = "label4";
        this.label4.Size = new System.Drawing.Size(37, 13);
        this.label4.TabIndex = 20;
        this.label4.Text = "pl/ha";
        // 
        // textBoxMåluppfyllelse
        // 
        this.textBoxMåluppfyllelse.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxMåluppfyllelse.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxMåluppfyllelse.Location = new System.Drawing.Point(431, 201);
        this.textBoxMåluppfyllelse.Name = "textBoxMåluppfyllelse";
        this.textBoxMåluppfyllelse.ReadOnly = true;
        this.textBoxMåluppfyllelse.Size = new System.Drawing.Size(63, 21);
        this.textBoxMåluppfyllelse.TabIndex = 24;
        this.textBoxMåluppfyllelse.TabStop = false;
        this.textBoxMåluppfyllelse.Text = "0";
        this.textBoxMåluppfyllelse.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // label5
        // 
        this.label5.AutoSize = true;
        this.label5.Location = new System.Drawing.Point(336, 206);
        this.label5.Name = "label5";
        this.label5.Size = new System.Drawing.Size(88, 13);
        this.label5.TabIndex = 23;
        this.label5.Text = "Måluppfyllelse";
        // 
        // label6
        // 
        this.label6.AutoSize = true;
        this.label6.Location = new System.Drawing.Point(397, 169);
        this.label6.Name = "label6";
        this.label6.Size = new System.Drawing.Size(27, 13);
        this.label6.TabIndex = 18;
        this.label6.Text = "Mål";
        // 
        // textBoxMål
        // 
        this.textBoxMål.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetPlantering, "UserData.Mal", true));
        this.textBoxMål.Location = new System.Drawing.Point(431, 165);
        this.textBoxMål.MaxLength = 16;
        this.textBoxMål.Name = "textBoxMål";
        this.textBoxMål.Size = new System.Drawing.Size(63, 21);
        this.textBoxMål.TabIndex = 11;
        this.textBoxMål.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        this.textBoxMål.TextChanged += new System.EventHandler(this.textBoxMål_TextChanged);
        this.textBoxMål.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxMål_KeyDown);
        this.textBoxMål.Leave += new System.EventHandler(this.textBoxMål_Leave);
        // 
        // label7
        // 
        this.label7.AutoSize = true;
        this.label7.Location = new System.Drawing.Point(227, 24);
        this.label7.Name = "label7";
        this.label7.Size = new System.Drawing.Size(25, 13);
        this.label7.TabIndex = 2;
        this.label7.Text = "m2";
        // 
        // label8
        // 
        this.label8.AutoSize = true;
        this.label8.Location = new System.Drawing.Point(294, 133);
        this.label8.Name = "label8";
        this.label8.Size = new System.Drawing.Size(38, 13);
        this.label8.TabIndex = 15;
        this.label8.Text = "st/ha";
        // 
        // label21
        // 
        this.label21.AutoSize = true;
        this.label21.Location = new System.Drawing.Point(293, 99);
        this.label21.Name = "label21";
        this.label21.Size = new System.Drawing.Size(38, 13);
        this.label21.TabIndex = 12;
        this.label21.Text = "st/ha";
        // 
        // label22
        // 
        this.label22.AutoSize = true;
        this.label22.Location = new System.Drawing.Point(495, 24);
        this.label22.Name = "label22";
        this.label22.Size = new System.Drawing.Size(38, 13);
        this.label22.TabIndex = 5;
        this.label22.Text = "st/ha";
        // 
        // textBoxUttnyttjandegrad
        // 
        this.textBoxUttnyttjandegrad.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxUttnyttjandegrad.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxUttnyttjandegrad.Location = new System.Drawing.Point(162, 55);
        this.textBoxUttnyttjandegrad.Name = "textBoxUttnyttjandegrad";
        this.textBoxUttnyttjandegrad.ReadOnly = true;
        this.textBoxUttnyttjandegrad.Size = new System.Drawing.Size(63, 21);
        this.textBoxUttnyttjandegrad.TabIndex = 7;
        this.textBoxUttnyttjandegrad.TabStop = false;
        this.textBoxUttnyttjandegrad.Text = "0";
        this.textBoxUttnyttjandegrad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // label24
        // 
        this.label24.AutoSize = true;
        this.label24.Location = new System.Drawing.Point(13, 58);
        this.label24.Name = "label24";
        this.label24.Size = new System.Drawing.Size(143, 13);
        this.label24.TabIndex = 6;
        this.label24.Text = "Utnyttjandegrad av mb ";
        // 
        // textBoxAndelMedBraDjup
        // 
        this.textBoxAndelMedBraDjup.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxAndelMedBraDjup.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxAndelMedBraDjup.Location = new System.Drawing.Point(431, 55);
        this.textBoxAndelMedBraDjup.Name = "textBoxAndelMedBraDjup";
        this.textBoxAndelMedBraDjup.ReadOnly = true;
        this.textBoxAndelMedBraDjup.Size = new System.Drawing.Size(63, 21);
        this.textBoxAndelMedBraDjup.TabIndex = 9;
        this.textBoxAndelMedBraDjup.TabStop = false;
        this.textBoxAndelMedBraDjup.Text = "0";
        this.textBoxAndelMedBraDjup.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // textBoxUtmärktPlanteringsIndex
        // 
        this.textBoxUtmärktPlanteringsIndex.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxUtmärktPlanteringsIndex.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxUtmärktPlanteringsIndex.Location = new System.Drawing.Point(230, 201);
        this.textBoxUtmärktPlanteringsIndex.Name = "textBoxUtmärktPlanteringsIndex";
        this.textBoxUtmärktPlanteringsIndex.ReadOnly = true;
        this.textBoxUtmärktPlanteringsIndex.Size = new System.Drawing.Size(63, 21);
        this.textBoxUtmärktPlanteringsIndex.TabIndex = 22;
        this.textBoxUtmärktPlanteringsIndex.TabStop = false;
        this.textBoxUtmärktPlanteringsIndex.Text = "0";
        this.textBoxUtmärktPlanteringsIndex.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // label23
        // 
        this.label23.AutoSize = true;
        this.label23.Location = new System.Drawing.Point(13, 204);
        this.label23.Name = "label23";
        this.label23.Size = new System.Drawing.Size(161, 13);
        this.label23.TabIndex = 21;
        this.label23.Text = "Utmärkt Planterings Index ";
        // 
        // textBoxAndelMedBraTillryckning
        // 
        this.textBoxAndelMedBraTillryckning.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxAndelMedBraTillryckning.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxAndelMedBraTillryckning.Location = new System.Drawing.Point(230, 165);
        this.textBoxAndelMedBraTillryckning.Name = "textBoxAndelMedBraTillryckning";
        this.textBoxAndelMedBraTillryckning.ReadOnly = true;
        this.textBoxAndelMedBraTillryckning.Size = new System.Drawing.Size(63, 21);
        this.textBoxAndelMedBraTillryckning.TabIndex = 17;
        this.textBoxAndelMedBraTillryckning.TabStop = false;
        this.textBoxAndelMedBraTillryckning.Text = "0";
        this.textBoxAndelMedBraTillryckning.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // label25
        // 
        this.label25.AutoSize = true;
        this.label25.Location = new System.Drawing.Point(12, 168);
        this.label25.Name = "label25";
        this.label25.Size = new System.Drawing.Size(158, 13);
        this.label25.TabIndex = 16;
        this.label25.Text = "Andel med bra tillryckning ";
        // 
        // label26
        // 
        this.label26.AutoSize = true;
        this.label26.Location = new System.Drawing.Point(310, 58);
        this.label26.Name = "label26";
        this.label26.Size = new System.Drawing.Size(117, 13);
        this.label26.TabIndex = 8;
        this.label26.Text = "Andel med bra djup";
        // 
        // labelGISData
        // 
        this.labelGISData.AutoSize = true;
        this.labelGISData.Location = new System.Drawing.Point(13, 24);
        this.labelGISData.Name = "labelGISData";
        this.labelGISData.Size = new System.Drawing.Size(123, 13);
        this.labelGISData.TabIndex = 0;
        this.labelGISData.Text = "Provytestorlek (m2)";
        // 
        // textBoxAntalSattaPlantor
        // 
        this.textBoxAntalSattaPlantor.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxAntalSattaPlantor.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxAntalSattaPlantor.Location = new System.Drawing.Point(431, 21);
        this.textBoxAntalSattaPlantor.Name = "textBoxAntalSattaPlantor";
        this.textBoxAntalSattaPlantor.ReadOnly = true;
        this.textBoxAntalSattaPlantor.Size = new System.Drawing.Size(63, 21);
        this.textBoxAntalSattaPlantor.TabIndex = 4;
        this.textBoxAntalSattaPlantor.TabStop = false;
        this.textBoxAntalSattaPlantor.Text = "0";
        this.textBoxAntalSattaPlantor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // textBoxPlanteringAntalOptimalaBra
        // 
        this.textBoxPlanteringAntalOptimalaBra.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxPlanteringAntalOptimalaBra.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxPlanteringAntalOptimalaBra.Location = new System.Drawing.Point(230, 129);
        this.textBoxPlanteringAntalOptimalaBra.Name = "textBoxPlanteringAntalOptimalaBra";
        this.textBoxPlanteringAntalOptimalaBra.ReadOnly = true;
        this.textBoxPlanteringAntalOptimalaBra.Size = new System.Drawing.Size(63, 21);
        this.textBoxPlanteringAntalOptimalaBra.TabIndex = 14;
        this.textBoxPlanteringAntalOptimalaBra.TabStop = false;
        this.textBoxPlanteringAntalOptimalaBra.Text = "0";
        this.textBoxPlanteringAntalOptimalaBra.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // labelPlanteringAntalOptimalaBra
        // 
        this.labelPlanteringAntalOptimalaBra.AutoSize = true;
        this.labelPlanteringAntalOptimalaBra.Location = new System.Drawing.Point(11, 132);
        this.labelPlanteringAntalOptimalaBra.Name = "labelPlanteringAntalOptimalaBra";
        this.labelPlanteringAntalOptimalaBra.Size = new System.Drawing.Size(210, 13);
        this.labelPlanteringAntalOptimalaBra.TabIndex = 13;
        this.labelPlanteringAntalOptimalaBra.Text = "Antal optimalt och bra satta plantor";
        // 
        // textBoxAntalOptimalaPlantor
        // 
        this.textBoxAntalOptimalaPlantor.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxAntalOptimalaPlantor.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxAntalOptimalaPlantor.Location = new System.Drawing.Point(229, 95);
        this.textBoxAntalOptimalaPlantor.Name = "textBoxAntalOptimalaPlantor";
        this.textBoxAntalOptimalaPlantor.ReadOnly = true;
        this.textBoxAntalOptimalaPlantor.Size = new System.Drawing.Size(63, 21);
        this.textBoxAntalOptimalaPlantor.TabIndex = 11;
        this.textBoxAntalOptimalaPlantor.TabStop = false;
        this.textBoxAntalOptimalaPlantor.Text = "0";
        this.textBoxAntalOptimalaPlantor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // labelPlanteringAntalOptimala
        // 
        this.labelPlanteringAntalOptimala.AutoSize = true;
        this.labelPlanteringAntalOptimala.Location = new System.Drawing.Point(13, 98);
        this.labelPlanteringAntalOptimala.Name = "labelPlanteringAntalOptimala";
        this.labelPlanteringAntalOptimala.Size = new System.Drawing.Size(165, 13);
        this.labelPlanteringAntalOptimala.TabIndex = 10;
        this.labelPlanteringAntalOptimala.Text = "Antal optimalt satta plantor";
        // 
        // labelGISStracka
        // 
        this.labelGISStracka.AutoSize = true;
        this.labelGISStracka.Location = new System.Drawing.Point(310, 24);
        this.labelGISStracka.Name = "labelGISStracka";
        this.labelGISStracka.Size = new System.Drawing.Size(114, 13);
        this.labelGISStracka.TabIndex = 3;
        this.labelGISStracka.Text = "Antal satta plantor";
        // 
        // textBoxProvytestorlek
        // 
        this.textBoxProvytestorlek.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxProvytestorlek.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetPlantering, "UserData.Provytestorlek", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.textBoxProvytestorlek.Location = new System.Drawing.Point(162, 20);
        this.textBoxProvytestorlek.Name = "textBoxProvytestorlek";
        this.textBoxProvytestorlek.ReadOnly = true;
        this.textBoxProvytestorlek.Size = new System.Drawing.Size(63, 21);
        this.textBoxProvytestorlek.TabIndex = 1;
        this.textBoxProvytestorlek.TabStop = false;
        this.textBoxProvytestorlek.Text = "100";
        this.textBoxProvytestorlek.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        this.textBoxProvytestorlek.TextChanged += new System.EventHandler(this.textBoxProvytestorlek_TextChanged);
        this.textBoxProvytestorlek.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxProvytestorlek_KeyDown);
        this.textBoxProvytestorlek.Leave += new System.EventHandler(this.textBoxProvytestorlek_Leave);
        // 
        // groupBoxTrakt
        // 
        this.groupBoxTrakt.Controls.Add(this.textBoxTraktDel);
        this.groupBoxTrakt.Controls.Add(this.textBoxTraktnr);
        this.groupBoxTrakt.Controls.Add(this.textBoxTraktnamn);
        this.groupBoxTrakt.Controls.Add(this.textBoxEntreprenor);
        this.groupBoxTrakt.Controls.Add(this.labeEntreprenor);
        this.groupBoxTrakt.Controls.Add(this.labelStandort);
        this.groupBoxTrakt.Controls.Add(this.labelTraktnamn);
        this.groupBoxTrakt.Controls.Add(this.labelTraktnr);
        this.groupBoxTrakt.Location = new System.Drawing.Point(8, 106);
        this.groupBoxTrakt.Name = "groupBoxTrakt";
        this.groupBoxTrakt.Size = new System.Drawing.Size(540, 72);
        this.groupBoxTrakt.TabIndex = 1;
        this.groupBoxTrakt.TabStop = false;
        // 
        // textBoxTraktDel
        // 
        this.textBoxTraktDel.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetPlantering, "UserData.Standort", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, null, "0000"));
        this.textBoxTraktDel.Location = new System.Drawing.Point(267, 30);
        this.textBoxTraktDel.MaxLength = 4;
        this.textBoxTraktDel.Name = "textBoxTraktDel";
        this.textBoxTraktDel.Size = new System.Drawing.Size(116, 21);
        this.textBoxTraktDel.TabIndex = 9;
        this.textBoxTraktDel.TextChanged += new System.EventHandler(this.textBoxTraktDel_TextChanged);
        this.textBoxTraktDel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxTraktDel_KeyDown);
        this.textBoxTraktDel.Leave += new System.EventHandler(this.textBoxTraktDel_Leave);
        // 
        // textBoxTraktnr
        // 
        this.textBoxTraktnr.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetPlantering, "UserData.Traktnr", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, null, "000000000"));
        this.textBoxTraktnr.Location = new System.Drawing.Point(15, 30);
        this.textBoxTraktnr.MaxLength = 9;
        this.textBoxTraktnr.Name = "textBoxTraktnr";
        this.textBoxTraktnr.Size = new System.Drawing.Size(116, 21);
        this.textBoxTraktnr.TabIndex = 7;
        this.textBoxTraktnr.TextChanged += new System.EventHandler(this.textBoxTraktnr_TextChanged);
        this.textBoxTraktnr.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxTraktnr_KeyDown);
        this.textBoxTraktnr.Leave += new System.EventHandler(this.textBoxTraktnr_Leave);
        // 
        // textBoxTraktnamn
        // 
        this.textBoxTraktnamn.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetPlantering, "UserData.Traktnamn", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.textBoxTraktnamn.Location = new System.Drawing.Point(140, 30);
        this.textBoxTraktnamn.MaxLength = 31;
        this.textBoxTraktnamn.Name = "textBoxTraktnamn";
        this.textBoxTraktnamn.Size = new System.Drawing.Size(116, 21);
        this.textBoxTraktnamn.TabIndex = 8;
        this.textBoxTraktnamn.TextChanged += new System.EventHandler(this.textBoxTraktnamn_TextChanged);
        // 
        // textBoxEntreprenor
        // 
        this.textBoxEntreprenor.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetPlantering, "UserData.Entreprenor", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, null, "0000"));
        this.textBoxEntreprenor.Location = new System.Drawing.Point(391, 30);
        this.textBoxEntreprenor.MaxLength = 4;
        this.textBoxEntreprenor.Name = "textBoxEntreprenor";
        this.textBoxEntreprenor.Size = new System.Drawing.Size(116, 21);
        this.textBoxEntreprenor.TabIndex = 10;
        this.textBoxEntreprenor.TextChanged += new System.EventHandler(this.textBoxEntreprenor_TextChanged);
        this.textBoxEntreprenor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxEntreprenor_KeyDown);
        this.textBoxEntreprenor.Leave += new System.EventHandler(this.textBoxEntreprenor_Leave);
        // 
        // labeEntreprenor
        // 
        this.labeEntreprenor.AutoSize = true;
        this.labeEntreprenor.Location = new System.Drawing.Point(391, 14);
        this.labeEntreprenor.Name = "labeEntreprenor";
        this.labeEntreprenor.Size = new System.Drawing.Size(145, 13);
        this.labeEntreprenor.TabIndex = 6;
        this.labeEntreprenor.Text = "Maskinnr/Entreprenörnr";
        // 
        // labelStandort
        // 
        this.labelStandort.AutoSize = true;
        this.labelStandort.Location = new System.Drawing.Point(267, 14);
        this.labelStandort.Name = "labelStandort";
        this.labelStandort.Size = new System.Drawing.Size(55, 13);
        this.labelStandort.TabIndex = 4;
        this.labelStandort.Text = "Traktdel";
        // 
        // labelTraktnamn
        // 
        this.labelTraktnamn.AutoSize = true;
        this.labelTraktnamn.Location = new System.Drawing.Point(140, 14);
        this.labelTraktnamn.Name = "labelTraktnamn";
        this.labelTraktnamn.Size = new System.Drawing.Size(70, 13);
        this.labelTraktnamn.TabIndex = 2;
        this.labelTraktnamn.Text = "Traktnamn";
        // 
        // labelTraktnr
        // 
        this.labelTraktnr.AutoSize = true;
        this.labelTraktnr.Location = new System.Drawing.Point(15, 14);
        this.labelTraktnr.Name = "labelTraktnr";
        this.labelTraktnr.Size = new System.Drawing.Size(50, 13);
        this.labelTraktnr.TabIndex = 0;
        this.labelTraktnr.Text = "Traktnr";
        // 
        // groupBoxRegion
        // 
        this.groupBoxRegion.Controls.Add(this.textBoxDistrikt);
        this.groupBoxRegion.Controls.Add(this.textBoxRegion);
        this.groupBoxRegion.Controls.Add(this.comboBoxInvTyp);
        this.groupBoxRegion.Controls.Add(this.labelInvTyp);
        this.groupBoxRegion.Controls.Add(this.textBoxAtgAreal);
        this.groupBoxRegion.Controls.Add(this.labelAtgAreal);
        this.groupBoxRegion.Controls.Add(this.comboBoxUrsprung);
        this.groupBoxRegion.Controls.Add(this.dateTimePicker);
        this.groupBoxRegion.Controls.Add(this.comboBoxDistrikt);
        this.groupBoxRegion.Controls.Add(this.comboBoxRegion);
        this.groupBoxRegion.Controls.Add(this.labelDatum);
        this.groupBoxRegion.Controls.Add(this.labelUrsprung);
        this.groupBoxRegion.Controls.Add(this.labelDistrikt);
        this.groupBoxRegion.Controls.Add(this.labelRegion);
        this.groupBoxRegion.Location = new System.Drawing.Point(8, 3);
        this.groupBoxRegion.Name = "groupBoxRegion";
        this.groupBoxRegion.Size = new System.Drawing.Size(540, 109);
        this.groupBoxRegion.TabIndex = 0;
        this.groupBoxRegion.TabStop = false;
        // 
        // textBoxDistrikt
        // 
        this.textBoxDistrikt.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetPlantering, "UserData.Distrikt", true));
        this.textBoxDistrikt.Location = new System.Drawing.Point(264, 30);
        this.textBoxDistrikt.MaxLength = 32;
        this.textBoxDistrikt.Name = "textBoxDistrikt";
        this.textBoxDistrikt.ReadOnly = true;
        this.textBoxDistrikt.Size = new System.Drawing.Size(116, 21);
        this.textBoxDistrikt.TabIndex = 7;
        this.textBoxDistrikt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        this.textBoxDistrikt.Visible = false;
        // 
        // textBoxRegion
        // 
        this.textBoxRegion.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetPlantering, "UserData.Region", true));
        this.textBoxRegion.Location = new System.Drawing.Point(140, 30);
        this.textBoxRegion.MaxLength = 32;
        this.textBoxRegion.Name = "textBoxRegion";
        this.textBoxRegion.ReadOnly = true;
        this.textBoxRegion.Size = new System.Drawing.Size(116, 21);
        this.textBoxRegion.TabIndex = 22;
        this.textBoxRegion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        this.textBoxRegion.Visible = false;
        // 
        // comboBoxInvTyp
        // 
        this.comboBoxInvTyp.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetPlantering, "UserData.InvTyp", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.comboBoxInvTyp.DataSource = this.dataSetSettings;
        this.comboBoxInvTyp.DisplayMember = "InvTyp.Namn";
        this.comboBoxInvTyp.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxInvTyp.FormattingEnabled = true;
        this.comboBoxInvTyp.Location = new System.Drawing.Point(15, 30);
        this.comboBoxInvTyp.Name = "comboBoxInvTyp";
        this.comboBoxInvTyp.Size = new System.Drawing.Size(116, 21);
        this.comboBoxInvTyp.TabIndex = 1;
        this.comboBoxInvTyp.ValueMember = "InvTyp.Id";
        this.comboBoxInvTyp.SelectionChangeCommitted += new System.EventHandler(this.comboBoxInvTyp_SelectionChangeCommitted);
        this.comboBoxInvTyp.SelectedIndexChanged += new System.EventHandler(this.comboBoxInvTyp_SelectedIndexChanged);
        // 
        // labelInvTyp
        // 
        this.labelInvTyp.AutoSize = true;
        this.labelInvTyp.Location = new System.Drawing.Point(15, 14);
        this.labelInvTyp.Name = "labelInvTyp";
        this.labelInvTyp.Size = new System.Drawing.Size(45, 13);
        this.labelInvTyp.TabIndex = 21;
        this.labelInvTyp.Text = "Invtyp";
        // 
        // textBoxAtgAreal
        // 
        this.textBoxAtgAreal.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetPlantering, "UserData.AtgAreal", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged, null, "N1"));
        this.textBoxAtgAreal.Location = new System.Drawing.Point(140, 76);
        this.textBoxAtgAreal.MaxLength = 32;
        this.textBoxAtgAreal.Name = "textBoxAtgAreal";
        this.textBoxAtgAreal.Size = new System.Drawing.Size(116, 21);
        this.textBoxAtgAreal.TabIndex = 6;
        this.textBoxAtgAreal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        this.textBoxAtgAreal.TextChanged += new System.EventHandler(this.textBoxAtgAreal_TextChanged);
        this.textBoxAtgAreal.Validated += new System.EventHandler(this.textBoxAtgAreal_Validated);
        this.textBoxAtgAreal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxAtgAreal_KeyDown);
        this.textBoxAtgAreal.Leave += new System.EventHandler(this.textBoxAtgAreal_Leave);
        // 
        // labelAtgAreal
        // 
        this.labelAtgAreal.AutoSize = true;
        this.labelAtgAreal.Location = new System.Drawing.Point(140, 57);
        this.labelAtgAreal.Name = "labelAtgAreal";
        this.labelAtgAreal.Size = new System.Drawing.Size(59, 13);
        this.labelAtgAreal.TabIndex = 8;
        this.labelAtgAreal.Text = "Åtg areal";
        // 
        // comboBoxUrsprung
        // 
        this.comboBoxUrsprung.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetPlantering, "UserData.Ursprung", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.comboBoxUrsprung.DataSource = this.dataSetSettings;
        this.comboBoxUrsprung.DisplayMember = "Ursprung.Namn";
        this.comboBoxUrsprung.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxUrsprung.FormattingEnabled = true;
        this.comboBoxUrsprung.Location = new System.Drawing.Point(391, 30);
        this.comboBoxUrsprung.Name = "comboBoxUrsprung";
        this.comboBoxUrsprung.Size = new System.Drawing.Size(116, 21);
        this.comboBoxUrsprung.TabIndex = 4;
        this.comboBoxUrsprung.SelectionChangeCommitted += new System.EventHandler(this.comboBoxUrsprung_SelectionChangeCommitted);
        this.comboBoxUrsprung.SelectedIndexChanged += new System.EventHandler(this.comboBoxUrsprung_SelectedIndexChanged);
        // 
        // dateTimePicker
        // 
        this.dateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.dataSetPlantering, "UserData.Datum", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.dateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
        this.dateTimePicker.Location = new System.Drawing.Point(15, 76);
        this.dateTimePicker.Name = "dateTimePicker";
        this.dateTimePicker.Size = new System.Drawing.Size(116, 21);
        this.dateTimePicker.TabIndex = 5;
        this.dateTimePicker.ValueChanged += new System.EventHandler(this.dateTimePicker_ValueChanged);
        // 
        // comboBoxDistrikt
        // 
        this.comboBoxDistrikt.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetPlantering, "UserData.Distrikt", true));
        this.comboBoxDistrikt.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxDistrikt.FormattingEnabled = true;
        this.comboBoxDistrikt.Location = new System.Drawing.Point(264, 30);
        this.comboBoxDistrikt.Name = "comboBoxDistrikt";
        this.comboBoxDistrikt.Size = new System.Drawing.Size(116, 21);
        this.comboBoxDistrikt.TabIndex = 3;
        this.comboBoxDistrikt.SelectionChangeCommitted += new System.EventHandler(this.comboBoxDistrikt_SelectionChangeCommitted);
        this.comboBoxDistrikt.SelectedIndexChanged += new System.EventHandler(this.comboBoxDistrikt_SelectedIndexChanged);
        // 
        // comboBoxRegion
        // 
        this.comboBoxRegion.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetPlantering, "UserData.Region", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.comboBoxRegion.DataSource = this.dataSetSettings;
        this.comboBoxRegion.DisplayMember = "Region.Namn";
        this.comboBoxRegion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxRegion.FormattingEnabled = true;
        this.comboBoxRegion.Location = new System.Drawing.Point(140, 30);
        this.comboBoxRegion.Name = "comboBoxRegion";
        this.comboBoxRegion.Size = new System.Drawing.Size(116, 21);
        this.comboBoxRegion.TabIndex = 2;
        this.comboBoxRegion.ValueMember = "Region.Id";
        this.comboBoxRegion.SelectionChangeCommitted += new System.EventHandler(this.comboBoxRegion_SelectionChangeCommitted);
        this.comboBoxRegion.SelectedIndexChanged += new System.EventHandler(this.comboBoxRegion_SelectedIndexChanged);
        // 
        // labelDatum
        // 
        this.labelDatum.AutoSize = true;
        this.labelDatum.Location = new System.Drawing.Point(15, 57);
        this.labelDatum.Name = "labelDatum";
        this.labelDatum.Size = new System.Drawing.Size(45, 13);
        this.labelDatum.TabIndex = 6;
        this.labelDatum.Text = "Datum";
        // 
        // labelUrsprung
        // 
        this.labelUrsprung.AutoSize = true;
        this.labelUrsprung.Location = new System.Drawing.Point(391, 14);
        this.labelUrsprung.Name = "labelUrsprung";
        this.labelUrsprung.Size = new System.Drawing.Size(59, 13);
        this.labelUrsprung.TabIndex = 4;
        this.labelUrsprung.Text = "Ursprung";
        // 
        // labelDistrikt
        // 
        this.labelDistrikt.AutoSize = true;
        this.labelDistrikt.Location = new System.Drawing.Point(264, 14);
        this.labelDistrikt.Name = "labelDistrikt";
        this.labelDistrikt.Size = new System.Drawing.Size(49, 13);
        this.labelDistrikt.TabIndex = 2;
        this.labelDistrikt.Text = "Distrikt";
        // 
        // labelRegion
        // 
        this.labelRegion.AutoSize = true;
        this.labelRegion.Location = new System.Drawing.Point(137, 14);
        this.labelRegion.Name = "labelRegion";
        this.labelRegion.Size = new System.Drawing.Size(46, 13);
        this.labelRegion.TabIndex = 0;
        this.labelRegion.Text = "Region";
        // 
        // tabPageYtor
        // 
        this.tabPageYtor.Controls.Add(this.groupBoxPlanteringsstallen);
        this.tabPageYtor.Location = new System.Drawing.Point(4, 22);
        this.tabPageYtor.Name = "tabPageYtor";
        this.tabPageYtor.Padding = new System.Windows.Forms.Padding(3);
        this.tabPageYtor.Size = new System.Drawing.Size(644, 468);
        this.tabPageYtor.TabIndex = 0;
        this.tabPageYtor.Text = "Ytor";
        this.tabPageYtor.UseVisualStyleBackColor = true;
        // 
        // groupBoxPlanteringsstallen
        // 
        this.groupBoxPlanteringsstallen.Controls.Add(this.groupBoxMedelvarde);
        this.groupBoxPlanteringsstallen.Controls.Add(this.groupBoxSumma);
        this.groupBoxPlanteringsstallen.Controls.Add(this.dataGridViewPlanteringsstallen);
        this.groupBoxPlanteringsstallen.Location = new System.Drawing.Point(8, 6);
        this.groupBoxPlanteringsstallen.Name = "groupBoxPlanteringsstallen";
        this.groupBoxPlanteringsstallen.Size = new System.Drawing.Size(624, 452);
        this.groupBoxPlanteringsstallen.TabIndex = 0;
        this.groupBoxPlanteringsstallen.TabStop = false;
        this.groupBoxPlanteringsstallen.Text = "Antalet Planteringsställen";
        // 
        // groupBoxMedelvarde
        // 
        this.groupBoxMedelvarde.Controls.Add(this.textBoxMedelvardeTilltryckning);
        this.groupBoxMedelvarde.Controls.Add(this.textBoxMedelvardeBra);
        this.groupBoxMedelvarde.Controls.Add(this.label16);
        this.groupBoxMedelvarde.Controls.Add(this.labelMedelvardeBra);
        this.groupBoxMedelvarde.Controls.Add(this.textBoxMedelvardePlanteringsdjupfel);
        this.groupBoxMedelvarde.Controls.Add(this.textBoxMedelvardeOptimalt);
        this.groupBoxMedelvarde.Controls.Add(this.label17);
        this.groupBoxMedelvarde.Controls.Add(this.labelMedelvardeOptimalt);
        this.groupBoxMedelvarde.Controls.Add(this.label18);
        this.groupBoxMedelvarde.Controls.Add(this.textBoxMedelvardeVaravBattre);
        this.groupBoxMedelvarde.Controls.Add(this.textBoxMedelvardePlanterade);
        this.groupBoxMedelvarde.Controls.Add(this.label20);
        this.groupBoxMedelvarde.Controls.Add(this.textBoxMedelvardeOvrigt);
        this.groupBoxMedelvarde.Controls.Add(this.label19);
        this.groupBoxMedelvarde.Location = new System.Drawing.Point(0, 357);
        this.groupBoxMedelvarde.Name = "groupBoxMedelvarde";
        this.groupBoxMedelvarde.Size = new System.Drawing.Size(624, 95);
        this.groupBoxMedelvarde.TabIndex = 2;
        this.groupBoxMedelvarde.TabStop = false;
        this.groupBoxMedelvarde.Text = "MEDELVÄRDE";
        // 
        // textBoxMedelvardeTilltryckning
        // 
        this.textBoxMedelvardeTilltryckning.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxMedelvardeTilltryckning.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxMedelvardeTilltryckning.Location = new System.Drawing.Point(517, 60);
        this.textBoxMedelvardeTilltryckning.Name = "textBoxMedelvardeTilltryckning";
        this.textBoxMedelvardeTilltryckning.ReadOnly = true;
        this.textBoxMedelvardeTilltryckning.Size = new System.Drawing.Size(63, 21);
        this.textBoxMedelvardeTilltryckning.TabIndex = 13;
        this.textBoxMedelvardeTilltryckning.TabStop = false;
        this.textBoxMedelvardeTilltryckning.Text = "0";
        this.textBoxMedelvardeTilltryckning.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // textBoxMedelvardeBra
        // 
        this.textBoxMedelvardeBra.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxMedelvardeBra.Location = new System.Drawing.Point(201, 27);
        this.textBoxMedelvardeBra.Name = "textBoxMedelvardeBra";
        this.textBoxMedelvardeBra.ReadOnly = true;
        this.textBoxMedelvardeBra.Size = new System.Drawing.Size(63, 21);
        this.textBoxMedelvardeBra.TabIndex = 3;
        this.textBoxMedelvardeBra.TabStop = false;
        this.textBoxMedelvardeBra.Text = "0";
        this.textBoxMedelvardeBra.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // label16
        // 
        this.label16.AutoSize = true;
        this.label16.Location = new System.Drawing.Point(437, 64);
        this.label16.Name = "label16";
        this.label16.Size = new System.Drawing.Size(80, 13);
        this.label16.TabIndex = 12;
        this.label16.Text = "Tilltryckning:";
        // 
        // labelMedelvardeBra
        // 
        this.labelMedelvardeBra.AutoSize = true;
        this.labelMedelvardeBra.Location = new System.Drawing.Point(170, 30);
        this.labelMedelvardeBra.Name = "labelMedelvardeBra";
        this.labelMedelvardeBra.Size = new System.Drawing.Size(29, 13);
        this.labelMedelvardeBra.TabIndex = 2;
        this.labelMedelvardeBra.Text = "Bra:";
        // 
        // textBoxMedelvardePlanteringsdjupfel
        // 
        this.textBoxMedelvardePlanteringsdjupfel.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxMedelvardePlanteringsdjupfel.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxMedelvardePlanteringsdjupfel.Location = new System.Drawing.Point(363, 60);
        this.textBoxMedelvardePlanteringsdjupfel.Name = "textBoxMedelvardePlanteringsdjupfel";
        this.textBoxMedelvardePlanteringsdjupfel.ReadOnly = true;
        this.textBoxMedelvardePlanteringsdjupfel.Size = new System.Drawing.Size(63, 21);
        this.textBoxMedelvardePlanteringsdjupfel.TabIndex = 11;
        this.textBoxMedelvardePlanteringsdjupfel.TabStop = false;
        this.textBoxMedelvardePlanteringsdjupfel.Text = "0";
        this.textBoxMedelvardePlanteringsdjupfel.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // textBoxMedelvardeOptimalt
        // 
        this.textBoxMedelvardeOptimalt.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxMedelvardeOptimalt.Location = new System.Drawing.Point(83, 27);
        this.textBoxMedelvardeOptimalt.Name = "textBoxMedelvardeOptimalt";
        this.textBoxMedelvardeOptimalt.ReadOnly = true;
        this.textBoxMedelvardeOptimalt.Size = new System.Drawing.Size(63, 21);
        this.textBoxMedelvardeOptimalt.TabIndex = 1;
        this.textBoxMedelvardeOptimalt.TabStop = false;
        this.textBoxMedelvardeOptimalt.Text = "0";
        this.textBoxMedelvardeOptimalt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // label17
        // 
        this.label17.AutoSize = true;
        this.label17.Location = new System.Drawing.Point(249, 64);
        this.label17.Name = "label17";
        this.label17.Size = new System.Drawing.Size(116, 13);
        this.label17.TabIndex = 10;
        this.label17.Text = "Planteringsdjup fel:";
        // 
        // labelMedelvardeOptimalt
        // 
        this.labelMedelvardeOptimalt.AutoSize = true;
        this.labelMedelvardeOptimalt.Location = new System.Drawing.Point(18, 30);
        this.labelMedelvardeOptimalt.Name = "labelMedelvardeOptimalt";
        this.labelMedelvardeOptimalt.Size = new System.Drawing.Size(59, 13);
        this.labelMedelvardeOptimalt.TabIndex = 0;
        this.labelMedelvardeOptimalt.Text = "Optimalt:";
        // 
        // label18
        // 
        this.label18.AutoSize = true;
        this.label18.Location = new System.Drawing.Point(5, 64);
        this.label18.Name = "label18";
        this.label18.Size = new System.Drawing.Size(72, 13);
        this.label18.TabIndex = 8;
        this.label18.Text = "Planterade:";
        // 
        // textBoxMedelvardeVaravBattre
        // 
        this.textBoxMedelvardeVaravBattre.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxMedelvardeVaravBattre.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxMedelvardeVaravBattre.Location = new System.Drawing.Point(363, 27);
        this.textBoxMedelvardeVaravBattre.Name = "textBoxMedelvardeVaravBattre";
        this.textBoxMedelvardeVaravBattre.ReadOnly = true;
        this.textBoxMedelvardeVaravBattre.Size = new System.Drawing.Size(63, 21);
        this.textBoxMedelvardeVaravBattre.TabIndex = 5;
        this.textBoxMedelvardeVaravBattre.TabStop = false;
        this.textBoxMedelvardeVaravBattre.Text = "0";
        this.textBoxMedelvardeVaravBattre.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // textBoxMedelvardePlanterade
        // 
        this.textBoxMedelvardePlanterade.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxMedelvardePlanterade.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxMedelvardePlanterade.Location = new System.Drawing.Point(83, 59);
        this.textBoxMedelvardePlanterade.Name = "textBoxMedelvardePlanterade";
        this.textBoxMedelvardePlanterade.ReadOnly = true;
        this.textBoxMedelvardePlanterade.Size = new System.Drawing.Size(63, 21);
        this.textBoxMedelvardePlanterade.TabIndex = 9;
        this.textBoxMedelvardePlanterade.TabStop = false;
        this.textBoxMedelvardePlanterade.Text = "0";
        this.textBoxMedelvardePlanterade.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // label20
        // 
        this.label20.AutoSize = true;
        this.label20.Location = new System.Drawing.Point(282, 30);
        this.label20.Name = "label20";
        this.label20.Size = new System.Drawing.Size(82, 13);
        this.label20.TabIndex = 4;
        this.label20.Text = "Varav bättre:";
        // 
        // textBoxMedelvardeOvrigt
        // 
        this.textBoxMedelvardeOvrigt.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxMedelvardeOvrigt.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxMedelvardeOvrigt.Location = new System.Drawing.Point(517, 27);
        this.textBoxMedelvardeOvrigt.Name = "textBoxMedelvardeOvrigt";
        this.textBoxMedelvardeOvrigt.ReadOnly = true;
        this.textBoxMedelvardeOvrigt.Size = new System.Drawing.Size(63, 21);
        this.textBoxMedelvardeOvrigt.TabIndex = 7;
        this.textBoxMedelvardeOvrigt.TabStop = false;
        this.textBoxMedelvardeOvrigt.Text = "0";
        this.textBoxMedelvardeOvrigt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // label19
        // 
        this.label19.AutoSize = true;
        this.label19.Location = new System.Drawing.Point(472, 30);
        this.label19.Name = "label19";
        this.label19.Size = new System.Drawing.Size(45, 13);
        this.label19.TabIndex = 6;
        this.label19.Text = "Övrigt:";
        // 
        // groupBoxSumma
        // 
        this.groupBoxSumma.Controls.Add(this.textBoxSummaTilltryckning);
        this.groupBoxSumma.Controls.Add(this.label15);
        this.groupBoxSumma.Controls.Add(this.textBoxSummaPlanteringsdjupfel);
        this.groupBoxSumma.Controls.Add(this.label14);
        this.groupBoxSumma.Controls.Add(this.label13);
        this.groupBoxSumma.Controls.Add(this.textBoxSummaPlanterade);
        this.groupBoxSumma.Controls.Add(this.textBoxSummaOptimalt);
        this.groupBoxSumma.Controls.Add(this.textBoxSummaOvrigt);
        this.groupBoxSumma.Controls.Add(this.labelSummaOptimaltBra);
        this.groupBoxSumma.Controls.Add(this.textBoxSummaVaravBattre);
        this.groupBoxSumma.Controls.Add(this.labelSummaBlekjordsflack);
        this.groupBoxSumma.Controls.Add(this.textBoxSummaBra);
        this.groupBoxSumma.Controls.Add(this.labelSummaBra);
        this.groupBoxSumma.Controls.Add(this.labelSummaOptimalt);
        this.groupBoxSumma.Location = new System.Drawing.Point(0, 276);
        this.groupBoxSumma.Name = "groupBoxSumma";
        this.groupBoxSumma.Size = new System.Drawing.Size(624, 91);
        this.groupBoxSumma.TabIndex = 1;
        this.groupBoxSumma.TabStop = false;
        this.groupBoxSumma.Text = "SUMMA";
        // 
        // textBoxSummaTilltryckning
        // 
        this.textBoxSummaTilltryckning.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxSummaTilltryckning.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxSummaTilltryckning.Location = new System.Drawing.Point(517, 56);
        this.textBoxSummaTilltryckning.Name = "textBoxSummaTilltryckning";
        this.textBoxSummaTilltryckning.ReadOnly = true;
        this.textBoxSummaTilltryckning.Size = new System.Drawing.Size(63, 21);
        this.textBoxSummaTilltryckning.TabIndex = 13;
        this.textBoxSummaTilltryckning.TabStop = false;
        this.textBoxSummaTilltryckning.Text = "0";
        this.textBoxSummaTilltryckning.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // label15
        // 
        this.label15.AutoSize = true;
        this.label15.Location = new System.Drawing.Point(437, 59);
        this.label15.Name = "label15";
        this.label15.Size = new System.Drawing.Size(80, 13);
        this.label15.TabIndex = 12;
        this.label15.Text = "Tilltryckning:";
        // 
        // textBoxSummaPlanteringsdjupfel
        // 
        this.textBoxSummaPlanteringsdjupfel.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxSummaPlanteringsdjupfel.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxSummaPlanteringsdjupfel.Location = new System.Drawing.Point(363, 56);
        this.textBoxSummaPlanteringsdjupfel.Name = "textBoxSummaPlanteringsdjupfel";
        this.textBoxSummaPlanteringsdjupfel.ReadOnly = true;
        this.textBoxSummaPlanteringsdjupfel.Size = new System.Drawing.Size(63, 21);
        this.textBoxSummaPlanteringsdjupfel.TabIndex = 11;
        this.textBoxSummaPlanteringsdjupfel.TabStop = false;
        this.textBoxSummaPlanteringsdjupfel.Text = "0";
        this.textBoxSummaPlanteringsdjupfel.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // label14
        // 
        this.label14.AutoSize = true;
        this.label14.Location = new System.Drawing.Point(249, 59);
        this.label14.Name = "label14";
        this.label14.Size = new System.Drawing.Size(116, 13);
        this.label14.TabIndex = 10;
        this.label14.Text = "Planteringsdjup fel:";
        // 
        // label13
        // 
        this.label13.AutoSize = true;
        this.label13.Location = new System.Drawing.Point(11, 59);
        this.label13.Name = "label13";
        this.label13.Size = new System.Drawing.Size(72, 13);
        this.label13.TabIndex = 8;
        this.label13.Text = "Planterade:";
        // 
        // textBoxSummaPlanterade
        // 
        this.textBoxSummaPlanterade.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxSummaPlanterade.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxSummaPlanterade.Location = new System.Drawing.Point(83, 56);
        this.textBoxSummaPlanterade.Name = "textBoxSummaPlanterade";
        this.textBoxSummaPlanterade.ReadOnly = true;
        this.textBoxSummaPlanterade.Size = new System.Drawing.Size(63, 21);
        this.textBoxSummaPlanterade.TabIndex = 9;
        this.textBoxSummaPlanterade.TabStop = false;
        this.textBoxSummaPlanterade.Text = "0";
        this.textBoxSummaPlanterade.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // textBoxSummaOptimalt
        // 
        this.textBoxSummaOptimalt.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxSummaOptimalt.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxSummaOptimalt.Location = new System.Drawing.Point(83, 24);
        this.textBoxSummaOptimalt.Name = "textBoxSummaOptimalt";
        this.textBoxSummaOptimalt.ReadOnly = true;
        this.textBoxSummaOptimalt.Size = new System.Drawing.Size(63, 21);
        this.textBoxSummaOptimalt.TabIndex = 1;
        this.textBoxSummaOptimalt.TabStop = false;
        this.textBoxSummaOptimalt.Text = "0";
        this.textBoxSummaOptimalt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // textBoxSummaOvrigt
        // 
        this.textBoxSummaOvrigt.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxSummaOvrigt.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxSummaOvrigt.Location = new System.Drawing.Point(517, 24);
        this.textBoxSummaOvrigt.Name = "textBoxSummaOvrigt";
        this.textBoxSummaOvrigt.ReadOnly = true;
        this.textBoxSummaOvrigt.Size = new System.Drawing.Size(63, 21);
        this.textBoxSummaOvrigt.TabIndex = 7;
        this.textBoxSummaOvrigt.TabStop = false;
        this.textBoxSummaOvrigt.Text = "0";
        this.textBoxSummaOvrigt.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // labelSummaOptimaltBra
        // 
        this.labelSummaOptimaltBra.AutoSize = true;
        this.labelSummaOptimaltBra.Location = new System.Drawing.Point(472, 27);
        this.labelSummaOptimaltBra.Name = "labelSummaOptimaltBra";
        this.labelSummaOptimaltBra.Size = new System.Drawing.Size(45, 13);
        this.labelSummaOptimaltBra.TabIndex = 6;
        this.labelSummaOptimaltBra.Text = "Övrigt:";
        // 
        // textBoxSummaVaravBattre
        // 
        this.textBoxSummaVaravBattre.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxSummaVaravBattre.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxSummaVaravBattre.Location = new System.Drawing.Point(363, 24);
        this.textBoxSummaVaravBattre.Name = "textBoxSummaVaravBattre";
        this.textBoxSummaVaravBattre.ReadOnly = true;
        this.textBoxSummaVaravBattre.Size = new System.Drawing.Size(63, 21);
        this.textBoxSummaVaravBattre.TabIndex = 5;
        this.textBoxSummaVaravBattre.TabStop = false;
        this.textBoxSummaVaravBattre.Text = "0";
        this.textBoxSummaVaravBattre.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // labelSummaBlekjordsflack
        // 
        this.labelSummaBlekjordsflack.AutoSize = true;
        this.labelSummaBlekjordsflack.Location = new System.Drawing.Point(283, 27);
        this.labelSummaBlekjordsflack.Name = "labelSummaBlekjordsflack";
        this.labelSummaBlekjordsflack.Size = new System.Drawing.Size(82, 13);
        this.labelSummaBlekjordsflack.TabIndex = 4;
        this.labelSummaBlekjordsflack.Text = "Varav bättre:";
        // 
        // textBoxSummaBra
        // 
        this.textBoxSummaBra.BackColor = System.Drawing.SystemColors.Info;
        this.textBoxSummaBra.ForeColor = System.Drawing.SystemColors.WindowText;
        this.textBoxSummaBra.Location = new System.Drawing.Point(201, 24);
        this.textBoxSummaBra.Name = "textBoxSummaBra";
        this.textBoxSummaBra.ReadOnly = true;
        this.textBoxSummaBra.Size = new System.Drawing.Size(63, 21);
        this.textBoxSummaBra.TabIndex = 3;
        this.textBoxSummaBra.TabStop = false;
        this.textBoxSummaBra.Text = "0";
        this.textBoxSummaBra.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
        // 
        // labelSummaBra
        // 
        this.labelSummaBra.AutoSize = true;
        this.labelSummaBra.Location = new System.Drawing.Point(171, 27);
        this.labelSummaBra.Name = "labelSummaBra";
        this.labelSummaBra.Size = new System.Drawing.Size(29, 13);
        this.labelSummaBra.TabIndex = 2;
        this.labelSummaBra.Text = "Bra:";
        // 
        // labelSummaOptimalt
        // 
        this.labelSummaOptimalt.AutoSize = true;
        this.labelSummaOptimalt.Location = new System.Drawing.Point(24, 27);
        this.labelSummaOptimalt.Name = "labelSummaOptimalt";
        this.labelSummaOptimalt.Size = new System.Drawing.Size(59, 13);
        this.labelSummaOptimalt.TabIndex = 0;
        this.labelSummaOptimalt.Text = "Optimalt:";
        // 
        // dataGridViewPlanteringsstallen
        // 
        this.dataGridViewPlanteringsstallen.AllowUserToResizeColumns = false;
        this.dataGridViewPlanteringsstallen.AllowUserToResizeRows = false;
        dataGridViewCellStyle25.Format = "N0";
        dataGridViewCellStyle25.NullValue = null;
        this.dataGridViewPlanteringsstallen.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle25;
        this.dataGridViewPlanteringsstallen.AutoGenerateColumns = false;
        this.dataGridViewPlanteringsstallen.BackgroundColor = System.Drawing.SystemColors.ControlDark;
        this.dataGridViewPlanteringsstallen.BorderStyle = System.Windows.Forms.BorderStyle.None;
        dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.TopCenter;
        dataGridViewCellStyle26.BackColor = System.Drawing.SystemColors.Control;
        dataGridViewCellStyle26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        dataGridViewCellStyle26.ForeColor = System.Drawing.SystemColors.WindowText;
        dataGridViewCellStyle26.NullValue = null;
        dataGridViewCellStyle26.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle26.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
        this.dataGridViewPlanteringsstallen.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle26;
        this.dataGridViewPlanteringsstallen.ColumnHeadersHeight = 68;
        this.dataGridViewPlanteringsstallen.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
        this.dataGridViewPlanteringsstallen.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ytaDataGridViewTextBoxColumn,
            this.optimaltDataGridViewTextBoxColumn,
            this.braDataGridViewTextBoxColumn,
            this.varavBattreDataGridViewTextBoxColumn,
            this.ovrigtDataGridViewTextBoxColumn,
            this.planteradeDataGridViewTextBoxColumn,
            this.planteringsDjupFelDataGridViewTextBoxColumn,
            this.tilltryckningDataGridViewTextBoxColumn});
        this.dataGridViewPlanteringsstallen.DataMember = "Yta";
        this.dataGridViewPlanteringsstallen.DataSource = this.dataSetPlantering;
        dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
        dataGridViewCellStyle35.BackColor = System.Drawing.SystemColors.Window;
        dataGridViewCellStyle35.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        dataGridViewCellStyle35.ForeColor = System.Drawing.SystemColors.ControlText;
        dataGridViewCellStyle35.Format = "N0";
        dataGridViewCellStyle35.NullValue = null;
        dataGridViewCellStyle35.SelectionBackColor = System.Drawing.SystemColors.Highlight;
        dataGridViewCellStyle35.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
        dataGridViewCellStyle35.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
        this.dataGridViewPlanteringsstallen.DefaultCellStyle = dataGridViewCellStyle35;
        this.dataGridViewPlanteringsstallen.Location = new System.Drawing.Point(16, 29);
        this.dataGridViewPlanteringsstallen.Name = "dataGridViewPlanteringsstallen";
        this.dataGridViewPlanteringsstallen.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
        dataGridViewCellStyle36.Format = "N0";
        dataGridViewCellStyle36.NullValue = null;
        this.dataGridViewPlanteringsstallen.RowsDefaultCellStyle = dataGridViewCellStyle36;
        this.dataGridViewPlanteringsstallen.RowTemplate.DefaultCellStyle.Format = "N0";
        this.dataGridViewPlanteringsstallen.RowTemplate.DefaultCellStyle.NullValue = null;
        this.dataGridViewPlanteringsstallen.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
        this.dataGridViewPlanteringsstallen.Size = new System.Drawing.Size(592, 244);
        this.dataGridViewPlanteringsstallen.TabIndex = 0;
        this.dataGridViewPlanteringsstallen.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.dataGridViewPlanteringsstallen_UserDeletingRow);
        this.dataGridViewPlanteringsstallen.UserDeletedRow += new System.Windows.Forms.DataGridViewRowEventHandler(this.dataGridViewPlanteringsstallen_UserDeletedRow);
        this.dataGridViewPlanteringsstallen.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this.dataGridViewPlanteringsstallen_CellValidating);
        this.dataGridViewPlanteringsstallen.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this.dataGridViewPlanteringsstallen_RowsAdded);
        this.dataGridViewPlanteringsstallen.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewPlanteringsstallen_CellEndEdit);
        this.dataGridViewPlanteringsstallen.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewPlanteringsstallen_DataError);
        // 
        // ytaDataGridViewTextBoxColumn
        // 
        this.ytaDataGridViewTextBoxColumn.DataPropertyName = "Yta";
        dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle27.BackColor = System.Drawing.Color.LemonChiffon;
        dataGridViewCellStyle27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.ytaDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle27;
        this.ytaDataGridViewTextBoxColumn.HeaderText = "Yta";
        this.ytaDataGridViewTextBoxColumn.Name = "ytaDataGridViewTextBoxColumn";
        this.ytaDataGridViewTextBoxColumn.ReadOnly = true;
        this.ytaDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
        this.ytaDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        this.ytaDataGridViewTextBoxColumn.Width = 50;
        // 
        // optimaltDataGridViewTextBoxColumn
        // 
        this.optimaltDataGridViewTextBoxColumn.DataPropertyName = "Optimalt";
        dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle28.BackColor = System.Drawing.Color.White;
        dataGridViewCellStyle28.NullValue = null;
        this.optimaltDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle28;
        this.optimaltDataGridViewTextBoxColumn.HeaderText = "Optimalt";
        this.optimaltDataGridViewTextBoxColumn.Name = "optimaltDataGridViewTextBoxColumn";
        this.optimaltDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
        this.optimaltDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        this.optimaltDataGridViewTextBoxColumn.Width = 65;
        // 
        // braDataGridViewTextBoxColumn
        // 
        this.braDataGridViewTextBoxColumn.DataPropertyName = "Bra";
        dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle29.BackColor = System.Drawing.Color.White;
        dataGridViewCellStyle29.NullValue = null;
        this.braDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle29;
        this.braDataGridViewTextBoxColumn.HeaderText = "Bra";
        this.braDataGridViewTextBoxColumn.Name = "braDataGridViewTextBoxColumn";
        this.braDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
        this.braDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        this.braDataGridViewTextBoxColumn.Width = 65;
        // 
        // varavBattreDataGridViewTextBoxColumn
        // 
        this.varavBattreDataGridViewTextBoxColumn.DataPropertyName = "VaravBattre";
        dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle30.BackColor = System.Drawing.Color.White;
        dataGridViewCellStyle30.NullValue = null;
        this.varavBattreDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle30;
        this.varavBattreDataGridViewTextBoxColumn.HeaderText = "Varav bättre inom 1m radie";
        this.varavBattreDataGridViewTextBoxColumn.Name = "varavBattreDataGridViewTextBoxColumn";
        this.varavBattreDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
        this.varavBattreDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        this.varavBattreDataGridViewTextBoxColumn.Width = 75;
        // 
        // ovrigtDataGridViewTextBoxColumn
        // 
        this.ovrigtDataGridViewTextBoxColumn.DataPropertyName = "Ovrigt";
        dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle31.BackColor = System.Drawing.Color.White;
        dataGridViewCellStyle31.NullValue = null;
        this.ovrigtDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle31;
        this.ovrigtDataGridViewTextBoxColumn.HeaderText = "Övrigt";
        this.ovrigtDataGridViewTextBoxColumn.Name = "ovrigtDataGridViewTextBoxColumn";
        this.ovrigtDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
        this.ovrigtDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        this.ovrigtDataGridViewTextBoxColumn.Width = 65;
        // 
        // planteradeDataGridViewTextBoxColumn
        // 
        this.planteradeDataGridViewTextBoxColumn.DataPropertyName = "Planterade";
        dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle32.BackColor = System.Drawing.Color.LemonChiffon;
        this.planteradeDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle32;
        this.planteradeDataGridViewTextBoxColumn.HeaderText = "Planterade";
        this.planteradeDataGridViewTextBoxColumn.Name = "planteradeDataGridViewTextBoxColumn";
        this.planteradeDataGridViewTextBoxColumn.ReadOnly = true;
        this.planteradeDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
        this.planteradeDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        this.planteradeDataGridViewTextBoxColumn.Width = 75;
        // 
        // planteringsDjupFelDataGridViewTextBoxColumn
        // 
        this.planteringsDjupFelDataGridViewTextBoxColumn.DataPropertyName = "PlanteringsdjupFel";
        dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle33.BackColor = System.Drawing.Color.White;
        this.planteringsDjupFelDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle33;
        this.planteringsDjupFelDataGridViewTextBoxColumn.HeaderText = "Planterings djup fel";
        this.planteringsDjupFelDataGridViewTextBoxColumn.Name = "planteringsDjupFelDataGridViewTextBoxColumn";
        this.planteringsDjupFelDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
        this.planteringsDjupFelDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        this.planteringsDjupFelDataGridViewTextBoxColumn.Width = 75;
        // 
        // tilltryckningDataGridViewTextBoxColumn
        // 
        this.tilltryckningDataGridViewTextBoxColumn.DataPropertyName = "Tilltryckning";
        dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
        dataGridViewCellStyle34.BackColor = System.Drawing.Color.White;
        this.tilltryckningDataGridViewTextBoxColumn.DefaultCellStyle = dataGridViewCellStyle34;
        this.tilltryckningDataGridViewTextBoxColumn.HeaderText = "Till- tryckning";
        this.tilltryckningDataGridViewTextBoxColumn.Name = "tilltryckningDataGridViewTextBoxColumn";
        this.tilltryckningDataGridViewTextBoxColumn.Resizable = System.Windows.Forms.DataGridViewTriState.False;
        this.tilltryckningDataGridViewTextBoxColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
        this.tilltryckningDataGridViewTextBoxColumn.Width = 65;
        // 
        // tabPageFrågor
        // 
        this.tabPageFrågor.Controls.Add(this.groupBoxTrädslag);
        this.tabPageFrågor.Controls.Add(this.groupBoxStambrev);
        this.tabPageFrågor.Controls.Add(this.groupBoxStorlek);
        this.tabPageFrågor.Controls.Add(this.groupBoxAntal);
        this.tabPageFrågor.Controls.Add(this.groupBox5);
        this.tabPageFrågor.Controls.Add(this.groupBoxOBS);
        this.tabPageFrågor.Controls.Add(this.groupBox4);
        this.tabPageFrågor.Controls.Add(this.groupBox3);
        this.tabPageFrågor.Location = new System.Drawing.Point(4, 22);
        this.tabPageFrågor.Name = "tabPageFrågor";
        this.tabPageFrågor.Padding = new System.Windows.Forms.Padding(3);
        this.tabPageFrågor.Size = new System.Drawing.Size(644, 468);
        this.tabPageFrågor.TabIndex = 1;
        this.tabPageFrågor.Text = "Frågor";
        this.tabPageFrågor.UseVisualStyleBackColor = true;
        // 
        // groupBoxTrädslag
        // 
        this.groupBoxTrädslag.Controls.Add(this.labelContorta);
        this.groupBoxTrädslag.Controls.Add(this.labelTall);
        this.groupBoxTrädslag.Controls.Add(this.labelGran);
        this.groupBoxTrädslag.Location = new System.Drawing.Point(11, 231);
        this.groupBoxTrädslag.Name = "groupBoxTrädslag";
        this.groupBoxTrädslag.Size = new System.Drawing.Size(68, 101);
        this.groupBoxTrädslag.TabIndex = 3;
        this.groupBoxTrädslag.TabStop = false;
        // 
        // labelContorta
        // 
        this.labelContorta.AutoSize = true;
        this.labelContorta.Location = new System.Drawing.Point(6, 77);
        this.labelContorta.Name = "labelContorta";
        this.labelContorta.Size = new System.Drawing.Size(57, 13);
        this.labelContorta.TabIndex = 2;
        this.labelContorta.Text = "Contorta";
        // 
        // labelTall
        // 
        this.labelTall.AutoSize = true;
        this.labelTall.Location = new System.Drawing.Point(35, 23);
        this.labelTall.Name = "labelTall";
        this.labelTall.Size = new System.Drawing.Size(27, 13);
        this.labelTall.TabIndex = 0;
        this.labelTall.Text = "Tall";
        // 
        // labelGran
        // 
        this.labelGran.AutoSize = true;
        this.labelGran.Location = new System.Drawing.Point(28, 50);
        this.labelGran.Name = "labelGran";
        this.labelGran.Size = new System.Drawing.Size(34, 13);
        this.labelGran.TabIndex = 1;
        this.labelGran.Text = "Gran";
        // 
        // groupBoxStambrev
        // 
        this.groupBoxStambrev.Controls.Add(this.textBoxContortaStambrev);
        this.groupBoxStambrev.Controls.Add(this.textBoxGranStambrev);
        this.groupBoxStambrev.Controls.Add(this.textBoxTallStambrev);
        this.groupBoxStambrev.Location = new System.Drawing.Point(243, 231);
        this.groupBoxStambrev.Name = "groupBoxStambrev";
        this.groupBoxStambrev.Size = new System.Drawing.Size(308, 101);
        this.groupBoxStambrev.TabIndex = 6;
        this.groupBoxStambrev.TabStop = false;
        this.groupBoxStambrev.Text = "Stambrev/proveniens";
        // 
        // textBoxContortaStambrev
        // 
        this.textBoxContortaStambrev.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetPlantering, "Fragor.Fraga11", true));
        this.textBoxContortaStambrev.Location = new System.Drawing.Point(6, 74);
        this.textBoxContortaStambrev.MaxLength = 32;
        this.textBoxContortaStambrev.Name = "textBoxContortaStambrev";
        this.textBoxContortaStambrev.Size = new System.Drawing.Size(291, 21);
        this.textBoxContortaStambrev.TabIndex = 2;
        this.textBoxContortaStambrev.TextChanged += new System.EventHandler(this.textBoxContortaStambrev_TextChanged);
        // 
        // textBoxGranStambrev
        // 
        this.textBoxGranStambrev.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetPlantering, "Fragor.Fraga8", true));
        this.textBoxGranStambrev.Location = new System.Drawing.Point(6, 47);
        this.textBoxGranStambrev.MaxLength = 32;
        this.textBoxGranStambrev.Name = "textBoxGranStambrev";
        this.textBoxGranStambrev.Size = new System.Drawing.Size(291, 21);
        this.textBoxGranStambrev.TabIndex = 1;
        this.textBoxGranStambrev.TextChanged += new System.EventHandler(this.textBoxGranStambrev_TextChanged);
        // 
        // textBoxTallStambrev
        // 
        this.textBoxTallStambrev.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetPlantering, "Fragor.Fraga5", true));
        this.textBoxTallStambrev.Location = new System.Drawing.Point(6, 20);
        this.textBoxTallStambrev.MaxLength = 32;
        this.textBoxTallStambrev.Name = "textBoxTallStambrev";
        this.textBoxTallStambrev.Size = new System.Drawing.Size(291, 21);
        this.textBoxTallStambrev.TabIndex = 0;
        this.textBoxTallStambrev.TextChanged += new System.EventHandler(this.textBoxTallStambrev_TextChanged);
        // 
        // groupBoxStorlek
        // 
        this.groupBoxStorlek.Controls.Add(this.comboBoxStorlekContorta);
        this.groupBoxStorlek.Controls.Add(this.comboBoxStorlekGran);
        this.groupBoxStorlek.Controls.Add(this.comboBoxStorlekTall);
        this.groupBoxStorlek.Location = new System.Drawing.Point(160, 231);
        this.groupBoxStorlek.Name = "groupBoxStorlek";
        this.groupBoxStorlek.Size = new System.Drawing.Size(85, 101);
        this.groupBoxStorlek.TabIndex = 5;
        this.groupBoxStorlek.TabStop = false;
        this.groupBoxStorlek.Text = "Planttyp";
        // 
        // comboBoxStorlekContorta
        // 
        this.comboBoxStorlekContorta.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetPlantering, "Fragor.Fraga10", true));
        this.comboBoxStorlekContorta.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxStorlekContorta.FormattingEnabled = true;
        this.comboBoxStorlekContorta.Items.AddRange(new object[] {
            "S 50",
            "S 90",
            "S 120",
            "Barrot",
            "Annan"});
        this.comboBoxStorlekContorta.Location = new System.Drawing.Point(6, 74);
        this.comboBoxStorlekContorta.Name = "comboBoxStorlekContorta";
        this.comboBoxStorlekContorta.Size = new System.Drawing.Size(70, 21);
        this.comboBoxStorlekContorta.TabIndex = 2;
        this.comboBoxStorlekContorta.SelectionChangeCommitted += new System.EventHandler(this.comboBoxStorlekContorta_SelectionChangeCommitted);
        // 
        // comboBoxStorlekGran
        // 
        this.comboBoxStorlekGran.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetPlantering, "Fragor.Fraga7", true));
        this.comboBoxStorlekGran.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxStorlekGran.FormattingEnabled = true;
        this.comboBoxStorlekGran.Items.AddRange(new object[] {
            "S 50",
            "S 90",
            "S 120",
            "Barrot",
            "Annan"});
        this.comboBoxStorlekGran.Location = new System.Drawing.Point(7, 47);
        this.comboBoxStorlekGran.Name = "comboBoxStorlekGran";
        this.comboBoxStorlekGran.Size = new System.Drawing.Size(70, 21);
        this.comboBoxStorlekGran.TabIndex = 1;
        this.comboBoxStorlekGran.SelectionChangeCommitted += new System.EventHandler(this.comboBoxStorlekGran_SelectionChangeCommitted);
        // 
        // comboBoxStorlekTall
        // 
        this.comboBoxStorlekTall.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetPlantering, "Fragor.Fraga4", true));
        this.comboBoxStorlekTall.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
        this.comboBoxStorlekTall.FormattingEnabled = true;
        this.comboBoxStorlekTall.Items.AddRange(new object[] {
            "S 50",
            "S 90",
            "S 120",
            "Barrot",
            "Annan"});
        this.comboBoxStorlekTall.Location = new System.Drawing.Point(7, 20);
        this.comboBoxStorlekTall.Name = "comboBoxStorlekTall";
        this.comboBoxStorlekTall.Size = new System.Drawing.Size(70, 21);
        this.comboBoxStorlekTall.TabIndex = 0;
        this.comboBoxStorlekTall.SelectionChangeCommitted += new System.EventHandler(this.comboBoxStorlekTall_SelectionChangeCommitted);
        // 
        // groupBoxAntal
        // 
        this.groupBoxAntal.Controls.Add(this.textBoxAntalContorta);
        this.groupBoxAntal.Controls.Add(this.textBoxAntalGran);
        this.groupBoxAntal.Controls.Add(this.textBoxAntalTall);
        this.groupBoxAntal.Location = new System.Drawing.Point(77, 231);
        this.groupBoxAntal.Name = "groupBoxAntal";
        this.groupBoxAntal.Size = new System.Drawing.Size(85, 101);
        this.groupBoxAntal.TabIndex = 4;
        this.groupBoxAntal.TabStop = false;
        this.groupBoxAntal.Text = "Antal";
        // 
        // textBoxAntalContorta
        // 
        this.textBoxAntalContorta.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetPlantering, "Fragor.Fraga9", true));
        this.textBoxAntalContorta.Location = new System.Drawing.Point(7, 74);
        this.textBoxAntalContorta.MaxLength = 6;
        this.textBoxAntalContorta.Name = "textBoxAntalContorta";
        this.textBoxAntalContorta.Size = new System.Drawing.Size(70, 21);
        this.textBoxAntalContorta.TabIndex = 2;
        this.textBoxAntalContorta.TextChanged += new System.EventHandler(this.textBoxAntalContorta_TextChanged);
        this.textBoxAntalContorta.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxAntalContorta_KeyDown);
        this.textBoxAntalContorta.Leave += new System.EventHandler(this.textBoxAntalContorta_Leave);
        // 
        // textBoxAntalGran
        // 
        this.textBoxAntalGran.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetPlantering, "Fragor.Fraga6", true));
        this.textBoxAntalGran.Location = new System.Drawing.Point(7, 47);
        this.textBoxAntalGran.MaxLength = 6;
        this.textBoxAntalGran.Name = "textBoxAntalGran";
        this.textBoxAntalGran.Size = new System.Drawing.Size(70, 21);
        this.textBoxAntalGran.TabIndex = 1;
        this.textBoxAntalGran.TextChanged += new System.EventHandler(this.textBoxAntalGran_TextChanged);
        this.textBoxAntalGran.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxAntalGran_KeyDown);
        this.textBoxAntalGran.Leave += new System.EventHandler(this.textBoxAntalGran_Leave);
        // 
        // textBoxAntalTall
        // 
        this.textBoxAntalTall.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetPlantering, "Fragor.Fraga3", true));
        this.textBoxAntalTall.Location = new System.Drawing.Point(7, 20);
        this.textBoxAntalTall.MaxLength = 6;
        this.textBoxAntalTall.Name = "textBoxAntalTall";
        this.textBoxAntalTall.Size = new System.Drawing.Size(70, 21);
        this.textBoxAntalTall.TabIndex = 0;
        this.textBoxAntalTall.TextChanged += new System.EventHandler(this.textBoxAntalTall_TextChanged);
        this.textBoxAntalTall.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBoxAntalTall_KeyDown);
        this.textBoxAntalTall.Leave += new System.EventHandler(this.textBoxAntalTall_Leave);
        // 
        // groupBox5
        // 
        this.groupBox5.Controls.Add(this.richTextBoxKommentar);
        this.groupBox5.Location = new System.Drawing.Point(10, 353);
        this.groupBox5.Name = "groupBox5";
        this.groupBox5.Size = new System.Drawing.Size(540, 100);
        this.groupBox5.TabIndex = 7;
        this.groupBox5.TabStop = false;
        this.groupBox5.Text = "KOMMENTARER";
        // 
        // richTextBoxKommentar
        // 
        this.richTextBoxKommentar.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.dataSetPlantering, "UserData.Kommentar", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
        this.richTextBoxKommentar.Location = new System.Drawing.Point(14, 20);
        this.richTextBoxKommentar.MaxLength = 4095;
        this.richTextBoxKommentar.Name = "richTextBoxKommentar";
        this.richTextBoxKommentar.Size = new System.Drawing.Size(515, 68);
        this.richTextBoxKommentar.TabIndex = 0;
        this.richTextBoxKommentar.Text = "";
        this.richTextBoxKommentar.TextChanged += new System.EventHandler(this.richTextBoxKommentar_TextChanged);
        // 
        // groupBoxOBS
        // 
        this.groupBoxOBS.Controls.Add(this.label1);
        this.groupBoxOBS.Controls.Add(this.label2);
        this.groupBoxOBS.Location = new System.Drawing.Point(11, 148);
        this.groupBoxOBS.Name = "groupBoxOBS";
        this.groupBoxOBS.Size = new System.Drawing.Size(539, 66);
        this.groupBoxOBS.TabIndex = 2;
        this.groupBoxOBS.TabStop = false;
        // 
        // label1
        // 
        this.label1.AutoSize = true;
        this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label1.Location = new System.Drawing.Point(24, 38);
        this.label1.Name = "label1";
        this.label1.Size = new System.Drawing.Size(456, 13);
        this.label1.TabIndex = 1;
        this.label1.Text = "Ifall något ”Nej” har valts på någon av frågorna så skall en miljörapport skrivas" +
            "!";
        // 
        // label2
        // 
        this.label2.AutoSize = true;
        this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label2.Location = new System.Drawing.Point(19, 17);
        this.label2.Name = "label2";
        this.label2.Size = new System.Drawing.Size(32, 13);
        this.label2.TabIndex = 0;
        this.label2.Text = "OBS!";
        // 
        // groupBox4
        // 
        this.groupBox4.Controls.Add(this.radioButton2Nej);
        this.groupBox4.Controls.Add(this.radioButton2Ja);
        this.groupBox4.Controls.Add(this.label9);
        this.groupBox4.Location = new System.Drawing.Point(11, 99);
        this.groupBox4.Name = "groupBox4";
        this.groupBox4.Size = new System.Drawing.Size(539, 55);
        this.groupBox4.TabIndex = 1;
        this.groupBox4.TabStop = false;
        this.groupBox4.Text = "2.";
        // 
        // radioButton2Nej
        // 
        this.radioButton2Nej.AutoSize = true;
        this.radioButton2Nej.Location = new System.Drawing.Point(474, 19);
        this.radioButton2Nej.Name = "radioButton2Nej";
        this.radioButton2Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton2Nej.TabIndex = 2;
        this.radioButton2Nej.TabStop = true;
        this.radioButton2Nej.Text = "Nej";
        this.radioButton2Nej.UseVisualStyleBackColor = true;
        this.radioButton2Nej.CheckedChanged += new System.EventHandler(this.radioButton2Nej_CheckedChanged);
        // 
        // radioButton2Ja
        // 
        this.radioButton2Ja.AutoSize = true;
        this.radioButton2Ja.Location = new System.Drawing.Point(424, 19);
        this.radioButton2Ja.Name = "radioButton2Ja";
        this.radioButton2Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton2Ja.TabIndex = 1;
        this.radioButton2Ja.TabStop = true;
        this.radioButton2Ja.Text = "Ja";
        this.radioButton2Ja.UseVisualStyleBackColor = true;
        this.radioButton2Ja.CheckedChanged += new System.EventHandler(this.radioButton2Ja_CheckedChanged);
        // 
        // label9
        // 
        this.label9.AutoSize = true;
        this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label9.Location = new System.Drawing.Point(19, 21);
        this.label9.Name = "label9";
        this.label9.Size = new System.Drawing.Size(128, 13);
        this.label9.TabIndex = 0;
        this.label9.Text = "Är trakten avstädad?";
        // 
        // groupBox3
        // 
        this.groupBox3.Controls.Add(this.radioButton1Nej);
        this.groupBox3.Controls.Add(this.radioButton1Ja);
        this.groupBox3.Controls.Add(this.label10);
        this.groupBox3.Location = new System.Drawing.Point(10, 6);
        this.groupBox3.Name = "groupBox3";
        this.groupBox3.Size = new System.Drawing.Size(540, 55);
        this.groupBox3.TabIndex = 0;
        this.groupBox3.TabStop = false;
        this.groupBox3.Text = "1.";
        // 
        // radioButton1Nej
        // 
        this.radioButton1Nej.AutoSize = true;
        this.radioButton1Nej.Location = new System.Drawing.Point(475, 20);
        this.radioButton1Nej.Name = "radioButton1Nej";
        this.radioButton1Nej.Size = new System.Drawing.Size(43, 17);
        this.radioButton1Nej.TabIndex = 2;
        this.radioButton1Nej.TabStop = true;
        this.radioButton1Nej.Text = "Nej";
        this.radioButton1Nej.UseVisualStyleBackColor = true;
        this.radioButton1Nej.CheckedChanged += new System.EventHandler(this.radioButton1Nej_CheckedChanged);
        // 
        // radioButton1Ja
        // 
        this.radioButton1Ja.AutoSize = true;
        this.radioButton1Ja.Location = new System.Drawing.Point(425, 20);
        this.radioButton1Ja.Name = "radioButton1Ja";
        this.radioButton1Ja.Size = new System.Drawing.Size(38, 17);
        this.radioButton1Ja.TabIndex = 1;
        this.radioButton1Ja.TabStop = true;
        this.radioButton1Ja.Text = "Ja";
        this.radioButton1Ja.UseVisualStyleBackColor = true;
        this.radioButton1Ja.CheckedChanged += new System.EventHandler(this.radioButton1Ja_CheckedChanged);
        // 
        // label10
        // 
        this.label10.AutoSize = true;
        this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.label10.Location = new System.Drawing.Point(19, 22);
        this.label10.Name = "label10";
        this.label10.Size = new System.Drawing.Size(137, 13);
        this.label10.TabIndex = 0;
        this.label10.Text = "Är traktdirektiv följda?";
        // 
        // environmentPlantering
        // 
        designerSettings3.ApplicationConnection = null;
        designerSettings3.DefaultFont = new System.Drawing.Font("Arial", 10F);
        designerSettings3.Icon = ((System.Drawing.Icon)(resources.GetObject("designerSettings3.Icon")));
        designerSettings3.Restrictions = designerRestrictions3;
        designerSettings3.Text = "";
        this.environmentPlantering.DesignerSettings = designerSettings3;
        emailSettings3.Address = "";
        emailSettings3.Host = "";
        emailSettings3.MessageTemplate = "";
        emailSettings3.Name = "";
        emailSettings3.Password = "";
        emailSettings3.Port = 49;
        emailSettings3.UserName = "";
        this.environmentPlantering.EmailSettings = emailSettings3;
        previewSettings3.Buttons = ((FastReport.PreviewButtons)((((((FastReport.PreviewButtons.Print | FastReport.PreviewButtons.Save)
                    | FastReport.PreviewButtons.Find)
                    | FastReport.PreviewButtons.Zoom)
                    | FastReport.PreviewButtons.Navigator)
                    | FastReport.PreviewButtons.Close)));
        previewSettings3.Icon = ((System.Drawing.Icon)(resources.GetObject("previewSettings3.Icon")));
        previewSettings3.Text = "";
        this.environmentPlantering.PreviewSettings = previewSettings3;
        this.environmentPlantering.ReportSettings = reportSettings3;
        this.environmentPlantering.UIStyle = FastReport.Utils.UIStyle.Office2007Black;
        // 
        // labelTotaltAntalYtor
        // 
        this.labelTotaltAntalYtor.AutoSize = true;
        this.labelTotaltAntalYtor.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelTotaltAntalYtor.Location = new System.Drawing.Point(1, 523);
        this.labelTotaltAntalYtor.Name = "labelTotaltAntalYtor";
        this.labelTotaltAntalYtor.Size = new System.Drawing.Size(118, 13);
        this.labelTotaltAntalYtor.TabIndex = 2;
        this.labelTotaltAntalYtor.Text = "Totalt antalt ytor: 0";
        // 
        // labelProgress
        // 
        this.labelProgress.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.labelProgress.Location = new System.Drawing.Point(580, 524);
        this.labelProgress.Name = "labelProgress";
        this.labelProgress.Size = new System.Drawing.Size(68, 13);
        this.labelProgress.TabIndex = 4;
        this.labelProgress.Text = "0% Klar";
        this.labelProgress.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
        // 
        // progressBarData
        // 
        this.progressBarData.ForeColor = System.Drawing.SystemColors.Desktop;
        this.progressBarData.Location = new System.Drawing.Point(4, 543);
        this.progressBarData.Maximum = 12;
        this.progressBarData.Name = "progressBarData";
        this.progressBarData.Size = new System.Drawing.Size(644, 20);
        this.progressBarData.Step = 1;
        this.progressBarData.TabIndex = 3;
        // 
        // PlanteringForm
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.AutoScroll = true;
        this.AutoSize = true;
        this.ClientSize = new System.Drawing.Size(657, 569);
        this.Controls.Add(this.labelTotaltAntalYtor);
        this.Controls.Add(this.labelProgress);
        this.Controls.Add(this.progressBarData);
        this.Controls.Add(this.tabControl1);
        this.Controls.Add(this.menuStripPlantering);
        this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
        this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
        this.MainMenuStrip = this.menuStripPlantering;
        this.MaximizeBox = false;
        this.MaximumSize = new System.Drawing.Size(715, 646);
        this.MinimizeBox = false;
        this.Name = "PlanteringForm";
        this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        this.Text = "KORUS Plantering";
        this.Load += new System.EventHandler(this.Plantering_Load);
        this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Plantering_FormClosing);
        ((System.ComponentModel.ISupportInitialize)(this.dataSetPlantering)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableYta)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableUserData)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableFragor)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataSetSettings)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableRegion)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableDistrikt)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableMarkberedningsmetod)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableUrsprung)).EndInit();
        ((System.ComponentModel.ISupportInitialize)(this.dataTableInvTyp)).EndInit();
        this.menuStripPlantering.ResumeLayout(false);
        this.menuStripPlantering.PerformLayout();
        ((System.ComponentModel.ISupportInitialize)(this.reportPlantering)).EndInit();
        this.tabControl1.ResumeLayout(false);
        this.tabPagePlantering.ResumeLayout(false);
        this.groupBoxPlantering.ResumeLayout(false);
        this.groupBoxPlantering.PerformLayout();
        this.groupBoxTrakt.ResumeLayout(false);
        this.groupBoxTrakt.PerformLayout();
        this.groupBoxRegion.ResumeLayout(false);
        this.groupBoxRegion.PerformLayout();
        this.tabPageYtor.ResumeLayout(false);
        this.groupBoxPlanteringsstallen.ResumeLayout(false);
        this.groupBoxMedelvarde.ResumeLayout(false);
        this.groupBoxMedelvarde.PerformLayout();
        this.groupBoxSumma.ResumeLayout(false);
        this.groupBoxSumma.PerformLayout();
        ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPlanteringsstallen)).EndInit();
        this.tabPageFrågor.ResumeLayout(false);
        this.groupBoxTrädslag.ResumeLayout(false);
        this.groupBoxTrädslag.PerformLayout();
        this.groupBoxStambrev.ResumeLayout(false);
        this.groupBoxStambrev.PerformLayout();
        this.groupBoxStorlek.ResumeLayout(false);
        this.groupBoxAntal.ResumeLayout(false);
        this.groupBoxAntal.PerformLayout();
        this.groupBox5.ResumeLayout(false);
        this.groupBoxOBS.ResumeLayout(false);
        this.groupBoxOBS.PerformLayout();
        this.groupBox4.ResumeLayout(false);
        this.groupBox4.PerformLayout();
        this.groupBox3.ResumeLayout(false);
        this.groupBox3.PerformLayout();
        this.ResumeLayout(false);
        this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.MenuStrip menuStripPlantering;
    private System.Windows.Forms.ToolStripMenuItem arkivToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem sparaToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem sparaSomToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    private System.Windows.Forms.ToolStripMenuItem avslutaToolStripMenuItem;
    private System.Data.DataSet dataSetPlantering;
    private FastReport.Report reportPlantering;
    private System.Data.DataTable dataTableYta;
    private System.Data.DataTable dataTableUserData;
    private System.Data.DataColumn dataColumnYta;
    private System.Data.DataColumn dataColumnOptimalt;
    private System.Data.DataColumn dataColumnBra;
    private System.Data.DataColumn dataColumnVaravBattre;
    private System.Data.DataColumn dataColumnOvrigt;
    private System.Windows.Forms.OpenFileDialog openReportFileDialog;
    private System.Windows.Forms.TabControl tabControl1;
    private System.Windows.Forms.TabPage tabPageYtor;
    private System.Windows.Forms.TabPage tabPageFrågor;
    private System.Data.DataTable dataTableFragor;
    private System.Data.DataColumn dataColumn1;
    private System.Data.DataColumn dataColumn2;
    private System.Data.DataColumn dataColumn9;
    private System.Data.DataSet dataSetSettings;
    private System.Data.DataTable dataTableRegion;
    private System.Data.DataTable dataTableDistrikt;
    private FastReport.EnvironmentSettings environmentPlantering;
    private System.Data.DataColumn dataColumnSettingsRegionId;
    private System.Data.DataColumn dataColumnSettingsDistriktRegionId;
    private System.Data.DataColumn dataColumnPlanterade;
    private System.Data.DataColumn dataColumnPlanteringsdjupFel;
    private System.Data.DataColumn dataColumnTilltryckning;
    private System.Data.DataColumn dataColumnSettingsRegionNamn;
    private System.Data.DataColumn dataColumnSettingsDistriktId;
    private System.Data.DataColumn dataColumnSettingsDistriktNamn;
    private System.Data.DataTable dataTableMarkberedningsmetod;
    private System.Data.DataColumn dataColumnSettingsMarkberedningsmetodNamn;
    private System.Data.DataTable dataTableUrsprung;
    private System.Data.DataColumn dataColumnSettingsUrsprungNamn;
    private System.Data.DataColumn dataColumnRegionId;
    private System.Data.DataColumn dataColumnRegion;
    private System.Data.DataColumn dataColumnDistriktId;
    private System.Data.DataColumn dataColumnDistrikt;
    private System.Data.DataColumn dataColumnUrsprung;
    private System.Data.DataColumn dataColumnDatum;
    private System.Data.DataColumn dataColumnTraktnr;
    private System.Data.DataColumn dataColumnTraktnamn;
    private System.Data.DataColumn dataColumnStandort;
    private System.Data.DataColumn dataColumnEntreprenor;
    private System.Data.DataColumn dataColumnKommentar;
    private System.Data.DataColumn dataColumnLanguage;
    private System.Data.DataColumn dataColumnMailFrom;
    private System.Data.DataColumn dataColumnProvytestorlek;
    private System.Data.DataColumn dataColumnStatus;
    private System.Data.DataColumn dataColumnStatus_Datum;
    private System.Data.DataColumn dataColumnArtal;
    private System.Windows.Forms.FolderBrowserDialog saveReportFolderBrowserDialog;
    private System.Windows.Forms.TabPage tabPagePlantering;
    private System.Windows.Forms.GroupBox groupBoxRegion;
    private System.Windows.Forms.ComboBox comboBoxUrsprung;
    private System.Windows.Forms.DateTimePicker dateTimePicker;
    private System.Windows.Forms.ComboBox comboBoxDistrikt;
    private System.Windows.Forms.ComboBox comboBoxRegion;
    private System.Windows.Forms.Label labelDatum;
    private System.Windows.Forms.Label labelUrsprung;
    private System.Windows.Forms.Label labelDistrikt;
    private System.Windows.Forms.Label labelRegion;
    private System.Windows.Forms.GroupBox groupBoxTrakt;
    private System.Windows.Forms.TextBox textBoxTraktnr;
    private System.Windows.Forms.TextBox textBoxTraktnamn;
    private System.Windows.Forms.TextBox textBoxEntreprenor;
    //private System.Windows.Forms.ComboBox comboBoxStandort;
    private System.Windows.Forms.Label labeEntreprenor;
    private System.Windows.Forms.Label labelStandort;
    private System.Windows.Forms.Label labelTraktnamn;
    private System.Windows.Forms.Label labelTraktnr;
    private System.Windows.Forms.GroupBox groupBoxPlantering;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.TextBox textBoxMåluppfyllelse;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.TextBox textBoxMål;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.Label label21;
    private System.Windows.Forms.Label label22;
    private System.Windows.Forms.TextBox textBoxUttnyttjandegrad;
    private System.Windows.Forms.Label label24;
    private System.Windows.Forms.TextBox textBoxAndelMedBraDjup;
    private System.Windows.Forms.TextBox textBoxUtmärktPlanteringsIndex;
    private System.Windows.Forms.Label label23;
    private System.Windows.Forms.TextBox textBoxAndelMedBraTillryckning;
    private System.Windows.Forms.Label label25;
    private System.Windows.Forms.Label label26;
    private System.Windows.Forms.Label labelGISData;
    private System.Windows.Forms.TextBox textBoxAntalSattaPlantor;
    private System.Windows.Forms.TextBox textBoxPlanteringAntalOptimalaBra;
    private System.Windows.Forms.Label labelPlanteringAntalOptimalaBra;
    private System.Windows.Forms.TextBox textBoxAntalOptimalaPlantor;
    private System.Windows.Forms.Label labelPlanteringAntalOptimala;
    private System.Windows.Forms.Label labelGISStracka;
    private System.Windows.Forms.TextBox textBoxProvytestorlek;
    private System.Windows.Forms.GroupBox groupBoxPlanteringsstallen;
    private System.Windows.Forms.GroupBox groupBoxMedelvarde;
    private System.Windows.Forms.TextBox textBoxMedelvardeTilltryckning;
    private System.Windows.Forms.TextBox textBoxMedelvardeBra;
    private System.Windows.Forms.Label label16;
    private System.Windows.Forms.Label labelMedelvardeBra;
    private System.Windows.Forms.TextBox textBoxMedelvardePlanteringsdjupfel;
    private System.Windows.Forms.TextBox textBoxMedelvardeOptimalt;
    private System.Windows.Forms.Label label17;
    private System.Windows.Forms.Label labelMedelvardeOptimalt;
    private System.Windows.Forms.Label label18;
    private System.Windows.Forms.TextBox textBoxMedelvardeVaravBattre;
    private System.Windows.Forms.TextBox textBoxMedelvardePlanterade;
    private System.Windows.Forms.Label label20;
    private System.Windows.Forms.TextBox textBoxMedelvardeOvrigt;
    private System.Windows.Forms.Label label19;
    private System.Windows.Forms.GroupBox groupBoxSumma;
    private System.Windows.Forms.TextBox textBoxSummaTilltryckning;
    private System.Windows.Forms.Label label15;
    private System.Windows.Forms.TextBox textBoxSummaPlanteringsdjupfel;
    private System.Windows.Forms.Label label14;
    private System.Windows.Forms.Label label13;
    private System.Windows.Forms.TextBox textBoxSummaPlanterade;
    private System.Windows.Forms.TextBox textBoxSummaOptimalt;
    private System.Windows.Forms.TextBox textBoxSummaOvrigt;
    private System.Windows.Forms.Label labelSummaOptimaltBra;
    private System.Windows.Forms.TextBox textBoxSummaVaravBattre;
    private System.Windows.Forms.Label labelSummaBlekjordsflack;
    private System.Windows.Forms.TextBox textBoxSummaBra;
    private System.Windows.Forms.Label labelSummaBra;
    private System.Windows.Forms.Label labelSummaOptimalt;
    private System.Windows.Forms.DataGridView dataGridViewPlanteringsstallen;
    private System.Data.DataColumn dataColumnMal;
    private System.Windows.Forms.DataGridViewTextBoxColumn ytaDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn optimaltDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn braDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn varavBattreDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn ovrigtDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn planteradeDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn planteringsDjupFelDataGridViewTextBoxColumn;
    private System.Windows.Forms.DataGridViewTextBoxColumn tilltryckningDataGridViewTextBoxColumn;
    private System.Windows.Forms.Label labelTotaltAntalYtor;
    private System.Windows.Forms.Label labelProgress;
    private System.Windows.Forms.ProgressBar progressBarData;
    private System.Windows.Forms.GroupBox groupBoxOBS;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.GroupBox groupBox4;
    private System.Windows.Forms.RadioButton radioButton2Nej;
    private System.Windows.Forms.RadioButton radioButton2Ja;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.GroupBox groupBox3;
    private System.Windows.Forms.RadioButton radioButton1Nej;
    private System.Windows.Forms.RadioButton radioButton1Ja;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.GroupBox groupBox5;
    private System.Windows.Forms.RichTextBox richTextBoxKommentar;
    private System.Data.DataColumn dataColumn3;
    private System.Data.DataColumn dataColumn4;
    private System.Data.DataColumn dataColumn5;
    private System.Data.DataColumn dataColumn6;
    private System.Data.DataColumn dataColumn7;
    private System.Data.DataColumn dataColumn8;
    private System.Data.DataColumn dataColumn10;
    private System.Data.DataColumn dataColumn11;
    private System.Data.DataColumn dataColumn12;
    private System.Windows.Forms.GroupBox groupBoxTrädslag;
    private System.Windows.Forms.Label labelContorta;
    private System.Windows.Forms.Label labelTall;
    private System.Windows.Forms.Label labelGran;
    private System.Windows.Forms.GroupBox groupBoxStambrev;
    private System.Windows.Forms.TextBox textBoxContortaStambrev;
    private System.Windows.Forms.TextBox textBoxGranStambrev;
    private System.Windows.Forms.TextBox textBoxTallStambrev;
    private System.Windows.Forms.GroupBox groupBoxStorlek;
    private System.Windows.Forms.ComboBox comboBoxStorlekContorta;
    private System.Windows.Forms.ComboBox comboBoxStorlekGran;
    private System.Windows.Forms.ComboBox comboBoxStorlekTall;
    private System.Windows.Forms.GroupBox groupBoxAntal;
    private System.Windows.Forms.TextBox textBoxAntalContorta;
    private System.Windows.Forms.TextBox textBoxAntalGran;
    private System.Windows.Forms.TextBox textBoxAntalTall;
    private System.Data.DataColumn dataColumnInvTypId;
    private System.Data.DataColumn dataColumnInvTyp;
    private System.Data.DataColumn dataColumnAtgAreal;
    private System.Data.DataTable dataTableInvTyp;
    private System.Data.DataColumn dataColumnSettingsInvTypId;
    private System.Data.DataColumn dataColumnSettingsInvTypNamn;
    private System.Windows.Forms.TextBox textBoxTraktDel;
    private System.Windows.Forms.TextBox textBoxAtgAreal;
    private System.Windows.Forms.Label labelAtgAreal;
    private System.Windows.Forms.ComboBox comboBoxInvTyp;
    private System.Windows.Forms.Label labelInvTyp;
    private System.Windows.Forms.TextBox textBoxRegion;
    private System.Windows.Forms.TextBox textBoxDistrikt;
  }
}

