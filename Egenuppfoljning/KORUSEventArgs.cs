﻿#region

using System;
using System.Collections.Generic;

#endregion

namespace Egenuppfoljning
{
  public delegate void StatusEventHandler(object sender, KorusStatusEventArgs status);

  public delegate void DataEventHandler(object sender, KorusDataEventArgs data);

  public delegate void RapportEventHandler(object sender, List<KorusDataEventArgs.Data> rapporter);

  public delegate void FrågorEventHandler(object sender, List<KorusDataEventArgs.Frågor> frågor);

  public delegate void YtorEventHandler(object sender, List<KorusDataEventArgs.Yta> ytor);

  public delegate void MethodFlagInvoker(bool aFlag);

  public delegate void EntreprenörEventHandler(object sender, List<KorusDataEventArgs.Entreprenör> entreprenörer);

  public class KorusStatusEventArgs : EventArgs
  {
    private readonly bool mBStatusRedigeraRapport;
    private readonly int mDbIndex;
    private readonly string mSStatusText;
    private readonly int mSistaDbIndex;
    private readonly bool mTomDataBas;

// ReSharper disable UnusedMember.Global
    public KorusStatusEventArgs()
// ReSharper restore UnusedMember.Global
    {
    }

    public KorusStatusEventArgs(bool aTomDataBas, int aDBIndex, int aTotalaAntaletRapporter)
    {
      mTomDataBas = aTomDataBas;
      mDbIndex = aDBIndex;
      mSistaDbIndex = aTotalaAntaletRapporter;
    }

    public KorusStatusEventArgs(bool aStatusRedigeraRapport, string aStatusText)
    {
      mBStatusRedigeraRapport = aStatusRedigeraRapport;
      mSStatusText = aStatusText;
    }

// ReSharper disable UnusedMember.Global
    public bool StatusRedigeraRapport

    {
      get { return mBStatusRedigeraRapport; }
    }

    public string StatusText
    {
      get { return mSStatusText; }
    }

    public bool TomDataBas
    {
      get { return mTomDataBas; }
    }

    public int DBIndex
    {
      get { return mDbIndex; }
    }

    public int SistaDBIndex
    {
      get { return mSistaDbIndex; }
    }

    // ReSharper restore UnusedMember.Global
  }

  public class KorusDataEventArgs : EventArgs
  {
// ReSharper disable InconsistentNaming
    public readonly List<Yta> ytor;
    public Data data;
    public Frågor frågeformulär;
    public Yta yta;
    // ReSharper restore InconsistentNaming
    public KorusDataEventArgs()
    {
      ytor = new List<Yta>();
      data.regionid = -1;
      data.distriktid = -1;
      data.traktnr = -1;
      data.standort = -1;
      data.entreprenor = -1;
      data.rapport_typ = -1;
      data.årtal = -1;
      data.invtyp = -1;
      yta.yta = -1;
    }

    public KorusDataEventArgs(int aRapportTyp, int aRegionId, int aDistriktId, int aTraktNr, int aEntreprenörId,
                              string aUrsprung, int aStandort, int aArtal, int aInvTypId)
    {
      ytor = new List<Yta>();
      data.rapport_typ = aRapportTyp;
      data.regionid = aRegionId;
      data.distriktid = aDistriktId;
      data.traktnr = aTraktNr;
      data.entreprenor = aEntreprenörId;
      data.ursprung = aUrsprung;
      data.standort = aStandort;
      data.årtal = aArtal;
      yta.yta = -1;
      data.invtyp = aInvTypId;
    }

    public bool KorrektaNyckelvärden
    {
      get
      {
          return (data.invtyp >= 0 && data.regionid >= 0 && data.distriktid >= 0 && data.traktnr >= 0 &&
                data.standort >= 0 && data.entreprenor >= 0 && data.rapport_typ >= 0 && data.årtal >= 0);
      }
    }

    public bool IsEmpty()
    {
      return (data.invtyp<0 && data.regionid < 0 && data.distriktid < 0 && data.traktnr < 0 && data.standort < 0 && data.entreprenor < 0 &&
              data.rapport_typ < 0 && data.årtal < 0);
    }

    #region Nested type: Data

    public struct Data
    {
      //Nyckel variabler
      // ReSharper disable InconsistentNaming
      public int aker;
      public int antalvaltor;
      public float areal;
      public int avlagg1norr;
      public int avlagg1ost;
      public int avlagg2norr;
      public int avlagg2ost;
      public int avlagg3norr;
      public int avlagg3ost;
      public int avlagg4norr;
      public int avlagg4ost;
      public int avlagg5norr;
      public int avlagg5ost;
      public int avlagg6norr;
      public int avlagg6ost;
      public int avstand;
      public int bedomdvolym;
      public string datainsamlingsmetod;
      public string datum;
      public int distriktid;
      public string distriktnamn;
      public int entreprenor;
      public string entreprenornamn;
      public float gisareal;
      public int gisstracka;
      public bool grotbil;
      public int hygge;
      public string kommentar;
      public bool latbilshugg;
      public int malgrundyta;
      public string markberedningsmetod;
      public int provytestorlek;
      public int rapport_typ;
      public int regionid;
      public string regionnamn;
      public bool skotarburenhugg;
      public int standort;
      public int stickvagsavstand;
      public string stickvagssystem;
      public string traktnamn;
      public int traktnr;
      public bool traktordragenhugg;
      public string ursprung;
      public int vagkant;
      public int årtal;

      public int invtyp;
      public float atgareal;
      // ReSharper restore InconsistentNaming
    }

    #endregion

    #region Nested type: Entreprenör

    public struct Entreprenör
    {
      // ReSharper disable InconsistentNaming
      public int id;
      public string namn;
      public int regionid;
      // ReSharper restore InconsistentNaming
    }

    #endregion

    #region Nested type: Frågor

    public struct Frågor
    {
      // ReSharper disable UnassignedField.Global
      // ReSharper disable InconsistentNaming
      public int distriktid;
      public int entreprenor;

      public string fraga1;

      public string fraga10;
      public string fraga11;
      public string fraga12;
      public string fraga13;
      public string fraga14;
      public string fraga15;
      public string fraga16;
      public string fraga17;
      public string fraga18;
      public string fraga19;
      public string fraga2;
      public string fraga20;
      public string fraga21;
      public string fraga3;
      public string fraga4;
      public string fraga5;
      public string fraga6;
      public string fraga7;
      public string fraga8;
      public string fraga9;
      public int fragorid; //Unik identifierare
      public string ovrigt;
      public int rapport_typ;
      public int regionid;
      public int standort;
      public int traktnr;
      public int årtal;
      public int invtyp;
      // ReSharper restore InconsistentNaming
      // ReSharper restore UnassignedField.Global
    }

    #endregion

    #region Nested type: Yta

    public struct Yta
    {
      // ReSharper disable InconsistentNaming
      public int blekjordsflack;
      public int bra;
      public int contorta;
      public float contortamedelhojd;
      public int distriktid;
      public int entreprenor;
      public int gran;
      public float granmedelhojd;
      public int hst;
      public int lov;
      public float lovmedelhojd;
      public float mhojd;

      public int optimalt;
      public int optimaltbra;
      public int ovrigt;
      public int planterade;
      public int planteringsdjupfel;
      public int rapport_typ;
      public int regionid;
      public int skador;
      public int standort;
      public float stickvbredd;
      public int summa;
      public int tall;
      public float tallmedelhojd;
      public int tilltryckning;
      public int traktnr;
      public int varavbattre;
      public int yta; //löpnummer
      public int årtal;
      public int invtyp;
      public int rojstam;
      // ReSharper restore InconsistentNaming
    }

    #endregion
  }
}