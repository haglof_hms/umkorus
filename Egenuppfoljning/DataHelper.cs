﻿#region

using System;
using System.Windows.Forms;
using Egenuppfoljning.Interfaces;
using Egenuppfoljning.Properties;

#endregion

namespace Egenuppfoljning
{
  public static class DataHelper
  {
      public static void AccepteraEndastHeltalSomKeyDown(KeyEventArgs e, bool bAllowDecimal)
    {
      try
      {
        //Tillåt piltangenter och Tabb
        switch (e.KeyCode)
        {
          case Keys.Up:
          case Keys.Down:
          case Keys.Left:
          case Keys.Right:
          case Keys.Tab:
          case Keys.Delete:
          case Keys.Back:
          case Keys.Subtract: //tillåt "-" används i combobox input
            e.SuppressKeyPress = false;
            return;
        }

        //Tillåt numkeys
        if (e.KeyCode >= Keys.NumPad0 && e.KeyCode <= Keys.NumPad9 || (bAllowDecimal && e.KeyCode == Keys.Decimal))
        {
          e.SuppressKeyPress = false;
          return;
        }

        e.SuppressKeyPress = !char.IsNumber((char) e.KeyCode);
      }
      catch (Exception)
      {
        e.SuppressKeyPress = true;
      }
    }

    /// <summary>
    ///   Den grafiska komponenten DataGridView uppdaterar endast till datasettet vid focus lost, denna metod simulerar ett sånt event för att uppdatera datasettet direkt.
    /// </summary>
    /// <param name="aDataGridView"> </param>
    public static void TvingaDataGridViewAttValidera(DataGridView aDataGridView)
    {
      try
      {
        var y = aDataGridView.CurrentCellAddress.Y;
        var x = aDataGridView.CurrentCellAddress.X;
        aDataGridView.CurrentCell = y > 0 ? aDataGridView.Rows[y - 1].Cells[x] : aDataGridView.Rows[y + 1].Cells[x];
        aDataGridView.CurrentCell = aDataGridView.Rows[y].Cells[x];
      }
      catch (Exception)
      {
        //Ignorera om något fel händer.    	
      }
    }

    public static string GetRapportTyp(int aRapportTyp)
    {
      switch (aRapportTyp)
      {
        case (int) RapportTyp.Markberedning:
          return Resources.Markberedning;
        case (int) RapportTyp.Plantering:
          return Resources.Plantering;
        case (int) RapportTyp.Gallring:
          return Resources.Gallring;
        case (int) RapportTyp.Röjning:
          return Resources.Rojning;
        case (int) RapportTyp.Biobränsle:
          return Resources.Biobransle;
        case (int) RapportTyp.Slutavverkning:
          return Resources.Slutavverkning;
        default:
          return string.Empty;
      }
    }

    public static int GetRapportTyp(object aRapportTyp)
    {
      if (aRapportTyp != null && !aRapportTyp.ToString().Equals(string.Empty))
      {
        var typ = aRapportTyp.ToString().Trim().ToLower();
        if (typ.Equals(Resources.Markberedning.ToLower()))
          return (int) RapportTyp.Markberedning;
        if (typ.Equals(Resources.Plantering.ToLower()))
          return (int) RapportTyp.Plantering;
        if (typ.Equals(Resources.Gallring.ToLower()))
          return (int) RapportTyp.Gallring;
        if (typ.Equals(Resources.Rojning.ToLower()))
          return (int) RapportTyp.Röjning;
        if (typ.Equals(Resources.Biobransle.ToLower()))
          return (int) RapportTyp.Biobränsle;
        if (typ.Equals(Resources.Slutavverkning.ToLower()))
          return (int) RapportTyp.Slutavverkning;
      }
      return -1;
    }
  }
}