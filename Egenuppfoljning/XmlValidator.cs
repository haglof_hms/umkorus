﻿#region

using System;
using System.Xml;
using System.Xml.Schema;

#endregion

namespace Egenuppfoljning
{
  /// <summary>
  ///   Validates a XML file so it contain the correct XML nodes according to the Schema.
  /// </summary>
  internal class XmlValidator
  {
    private readonly string mSchemaFileName;
    private readonly string mXmlFileName;
    private readonly XmlSchemaSet mXmlSchemaSet;
    private bool mIsFailure;

    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="sXMLFileName"> The name of the XML file </param>
    /// <param name="sSchemaFileName"> The name of the Schema file </param>
    public XmlValidator(string sXMLFileName, string sSchemaFileName)
    {
      mXmlFileName = sXMLFileName;
      mSchemaFileName = sSchemaFileName;
      mXmlSchemaSet = new XmlSchemaSet();
      mXmlSchemaSet.Add(null, mSchemaFileName);
      mXmlSchemaSet.Compile();
    }

    /// <summary>
    ///   Validates through the XML file by reading it and fires a failure event if something goes wrong.
    /// </summary>
    /// <returns> true if somethings is wrong with the XML file </returns>
    public bool ValidateXmlFileFailed()
    {
      try
      {
        // Create the XmlNodeReader object.
        var doc = new XmlDocument();
        doc.Load(mXmlFileName);
        var nodeReader = new XmlNodeReader(doc);

        var xmlReaderSettings = new XmlReaderSettings {ValidationType = ValidationType.Schema};

        xmlReaderSettings.Schemas.Add(mXmlSchemaSet);
        xmlReaderSettings.ValidationEventHandler += ValidationFailed;

        var xmlReader = XmlReader.Create(nodeReader, xmlReaderSettings);

        while (xmlReader.Read() && !mIsFailure)
        {
          //Read through the Settings.xml file, and invoke failure event if found.
        }
      }
      catch (Exception)
      {
        return true;
      }

      return mIsFailure;
    }

    /// <summary>
    ///   This method is called if a node does not correspond to the data from the schema.
    /// </summary>
    /// <param name="sender"> Object of the sender </param>
    /// <param name="args"> The validation event arguments </param>
    private void ValidationFailed(object sender, ValidationEventArgs args)
    {
      mIsFailure = true;
    }
  }
}