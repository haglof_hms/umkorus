﻿#region

using System;
using System.Data;
using System.Linq;
using Egenuppfoljning.Properties;

#endregion

namespace Egenuppfoljning
{
  public static class DataSetParser
  {
    private static string AdjustCommaSeparator(object objValue, bool aSättNollVidFail)
    {
      var sValue = "-1";

      if (aSättNollVidFail)
      {
        sValue = Resources.Noll;
      }

      try
      {
        if (objValue != null && !objValue.ToString().Trim().Equals(string.Empty))
        {
          sValue = objValue.ToString().Trim();

          if (sValue.Contains('.'))
          {
            sValue = sValue.Replace('.', ',');
          }
        }
      }
      catch (Exception)
      {
      }
      return sValue;
    }

    public static double GetObjDoubleValue(object objValue, bool aSättNollVidFail)
    {
      var doubleValue = -1.0;

      try
      {
        doubleValue = double.Parse(AdjustCommaSeparator(objValue, aSättNollVidFail));
      }
      catch (Exception)
      {
      }

      if (aSättNollVidFail && doubleValue < 0.0)
      {
        return 0.0;
      }
      return doubleValue;
    }

    public static float GetObjFloatValue(object objValue)
    {
      var floatValue = -1.0f;

      try
      {
        floatValue = float.Parse(AdjustCommaSeparator(objValue, false));
      }
      catch (Exception)
      {
      }

      return floatValue;
    }

    public static decimal GetObjDecimalValue(object objValue)
    {
      decimal decimalValue = -1;

      try
      {
        decimalValue = decimal.Parse(AdjustCommaSeparator(objValue, false));
      }
      catch (Exception)
      {
      }

      return decimalValue;
    }

    public static int GetObjIntValue(object objValue, bool aSättNollVidFail)
    {
      var intValue = -1;

      try
      {
        intValue = int.Parse(AdjustCommaSeparator(objValue, aSättNollVidFail));
      }
      catch (Exception)
      {
      }

      if (aSättNollVidFail && intValue < 0)
      {
        return 0;
      }
      return intValue;
    }

    public static int GetObjIntValue(object objValue)
    {
      return GetObjIntValue(objValue, false);
    }

    public static string GetDsStr(string aTable, string aColumn, int aRowIndex, DataSet aDataSet)
    {
      try
      {
        if (aDataSet != null && aDataSet.Tables[aTable] != null)
        {
          if (aDataSet.Tables[aTable].Rows.Count > aRowIndex && aDataSet.Tables[aTable].Rows[aRowIndex][aColumn] != null)
          {
            return aDataSet.Tables[aTable].Rows[aRowIndex][aColumn].ToString();
          }
        }
      }
      catch (Exception)
      {
        //ignore 
      }
      return string.Empty;
    }
    public static string getInvNamn(int nIndex)
    {
        try
        {
            if (nIndex == 0)
                return "Egen";
            else
                if (nIndex == 1)
                    return "Distrikt";
        }
        catch { 
        }
        return string.Empty;
    }

    public static void TabortFelaktigaYtor(DataSet aDataSet)
    {
      try
      {
        var imax = aDataSet.Tables["Yta"].Rows.Count;
        for (var i = 0; i < imax; i++)
        {
          var row = aDataSet.Tables["Yta"].Rows[i];
          if (row.RowState == DataRowState.Deleted || row[0] == null) continue;

          int ytaId;
          int.TryParse(row[0].ToString(), out ytaId);

          if (ytaId > 0) continue;
          aDataSet.Tables["Yta"].Rows.Remove(row);
          i--;
          imax--;
        }
      }
      catch (Exception)
      {
        //ignore
      }
    }

    public static void SetDsDouble(string aTable, string aColumn, double aNumber, DataSet aDataSet)
    {
      if (aDataSet == null || aDataSet.Tables[aTable] == null) return;
      if (aDataSet.Tables[aTable].Rows.Count <= 0 || aDataSet.Tables[aTable].Rows[0][aColumn] == null) return;
      try
      {
        aDataSet.Tables[aTable].Rows[0][aColumn] = aNumber;
      }
      catch (Exception)
      {
        //ignore            
      }
    }

    public static void SetDsInt(string aTable, string aColumn, int aNumber, DataSet aDataSet)
    {
      if (aDataSet == null || aDataSet.Tables[aTable] == null) return;
      if (aDataSet.Tables[aTable].Rows.Count <= 0 || aDataSet.Tables[aTable].Rows[0][aColumn] == null) return;
      try
      {
        aDataSet.Tables[aTable].Rows[0][aColumn] = aNumber;
      }
      catch (Exception)
      {
        //ignore            
      }
    }

    public static void SetDsStr(string aTable, string aColumn, string aText, DataSet aDataSet)
    {
      if (aDataSet == null || aDataSet.Tables[aTable] == null) return;
      if (aDataSet.Tables[aTable].Rows.Count <= 0 || aDataSet.Tables[aTable].Rows[0][aColumn] == null ||
          aDataSet.Tables[aTable].Rows[0][aColumn].ToString().Equals(aText)) return;
      try
      {
        aDataSet.Tables[aTable].Rows[0][aColumn] = aText;
      }
      catch (Exception)
      {
        //ignore            
      }
    }
  }
}