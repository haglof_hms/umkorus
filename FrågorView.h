#pragma once

#pragma managed

#include <afxwin.h>
#include <afxwinforms.h>
#include <map>
#include "DBFunc.h"

#using <Egenuppfoljning.dll>

using namespace Egenuppfoljning;
using namespace std; 

class CFr�gorView : public CWinFormsView
{
public:
	void OnKORUSStatusRefresh(System::Object^ sender, Egenuppfoljning::KorusStatusEventArgs^ dataobjekt);
	void OnKORUSDataG�TillRapport(System::Object^ sender, Egenuppfoljning::KorusDataEventArgs^ dataobjekt);
	void OnKORUSDataTaBortFr�ga( System::Object^ sender, Egenuppfoljning::KorusDataEventArgs^ kd );
	void OnKORUSLagra�ndradeFr�gor( System::Object^ sender, List<Egenuppfoljning::KorusDataEventArgs::Fr�gor>^ fr�gor );

	BEGIN_DELEGATE_MAP( CFr�gorView )
		EVENT_DELEGATE_ENTRY( OnKORUSStatusRefresh, System::Object^, Egenuppfoljning::KorusStatusEventArgs^ )
		EVENT_DELEGATE_ENTRY( OnKORUSDataG�TillRapport, System::Object^, Egenuppfoljning::KorusDataEventArgs^ )
		EVENT_DELEGATE_ENTRY( OnKORUSDataTaBortFr�ga, System::Object^, Egenuppfoljning::KorusDataEventArgs^ )
		EVENT_DELEGATE_ENTRY( OnKORUSLagra�ndradeFr�gor, System::Object^, List<Egenuppfoljning::KorusDataEventArgs::Fr�gor>^ )
	END_DELEGATE_MAP()
	DECLARE_DYNCREATE(CFr�gorView)

protected:
	CFr�gorView();
	virtual ~CFr�gorView();
	CDBFunc *m_pDB;
	LRESULT OnSuiteMessage(WPARAM wParam, LPARAM lParam);
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
Egenuppfoljning::Controls::FormListaFr�gor^ GetControl();

	enum { IDD = IDD_LISTA_FRAGOR };

protected:
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg LRESULT OnMessageFromSuite(WPARAM wParam, LPARAM lParam);
	afx_msg int  OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	CString m_sLangFN;
public:
	DECLARE_MESSAGE_MAP()

};
