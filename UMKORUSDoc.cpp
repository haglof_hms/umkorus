#include "stdafx.h"
#include "UMKORUSDoc.h"

/////////////////////////////////////////////////////////////////////////////
// CKORUSDoc

IMPLEMENT_DYNCREATE(CKORUSDoc, CDocument)

BEGIN_MESSAGE_MAP(CKORUSDoc, CDocument)
	//{{AFX_MSG_MAP(CKORUSDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CKORUSDoc construction/destruction

CKORUSDoc::CKORUSDoc()
{
}

CKORUSDoc::~CKORUSDoc()
{
}


BOOL CKORUSDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// Extract filename of this module
	TCHAR szBuf[MAX_PATH], szModule[MAX_PATH];
	GetModuleFileName(g_hInstance, szBuf, sizeof(szBuf) / sizeof(TCHAR));
	_tsplitpath(szBuf, NULL, NULL, szModule, NULL);

	// Check if we have a valid license
	_user_msg msg(820, _T("CheckLicense"), _T("License.dll"), _T("H9003"), _T(""), _T(""), szModule);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, WM_USER+4, (LPARAM)&msg);
#ifdef UNICODE
	if( _tcscmp(msg.getSuite(), _T("0")) == 0 ||
		_tcscmp(msg.getSuite(), _T("License.dll")) == 0 )
	{
		AfxMessageBox(_T("En giltig licens till UMKORUS saknas!"));
		return FALSE;
	}
#else
	if( _tcscmp(msg.getSuite(), _T("-1")) == 0 ||
		_tcscmp(msg.getSuite(), _T("License.dll")) == 0 )
	{
		AfxMessageBox(_T("En giltig licens till UMKORUS saknas!"));
		return FALSE;
	}
#endif


	return TRUE;
}