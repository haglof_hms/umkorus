#pragma once
#pragma managed

#include <DBBaseClass_SQLApi.h>
#include <SQLAPI.h>
#include <list>
#include <vector>

#using <Egenuppfoljning.dll>

using namespace Egenuppfoljning;
using namespace std; 
using namespace System;
using namespace System::Collections::Generic;

class CDBFunc : public CDBBaseClass_SQLApi
{
public:
	CDBFunc( DB_CONNECTION_DATA &condata );
	~CDBFunc();

  void PrepareSelectCommand(Egenuppfoljning::KorusDataEventArgs^ &kh, CString aTable);
	bool AddKORUSDataRecord(Egenuppfoljning::KorusDataEventArgs^ &kh);
	bool AddKORUSEntreprenör(Egenuppfoljning::KorusDataEventArgs^ &kh);
	bool ExportRecords(CString dbFile);
	bool DeleteKORUSData(Egenuppfoljning::KorusDataEventArgs^ &kh);
	bool DeleteKORUSFrågeformulär(Egenuppfoljning::KorusDataEventArgs^ &kh);
	bool DeleteKORUSYta(Egenuppfoljning::KorusDataEventArgs^ &kh);
	bool DeleteKORUSEntreprenör(Egenuppfoljning::KorusDataEventArgs^ &kh);
	List<Egenuppfoljning::KorusDataEventArgs^>^ GetKORUSData();
	List<Egenuppfoljning::KorusDataEventArgs::Frågor>^ GetFragorLista();
	List<Egenuppfoljning::KorusDataEventArgs::Frågor>^ GetFragorLista(Egenuppfoljning::KorusDataEventArgs^ &kh);	
	List<Egenuppfoljning::KorusDataEventArgs::Yta>^ GetYtorLista();
	List<Egenuppfoljning::KorusDataEventArgs::Yta>^ GetYtorLista(Egenuppfoljning::KorusDataEventArgs^ &kh);
	List<Egenuppfoljning::KorusDataEventArgs::Data>^ GetRapporterLista();
	List<Egenuppfoljning::KorusDataEventArgs::Entreprenör>^ GetEntreprenörerLista();
	bool MakeSureTablesExist();
  bool DropAllTables();
	bool UpdateKORUSRapporter(List<Egenuppfoljning::KorusDataEventArgs::Entreprenör>^ entreprenörer);
	bool UpdateKORUSRapporter(List<Egenuppfoljning::KorusDataEventArgs::Data>^ rapporter);
	bool UpdateKORUSFrågor(List<Egenuppfoljning::KorusDataEventArgs::Frågor>^ frågor);
	bool UpdateKORUSYtor(List<Egenuppfoljning::KorusDataEventArgs::Yta>^ ytor);
	bool UpdateKORUSData(Egenuppfoljning::KorusDataEventArgs^ &kh);
	bool UpdateKORUSEntreprenörer(List<Egenuppfoljning::KorusDataEventArgs::Entreprenör>^ aEntreprenörer);
	static void setupForDBConnection( HWND hWndReciv, HWND hWndSend );
	System::String^ GetKORUSEntreprenör(Egenuppfoljning::KorusDataEventArgs^ &kh);

	enum { KORUS_RAPPORT, KORUS_FRAGA, KORUS_YTA };
};
