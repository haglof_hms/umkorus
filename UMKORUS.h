#pragma once

#define __BUILD

#ifdef __BUILD
#define DLL_BUILD __declspec(dllexport)
#else
#define DLL_BUILD __declspec(dllimport)
#endif

#include <vector>

typedef std::vector<INDEX_TABLE> vecINDEX_TABLE;
typedef std::vector<INFO_TABLE> vecINFO_TABLE;

// Initialize the DLL, register the classes etc
extern "C" void DLL_BUILD InitModule(CWinApp *app, LPCTSTR suite, vecINDEX_TABLE &idx, vecINFO_TABLE &vecInfo);

extern RLFReader* g_pXML;


template<class BASE_CLASS>
class CNoFlickerWnd : public BASE_CLASS
{

protected:
	virtual LRESULT WindowProc(UINT message,WPARAM wParam,LPARAM lParam)
	{
		switch (message)
		{
		case WM_PAINT:
			{
				CPaintDC dc(this);

				// Get the client rect, and paint to a memory device context.
				// This will help reduce screen flicker.  Pass the memory
				// device context to the default window procedure to do
				// default painting.

				CXTPClientRect rc(this);
				CXTPBufferDC memDC(dc, rc);
				memDC.FillSolidRect(rc, GetXtremeColor(COLOR_WINDOW));

				return BASE_CLASS::DefWindowProc(WM_PAINT,
					(WPARAM)memDC.m_hDC, 0);
			}

		case WM_ERASEBKGND:
				return TRUE;
		}

		return BASE_CLASS::WindowProc(message, wParam, lParam);
	}
};
