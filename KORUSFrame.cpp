#include "stdafx.h"
#include "UMKORUSGenerics.h"
#include "KORUSFrame.h"
#include "KORUSView.h"

using namespace std;

/////////////////////////////////////////////////////////////////////////////
// CKORUSFrame

static UINT indicators[] =
{
	ID_SEPARATOR           // status line indicator
};

IMPLEMENT_DYNCREATE(CKORUSFrame, CChildFrameBase)

BEGIN_MESSAGE_MAP(CKORUSFrame, CChildFrameBase)
	ON_WM_PAINT()
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromSuite)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CKORUSFrame construction/destruction

CKORUSFrame::CKORUSFrame():
m_bFirstShow(TRUE)
{
}

CKORUSFrame::~CKORUSFrame()
{
}

LRESULT CKORUSFrame::OnMessageFromSuite(WPARAM wParam, LPARAM lParam)
{
	// Forward message to view
	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(WM_USER_MSG_SUITE, wParam, lParam);
		}
	}

	return 0L;
}


BOOL CKORUSFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~(WS_EX_CLIENTEDGE);
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}


int CKORUSFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{

	// Set up language filename
	//m_sLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, getLangSet(), LANGUAGE_FN_EXT);

	try
	{
		if(CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		{
			return -1;
		}
	}
	catch( System::Exception ^e )
	{
		CString str(e->ToString());
		AfxMessageBox(str, MB_OK | MB_ICONSTOP);
		PostMessage(WM_CLOSE);
		return -1;
	}

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// Create and Load toolbar
	m_wndToolBar.CreateToolBar(WS_TABSTOP|WS_VISIBLE|WS_CHILD|CBRS_TOOLTIPS, this);
	m_wndToolBar.LoadToolBar(IDR_TOOLBAR1);


	HICON hIcon = NULL;
	HMODULE hResModule = NULL;
	CXTPControl *pCtrl = NULL;
	CString sTBResFN = getProgDir() + _T("HMSIcons.icl");

	if (fileExists(sTBResFN))
	{
		// Setup commandbars and manues
		CXTPToolBar* pToolBar = &m_wndToolBar;
		if (pToolBar->IsBuiltIn())
		{
			if (pToolBar->GetType() != xtpBarTypeMenuBar)
			{

				UINT nBarID = pToolBar->GetBarID();
				pToolBar->LoadToolBar(nBarID, FALSE);
				CXTPControls *p = pToolBar->GetControls();

				// Setup icons on toolbars, using resource dll; 051208 p�d
				if (nBarID == IDR_TOOLBAR1)
				{		
					pCtrl = p->GetAt(0);
					pCtrl->SetTooltip(_T("Importera Rapport"));
					hIcon = ExtractIcon(AfxGetInstanceHandle(), sTBResFN, 19);	//Importera Rapport
					if (hIcon) pCtrl->SetCustomIcon(hIcon);

					pCtrl = p->GetAt(1);
					pCtrl->SetTooltip(_T("F�rhandsgranska Rapport"));
					hIcon = ExtractIcon(AfxGetInstanceHandle(), sTBResFN, 31);	// F�rhandsgranska Rapport
					if (hIcon) pCtrl->SetCustomIcon(hIcon);
					pCtrl->SetEnabled(FALSE);

					pCtrl = p->GetAt(2);
					pCtrl->SetTooltip(_T("Redigera Rapport"));
					hIcon = ExtractIcon(AfxGetInstanceHandle(), sTBResFN, 7);	// Redigera Rapport
					if (hIcon) pCtrl->SetCustomIcon(hIcon);
					pCtrl->SetEnabled(FALSE);

					pCtrl = p->GetAt(3);
					pCtrl->SetTooltip(_T("Skriv ut"));
					hIcon = ExtractIcon(AfxGetInstanceHandle(), sTBResFN, 36);	// Skriv ut
					if (hIcon) pCtrl->SetCustomIcon(hIcon);
					pCtrl->SetEnabled(FALSE);

					pCtrl = p->GetAt(4);
					pCtrl->SetTooltip(_T("Mail inst�llningar"));
					hIcon = ExtractIcon(AfxGetInstanceHandle(), sTBResFN, 27);	// Mail inst�llningar
					if (hIcon) pCtrl->SetCustomIcon(hIcon);

				}	// if (nBarID == IDR_TOOLBAR1)
			}	// if (pToolBar->GetType() != xtpBarTypeMenuBar)
		}	// if (pToolBar->IsBuiltIn())
	}	// if (fileExists(sTBResFN))

	UpdateWindow();

	return 0;
}

void CKORUSFrame::OnDestroy()
{
	// Save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT, REGWP_KORUS_IMPORTERAVIEW);
	SavePlacement(this, csBuf);

	CMDIChildWnd::OnDestroy();
}

void CKORUSFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CMDIChildWnd::OnShowWindow(bShow, nStatus);

	// Load window position
	if(bShow && !IsWindowVisible() && m_bFirstShow)
	{
		m_bFirstShow = false; // Important: set this flag before loading window placement!

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT, REGWP_KORUS_IMPORTERAVIEW);
		LoadPlacement(this, csBuf);
	}
}

void CKORUSFrame::OnPaint()
{
	CSize sz;
	int nTop = 0;
	RECT rect;

	if (m_wndToolBar.GetSafeHwnd())
	{
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, /*LM_HIDEWRAP|*/ LM_HORZDOCK|LM_HORZ | LM_COMMIT);

		m_wndToolBar.MoveWindow(0, nTop, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
		nTop += sz.cy;
	}	// if (m_wndToolBar.GetSafeHwnd())

	CMDIChildWnd::OnPaint();
}

void CKORUSFrame::OnSize(UINT nType, int cx, int cy)
{
	CChildFrameBase::OnSize(nType, cx, cy);

	CSize sz(0);
	if (m_wndToolBar.GetSafeHwnd())
	{
		RECT rect;
		GetClientRect(&rect);
		sz = m_wndToolBar.CalcDockingLayout(rect.right, LM_HORZDOCK|LM_HORZ|LM_COMMIT);

		m_wndToolBar.MoveWindow(0, 0, rect.right, sz.cy);
		m_wndToolBar.Invalidate(FALSE);
	}
}


void CKORUSFrame::OnClose()
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
	/*AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 070905 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE);*/

	// Save window position
	if(AfxMessageBox(_T("Ifall data har �ndrats men �nnu inte �r valt att bli lagrat s� f�rsvinner dessa �ndringar. Vill du st�nga ner?"), MB_YESNO | MB_ICONWARNING)==IDYES)
	{
	  CString csBuf;
	  csBuf.Format(_T("%s\\%s"), REG_ROOT, REGWP_KORUS_IMPORTERAVIEW);
	  SavePlacement(this, csBuf);
	  CMDIChildWnd::OnClose();
	}
}


