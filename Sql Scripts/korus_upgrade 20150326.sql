USE "mathias test"
--Add vars
ALTER TABLE korus_data ADD invtyp INT
GO
UPDATE korus_data SET invtyp = 0
ALTER TABLE korus_data ALTER COLUMN invtyp INT NOT NULL

ALTER TABLE korus_fragor ADD invtyp INT
GO
UPDATE korus_fragor SET invtyp = 0
ALTER TABLE korus_fragor ALTER COLUMN invtyp INT NOT NULL

ALTER TABLE korus_ytor ADD invtyp INT
GO
UPDATE korus_ytor SET invtyp = 0
ALTER TABLE korus_ytor ALTER COLUMN invtyp INT NOT NULL

ALTER TABLE korus_data ADD atgareal FLOAT
ALTER TABLE korus_ytor ADD rojstam INT

ALTER TABLE korus_ytor ADD stickvbredd FLOAT


--Drop relations
ALTER TABLE korus_fragor DROP CONSTRAINT FK_korus_fragor_korus_data
ALTER TABLE korus_ytor DROP CONSTRAINT FK_korus_ytor_korus_data

--Drop primary key
DECLARE @name NVARCHAR(100), @sql NVARCHAR(100)
SELECT @name = (SELECT name FROM sys.key_constraints WHERE type = 'PK' AND OBJECT_NAME(parent_object_id) = N'korus_data')
SELECT @sql = 'ALTER TABLE korus_data DROP CONSTRAINT ' + @name
EXEC sp_executeSQL @sql

--Create new primary key
ALTER TABLE korus_data ADD CONSTRAINT PK_data PRIMARY KEY (regionid,	distriktid,	traktnr, standort, entreprenor,	rapport_typ, artal,	invtyp)

--Create new relations
ALTER TABLE korus_fragor ADD CONSTRAINT FK_korus_fragor_korus_data FOREIGN KEY(regionid, distriktid, traktnr, standort, entreprenor, rapport_typ, artal, invtyp)
REFERENCES korus_data (regionid, distriktid, traktnr, standort, entreprenor, rapport_typ, artal, invtyp) ON UPDATE CASCADE ON DELETE CASCADE

ALTER TABLE korus_ytor ADD CONSTRAINT FK_korus_ytor_korus_data FOREIGN KEY(regionid, distriktid, traktnr, standort, entreprenor, rapport_typ, artal, invtyp)
REFERENCES korus_data (regionid, distriktid, traktnr, standort, entreprenor, rapport_typ, artal, invtyp) ON UPDATE CASCADE ON DELETE CASCADE