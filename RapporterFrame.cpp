#include "stdafx.h"
#include "UMKORUSGenerics.h"
#include "RapporterFrame.h"
#include "RapporterView.h"

using namespace std;

/////////////////////////////////////////////////////////////////////////////
// CRapporterFrame

static UINT indicators[] =
{
	ID_SEPARATOR           // status line indicator
};

IMPLEMENT_DYNCREATE(CRapporterFrame, CChildFrameBase)

BEGIN_MESSAGE_MAP(CRapporterFrame, CChildFrameBase)
	ON_WM_PAINT()
	ON_WM_CREATE()
	ON_WM_CLOSE()
	ON_WM_DESTROY()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromSuite)
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromShell)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRapporterFrame construction/destruction

CRapporterFrame::CRapporterFrame():
m_bFirstShow(TRUE)
{
}

CRapporterFrame::~CRapporterFrame()
{
}

LRESULT CRapporterFrame::OnMessageFromSuite(WPARAM wParam, LPARAM lParam)
{
	// Forward message to view
	CDocument *pDoc = GetActiveDocument();
	if (pDoc != NULL)
	{
		POSITION pos = pDoc->GetFirstViewPosition();
		while (pos != NULL)
		{
			CView *pView = pDoc->GetNextView(pos);
			pView->SendMessage(WM_USER_MSG_SUITE, wParam, lParam);
		}
	}

	return 0L;
}


BOOL CRapporterFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CMDIChildWnd::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~(WS_EX_CLIENTEDGE);
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}


int CRapporterFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{

	// Set up language filename
	//m_sLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, getLangSet(), LANGUAGE_FN_EXT);

	try
	{
		if(CMDIChildWnd::OnCreate(lpCreateStruct) == -1)
		{
			return -1;
		}
	}
	catch( System::Exception ^e )
	{
		CString str(e->ToString());
		AfxMessageBox(str, MB_OK | MB_ICONSTOP);
		PostMessage(WM_CLOSE);
		return -1;
	}

	UpdateWindow();

	return 0;
}

void CRapporterFrame::OnDestroy()
{
	// Save window position
	CString csBuf;
	csBuf.Format(_T("%s\\%s"), REG_ROOT, REGWP_KORUS_RAPPORTERVIEWSELECT);
	SavePlacement(this, csBuf);

	CMDIChildWnd::OnDestroy();
}

void CRapporterFrame::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CMDIChildWnd::OnShowWindow(bShow, nStatus);

	// Load window position
	if(bShow && !IsWindowVisible() && m_bFirstShow)
	{
		m_bFirstShow = false; // Important: set this flag before loading window placement!

		CString csBuf;
		csBuf.Format(_T("%s\\%s"), REG_ROOT, REGWP_KORUS_RAPPORTERVIEWSELECT);
		LoadPlacement(this, csBuf);
	}
}

void CRapporterFrame::OnPaint()
{
	CMDIChildWnd::OnPaint();
}

void CRapporterFrame::OnSize(UINT nType, int cx, int cy)
{
	CChildFrameBase::OnSize(nType, cx, cy);
}


void CRapporterFrame::OnClose()
{
	// Send messages to HMSShell, disable buttons on toolbar; 060518 p�d
/*	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_NEW_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_OPEN_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_SAVE_ITEM,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DELETE_ITEM,FALSE);

	// Send messages to HMSShell, disable DBNavigation buttons on DBNavigation toolbar; 070905 p�d
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_START,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_PREV,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_NEXT,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_END,FALSE);
	AfxGetMainWnd()->PostMessage(WM_USER_MSG_SUITE,ID_DBNAVIG_LIST,FALSE); */

	// Save window position
	if(AfxMessageBox(_T("Ifall data har �ndrats men �nnu inte �r valt att bli lagrat s� f�rsvinner dessa �ndringar. Vill du st�nga ner?"), MB_YESNO | MB_ICONWARNING)==IDYES)
	{
	  CString csBuf;
	  csBuf.Format(_T("%s\\%s"), REG_ROOT, REGWP_KORUS_RAPPORTERVIEWSELECT);
	  SavePlacement(this, csBuf);
	  CMDIChildWnd::OnClose();
	}
}


LRESULT CRapporterFrame::OnMessageFromShell(WPARAM wParam, LPARAM lParam)
{
	switch( wParam )
	{
	case ID_DBNAVIG_LIST:
		//showFormView( IDD_TRAKTSELECT, m_sLangFN );
		break;

	default:
		// Forward message to view(s)
		CDocument *pDoc = GetActiveDocument();
		if( pDoc )
		{
			POSITION pos = pDoc->GetFirstViewPosition();
			while( pos )
			{
				CView *pView = pDoc->GetNextView(pos);
				pView->SendMessage( MSG_IN_SUITE, wParam, lParam );
			}
		}
		break;
	}

	return 0L;
}

