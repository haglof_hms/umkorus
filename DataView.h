#pragma once

#pragma managed

#include <afxwin.h>
#include <afxwinforms.h>
#include <map>
#include "DBFunc.h"

#using <Egenuppfoljning.dll>

using namespace std; 

class CDataView : public CWinFormsView
{
public:
	void OnKORUSStatusGUIUpdate(System::Object^ sender, Egenuppfoljning::KorusStatusEventArgs^ dataobjekt);
	void OnKORUSStatusRefresh(System::Object^ sender, Egenuppfoljning::KorusStatusEventArgs^ dataobjekt);
	void OnKORUSDataUpdate(System::Object^ sender, Egenuppfoljning::KorusDataEventArgs^ dataobjekt);
	void OnKORUSDataRemove(System::Object^ sender, Egenuppfoljning::KorusDataEventArgs^ dataobjekt);

	BEGIN_DELEGATE_MAP( CDataView )
		EVENT_DELEGATE_ENTRY( OnKORUSStatusGUIUpdate, System::Object^, Egenuppfoljning::KorusStatusEventArgs^ )
		EVENT_DELEGATE_ENTRY( OnKORUSStatusRefresh, System::Object^, Egenuppfoljning::KorusStatusEventArgs^ )
		EVENT_DELEGATE_ENTRY( OnKORUSDataUpdate, System::Object^, Egenuppfoljning::KorusDataEventArgs^ )
		EVENT_DELEGATE_ENTRY( OnKORUSDataRemove, System::Object^, Egenuppfoljning::KorusDataEventArgs^ )
	END_DELEGATE_MAP()
	DECLARE_DYNCREATE(CDataView)

protected:
	CDataView();
	virtual ~CDataView();
	CDBFunc *m_pDB;
  LRESULT OnSuiteMessage(WPARAM wParam, LPARAM lParam);
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	Egenuppfoljning::Controls::FormHanteraData^ GetControl();

	enum { IDD = IDD_HANTERA_RAPPORT };

protected:
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg LRESULT OnMessageFromSuite(WPARAM wParam, LPARAM lParam);
	afx_msg int  OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	CString m_sLangFN;
public:
	DECLARE_MESSAGE_MAP()

};
