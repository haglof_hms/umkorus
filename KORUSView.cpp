#include "stdafx.h"
#include "MessageDlg.h"
#include "UMKORUSStrings.h"
#include "KORUSView.h"
#include "KORUSFrame.h"

using namespace std;

// CKORUSView

IMPLEMENT_DYNCREATE(CKORUSView, CWinFormsView)
CKORUSView::CKORUSView():
CWinFormsView(Egenuppfoljning::Controls::FormImporteraRapport::typeid),
m_pDB(NULL)
{
}

CKORUSView::~CKORUSView()
{
	if( m_pDB )
	{
		delete m_pDB;
		m_pDB = NULL;
	}
}


BEGIN_MESSAGE_MAP(CKORUSView, CWinFormsView)
	ON_WM_CREATE()
	ON_WM_COPYDATA()
	ON_WM_SETFOCUS()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_WM_DESTROY()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromSuite)
	ON_COMMAND(ID_TBTN_IMPORT_REPORT, OnImportReport)
	ON_COMMAND(ID_TBTN_PREVIEW_REPORT, OnPreviewReport)
	ON_COMMAND(ID_TBTN_EDIT_REPORT, OnEditReport)
	ON_COMMAND(ID_TBTN_REPORT_PRINT, OnReportPrint)
	ON_COMMAND(ID_TBTN_KORUS_MAIL, OnMail)
END_MESSAGE_MAP()



Egenuppfoljning::Controls::FormImporteraRapport^ CKORUSView::GetControl()
{
	System::Windows::Forms::Control^ control = CWinFormsView::GetControl();
	return safe_cast<Egenuppfoljning::Controls::FormImporteraRapport^>(control);
}

int CKORUSView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if( CWinFormsView::OnCreate(lpCreateStruct) == -1 )
		return -1;

	// Request db connection
	CDBFunc::setupForDBConnection( AfxGetMainWnd()->GetSafeHwnd(), this->GetSafeHwnd() );

	return 0;
}



BOOL CKORUSView::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CKORUSView::OnSize(UINT nType, int cx, int cy)
{
	CWinFormsView::OnSize(nType, cx, cy);
}

void CKORUSView::OnDestroy()
{
	GetControl()->Close();
}

void CKORUSView::OnInitialUpdate()
{
	CWinFormsView::OnInitialUpdate();

	// First of all, make sure required tables exist
	m_pDB->MakeSureTablesExist();

	GetControl()->StatusEvent += MAKE_DELEGATE( Egenuppfoljning::StatusEventHandler, OnStatusChanged );
	GetControl()->DataEvent += MAKE_DELEGATE( Egenuppfoljning::DataEventHandler, OnKORUSData );

	m_toolbarControls = ((CKORUSFrame*)GetParentFrame())->m_wndToolBar.GetControls();
	m_wndStatusBar=&((CKORUSFrame*)GetParentFrame())->m_wndStatusBar;

	//Maximera fönstret
	GetParentFrame()->ShowWindow( SW_SHOWMAXIMIZED );
}

void CKORUSView::OnSetFocus(CWnd* pOldWnd)
{
	CWinFormsView::OnSetFocus(pOldWnd);

	// Turn off all imagebuttons in the main window
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_PREVIEW_ITEM, FALSE);

	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_LIST, FALSE);

	GetControl()->InvokeRapportInformation();
}

void CKORUSView::OnImportReport()
{
	GetControl()->ImporteraRapport();
//	m_wndStatusBar->SetPaneText(0, _T("Importerar KORUS Rapport"));
}

void CKORUSView::OnPreviewReport()
{
	GetControl()->FörhandsgranskaRapport();
}

void CKORUSView::OnEditReport()
{
	GetControl()->RedigeraRapport();
}
void CKORUSView::OnReportPrint()
{
	GetControl()->SkrivUtRapport();
}
void CKORUSView::OnMail()
{
	GetControl()->MailInställningar();
}

void CKORUSView::OnStatusChanged( System::Object^ /*sender*/, Egenuppfoljning::KorusStatusEventArgs^ status )
{
	m_toolbarControls->GetAt(1)->SetEnabled(TRUE); 
	m_toolbarControls->GetAt(2)->SetEnabled(status->StatusRedigeraRapport); 
	m_toolbarControls->GetAt(3)->SetEnabled(TRUE); 
	m_wndStatusBar->SetPaneText(0, status->StatusText);
}
 
void CKORUSView::OnKORUSData( System::Object^ /*sender*/, Egenuppfoljning::KorusDataEventArgs^ data )
{
	//Först kolla om entreprenörnamn finns lagrat för det gällande entreprenör id:et.
	data->data.entreprenornamn=m_pDB->GetKORUSEntreprenör(data);

	if(data->data.entreprenornamn->Equals(System::String::Empty))
	{ //Ingen entreprenör för detta id hittades, ställ frågan om det ska lagras ett nytt
	  data->data.entreprenornamn=GetControl()->SparaEntreprenör(data->data.entreprenor, data->data.regionid);
	}

	if(!data->data.entreprenornamn->Equals(System::String::Empty))
	{
	  m_pDB->AddKORUSEntreprenör(data); //Add entreprenör ignoreras om det redan finns, addar annars
	  if(m_pDB->AddKORUSDataRecord(data))
	  { 
		GetControl()->LyckadesAttLagraData();
	  }
	}
}


LRESULT CKORUSView::OnMessageFromSuite(WPARAM wParam, LPARAM lParam)
{
	try
	{
		if( wParam == ID_LPARAM_COMMAND_HANDLE_FILETYPE)
		{ //Hantera filtyp, inkommande sökväg till den dubbelklickade filen, öppna den i import
			GetControl()->ÖppnaRapport(gcnew String(CString((LPCTSTR)lParam)));
		}
	}
	catch( System::Exception ^e ) // Catch any unhandled exception
	{
		CString str(e->ToString());
		AfxMessageBox(str, MB_ICONSTOP);
		GetParentFrame()->PostMessage(WM_CLOSE);
	}

	return 0L;
}


BOOL CKORUSView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if( pData->cbData == sizeof(DB_CONNECTION_DATA) )
	{
		DB_CONNECTION_DATA connData;

		// Check if we have already set up a connection
		if( !m_pDB )
		{
			memcpy( &connData, pData->lpData, sizeof(DB_CONNECTION_DATA) );
			m_pDB = new CDBFunc(connData);
		}
	}

	return CWinFormsView::OnCopyData(pWnd, pData);
}

