#pragma once

#pragma managed

#include <afxwin.h>
#include <afxwinforms.h>
#include <map>
#include "DBFunc.h"

#using <Egenuppfoljning.dll>

using namespace Egenuppfoljning;
using namespace std; 

class CKORUSView : public CWinFormsView
{
public:
	void OnStatusChanged( System::Object^ sender, KorusStatusEventArgs^ status);
	void OnKORUSData(System::Object^ sender, Egenuppfoljning::KorusDataEventArgs^ data);
	BEGIN_DELEGATE_MAP( CKORUSView )
		EVENT_DELEGATE_ENTRY( OnStatusChanged, System::Object^, Egenuppfoljning::KorusStatusEventArgs^ )
		EVENT_DELEGATE_ENTRY( OnKORUSData, System::Object^, Egenuppfoljning::KorusDataEventArgs^ )
	END_DELEGATE_MAP()

	DECLARE_DYNCREATE(CKORUSView)

protected:
	CKORUSView();
	virtual ~CKORUSView();
	CDBFunc *m_pDB;
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	Egenuppfoljning::Controls::FormImporteraRapport^ GetControl();

	enum { IDD = IDD_IMPORTERA_RAPPORT };

protected:
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg LRESULT OnMessageFromSuite(WPARAM wParam, LPARAM lParam);
	afx_msg int  OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	void OnImportReport();
	void OnPreviewReport();
	void OnEditReport();
	void OnReportPrint();
	void OnMail();
private:
	CXTPControls *m_toolbarControls;
	CXTPStatusBar *m_wndStatusBar;
public:
		DECLARE_MESSAGE_MAP()

};
