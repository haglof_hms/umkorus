#pragma once

const int INVTYP_CT				= 4;
const int URSPRUNG_CT			= 2;
const int RADIE_CT				= 3;
const int VEGTYP_CT				= 16;
const int JORDART_CT			= 11;
const int MYR_CT				= 3;
const int HALLMARK_CT			= 3;
const int FORNMINNEN_CT			= 3;
const int KULTURLAMNING_CT		= 3;
const int SKYDDSZONER_CT		= 4;
const int NATURVARDESTRAD_CT	= 2;
const int MARKPAVERKAN_CT		= 2;
const int NEDSKRAPNING_CT		= 2;
const int RATTTRADSLAG_CT		= 3;
const int NASTAATGARD_CT		= 2;
const int MARKFUKT_CT			= 4;
const int KOMMENTARANTSTAM_CT	= 7;
const int NEJJA_CT				= 2;
const int HANSYNBIOTOP_CT		= 4;
const int LOVANDEL_CT			= 4;

const TCHAR INVTYP[INVTYP_CT][10]						= { {_T("Egen")}, {_T("Distrikts")}, {_T("Regions")}, {_T("Central")} };
const TCHAR URSPRUNG[URSPRUNG_CT][20]					= { {_T("Eget")}, {_T("Externt")} };
const TCHAR RADIE[RADIE_CT][20]							= { {_T("1.78 (10 m2)")}, {_T("3.99 (50 m2)")}, {_T("5.64 (100 m2)")} };
const TCHAR VEGTYP[VEGTYP_CT][25]						= { {_T("Lavrik typ")}, {_T("Lav typ")}, {_T("Starr-fr�ken typ")}, {_T("Fattigris typ")}, {_T("Kr�kb�r-ljung typ")}, {_T("Lingon typ")},
															{_T("Bl�b�rs typ")}, {_T("Smalbladig typ")}, {_T("Bredbladig typ")}, {_T("Mark utan f�ltskikt")}, {_T("L�g�rt m ris ej bl�b�r")}, {_T("L�g�rttyp med bl�b�r")},
															{_T("L�g�rttyp utan ris")}, {_T("H�g�rt m ris ej bl�b�r")}, {_T("H�g�rttyp med bl�b�r")}, {_T("H�g�rttyp utan ris")} };
const TCHAR JORDART[JORDART_CT][25]						= { {_T("Grusig mor�n")}, {_T("Sandig mor�n")}, {_T("Sanding,moig mor�n")}, {_T("Moig,mj�l,lerig mor")}, {_T("Grus")}, {_T("Grovsand")},
															{_T("Mellansand")}, {_T("Grovmo")}, {_T("Finmo,mj�la,lera")}, {_T("V�l f�rmultnad torv")}, {_T("L�gf�rmultnad torv")} };
const TCHAR MYR[MYR_CT][20]								= { {_T("F�rekommer ej")}, {_T("Tillr�cklig h�nsyn")}, {_T("Otillr�cklig h�nsyn")} };
const TCHAR HALLMARK[HALLMARK_CT][20]					= { {_T("F�rekommer ej")}, {_T("Tillr�cklig h�nsyn")}, {_T("Otillr�cklig h�nsyn")} };
const TCHAR FORNMINNEN[FORNMINNEN_CT][21]				= { {_T("F�rekommer ej")}, {_T("Bibeh�llen s�rpr�gel")}, {_T("F�rlorad s�rpr�gel")} };
const TCHAR KULTURLAMNING[KULTURLAMNING_CT][21]			= { {_T("F�rekommer ej")}, {_T("Bibeh�llen s�rpr�gel")}, {_T("F�rlorad s�rpr�gel")} };
const TCHAR SKYDDSZONER[SKYDDSZONER_CT][25]				= { {_T("F�rekommer ej")}, {_T("Tillr�ckliga")}, {_T("On�digt breda")}, {_T("Inget eller lite sparat")} };
const TCHAR NATURVARDESTRAD[NATURVARDESTRAD_CT][30]		= { {_T("Inget bortr�jt")}, {_T("Bortr�jt")} };
const TCHAR NEDSKRAPNING[NEDSKRAPNING_CT][10]			= { {_T("Ingen")}, {_T("Ej st�dat")} };
const TCHAR RATTTRADSLAG[RATTTRADSLAG_CT][30]			= { {_T("Korrekt")}, {_T("Felaktigt borttag av barr")}, {_T("Felaktigt borttag av l�v")} };
const TCHAR NASTAATGARD[NASTAATGARD_CT][10]				= { {_T("Ingen")}, {_T("KORUS")} };
const TCHAR MARKFUKT[MARKFUKT_CT][20]					= { {_T("Torr mark")}, {_T("Frisk mark")}, {_T("Fuktig mark")}, {_T("Bl�t mark")} };
const TCHAR KOMMENTARANTSTAM[KOMMENTARANTSTAM_CT][22]	= { {_T("Ingen")}, {_T("F�r h�rt r�jt")}, {_T("F�r svagt r�jt")}, {_T("Finns ej fler stammar")}, {_T("Sv�rt skadat")}, {_T("Delar ej r�jt")}, {_T("Kvarl�mnat underv�xt")} };
const TCHAR NEJJA[NEJJA_CT][5]							= { {_T("Nej")}, {_T("Ja")} };
const TCHAR HANSYNBIOTOP[HANSYNBIOTOP_CT][20]			= { {_T("F�rekommer ej")}, {_T("Tillr�cklig h�nsyn")}, {_T("F�r stor h�nsyn")}, {_T("Otillr�cklig h�nsyn")} };
const TCHAR LOVANDEL[LOVANDEL_CT][25]					= { {_T("Ren barr/l�vskog")}, {_T("Minst 5 resp 10% l�v")}, {_T("Omotiverat h�g")}, {_T("S�nkt under 5 resp 10%")} };

const int VEGTYP_INDICES[VEGTYP_CT]						= { 11, 12, 21, 31, 32, 33, 34, 41, 42, 51, 61, 62, 63, 71, 72, 73 };
const int JORDART_INDICES[JORDART_CT]					= { 11, 12, 13, 14, 21, 22, 23, 24, 25, 31, 32 };
const int MYR_INDICES[MYR_CT]							= { 0, 1, 3 };
const int HALLMARK_INDICES[HALLMARK_CT]					= { 0, 1, 3 };
const int FORNMINNEN_INDICES[FORNMINNEN_CT]				= { 0, 1, 3 };
const int KULTURLAMNING_INDICES[KULTURLAMNING_CT]		= { 0, 1, 3 };
const int NATURVARDESTRAD_INDICES[NATURVARDESTRAD_CT]	= { 1, 3 };
const int NEDSKRAPNING_INDICES[NEDSKRAPNING_CT]			= { 1, 3 };
