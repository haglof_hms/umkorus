#include "stdafx.h"
#include "MessageDlg.h"
#include "UMKORUSStrings.h"
#include "UMKORUSGenerics.h"
#include "DataView.h"
#include "DataFrame.h"

using namespace std;


// CDataView

IMPLEMENT_DYNCREATE(CDataView, CWinFormsView)
CDataView::CDataView():
CWinFormsView(Egenuppfoljning::Controls::FormHanteraData::typeid),
m_pDB(NULL)
{
}

CDataView::~CDataView()
{
	if( m_pDB )
	{
		delete m_pDB;
		m_pDB = NULL;
	}
}


BEGIN_MESSAGE_MAP(CDataView, CWinFormsView)
	ON_WM_CREATE()
	ON_WM_COPYDATA()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromSuite)
	ON_WM_SETFOCUS()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_WM_DESTROY()
END_MESSAGE_MAP()



Egenuppfoljning::Controls::FormHanteraData^ CDataView::GetControl()
{
	System::Windows::Forms::Control^ control = CWinFormsView::GetControl();
	return safe_cast<Egenuppfoljning::Controls::FormHanteraData^>(control);
}

int CDataView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if( CWinFormsView::OnCreate(lpCreateStruct) == -1 )
		return -1;

	// Request db connection
	CDBFunc::setupForDBConnection( AfxGetMainWnd()->GetSafeHwnd(), this->GetSafeHwnd() );

	// Set up language filename
	m_sLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, getLangSet(), LANGUAGE_FN_EXT);
	return 0;
}



BOOL CDataView::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CDataView::OnSize(UINT nType, int cx, int cy)
{
	CWinFormsView::OnSize(nType, cx, cy);
}

void CDataView::OnDestroy()
{
  GetControl()->Close();
}

void CDataView::OnInitialUpdate()
{
	CWinFormsView::OnInitialUpdate();

	// First of all, make sure required tables exist
	m_pDB->MakeSureTablesExist();
	
	//Add the required events.
	GetControl()->DataEventUpdate += MAKE_DELEGATE( Egenuppfoljning::DataEventHandler, OnKORUSDataUpdate );
	GetControl()->DataEventRemove += MAKE_DELEGATE( Egenuppfoljning::DataEventHandler, OnKORUSDataRemove );
	GetControl()->StatusEventRefresh += MAKE_DELEGATE( Egenuppfoljning::StatusEventHandler, OnKORUSStatusRefresh );
	GetControl()->StatusEventGUIUpdate += MAKE_DELEGATE( Egenuppfoljning::StatusEventHandler, OnKORUSStatusGUIUpdate);

	//Maximera fönstret
	GetParentFrame()->ShowWindow( SW_SHOWMAXIMIZED );
	GetControl()->UppdateraRapportData(m_pDB->GetKORUSData());
}

void CDataView::OnSetFocus(CWnd* pOldWnd)
{
	CWinFormsView::OnSetFocus(pOldWnd);
	//GetControl()->UppdateraRapportData(m_pDB->GetKORUSData());
	GetControl()->UppdateraGUI();
}

void CDataView::OnKORUSStatusGUIUpdate( System::Object^ /*sender*/, Egenuppfoljning::KorusStatusEventArgs^ kd )
{
	if(kd->DBIndex==0)
	{ //Första position
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, TRUE);
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, TRUE);
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_LIST, TRUE);
	}
	else if(kd->DBIndex==kd->SistaDBIndex)
	{ //Sista position
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, kd->SistaDBIndex>0);
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, kd->SistaDBIndex>0);
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_LIST, TRUE);
	}
	else
	{
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, TRUE);
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, TRUE);
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, TRUE);
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, TRUE);
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_LIST, TRUE);
	}

	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, !kd->TomDataBas); //edit
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, !kd->TomDataBas);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_PREVIEW_ITEM, !kd->TomDataBas);

	if(kd->TomDataBas || kd->SistaDBIndex==0)
	{
		 //Disabla allt om databas är tom eller endast ett element kvar.
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);
		AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_LIST, FALSE);
	}
}

void CDataView::OnKORUSStatusRefresh( System::Object^ /*sender*/, Egenuppfoljning::KorusStatusEventArgs^ /*kd*/ )
{
	//Read the data from the database, into to the rapport vector, and show it in the GUI
	GetControl()->UppdateraRapportData(m_pDB->GetKORUSData());
	AfxMessageBox(_T("Datat har nu blivit uppdaterat från databasen."), MB_OK | MB_ICONINFORMATION);
}

void CDataView::OnKORUSDataUpdate( System::Object^ /*sender*/, Egenuppfoljning::KorusDataEventArgs^ kd )
{
	if(m_pDB->UpdateKORUSData(kd))
	{
		AfxMessageBox(_T("Den lagrade KORUS rapporten har nu blivit uppdaterad."), MB_OK | MB_ICONINFORMATION);
	}
}

void CDataView::OnKORUSDataRemove( System::Object^ /*sender*/, Egenuppfoljning::KorusDataEventArgs^ kd )
{
	if(m_pDB->DeleteKORUSData(kd))
	{
		GetControl()->TaBortKORUSDataLyckades();
	}
}

LRESULT CDataView::OnMessageFromSuite(WPARAM wParam, LPARAM lParam)
{
	Egenuppfoljning::KorusDataEventArgs^ *rapportData;
	switch(wParam)
	{
	case ID_OPEN_ITEM:
		GetControl()->RedigeraRapport();
		break;
	case ID_DELETE_ITEM:
		GetControl()->TabortRapport();
		break;
	case ID_PREVIEW_ITEM:
		GetControl()->VisaRapport();
		break;
	case ID_DBNAVIG_START:
		GetControl()->GåTillFörstaRapport();
		break;
	case ID_DBNAVIG_NEXT:
		GetControl()->GåTillNästaRapport();
		break;
	case ID_DBNAVIG_PREV:
		GetControl()->GåTillFöregåendeRapport();
		break;
	case ID_DBNAVIG_END:
		GetControl()->GåTillSistaRapport();
		break;
	case ID_DBNAVIG_LIST:
		showFormView( IDD_LISTA_RAPPORTER, m_sLangFN );
		break;
	case ID_UPDATE_ITEM:
		rapportData=(Egenuppfoljning::KorusDataEventArgs^*)lParam;
		GetControl()->GåTillRapport(*rapportData);
		break;
	default:
		break;
	}

 return 0;
}
LRESULT CDataView::OnSuiteMessage(WPARAM /*wParam*/, LPARAM /*lParam*/)
{
	return 0;
}


BOOL CDataView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if( pData->cbData == sizeof(DB_CONNECTION_DATA) )
	{
		DB_CONNECTION_DATA connData;

		// Check if we have already set up a connection
		if( !m_pDB )
		{
			memcpy( &connData, pData->lpData, sizeof(DB_CONNECTION_DATA) );
			m_pDB = new CDBFunc(connData);
		}
	}

	return CWinFormsView::OnCopyData(pWnd, pData);
}

