#pragma once

#include "stdafx.h"

class CKORUSDoc : public CDocument
{
protected: // create from serialization only
	CKORUSDoc();
	virtual ~CKORUSDoc();

	DECLARE_DYNCREATE(CKORUSDoc)

// Generated message map functions
protected:
	//{{AFX_MSG(CKORUSDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	virtual BOOL OnNewDocument( );
};
