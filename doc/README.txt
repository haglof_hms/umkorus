UMKORUS

Exempel p� den korrekta fil/katalog strukturen som kr�vs f�r UMKORUS modulen finns under -> "HMS Release" i detta projekt. 

De filer som kr�vs f�r att UMKORUS modulen ska fungera �r: 

HMS\Egenuppfoljning.dll
HMS\Egenuppfoljning.dll.config
HMS\FastReport.Bars.dll
HMS\FastReport.dll
HNS\Microsoft.Office.Interop.Outlook.dll
HMS\OFFICE.DLL

HMS\Language\UMKORUSENU.xml
HMS\Language\UMKORUSSVE.xml

HMS\Modules\UMKORUS.dll

HMS\Settings\Report_Biobr�nsle.xsd
HMS\Settings\Report_Gallring.xsd
HMS\Settings\Report_MB.xsd
HMS\Settings\Report_Plantering.xsd
HMS\Settings\Report_R�jning.xsd
HMS\Settings\Report_Slutavverkning.xsd
HMS\Settings\Settings.en-US.xml
HMS\Settings\Settings.sv-SE.xml
HMS\Settings\Settings.xsd

HMS\Setup\ShellData\Holmen.xml
HMS\Setup\ShellData\HolmenSkog.ico

Under "C:\Users\<anv�ndare>\Documents\KORUS\" lagras rapporterna som ".keg" filer, exempel: Plantering_654645_Testtrakt2_5_6465.keg


F�ljande m�ste vara installerat och inkluderat i Visual Studio Paths (Tools/Options/VC++ Directories:
* Microsoft Windows SDK for Visual Studio 2008

* Codejock Xtreme ToolkitPro (default location)
- Executeable files: C:\Program Files\Codejock Software\MFC\Xtreme ToolkitPro v11.2.1
- Include files: C:\Program Files\Codejock Software\MFC\Xtreme ToolkitPro v11.2.1\Source
- Library files: C:\Program Files\Codejock Software\MFC\Xtreme ToolkitPro v11.2.1\lib\vc90

SQLAPI++ (default location)
- Include files: C:\SQLAPI\include
- Libary files: C:\SQLAPI\lib

--------------------------------------------------------------------------------------------------


F�ljande m�ste checkas ut var f�r sig i samma katalog d�r projektet ligger med de korrekta namnen:
lib (tex checkout https://hagsrv02/svn/code/PC/HMS/lib/trunk)
tinyxml (tex checkout tag: https://hagsrv02/svn/code/PC/HMS/tinyxml/trunk)
DBTransactionLib (tex checkout tag: https://hagsrv02/svn/code/PC/HMS/DBTransactionLib/trunk)
HMSFuncLib (tex checkout tag: https://hagsrv02/svn/code/PC/HMS/HMSFuncLib/trunk)

Exempel:
workspace\UMKORUS
workspace\tinyxml
workspace\lib
workspace\DBTransactionLib
workspace\HMSFuncLib

ANV�NDAR BESKRIVNING:

APPLIKATIONER:
Man matar in olika data i applikationerna och l�gger till ytor f�r att sedan genom �f�rhandsgranska� se hur rapporten blir. N�r man �r klar lagrar man rapporten eller v�ljer att s�nda den direkt fr�n applikationen. E-post inst�llningar beh�ver i s� fall justeras f�r detta test. Dessa rapportfiler kan sedan importeras av UMKORUS i HMS.

HMS UMKORUS:
Fr�n denna modul i HMS s� kan man v�lja att importera rapport filer (.keg). N�r rapporten �r importerad s� kan man granska och ev. redigera data. Man kan sedan v�lj att antingen neka rapporten eller godk�nna rapporten. Godk�nner man rapporten s� kommer den att lagras i databasen. Ifall ett id f�r en entrepren�r saknar namn s� kommer  en fr�ga upp vilket namn det id:et ska ha och detta m�ste skrivas in. Ett kvitto kan s�ndas tillbaka vid neka/godk�nn och ytterligare inst�llningar f�r dessa kvitton finns i e-post inst�llningar. Efter att rapporten har blivit lagrad s� finns det olika alternativ att lista datat f�r rapporten. Dels kan man g� in och editera datat liknande applikationen via hantera data. Lista rapporter, listar kort �versiktlig data f�r alla rapporter som sedan �ven kan filtreras beroende p� rapportypen med olika radio-buttons. Ytterligare filter g�r ocks� att l�gga p� genom att v�lja det i filter-dialogen. Ytor visar alla ytor, och fr�gor alla fr�gor och samma filtrerings-regler g�ller �ven h�r. I inst�llningar finns alternativen att �ndra entrepren�r namn men ocks� att �ndra var rapporten kommer att lagras beroende p� vilket val som g�rs.

PROJEKT:

UMKORUS SVN Kod:
https://hagsrv02/svn/code/PC/HMS/UMKORUS/trunk

UMKORUS Release uppdaterad f�r HMS Temp:
\\hagfil01\Kod\Hagl�f Information System\HMS Temp

UMKORUS Nedbantad Release version:
\\hagfil01\Kod\KORUS\UMKORUS HMS Release

UMKORUS Nedbantad Release version som Zip-fil.
https://hagsrv02/svn/code/PC/HMS/UMKORUS/trunk

EXEMPELDATA Rapporter:
\\hagfil01\Kod\Hagl�f Information System\HMS Exempeldata\KORUS\Rapporter

BIOBR�NSLE Release Installation:
\\hagfil01\Kod\KORUS\KORUS Biobr�nsle Setup

BIOBR�NSLE SVN Kod:
https://hagsrv02/svn/code/PC/KORUS/Egenuppf�ljning/KORUS Biobr�nsle/trunk

GALLRING Release Installation
\\hagfil01\Kod\KORUS\KORUS Gallring Setup

GALLRING SVN Kod:
https://hagsrv02/svn/code/PC/KORUS/Egenuppf�ljning/KORUS Gallring/trunk

MARKBEREDNING Release Installation
\\hagfil01\Kod\KORUS\KORUS Markberedning Setup

MARKBEREDNING SVN Kod:
https://hagsrv02/svn/code/PC/KORUS/Egenuppf�ljning/KORUS Markberedning/trunk

PLANTERING Release Installation
\\hagfil01\Kod\KORUS\KORUS Plantering Setup

PLANTERING SVN Kod:
https://hagsrv02/svn/code/PC/KORUS/Egenuppf�ljning/KORUS Plantering/trunk

R�JNING Release Installation
\\hagfil01\Kod\KORUS\KORUS R�jning Setup

R�JNING SVN Kod:
https://hagsrv02/svn/code/PC/KORUS/Egenuppf�ljning/KORUS R�jning/trunk

SLUTAVVERKNING Release Installation
\\hagfil01\Kod\KORUS\KORUS Slutavverkning Setup

SLUTAVVERKNING SVN Kod:
https://hagsrv02/svn/code/PC/KORUS/Egenuppf�ljning/KORUS Slutavverkning/trunk
