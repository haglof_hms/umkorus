#pragma once

#pragma managed

#include <afxwin.h>
#include <afxwinforms.h>
#include <map>
#include "DBFunc.h"

#using <Egenuppfoljning.dll>

using namespace Egenuppfoljning;
using namespace std; 

class CRapporterView : public CWinFormsView
{
public:
	void OnKORUSDataHämtaFrågorFörValtFilter(System::Object^ sender, Egenuppfoljning::KorusDataEventArgs^ dataobjekt);
	void OnKORUSDataHämtaYtorFörValtFilter(System::Object^ sender, Egenuppfoljning::KorusDataEventArgs^ dataobjekt);
	void OnKORUSStatusRefresh(System::Object^ sender, Egenuppfoljning::KorusStatusEventArgs^ dataobjekt);
	void OnKORUSDataGåTillRapport(System::Object^ sender, Egenuppfoljning::KorusDataEventArgs^ dataobjekt);
	void OnKORUSDataTaBortRapport(System::Object^ sender, Egenuppfoljning::KorusDataEventArgs^ dataobjekt);
	void OnKORUSDataLagraÄndradeRapporter( System::Object^ sender, List<Egenuppfoljning::KorusDataEventArgs::Data>^ rapporter );

	BEGIN_DELEGATE_MAP( CRapporterView )
		EVENT_DELEGATE_ENTRY( OnKORUSDataHämtaFrågorFörValtFilter, System::Object^, Egenuppfoljning::KorusDataEventArgs^ )
		EVENT_DELEGATE_ENTRY( OnKORUSDataHämtaYtorFörValtFilter, System::Object^, Egenuppfoljning::KorusDataEventArgs^ )
		EVENT_DELEGATE_ENTRY( OnKORUSStatusRefresh, System::Object^, Egenuppfoljning::KorusStatusEventArgs^ )
		EVENT_DELEGATE_ENTRY( OnKORUSDataGåTillRapport, System::Object^, Egenuppfoljning::KorusDataEventArgs^ )
		EVENT_DELEGATE_ENTRY( OnKORUSDataTaBortRapport, System::Object^, Egenuppfoljning::KorusDataEventArgs^ )
		EVENT_DELEGATE_ENTRY( OnKORUSDataLagraÄndradeRapporter, System::Object^, List<Egenuppfoljning::KorusDataEventArgs::Data>^)
	END_DELEGATE_MAP()
	DECLARE_DYNCREATE(CRapporterView)

protected:
	CRapporterView();
	virtual ~CRapporterView();
	CDBFunc *m_pDB;
	LRESULT OnSuiteMessage(WPARAM wParam, LPARAM lParam);
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	Egenuppfoljning::Controls::FormListaRapporter^ GetControl();

	enum { IDD = IDD_LISTA_RAPPORTER };

protected:
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg LRESULT OnMessageFromSuite(WPARAM wParam, LPARAM lParam);
	afx_msg int  OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	CString m_sLangFN;
public:
	DECLARE_MESSAGE_MAP()

};
