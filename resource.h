//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by UMKORUS.rc
//
#define ID_TBTN_PREVIEW_KORUS_REPORT    0
#define IDD_EXPORT                      1206
#define IDD_IMPORTERA_RAPPORT           2201
#define IDD_HANTERA_RAPPORT             2202
#define IDD_LISTA_RAPPORTER             2203
#define IDD_LISTA_YTOR                  2204
#define IDD_LISTA_FRAGOR                2205
#define IDD_INSTALLNINGAR               2206
#define IDR_TOOLBAR1                    7060
#define IDD_MESSAGE                     7131
#define IDC_HTML_TEXT                   7132
#define ID_TBTN_EDIT_KORUS_REPORT       32818
#define ID_TBTN_IMPORT_REPORT           32819
#define ID_TBTN_PREVIEW_REPORT          32820
#define ID_TBTN_REPORT_PRINT            32822
#define ID_TBTN_KORUS_SETTINGS          32823
#define ID_TBTN_KORUS_MAIL              32823
#define ID_TBTN_EDIT_REPORT             32827

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        7065
#define _APS_NEXT_COMMAND_VALUE         32828
#define _APS_NEXT_CONTROL_VALUE         7185
#define _APS_NEXT_SYMED_VALUE           7008
#endif
#endif
