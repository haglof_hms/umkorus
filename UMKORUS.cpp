// UMKORUS.cpp : Defines the initialization routines for the DLL.
//

#include "stdafx.h"
#include "KORUSFrame.h"
#include "KORUSView.h"
#include "InstallningarFrame.h"
#include "InstallningarView.h"
#include "DataFrame.h"
#include "DataView.h"
#include "RapporterFrame.h"
#include "RapporterView.h"
#include "YtorFrame.h"
#include "YtorView.h"
#include "Fr�gorFrame.h"
#include "Fr�gorView.h"
#include "UMKORUSDoc.h"
#include "UMKORUS.h"

// globals
RLFReader* g_pXML;
HINSTANCE g_hInstance = NULL;
static AFX_EXTENSION_MODULE UMKORUSDLL = { NULL, NULL };

#ifdef _MANAGED
#pragma managed(push, off)
#endif 

extern "C" int APIENTRY DllMain(HINSTANCE hInstance, DWORD dwReason, LPVOID lpReserved)
{
	// Remove this if you use lpReserved
	UNREFERENCED_PARAMETER(lpReserved);

	if (dwReason == DLL_PROCESS_ATTACH)
	{
		TRACE0("UMKORUS.DLL Initializing!\n");
	
		// create the XML-parser class.
		g_pXML = new RLFReader();

		// Extension DLL one-time initialization
		if (!AfxInitExtensionModule(UMKORUSDLL, hInstance))
			return 0;
	}
	else if (dwReason == DLL_PROCESS_DETACH)
	{
		TRACE0("UMKORUS.DLL Terminating!\n");

		// Terminate the library before destructors are called
		AfxTermExtensionModule(UMKORUSDLL);

		delete g_pXML;
	}

	g_hInstance = hInstance;

	return 1;   // ok
}

#ifdef _MANAGED
#pragma managed(pop)
#endif


void DLL_BUILD InitModule(CWinApp *pApp, LPCTSTR suite, vecINDEX_TABLE &idx, vecINFO_TABLE &vecInfo)
{
	new CDynLinkLibrary(UMKORUSDLL);
	CString sLangFN;
	CString sVersion;
	CString sCopyright;
	CString sCompany;

	CString sModuleFN = getModuleFN(g_hInstance);
	sLangFN.Format(_T("%s%s"), getLanguageDir(), PROGRAM_NAME);

	// Doc Importera rapport
	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_IMPORTERA_RAPPORT, 
			RUNTIME_CLASS(CKORUSDoc),
			RUNTIME_CLASS(CKORUSFrame),
			RUNTIME_CLASS(CKORUSView)));
	idx.push_back(INDEX_TABLE(IDD_IMPORTERA_RAPPORT, suite, sLangFN, TRUE));

	//Inst�llningar
	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_INSTALLNINGAR, 
		RUNTIME_CLASS(CKORUSDoc),
		RUNTIME_CLASS(CInstallningarFrame),
		RUNTIME_CLASS(CInstallningarView)));
	idx.push_back(INDEX_TABLE(IDD_INSTALLNINGAR, suite, sLangFN, TRUE));

	//Hantera lagrade rapporter
	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_HANTERA_RAPPORT, 
		RUNTIME_CLASS(CKORUSDoc),
		RUNTIME_CLASS(CDataFrame),
		RUNTIME_CLASS(CDataView)));
	idx.push_back(INDEX_TABLE(IDD_HANTERA_RAPPORT, suite, sLangFN, TRUE));

	//Lista lagrade rapporter
	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_LISTA_RAPPORTER, 
		RUNTIME_CLASS(CKORUSDoc),
		RUNTIME_CLASS(CRapporterFrame),
		RUNTIME_CLASS(CRapporterView)));
	idx.push_back(INDEX_TABLE(IDD_LISTA_RAPPORTER, suite, sLangFN, TRUE));

	//Lista lagrade ytor
	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_LISTA_YTOR, 
		RUNTIME_CLASS(CKORUSDoc),
		RUNTIME_CLASS(CYtorFrame),
		RUNTIME_CLASS(CYtorView)));
	idx.push_back(INDEX_TABLE(IDD_LISTA_YTOR, suite, sLangFN, TRUE));

	//Lista lagrade fr�gor
	pApp->AddDocTemplate(new CMultiDocTemplate(IDD_LISTA_FRAGOR, 
		RUNTIME_CLASS(CKORUSDoc),
		RUNTIME_CLASS(CFr�gorFrame),
		RUNTIME_CLASS(CFr�gorView)));
	idx.push_back(INDEX_TABLE(IDD_LISTA_FRAGOR, suite, sLangFN, TRUE));

	// Get version information; 060803 p�d
	sVersion	= getVersionInfo(g_hInstance,VER_NUMBER);
	sCopyright	= getVersionInfo(g_hInstance,VER_COPYRIGHT);
	sCompany	= getVersionInfo(g_hInstance,VER_COMPANY);

	vecInfo.push_back(INFO_TABLE(-999,
		2/* Set to 1 to indicate a SUITE; 2 indicates a  User Module*/,
		sLangFN.GetBuffer(),
		sVersion.GetBuffer(),
		sCopyright.GetBuffer(),
		sCompany.GetBuffer()));
}
