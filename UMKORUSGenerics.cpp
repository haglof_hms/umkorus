#include "stdafx.h"
#include "MessageDlg.h"
#include "UMKORUSGenerics.h"

void showFormView(int idd, LPCTSTR lang_fn, WPARAM wp, LPARAM lp)
{
	CString sResStr;
	CString sDocName;
	CString sCaption;
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	RLFReader *xml = new RLFReader();
	if (xml->Load(lang_fn))
	{
		sCaption = xml->str(idd);
	}
	delete xml;


	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		if( pTemplate )
		{
			pTemplate->GetDocString(sDocName, CDocTemplate::docName);
			sDocName = '\n' + sDocName;
			if( sDocName.Compare(sResStr) == 0 )
			{
				POSITION posDOC = pTemplate->GetFirstDocPosition();
				// Open only one instance of this window; 061002 p�d
				while(posDOC != NULL)
				{
					CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
					POSITION posView = pDocument->GetFirstViewPosition();
					if(posView != NULL)
					{
						CView* pView = pDocument->GetNextView(posView);
						pView->GetParent()->BringWindowToTop();
						pView->GetParent()->SetFocus();
					//	pView->SendMessage(ID_UPDATE_ITEM);
						pView->SendMessage(WM_USER_MSG_SUITE,wp,lp);

						posDOC = (POSITION)1;
						break;
					}
				}

				if (posDOC == NULL)
				{
					pTemplate->OpenDocumentFile(NULL);

					// Find the CDocument for this tamplate, and set title.
					// Title is set in Languagefile; OBS! The nTableIndex
					// matches the string id in the languagefile; 051129 p�d
					POSITION posDOC = pTemplate->GetFirstDocPosition();
					while (posDOC != NULL)
					{
						CDocument* pDocument = pTemplate->GetNextDoc(posDOC);
						// Set the caption of the document. Can be a resource string,
						// a string set in the language xml-file etc.
						CString sDocTitle;
						sDocTitle.Format(_T("%s"),sCaption);
						pDocument->SetTitle(sDocTitle);
						POSITION posView = pDocument->GetFirstViewPosition();
						if(posView != NULL)
						{
							CView* pView = pDocument->GetNextView(posView);
							pView->SendMessage(WM_USER_MSG_SUITE,wp,lp);
							break;
						}	// if(posView != NULL)
					}
					break;
				}
			}
		}
	}
}

CView *getFormViewByID(int idd)
{
	CString sResStr;
	CString sDocName;
	CString sCaption;
	CDocTemplate *pTemplate = NULL;
	CWinApp *pApp = AfxGetApp();

	// Get the stringtable resource, matching the TableIndex
	// This string is compared to the title of the document; 051212 p�d
	sResStr.LoadString(idd);

	POSITION pos = pApp->GetFirstDocTemplatePosition();
	while(pos != NULL)
	{
		pTemplate = pApp->GetNextDocTemplate(pos);
		if( pTemplate )
		{
			pTemplate->GetDocString(sDocName, CDocTemplate::docName);
			sDocName = '\n' + sDocName;
			if( sDocName.Compare(sResStr) == 0 )
			{
				
				POSITION posDOC = pTemplate->GetFirstDocPosition();
				// Open only one instance of this window; 061002 p�d
				while(posDOC != NULL)
				{
					CDocument* pDocument = (CDocument*)pTemplate->GetNextDoc(posDOC);
					POSITION posView = pDocument->GetFirstViewPosition();
					if(posView != NULL)
					{
						CView* pView = pDocument->GetNextView(posView);
						if (pView)
						{
							return pView;
						}
					}
				}
			}
		}
	}
	return NULL;
}

BOOL messageDialog(LPCTSTR cap, LPCTSTR ok_btn, LPCTSTR cancel_btn, LPCTSTR msg)
{
	BOOL bReturn = FALSE;
	CMessageDialog *dlg = new CMessageDialog(cap,ok_btn,cancel_btn,msg);

	bReturn = (dlg->DoModal() == IDOK);

	delete dlg;

	return bReturn;
}
