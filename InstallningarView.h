#pragma once

#pragma managed

#include <afxwin.h>
#include <afxwinforms.h>
#include <map>
#include "DBFunc.h"

#using <Egenuppfoljning.dll>

using namespace Egenuppfoljning;
using namespace std; 

class CInstallningarView : public CWinFormsView
{
public:
	void OnKORUSStatusRefresh(System::Object^ sender, Egenuppfoljning::KorusStatusEventArgs^ dataobjekt);
	void OnKORUSDataRensadatabas(System::Object^ sender, Egenuppfoljning::KorusDataEventArgs^ dataobjekt);
	void OnKORUSDataLagraÄndradeEntreprenörer( System::Object^ sender, List<Egenuppfoljning::KorusDataEventArgs::Entreprenör>^ rapporter );

	BEGIN_DELEGATE_MAP( CInstallningarView )
		EVENT_DELEGATE_ENTRY( OnKORUSStatusRefresh, System::Object^, Egenuppfoljning::KorusStatusEventArgs^ )
		EVENT_DELEGATE_ENTRY( OnKORUSDataRensadatabas, System::Object^, Egenuppfoljning::KorusDataEventArgs^ )
		EVENT_DELEGATE_ENTRY( OnKORUSDataLagraÄndradeEntreprenörer, System::Object^, List<Egenuppfoljning::KorusDataEventArgs::Entreprenör>^)
	END_DELEGATE_MAP()

	DECLARE_DYNCREATE(CInstallningarView)
protected:
	CInstallningarView();
	virtual ~CInstallningarView();
	CDBFunc *m_pDB;

public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	Egenuppfoljning::Controls::FormInställningar^ GetControl();

	enum { IDD = IDD_INSTALLNINGAR };

protected:
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg LRESULT OnMessageFromSuite(WPARAM wParam, LPARAM lParam);
	afx_msg int  OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData);
	CString m_sLangFN;
public:
	DECLARE_MESSAGE_MAP()

};
