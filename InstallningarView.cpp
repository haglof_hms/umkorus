#include "stdafx.h"
#include "MessageDlg.h"
#include "UMKORUSStrings.h"
#include "InstallningarView.h"
#include "InstallningarFrame.h"

using namespace std;

// CInstallningarView

IMPLEMENT_DYNCREATE(CInstallningarView, CWinFormsView)
CInstallningarView::CInstallningarView():
CWinFormsView(Egenuppfoljning::Controls::FormInställningar::typeid),
m_pDB(NULL)
{
}

CInstallningarView::~CInstallningarView()
{
	if( m_pDB )
	{
		delete m_pDB;
		m_pDB = NULL;
	}
}


BEGIN_MESSAGE_MAP(CInstallningarView, CWinFormsView)
	ON_WM_CREATE()
	ON_WM_COPYDATA()
	ON_WM_SETFOCUS()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_WM_DESTROY()
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromSuite)
END_MESSAGE_MAP()



Egenuppfoljning::Controls::FormInställningar^ CInstallningarView::GetControl()
{
	System::Windows::Forms::Control^ control = CWinFormsView::GetControl();
	return safe_cast<Egenuppfoljning::Controls::FormInställningar^>(control);
}

int CInstallningarView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if( CWinFormsView::OnCreate(lpCreateStruct) == -1 )
		return -1;

	// Request db connection
	CDBFunc::setupForDBConnection( AfxGetMainWnd()->GetSafeHwnd(), this->GetSafeHwnd() );

	// Set up language filename
	m_sLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, getLangSet(), LANGUAGE_FN_EXT);
	return 0;
}



BOOL CInstallningarView::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CInstallningarView::OnSize(UINT nType, int cx, int cy)
{
	CWinFormsView::OnSize(nType, cx, cy);
}

void CInstallningarView::OnDestroy()
{
	GetControl()->Close();
}

void CInstallningarView::OnInitialUpdate()
{
	CWinFormsView::OnInitialUpdate();

	// First of all, make sure required tables exist
	m_pDB->MakeSureTablesExist();
	GetControl()->StatusEventRefresh += MAKE_DELEGATE( Egenuppfoljning::StatusEventHandler, OnKORUSStatusRefresh );
	GetControl()->DataEventLagraÄndradeEntreprenörer += MAKE_DELEGATE( Egenuppfoljning::EntreprenörEventHandler, OnKORUSDataLagraÄndradeEntreprenörer );
	GetControl()->DataEventRensaDatabas += MAKE_DELEGATE( Egenuppfoljning::DataEventHandler, OnKORUSDataRensadatabas );

	//Maximera fönstret
	GetParentFrame()->ShowWindow( SW_SHOWMAXIMIZED );

	GetControl()->UppdateraEntreprenörLista(m_pDB->GetEntreprenörerLista());
}

void CInstallningarView::OnSetFocus(CWnd* pOldWnd)
{
	CWinFormsView::OnSetFocus(pOldWnd);

	// Turn off all imagebuttons in the main window
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_PREVIEW_ITEM, FALSE);

	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_LIST, FALSE);
//	GetControl()->UppdateraEntreprenörLista(m_pDB->GetEntreprenörerLista());
}

LRESULT CInstallningarView::OnMessageFromSuite(WPARAM /*wParam*/, LPARAM /*lParam*/)
{
	return 0;
}

void CInstallningarView::OnKORUSStatusRefresh( System::Object^ /*sender*/, Egenuppfoljning::KorusStatusEventArgs^ /*kd*/ )
{
	//Read the data from the database, into to the rapport vector, and show it in the GUI
	GetControl()->UppdateraEntreprenörLista(m_pDB->GetEntreprenörerLista());
	AfxMessageBox(_T("Datat för entreprenörer har nu blivit uppdaterat från databasen."), MB_OK | MB_ICONINFORMATION);
}

void CInstallningarView::OnKORUSDataRensadatabas( System::Object^ /*sender*/, Egenuppfoljning::KorusDataEventArgs^ /*kd*/ )
{
	if(m_pDB->DropAllTables() && m_pDB->MakeSureTablesExist())
	{
		AfxMessageBox(_T("Databasen är nu rensad från all data som tillhör KORUS och nya tomma tabeller har blivit skapade."), MB_OK | MB_ICONINFORMATION);
	}
}

void CInstallningarView::OnKORUSDataLagraÄndradeEntreprenörer( System::Object^ /*sender*/, List<Egenuppfoljning::KorusDataEventArgs::Entreprenör>^ entreprenörer )
{
	if(m_pDB->UpdateKORUSEntreprenörer(entreprenörer))
	{
		if(AfxMessageBox(_T("De lagrade KORUS entreprenörerna har nu blivit uppdaterade. Uppdatera alla rapporter med denna data?"), MB_YESNO | MB_ICONQUESTION)==IDYES)
		{
			if(m_pDB->UpdateKORUSRapporter(entreprenörer))
			{
				AfxMessageBox(_T("De lagrade KORUS rapporterna har nu blivit uppdaterade."), MB_OK | MB_ICONINFORMATION);
			}
		}
	}
}

BOOL CInstallningarView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if( pData->cbData == sizeof(DB_CONNECTION_DATA) )
	{
		DB_CONNECTION_DATA connData;

		// Check if we have already set up a connection
		if( !m_pDB )
		{
			memcpy( &connData, pData->lpData, sizeof(DB_CONNECTION_DATA) );
			m_pDB = new CDBFunc(connData);
		}
	}

	return CWinFormsView::OnCopyData(pWnd, pData);
}
