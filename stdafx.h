// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently

#pragma once

#pragma warning(disable: 4996)

#ifndef VC_EXTRALEAN
#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers
#endif

#define WINVER 0x0500		// Target Windows 2000
#define _WIN32_WINNT 0x0500

//#define _ATL_CSTRING_EXPLICIT_CONSTRUCTORS	// some CString constructors will be explicit

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions

//#ifndef _AFX_NO_OLE_SUPPORT
#include <afxole.h>         // MFC OLE classes
#include <afxodlgs.h>       // MFC OLE dialog classes
#include <afxdisp.h>        // MFC Automation classes
//#endif // _AFX_NO_OLE_SUPPORT

#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT


#include "Resource.h"

// Xtreeme toolkit
#if (_MSC_VER > 1310) // VS2005
#pragma comment(linker, "\"/manifestdependency:type='Win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='X86' publicKeyToken='6595b64144ccf1df' language='*'\"")
#endif

//#define _XTLIB_NOAUTOLINK
#include <XTToolkitPro.h> // Xtreme Toolkit MFC extensions

#define MSG_IN_SUITE				 			(WM_USER + 10)			// This identifer's used to send messages internally

#define IDC_KORUSREPORT							8000
#define IDC_PLOTREPORT							8001

const LPCTSTR PROGRAM_NAME			  			= _T("UMKORUS");	// Name of suite/module, used on setting Language filename; 051214 p�d

const LPCTSTR REGWP_KORUS_IMPORTERAVIEW				= _T("UMKORUS\\Importera\\Placement");
const LPCTSTR REGWP_KORUS_HANTERADATAVIEW			= _T("UMKORUS\\HanteraData\\Placement");
const LPCTSTR REGWP_KORUS_RAPPORTERVIEWSELECT	= _T("UMKORUS\\Rapporter\\Placement");
const LPCTSTR REGWP_KORUS_FRAGORVIEWSELECT		= _T("UMKORUS\\Fragor\\Placement");
const LPCTSTR REGWP_KORUS_YTORVIEWSELECT			= _T("UMKORUS\\Ytor\\Placement");
const LPCTSTR REGWP_KORUS_INSTALLNINGARVIEW		= _T("UMKORUS\\Installningar\\Placement");

#include "ResLangFileReader.h"
#include "pad_hms_miscfunc.h"
#include <afxdlgs.h>
#include <afxctl.h>
#include <afxtempl.h>

#define MAX(x, y) x > y ? x : y

extern HINSTANCE g_hInstance;
