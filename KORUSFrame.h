#pragma once

#include "UMKORUSDoc.h"

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
class CKORUSFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CKORUSFrame)

	CXTPToolBar m_wndToolBar;
	CXTPStatusBar m_wndStatusBar;
public:
	CKORUSFrame();
	virtual ~CKORUSFrame();
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

protected:
	BOOL m_bFirstShow;
	afx_msg LRESULT OnMessageFromSuite(WPARAM wParam, LPARAM lParam);
	afx_msg void OnDestroy();	
	afx_msg void OnClose();
	afx_msg int  OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnPaint(void);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	DECLARE_MESSAGE_MAP()
};
