#pragma once

void showFormView(int idd, LPCTSTR lang_fn, WPARAM wp=0, LPARAM lp=0);
CView *getFormViewByID(int idd);
BOOL messageDialog(LPCTSTR cap, LPCTSTR ok_btn, LPCTSTR cancel_btn, LPCTSTR msg);

class PLANTHEADER
{
public:
	PLANTHEADER() { Reset(); }
	~PLANTHEADER() {}

	int tradslag;
	int vitalitet;
	int orsak;				// Orsak (om d�lig vitalitet)
	int planteringspunkt;
	int battre;				// Finns b�ttre (J/N)
	int planteringsdjup;	// Planteringsdjup
	int tilltryckning;

	void Reset()
	{
		tradslag			= -1;
		vitalitet			= -1;
		orsak				= -1;
		planteringspunkt	= -1;
		battre				= -1;
		planteringsdjup		= -1;
		tilltryckning		= -1;
	}
};

