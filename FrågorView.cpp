#include "stdafx.h"
#include "MessageDlg.h"
#include "UMKORUSStrings.h"
#include "UMKORUSGenerics.h"
#include "Fr�gorView.h"
#include "Fr�gorFrame.h"

using namespace std;

// CFr�gorView

IMPLEMENT_DYNCREATE(CFr�gorView, CWinFormsView)
CFr�gorView::CFr�gorView():
CWinFormsView(Egenuppfoljning::Controls::FormListaFr�gor::typeid),
m_pDB(NULL)
{
}

CFr�gorView::~CFr�gorView()
{
	if( m_pDB )
	{
		delete m_pDB;
		m_pDB = NULL;
	}
}


BEGIN_MESSAGE_MAP(CFr�gorView, CWinFormsView)
	ON_WM_CREATE()
	ON_WM_COPYDATA()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromSuite)
	ON_WM_SETFOCUS()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_WM_DESTROY()
END_MESSAGE_MAP()



Egenuppfoljning::Controls::FormListaFr�gor^ CFr�gorView::GetControl()
{
	System::Windows::Forms::Control^ control = CWinFormsView::GetControl();
	return safe_cast<Egenuppfoljning::Controls::FormListaFr�gor^>(control);
}

int CFr�gorView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if( CWinFormsView::OnCreate(lpCreateStruct) == -1 )
		return -1;

	// Request db connection
	CDBFunc::setupForDBConnection( AfxGetMainWnd()->GetSafeHwnd(), this->GetSafeHwnd() );

	// Set up language filename
	m_sLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, getLangSet(), LANGUAGE_FN_EXT);

	return 0;
}



BOOL CFr�gorView::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CFr�gorView::OnSize(UINT nType, int cx, int cy)
{
	CWinFormsView::OnSize(nType, cx, cy);
}

void CFr�gorView::OnDestroy()
{
	GetControl()->Close();
}

void CFr�gorView::OnInitialUpdate()
{
	CWinFormsView::OnInitialUpdate();

	// First of all, make sure required tables exist
	m_pDB->MakeSureTablesExist();
	GetControl()->StatusEventRefresh += MAKE_DELEGATE( Egenuppfoljning::StatusEventHandler, OnKORUSStatusRefresh );
	GetControl()->DataEventG�TillRapport += MAKE_DELEGATE( Egenuppfoljning::DataEventHandler, OnKORUSDataG�TillRapport );
	GetControl()->DataEventTaBortFr�ga += MAKE_DELEGATE(Egenuppfoljning::DataEventHandler, OnKORUSDataTaBortFr�ga);
	GetControl()->DataEventLagra�ndradeFr�gor += MAKE_DELEGATE(Egenuppfoljning::Fr�gorEventHandler, OnKORUSLagra�ndradeFr�gor);

	//Maximera f�nstret
	GetParentFrame()->ShowWindow( SW_SHOWMAXIMIZED );
	GetControl()->UppdateraFr�geformul�rLista(m_pDB->GetFragorLista());
}

void CFr�gorView::OnSetFocus(CWnd* pOldWnd)
{
	CWinFormsView::OnSetFocus(pOldWnd);

	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, TRUE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, TRUE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, TRUE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_PREVIEW_ITEM, FALSE);

	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_LIST, FALSE);
	//GetControl()->UppdateraFr�geformul�rLista(m_pDB->GetFragorLista());
}

void CFr�gorView::OnKORUSStatusRefresh( System::Object^ /*sender*/, Egenuppfoljning::KorusStatusEventArgs^ /*kd*/ )
{
	//Read the data from the database, into to the rapport vector, and show it in the GUI
	GetControl()->UppdateraFr�geformul�rLista(m_pDB->GetFragorLista());
	AfxMessageBox(_T("Datat f�r fr�gor har nu blivit uppdaterat fr�n databasen."), MB_OK | MB_ICONINFORMATION);
}
void CFr�gorView::OnKORUSDataG�TillRapport( System::Object^ /*sender*/, Egenuppfoljning::KorusDataEventArgs^ kd )
{
	showFormView( IDD_HANTERA_RAPPORT, m_sLangFN, ID_UPDATE_ITEM, (LPARAM)&kd );
}

void CFr�gorView::OnKORUSDataTaBortFr�ga( System::Object^ /*sender*/, Egenuppfoljning::KorusDataEventArgs^ kd )
{
	if(m_pDB->DeleteKORUSFr�geformul�r(kd))
	{
		GetControl()->TaBortKORUSDataLyckades();
	}
}
void CFr�gorView::OnKORUSLagra�ndradeFr�gor( System::Object^ /*sender*/, List<Egenuppfoljning::KorusDataEventArgs::Fr�gor>^ fr�gor )
{
	if(m_pDB->UpdateKORUSFr�gor(fr�gor))
	{
		AfxMessageBox(_T("De lagrade KORUS fr�gorna har nu blivit uppdaterade."), MB_OK | MB_ICONINFORMATION);
	}
}


LRESULT CFr�gorView::OnMessageFromSuite(WPARAM wParam, LPARAM /*lParam*/)
{
	switch(wParam)
	{
	case ID_OPEN_ITEM:
		GetControl()->G�TillRapportData();
		break;
	case ID_SAVE_ITEM:
		GetControl()->Lagra�ndradeKORUSFr�gor();
		break;
	case ID_DELETE_ITEM:
		GetControl()->TaBortKORUSFr�ga();
		break;
	default:
		break;
	}
	return 0;
}
LRESULT CFr�gorView::OnSuiteMessage(WPARAM /*wParam*/, LPARAM /*lParam*/)
{
	return 0;
}


BOOL CFr�gorView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if( pData->cbData == sizeof(DB_CONNECTION_DATA) )
	{
		DB_CONNECTION_DATA connData;

		// Check if we have already set up a connection
		if( !m_pDB )
		{
			memcpy( &connData, pData->lpData, sizeof(DB_CONNECTION_DATA) );
			m_pDB = new CDBFunc(connData);
		}
	}

	return CWinFormsView::OnCopyData(pWnd, pData);
}

