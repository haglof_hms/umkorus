#pragma once

#include "UMKORUSDoc.h"

#define CChildFrameBase CXTPFrameWndBase<CMDIChildWnd>
class CDataFrame : public CChildFrameBase
{
	DECLARE_DYNCREATE(CDataFrame)
public:
	CDataFrame();
	virtual ~CDataFrame();
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

protected:
	BOOL m_bFirstShow;
	afx_msg LRESULT OnMessageFromSuite(WPARAM wParam, LPARAM lParam);
	afx_msg void OnDestroy();	
	afx_msg void OnClose();
	afx_msg int  OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSetFocus(CWnd *pOldWnd);
	afx_msg void OnPaint(void);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	LRESULT OnMessageFromShell(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()
};
