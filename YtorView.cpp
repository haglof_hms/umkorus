#include "stdafx.h"
#include "MessageDlg.h"
#include "UMKORUSStrings.h"
#include "UMKORUSGenerics.h"
#include "YtorView.h"
#include "YtorFrame.h"

using namespace std;

// CYtorView

IMPLEMENT_DYNCREATE(CYtorView, CWinFormsView)
CYtorView::CYtorView():
CWinFormsView(Egenuppfoljning::Controls::FormListaYtor::typeid),
m_pDB(NULL)
{
}

CYtorView::~CYtorView()
{
	if( m_pDB )
	{
		delete m_pDB;
		m_pDB = NULL;
	}
}


BEGIN_MESSAGE_MAP(CYtorView, CWinFormsView)
	ON_WM_CREATE()
	ON_WM_COPYDATA()
	ON_MESSAGE(MSG_IN_SUITE, OnSuiteMessage)
	ON_MESSAGE(WM_USER_MSG_SUITE, OnMessageFromSuite)
	ON_WM_SETFOCUS()
	ON_WM_SIZE()
	ON_WM_SHOWWINDOW()
	ON_WM_DESTROY()
END_MESSAGE_MAP()



Egenuppfoljning::Controls::FormListaYtor^ CYtorView::GetControl()
{
	System::Windows::Forms::Control^ control = CWinFormsView::GetControl();
	return safe_cast<Egenuppfoljning::Controls::FormListaYtor^>(control);
}

int CYtorView::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if( CWinFormsView::OnCreate(lpCreateStruct) == -1 )
		return -1;

	// Request db connection
	CDBFunc::setupForDBConnection( AfxGetMainWnd()->GetSafeHwnd(), this->GetSafeHwnd() );

	// Set up language filename
	m_sLangFN.Format(_T("%s%s%s%s"), getLanguageDir(), PROGRAM_NAME, getLangSet(), LANGUAGE_FN_EXT);
	return 0;
}



BOOL CYtorView::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CView::PreCreateWindow(cs) )
		return FALSE;

	cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
	cs.style |= WS_CLIPCHILDREN|WS_CLIPSIBLINGS;

	return TRUE;
}

void CYtorView::OnSize(UINT nType, int cx, int cy)
{
	CWinFormsView::OnSize(nType, cx, cy);
}

void CYtorView::OnDestroy()
{
	GetControl()->Close();
}

void CYtorView::OnInitialUpdate()
{
	CWinFormsView::OnInitialUpdate();

	// First of all, make sure required tables exist
	m_pDB->MakeSureTablesExist();
	GetControl()->StatusEventRefresh += MAKE_DELEGATE( Egenuppfoljning::StatusEventHandler, OnKORUSStatusRefresh );
	GetControl()->DataEventGåTillRapport += MAKE_DELEGATE( Egenuppfoljning::DataEventHandler, OnKORUSDataGåTillRapport );
	GetControl()->DataEventTaBortYta += MAKE_DELEGATE( Egenuppfoljning::DataEventHandler, OnKORUSDataTaBortYta);
	GetControl()->DataEventLagraÄndradeYtor += MAKE_DELEGATE(Egenuppfoljning::YtorEventHandler, OnKORUSLagraÄndradeYtor);

	//Maximera fönstret
	GetParentFrame()->ShowWindow( SW_SHOWMAXIMIZED );
	GetControl()->UppdateraYtaLista(m_pDB->GetYtorLista());
}


void CYtorView::OnSetFocus(CWnd* pOldWnd)
{
	CWinFormsView::OnSetFocus(pOldWnd);

	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE, ID_NEW_ITEM, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_OPEN_ITEM, TRUE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_SAVE_ITEM, TRUE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DELETE_ITEM, TRUE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_PREVIEW_ITEM, FALSE);

	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_START, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_NEXT, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_PREV, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_END, FALSE);
	AfxGetMainWnd()->SendMessage(WM_USER_MSG_SUITE,	ID_DBNAVIG_LIST, FALSE);

//	GetControl()->UppdateraYtaLista(m_pDB->GetYtorLista());
}

void CYtorView::OnKORUSStatusRefresh( System::Object^ /*sender*/, Egenuppfoljning::KorusStatusEventArgs^ /*kd*/ )
{
	//Read the data from the database, into to the rapport vector, and show it in the GUI
	GetControl()->UppdateraYtaLista(m_pDB->GetYtorLista());
	AfxMessageBox(_T("Datat för ytor har nu blivit uppdaterat från databasen."), MB_OK | MB_ICONINFORMATION);
}

void CYtorView::OnKORUSDataGåTillRapport( System::Object^ /*sender*/, Egenuppfoljning::KorusDataEventArgs^ kd )
{
	showFormView( IDD_HANTERA_RAPPORT, m_sLangFN, ID_UPDATE_ITEM, (LPARAM)&kd );
}

void CYtorView::OnKORUSDataTaBortYta( System::Object^ /*sender*/, Egenuppfoljning::KorusDataEventArgs^ kd )
{
	if(m_pDB->DeleteKORUSYta(kd))
	{
		GetControl()->TaBortKORUSDataLyckades();
	}
}

void CYtorView::OnKORUSLagraÄndradeYtor( System::Object^ /*sender*/, List<Egenuppfoljning::KorusDataEventArgs::Yta>^ ytor )
{
	if(m_pDB->UpdateKORUSYtor(ytor))
	{
		AfxMessageBox(_T("De lagrade KORUS ytorna har nu blivit uppdaterade."), MB_OK | MB_ICONINFORMATION);
	}
}

LRESULT CYtorView::OnMessageFromSuite(WPARAM wParam, LPARAM /*lParam*/)
{
	switch(wParam)
	{
	case ID_OPEN_ITEM:
		GetControl()->GåTillRapportData();
		break;
	case ID_SAVE_ITEM:
		GetControl()->LagraÄndradeKORUSYtor();
		break;
	case ID_DELETE_ITEM:
		GetControl()->TaBortKORUSYta();
		break;
	default:
		break;
	}
	return 0;
}

LRESULT CYtorView::OnSuiteMessage(WPARAM /*wParam*/, LPARAM /*lParam*/)
{
	return 0;
}


BOOL CYtorView::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pData)
{
	// if size doesn't match we don't know what this is
	if( pData->cbData == sizeof(DB_CONNECTION_DATA) )
	{
		DB_CONNECTION_DATA connData;

		// Check if we have already set up a connection
		if( !m_pDB )
		{
			memcpy( &connData, pData->lpData, sizeof(DB_CONNECTION_DATA) );
			m_pDB = new CDBFunc(connData);
		}
	}

	return CWinFormsView::OnCopyData(pWnd, pData);
}

